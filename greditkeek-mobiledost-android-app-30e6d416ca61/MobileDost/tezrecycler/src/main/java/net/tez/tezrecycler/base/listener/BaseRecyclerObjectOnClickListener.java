package net.tez.tezrecycler.base.listener;

/**
 * Created by FARHAN DHANANI on 6/20/2018.
 */
public interface BaseRecyclerObjectOnClickListener<T> extends BaseRecyclerViewListener {
    void onItemClicked(T item);
}
