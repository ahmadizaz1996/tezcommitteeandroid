package net.tez.tezrecycler.model;

/**
 * Created by VINOD KUMAR on 11/23/2018.
 */
public abstract class GenericDropDownListModel {

    private boolean isOpen;

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }
}
