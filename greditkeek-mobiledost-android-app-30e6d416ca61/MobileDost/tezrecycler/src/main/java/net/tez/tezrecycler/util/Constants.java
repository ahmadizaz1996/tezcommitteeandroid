package net.tez.tezrecycler.util;

/**
 * Created by FARHAN DHANANI on 1/15/2019.
 */
public class Constants {
    public static final String STRING_NULL_ITEM_ADDITION_EXCEPTION_IN_ADAPTER = "Cannot add null item to the Recycler adapter";
}
