package net.tez.tezrecycler.adapter;




import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.tezrecycler.util.Constants;

import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * Created by FARHAN DHANANI on 6/20/2018.
 */
public abstract class AdapterDisplayListItemSuccessively<T, L extends BaseRecyclerViewListener,
        VH extends BaseViewHolder<T,L>>
        extends GenericRecyclerViewAdapter<T, L, VH> {

    private Stack<T> stack = new Stack<>();

    public AdapterDisplayListItemSuccessively(){}

    public AdapterDisplayListItemSuccessively(L listener){
        super(listener);
    }

    public AdapterDisplayListItemSuccessively(List<T> items, L listener) {
        super(items, listener);
    }

    public void addAllElementsToStack(List<T> stackElements){
        if(stackElements == null){
            throw new IllegalArgumentException(Constants.STRING_NULL_ITEM_ADDITION_EXCEPTION_IN_ADAPTER);
        }
        Collections.reverse(stackElements);
        this.stack.addAll(stackElements);
    }

    public void addNewElementsToStack(List<T> stackElements){
        this.stack.clear();
        this.addAllElementsToStack(stackElements);
    }

    public boolean displayQuestionOneByOne(){
        if(!this.stack.isEmpty()){
            this.add(this.stack.pop());
            return true;
        }
        return false;
    }

    public boolean allItemsConsumed() {
        return this.stack == null || this.stack.isEmpty();
    }
}
