package net.tez.tezrecycler.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.model.GenericDropDownListModel;
import net.tez.tezrecycler.viewholder.GenericDropDownAdapterViewHolder;

import java.util.List;

/**
 * Created by VINOD KUMAR on 11/23/2018.
 */
public abstract class GenericDropDownAdapter<T extends GenericDropDownListModel,
        L extends BaseRecyclerViewListener,
        VH extends GenericDropDownAdapterViewHolder<T, L>>
        extends GenericRecyclerViewAdapter<T, L, VH> {

    public GenericDropDownAdapter() {

    }

    public GenericDropDownAdapter(@NonNull List<T> items) {
        super(items);
    }

    public GenericDropDownAdapter(@NonNull List<T> items, @Nullable L listener) {
        super(items, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.bindItem(position, getItems(), getListener());
    }
}
