package net.tez.tezrecycler.viewholder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.tez.tezrecycler.adapter.GenericDropDownAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.tezrecycler.model.GenericDropDownListModel;

import java.util.List;

/**
 * Created by VINOD KUMAR on 11/23/2018.
 */
public abstract class GenericDropDownAdapterViewHolder<T extends GenericDropDownListModel, L extends BaseRecyclerViewListener> extends BaseViewHolder<T, L> {

    protected final View header;
    protected GenericDropDownAdapter adapter;

    public GenericDropDownAdapterViewHolder(GenericDropDownAdapter adapter, View itemView) {
        super(itemView);
        this.adapter = adapter;
        this.header = getHeader();
    }

    @Override
    public final void onBind(T item, L listener) {
        super.onBind(item, listener);
    }

    public void bindItem(int position, List<T> items, @Nullable L listener) {
        header.setOnClickListener(view -> setOnHeaderClickListener(items, position));
        onBind(position, items, listener);
    }

    protected void resetDropDownForItem(List<T> items, int position, boolean isOpen) {
        int openItemIndex = -1;

        for (int i = 0; i < items.size(); i++) {
            T item = items.get(i);
            if (item.isOpen())
                openItemIndex = i;
            item.setOpen(false);
        }

        items.get(position).setOpen(isOpen);

        if (openItemIndex != -1)
            adapter.notifyItemChanged(openItemIndex);

        if (openItemIndex != position)
            adapter.notifyItemChanged(position);
    }

    protected void toggleItem(T item, int position) {
        item.setOpen(!item.isOpen());
        adapter.notifyItemChanged(position);
    }

    protected void setOnHeaderClickListener(List<T> items, int position) {
        resetDropDownForItem(items, position, !items.get(position).isOpen());
    }

    @Override
    public void onBind(int position, List<T> items, L listener) {
        super.onBind(position, items, listener);
        handleDropDown(items.get(position));
    }

    protected void handleDropDown(T item) {
        if (item.isOpen())
            this.onOpen();
        else
            this.onClose();
    }

    protected abstract @NonNull
    View getHeader();

    protected abstract void onOpen();

    protected abstract void onClose();
}
