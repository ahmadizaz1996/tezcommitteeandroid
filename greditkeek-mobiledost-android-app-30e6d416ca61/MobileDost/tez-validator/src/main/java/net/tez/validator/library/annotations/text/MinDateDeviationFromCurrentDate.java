package net.tez.validator.library.annotations.text;

import net.tez.validator.library.annotations.Filterable;
import net.tez.validator.library.filters.text.MinDateDeviationFromCurrentDateFilter;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by FARHAN DHANANI on 11/25/2018.
 */

@SuppressWarnings("unused")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Filterable(MinDateDeviationFromCurrentDateFilter.class)
public @interface MinDateDeviationFromCurrentDate {
    String dateFormat();

    int dayDeviation();

    int monthDeviation();

    int yearDeviation();

    int[] value();
}
