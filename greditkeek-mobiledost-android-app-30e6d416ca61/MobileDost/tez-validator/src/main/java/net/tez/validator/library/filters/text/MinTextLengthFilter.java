package net.tez.validator.library.filters.text;

import androidx.annotation.NonNull;
import android.widget.EditText;

import net.tez.validator.library.annotations.text.MinLength;
import net.tez.validator.library.filters.Filter;

/**
 * Created by FARHAN DHANANI on 5/13/2018.
 */

@SuppressWarnings("unused")
public class MinTextLengthFilter implements Filter<EditText, MinLength> {

    @Override
    public boolean isValidated(@NonNull EditText view, @NonNull MinLength annotation) {
        String text = view.getText().toString();
        return text.length() >= annotation.length();
    }
}
