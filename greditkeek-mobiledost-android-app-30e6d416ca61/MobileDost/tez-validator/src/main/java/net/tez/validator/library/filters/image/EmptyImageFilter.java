package net.tez.validator.library.filters.image;

import androidx.annotation.NonNull;
import android.widget.ImageView;

import net.tez.validator.library.annotations.image.EmptyImage;
import net.tez.validator.library.filters.Filter;

/**
 * Created by FARHAN DHANANI on 9/19/2018.
 */

@SuppressWarnings("unused")
public class EmptyImageFilter implements Filter<ImageView, EmptyImage> {

    @Override
    public boolean isValidated(@NonNull ImageView view, @NonNull EmptyImage annotation) {
        return view.getDrawable() != null;
    }
}
