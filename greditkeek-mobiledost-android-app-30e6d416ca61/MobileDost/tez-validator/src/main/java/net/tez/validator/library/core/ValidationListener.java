package net.tez.validator.library.core;

import androidx.annotation.NonNull;
import android.view.View;

/**
 * Created by FARHAN DHANANI on 5/12/2018.
 */

@SuppressWarnings("WeakerAccess")
public interface ValidationListener {
    void validateSuccess();
    void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain);
    default void validatePassed(@NonNull View v){

    }
    default void validateUnSuccessFull(){

    }
}
