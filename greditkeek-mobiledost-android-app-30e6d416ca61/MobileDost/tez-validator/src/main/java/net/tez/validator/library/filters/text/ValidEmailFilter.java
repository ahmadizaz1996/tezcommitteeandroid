package net.tez.validator.library.filters.text;

import androidx.annotation.NonNull;
import android.widget.EditText;

import net.tez.validator.library.annotations.text.ValidEmail;
import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;

import java.util.regex.Pattern;

/**
 * Created by FARHAN DHANANI on 5/13/2018.
 */

@SuppressWarnings("unused")
public class ValidEmailFilter implements Filter<EditText, ValidEmail> {

    @Override
    public boolean isValidated(@NonNull EditText view, @NonNull ValidEmail annotation) {
        String text = view.getText().toString();
        if (TextUtil.isNotEmpty(annotation.regex())) {
            return TextUtil.isValidWithRegex(text, Pattern.compile(annotation.regex(), Pattern.CASE_INSENSITIVE));
        }
        else {
            return TextUtil.isValidEmail(text);
        }
    }
}
