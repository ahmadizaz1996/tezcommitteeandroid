package net.tez.validator.library.core;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;

import net.tez.validator.library.annotations.Filterable;
import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.models.FilterProperty;
import net.tez.validator.library.models.ProcessModel;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

/**
 * Created by FARHAN DHANANI on 5/13/2018.
 */

@SuppressWarnings({"WeakerAccess", "unchecked", "ConstantConditions"})
public abstract class FieldProcessor {

    private boolean areAllFieldsValidated;
    private ValidationListener validationListener;

    public void init(@NonNull ValidationListener validationListener) {
        areAllFieldsValidated = true;
        this.validationListener = validationListener;
    }

    protected Queue<FilterProperty> getFilters(@NonNull ProcessModel processModel) {

        Map<Integer, FilterProperty> filterMap = new LinkedHashMap<>();

        for (Annotation annotation : processModel.getFilteredField().getDeclaredAnnotations()) {
            if (annotation.annotationType().isAnnotationPresent(Filterable.class)) {
                FilterProperty filterProperty = getFilterProperty(processModel.getInspectedView(), annotation);
                filterMap.put(filterProperty.getOrder(), filterProperty);
            }
        }

        return new LinkedList<>(new TreeMap<>(filterMap).values());
    }

    protected FilterProperty getFilterProperty(@NonNull View inspectedView, @NonNull Annotation annotation) {

        FilterProperty property = new FilterProperty();
        property.setFilterable(true);

        Object valObj = null;
        try {
            valObj = annotation.annotationType().getDeclaredMethod("value").invoke(annotation);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e){
            e.printStackTrace();
        } catch (NoSuchMethodException e){
            e.printStackTrace();
        }

        if (!(valObj instanceof int[])) {
            throw new IllegalStateException("value must be int array");
        }

        int[] values = (int[]) valObj;

        if (values.length == 0) {
            throw new IllegalStateException("No order found in value array. note: value array should be contains minimum 2 value 1st is order and the 2nd is string message id");
        }

        else if (values.length == 1) {
            throw new IllegalStateException("No message id found in value array. Note: value array should be contains minimum 2 value 1st is order and the 2nd is string message id");
        }

        else if (values.length == 3) {
            int state = values[2];
            if (state != View.VISIBLE && state != View.INVISIBLE && state != View.GONE) {
                throw new IllegalStateException("third value of value array should be contain one of these value (View.VISIBLE, View.INVISIBLE, View.GONE)");
            }
            else {
                property.setFilterable(inspectedView.getVisibility() == state);
            }
        }

        // try to get filter
        try {

            Filterable filterableAnnotation = annotation.annotationType().getAnnotation(Filterable.class);
            Class<? extends Filter<? extends View, ? extends Annotation>> filterClass = (Class<? extends Filter<? extends View, ? extends Annotation>>) filterableAnnotation.annotationType().getDeclaredMethod("value").invoke(filterableAnnotation);
            property.setFilter(makeFilter(filterClass));
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e){
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e){
            throw new RuntimeException(e);
        }

        property.setOrder(values[0]);
        property.setMessage(getMessage(inspectedView.getContext(), values[1]));
        property.setAnnotation(annotation);

        return property;
    }

    protected String getMessage(Context context, int resId) {
        try {
            return context.getResources().getString(resId);
        }
        catch (Exception ex) {
            return "";
        }
    }

    @Nullable
    protected Filter makeFilter(Class<? extends Filter<?, ?>> filterClass) {

        try {
            Constructor<? extends Filter<?, ?>> constructor = filterClass.getDeclaredConstructor();
            try {
                return constructor.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e){
                e.printStackTrace();
            } catch (InvocationTargetException e){
                e.printStackTrace();
            }
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    protected void validateFailed(@NonNull FilterProperty filterProperty, @NonNull ProcessModel processModel, @NonNull FilterChain filterChain) {
        areAllFieldsValidated = false;
        boolean isFilterChainEmpty = !filterChain.hasNext();
        validationListener.validateFailed(new ValidationError(filterProperty.getMessage(), processModel.getInspectedView(), filterProperty.getAnnotation()), filterChain);
        if(isFilterChainEmpty){
            validationListener.validateUnSuccessFull();
        }
    }

    protected void validatePassed(@NonNull View view, FilterChain filterChain){
        //boolean isFilterChainEmpty = !filterChain.hasNext();
        validationListener.validatePassed(view);
//        if(isFilterChainEmpty){
//            validationListener.validateUnSuccessFull();
//        }
    }

    protected void validationCompleted(@NonNull FilterChain filterChain) {
        if (filterChain.hasNext()) {
            filterChain.doFilter();
        }
        else if (areAllFieldsValidated) {
            validationListener.validateSuccess();
        } else{
            validationListener.validateUnSuccessFull();
        }
    }

    public abstract void process(@NonNull ProcessModel processModel, @NonNull FilterChain filterChain);

}
