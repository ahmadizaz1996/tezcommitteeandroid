package net.tez.validator.library.filters.text;

import androidx.annotation.NonNull;
import android.widget.EditText;

import net.tez.validator.library.annotations.text.NotEmpty;
import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;

/**
 * Created by FARHAN DHANANI on 5/13/2018.
 */

@SuppressWarnings("unused")
public class NotEmptyFilter implements Filter<EditText, NotEmpty> {

    @Override
    public boolean isValidated(@NonNull EditText view, @NonNull NotEmpty annotation) {
        return TextUtil.isNotEmpty(view.getText());
    }
}
