package net.tez.validator.library.filters.text;

import androidx.annotation.NonNull;
import android.widget.EditText;

import net.tez.validator.library.annotations.text.MaxDateRange;
import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by FARHAN DHANANI on 11/25/2018.
 */
@SuppressWarnings("unused")
public class MaxDateRangeFilter implements Filter<EditText, MaxDateRange> {

    @Override
    public boolean isValidated(@NonNull EditText view, @NonNull MaxDateRange annotation) {
        String text = view.getText().toString();
        try {
            return TextUtil.getFormattedDate(text, annotation.dateFormat())
                    .before(new Date(annotation.maxDateMillis()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
}
