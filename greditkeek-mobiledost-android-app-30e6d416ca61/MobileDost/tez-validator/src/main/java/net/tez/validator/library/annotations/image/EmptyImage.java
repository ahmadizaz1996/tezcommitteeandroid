package net.tez.validator.library.annotations.image;

import net.tez.validator.library.annotations.Filterable;
import net.tez.validator.library.filters.image.EmptyImageFilter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by FARHAN DHANANI on 9/19/2018.
 */
@SuppressWarnings("unused")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Filterable(EmptyImageFilter.class)
public @interface EmptyImage {
    int[] value();
}