package net.tez.validator.library.annotations;

import androidx.annotation.NonNull;

import net.tez.validator.library.filters.Filter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by FARHAN DHANANI on 8/4/2018.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
@SuppressWarnings("unused")
public @interface Filterable {
    @NonNull Class<? extends Filter> value();
}
