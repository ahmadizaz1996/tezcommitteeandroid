package net.tez.validator.library.filters.spinner;

import androidx.annotation.NonNull;
import android.widget.Spinner;

import net.tez.validator.library.annotations.spinner.SpinnerValidation;
import net.tez.validator.library.filters.Filter;

/**
 * Created by FARHAN DHANANI on 8/12/2018.
 */

@SuppressWarnings("unused")
public class SpinnerValidationFilter implements Filter<Spinner, SpinnerValidation> {

    @Override
    public boolean isValidated(@NonNull Spinner view, @NonNull SpinnerValidation annotation) {
        return view.getSelectedItemPosition() != annotation.index();
    }
}
