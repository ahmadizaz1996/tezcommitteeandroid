package net.tez.validator.library.filters.checkbox;

import androidx.annotation.NonNull;
import android.widget.CheckBox;

import net.tez.validator.library.annotations.checkbox.CheckedCheckBox;
import net.tez.validator.library.filters.Filter;

/**
 * Created by FARHAN DHANANI on 5/16/2018.
 */

@SuppressWarnings("unused")
public class CheckBoxFilter implements Filter<CheckBox, CheckedCheckBox> {

    @Override
    public boolean isValidated(@NonNull CheckBox view, @NonNull CheckedCheckBox annotation) {
        return view.isChecked();
    }
}
