package net.tez.validator.library.core;

import androidx.annotation.NonNull;

import java.lang.reflect.Field;

/**
 * Created by FARHAN DHANANI on 10/19/2018.
 */
public interface FieldProvider {

    Field[] getFields(@NonNull Object inspectedObj);
}
