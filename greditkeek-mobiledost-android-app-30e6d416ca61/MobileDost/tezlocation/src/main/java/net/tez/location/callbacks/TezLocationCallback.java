package net.tez.location.callbacks;

import android.location.Location;

/**
 * Created by Vinod Kumar on 10/20/2018.
 */

public interface TezLocationCallback {
    void onResult(Location location);
    default void onFailed(String error){}
}
