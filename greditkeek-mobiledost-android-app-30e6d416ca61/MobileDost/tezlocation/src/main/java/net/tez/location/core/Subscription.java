package net.tez.location.core;

/**
 * Created by Vinod Kumar on 10/21/2018.
 */

public interface Subscription {

    void start();
    void stop();
}
