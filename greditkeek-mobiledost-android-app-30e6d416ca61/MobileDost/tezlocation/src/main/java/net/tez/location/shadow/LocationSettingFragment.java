package net.tez.location.shadow;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;

import net.tez.location.callbacks.CurrentLocationCallback;
import net.tez.location.callbacks.InitializationCallback;

import java.util.concurrent.TimeoutException;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;
import static com.google.android.gms.location.LocationServices.getSettingsClient;

/**
 * Created by Vinod Kumar on 10/13/2018.
 */

@SuppressLint("MissingPermission")
public class LocationSettingFragment extends Fragment {

    private static final int REQUEST_CHECK_SETTINGS = 2016;
    private static LocationSettingFragment INSTANCE;
    private ResolvableApiException e;
    private LocationRequest locationRequest;
    private CurrentLocationCallback currentLocationCallback;
    private InitializationCallback initializationCallback;
    private CountDownTimer timer = new CountDownTimer(10 * 1000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            removeFragment();
            sendFailureCallback(new TimeoutException("Get location time out"));
        }
    };

    public static LocationSettingFragment newInstance(@NonNull LocationRequest locationRequest, @NonNull CurrentLocationCallback currentLocationCallback) {
        LocationSettingFragment.INSTANCE = new LocationSettingFragment();
        LocationSettingFragment fragment = LocationSettingFragment.INSTANCE;
        fragment.locationRequest = locationRequest;
        fragment.currentLocationCallback = currentLocationCallback;
        return fragment;
    }

    public static LocationSettingFragment newInstance(@NonNull LocationRequest locationRequest, @NonNull InitializationCallback initializationCallback) {
        LocationSettingFragment.INSTANCE = new LocationSettingFragment();
        LocationSettingFragment fragment = LocationSettingFragment.INSTANCE;
        fragment.locationRequest = locationRequest;
        fragment.initializationCallback = initializationCallback;
        return fragment;
    }

    public static LocationSettingFragment getInstance() {
        return LocationSettingFragment.INSTANCE;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        requestLocation();
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            getLocation();
        } else {
            String error = data != null
                    ? data.getStringExtra("error") != null
                    ? data.getStringExtra("error")
                    : "failed to get location"
                    : "failed to get location";

            if (currentLocationCallback != null) {
                currentLocationCallback.onFailed(error);
            } else if (initializationCallback != null) {
                initializationCallback.onFailed(error);
            }
            removeFragment();
        }
    }

    private void requestLocation() {
        try {
            Activity activity = getActivity();
            if (activity != null) {
                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
                SettingsClient client = getSettingsClient(activity);
                client.checkLocationSettings(builder.build())
                        .addOnSuccessListener(locationSettingsResponse -> getLocation())
                        .addOnFailureListener(e -> {
                            try {
                                if (e instanceof ResolvableApiException && getActivity() != null) {
                                    this.e = (ResolvableApiException) e;
                                    Intent intent = new Intent(activity, ShadowLocationActivity.class);
                                    startActivityForResult(intent, REQUEST_CHECK_SETTINGS);
                                } else {
                                    sendFailureCallback(e);
                                    removeFragment();
                                }
                            } catch (Exception e1) {
                                sendFailureCallback(e1);
                            }
                        });
            }
        } catch (Exception e) {
            sendFailureCallback(e);
        }
    }

    ResolvableApiException getResolvableApiException() {
        return e;
    }

    private void sendFailureCallback(Exception e) {
        if (currentLocationCallback != null) {
            currentLocationCallback.onFailed(e.getMessage());
        } else if (initializationCallback != null) {
            initializationCallback.onFailed(e.getMessage());
        }
    }

    private void getLocation() {
        FragmentActivity activity = getActivity();
        if (currentLocationCallback != null && activity != null) {

            currentLocationCallback.onInitialized();

            timer.start();

            FusedLocationProviderClient locationService = getFusedLocationProviderClient(activity);
            locationService.requestLocationUpdates(locationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    locationService.removeLocationUpdates(this);
                    timer.cancel();
                    removeFragment();
                    if (locationResult.getLastLocation() != null)
                        currentLocationCallback.onResult(locationResult.getLastLocation());
                    else
                        currentLocationCallback.onFailed("Cannot fetch location");
                }
            }, null);

        } else if (initializationCallback != null) {
            initializationCallback.onSuccess();
            removeFragment();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    private void removeFragment() {

        try {
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null) {
                fragmentManager.beginTransaction().remove(this).commitAllowingStateLoss();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}