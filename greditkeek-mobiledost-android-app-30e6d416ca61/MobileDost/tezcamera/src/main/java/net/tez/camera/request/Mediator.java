package net.tez.camera.request;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import net.tez.camera.activity.CameraActivity;
import net.tez.camera.exceptions.TitanCameraException;
import net.tez.camera.interfaces.Camera;
import net.tez.camera.interfaces.CaptureResult;
import net.tez.camera.util.Constants;

/**
 * Created by VINOD KUMAR on 1/10/2019.
 */
public class Mediator implements Camera {

    private ClientRequest clientRequest;

    Mediator() {
        this.clientRequest = new ClientRequest();
    }

    ClientRequest getClientRequest() {
        return clientRequest;
    }

    @Override
    public void startCamera(@NonNull Context context, @Nullable CaptureResult captureResult) {
        clientRequest.setFromCallback(true);
        registerReceiverToReceiveCaptureResults(context, captureResult);
        Intent intent = new Intent(context, CameraActivity.class);
        intent.putExtra(Constants.CLIENT_REQUEST, clientRequest);
        context.startActivity(intent);
    }

    @Override
    public void startCamera(@NonNull Fragment fragment) {
        clientRequest.setFromCallback(false);
        Context context = fragment.getContext();
        if (context != null) {
            Intent intent = new Intent(context, CameraActivity.class);
            intent.putExtra(Constants.CLIENT_REQUEST, clientRequest);
            fragment.startActivityForResult(intent, CameraRequestBuilder.REQUEST_CODE_CAMERA);
        }
    }

    @Override
    public void startCamera(@NonNull Fragment fragment, @Nullable Bundle parameters) {
        clientRequest.setFromCallback(false);
        Context context = fragment.getContext();
        if (context != null) {
            Intent intent = new Intent(context, CameraActivity.class);
            intent.putExtra(Constants.CLIENT_REQUEST, clientRequest);
            intent.putExtra(Constants.CLIENT_PARAMETERS, parameters);
            fragment.startActivityForResult(intent, CameraRequestBuilder.REQUEST_CODE_CAMERA);
        }
    }

    @Override
    public void startCamera(@NonNull Activity activity) {
        clientRequest.setFromCallback(false);
        Intent intent = new Intent(activity, CameraActivity.class);
        intent.putExtra(Constants.CLIENT_REQUEST, clientRequest);
        activity.startActivityForResult(intent, CameraRequestBuilder.REQUEST_CODE_CAMERA);
    }

    @Override
    public void startCamera(@NonNull Activity activity, @Nullable Bundle parameters) {
        clientRequest.setFromCallback(false);
        Intent intent = new Intent(activity, CameraActivity.class);
        intent.putExtra(Constants.CLIENT_REQUEST, clientRequest);
        intent.putExtra(Constants.CLIENT_PARAMETERS, parameters);
        activity.startActivityForResult(intent, CameraRequestBuilder.REQUEST_CODE_CAMERA);
    }

    private void registerReceiverToReceiveCaptureResults(@NonNull final Context context,
                                                         @Nullable final CaptureResult captureResult) {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                context.unregisterReceiver(this);
                if (captureResult != null) {
                    if (intent.getBooleanExtra(Constants.IS_ERROR, false))
                        captureResult.onFailure(new TitanCameraException(intent.getStringExtra(Constants.ERROR_MESSAGE)));
                    else
                        captureResult.onCapture(intent.getStringArrayListExtra(CameraRequestBuilder.RESULT_IMAGE_FILES_ARRAY_LIST));
                }
            }
        };
        context.registerReceiver(receiver, new IntentFilter(Constants.BROADCAST_RECEIVER_CAPTURE_RESULTS));
    }
}
