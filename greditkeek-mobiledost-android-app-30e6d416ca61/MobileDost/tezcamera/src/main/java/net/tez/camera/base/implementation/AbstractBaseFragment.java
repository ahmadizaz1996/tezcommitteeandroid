package net.tez.camera.base.implementation;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentActivity;

import net.tez.camera.base.contracts.AbstractBaseFragmentView;
import net.tez.camera.interfaces.FragmentActivityCommunication;
import net.tez.fragment.util.base.AbstractHelperFragment;

/**
 * Created by VINOD KUMAR on 12/26/2018.
 */
public abstract class AbstractBaseFragment extends AbstractHelperFragment implements AbstractBaseFragmentView {

    @Override
    public void onStart() {
        super.onStart();
        isInstanceOfActivity(FragmentActivityCommunication.class, f -> f.setCurrentFragment(this));
    }


    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void showToast(@StringRes int resId) {
        getBaseFragmentActivity(fragmentActivity -> {
            fragmentActivity.runOnUiThread(() -> Toast.makeText(fragmentActivity, resId, Toast.LENGTH_SHORT).show());
        });
    }

    @Override
    public void showToast(@NonNull String s) {
        getBaseFragmentActivity(fragmentActivity -> {
            fragmentActivity.runOnUiThread(() -> Toast.makeText(fragmentActivity, s, Toast.LENGTH_SHORT).show());
        });
    }

    @Override
    public void finishActivity() {
        getBaseFragmentActivity(FragmentActivity::finish);
    }
}
