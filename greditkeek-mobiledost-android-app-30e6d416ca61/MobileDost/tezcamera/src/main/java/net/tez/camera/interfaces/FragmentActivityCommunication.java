package net.tez.camera.interfaces;

import androidx.annotation.NonNull;

import net.tez.camera.base.implementation.AbstractBaseFragment;
import net.tez.camera.request.ClientRequest;

/**
 * Created by VINOD KUMAR on 12/26/2018.
 */
public interface FragmentActivityCommunication {

    void setCurrentFragment(@NonNull AbstractBaseFragment currentFragment);

    ClientRequest getClientRequest();

    void replaceCamera2WithCamera1();

    void requestPermissionsOnSecurityException();
}
