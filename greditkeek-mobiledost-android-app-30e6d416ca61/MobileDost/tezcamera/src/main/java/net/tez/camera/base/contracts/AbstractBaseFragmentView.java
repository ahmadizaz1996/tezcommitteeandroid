package net.tez.camera.base.contracts;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;


/**
 * Created by VINOD KUMAR on 1/8/2019.
 */
public interface AbstractBaseFragmentView {

    void finishActivity();

    void showToast(@StringRes int resId);

    void showToast(@NonNull String s);
}
