package net.tez.camera.base.implementation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.github.chrisbanes.photoview.PhotoView;

import net.tez.camera.base.contracts.AbstractCameraFragmentView;
import net.tez.camera.cameras.camera2api.R;
import net.tez.camera.interfaces.FragmentActivityCommunication;
import net.tez.camera.request.CameraRequestBuilder;
import net.tez.camera.request.ClientRequest;
import net.tez.camera.util.Constants;
import net.tez.camera.util.FileUtil;
import net.tez.filepicker.request.image.ImagePickerRequestBuilder;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.fragment.util.optional.Optional;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by VINOD KUMAR on 1/8/2019.
 */
public abstract class AbstractCameraFragment extends AbstractBaseFragment implements AbstractCameraFragmentView {

    protected View rootView;
    protected FrameLayout cameraContainer;
    protected View photoViewShow;
    protected View buttonCapture;
    protected View buttonDone;
    protected View buttonRetake;
    protected View viewOverlay;
    protected View galleryIcon;
    protected FrameLayout frameLayoutSwitchCamera;
    protected FrameLayout customLayoutContainer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Optional.doWhen(!isSafeObject(getClientRequest()), this::finishActivity);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_abstract_camera, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initViews(view);
        initOnClickListeners();
        initOnTouchListener();
        applyConfigurations();
    }

    @Nullable
    protected FragmentActivityCommunication getHostActivity() {
        return isInstanceOfActivity(FragmentActivityCommunication.class);
    }

    protected ClientRequest getClientRequest() {
        return Optional.ifPresent(getHostActivity(), FragmentActivityCommunication::getClientRequest);
    }


    @Nullable
    protected Bundle getClientParameters() {
        return getBaseFragmentActivity(fragmentActivity -> {
            return fragmentActivity.getIntent().getBundleExtra(Constants.CLIENT_PARAMETERS);
        });
    }

    @Override
    public boolean onBackPressed() {
        boolean shouldBack = photoViewShow.getVisibility() != View.VISIBLE;
        if (!shouldBack)
            onClickRetry();
        return shouldBack;
    }

    @Override
    public void displayImage(@NonNull Bitmap bitmap) {
        ((PhotoView) this.photoViewShow).setImageBitmap(bitmap);
    }

    @Override
    public void saveImageAndFinish(@NonNull Bitmap bitmap) {
        String rootPath = getClientRequest().getRootPath();
        File file;
        if (rootPath != null) {
            String imagePath = getClientRequest().getImageFileName();
            file = imagePath != null ? FileUtil.createImageFile(rootPath, imagePath) : FileUtil.createImageFile(rootPath);
        } else {
            file = FileUtil.createImageFile();
            if (file != null)
                makePicturePublic(file);
        }
        if (file != null && FileUtil.saveImageInFile(bitmap, file)) {
            sendSuccessResult(file);
        } else {
            showToast(R.string.image_save_error);
            sendFailureResultToClient();
            finishActivity();
        }
    }

    private void sendSuccessResult(@NonNull File file) {
        ArrayList<String> files = new ArrayList<>();
        files.add(file.getAbsolutePath());
        sendSuccessResult(files);
    }

    private void sendSuccessResult(@NonNull ArrayList<String> requestedFiles) {
        if (getClientRequest().isFromCallback())
            sendImageInCallback(requestedFiles);
        else
            sendImageInOnActivityResult(requestedFiles);
        finishActivity();
    }

    @Override
    public void setButtonCaptureVisibility(int visibility) {
        if (buttonCapture.getVisibility() != visibility)
            buttonCapture.post(() -> buttonCapture.setVisibility(visibility));
    }

    @Override
    public void setFrameLayoutSwitchCameraVisibility(int visibility) {
        if (frameLayoutSwitchCamera.getVisibility() != visibility && getClientRequest().isEnableSwitchCamera())
            frameLayoutSwitchCamera.post(() -> frameLayoutSwitchCamera.setVisibility(visibility));
    }

    @Override
    public void makePicturePublic(@NonNull File file) {
        getBaseFragmentActivity(fragmentActivity -> {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(file);
            mediaScanIntent.setData(contentUri);
            fragmentActivity.sendBroadcast(mediaScanIntent);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == ImagePickerRequestBuilder.REQUEST_CODE_IMAGE) {
            ArrayList<String> selectedPictures = data.getStringArrayListExtra(ImagePickerRequestBuilder.RESULT_IMAGE_ARRAY_LIST);
            if (selectedPictures != null && !selectedPictures.isEmpty())
                sendSuccessResult(selectedPictures);
        }
    }

    @CallSuper
    protected void initViews(@NonNull final View view) {
        buttonCapture = getButtonCapture();
        buttonDone = getButtonDone();
        buttonRetake = getButtonRetake();
        photoViewShow = getImageContainer();
        galleryIcon = getGalleryIcon();
        viewOverlay = view.findViewById(R.id.viewOverlay);
        cameraContainer = view.findViewById(R.id.cameraContainer);
        frameLayoutSwitchCamera = view.findViewById(R.id.frameLayoutSwitchCamera);
        customLayoutContainer = view.findViewById(R.id.customLayoutContainer);
    }

    @CallSuper
    protected void initOnClickListeners() {
        buttonCapture.setOnClickListener((DoubleTapSafeOnClickListener) view -> onClickCapture());
        buttonDone.setOnClickListener((DoubleTapSafeOnClickListener) view -> onClickDone());
        buttonRetake.setOnClickListener((DoubleTapSafeOnClickListener) view -> onClickRetry());
        frameLayoutSwitchCamera.setOnClickListener((DoubleTapSafeOnClickListener) view -> onClickSwitchCamera());
        galleryIcon.setOnClickListener((DoubleTapSafeOnClickListener) view -> onClickGalleryIcon());
    }

    @SuppressLint("ClickableViewAccessibility")
    @CallSuper
    protected void initOnTouchListener() {
        cameraContainer.setOnTouchListener(this::onTouchToPreview);
    }

    protected View getImageContainer() {
        return rootView.findViewById(R.id.photoViewShow);
    }

    protected View getButtonCapture() {
        return rootView.findViewById(R.id.imageViewCapture);
    }

    protected View getGalleryIcon() {
        return rootView.findViewById(R.id.galleryIcon);
    }

    protected View getButtonDone() {
        return rootView.findViewById(R.id.imageViewDone);
    }

    protected View getButtonRetake() {
        return rootView.findViewById(R.id.frameLayoutRetake);
    }

    protected boolean onTouchToPreview(View v, MotionEvent event) {
        return false;
    }

    private void sendImageInCallback(@NonNull ArrayList<String> files) {
        getBaseFragmentContext(context -> {
            Intent intent = new Intent(Constants.BROADCAST_RECEIVER_CAPTURE_RESULTS);
            intent.putStringArrayListExtra(CameraRequestBuilder.RESULT_IMAGE_FILES_ARRAY_LIST, files);
            context.sendBroadcast(intent);
        });
    }

    private void sendImageInOnActivityResult(@NonNull ArrayList<String> files) {
        getBaseFragmentActivity(fragmentActivity -> {
            Intent intent = new Intent();
            intent.putStringArrayListExtra(CameraRequestBuilder.RESULT_IMAGE_FILES_ARRAY_LIST, files);
            intent.putExtra(CameraRequestBuilder.PARAMETERS_BUNDLE, getClientParameters());
            fragmentActivity.setResult(Activity.RESULT_OK, intent);
        });
    }

    private void onClickCapture() {
        buttonCapture.setEnabled(false);
        viewOverlay.setVisibility(View.VISIBLE);
        photoViewShow.setVisibility(View.VISIBLE);
        onCapture();
        cameraContainer.setVisibility(View.GONE);
        buttonDone.setVisibility(View.VISIBLE);
        buttonRetake.setVisibility(View.VISIBLE);
        buttonCapture.setVisibility(View.GONE);
        viewOverlay.setVisibility(View.GONE);
        buttonCapture.setEnabled(true);
    }

    private void onClickDone() {
        buttonDone.setEnabled(false);
        onCompleted();
    }

    private void onClickRetry() {
        onRetry();
        cameraContainer.setVisibility(View.VISIBLE);
        buttonCapture.setVisibility(View.VISIBLE);
        photoViewShow.setVisibility(View.GONE);
        buttonDone.setVisibility(View.GONE);
        buttonRetake.setVisibility(View.GONE);
    }

    private void onClickSwitchCamera() {
        onSwitchCamera();
    }

    private void onClickGalleryIcon() {

        new ImagePickerRequestBuilder()
                .setTitle("Select Image")
                .setMaxImageCount(getClientRequest().getMaxImageCount())
                .build()
                .pickImage(this);
    }

    private void applyConfigurations() {
        getBaseFragmentContext(context -> {

            if (isSafeObject(getClientRequest())) {
                Integer customLayoutId = getClientRequest().getCustomLayoutId();

                if (isSafeObject(customLayoutId))
                    LayoutInflater.from(context).inflate(customLayoutId, customLayoutContainer, true);
            }
        });
        boolean enableGallery = getClientRequest().isEnableGallery();
        this.setGalleryIconVisibility(enableGallery ? View.VISIBLE : View.GONE);
    }

    protected void setGalleryIconVisibility(int visibility) {
        this.galleryIcon.setVisibility(visibility);
    }

    private void sendFailureResultToClient() {
        getBaseFragmentActivity(fragmentActivity -> {
            Intent intent = new Intent();
            intent.putExtra(Constants.IS_ERROR, true);
            intent.putExtra(Constants.ERROR_MESSAGE, Constants.FILE_CREATE_ERROR);
            if (getClientRequest().isFromCallback()) {
                intent.setAction(Constants.BROADCAST_RECEIVER_CAPTURE_RESULTS);
                fragmentActivity.sendBroadcast(intent);
            } else {
                intent.putExtra(CameraRequestBuilder.PARAMETERS_BUNDLE, getClientParameters());
                fragmentActivity.setResult(Activity.RESULT_CANCELED, intent);
            }
        });
    }

    protected abstract void onSwitchCamera();

    protected abstract void onCapture();

    protected abstract void onCompleted();

    protected abstract void onRetry();
}
