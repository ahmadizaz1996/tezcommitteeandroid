package net.tez.calendar;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.StyleRes;
import androidx.appcompat.widget.AppCompatButton;

import net.tez.calendar.picker.date.DatePicker;

import java.util.Calendar;

/**
 * Created by FARHAN DHANANI on 10/29/2018.
 */
public abstract class AbstractAndroidDatePickerDialog extends AlertDialog {

    private DatePicker datePicker;

    private View datePickerDisablerView;

    protected AppCompatButton buttonOk;

    private AppCompatButton buttonCancel;

    private View inflatedView;

    @NonNull
    private final OnDateSetListener onDateSetListener;

    AbstractAndroidDatePickerDialog(@NonNull Context context, @NonNull OnDateSetListener onDateSetListener) {
        super(context);
        this.onDateSetListener = onDateSetListener;
    }

    AbstractAndroidDatePickerDialog(@NonNull Context context, @StyleRes int themeResId, @NonNull OnDateSetListener onDateSetListener) {
        super(context, themeResId);
        this.onDateSetListener = onDateSetListener;
    }

    protected void initViews() {
        this.datePicker = findViewById(R.id.datePicker);
        this.datePickerDisablerView = getDatePickerDisablerView();
        this.buttonOk = getButtonOk();
        this.buttonCancel = getButtonCancel();
    }

    private View getDatePickerDisablerView() {
        return findViewById(R.id.datePickerDisablerView);
    }

    public void setDate(int year, int month, int day) {
        datePicker.setDate(year, month, day);
    }

    protected abstract AppCompatButton getButtonOk();

    protected abstract AppCompatButton getButtonCancel();

    protected abstract int getClientCustomLayout();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.android_date_picker_view_default);
        FrameLayout clientCalendarLayout = findViewById(R.id.clientCalendarContainer);
        this.inflatedView = LayoutInflater.from(getContext()).inflate(getClientCustomLayout(), clientCalendarLayout, true);
        initViews();
        initListners();
    }

    private void initListners() {
        setButtonOkOnClickListener();
        setButtonCancelOnClickListener();
    }


    protected void enableDatePickerView(boolean enable) {
        if (enable) {
            this.datePicker.setEnabled(true);
            this.datePickerDisablerView.setVisibility(View.GONE);
        } else {
            this.datePicker.setEnabled(false);
            this.datePickerDisablerView.setVisibility(View.VISIBLE);
        }
    }

    protected void setButtonOkOnClickListener() {
        buttonOk.setOnClickListener(view -> {
            notifyListener();
            dismiss();
        });
    }

    private void setButtonCancelOnClickListener() {
        buttonCancel.setOnClickListener(view -> dismiss());
    }

    protected void notifyListener() {
        this.onDateSetListener.onDateSet(datePicker.getYear(), datePicker.getMonth(), datePicker.getDay());
    }

    private void setMaxDate(long maxDate) {
        this.datePicker.setMaxDate(maxDate);
    }

    private void setMinDate(long minDate) {
        this.datePicker.setMinDate(minDate);
    }

    protected View getInflatedView() {
        return inflatedView;
    }

    public void disableDateBeforeToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        this.setMinDate(calendar.getTimeInMillis());
    }

    public void disableDateAfterToday() {
        Calendar calendar = Calendar.getInstance();
        this.setMaxDate(calendar.getTimeInMillis());
    }

    public void disableForwardDates(long dateInMilliseconds) {
        this.setMaxDate(dateInMilliseconds);
    }

    public interface OnDateSetListener {

        void onDateSet(int year, int month, int day);
    }
}
