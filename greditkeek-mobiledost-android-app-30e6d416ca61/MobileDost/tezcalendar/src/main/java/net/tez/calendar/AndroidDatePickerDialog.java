package net.tez.calendar;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;


/**
 * Created by FARHAN DHANANI on 1/14/2019.
 */
public class AndroidDatePickerDialog extends AbstractAndroidDatePickerDialog {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public AndroidDatePickerDialog(@NonNull Context context, @NonNull OnDateSetListener onDateSetListener) {
        super(context, onDateSetListener);
    }

    AndroidDatePickerDialog(@NonNull Context context, int themeResId, @NonNull OnDateSetListener onDateSetListener) {
        super(context, themeResId, onDateSetListener);
    }

    @Override
    protected AppCompatButton getButtonOk() {
        return getInflatedView().findViewById(R.id.buttonOk);
    }

    @Override
    protected AppCompatButton getButtonCancel() {
        return getInflatedView().findViewById(R.id.buttonCancel);
    }

    @Override
    protected int getClientCustomLayout() {
        return R.layout.date_picker_default_client;
    }
}
