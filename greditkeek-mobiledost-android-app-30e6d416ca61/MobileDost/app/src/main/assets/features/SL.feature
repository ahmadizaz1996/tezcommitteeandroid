Feature: Select language feature for users
    Inorder to use application user must select appropriate language
    As a user of app
    I want to select a language

	Background:
		Given Call api '/v1/user/info' with request 'signup_with_facebook_request.json' and with response 'signup_with_facebook_response.json'

	Scenario: Facebook Login Test
	    When I tap on button with text 'English'
	    Then I tap on button with text 'Get Started'
	    Then I tap on button facebook
	    Then I must see button with text 'Create Account'