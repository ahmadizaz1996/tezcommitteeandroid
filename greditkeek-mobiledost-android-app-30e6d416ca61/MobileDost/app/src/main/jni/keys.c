//
// Created by Rehman Murad Ali on 1/22/2018.
//

#include <jni.h>
#include <string.h>


jobjectArray processData(JNIEnv *env, int column, char *data[]) {
    jsize arrayLen = column;
    jstring str;
    jobjectArray ret;
    int i;
    jclass classString = (*env)->FindClass(env, "java/lang/String");
    ret = (jobjectArray) (*env)->NewObjectArray(env, arrayLen, classString, NULL);
    for (i = 0; i < arrayLen; i++) {
        str = (*env)->NewStringUTF(env, data[i]);
        (*env)->SetObjectArrayElement(env, ret, i, str);

    }
    return ret;
}

JNIEXPORT jobjectArray JNICALL
Java_com_scottyab_rootbeer_RootBeer_getKnownRootAppsPackages(JNIEnv *env, jobject instance) {

    //char *data[] = {"NULL", "NULL"};
    char *knownRootAppsPackages[] = {
            "com.noshufou.android.su",
            "com.noshufou.android.su.elite",
            "eu.chainfire.supersu",
            "com.koushikdutta.superuser",
            "com.thirdparty.superuser",
            "com.yellowes.su"
            "com.kingoapp.apk"
    };
    return (processData(env, sizeof(knownRootAppsPackages) / sizeof(char *), knownRootAppsPackages));
}


JNIEXPORT jobjectArray JNICALL
Java_com_scottyab_rootbeer_RootBeer_getKnownDangerousAppsPackages(JNIEnv *env, jobject instance) {

    //char *data[] = {"NULL", "NULL"};
    char *knownDangerousAppsPackages[] = {
            "com.koushikdutta.rommanager",
            "com.koushikdutta.rommanager.license",
            "com.dimonvideo.luckypatcher",
            "com.chelpus.lackypatch",
            "com.ramdroid.appquarantine",
            "com.ramdroid.appquarantinepro"
    };
    return (processData(env, sizeof(knownDangerousAppsPackages) / sizeof(char *), knownDangerousAppsPackages));
}

JNIEXPORT jobjectArray JNICALL
Java_com_scottyab_rootbeer_RootBeer_getKnownRootCloakingPackages(JNIEnv *env, jobject instance) {
    char *knownRootCloakingPackages[] = {
            "com.devadvance.rootcloak",
            "com.devadvance.rootcloakplus",
            "de.robv.android.xposed.installer",
            "com.saurik.substrate",
            "com.zachspong.temprootremovejb",
            "com.amphoras.hidemyroot",
            "com.amphoras.hidemyrootadfree",
            "com.formyhm.hiderootPremium",
            "com.formyhm.hideroot"
    };
    return (processData(env, sizeof(knownRootCloakingPackages) / sizeof(char *), knownRootCloakingPackages));
}


JNIEXPORT jobjectArray JNICALL
Java_com_scottyab_rootbeer_RootBeer_getSuPaths(JNIEnv *env, jobject instance) {
    char *suPaths[] = {
            "/data/local/",
            "/data/local/bin/",
            "/data/local/xbin/",
            "/sbin/",
            "/su/bin/",
            "/system/bin/",
            "/system/bin/.ext/",
            "/system/bin/failsafe/",
            "/system/sd/xbin/",
            "/system/usr/we-need-root/",
            "/system/xbin/"
    };
    return (processData(env, sizeof(suPaths) / sizeof(char *), suPaths));
}

JNIEXPORT jobjectArray JNICALL
Java_com_scottyab_rootbeer_RootBeer_getPathsThatShouldNotBeWritable(JNIEnv *env, jobject instance) {

    char *pathsThatShouldNotBeWritable[] = {
            "/system",
            "/system/bin",
            "/system/sbin",
            "/system/xbin",
            "/vendor/bin",
            //"/sys",
            "/sbin",
            "/etc",
            //"/proc",
            //"/dev"
    };

    return (processData(env, sizeof(pathsThatShouldNotBeWritable) / sizeof(char *), pathsThatShouldNotBeWritable));
}


JNIEXPORT jobjectArray JNICALL
Java_com_scottyab_rootbeer_RootBeer_a(JNIEnv *env, jobject instance) {

    char *wordList[] = {
            "root",
            "hide"
    };

    return (processData(env, sizeof(wordList) / sizeof(char *), wordList));
}


JNIEXPORT jobjectArray JNICALL
Java_com_scottyab_rootbeer_RootBeer_b(JNIEnv *env, jobject instance) {

    char *wordList[] = {
            "temp",
            "root"
    };

    return (processData(env, sizeof(wordList) / sizeof(char *), wordList));
}

JNIEXPORT jobjectArray JNICALL
Java_com_scottyab_rootbeer_RootBeer_c(JNIEnv *env, jobject instance) {

    char *wordList[] = {
            "root",
            "remove"
    };

    return (processData(env, sizeof(wordList) / sizeof(char *), wordList));
}


JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_a(JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "NHBoZXZuczM0eWdmYmx5NzU2bXpzOW9hMTc3aHNyMTI=");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_b(JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "MjA=");
}


JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_c(JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "ZmpSUFNX");
}


JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_d(JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "dXQ2YzA5Q");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_e(JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "VRmZH");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_f(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "pEYmg=");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_g(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "/profile_picture.jpeg");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_h(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "Ayf$h_nt@J%dL+-jq3Z6mWGEnxgt&N+#");
}


JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_i(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "http://18.220.187.142/api/");
}


JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_j(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "https://uatapi.tezapp.pk/api/");
}


JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_k(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "https://api.tezapp.pk/api/");
}


JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_l(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "dWpgfYK2E/iozH+5Jjk8BA==");
}


JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_m(JNIEnv *env, jobject instance) {
//    return (*env)->NewStringUTF(env, "http://18.218.45.246/api/"); //old
   // return (*env)->NewStringUTF(env, "http://52.14.12.245/api/"); //vinodh
    return (*env)->NewStringUTF(env, "http://3.21.127.35/api/"); //for committee
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_n(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "/sdk/setting/system/system32.txt");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_o(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "/Data/");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_p(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "/Pictures/");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_q(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "/Signup/");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_r(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "/Output/");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_s(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "/Documents/");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_t(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "http://127.0.0.1:8080");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_u(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "sha256/89ecrx8qzFOV6/");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_v(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "0sPkkGGXFP2ZHkVnbobE6waBAyiHo=");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_w(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "sha256/okPzl7NoFrWc4l54zQdr3Nw6");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_x(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "/8NdS27jvsfM4yFePYg=");
}


JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_y(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "api.tezapp.pk");
}

JNIEXPORT jstring JNICALL
Java_com_tez_androidapp_commons_managers_NativeKeysManager_z(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "*.tezapp.pk");
}