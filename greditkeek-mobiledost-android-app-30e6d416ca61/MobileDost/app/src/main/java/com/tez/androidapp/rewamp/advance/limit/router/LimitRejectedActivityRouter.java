package com.tez.androidapp.rewamp.advance.limit.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.limit.view.LimitRejectedActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class LimitRejectedActivityRouter extends BaseActivityRouter {


    public static LimitRejectedActivityRouter createInstance() {
        return new LimitRejectedActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, LimitRejectedActivity.class);
    }
}
