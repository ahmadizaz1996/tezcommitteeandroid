package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;

/**
 * Created by VINOD KUMAR on 7/30/2019.
 */
public class TezLoader extends TezDialog {

    private boolean dissmissOnTransition;

    public TezLoader(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tez_loader);
    }

    public boolean isDissmissOnTransition() {
        return dissmissOnTransition;
    }

    public void setDissmissOnTransition(boolean dissmissOnTransition) {
        this.dissmissOnTransition = dissmissOnTransition;
    }
}
