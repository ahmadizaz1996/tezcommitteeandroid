package com.tez.androidapp.rewamp.general.profile.presenter;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Answer;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;

import java.util.List;

public interface IBasicProfileActivityPresenter {
    void getOccupationOptions(String lang);

    void updateUserBasicProfile();

    void onJobItemSelected(List<Option> options, Option job, Answer givenAnswer);

    void subCategoryDoesNotExist();

    void categoryDoesNotExist();
}
