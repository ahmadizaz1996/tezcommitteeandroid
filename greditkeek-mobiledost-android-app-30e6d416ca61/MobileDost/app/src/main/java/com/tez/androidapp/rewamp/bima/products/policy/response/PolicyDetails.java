package com.tez.androidapp.rewamp.bima.products.policy.response;

public class PolicyDetails {

    int policyId;
    String paymentCorrelationId;
    Boolean resumeAsyncResponse;

    public String getPaymentCorrelationId() {
        return paymentCorrelationId;
    }

    public void setPaymentCorrelationId(String paymentCorrelationId) {
        this.paymentCorrelationId = paymentCorrelationId;
    }

    public Boolean getResumeAsyncResponse() {
        return resumeAsyncResponse;
    }

    public void setResumeAsyncResponse(Boolean resumeAsyncResponse) {
        this.resumeAsyncResponse = resumeAsyncResponse;
    }

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }
}
