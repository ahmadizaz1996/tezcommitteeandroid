package com.tez.androidapp.rewamp.dashboard.presenter;

public interface IDashboardActivityPresenter {

    void loginCall();
    void setDashboard();
    void getCommitteeMetadata();
    void setDashboardFromPreference();
}
