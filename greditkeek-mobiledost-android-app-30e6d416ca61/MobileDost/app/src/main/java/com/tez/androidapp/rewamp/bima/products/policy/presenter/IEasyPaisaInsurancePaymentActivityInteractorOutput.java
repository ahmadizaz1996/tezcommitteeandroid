package com.tez.androidapp.rewamp.bima.products.policy.presenter;

import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InitiatePurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.PurchasePolicyCallback;

public interface IEasyPaisaInsurancePaymentActivityInteractorOutput
        extends InitiatePurchasePolicyCallback,
        PurchasePolicyCallback {
}
