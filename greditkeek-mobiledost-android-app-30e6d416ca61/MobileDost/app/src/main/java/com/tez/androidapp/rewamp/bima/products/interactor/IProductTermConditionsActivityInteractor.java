package com.tez.androidapp.rewamp.bima.products.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

public interface IProductTermConditionsActivityInteractor extends IBaseInteractor {

    void getProductTermConditions(int productId);

    void getProductDetailedTermCondition(int productId);
}
