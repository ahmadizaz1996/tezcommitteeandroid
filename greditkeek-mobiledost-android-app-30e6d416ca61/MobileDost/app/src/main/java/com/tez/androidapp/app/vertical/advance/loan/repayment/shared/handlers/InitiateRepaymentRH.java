package com.tez.androidapp.app.vertical.advance.loan.repayment.shared.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.InitiateRepaymentCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.InitiateRepaymentResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 7/20/2017.
 */

public class InitiateRepaymentRH extends BaseRH<InitiateRepaymentResponse> {

    InitiateRepaymentCallback initiateRepaymentCallback;

    public InitiateRepaymentRH(BaseCloudDataStore baseCloudDataStore, InitiateRepaymentCallback initiateRepaymentCallback) {
        super(baseCloudDataStore);
        this.initiateRepaymentCallback = initiateRepaymentCallback;
    }

    @Override
    protected void onSuccess(Result<InitiateRepaymentResponse> value) {
        InitiateRepaymentResponse initiateRepaymentResponse = value.response().body();
        if (initiateRepaymentResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            initiateRepaymentCallback.onInitiateRepaymentSuccess(initiateRepaymentResponse);
        } else {
            onFailure(initiateRepaymentResponse.getStatusCode(), initiateRepaymentResponse.getErrorDescription());
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        initiateRepaymentCallback.onInitiateRepaymentFailure(errorCode, message);

    }
}
