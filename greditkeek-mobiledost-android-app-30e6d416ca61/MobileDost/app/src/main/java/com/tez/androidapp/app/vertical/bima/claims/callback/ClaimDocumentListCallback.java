package com.tez.androidapp.app.vertical.bima.claims.callback;

import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.MandatoryClaimDocument;

import java.util.List;

/**
 * Created by FARHAN DHANANI on 6/13/2018.
 */
public interface ClaimDocumentListCallback {

    void onClaimDocumentListSuccess(List<MandatoryClaimDocument> documentList);

    void onClaimDocumentListFailure(int errorCode, String message);
}
