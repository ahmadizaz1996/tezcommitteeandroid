package com.tez.androidapp.rewamp.general.transactions;

import com.tez.androidapp.app.general.feature.transactions.callbacks.GetUserTransactionsCallback;
import com.tez.androidapp.app.general.feature.transactions.models.network.dto.response.GetUserTransactionsResponse;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;

/**
 * Created by VINOD KUMAR on 8/28/2019.
 */
public class MyTransactionsActivityInteractor implements IMyTransactionsActivityInteractor {

    private final IMyTransactionsActivityInteractorOutput iMyTransactionsActivityInteractorOutput;

    MyTransactionsActivityInteractor(IMyTransactionsActivityInteractorOutput iMyTransactionsActivityInteractorOutput) {
        this.iMyTransactionsActivityInteractorOutput = iMyTransactionsActivityInteractorOutput;
    }

    @Override
    public void getTransactions() {
        UserAuthCloudDataStore.getInstance().getUserTransactions(new GetUserTransactionsCallback() {
            @Override
            public void onGetUserTransactionsSuccess(GetUserTransactionsResponse getUserTransactionsResponse) {
                iMyTransactionsActivityInteractorOutput.onGetUserTransactionsSuccess(getUserTransactionsResponse);
            }

            @Override
            public void onGetUserTransactionsFailure(int statusCode, String message) {
                if (Utility.isUnauthorized(statusCode))
                    getTransactions();
                else
                    iMyTransactionsActivityInteractorOutput.onGetUserTransactionsFailure(statusCode, message);
            }
        });
    }
}
