package com.tez.androidapp.rewamp.general.beneficiary.view;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy;

import java.util.List;

/**
 * Created by VINOD KUMAR on 8/30/2019.
 */
public interface IManageBeneficiaryActivityView extends IBaseView {

    void setManageBeneficiaryAdapter(@NonNull List<Policy> items);

    void setLayoutEmptyBeneficiaryVisibility(int visibility);

    void setRvPoliciesVisibility(int visibility);

    void onProfileIncompleteError();
}
