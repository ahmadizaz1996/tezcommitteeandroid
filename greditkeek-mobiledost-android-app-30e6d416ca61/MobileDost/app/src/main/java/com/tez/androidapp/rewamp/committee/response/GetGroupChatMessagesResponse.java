
package com.tez.androidapp.rewamp.committee.response;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.tez.androidapp.app.base.response.BaseResponse;

public class GetGroupChatMessagesResponse extends BaseResponse {

    @Expose
    private List<ChatMessage> chatMessages;

    public List<ChatMessage> getChatMessages() {
        return chatMessages;
    }

    public void setChatMessages(List<ChatMessage> chatMessages) {
        this.chatMessages = chatMessages;
    }


}
