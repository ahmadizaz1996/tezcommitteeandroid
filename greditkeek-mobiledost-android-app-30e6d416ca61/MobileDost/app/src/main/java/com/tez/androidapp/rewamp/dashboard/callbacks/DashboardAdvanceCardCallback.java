package com.tez.androidapp.rewamp.dashboard.callbacks;

import com.tez.androidapp.rewamp.dashboard.response.DashboardAdvanceCardResponse;

public interface DashboardAdvanceCardCallback {

    void onDashboardAdvanceCardSuccess(DashboardAdvanceCardResponse dashboardAdvanceCardResponse);

    void onDashboardAdvanceCardFailure(int errorCode, String message);
}
