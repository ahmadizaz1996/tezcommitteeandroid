package com.tez.androidapp.rewamp.signup.interactor;

import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserLoginCallback;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserSignUpSetPinCallback;
import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;
import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserSignUpSetPinResponse;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.signup.presenter.INewTermsAndConditionActivityInteractorOutput;

import net.tez.fragment.util.optional.Optional;

public class NewTermsAndConditionActivityInterator
        implements INewTermsAndConditionActivityInteractor{

    private final INewTermsAndConditionActivityInteractorOutput iNewTermsAndConditionActivityInteractorOutput;

    public NewTermsAndConditionActivityInterator(INewTermsAndConditionActivityInteractorOutput iNewTermsAndConditionActivityInteractorOutput) {
        this.iNewTermsAndConditionActivityInteractorOutput = iNewTermsAndConditionActivityInteractorOutput;
    }

    @Override
    public void callSetPin(String pin){
        UserCloudDataStore.getInstance().setUserPin(pin, new UserSignUpSetPinCallback() {
            @Override
            public void onUserSignUpSetPinSuccess(UserSignUpSetPinResponse userSignUpSetPinResponse) {
                iNewTermsAndConditionActivityInteractorOutput.onUserSignUpSetPinSuccess(
                        userSignUpSetPinResponse, pin);
            }

            @Override
            public void onUserSignUpSetPinError(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        ()->callSetPin(pin),
                        ()->iNewTermsAndConditionActivityInteractorOutput.onUserSignUpSetPinError(errorCode, message));
            }
        });
    }

    @Override
    public void callLogin(UserLoginRequest loginRequest){
        UserCloudDataStore.getInstance().login(loginRequest, new UserLoginCallback() {
            @Override
            public void onUserLoginSuccess(UserLoginResponse loginResponse) {
                iNewTermsAndConditionActivityInteractorOutput.onUserLoginSuccess(loginResponse);
            }

            @Override
            public void onUserLoginFailure(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        ()->callLogin(loginRequest),
                        ()-> iNewTermsAndConditionActivityInteractorOutput.onUserLoginFailure(errorCode, message));
            }
        });
    }
}
