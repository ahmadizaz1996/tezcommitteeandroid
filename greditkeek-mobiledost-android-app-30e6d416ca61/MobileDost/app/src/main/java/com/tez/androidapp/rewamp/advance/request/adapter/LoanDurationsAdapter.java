package com.tez.androidapp.rewamp.advance.request.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.request.model.LoanDuration;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

/**
 * Created by Rehman Murad Ali
 **/
public class LoanDurationsAdapter extends GenericRecyclerViewAdapter<LoanDuration, LoanDurationsAdapter.LoanDurationListener, LoanDurationsAdapter.LoanDurationViewHolder> {


    private int selectedLoanDurationPosition = 0;
    private double loanAmount = 1000;

    public LoanDurationsAdapter(@NonNull List<LoanDuration> items, @Nullable LoanDurationListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public LoanDurationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_loan_duration, parent, false);
        return new LoanDurationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LoanDurationViewHolder holder, int position) {
        holder.onBind(position, getItems(), getListener());
    }

    public LoanDuration getSelectedLoanDuration() {
        return get(selectedLoanDurationPosition);
    }

    private int getStartPosition(double loanAmount) {
        int position = 0;
        for (LoanDuration loanDuration : getItems()) {
            if (loanAmount < loanDuration.getMinAmount())
                break;
            position++;
        }
        return position - 1;
    }

    public void setLoanAmount(double loanAmount) {

        /* Below code is for optimization. It decides the minimum number of items to be notified.
           It would be easier to notify all the items, but it will be in-efficient.
        */
        int startPosition = getStartPosition(loanAmount);

        this.loanAmount = loanAmount;

        if (startPosition != -1)
            notifyItemRangeChanged(startPosition, getItemCount() - startPosition);
    }

    public interface LoanDurationListener extends BaseRecyclerViewListener {

        void onClickLoanDuration(@NonNull LoanDuration tenureDetail);
    }

    class LoanDurationViewHolder extends BaseViewHolder<LoanDuration, LoanDurationListener> {

        @BindView(R.id.cardViewContainer)
        private TezCardView cardViewContainer;

        @BindView(R.id.tvTitle)
        private TezTextView tvTitle;


        LoanDurationViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(int position, List<LoanDuration> items, @Nullable LoanDurationListener listener) {
            LoanDuration item = items.get(position);
            tvTitle.setText(item.getTitle());

            cardViewContainer.setOnClickListener(v -> {
                if (selectedLoanDurationPosition != position) {
                    notifyItemChanged(selectedLoanDurationPosition);
                    notifyItemChanged(position);
                    selectedLoanDurationPosition = position;
                }
            });

            if (position + 1 < getItemCount()) {
                LoanDuration nextItem = items.get(position + 1);
                if (loanAmount < nextItem.getMinAmount() && selectedLoanDurationPosition > position)
                    selectedLoanDurationPosition = position;
            }

            if (listener != null && selectedLoanDurationPosition == position)
                listener.onClickLoanDuration(item);

            boolean isVisibleContainer = loanAmount >= item.getMinAmount();

            tvTitle.setVisibility(isVisibleContainer ? View.VISIBLE : View.GONE);
            itemView.setVisibility(isVisibleContainer ? View.VISIBLE : View.GONE);

            if (isVisibleContainer) {
                if (selectedLoanDurationPosition == position) {
                    cardViewContainer.setCardBackgroundColor(Utility.getColorFromResource(R.color.textViewTextColorGreen));
                    tvTitle.setTextColor(Utility.getColorFromResource(R.color.colorWhite));
                } else {
                    cardViewContainer.setCardBackgroundColor(Utility.getColorFromResource(R.color.screen_background));
                    tvTitle.setTextColor(Utility.getColorFromResource(R.color.editTextTextColorGrey));
                }
            }
        }
    }
}
