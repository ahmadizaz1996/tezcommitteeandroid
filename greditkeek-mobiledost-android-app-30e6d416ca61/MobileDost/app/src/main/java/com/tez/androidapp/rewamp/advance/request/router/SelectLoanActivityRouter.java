package com.tez.androidapp.rewamp.advance.request.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.request.view.SelectLoanActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class SelectLoanActivityRouter extends BaseActivityRouter {

    public static SelectLoanActivityRouter createInstance() {
        return new SelectLoanActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, SelectLoanActivity.class);
    }
}
