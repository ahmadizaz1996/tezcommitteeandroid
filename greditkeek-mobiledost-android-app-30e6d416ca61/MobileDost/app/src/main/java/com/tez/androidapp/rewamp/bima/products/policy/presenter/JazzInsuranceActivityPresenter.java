package com.tez.androidapp.rewamp.bima.products.policy.presenter;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.InitiateRepaymentRequest;
import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.rewamp.bima.products.policy.interactor.IJazzInsuranceActivityInteractor;
import com.tez.androidapp.rewamp.bima.products.policy.interactor.JazzInsuranceActivityInteractor;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.response.PolicyDetails;
import com.tez.androidapp.rewamp.bima.products.policy.view.IEasyPaisaInsurancePaymentActivityView;
import com.tez.androidapp.rewamp.bima.products.policy.view.IJazzInsurancePaymentActivityView;

public class JazzInsuranceActivityPresenter extends EasyPaisaInsurancePaymentActivityPresenter
        implements IJazzInsuranceActivityPresenter, IJazzInsuranceActivityInteractorOutput {

    private final IJazzInsuranceActivityInteractor iJazzInsuranceActivityInteractor;

    public JazzInsuranceActivityPresenter(IJazzInsurancePaymentActivityView iJazzInsurancePaymentActivityView) {
        super(iJazzInsurancePaymentActivityView);
        this.iJazzInsuranceActivityInteractor = new JazzInsuranceActivityInteractor(this);
    }


    @Override
    public void initiatePurchasePolicy(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest,
                                       Location location,
                                       String pin){

        iEasyPaisaInsurancePaymentActivityView.setPinViewOtpEnabled(false);
        iEasyPaisaInsurancePaymentActivityView.showTezLoader();
        initiatePurchasePolicyRequest.setPin(pin);
        iJazzInsuranceActivityInteractor.initiatePurchasePolicy(initiatePurchasePolicyRequest, pin, location);
    }


    @Override
    public void initiatePurchasePolicySuccess(PolicyDetails policyDetails, @NonNull String pin, @Nullable Location location) {
        this.policyDetails = policyDetails;
        purchasePolicy(pin, location);
    }
}
