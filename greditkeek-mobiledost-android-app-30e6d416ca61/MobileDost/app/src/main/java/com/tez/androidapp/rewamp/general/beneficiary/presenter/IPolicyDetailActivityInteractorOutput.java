package com.tez.androidapp.rewamp.general.beneficiary.presenter;

import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetActiveInsurancePolicyDetailsCallback;

public interface IPolicyDetailActivityInteractorOutput extends GetActiveInsurancePolicyDetailsCallback {
}
