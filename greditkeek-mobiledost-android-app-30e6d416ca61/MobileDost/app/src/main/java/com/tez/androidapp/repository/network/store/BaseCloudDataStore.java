package com.tez.androidapp.repository.network.store;

import com.crashlytics.android.Crashlytics;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.readystatesoftware.chuck.ChuckInterceptor;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.managers.NativeKeysManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.services.TLSSocketFactory;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Data store to access data from network. Initializes the {@link Retrofit} and {@link OkHttpClient} instance to make calls to the server.
 * <p>
 * Created  on 12/14/2016.
 */

public class BaseCloudDataStore {

    Retrofit.Builder tezAPIBuilder;
    OkHttpClient.Builder okHttpClientBuilder;
    private CompositeDisposable compositeDisposable;

    public BaseCloudDataStore() {
        CertificatePinner certificatePinner = new CertificatePinner.Builder()
                .add(NativeKeysManager.getInstance().getTezSSLpattern2(), NativeKeysManager.getInstance().getTezSSLpin1()+
                        NativeKeysManager.getInstance().getTezSSLpin2())
                .add(NativeKeysManager.getInstance().getTezSSLpattern1(), NativeKeysManager.getInstance().getTezSSLpinA()+
                        NativeKeysManager.getInstance().getTezSSLpinB())
                .build();

        ChuckInterceptor chuckInterceptor = new ChuckInterceptor(MobileDostApplication.getInstance());
        chuckInterceptor.showNotification(!(MobileDostApplication.getInstance().isProductionBuildChangeable() && Constants.IS_EXTERNAL_BUILD));
        compositeDisposable = new CompositeDisposable();
        okHttpClientBuilder = new OkHttpClient.Builder();
        okHttpClientBuilder = okHttpClientBuilder.
                addInterceptor(chuckInterceptor);
        okHttpClientBuilder = okHttpClientBuilder.followRedirects(true).followSslRedirects(true);
        int timeOutSeconds = 60;
        okHttpClientBuilder.connectTimeout(timeOutSeconds, TimeUnit.SECONDS);
        okHttpClientBuilder.readTimeout(timeOutSeconds, TimeUnit.SECONDS);
        okHttpClientBuilder.writeTimeout(timeOutSeconds, TimeUnit.SECONDS);
        okHttpClientBuilder = addMoreFeaturesToClient();
        try {
            TLSSocketFactory tlsSocketFactory = new TLSSocketFactory();
            okHttpClientBuilder.sslSocketFactory(tlsSocketFactory, tlsSocketFactory.systemDefaultTrustManager());
        } catch (KeyManagementException | NoSuchAlgorithmException e) {

            Crashlytics.logException(e);
        }
        OkHttpClient client = okHttpClientBuilder.certificatePinner(certificatePinner).build();

        tezAPIBuilder = new Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).client(client);

    }

    protected String getBaseUrl(){
        return MDPreferenceManager.getBaseURL();
    }

    public void addDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    protected OkHttpClient.Builder addMoreFeaturesToClient() {
        return okHttpClientBuilder;
    }
}
