package com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created  on 9/17/2017.
 */

public interface UserReActivateAccountResendOTPCallback {

    void onUserAccountReActivateResendOTPSuccess(BaseResponse baseResponse);

    void onUserAccountReActivateResendOTPFailure(BaseResponse baseResponse);
}
