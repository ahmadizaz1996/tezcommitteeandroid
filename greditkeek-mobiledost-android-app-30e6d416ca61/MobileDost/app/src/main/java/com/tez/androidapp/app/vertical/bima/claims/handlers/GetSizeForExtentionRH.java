package com.tez.androidapp.app.vertical.bima.claims.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.bima.claims.callback.GetSizeForExtentionCallback;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.GetSizeForExtentionResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by FARHAN DHANANI on 7/20/2018.
 */
public class GetSizeForExtentionRH extends BaseRH<GetSizeForExtentionResponse> {
    private final GetSizeForExtentionCallback getSizeForExtentionCallback;
    public GetSizeForExtentionRH(BaseCloudDataStore baseCloudDataStore,
                                 GetSizeForExtentionCallback getSizeForExtentionCallback) {
        super(baseCloudDataStore);
        this.getSizeForExtentionCallback = getSizeForExtentionCallback;
    }

    @Override
    protected void onSuccess(Result<GetSizeForExtentionResponse> value) {
        GetSizeForExtentionResponse getSizeForExtentionResponse = value.response().body();
        if (getSizeForExtentionResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            if (this.getSizeForExtentionCallback != null)
                this.getSizeForExtentionCallback.onGetSizeForExtentionSuccess(getSizeForExtentionResponse.getNameValueList());
        } else
            onFailure(getSizeForExtentionResponse.getStatusCode(), getSizeForExtentionResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.getSizeForExtentionCallback.onGetSizeForExtentionFailure(errorCode, message);
    }
}
