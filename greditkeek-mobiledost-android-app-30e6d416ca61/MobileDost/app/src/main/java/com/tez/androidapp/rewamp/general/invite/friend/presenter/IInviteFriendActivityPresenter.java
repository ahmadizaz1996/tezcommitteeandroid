package com.tez.androidapp.rewamp.general.invite.friend.presenter;

public interface IInviteFriendActivityPresenter {
    void getRefferalCode();

    void onClickAppCompatButtonInviteFriend(String refCode);
}
