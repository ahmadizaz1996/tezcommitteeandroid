package com.tez.androidapp.rewamp.general.invite.friend.presenter;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.general.invite.friend.interactor.IInviteFriendActivityInteractor;
import com.tez.androidapp.rewamp.general.invite.friend.interactor.InviteFriendActivityInteractor;
import com.tez.androidapp.rewamp.general.invite.friend.view.IInviteFriendActivityView;

public class InviteFriendActivityPresenter implements
        IInviteFriendActivityPresenter, IInviteFriendActivityInteractorOutput {

    private final IInviteFriendActivityView iInviteFriendActivityView;
    private final IInviteFriendActivityInteractor iInviteFriendActivityInteractor;

    public InviteFriendActivityPresenter(IInviteFriendActivityView iInviteFriendActivityView) {
        iInviteFriendActivityInteractor = new InviteFriendActivityInteractor(this);
        this.iInviteFriendActivityView = iInviteFriendActivityView;
    }

    @Override
    public void getRefferalCode() {
        this.iInviteFriendActivityInteractor.getReferralCode();
    }

    @Override
    public void onGetReferralCodeSuccess(String referralCode) {
        this.iInviteFriendActivityView.setOnClickListnerToBtOnClickListner(
                view -> onClickAppCompatButtonInviteFriend(referralCode));
        this.iInviteFriendActivityView.setVisibillityForTezLinearLayoutShimmer(View.GONE);
        this.iInviteFriendActivityView.setVisibillityForBtContinue(View.VISIBLE);
        this.iInviteFriendActivityView.setVisibillityForIvLogo(View.VISIBLE);
        this.iInviteFriendActivityView.setVisibillityForTvDescription(View.VISIBLE);
        this.iInviteFriendActivityView.setVisibillityForTvHeading(View.VISIBLE);
    }

    @Override
    public void onGetReferralCodeFailure(int errorCode, String message) {
        this.iInviteFriendActivityView.showError(errorCode,
                (d, v) -> this.iInviteFriendActivityView.finishActivity());
    }

    @Override
    public void onClickAppCompatButtonInviteFriend(String refCode) {
        if (!Utility.isEmpty(refCode)) {
            share(refCode);
        } else {
            this.iInviteFriendActivityView.showInformativeMessage(R.string.invalid_ref,
                    (d, v) -> iInviteFriendActivityView.finishActivity());
        }
    }


    private void share(String refCode) {
        this.iInviteFriendActivityView.showTezLoader();
        shortenLink(
                createDynamicUri(
                        createShareUri(refCode)), refCode);
    }


    private Uri createShareUri(String refCode) {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme(Utility.getStringFromResource(R.string.config_scheme))
                .authority(Utility.getStringFromResource(R.string.chrome_url))
                .appendQueryParameter(Constants.REF_CODE, refCode);
        return builder.build();
    }

    private void shortenLink(Uri uri, String refCode) {
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(uri)
                .buildShortDynamicLink()
                .addOnFailureListener(task->{
                    iInviteFriendActivityView.dismissTezLoader();
                    iInviteFriendActivityView.showInformativeMessage(R.string.default_response);
                })
                .addOnCompleteListener(task -> {
                    try {
                        ShortDynamicLink shortDynamicLink = task.getResult();
                        if (task.isSuccessful() && shortDynamicLink != null) {
                            Uri shortLink = shortDynamicLink.getShortLink();
                            String msg = Utility.getStringFromResource(R.string.string_hello)
                                    + " "
                                    + Utility.getStringFromResource(R.string.string_app_invite_desc)
                                    + " Invitation link: "
                                    + " "
                                    + shortLink;
                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
                            sendIntent.setType("text/plain");
                            this.iInviteFriendActivityView.startActivityFromIntent(sendIntent);
                        } else {
                            Crashlytics.logException(task.getException());
                            Crashlytics.log("Deep linking failed to send ref Code");
                            this.iInviteFriendActivityView.showInformativeMessage(R.string.retry,
                                    (d, v) -> this.iInviteFriendActivityView.finishActivity());
                        }
                        this.iInviteFriendActivityView.setBtContinueEnabled(true);
                    } catch (Exception e) {
                        this.iInviteFriendActivityView.showInformativeMessage(R.string.retry,
                                (d, v) -> this.iInviteFriendActivityView.finishActivity());
                    }
                    this.iInviteFriendActivityView.dismissTezLoader();
                });
    }

    private Uri createDynamicUri(Uri uri) {
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance()
                .createDynamicLink()
                .setDomainUriPrefix(Utility.getStringFromResource(R.string.config_host))
                .setLink(uri)
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder()
                        .build())
                .buildDynamicLink();
        return dynamicLink.getUri();
    }
}
