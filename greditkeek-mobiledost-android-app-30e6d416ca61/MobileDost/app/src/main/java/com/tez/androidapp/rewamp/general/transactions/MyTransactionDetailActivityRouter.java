package com.tez.androidapp.rewamp.general.transactions;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.transactions.models.network.Transaction;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

/**
 * Created by VINOD KUMAR on 8/28/2019.
 */
public class MyTransactionDetailActivityRouter extends BaseActivityRouter {

    public static final String TRANSACTION = "TRANSACTION";


    public static MyTransactionDetailActivityRouter createInstance() {
        return new MyTransactionDetailActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, @NonNull Transaction transaction) {
        Intent intent = createIntent(from);
        intent.putExtra(TRANSACTION, transaction);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, MyTransactionDetailActivity.class);
    }
}
