package com.tez.androidapp.rewamp.general.wallet.interactor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks.AddWalletCallback;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks.AddWalletResendOTPCallback;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks.AddWalletVerifyOTPCallback;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request.AddWalletRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.general.wallet.presenter.IVerifyWalletOtpActivityInteractorOutput;

public class VerifyWalletOtpActivityInteractor implements IVerifyWalletOtpActivityInteractor {

    private final IVerifyWalletOtpActivityInteractorOutput iVerifyWalletOtpActivityInteractorOutput;

    public VerifyWalletOtpActivityInteractor(IVerifyWalletOtpActivityInteractorOutput iVerifyWalletOtpActivityInteractorOutput) {
        this.iVerifyWalletOtpActivityInteractorOutput = iVerifyWalletOtpActivityInteractorOutput;
    }

    @Override
    public void addWallet(@NonNull AddWalletRequest addWalletRequest) {
        UserAuthCloudDataStore.getInstance().addWallet(addWalletRequest, new AddWalletCallback() {
            @Override
            public void onAddWalletSuccess(BaseResponse baseResponse) {
                iVerifyWalletOtpActivityInteractorOutput.onAddWalletSuccess(addWalletRequest.getWalletServiceProviderId(), addWalletRequest.getMobileAccountNumber());
            }

            @Override
            public void onAddWalletFailure(int errorCode, @Nullable String message) {
                if (Utility.isUnauthorized(errorCode))
                    addWallet(addWalletRequest);
                else
                    iVerifyWalletOtpActivityInteractorOutput.onAddWalletFailure(addWalletRequest.getWalletServiceProviderId(), errorCode, message);
            }
        });
    }

    @Override
    public void verifyAddWalletOtp(int serviceProviderId, String mobileAccountNumber, String otp) {
        UserAuthCloudDataStore.getInstance().addWalletVerifyOTP(otp, serviceProviderId, mobileAccountNumber,
                new AddWalletVerifyOTPCallback() {

                    @Override
                    public void onAddWalletVerifyOTPSuccess() {
                        iVerifyWalletOtpActivityInteractorOutput.onAddWalletVerifyOTPSuccess();
                    }

                    @Override
                    public void onAddWalletVerifyOTPFailure(int errorCode, String message) {
                        if (Utility.isUnauthorized(errorCode))
                            verifyAddWalletOtp(serviceProviderId, mobileAccountNumber, otp);
                        else
                            iVerifyWalletOtpActivityInteractorOutput.onAddWalletVerifyOTPFailure(serviceProviderId, errorCode);
                    }
                });
    }

    @Override
    public void resendAddWalletOtp(int serviceProviderId, String mobileAccountNumber) {
        UserAuthCloudDataStore.getInstance().addWalletResendOTP(
                mobileAccountNumber,
                serviceProviderId,
                new AddWalletResendOTPCallback() {
                    @Override
                    public void onAddWalletResendOTPSuccess() {
                        iVerifyWalletOtpActivityInteractorOutput.onAddWalletResendOTPSuccess();
                    }

                    @Override
                    public void onAddWalletResendOTPFailure(int errorCode, String message) {
                        if (Utility.isUnauthorized(errorCode)) {
                            resendAddWalletOtp(serviceProviderId, mobileAccountNumber);
                        } else {
                            iVerifyWalletOtpActivityInteractorOutput.onAddWalletResendOTPFailure(errorCode, message);
                        }

                    }
                });
    }
}
