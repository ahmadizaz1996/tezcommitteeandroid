package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.google.android.material.textfield.TextInputLayout;
import com.tez.androidapp.R;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.fragment.util.optional.Optional;
import net.tez.fragment.util.optional.TextUtil;

/**
 * Created by FARHAN DHANANI on 6/7/2019.
 */
public class TezTextInputLayout extends TextInputLayout implements IBaseWidget {


    public TezTextInputLayout(Context context) {
        super(context);
        init(context);
    }

    public TezTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TezTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        setFontStyleForLabels(context);
    }

    private void setFontStyleForLabels(Context context){
        try {
            Typeface font= getFont(context);
            setTypeface(font);
        } catch (Exception ignore){

        }
    }

    private Typeface getFont(Context context){
        try {
            return ResourcesCompat.getFont(context, R.font.barlow_regular);
        } catch (Exception ex){
            return null;
        }
    }

    @Override
    public void setError(@Nullable CharSequence errorText) {
        Optional.ifPresent(errorText, (et)->{
            super.setError(getSpannableString(et));
        }, ()-> {
            super.setError(errorText);
            super.setErrorEnabled(false);
        });
        Optional.isInstanceOf(getEditText(), TezEditTextView.class,
                (tezEditText) -> tezEditText.changeDrawableColorOnError(errorText));
    }

    private @Nullable SpannableString getSpannableString(@NonNull CharSequence text){
        try {
            final SpannableString ss = new SpannableString(text);
            ss.setSpan(new FontSpan(getFont(getContext())), 0, ss.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return ss;
        } catch (Exception e){
            return null;
        }
    }

    @NonNull
    @Override
    public String getLabel() {
        if (getEditText() != null && TextUtil.isNotEmpty(getEditText().getText()))
            return getEditText().getText().toString();
        return "";
    }

    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        super.setOnClickListener(doubleTapSafeOnClickListener);
    }

    private static final class FontSpan extends MetricAffectingSpan {

        private final Typeface mNewFont;

        private FontSpan(Typeface newFont) {
            mNewFont = newFont;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setTypeface(mNewFont);
        }

        @Override
        public void updateMeasureState(TextPaint paint) {
            paint.setTypeface(mNewFont);
        }

    }
}
