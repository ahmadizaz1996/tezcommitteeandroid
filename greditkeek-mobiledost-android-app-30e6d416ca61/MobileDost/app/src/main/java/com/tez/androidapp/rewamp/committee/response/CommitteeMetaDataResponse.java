package com.tez.androidapp.rewamp.committee.response;

import com.google.gson.annotations.SerializedName;
import com.tez.androidapp.app.base.response.BaseResponse;

import java.io.Serializable;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class CommitteeMetaDataResponse extends BaseResponse implements Serializable {

    int id;
    int minTenure;
    int maxTenure;
    int minAmount;
    int maxAmount;
    int minMembers;
    int maxMembers;
    boolean status;
    boolean hasCommittee;

    public boolean isHasCommittee() {
        return hasCommittee;
    }

    public void setHasCommittee(boolean hasCommittee) {
        this.hasCommittee = hasCommittee;
    }

    public FrequencyMap frequencyMap;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMinTenure() {
        return minTenure;
    }

    public void setMinTenure(int minTenure) {
        this.minTenure = minTenure;
    }

    public int getMaxTenure() {
        return maxTenure;
    }

    public void setMaxTenure(int maxTenure) {
        this.maxTenure = maxTenure;
    }

    public int getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(int minAmount) {
        this.minAmount = minAmount;
    }

    public int getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(int maxAmount) {
        this.maxAmount = maxAmount;
    }

    public int getMinMembers() {
        return minMembers;
    }

    public void setMinMembers(int minMembers) {
        this.minMembers = minMembers;
    }

    public int getMaxMembers() {
        return maxMembers;
    }

    public void setMaxMembers(int maxMembers) {
        this.maxMembers = maxMembers;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public FrequencyMap getFrequencyMap() {
        return frequencyMap;
    }

    public void setFrequencyMap(FrequencyMap frequencyMap) {
        this.frequencyMap = frequencyMap;
    }

    public static class FrequencyMap implements Serializable {
        @SerializedName("1")
        public String one;

        @SerializedName("2")
        public String two;

        public String getOne() {
            return one;
        }

        public void setOne(String one) {
            this.one = one;
        }

        public String getTwo() {
            return two;
        }

        public void setTwo(String two) {
            this.two = two;
        }
    }
}


