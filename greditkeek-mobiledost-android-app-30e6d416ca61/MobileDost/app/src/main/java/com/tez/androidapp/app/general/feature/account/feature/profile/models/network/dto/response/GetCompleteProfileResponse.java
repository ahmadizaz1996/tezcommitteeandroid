package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.User;

public class GetCompleteProfileResponse extends BaseResponse {

    private User userProfile;

    public User getUserProfile() {
        return userProfile;
    }
}
