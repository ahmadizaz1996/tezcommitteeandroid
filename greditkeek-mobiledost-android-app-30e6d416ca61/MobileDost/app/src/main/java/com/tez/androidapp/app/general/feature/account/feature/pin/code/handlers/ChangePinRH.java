package com.tez.androidapp.app.general.feature.account.feature.pin.code.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.ChangePinCallback;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by Rehman Murad Ali on 8/25/2017.
 */

public class ChangePinRH extends BaseRH<BaseResponse> {

    ChangePinCallback changePinCallback;

    public ChangePinRH(BaseCloudDataStore baseCloudDataStore, ChangePinCallback changePinCallback) {
        super(baseCloudDataStore);
        this.changePinCallback = changePinCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (baseResponse != null) {
            if (baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
                if (changePinCallback != null)
                    changePinCallback.onChangePinSuccess(baseResponse);
            } else {
                onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
            }
        } else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        changePinCallback.onChangePinFailure(errorCode, message);
    }


}
