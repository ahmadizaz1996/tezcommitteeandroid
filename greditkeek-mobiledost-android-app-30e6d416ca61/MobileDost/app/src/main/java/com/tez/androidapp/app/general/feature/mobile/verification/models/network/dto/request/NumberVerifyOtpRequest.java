package com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by FARHAN DHANANI on 2/26/2019.
 */
public class NumberVerifyOtpRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/user/otp/verify";
    private String principalName;
    private String mobileNumber;
    private String otp;

    public NumberVerifyOtpRequest(String mobileNumber, String principalName, String otp){
        this.mobileNumber = mobileNumber;
        this.otp = otp;
        this.principalName = principalName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getOtp() {
        return otp;
    }


}
