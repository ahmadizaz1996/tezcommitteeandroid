package com.tez.androidapp.commons.managers;

import androidx.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.BuildConfig;
import com.tez.androidapp.commons.models.network.AccessToken;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.pref.GenericObjectParser;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.CommittteeLoginResponse;
import com.tez.androidapp.rewamp.dashboard.entity.DashboardCards;

import net.tez.storage.library.core.Storage;

/**
 * Created by FARHAN DHANANI on 1/23/2019.
 */

public class MDPreferenceManager {

    public static final String PREFS_NAME = "mobileDostPrefs";
    private static final String PREF_BLUETOOTH_DATA = "0";
    private static final String PREF_WIFI_DATA = "1";
    private static final String PREF_LAST_SENT_WIFI_TIME = "2";
    private static final String PREF_LAST_SENT_BLUETOOTH_TIME = "3";
    private static final String PREF_LAST_LOCATION_LAT = "4";
    private static final String PREF_LAST_LOCATION_LONG = "5";
    private static final String PREF_PRINCIPAL_NAME = "17";
    private static final String PREF_REFERRAL_CODE = "20";
    private static final String PREF_BASE_URL = "25";
    private static final String PREF_USER = "26";
    private static final String PREF_USER_SIGNED_UP = "32";
    private static final String PREF_ACCESS_TOKEN = "33";
    private static final String PREF_LOAN_DETAILS_SERVER = "35";
    private static final String PREF_FIREBASE_KEY = "38";
    private static final String PREF_LAST_UPLOAD_ELAPSED_TIME = "42";
    private static final String PREF_DISBURSEMENT_RECEIPT_VIEWED = "45";
    private static final String PREF_PREVIOUS_PRINCIPAL_NAME = "47";
    private static final String PREF_FLASH_CALL_FAILURE_COUNT = "48";
    private static final String PREF_IS_DATA_READY = "51";
    private static final String PREF_OTP_FAILURE_COUNT = "56";
    private static final String PREF_PROFILE_PICTURE_LAST_MODIFIED_TIME = "57";
    private static final String PREF_DEVICE_KEY_SENT_TO_SERVER = "58";
    private static final String PREF_IS_USER_LANG_RECORDED = "60";
    private static final String PREF_DASHBOARD_CARDS = "61";
    private static final String PREF_OCR_FAILED = "62";
    private static final String PREF_COMMITTEE_CREATION_DATA = "63";

    private MDPreferenceManager() {
    }

    public static void clearAllPreferences() {
        String deviceKey = MDPreferenceManager.getFirebaseKey();
        Storage.clear();
        MDPreferenceManager.setFirebaseKey(deviceKey);
    }

    public static synchronized String getBlueToothData() {
        try {
            return Storage.getString(PREF_BLUETOOTH_DATA, "{}");
        } catch (Exception e) {
            Crashlytics.logException(e);
            Crashlytics.log("crash on getBlueToothData MDPreference Manager");
            return null;
        }
    }

    public static synchronized void setBlueToothData(String bluetoothData) {
        Storage.put(PREF_BLUETOOTH_DATA, bluetoothData);
    }

    public static synchronized String getWiFiData() {
        try {
            return Storage.getString(PREF_WIFI_DATA, "{}");
        } catch (Exception e) {
            Crashlytics.logException(e);
            Crashlytics.log("crash on getWiFiData MDPreferenceManger");
            return null;
        }
    }

    public static synchronized void setWiFiData(String wiFiData) {
        Storage.put(PREF_WIFI_DATA, wiFiData);
    }

    public static synchronized Long getLastSentWiFiTime() {
        return Storage.getLong(PREF_LAST_SENT_WIFI_TIME, -11);
    }

    public static synchronized void setLastSentWiFiTime(long wiFiTime) {
        Storage.put(PREF_LAST_SENT_WIFI_TIME, wiFiTime);
    }

    public static synchronized Long getLastSentBluetoothTime() {
        return Storage.getLong(PREF_LAST_SENT_BLUETOOTH_TIME, -11);
    }

    public static synchronized void setLastSentBluetoothTime(long wiFiTime) {
        Storage.put(PREF_LAST_SENT_BLUETOOTH_TIME, wiFiTime);
    }

    public static synchronized void setLastLocation(double latitude, double longitude) {
        Storage.put(PREF_LAST_LOCATION_LAT, latitude);
        Storage.put(PREF_LAST_LOCATION_LONG, longitude);
    }

    public static synchronized String[] getLastLocation() {
        return new String[]{
                Storage.getString(PREF_LAST_LOCATION_LAT, ""),
                Storage.getString(PREF_LAST_LOCATION_LONG, "")
        };
    }


    public static synchronized String getPrincipalName() {
        return Storage.getString(PREF_PRINCIPAL_NAME, null);
    }

    public static synchronized void setPrincipalName(String principalName) {
        setSharedPreferenceValues(principalName);
        Storage.put(PREF_PRINCIPAL_NAME, principalName);
    }

    public static synchronized String getReferralCode() {
        return Storage.getString(PREF_REFERRAL_CODE, "");
    }

    public static synchronized void setReferralCode(String referralCode) {
        Storage.put(PREF_REFERRAL_CODE, referralCode);
    }


    public static synchronized String getBaseURL() {
        return Storage.getString(PREF_BASE_URL, Utility.getBaseURL(BuildConfig.FLAVOR));
    }

    public static synchronized void setBaseURL(String baseURL) {
        Storage.put(PREF_BASE_URL, baseURL);
    }

    public static synchronized User getUser() {
        return Storage.get(PREF_USER, User.class, new GenericObjectParser());
    }

    public static synchronized void setUser(User user) {
        Storage.put(PREF_USER, user.getUserWithMinProps());
        setPrincipalName(user.getPrincipalName());
        Utility.setUserForCrashlytics();
        Utility.setUserForFacebook();
        Utility.setUserForFirebase();
    }

    public static synchronized void setUser(CommittteeLoginResponse.UserDetails user) {
        Storage.put(PREF_USER, user.getUserWithMinProps());
        setPrincipalName(user.getPrincipalName());
        Utility.setUserForCrashlytics();
        Utility.setUserForFacebook();
        Utility.setUserForFirebase();
    }

    public static synchronized void setCommitteeCreationData(CommitteeCreateResponse committeeCreationData) {
        Storage.put(PREF_COMMITTEE_CREATION_DATA, CommitteeCreateResponse.class);
    }

    public static synchronized CommitteeCreateResponse getCommitteeCreationData() {
        return Storage.get(PREF_COMMITTEE_CREATION_DATA, CommitteeCreateResponse.class, new GenericObjectParser());
    }

    public static synchronized void removeUser() {
        Storage.remove(PREF_USER);
    }

    public static synchronized void setUserSignedUp(boolean userSignedUp) {
        Storage.put(PREF_USER_SIGNED_UP, userSignedUp);
    }

    public static synchronized Boolean isUserSignedUp() {
        return Storage.getBoolean(PREF_USER_SIGNED_UP, false);
    }

    public static synchronized AccessToken getAccessToken() {
        return Storage.get(PREF_ACCESS_TOKEN, AccessToken.class);
    }

    public static synchronized void setAccessToken(AccessToken accessToken) {
        Storage.put(PREF_ACCESS_TOKEN, accessToken);
    }

    public static synchronized void setAccessToken(CommittteeLoginResponse.Token accessToken) {
        Storage.put(PREF_ACCESS_TOKEN, accessToken);
    }

    public static synchronized void removeAccessToken() {
        Storage.remove(PREF_ACCESS_TOKEN);
    }

    public static synchronized LoanDetails getLoanDetails() {
        return Storage.get(PREF_LOAN_DETAILS_SERVER, LoanDetails.class);
    }

    public static synchronized void setLoanDetails(LoanDetails loanDetails) {
        Storage.put(PREF_LOAN_DETAILS_SERVER, loanDetails);
    }


    public static synchronized String getFirebaseKey() {
        return Storage.getString(PREF_FIREBASE_KEY, null);
    }

    public static synchronized void setFirebaseKey(String firebaseKey) {
        Storage.put(PREF_FIREBASE_KEY, firebaseKey);
    }

    public static synchronized long getLastUploadElapsedTime() {
        //To handle update Scenario
        try {
            return Storage.getLong(PREF_LAST_UPLOAD_ELAPSED_TIME, -Constants.TIME_TO_LIFT);
        } catch (Exception e) {
            setLastUploadElapsedTime(-Constants.TIME_TO_LIFT);
            return -Constants.TIME_TO_LIFT;
        }
    }

    public static synchronized void setLastUploadElapsedTime(long elapsedTime) {
        Storage.put(PREF_LAST_UPLOAD_ELAPSED_TIME, elapsedTime);
    }

    public static synchronized boolean getDisbursementReceiptViewed() {
        return Storage.getBoolean(PREF_DISBURSEMENT_RECEIPT_VIEWED, false);
    }

    public static synchronized void setDisbursementReceiptViewed(boolean disbursementReceiptViewed) {
        Storage.put(PREF_DISBURSEMENT_RECEIPT_VIEWED, disbursementReceiptViewed);
    }

    private static synchronized String getPreviousUserPrincipalName() {
        return Storage.getString(PREF_PREVIOUS_PRINCIPAL_NAME, null);
    }

    private static synchronized void setPreviousUserPrincipalName(String principalName) {
        Storage.put(PREF_PREVIOUS_PRINCIPAL_NAME, principalName);
    }

    private static void setSharedPreferenceValues(String principleName) {
        if (principleName == null || !principleName.equalsIgnoreCase(getPreviousUserPrincipalName())) {
            setDisbursementReceiptViewed(false);
        }
        setPreviousUserPrincipalName(principleName);
    }

    public static synchronized int getFlashCallVerificationFailureCount() {
        return Storage.getInt(PREF_FLASH_CALL_FAILURE_COUNT, 0);
    }

    public static synchronized void setFlashCallVerificationFailureCount(int flashCallVerificationFailureCount) {
        Storage.put(PREF_FLASH_CALL_FAILURE_COUNT, flashCallVerificationFailureCount);
    }

    public static synchronized int getOtpVerificationFailureCount() {
        return Storage.getInt(PREF_OTP_FAILURE_COUNT, 0);
    }

    public static synchronized void setOtpVerificationFailureCount(int otpVerificationFailureCount) {
        Storage.put(PREF_OTP_FAILURE_COUNT, otpVerificationFailureCount);
    }

    public static synchronized void setUploadDeviceDataReady(boolean isReady) {
        Storage.put(PREF_IS_DATA_READY, isReady);
    }

    public static synchronized boolean getIsUploadDeviceDataReady() {
        return Storage.getBoolean(PREF_IS_DATA_READY, false);
    }

    public static long getProfilePictureLastModifiedTime() {
        long value = Storage.getLong(PREF_PROFILE_PICTURE_LAST_MODIFIED_TIME);
        if (value == 0) {
            long currentTime = System.currentTimeMillis();
            setProfilePictureLastModifiedTime(currentTime);
            return currentTime;
        }
        return value;
    }

    public static void setProfilePictureLastModifiedTime(long lastModifiedTime) {
        Storage.put(PREF_PROFILE_PICTURE_LAST_MODIFIED_TIME, lastModifiedTime);
    }

    public static synchronized boolean isDeviceKeySentToServer() {
        return Storage.getBoolean(PREF_DEVICE_KEY_SENT_TO_SERVER, false);
    }

    public static synchronized void setDeviceKeySentToServer(boolean sent) {
        Storage.put(PREF_DEVICE_KEY_SENT_TO_SERVER, sent);
    }

    public static synchronized void setPrefIsUserLangRecorded(boolean langRecorded) {
        Storage.put(PREF_IS_USER_LANG_RECORDED, langRecorded);
    }

    public static synchronized boolean getPrefIsUserLangRecorder() {
        return Storage.getBoolean(PREF_IS_USER_LANG_RECORDED, false);
    }

    public static synchronized int getOCRFailedCount() {
        return Storage.getInt(PREF_OCR_FAILED, 0);
    }

    public static synchronized void setOCRFailedCount(int count) {
        Storage.put(PREF_OCR_FAILED, count);
    }

    public static synchronized DashboardCards getDashboardCards() {
        return Storage.get(PREF_DASHBOARD_CARDS, DashboardCards.class, new GenericObjectParser());
    }

    public static synchronized void setDashboardCards(@NonNull DashboardCards cards) {
        Storage.put(PREF_DASHBOARD_CARDS, cards);
    }


}
