package com.tez.androidapp.app.general.feature.mobile.verification.callback;

/**
 * Created by FARHAN DHANANI on 2/26/2019.
 */
public interface NumberVerifyOtpCallback {

    void onNumberVerifyOtpSuccess();

    void onNumberVerifyOtpFailure(int errorCode, String message);
}
