package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.TezDrawableHelper;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.fragment.util.optional.TextUtil;

/**
 * Created by FARHAN DHANANI on 1/9/2019.
 */
public class TezTextView extends AppCompatTextView implements IBaseWidget {

    public TezTextView(Context context) {
        super(context);
    }

    public TezTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public TezTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    public String getValueText() {
        CharSequence charSequence = getText();
        if (charSequence != null)
            return charSequence.toString();
        return null;
    }

    public void setTextViewDrawableColor(@ColorRes int color) {
        for (Drawable drawable : getCompoundDrawables())
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(getContext(), color), PorterDuff.Mode.SRC_IN));
            }
    }

    public void clearTextViewDrawableColor() {
        for (Drawable drawable : getCompoundDrawables())
            if (drawable != null) {
                drawable.clearColorFilter();
            }
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TezTextView);
        try {

            int clickEffect = typedArray.getInt(R.styleable.TezTextView_click_effect, NO_EFFECT);
            attachClickEffect(clickEffect, this);

            Drawable start = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezTextView_drawableStartCompat);
            Drawable end = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezTextView_drawableEndCompat);
            Drawable top = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezTextView_drawableTopCompat);
            Drawable bottom = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezTextView_drawableBottomCompat);
            changeDrawable(this, start, top, end, bottom);

        } finally {
            typedArray.recycle();
        }
    }

    @NonNull
    @Override
    public String getLabel() {
        return TextUtil.isNotEmpty(getText()) ? getText().toString() : "";
    }


    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        super.setOnClickListener(doubleTapSafeOnClickListener);
    }
}
