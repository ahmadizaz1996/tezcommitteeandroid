package com.tez.androidapp.commons.models.network.dto.response;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.response.BaseResponse;

import net.tez.fragment.util.optional.TextUtil;

import java.io.Serializable;

/**
 * Created  on 2/9/2017.
 */

public class UserInfoResponse extends BaseResponse implements Serializable {

    private String userStatus;
    private String principalName;
    private String fullName;

    public UserInfoResponse(String userStatus, String principalName, String fullName) {
        this.userStatus = userStatus;
        this.principalName = principalName;
        this.fullName = fullName;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public String getFullName() {
        return fullName;
    }

    public boolean isUserRegistered() {
        return !TextUtil.equals(userStatus,"UNREGISTERED", true);
    }

    @NonNull
    @Override
    public String toString() {
        return "UserInfoResponse{" +
                "userStatus='" + userStatus + '\'' +
                ", principalName='" + principalName + '\'' +
                ", fullName='" + fullName + '\'' +
                "} " + super.toString();
    }
}
