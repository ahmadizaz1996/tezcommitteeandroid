package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeLeaveResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface CommitteeLeaveListener {

    void onCommitteeLeaveSuccess(CommitteeLeaveResponse committeeLeaveResponse);

    void onCommitteeLeaveFailure(int errorCode, String message);
}
