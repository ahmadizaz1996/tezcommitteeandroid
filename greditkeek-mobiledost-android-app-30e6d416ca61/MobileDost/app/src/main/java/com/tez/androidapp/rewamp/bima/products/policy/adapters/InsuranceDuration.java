package com.tez.androidapp.rewamp.bima.products.policy.adapters;

import androidx.annotation.Nullable;

public class InsuranceDuration {

    private int id;
    private int dayTenure;
    private String title;
    private int premium;
    private int discount;

    public InsuranceDuration(int id,
                             int dayTenure,
                             String title,
                             int premium,
                             int discount) {
        this.id = id;
        this.dayTenure = dayTenure;
        this.premium = premium;
        this.title = title;
        this.discount = discount;
    }


    public int getId() {
        return id;
    }


    public int getDiscount() {
        return discount;
    }

    public int getDayTenure() {
        return dayTenure;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPremium() {
        return premium;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return super.equals(obj) || (obj instanceof InsuranceDuration && ((InsuranceDuration) obj).title.equals(title));
    }
}
