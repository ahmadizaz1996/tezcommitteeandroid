package com.tez.androidapp.rewamp.general.suspend.account.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.app.general.feature.playback.TezCountDownTimer;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.receivers.SMSReceiver;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezPinView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.general.suspend.account.presenter.ISuspendAccountOTPActivityPresenter;
import com.tez.androidapp.rewamp.general.suspend.account.presenter.SuspendAccountOTPActivityPresenter;
import com.tez.androidapp.rewamp.general.suspend.account.router.SuspendAccountFeedbackActivityRouter;

import net.tez.viewbinder.library.core.BindView;

public class SuspendAccountOTPActivity extends PlaybackActivity implements ISuspendAccountOTPActivityView, TezPinView.OnPinEnteredListener {

    private final ISuspendAccountOTPActivityPresenter iSuspendAccountOTPActivityPresenter;

    @BindView(R.id.tvResendCode)
    private TezTextView tvResendCode;

    @BindView(R.id.pinViewOTP)
    private TezPinView pinViewOtp;

    private final SMSReceiver smsReceiver = new SMSReceiver(this::onOtpReceived);

    @BindView(R.id.tvEnterCode)
    private TezTextView tvEnterCode;

    public SuspendAccountOTPActivity() {
        iSuspendAccountOTPActivityPresenter = new SuspendAccountOTPActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suspend_account_otp);
        this.updateToolbar(findViewById(R.id.toolbarSuspendAccount));
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsReceiver.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsReceiver.unregister(this);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void takeFeedbackFromUser() {
        SuspendAccountFeedbackActivityRouter.createInstance().setDependenciesAndRoute(this);
        finish();
    }

    @Override
    public void setPinViewOtpClearText() {
        pinViewOtp.clearText();
    }

    private void init() {
        iSuspendAccountOTPActivityPresenter.suspendAccount();
        initText();
        initOnClickListener();
    }

    private void initText() {
        if (MDPreferenceManager.getPrincipalName() != null) {
            String otpMessage = getString(R.string.please_enter_code_sent_to_number, MDPreferenceManager.getPrincipalName());
            tvEnterCode.setText(otpMessage);
        }
    }

    private void initOnClickListener() {
        tvResendCode.setDoubleTapSafeOnClickListener(view -> iSuspendAccountOTPActivityPresenter.resendCode());
        pinViewOtp.setOnPinEnteredListener(this);
    }

    @Override
    public void startTimer() {
        int totalTime = 45 * 1000;
        int oneSecond = 1000;
        setTvResendCodeEnabled(false);
        new TezCountDownTimer(totalTime, oneSecond) {

            @Override
            public void onTick(long millisUntilFinished) {
                String second = "00:" + (int) millisUntilFinished / 1000;
                tvResendCode.setText(second);
            }

            @Override
            public void onFinish() {
                tvResendCode.setText(R.string.resend_code);
                setTvResendCodeEnabled(true);
            }
        }.start();
    }

    @Override
    public void setTvResendCodeEnabled(boolean enabled) {
        tvResendCode.setEnabled(enabled);
    }

    @Override
    public void setPinViewOtpEnabled(boolean enabled) {
        pinViewOtp.setEnabled(enabled);
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    public void onPinEntered(@NonNull String pin) {
        iSuspendAccountOTPActivityPresenter.suspendAccountVerifyOtp(pin);
    }

    private void onOtpReceived(String otp) {
        this.pinViewOtp.setText(otp);
    }

    @Override
    protected String getScreenName() {
        return "SuspendAccountOTPActivity";
    }
}
