package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.interactor.CommitteeFilterActivityInteractor;
import com.tez.androidapp.rewamp.committee.interactor.CommitteeGroupChatActivityInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeSendMessageRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeSendMessageResponse;
import com.tez.androidapp.rewamp.committee.response.GetGroupChatMessagesResponse;
import com.tez.androidapp.rewamp.committee.view.ICommitteeFilterActivityView;
import com.tez.androidapp.rewamp.committee.view.ICommitteeGroupChatActivityView;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeGroupChatActivityPresenter implements ICommitteeGroupChatActivityPresenter, ICommitteeGroupChatActivityInteractorOutput {

    private final ICommitteeGroupChatActivityView iCommitteeGroupChatActivityView;
    private final CommitteeGroupChatActivityInteractor committeeGroupChatActivityInteractor;

    public CommitteeGroupChatActivityPresenter(ICommitteeGroupChatActivityView iCommitteeGroupChatActivityView) {
        this.iCommitteeGroupChatActivityView = iCommitteeGroupChatActivityView;
        committeeGroupChatActivityInteractor = new CommitteeGroupChatActivityInteractor(this);
    }

    @Override
    public void onGetMessageSuccess(GetGroupChatMessagesResponse getGroupChatMessagesResponse) {
        iCommitteeGroupChatActivityView.hideLoader();
        iCommitteeGroupChatActivityView.onGetMessage(getGroupChatMessagesResponse);
    }

    @Override
    public void onGetMessageFailure(int errorCode, String message) {
        iCommitteeGroupChatActivityView.hideLoader();
        iCommitteeGroupChatActivityView.showError(errorCode,
                (dialog, which) -> iCommitteeGroupChatActivityView.finishActivity());
    }

    @Override
    public void getMessages(String committeeId) {
        iCommitteeGroupChatActivityView.showLoader();
        committeeGroupChatActivityInteractor.getMessages(committeeId);

    }

    @Override
    public void sendMessage(CommitteeSendMessageRequest committeeSendMessageRequest) {
        iCommitteeGroupChatActivityView.showLoader();
        committeeGroupChatActivityInteractor.sendMessage(committeeSendMessageRequest);
    }

    @Override
    public void onSendSuccess(CommitteeSendMessageResponse committeeSendMessageResponse) {
        iCommitteeGroupChatActivityView.hideLoader();
        iCommitteeGroupChatActivityView.onMessageSent(committeeSendMessageResponse);
    }

    @Override
    public void onSendFailure(int errorCode, String message) {
        iCommitteeGroupChatActivityView.hideLoader();
        iCommitteeGroupChatActivityView.showError(errorCode,
                (dialog, which) -> iCommitteeGroupChatActivityView.finishActivity());
    }
}
