package com.tez.androidapp.rewamp.general.profile.interactor;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.AccountLinkCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.AccountLinkRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.GetUserProfileResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.general.profile.presenter.IMyProfileActivityInteractorOutput;

public class MyProfileActivityInteractor implements IMyProfileActivityInteractor {

    private final IMyProfileActivityInteractorOutput iMyProfileActivityInteractorOutput;

    public MyProfileActivityInteractor(IMyProfileActivityInteractorOutput iMyProfileActivityInteractorOutput) {
        this.iMyProfileActivityInteractorOutput = iMyProfileActivityInteractorOutput;
    }

    @Override
    public void getProfileDetails(){
        UserAuthCloudDataStore.getInstance().getUserProfile(new GetUserProfileCallback() {
            @Override
            public void onGetUserProfileSuccess(GetUserProfileResponse getUserProfileResponse) {
                iMyProfileActivityInteractorOutput.
                        onGetUserProfileSuccess(getUserProfileResponse.getUserProfile());
            }

            @Override
            public void onGetUserProfileFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getProfileDetails();
                else
                    iMyProfileActivityInteractorOutput.onGetUserProfileFailure(errorCode, message);
            }
        });
    }

    @Override
    public void linkUserAccount(AccountLinkRequest accountLinkRequest){
        UserAuthCloudDataStore.getInstance().linkUserAccount(accountLinkRequest, new AccountLinkCallback() {
            @Override
            public void accountLinkSuccess() {
                iMyProfileActivityInteractorOutput.accountLinkSuccess(accountLinkRequest.getSocialType() );
            }

            @Override
            public void accountLinkFailure(int errorCode, String message) {
                iMyProfileActivityInteractorOutput.accountLinkFailure(errorCode, message);
            }
        });
    }
}
