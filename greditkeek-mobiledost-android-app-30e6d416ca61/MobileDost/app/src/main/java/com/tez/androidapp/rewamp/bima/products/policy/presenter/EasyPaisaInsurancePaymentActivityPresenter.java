package com.tez.androidapp.rewamp.bima.products.policy.presenter;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.rewamp.bima.products.policy.interactor.EasyPaisaInsurancePaymentActivityInteractor;
import com.tez.androidapp.rewamp.bima.products.policy.interactor.IEasyPaisaInsurancePaymentActivityInteractor;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.request.PurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.response.PolicyDetails;
import com.tez.androidapp.rewamp.bima.products.policy.response.PurchasedInsurancePolicy;
import com.tez.androidapp.rewamp.bima.products.policy.view.IEasyPaisaInsurancePaymentActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

public class EasyPaisaInsurancePaymentActivityPresenter implements IEasyPaisaInsurancePaymentActivityPresenter,
        IEasyPaisaInsurancePaymentActivityInteractorOutput {

    protected final IEasyPaisaInsurancePaymentActivityInteractor iEasyPaisaInsurancePaymentActivityInteractor;
    protected final IEasyPaisaInsurancePaymentActivityView iEasyPaisaInsurancePaymentActivityView;
    protected PolicyDetails policyDetails;

    public EasyPaisaInsurancePaymentActivityPresenter(IEasyPaisaInsurancePaymentActivityView iEasyPaisaInsurancePaymentActivityView){
        this.iEasyPaisaInsurancePaymentActivityView = iEasyPaisaInsurancePaymentActivityView;
        this.iEasyPaisaInsurancePaymentActivityInteractor = new EasyPaisaInsurancePaymentActivityInteractor(this);
    }

    @Override
    public void initiatePurchasePolicy(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest){

        iEasyPaisaInsurancePaymentActivityView.setPinViewOtpEnabled(false);
        iEasyPaisaInsurancePaymentActivityView.showTezLoader();
        this.iEasyPaisaInsurancePaymentActivityInteractor.initiatePurchasePolicy(initiatePurchasePolicyRequest);
    }


    @Override
    public void initiatePurchasePolicySuccess(PolicyDetails policyDetails) {
        iEasyPaisaInsurancePaymentActivityView.dismissTezLoader();
        iEasyPaisaInsurancePaymentActivityView.setPinViewOtpEnabled(true);
        this.policyDetails = policyDetails;
    }

    @Override
    public void initiatePurchasePolicyFailure(int errorCode, String message) {
        iEasyPaisaInsurancePaymentActivityView.dismissTezLoader();
        iEasyPaisaInsurancePaymentActivityView.showError(errorCode,
                iEasyPaisaInsurancePaymentActivityView.getFormattedErrorMessage(ResponseStatusCode.getDescriptionFromErrorCode(errorCode)),
                (dialog, which) -> iEasyPaisaInsurancePaymentActivityView.finishActivity());
    }

    @Override
    public void purchasePolicy(String pin, @Nullable Location location){
        PurchasePolicyRequest purchasePolicyRequest = new PurchasePolicyRequest();
        purchasePolicyRequest.setLat(location == null? null:location.getLatitude());
        purchasePolicyRequest.setLng(location == null? null: location.getLongitude());
        setPinInRequest(purchasePolicyRequest, pin);
        purchasePolicyRequest.setPaymentCorrelationId(policyDetails.getPaymentCorrelationId());
        purchasePolicyRequest.setPolicyId(policyDetails.getPolicyId());
        this.iEasyPaisaInsurancePaymentActivityInteractor.purchasePolicy(purchasePolicyRequest);
    }

    @Override
    public void purchasePolicySuccess(PurchasedInsurancePolicy purchasedInsurancePolicy) {
        iEasyPaisaInsurancePaymentActivityView.routeToRepaymentSuccessFullActivity(purchasedInsurancePolicy);
    }

    @Override
    public void purchasePolicyFailure(int errorCode, String message) {
        iEasyPaisaInsurancePaymentActivityView.dismissTezLoader();
        iEasyPaisaInsurancePaymentActivityView.setPinViewOtpClearText();
        iEasyPaisaInsurancePaymentActivityView.showError(errorCode,
                iEasyPaisaInsurancePaymentActivityView.getFormattedErrorMessage(ResponseStatusCode.getDescriptionFromErrorCode(errorCode)),
                (d, v) -> iEasyPaisaInsurancePaymentActivityView.navigateToDashboard());
    }


    protected void setPinInRequest(@NonNull PurchasePolicyRequest request, @NonNull String pin) {
        request.setPin(pin);
    }
}
