package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.tez.androidapp.R;

/**
 * ProgressBar with tinting for backward compatibility
 * <p>
 * Created  on 5/5/2017.
 */

public class ProgressBarCompat extends ProgressBar {

    public ProgressBarCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ProgressBarCompat);
        int color = typedArray.getResourceId(R.styleable.ProgressBarCompat_progressTint, R.color.colorAccent);
        color = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(color, null) :
                getResources().getColor(color);
        if (isIndeterminate()) {
            getIndeterminateDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        } else {
            getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        }
        typedArray.recycle();
    }

}

