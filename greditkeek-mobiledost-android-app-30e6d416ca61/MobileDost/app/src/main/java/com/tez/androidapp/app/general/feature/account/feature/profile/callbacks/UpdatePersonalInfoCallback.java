package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

public interface UpdatePersonalInfoCallback {
    void onUpdatePersonalInfoSuccess(boolean isProfileCompleted);
    void onUpdatePersonalInfoFailure(int errorCode, String message);
}
