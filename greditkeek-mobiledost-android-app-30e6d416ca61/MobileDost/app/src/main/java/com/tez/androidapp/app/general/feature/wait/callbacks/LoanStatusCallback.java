package com.tez.androidapp.app.general.feature.wait.callbacks;

import com.tez.androidapp.app.general.feature.wait.models.network.dto.response.LoanStatusResponse;

/**
 * Created  on 4/6/2017.
 */

public interface LoanStatusCallback {

    void onLoanStatusSuccess(LoanStatusResponse loanStatusResponse);

    default void onLoanStatusFailure(int code, String message) {}
}
