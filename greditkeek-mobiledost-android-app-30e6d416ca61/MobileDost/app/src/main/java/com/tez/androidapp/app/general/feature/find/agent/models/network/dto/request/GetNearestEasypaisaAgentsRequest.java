package com.tez.androidapp.app.general.feature.find.agent.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 1/18/2018.
 */

public class GetNearestEasypaisaAgentsRequest extends BaseRequest {

    public static final String METHOD_NAME = "https://www.easytickets.pk/demo/rest/agents/getCashAgents/";


    public static final class Params {

        public static final String APP_ID = "AppId";
        public static final String APP_KEY = "AppKey";
        public static final String MERCHANT_TYPE = "Merchant_Type";
        public static final String LATITUDE = "Lat";
        public static final String LONGITUDE = "Lng";
        public static final String CONTENT_TYPE = "Content-Type";

        private Params() {
        }
    }
}
