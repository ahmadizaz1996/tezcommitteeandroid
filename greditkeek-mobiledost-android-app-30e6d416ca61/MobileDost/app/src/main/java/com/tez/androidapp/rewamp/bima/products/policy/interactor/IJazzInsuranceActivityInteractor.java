package com.tez.androidapp.rewamp.bima.products.policy.interactor;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.InitiateRepaymentRequest;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.IJazzInsuranceActivityInteractorOutput;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;

public interface IJazzInsuranceActivityInteractor {

    void initiatePurchasePolicy(@NonNull InitiatePurchasePolicyRequest initiateRepaymentRequest,
                                @NonNull String pin, @Nullable Location location);
}
