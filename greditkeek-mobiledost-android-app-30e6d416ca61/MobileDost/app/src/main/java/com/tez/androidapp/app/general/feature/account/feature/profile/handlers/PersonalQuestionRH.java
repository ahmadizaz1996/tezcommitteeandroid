package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import android.app.Person;

import androidx.annotation.Nullable;

import com.google.android.gms.vision.L;
import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.PersonalQuestionCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Question;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.PersonalQuestionResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.QuestionOptionsResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

import java.util.ArrayList;
import java.util.List;

public class PersonalQuestionRH extends BaseRH<PersonalQuestionResponse> {

    private PersonalQuestionCallback personalQuestionCallback;

    public PersonalQuestionRH(BaseCloudDataStore baseCloudDataStore, PersonalQuestionCallback personalQuestionCallback) {
        super(baseCloudDataStore);
        this.personalQuestionCallback = personalQuestionCallback;
    }

    @Override
    protected void onSuccess(Result<PersonalQuestionResponse> value) {
        PersonalQuestionResponse personalQuestionResponse = value.response().body();
        if (personalQuestionResponse!=null
                && personalQuestionResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR
                && personalQuestionResponse.getData()!=null)

            personalQuestionCallback.onPersonalQuestionSuccess(transformDataSet(personalQuestionResponse.getData()));
        else onFailure(personalQuestionResponse.getStatusCode(),
                personalQuestionResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        personalQuestionCallback.onPersonalQuestionFailure(errorCode, message);
    }

    private List<Question> transformDataSet(List<Question> data){
        List<Question> questionList = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            Question question = data.get(i);
            question.setIndex(i);
            questionList.add(question);
        }
        return questionList;
    }
}
