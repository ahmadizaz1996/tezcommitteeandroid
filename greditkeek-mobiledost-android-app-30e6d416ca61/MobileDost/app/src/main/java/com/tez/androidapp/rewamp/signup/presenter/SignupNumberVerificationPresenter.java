package com.tez.androidapp.rewamp.signup.presenter;

import com.tez.androidapp.commons.models.network.dto.request.UserSignUpRequest;
import com.tez.androidapp.rewamp.signup.view.ISignupPermissionActivityView;

public class SignupNumberVerificationPresenter extends SignupPermissionActivityPresenter implements
        ISignupNumberVerificationInteractorOutput {


    public SignupNumberVerificationPresenter(ISignupPermissionActivityView iSignupNumberVerificationActivityView) {
        super(iSignupNumberVerificationActivityView);
    }

    @Override
    public void submitSignUpRequest(UserSignUpRequest userSignUpRequest) {
        this.iSignupPermissionActivityInteractor.submitUserSignUpRequest(userSignUpRequest);
    }

    @Override
    public void onSubmitSignUpRequestSuccess() {
        this.iSignupPermissionActivityView.createSinchVerification();
    }

    @Override
    public void onSubmitSignUpRequestFailure(int errorCode, String message) {
        this.iSignupPermissionActivityView.showError(errorCode,
                (dialog, which) -> iSignupPermissionActivityView.finishActivity());
    }
}
