package com.tez.androidapp.rewamp.committee.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.presenter.CommitteeTermsAndConditionPresenter;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeResponse;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeCompletedPopupActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeTermsandConditionActivityRouter;
import com.tez.androidapp.rewamp.committee.shared.api.client.CommitteeAPI;
import com.tez.androidapp.rewamp.common.BaseTermsAndConditionActivity;
import com.tez.androidapp.rewamp.signup.TermsAndCondition;
import com.tez.androidapp.rewamp.util.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter.COMMITTEE_DATA;
import static com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter.COMMITTE_CREATEION;

public class CommitteeTermsandConditionActivity extends BaseTermsAndConditionActivity implements ICommitteeTermsCondActivityView {


    private final CommitteeTermsAndConditionPresenter mCommitteeTermsAndConditionPresenter;
    private CommitteePackageModel committeePackage;
    private MyCommitteeResponse myCommitteeResponse;
    private CommitteeCreateResponse committeeCreationResponse;
    private JoinCommitteeResponse joinCommitteeResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fetchExtras();
    }

    private void fetchExtras() {
        Intent intent = getIntent();
        if (intent != null
                && intent.getExtras() != null
        ) {
//            committeePackage = (CommitteePackageModel) intent.getExtras().getSerializable(COMMITTEE_DATA);
            myCommitteeResponse = intent.getExtras().getParcelable(CommitteeSummaryActivityRouter.MY_COMMITTEE_RESPONSE);
            joinCommitteeResponse = intent.getExtras().getParcelable(CommitteeSummaryActivityRouter.JOIN_COMMITTEE_RESPONSE);
            committeeCreationResponse = (CommitteeCreateResponse) intent.getExtras().getSerializable(COMMITTE_CREATEION);
        }
    }

    public CommitteeTermsandConditionActivity() {
        mCommitteeTermsAndConditionPresenter = new CommitteeTermsAndConditionPresenter(this);
    }

    @Override
    protected String getScreenName() {
        return CommitteeTermsandConditionActivity.class.getSimpleName();
    }

    @Override
    protected List<TermsAndCondition> getTermAndConditions() {
        return new ArrayList<TermsAndCondition>() {
            {
                add(new TermsAndCondition("I hereby, agree that all the information I have provided or will provide is and will be correct and to the best of my knowledge."));
                add(new TermsAndCondition("I hereby agree, that I will not use this app for any unlawful, defamatory, threatening or hateful purposes."));
                add(new TermsAndCondition("I understand that Tez has the right ot revoke my acess to the Tez app for any reason Tez Financial Services find necessary."));
            }
        };
    }

    @Override
    protected void onClickBtMainButton() {
        if (joinCommitteeResponse != null)
            CommitteeCompletedPopupActivityRouter.createInstance().setDependenciesAndRoute(this, joinCommitteeResponse);
        else
            CommitteeCompletedPopupActivityRouter.createInstance().setDependenciesAndRoute(this, committeeCreationResponse);
//        CommitteeCreationSuccessDialog committeeCreationSuccessDialog = new CommitteeCreationSuccessDialog();
//        committeeCreationSuccessDialog.show(getSupportFragmentManager(), CommitteeCreationSuccessDialog.class.getSimpleName());
        /*Intent i = new Intent(this, CommitteeCompletedPopupActivity.class);
        i.putExtra("COMMITTEE_DATA",committeePackage);
        startActivity(i);*/
        /*CommitteeCreateRequest committeeCreateRequest = new CommitteeCreateRequest(
                committeePackage.getCommitteeName(),
                committeePackage.getDurationInMonths(),
                committeePackage.getSelectedMembers(),
                committeePackage.getId(),
                committeePackage.getStartDate(),
                committeePackage.getSelectedAmount()
        );
//        mCommitteeTermsAndConditionPresenter.createCommittee(committeeCreateRequest);
        CommitteeAPI committeeAPI = RetrofitClient.createAPI();
        Call<CommitteeCreateResponse> responseCall = committeeAPI.createCommittee("Bearer " + MDPreferenceManager.getAccessToken().getAccessToken(),
                committeeCreateRequest);
        responseCall.enqueue(new Callback<CommitteeCreateResponse>() {
            @Override
            public void onResponse(Call<CommitteeCreateResponse> call, Response<CommitteeCreateResponse> response) {
                Log.d("RESPONSE", response.toString());
                CommitteeTermsandConditionActivityRouter.createInstance().setDependenciesAndRoute(CommitteeTermsandConditionActivity.this,committeePackage);
                *//*Intent i = new Intent(CommitteeTermsandConditionActivity.this, CommitteeCompletedPopupActivity.class);
                i.putExtra("COMMITTEE_DATA",committeePackage);
                startActivity(i);*//*
            }

            @Override
            public void onFailure(Call<CommitteeCreateResponse> call, Throwable t) {
                Log.d("RESPONSE", t.toString());
                Toast.makeText(CommitteeTermsandConditionActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void onCommitteeCreate() {
        CommitteeCreationSuccessDialog committeeCreationSuccessDialog = new CommitteeCreationSuccessDialog();
        committeeCreationSuccessDialog.show(getSupportFragmentManager(), CommitteeCreationSuccessDialog.class.getSimpleName());
    }
}