package com.tez.androidapp.rewamp.committee.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeDeclineListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeLeaveListener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeDeclineRH extends BaseRH<CommitteeDeclineResponse> {

    private final CommitteeDeclineListener listener;

    public CommitteeDeclineRH(BaseCloudDataStore baseCloudDataStore, CommitteeDeclineListener committeeDeclineListener) {
        super(baseCloudDataStore);
        this.listener = committeeDeclineListener;
    }

    @Override
    protected void onSuccess(Result<CommitteeDeclineResponse> value) {
        CommitteeDeclineResponse committeeDeclineResponse = value.response().body();
        if (committeeDeclineResponse != null && committeeDeclineResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onCommitteeDeclineSuccess(committeeDeclineResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onCommitteeDeclineeFailure(errorCode, message);
    }
}
