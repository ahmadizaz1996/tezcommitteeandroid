package com.tez.androidapp.rewamp.bima.products.policy.view;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.rewamp.advance.repay.presenter.IVerifyJazzRepaymentActivityPresenter;
import com.tez.androidapp.rewamp.advance.repay.presenter.VerifyJazzRepaymentActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.IJazzInsuranceActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.JazzInsuranceActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.router.EasyPaisaInsurancePaymentActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.JazzInsurancePaymentActivityRouter;

public class JazzInsurancePaymentActivity
        extends EasyPaisaInsurancePaymentActivity
        implements IJazzInsurancePaymentActivityView{


    private static final int JAZZ_PIN_LENGTH = 4;

    private IJazzInsuranceActivityPresenter iJazzInsuranceActivityPresenter;

    public JazzInsurancePaymentActivity() {
        iJazzInsuranceActivityPresenter = new JazzInsuranceActivityPresenter(this);
    }

    @Override
    protected void init() {
        pinViewOTP.setOnPinEnteredListener(this);
        initViews();
    }

    @Override
    protected void purchasePolicy(@NonNull String pin, @Nullable Location location) {
        iJazzInsuranceActivityPresenter.initiatePurchasePolicy(getInitiatePurchasePolicyRequest(),
                location, pin);
    }

    @Override
    protected InitiatePurchasePolicyRequest getInitiatePurchasePolicyRequest() {
        return getIntent().getParcelableExtra(JazzInsurancePaymentActivityRouter.PRODUCT_DETAILS);
    }

    @Override
    protected int getPinLength() {
        return JAZZ_PIN_LENGTH;
    }

}
