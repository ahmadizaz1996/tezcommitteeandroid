package com.tez.androidapp.rewamp.bima.products.policy.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InsuranceProductDetailCallback;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.IPurchasePolicyActivityInteractorOutput;
import com.tez.androidapp.rewamp.bima.products.policy.response.InsuranceProductDetailResponse;

public class PurchasePolicyActivityInteractor implements IPurchasePolicyActivityInteractor {

    private IPurchasePolicyActivityInteractorOutput iPurchasePolicyActivityInteractorOutput;

    public PurchasePolicyActivityInteractor(IPurchasePolicyActivityInteractorOutput iPurchasePolicyActivityInteractorOutput){
        this.iPurchasePolicyActivityInteractorOutput = iPurchasePolicyActivityInteractorOutput;
    }

    @Override
    public void getInsuranceCoverage(int productId){
        BimaCloudDataStore.getInstance().getInsuranceCoverage(productId, new InsuranceProductDetailCallback() {
            @Override
            public void onSuccessInsuranceProductDetail(InsuranceProductDetailResponse insuranceProductDetailResponse) {
                iPurchasePolicyActivityInteractorOutput.onSuccessInsuranceProductDetail(insuranceProductDetailResponse);
            }

            @Override
            public void onFailureInsuranceProductDetal(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getInsuranceCoverage(productId);
                else
                    iPurchasePolicyActivityInteractorOutput.onFailureInsuranceProductDetal(errorCode, message);
            }
        });

    }
}
