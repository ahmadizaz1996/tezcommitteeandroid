package com.tez.androidapp.rewamp.committee.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.tez.androidapp.app.base.response.BaseResponse;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class CommitteeCheckInvitesResponse extends BaseResponse implements Parcelable {
    private int committeeId;
    private String committeeName;
    private int inviteId;
    private String status;
    private String inviteDate;

    protected CommitteeCheckInvitesResponse(Parcel in) {
        committeeId = in.readInt();
        committeeName = in.readString();
        inviteId = in.readInt();
        status = in.readString();
        inviteDate = in.readString();
    }

    public static final Creator<CommitteeCheckInvitesResponse> CREATOR = new Creator<CommitteeCheckInvitesResponse>() {
        @Override
        public CommitteeCheckInvitesResponse createFromParcel(Parcel in) {
            return new CommitteeCheckInvitesResponse(in);
        }

        @Override
        public CommitteeCheckInvitesResponse[] newArray(int size) {
            return new CommitteeCheckInvitesResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(committeeId);
        parcel.writeString(committeeName);
        parcel.writeInt(inviteId);
        parcel.writeString(status);
        parcel.writeString(inviteDate);
    }

    public int getCommitteeId() {
        return committeeId;
    }

    public void setCommitteeId(int committeeId) {
        this.committeeId = committeeId;
    }

    public String getCommitteeName() {
        return committeeName;
    }

    public void setCommitteeName(String committeeName) {
        this.committeeName = committeeName;
    }

    public int getInviteId() {
        return inviteId;
    }

    public void setInviteId(int inviteId) {
        this.inviteId = inviteId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInviteDate() {
        return inviteDate;
    }

    public void setInviteDate(String inviteDate) {
        this.inviteDate = inviteDate;
    }
}



