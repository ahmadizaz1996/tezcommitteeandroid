package com.tez.androidapp.rewamp.general.feedback.response.handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;
import com.tez.androidapp.rewamp.general.feedback.callback.LoanFeedbackCallback;

public class LoanFeedbackRH extends DashboardCardsRH<DashboardCardsResponse> {

    @NonNull
    private final LoanFeedbackCallback loanFeedbackCallback;

    public LoanFeedbackRH(BaseCloudDataStore baseCloudDataStore, @NonNull LoanFeedbackCallback loanFeedbackCallback) {
        super(baseCloudDataStore);
        this.loanFeedbackCallback = loanFeedbackCallback;
    }

    @Override
    protected void onSuccess(Result<DashboardCardsResponse> value) {
        super.onSuccess(value);
        DashboardCardsResponse response = value.response().body();
        if (response != null) {
            if (isErrorFree(response))
                loanFeedbackCallback.onSubmitFeedbackSuccess(response);
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        loanFeedbackCallback.onSubmitFeedbackFailure(errorCode, message);
    }
}
