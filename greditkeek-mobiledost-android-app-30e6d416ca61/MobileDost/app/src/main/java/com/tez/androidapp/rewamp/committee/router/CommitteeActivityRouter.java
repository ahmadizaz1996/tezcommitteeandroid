package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.view.CommitteeActivity;
import com.tez.androidapp.rewamp.committee.view.CommitteeSummaryActivity;

import androidx.annotation.NonNull;

public class CommitteeActivityRouter extends BaseActivityRouter {


    public static CommitteeActivityRouter createInstance() {
        return new CommitteeActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, @NonNull LoanDetails loanDetails) {
        Intent intent = createIntent(from);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

}
