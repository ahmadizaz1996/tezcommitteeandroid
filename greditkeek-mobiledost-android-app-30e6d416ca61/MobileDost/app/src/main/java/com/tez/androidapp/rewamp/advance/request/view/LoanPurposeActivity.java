package com.tez.androidapp.rewamp.advance.request.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.ToolbarActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutIfVisibleThenNotEmpty;
import com.tez.androidapp.commons.widgets.TezAutoCompleteTextView;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezSpinner;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.rewamp.advance.request.adapter.LoanPurposeAdapter;
import com.tez.androidapp.rewamp.advance.request.model.LoanPurpose;
import com.tez.androidapp.rewamp.advance.request.request.LoanApplyRequest;
import com.tez.androidapp.rewamp.advance.request.router.AdvanceTermsAndConditionActivityRouter;
import com.tez.androidapp.rewamp.advance.request.router.LoanPurposeActivityRouter;

import net.tez.fragment.util.optional.Optional;
import net.tez.fragment.util.optional.TextUtil;
import net.tez.validator.library.annotations.Order;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class LoanPurposeActivity extends ToolbarActivity implements LoanPurposeAdapter.LoanPurposeListener, ValidationListener {

    @BindView(R.id.rvLoanPurposes)
    private RecyclerView rvLoanPurposes;

    @BindView(R.id.btContinue)
    private TezButton btContinue;

    @BindView(R.id.tilOther)
    private TezTextInputLayout tilOther;

    @TextInputLayoutIfVisibleThenNotEmpty(value = {1, R.string.string_required_feild})
    @Order(1)
    @BindView(R.id.tilOutstandingAmount)
    private TezTextInputLayout tilOutstandingAmount;

    @TextInputLayoutIfVisibleThenNotEmpty(value = {1, R.string.string_required_feild})
    @Order(2)
    @BindView(R.id.tilLoanTakenFromNameType)
    private TezTextInputLayout tilLoanTakenFromNameType;

    @TextInputLayoutIfVisibleThenNotEmpty(value = {1, R.string.string_required_feild})
    @Order(3)
    @BindView(R.id.tilLoanTakenFromName)
    private TezTextInputLayout tilLoanTakenFromName;

    @BindView(R.id.sOutstandingAmount)
    private TezSpinner sOutstandingAmount;

    @BindView(R.id.sLoanTakenFromNameType)
    private TezSpinner sLoanTakenFromNameType;

    @BindView(R.id.actLoanTakenFromName)
    private TezAutoCompleteTextView actLoanTakenFromName;

    @BindView(R.id.etOther)
    private TezEditTextView etOther;

    private LoanPurposeAdapter loanPurposeAdapter;

    private String selectedFinancialInstitution = "";

    private CompositeDisposable allDisposables;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_purpose);
        this.tezToolbar.setToolbarTitle(R.string.loan_purpose);
        this.initLoanPurpose();
        this.initAdapters();
        this.initOnClickListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dispose();
    }

    private void dispose() {
        if (allDisposables != null)
            allDisposables.dispose();
    }

    private void initLoanPurpose() {
        List<LoanPurpose> loanPurposeList = new ArrayList<>();
        loanPurposeList.add(new LoanPurpose(258, R.drawable.ic_committee, R.string.committee));
        loanPurposeList.add(new LoanPurpose(259, R.drawable.ic_education, R.string.education));
        loanPurposeList.add(new LoanPurpose(260, R.drawable.ic_food, R.string.food_and_groceries));
        loanPurposeList.add(new LoanPurpose(261, R.drawable.ic_rent, R.string.house_rent));
        loanPurposeList.add(new LoanPurpose(262, R.drawable.ic_medical, R.string.medical_expenses));
        loanPurposeList.add(new LoanPurpose(263, R.drawable.ic_mobile_topup, R.string.mobile_topup));
        loanPurposeList.add(new LoanPurpose(264, R.drawable.ic_movie, R.string.movie));
        loanPurposeList.add(new LoanPurpose(265, R.drawable.ic_petrol, R.string.petrol));
        loanPurposeList.add(new LoanPurpose(266, R.drawable.ic_travel, R.string.travel));
        loanPurposeList.add(new LoanPurpose(267, R.drawable.ic_bill, R.string.household_bills));
        loanPurposeList.add(new LoanPurpose(268, R.drawable.ic_business_need, R.string.business_needs));
        loanPurposeList.add(new LoanPurpose(269, R.drawable.ic_loan_installment, R.string.loan_installment));
        loanPurposeList.add(new LoanPurpose(270, R.drawable.ic_other_purpose, R.string.other));

        loanPurposeAdapter = new LoanPurposeAdapter(loanPurposeList, this);
        rvLoanPurposes.setLayoutManager(new GridLayoutManager(this, 3));
        rvLoanPurposes.setNestedScrollingEnabled(false);
        rvLoanPurposes.setAdapter(loanPurposeAdapter);
    }

    private void listenChangesOnViews() {

        if (allDisposables == null || allDisposables.isDisposed()) {
            allDisposables = new CompositeDisposable();
            this.allDisposables.add(RxTextView.textChanges(this.sOutstandingAmount).toFlowable(BackpressureStrategy.LATEST)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(charSequence -> FieldValidator.validate(this, this)));

            this.allDisposables.add(RxTextView.textChanges(this.sLoanTakenFromNameType).toFlowable(BackpressureStrategy.LATEST)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(charSequence -> FieldValidator.validate(this, this)));

            this.allDisposables.add(RxTextView.textChanges(this.actLoanTakenFromName).toFlowable(BackpressureStrategy.LATEST)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(charSequence -> FieldValidator.validate(this, this)));
        }
    }

    private void initAdapters() {
        final ArrayAdapter<String> sOutstandingAmountAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.loan_outstanding_amount_array));

        final ArrayAdapter<String> sLoanTakenFromNameTypeAdapter = (new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.loan_provider_array)));

        final String[] financialInstitutionArray = getResources().getStringArray(R.array.loan_financial_institutions);

        final ArrayAdapter<String> actLoanTakenFromNameAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                financialInstitutionArray);


        sOutstandingAmount.setAdapter(sOutstandingAmountAdapter);
        sLoanTakenFromNameType.setAdapter(sLoanTakenFromNameTypeAdapter);
        actLoanTakenFromName.setAdapter(actLoanTakenFromNameAdapter);

        sOutstandingAmount.setOnItemClickListener((parent, view, position, id) -> sLoanTakenFromNameType.requestFocus());

        sLoanTakenFromNameType.setOnItemClickListener((parent, view, position, id) -> {
            tilLoanTakenFromName.setVisibility(position == 0 ? View.VISIBLE : View.GONE);
            if (position == 0) {
                actLoanTakenFromName.requestFocus();
                actLoanTakenFromName.setText(null);
            }
        });

        actLoanTakenFromName.setThreshold(1);

        actLoanTakenFromName.setOnItemClickListener((parent, view, position, id) -> {
            Utility.hideKeyboard(this, actLoanTakenFromName);
            tilLoanTakenFromName.setError(null);
            selectedFinancialInstitution = actLoanTakenFromNameAdapter.getItem(position);
            btContinue.requestFocus();
        });

        actLoanTakenFromName.setDoubleTapSafeOnClickListener(view -> actLoanTakenFromName.showDropDown());
    }

    private void initOnClickListener() {
        this.btContinue.setDoubleTapSafeOnClickListener(view -> onClickBtContinue());
    }

    private void onClickBtContinue() {
        FieldValidator.validate(this, new ValidationListener() {
            @Override
            public void validateSuccess() {
                if (tilLoanTakenFromName.getVisibility() == View.VISIBLE && !TextUtil.equals(selectedFinancialInstitution, actLoanTakenFromName.getValueText()))
                    tilLoanTakenFromName.setError(getString(R.string.invalid_name));

                else
                    Optional.ifPresent(getLoanApplyRequest(),
                            LoanPurposeActivity.this::routeToAdvanceTermsAndConditionsActivity,
                            LoanPurposeActivity.this::showUnExpectedErrorOccurredDialog);
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                btContinue.setButtonInactive();
                filterChain.doFilter();
            }
        });
    }

    private void routeToAdvanceTermsAndConditionsActivity(@NonNull LoanApplyRequest loanApplyRequest) {
        AdvanceTermsAndConditionActivityRouter.createInstance().setDependenciesAndRoute(this, loanApplyRequest);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onClickLoanPurpose(@NonNull LoanPurpose loanPurpose) {
        boolean isLoanInstallment = loanPurpose.getId() == 269;
        boolean isOther = loanPurpose.getId() == 270;
        this.tilOther.setVisibility(isOther ? View.VISIBLE : View.GONE);
        this.tilOutstandingAmount.setVisibility(isLoanInstallment ? View.VISIBLE : View.GONE);
        this.tilLoanTakenFromNameType.setVisibility(isLoanInstallment ? View.VISIBLE : View.GONE);
        this.tilLoanTakenFromName.setVisibility(View.GONE);
        this.sOutstandingAmount.setText(null);
        this.sLoanTakenFromNameType.setText(null);
        this.actLoanTakenFromName.setText(null);
        this.etOther.setText(null);
        this.btContinue.setEnabled(true);

        if (isLoanInstallment) {
            listenChangesOnViews();
            this.sOutstandingAmount.requestFocus();
        } else {
            if (isOther)
                this.etOther.requestFocus();

            this.btContinue.setButtonNormal();
            this.dispose();
        }
    }

    private LoanApplyRequest getLoanApplyRequest() {
        LoanApplyRequest loanApplyRequest = getIntent().getParcelableExtra(LoanPurposeActivityRouter.LOAN_APPLY_REQUEST_PARCELABLE);
        Optional.ifPresent(loanApplyRequest, this::fillPurposeDetailsInLoanApplyRequest);
        return loanApplyRequest;
    }

    private void fillPurposeDetailsInLoanApplyRequest(@NonNull LoanApplyRequest loanApplyRequest) {
        loanApplyRequest.setPurposeId(loanPurposeAdapter.getSelectedPurposeId());

        boolean isLoanInstallment = loanPurposeAdapter.getSelectedPurposeId() == 269;
        boolean isOther = loanPurposeAdapter.getSelectedPurposeId() == 270;
        boolean isIncludeFinancialInstitution = tilLoanTakenFromName.getVisibility() == View.VISIBLE;

        loanApplyRequest.setOtherReason(isOther ? etOther.getValueText() : null);
        loanApplyRequest.setLoanOutstandingAmountRange(isLoanInstallment ? sOutstandingAmount.getValueText() : null);
        loanApplyRequest.setLoanTakenFromType(isLoanInstallment ? sLoanTakenFromNameType.getValueText() : null);
        loanApplyRequest.setLoanTakenFromName(isLoanInstallment && isIncludeFinancialInstitution ? actLoanTakenFromName.getValueText() : null);
    }

    @Override
    public void validateSuccess() {
        this.btContinue.setButtonNormal();
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.btContinue.setButtonInactive();
        filterChain.doFilter();
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    protected String getScreenName() {
        return "LoanPurposeActivity";
    }
}
