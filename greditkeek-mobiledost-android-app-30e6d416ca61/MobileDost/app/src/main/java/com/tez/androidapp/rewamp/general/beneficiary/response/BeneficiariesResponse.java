package com.tez.androidapp.rewamp.general.beneficiary.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.general.beneficiary.entity.Beneficiary;

import java.util.List;

public class BeneficiariesResponse extends BaseResponse {

    private List<Beneficiary> beneficiaryList;

    public List<Beneficiary> getBeneficiaryList() {
        return beneficiaryList;
    }
}
