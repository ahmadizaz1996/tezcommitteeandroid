package com.tez.androidapp.commons.models.network;

import androidx.annotation.Nullable;

import com.google.android.gms.common.util.CollectionUtils;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.MobileUserRejectedFeildsDto;
import com.tez.androidapp.commons.location.models.network.City;
import com.tez.androidapp.commons.utils.app.Utility;

import net.tez.logger.library.utils.TextUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created  on 2/13/2017.
 */

public class User implements Serializable {
    public static final String GENDER_MALE = "M";
    public static final String GENDER_FEMALE = "F";
    public static final String GENDER_TRANSGENDER = "T";


    private List<UserDevice> userDevices;
    private List<UserAddress> addresses;
    private List<Integer> linkedSocialAccounts;
    private List<AnswerSelected> personalInformationList;
    private String id;
    private String email;
    private String fullName;
    private String mobileNumber;
    private String gender;
    private String dateOfBirth;
    private String cnicDateOfIssue;
    private String cnic;
    private Boolean isProfileCompleted;
    private String motherMaidenName;
    private String principalName;
    private Boolean isMobileNumberVerified;
    private Integer placeOfBirthId;
    private String placeOfBirthName;
    private Boolean isCnicExpired;
    private Boolean isCnicEditable;
    private String cnicDateOfExpiry;
    private String signUpDateTime;
    private String userReferralType;
    private String referralCode;
    private boolean cnicLifeTimeValid;
    private String fatherName;
    private String husbandName;
    private Integer cnicFieldsDetectionStatus;
    private Boolean isEditable;

    public String getFatherName() {
        return fatherName;
    }

    public String getHusbandName() {
        return husbandName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public void setHusbandName(String husbandName) {
        this.husbandName = husbandName;
    }

    public Integer getCnicFieldsDetectionStatus() {
        return cnicFieldsDetectionStatus;
    }

    private MobileUserRejectedFeildsDto mobileUserRejectedFieldsDto;

    public Boolean getCnicExpired() {
        return isCnicExpired;
    }

    public boolean isCnicLifeTimeValid() {
        return cnicLifeTimeValid;
    }

    public void setCnicLifeTimeValid(boolean cnicLifeTimeValid) {
        this.cnicLifeTimeValid = cnicLifeTimeValid;
    }

    public void setCnicExpired(Boolean cnicExpired) {
        isCnicExpired = cnicExpired;
    }

    public Boolean isProfileCompleted() {
        return isProfileCompleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCnic() {
        return cnic;
    }

    public List<Integer> getLinkedSocialAccounts() {
        return linkedSocialAccounts;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public String getCnicDateOfIssue() {
        return cnicDateOfIssue;
    }

    public void setCnicDateOfIssue(String cnicDateOfIssue) {
        this.cnicDateOfIssue = cnicDateOfIssue;
    }

    public String getCnicDateOfExpiry() {
        return cnicDateOfExpiry;
    }

    public void setCnicDateOfExpiry(String cnicDateOfExpiry) {
        this.cnicDateOfExpiry = cnicDateOfExpiry;
    }

    public String getSignUpDateTime() {
        return signUpDateTime;
    }

    public void setSignUpDateTime(String signUpDateTime) {
        this.signUpDateTime = signUpDateTime;
    }

    public String getUserReferralType() {
        return userReferralType;
    }

    public void setUserReferralType(String userReferralType) {
        this.userReferralType = userReferralType;
    }

    public String getMotherMaidenName() {
        return motherMaidenName;
    }

    public void setMotherMaidenName(String motherMaidenName) {
        this.motherMaidenName = motherMaidenName;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public List<UserDevice> getUserDevices() {
        return userDevices;
    }

    public void setUserDevices(List<UserDevice> userDevices) {
        this.userDevices = userDevices;
    }

    public List<UserAddress> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<UserAddress> addresses) {
        this.addresses = addresses;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public User getUserWithMinProps() {
        User user = new User();
        user.setCnicExpired(isCnicExpired);
        user.setId(id);
        user.setEmail(email);
        user.setMobileNumber(mobileNumber);
        user.setDateOfBirth(dateOfBirth);
        user.setCnic(cnic);
        user.setFullName(fullName);
        user.setPrincipalName(principalName);
        user.setCnicDateOfExpiry(cnicDateOfExpiry);
        return user;
    }

    public String getPlaceOfBirthName() {
        return placeOfBirthName;
    }

    public void setPlaceOfBirthName(String placeOfBirthName) {
        this.placeOfBirthName = placeOfBirthName;
    }

    public List<AnswerSelected> getPersonalInformationList() {
        return personalInformationList;
    }

    public void setPersonalInformationList(List<AnswerSelected> personalInformationList) {
        this.personalInformationList = personalInformationList;
    }

    public Integer getPlaceOfBirthId() {
        return placeOfBirthId;
    }

    public void setPlaceOfBirthId(Integer placeOfBirthId) {
        this.placeOfBirthId = placeOfBirthId;
    }

    public Boolean getCnicEditable() {
        return isCnicEditable;
    }

    public void setCnicEditable(Boolean cnicEditable) {
        isCnicEditable = cnicEditable;
    }


    public void setProfileCompleted(Boolean isProfileCompleted) {
        isProfileCompleted = isProfileCompleted;
    }

    public Boolean getMobileNumberVerified() {
        return isMobileNumberVerified;
    }

    public void setMobileNumberVerified(Boolean mobileNumberVerified) {
        isMobileNumberVerified = mobileNumberVerified;
    }


    public MobileUserRejectedFeildsDto getMobileUserRejectedFieldsDto() {
        return mobileUserRejectedFieldsDto;
    }
    public void setMobileUserRejectedFieldsDto(MobileUserRejectedFeildsDto mobileUserRejectedFieldsDto) {
        this.mobileUserRejectedFieldsDto =  mobileUserRejectedFieldsDto;
    }

    public boolean getEditable() {
        return isEditable==null? true: isEditable;
    }

    public void setEditable(boolean isEditable){
        this.isEditable =isEditable;
    }

    public boolean isAddressEquals(String address){
        if(CollectionUtils.isEmpty(addresses)
                || addresses.get(0) ==null
                || TextUtil.isEmpty(addresses.get(0).getAddress())){
            return TextUtil.isEmpty(address);
        } else{
            return TextUtil.equals(address, addresses.get(0).getAddress());
        }
    }

    public boolean isCityEquals(@Nullable Integer cityId){
        if(CollectionUtils.isEmpty(addresses)
                || addresses.get(0) ==null
                || addresses.get(0).getCity()==null){
            return cityId == null;
        } else {
            return cityId!=null && cityId ==  addresses.get(0).getCity().getCityId();
        }
    }

    public boolean isPlaceOfBirthEquals(Integer placeOfBirthId){
        if(this.placeOfBirthId == null && placeOfBirthId == null)
            return true;
        else
            return this.placeOfBirthId != null && this.placeOfBirthId.equals(placeOfBirthId);
    }
}
