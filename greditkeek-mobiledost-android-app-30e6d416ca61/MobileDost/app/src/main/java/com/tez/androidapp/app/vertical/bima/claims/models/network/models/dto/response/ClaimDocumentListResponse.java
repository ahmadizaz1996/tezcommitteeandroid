package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.MandatoryClaimDocument;

import java.util.Collections;
import java.util.List;

/**
 * Created by FARHAN DHANANI on 6/14/2018.
 */
public class ClaimDocumentListResponse extends BaseResponse {

    private List<MandatoryClaimDocument> documentList;

    public List<MandatoryClaimDocument> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<MandatoryClaimDocument> documentList) {
        this.documentList = documentList;
    }

    @Override
    public String toString() {
        return "documentList{" +
                "documents=" + Collections.singletonList(documentList) +
                "} " + super.toString();
    }
}
