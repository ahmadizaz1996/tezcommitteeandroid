package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 4/7/2017.
 */

public class UserDeleteRequest extends BaseRequest{

    public static final String METHOD_NAME = "v1/user/delete";
}
