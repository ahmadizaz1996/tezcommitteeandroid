package com.tez.androidapp.rewamp.advance.repay.presenter;

public interface IVerifyRepaymentActivityPresenter extends IVerifyEasyPaisaRepaymentActivityPresenter {

    void resendCode(int loanId, int mobileAccountId);
}
