package com.tez.androidapp.rewamp.committee.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeDeclineRequest extends BaseRequest {

    public static final String METHOD_NAME = "http://3.21.127.35/api/v1/committee/decline/{" + Params.COMMITTEE_ID + "}";

    public static final class Params {
        public static final String COMMITTEE_ID = "committee_id";

        private Params() {
        }
    }

}
