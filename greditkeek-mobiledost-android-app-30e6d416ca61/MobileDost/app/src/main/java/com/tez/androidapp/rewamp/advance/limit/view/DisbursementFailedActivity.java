package com.tez.androidapp.rewamp.advance.limit.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.rewamp.advance.request.router.SelectLoanActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

public class DisbursementFailedActivity extends BaseActivity {

    @BindView(R.id.btRetry)
    private TezButton btRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disbursement_failed);
        ViewBinder.bind(this);
        init();
    }

    private void init(){
        btRetry.setDoubleTapSafeOnClickListener(v->routeToSelectLoanActivity());
    }

    private void routeToSelectLoanActivity(){
        SelectLoanActivityRouter.createInstance().setDependenciesAndRoute(this);
    }


    @Override
    protected String getScreenName() {
        return "DisbursementFailedActivity";
    }
}
