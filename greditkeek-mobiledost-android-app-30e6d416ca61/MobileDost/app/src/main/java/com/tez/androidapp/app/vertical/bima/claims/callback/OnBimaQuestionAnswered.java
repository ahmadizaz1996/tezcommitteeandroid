package com.tez.androidapp.app.vertical.bima.claims.callback;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.find.agent.callbacks.OnUserAddressSelected;
import com.tez.androidapp.app.general.feature.questions.callbacks.OnQuestionAnswered;

import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;


/**
 * Created by VINOD KUMAR on 11/14/2018.
 */
public interface OnBimaQuestionAnswered extends OnQuestionAnswered, BaseRecyclerViewListener {

    void onMapRequested(@NonNull OnUserAddressSelected onUserAddressSelected);

    void onLocationAnswered(int questionPosition, @NonNull String address, double latitude, double longitude);

    void onAdditionalInformationEdited(int questionId, @NonNull String newInformation);

    void onDateAnswered(int position, int questionId, @NonNull String date);
}
