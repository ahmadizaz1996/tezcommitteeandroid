package com.tez.androidapp.app.general.feature.find.agent.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.find.agent.callbacks.FindAgentsCallback;
import com.tez.androidapp.app.general.feature.find.agent.models.network.dto.response.FindAgentsResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Log;

/**
 * Created  on 3/7/2017.
 */

public class FindAgentsRH extends BaseRH<FindAgentsResponse> {

    private FindAgentsCallback findAgentsCallback;

    public FindAgentsRH(BaseCloudDataStore baseCloudDataStore, FindAgentsCallback findAgentsCallback) {
        super(baseCloudDataStore);
        this.findAgentsCallback = findAgentsCallback;
    }

    @Override
    protected void onSuccess(Result<FindAgentsResponse> value) {
        FindAgentsResponse findAgentsResponse = value.response().body();
        Log.e(findAgentsResponse.toString());
        if (findAgentsResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            if (findAgentsCallback != null)
                findAgentsCallback.onFindAgentsSuccess(findAgentsResponse.getAgents());
        } else {
            onFailure(findAgentsResponse.getStatusCode(), findAgentsResponse.getErrorDescription());
        }

    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (findAgentsCallback != null) findAgentsCallback.onFindAgentsError(errorCode, message);
    }
}
