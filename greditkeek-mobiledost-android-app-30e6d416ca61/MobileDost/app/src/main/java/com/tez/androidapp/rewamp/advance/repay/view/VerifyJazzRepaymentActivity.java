package com.tez.androidapp.rewamp.advance.repay.view;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.rewamp.advance.repay.presenter.IVerifyJazzRepaymentActivityPresenter;
import com.tez.androidapp.rewamp.advance.repay.presenter.VerifyJazzRepaymentActivityPresenter;

public class VerifyJazzRepaymentActivity  extends VerifyEasyPaisaRepaymentActivity implements IVerifyEasyPaisaRepaymentActivityView {
    private static final int JAZZ_PIN_LENGTH = 4;

    private IVerifyJazzRepaymentActivityPresenter iVerifyJazzRepaymentActivityPresenter;

    public VerifyJazzRepaymentActivity() {
        iVerifyJazzRepaymentActivityPresenter = new VerifyJazzRepaymentActivityPresenter(this);
    }

    @Override
    protected void init() {
        pinViewOTP.setOnPinEnteredListener(this);
        initViews();
    }

    @Override
    protected void makeRepayment(@NonNull String pin, @Nullable Location location) {
        iVerifyJazzRepaymentActivityPresenter.initiateRepayment(pin, getMobileAccountIdFromIntent(),
                getRepayAmountFromIntent(), location, getLoanIdFromIntent());
    }

    @Override
    protected int getPinLength() {
        return JAZZ_PIN_LENGTH;
    }
}
