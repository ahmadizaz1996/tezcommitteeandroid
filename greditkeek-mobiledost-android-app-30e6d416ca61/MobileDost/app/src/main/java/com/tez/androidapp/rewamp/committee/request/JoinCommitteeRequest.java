package com.tez.androidapp.rewamp.committee.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class JoinCommitteeRequest extends BaseRequest {
    public static final String METHOD_NAME = "http://3.21.127.35/api/v1/committee/join";

    public int inviteId;
    public int commiteeId;

    public int getInviteId() {
        return inviteId;
    }

    public void setInviteId(int inviteId) {
        this.inviteId = inviteId;
    }

    public int getCommiteeId() {
        return commiteeId;
    }

    public void setCommiteeId(int commiteeId) {
        this.commiteeId = commiteeId;
    }
}
