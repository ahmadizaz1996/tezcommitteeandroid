package com.tez.androidapp.rewamp.general.beneficiary.interactor;

import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetActiveInsurancePolicyDetailsCallback;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.MobileUserPolicyDetailsDto;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.IPolicyDetailActivityInteractorOutput;

public class PolicyDetailActivityInteractor implements IPolicyDetailActivityInteractor {

    private final IPolicyDetailActivityInteractorOutput iPolicyDetailActivityInteractorOutput;

    public PolicyDetailActivityInteractor(IPolicyDetailActivityInteractorOutput iPolicyDetailActivityInteractorOutput) {
        this.iPolicyDetailActivityInteractorOutput = iPolicyDetailActivityInteractorOutput;
    }

    @Override
    public void getPolicyDetails(int policyId) {
        BimaCloudDataStore.getInstance().getInsurancePolicyDetails(policyId,
                new GetActiveInsurancePolicyDetailsCallback() {
                    @Override
                    public void onGetActiveInsurancePolicyDetailsSuccess(MobileUserPolicyDetailsDto mobileUserPolicyDetailsDto) {
                        iPolicyDetailActivityInteractorOutput.onGetActiveInsurancePolicyDetailsSuccess(mobileUserPolicyDetailsDto);
                    }

                    @Override
                    public void onGetActiveInsurancePolicyDetailsFailure(int errorCode, String message) {
                        if (Utility.isUnauthorized(errorCode))
                            getPolicyDetails(policyId);
                        else
                            iPolicyDetailActivityInteractorOutput.onGetActiveInsurancePolicyDetailsFailure(errorCode, message);

                    }
                });
    }
}
