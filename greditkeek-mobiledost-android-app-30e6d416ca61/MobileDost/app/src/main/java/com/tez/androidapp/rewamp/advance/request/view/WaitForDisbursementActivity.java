package com.tez.androidapp.rewamp.advance.request.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.rewamp.advance.AdvanceStatus;
import com.tez.androidapp.rewamp.advance.request.router.LoanReceiptActivityRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.general.timer.TimerActivity;

public class WaitForDisbursementActivity extends TimerActivity {

    private final BroadcastReceiver disbursementPushNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String status = intent.getStringExtra(PushNotificationConstants.LOAN_DISBURSEMENT_STATUS);

            if (status != null) {

                AdvanceStatus advanceStatus = AdvanceStatus.valueOf(status);

                switch (advanceStatus) {

                    case LOAN_DISBURSED:
                        LoanReceiptActivityRouter.createInstance().setDependenciesAndRoute(WaitForDisbursementActivity.this);
                        break;

                    case LOAN_DISBURSEMENT_FAILED:
                    case LOAN_DISBURSEMENT_PENDING:
                    default:
                        routeToDashboard();
                        break;
                }
            } else
                routeToDashboard();

            stopTimer();

            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tvMessage.setText(R.string.please_wait_we_are_depositing_money_in_your_wallet);
        tvPrecautionNote.setText(R.string.please_make_sure_message_for_disbursement);
        registerReceiver(disbursementPushNotificationReceiver, new IntentFilter(PushNotificationConstants.BROADCAST_TYPE_LOAN_DISBURSEMENT));
        startTimer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(disbursementPushNotificationReceiver);
        } catch (IllegalArgumentException e){
            //ignore
        }
    }

    @Override
    protected long getTotalTime() {
        return 45000; // 45-seconds in milliseconds
    }

    @Override
    protected void onTimerFinish() {
        routeToDashboard();
    }

    private void routeToDashboard() {
        DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected String getScreenName() {
        return "WaitForDisbursementActivity";
    }
}
