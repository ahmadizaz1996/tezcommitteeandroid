package com.tez.androidapp.rewamp.committee.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeMetadataLIstener;
import com.tez.androidapp.rewamp.committee.listener.JoinCommitteeInvitationListener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class GetInvitedCommitteePackageRH extends BaseRH<GetInvitedPackageResponse> {

    private final JoinCommitteeInvitationListener listener;

    public GetInvitedCommitteePackageRH(BaseCloudDataStore baseCloudDataStore, JoinCommitteeInvitationListener joinCommitteeInvitationListener) {
        super(baseCloudDataStore);
        this.listener = joinCommitteeInvitationListener;
    }

    @Override
    protected void onSuccess(Result<GetInvitedPackageResponse> value) {
        GetInvitedPackageResponse getInvitedPackageResponse = value.response().body();
        if (getInvitedPackageResponse != null && getInvitedPackageResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onGetInvitedPackageSuccess(getInvitedPackageResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onGetInvitedPackageFailure(errorCode, message);
    }
}
