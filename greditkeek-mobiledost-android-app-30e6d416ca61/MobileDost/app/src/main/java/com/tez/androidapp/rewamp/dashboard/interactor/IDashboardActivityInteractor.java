package com.tez.androidapp.rewamp.dashboard.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeLoginRequest;

public interface IDashboardActivityInteractor extends IBaseInteractor {

    void getDashboardAdvanceCard();

    void getDashboardActionCard();

    void getUserProfileStatus();

    void getCommitteeMetada();

    void loginCall(CommitteeLoginRequest committeeLoginRequest);

}
