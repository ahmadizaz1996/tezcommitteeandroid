package com.tez.androidapp.rewamp.committee.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.emoji.widget.EmojiAppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.login.LoginManager;
import com.google.android.gms.vision.text.Line;
import com.google.api.Distribution;
import com.tez.androidapp.R;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.extract.ExtractedContact;
import com.tez.androidapp.commons.utils.app.Log;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezCheckBox;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.response.ChatMessage;

import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeGroupChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int INCOMING_MESSAGE_ITEM = 0;
    public static final int OUTGOING_MESSAGE_ITEM = 1;
    private final List<ChatMessage> listItems = new ArrayList<>();
    private final ChatMessageListener listener;
    private ArrayList<ChatMessage> originalList = new ArrayList<>();
    HashMap<String, ExtractedContact> extractedContactHashMap = new HashMap<>();


    public CommitteeGroupChatAdapter(@NonNull List<ChatMessage> items, @Nullable ChatMessageListener listener) {
        this.listItems.addAll(items);
        this.originalList.addAll(this.listItems);
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == INCOMING_MESSAGE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.incoming_message_row_layout, parent, false);
            return new IncomingChatMessageViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.outgoing_message_row_layout, parent, false);
            return new OutgoingChatMessageViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (listItems.get(position).getUserId().equals(Long.valueOf(MDPreferenceManager.getUser().getId())))
            return OUTGOING_MESSAGE_ITEM;
        else
            return INCOMING_MESSAGE_ITEM;
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ChatMessage item = listItems.get(position);

        if (holder instanceof IncomingChatMessageViewHolder) {
            try {
                IncomingChatMessageViewHolder incomingChatMessageViewHolder = (IncomingChatMessageViewHolder) holder;
                incomingChatMessageViewHolder.messageTextView.setText(item.getText());
                incomingChatMessageViewHolder.messageUserNametv.setText(item.getText());
                incomingChatMessageViewHolder.dateTextView.setText(item.getDate().split("T")[1]);


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            OutgoingChatMessageViewHolder outgoingChatMessageViewHolder = (OutgoingChatMessageViewHolder) holder;
            outgoingChatMessageViewHolder.messageTextView.setText(item.getText());
            outgoingChatMessageViewHolder.dateTextView.setText(/*item.getDate().split("T")[1]*/ getFormatedDateTime(item.getDate().split("T")[1], "HH:mm:ss", "hh:mm a"));
        }
    }

    public static String getFormatedDateTime(String dateStr, String strReadFormat, String strWriteFormat) {

        String formattedDate = dateStr;

        DateFormat readFormat = new SimpleDateFormat(strReadFormat, Locale.getDefault());
        DateFormat writeFormat = new SimpleDateFormat(strWriteFormat, Locale.getDefault());

        Date date = null;

        try {
            date = readFormat.parse(dateStr);
        } catch (ParseException e) {
        }

        if (date != null) {
            formattedDate = writeFormat.format(date);
        }

        return formattedDate;
    }


    public interface ChatMessageListener extends BaseRecyclerViewListener {
        void onClickMessage();
    }

    static class IncomingChatMessageViewHolder extends BaseViewHolder<ChatMessage, ChatMessageListener> {

        @BindView(R.id.incomingMemberIv)
        TezImageView incomingMemberIv;

        @BindView(R.id.textLayout)
        LinearLayout textLayout;

        @BindView(R.id.messageUserNametv)
        TezTextView messageUserNametv;

        @BindView(R.id.messageTextView)
        TezTextView messageTextView;

        @BindView(R.id.datetv)
        TezTextView dateTextView;


        public IncomingChatMessageViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(ChatMessage item, @Nullable ChatMessageListener listener) {
            super.onBind(item, listener);


        }
    }

    static class OutgoingChatMessageViewHolder extends BaseViewHolder<ChatMessage, ChatMessageListener> {

        @BindView(R.id.textLayout)
        LinearLayout textLayout;

        @BindView(R.id.messageUserNametv)
        TezTextView messageUserNametv;

        @BindView(R.id.messageTextView)
        TezTextView messageTextView;

        @BindView(R.id.outgoingMemberIv)
        TezImageView outgoingMemberIv;

        @BindView(R.id.datetv)
        TezTextView dateTextView;


        public OutgoingChatMessageViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(ChatMessage item, @Nullable ChatMessageListener listener) {
            super.onBind(item, listener);
        }
    }
}
