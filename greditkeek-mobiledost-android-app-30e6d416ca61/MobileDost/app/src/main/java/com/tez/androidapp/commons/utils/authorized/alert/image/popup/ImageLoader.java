package com.tez.androidapp.commons.utils.authorized.alert.image.popup;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by FARHAN DHANANI on 7/8/2018.
 */
public interface ImageLoader {
    void setUI(@NonNull UIForAlertImage uiForAlertImage);
    void setUrlToLoad(@NonNull Uri urlToLoad);
    void setSignatureToFile(@Nullable String signature);
    void loadImage();
    default String getSignature(){
        return null;
    }
}
