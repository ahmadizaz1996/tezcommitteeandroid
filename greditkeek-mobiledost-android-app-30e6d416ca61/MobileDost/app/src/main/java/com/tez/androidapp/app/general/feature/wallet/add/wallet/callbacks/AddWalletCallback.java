package com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks;

import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created  on 2/14/2017.
 */

public interface AddWalletCallback {

    void onAddWalletSuccess(BaseResponse baseResponse);

    void onAddWalletFailure(int errorCode, @Nullable String message);
}
