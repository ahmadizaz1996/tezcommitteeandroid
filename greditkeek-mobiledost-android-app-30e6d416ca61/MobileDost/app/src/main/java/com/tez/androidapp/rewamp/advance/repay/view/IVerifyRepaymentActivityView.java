package com.tez.androidapp.rewamp.advance.repay.view;

public interface IVerifyRepaymentActivityView extends IVerifyEasyPaisaRepaymentActivityView {

    void startTimer();

    void setTvResendCodeEnabled(boolean enabled);
}
