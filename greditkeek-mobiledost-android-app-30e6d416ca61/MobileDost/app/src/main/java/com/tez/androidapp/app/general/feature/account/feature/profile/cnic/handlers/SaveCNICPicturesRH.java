package com.tez.androidapp.app.general.feature.account.feature.profile.cnic.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.cnic.callbacks.SaveCNICPicturesCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network.dto.response.SaveCNICPicturesResponse;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by VINOD KUMAR on 8/28/2018.
 */
public class SaveCNICPicturesRH extends BaseRH<SaveCNICPicturesResponse> {

    private SaveCNICPicturesCallback saveCNICPicturesCallback;

    public SaveCNICPicturesRH(BaseCloudDataStore baseCloudDataStore, @Nullable SaveCNICPicturesCallback saveCNICPicturesCallback) {
        super(baseCloudDataStore);
        this.saveCNICPicturesCallback = saveCNICPicturesCallback;
    }

    @Override
    protected void onSuccess(Result<SaveCNICPicturesResponse> value) {
        SaveCNICPicturesResponse saveCNICPicturesResponse = value.response().body();
        if (saveCNICPicturesResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            if (saveCNICPicturesResponse.areAllFilesPersisted()) {

            }
            if (saveCNICPicturesCallback != null)
                saveCNICPicturesCallback.onSaveCNICPicturesSuccess(saveCNICPicturesResponse);
        } else {
            onFailure(saveCNICPicturesResponse.getStatusCode(), saveCNICPicturesResponse.getErrorDescription());
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (saveCNICPicturesCallback != null)
            saveCNICPicturesCallback.onSaveCNICPicturesFailure(errorCode, message);
    }
}
