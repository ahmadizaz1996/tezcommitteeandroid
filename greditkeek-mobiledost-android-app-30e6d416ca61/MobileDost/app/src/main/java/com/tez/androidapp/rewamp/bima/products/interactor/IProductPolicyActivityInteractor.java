package com.tez.androidapp.rewamp.bima.products.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

public interface IProductPolicyActivityInteractor extends IBaseInteractor {

    void getProductPolicy(int policyId);
}
