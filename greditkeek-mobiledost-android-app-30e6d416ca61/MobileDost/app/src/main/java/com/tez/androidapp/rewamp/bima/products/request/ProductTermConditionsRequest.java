package com.tez.androidapp.rewamp.bima.products.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class ProductTermConditionsRequest extends BaseRequest {

    public static final String METHOD_NAME = "/api/v1/insurance/{" + Params.PRODUCT_ID + "}/termconditions";

    public static final class Params {

        public static final String PRODUCT_ID = "productId";
    }
}
