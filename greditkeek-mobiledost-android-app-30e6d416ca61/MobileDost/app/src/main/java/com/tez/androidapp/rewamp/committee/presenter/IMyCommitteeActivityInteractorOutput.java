package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.listener.CommitteeInviteListener;
import com.tez.androidapp.rewamp.committee.listener.MyCommitteeListener;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface IMyCommitteeActivityInteractorOutput extends MyCommitteeListener {
}
