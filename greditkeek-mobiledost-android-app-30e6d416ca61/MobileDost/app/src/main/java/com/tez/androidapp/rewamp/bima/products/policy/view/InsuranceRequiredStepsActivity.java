package com.tez.androidapp.rewamp.bima.products.policy.view;

import android.os.Bundle;
import android.view.View;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.advance.request.view.LoanRequiredStepsActivity;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.InsuranceRequiredStepsActivityPresenter;
import com.tez.androidapp.rewamp.profile.router.CompleteProfileRouter;
import com.tez.androidapp.rewamp.profile.trust.router.ProfileTrustActivityRouter;

public class InsuranceRequiredStepsActivity extends LoanRequiredStepsActivity {


    public InsuranceRequiredStepsActivity() {
        this.iLoanRequiredStepsActivityPresenter = new InsuranceRequiredStepsActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toolbar.setToolbarTitle(R.string.complete_cnic);
        tvLoanReadyToDisburse.setVisibility(View.GONE);
        tvCompleteSteps.setText(R.string.required_steps_to_buy_insurance);
        cvCompleteProfile.setStepDescription(R.string.complete_cnic);
    }

    @Override
    public void routeToCompleteProfile() {
        ProfileTrustActivityRouter.createInstance().setDependenciesAndRoute(this, CompleteProfileRouter.ROUTE_TO_INSURANCE);
    }

    @Override
    protected String getScreenName() {
        return "InsuranceRequiredStepsActivity";
    }
}
