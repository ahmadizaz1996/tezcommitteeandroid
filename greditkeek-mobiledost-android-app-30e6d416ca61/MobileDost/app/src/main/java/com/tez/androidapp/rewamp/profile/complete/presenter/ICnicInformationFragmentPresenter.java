package com.tez.androidapp.rewamp.profile.complete.presenter;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.UserProfile;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateProfileWithCnicRequest;
import com.tez.androidapp.commons.models.app.City;
import com.tez.androidapp.commons.models.network.User;

public interface ICnicInformationFragmentPresenter extends ICompleteProfileCnicUploadActivityPresenter{
    void getProfile();

    void updateUserProfile(UserProfile userProfile,User user,
                           UpdateProfileWithCnicRequest updateProfileWithCnicRequest,
                           final boolean validationsPartiallyPassed,  City selectedPlaceOfBirthCity,
                           City selectedCurrentCity, AnswerSelected selectedMaritalStatus);

    boolean isMaritalStatusEquals();

    void updateUserProfile(UserProfile userProfile, User user,
                           UpdateProfileWithCnicRequest updateProfileWithCnicRequest, City selectedPlaceOfBirthCity,
                           City selectedCurrentCity, AnswerSelected selectedMaritalStatus);

    boolean userHasEditedAnyFeilds(User user, UpdateProfileWithCnicRequest updateProfileWithCnicRequest);

    void handleEditableFeilds(User user);
}
