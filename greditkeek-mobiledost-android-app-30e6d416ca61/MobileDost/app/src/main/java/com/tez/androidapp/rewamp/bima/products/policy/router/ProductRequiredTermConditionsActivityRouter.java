package com.tez.androidapp.rewamp.bima.products.policy.router;

import android.content.Intent;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.view.ProductRequiredTermConditionsActivity;
import com.tez.androidapp.rewamp.bima.products.router.ProductTermConditionsActivityRouter;

public class ProductRequiredTermConditionsActivityRouter extends ProductTermConditionsActivityRouter {

    private static final String PRODUCT_DETAILS = "product_details";
    private static final String WALLET_ID = "wallet_id";
    private static final String TITLE = "title";


    public static ProductRequiredTermConditionsActivityRouter createInstance() {
        return new ProductRequiredTermConditionsActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, int productId, String title) {
        Intent intent = createIntent(from);
        intent.putExtra(PRODUCT_ID, productId);
        intent.putExtra(TITLE, title);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        InitiatePurchasePolicyRequest initiatePurchasePolicyRequest,
                                        int productId,
                                        int walletServiceProviderId) {
        Intent intent = createIntent(from);
        intent.putExtra(PRODUCT_DETAILS, (Parcelable) initiatePurchasePolicyRequest);
        intent.putExtra(WALLET_ID, walletServiceProviderId);
        intent.putExtra(PRODUCT_ID, productId);
        route(from, intent);
    }

    @Override
    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, ProductRequiredTermConditionsActivity.class);
    }

    public static Dependencies getDependencies(@NonNull Intent intent) {
        return new Dependencies(intent);
    }

    public static class Dependencies extends ProductTermConditionsActivityRouter.Dependencies {


        private Dependencies(@NonNull Intent intent) {
            super(intent);
        }


        @Nullable
        public InitiatePurchasePolicyRequest getProductDetails() {
            return intent.getParcelableExtra(PRODUCT_DETAILS);
        }

        public int getWalletProviderId() {
            return intent.getIntExtra(WALLET_ID, Constants.EASYPAISA_ID);
        }

        public String getTitle() {
            return intent.getStringExtra(TITLE);
        }
    }
}
