package com.tez.androidapp.rewamp.bima.products.view;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.fragments.BaseFragment;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.rewamp.bima.products.adapter.ProductsAdapter;
import com.tez.androidapp.rewamp.bima.products.model.InsuranceProduct;
import com.tez.androidapp.rewamp.bima.products.policy.router.BaseProductActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.CoronaDefenseActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.DigitalOpdActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.HospitalCoverageActivityRouter;
import com.tez.androidapp.rewamp.bima.products.router.ProductPolicyActivityRouter;
import com.tez.androidapp.rewamp.bima.products.viewmodel.ProductsViewModel;
import com.tez.androidapp.rewamp.general.network.model.NetworkCall;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class ProductsFragment extends BaseFragment implements ProductsAdapter.ProductListener {

    @BindView(R.id.rvProducts)
    private RecyclerView rvProducts;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    @BindView(R.id.shimmer)
    private TezLinearLayout shimmer;

    @Nullable
    private View baseView;

    private InsuranceProductsAndPolicyListCallback callback;

    static ProductsFragment newInstance() {
        return new ProductsFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.callback = (InsuranceProductsAndPolicyListCallback) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (baseView == null) {
            baseView = inflater.inflate(R.layout.products_fragment, container, false);
            ViewBinder.bind(this, baseView);
        }
        return baseView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        callback.setIllustrationVisibility(View.GONE);
        ProductsViewModel model = new ViewModelProvider(requireActivity()).get(ProductsViewModel.class);
        model.getInsuranceProductListLiveData().observe(getViewLifecycleOwner(), productList -> {
            if (rvProducts.getAdapter() == null) {
                ProductsAdapter adapter = new ProductsAdapter(productList, this);
                rvProducts.setAdapter(adapter);
            }
        });
        model.getNetworkCallMutableLiveData().observe(getViewLifecycleOwner(), networkCall -> {
            NetworkCall.State state = networkCall.getState();
            switch (state) {

                case LOADING:
                    shimmer.setVisibility(View.VISIBLE);
                    clContent.setVisibility(View.GONE);
                    break;

                case FAILURE:
                    shimmer.setVisibility(View.GONE);
                    clContent.setVisibility(View.GONE);
                    showError(networkCall.getErrorCode());
                    break;

                case SUCCESS:
                    shimmer.setVisibility(View.GONE);
                    clContent.setVisibility(View.VISIBLE);
                    break;
            }
        });
    }

    @Override
    public void onPurchaseProduct(@NonNull InsuranceProduct insuranceProduct) {
        getBaseActivity(activity -> {

            BaseProductActivityRouter router = null;

            switch (insuranceProduct.getId()) {

                case Constants.HOSPITAL_COVERAGE_PLAN_ID:
                    router = HospitalCoverageActivityRouter.createInstance();
                    break;

                case Constants.DIGITAL_OPD_PLAN_ID:
                    router = DigitalOpdActivityRouter.createInstance();
                    break;

                case Constants.CORONA_DEFENSE_PLAN_ID:
                    router = CoronaDefenseActivityRouter.createInstance();
            }

            if (router != null) {
                router.setDependenciesAndRoute(activity, insuranceProduct.getTitle());
            }
        });
    }

    @Override
    public void onGetProductDetails(@NonNull InsuranceProduct insuranceProduct) {
        getBaseActivity(baseActivity -> ProductPolicyActivityRouter
                .createInstance()
                .setDependenciesAndRoute(baseActivity, insuranceProduct.getPolicyId(), insuranceProduct.getId(), insuranceProduct.getTitle()));
    }

    @Override
    public void onClickTezInsurance(@NonNull InsuranceProduct insuranceProduct) {

    }
}
