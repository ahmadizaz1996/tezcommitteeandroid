package com.tez.androidapp.rewamp.bima.products.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.bima.products.view.InsuranceProductsAndPolicyListActivity;

public class InsuranceProductsAndPolicyListActivityRouter extends BaseActivityRouter {

    public static InsuranceProductsAndPolicyListActivityRouter createInstance() {
        return new InsuranceProductsAndPolicyListActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, new Intent(from, InsuranceProductsAndPolicyListActivity.class));
    }
}
