package com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created  on 7/21/2017.
 */

public class InitiateRepaymentResponse extends BaseResponse {

    private Integer repaymentId;
    private String repaymentCorrelationId;

    public Integer getRepaymentId() {
        return repaymentId;
    }

    public String getRepaymentCorrelationId() {
        return repaymentCorrelationId;
    }
}
