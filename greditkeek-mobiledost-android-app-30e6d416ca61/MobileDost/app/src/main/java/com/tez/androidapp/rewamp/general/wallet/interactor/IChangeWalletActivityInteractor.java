package com.tez.androidapp.rewamp.general.wallet.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

public interface IChangeWalletActivityInteractor extends IBaseInteractor {

    void getAllWallets();
}
