package com.tez.androidapp.app.base.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.util.DialogUtil;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener;


/**
 * Created by VINOD KUMAR on 5/13/2019.
 */
public interface IBaseView {

    default void showInformativeMessage(@StringRes int stringRes) {
        Optional.ifPresent(getHostActivity(), activity -> {
            showInformativeMessage(activity.getString(stringRes));
        });
    }

    default void showError(int errorCode) {
        showError(errorCode, (DoubleTapSafeDialogClickListener) null);
    }

    default void showError(int errorCode, DoubleTapSafeDialogClickListener listener) {
        Optional.ifPresent(getHostActivity(), activity -> {
            String description = activity.getString(ResponseStatusCode.getDescriptionFromErrorCode(errorCode));
            showError(errorCode, description, listener);
        });
    }

    default void showError(int errorCode, String message) {
        showError(errorCode, message, null);
    }

    default void showError(int errorCode, String message, DoubleTapSafeDialogClickListener listener) {
        Optional.ifPresent(getHostActivity(), activity -> {
            String error = activity.getString(R.string.error);
            if (errorCode != 0)
                error = error + ": " + errorCode;
            DialogUtil.showInformativeDialog(activity, error, message, listener);
        });
    }

    default void showInformativeMessage(@NonNull String message) {
        Optional.ifPresent(getHostActivity(), baseActivity -> {
            DialogUtil.showInformativeMessage(baseActivity, message);
        });
    }

    default void showTezLoader() {
        TezLoader tezLoader = getInitializedLoader();
        if (tezLoader != null && !tezLoader.isShowing())
            tezLoader.show();
    }

    default void dismissTezLoader() {
        TezLoader tezLoader = getInitializedLoader();
        if (tezLoader != null && tezLoader.isShowing())
            tezLoader.dismiss();
    }

    default void setTezLoaderToBeDissmissedOnTransition() {
        TezLoader tezLoader = getInitializedLoader();
        if (tezLoader != null)
            tezLoader.setDissmissOnTransition(true);
    }

    default void showInformativeMessage(@StringRes int stringRes, DoubleTapSafeDialogClickListener doubleTapSafeOnClickListener) {
        Optional.ifPresent(getHostActivity(), baseActivity -> {
            showInformativeMessage(baseActivity.getString(stringRes), doubleTapSafeOnClickListener);
        });
    }

    default void showInformativeMessage(@NonNull String message, DoubleTapSafeDialogClickListener doubleTapSafeOnClickListener) {
        Optional.ifPresent(getHostActivity(), baseActivity -> {
            DialogUtil.showInformativeMessage(baseActivity, message, doubleTapSafeOnClickListener);
        });
    }

    void getCurrentLocation(@NonNull LocationAvailableCallback callback);

    @Nullable
    BaseActivity getHostActivity();

    @Nullable
    default TezLoader getInitializedLoader() {
        return null;
    }

    void showInformativeDialog(@StringRes int title, @StringRes int desc);

    void showInformativeDialog(@StringRes int title, @StringRes int desc,
                               DoubleTapSafeDialogClickListener doubleTapSafeDialogClickListener);

    default void finishActivity() {

    }
}
