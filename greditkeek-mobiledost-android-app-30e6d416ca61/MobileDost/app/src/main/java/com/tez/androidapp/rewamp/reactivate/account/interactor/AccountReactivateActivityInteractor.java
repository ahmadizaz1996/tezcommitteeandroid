package com.tez.androidapp.rewamp.reactivate.account.interactor;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.UserReActivateAccountOTPCallback;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.UserReActivateAccountResendOTPCallback;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.reactivate.account.presenter.IAccountReactivateActivityInteractorOutput;

public class AccountReactivateActivityInteractor implements IAccountReactivateActivityInteractor, UserReActivateAccountResendOTPCallback {

    private final IAccountReactivateActivityInteractorOutput iAccountReactivateActivityInteractorOutput;

    public AccountReactivateActivityInteractor(IAccountReactivateActivityInteractorOutput iAccountReactivateActivityInteractorOutput) {
        this.iAccountReactivateActivityInteractorOutput = iAccountReactivateActivityInteractorOutput;
    }

    @Override
    public void resendAccountReactivationOtp(){
        UserAuthCloudDataStore.getInstance().userReActivateAccountResendOTP(this);
    }

    @Override
    public void verifyAccountOtp(final String otp){
        UserAuthCloudDataStore.getInstance().userReActivateAccount(otp, new UserReActivateAccountOTPCallback() {
            @Override
            public void onUserAccountReActivatedSuccess(BaseResponse baseResponse) {
                iAccountReactivateActivityInteractorOutput.onUserAccountReActivatedSuccess(baseResponse);
            }

            @Override
            public void onUserAccountReActivatedFailure(BaseResponse baseResponse) {
                if(Utility.isUnauthorized(baseResponse.getStatusCode())){
                    verifyAccountOtp(otp);
                } else{
                    iAccountReactivateActivityInteractorOutput.onUserAccountReActivatedFailure(baseResponse);
                }
            }
        });
    }

    @Override
    public void onUserAccountReActivateResendOTPSuccess(BaseResponse baseResponse) {
        this.iAccountReactivateActivityInteractorOutput.onUserAccountReActivateResendOTPSuccess(baseResponse);
    }

    @Override
    public void onUserAccountReActivateResendOTPFailure(BaseResponse baseResponse) {
        if (Utility.isUnauthorized(baseResponse.getStatusCode())){
            resendAccountReactivationOtp();
        } else {
            this.iAccountReactivateActivityInteractorOutput.onUserAccountReActivateResendOTPFailure(baseResponse);
        }
    }
}
