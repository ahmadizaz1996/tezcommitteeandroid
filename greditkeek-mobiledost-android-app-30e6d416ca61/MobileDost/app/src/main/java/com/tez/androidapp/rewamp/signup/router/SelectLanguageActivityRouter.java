package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.SelectLanguageActivity;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class SelectLanguageActivityRouter extends BaseActivityRouter {

    public static SelectLanguageActivityRouter createInstance() {
        return new SelectLanguageActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull AppCompatActivity from) {
        from.startActivity(createIntent(from));
    }

    private Intent createIntent(@NonNull AppCompatActivity from) {
        return new Intent(from, SelectLanguageActivity.class);
    }
}
