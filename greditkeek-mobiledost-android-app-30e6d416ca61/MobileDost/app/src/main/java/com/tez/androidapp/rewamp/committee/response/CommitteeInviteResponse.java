package com.tez.androidapp.rewamp.committee.response;

import com.tez.androidapp.app.base.response.BaseResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeInviteResponse extends BaseResponse implements Serializable {

    public String message;
    public Data data;

    public class Invite implements Serializable {
        public String email;
        public String mobilNumber;
        public String status;
    }

    public class Data implements Serializable {
        public int id;
        public String name;
        public int tenor;
        public int frequency;
        public int amount;
        public Date startDate;
        public ArrayList<Invite> invites;
    }
}
