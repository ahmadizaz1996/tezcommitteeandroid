package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.ConfirmMobileNumberCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 3/22/2017.
 */

public class ConfirmMobileNumberRH extends BaseRH<BaseResponse> {

    ConfirmMobileNumberCallback confirmMobileNumberCallback;

    public ConfirmMobileNumberRH(BaseCloudDataStore baseCloudDataStore, ConfirmMobileNumberCallback
            confirmMobileNumberCallback) {
        super(baseCloudDataStore);
        this.confirmMobileNumberCallback = confirmMobileNumberCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            confirmMobileNumberCallback.onConfirmMobileNumberSuccess();
        } else {
            onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        confirmMobileNumberCallback.onConfirmMobileNumberFailure(message);
    }
}
