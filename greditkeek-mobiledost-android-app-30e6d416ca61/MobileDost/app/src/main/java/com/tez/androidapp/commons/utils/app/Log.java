package com.tez.androidapp.commons.utils.app;

import com.tez.androidapp.BuildConfig;

/**
 * Created  on 12/14/2016.
 */

public class Log {

    private static final String LOG_TAG = "MobileDostLogger";

    public static void e(String tag, String message) {
        if (BuildConfig.IS_DEBUG ) android.util.Log.e(tag == null ? LOG_TAG : tag, message == null ? "null" :
                message);
    }

    public static void e(String message) {
        if (BuildConfig.IS_DEBUG) android.util.Log.e(LOG_TAG, message == null ? "null" : message);
    }

    public static void e(String tag, Throwable e) {
        if (BuildConfig.IS_DEBUG) android.util.Log.e(tag, "Error", e);
    }


    public static void e(Throwable e) {
        if (BuildConfig.IS_DEBUG) android.util.Log.e(LOG_TAG, "Error", e);
    }

    public static void d(String tag, String message) {
        if (BuildConfig.IS_DEBUG) android.util.Log.d(tag == null ? LOG_TAG : tag, message == null ? "null" :
                message);
    }

    public static void d(String message) {
        if (BuildConfig.IS_DEBUG) android.util.Log.d(LOG_TAG, message == null ? "null" : message);
    }

    public static void w(String tag, String message) {
        if (BuildConfig.IS_DEBUG) android.util.Log.w(tag == null ? LOG_TAG : tag, message == null ? "null" :
                message);
    }

    public static void w(String message) {
        if (BuildConfig.IS_DEBUG) android.util.Log.w(LOG_TAG, message == null ? "null" : message);
    }

    public static void i(String tag, String message) {
        if (BuildConfig.IS_DEBUG) android.util.Log.i(tag == null ? LOG_TAG : tag, message == null ? "null" :
                message);
    }

    public static void i(String message) {
        if (BuildConfig.IS_DEBUG) android.util.Log.i(LOG_TAG, message == null ? "null" : message);
    }

    public static void v(String tag, String message) {
        if (BuildConfig.IS_DEBUG) android.util.Log.v(tag == null ? LOG_TAG : tag, message == null ? "null" :
                message);
    }

    public static void v(String message) {
        if (BuildConfig.IS_DEBUG) android.util.Log.v(LOG_TAG, message == null ? "null" : message);
    }

    public static void wtf(String tag, String message) {
        if (BuildConfig.IS_DEBUG) android.util.Log.wtf(tag == null ? LOG_TAG : tag, message == null ? "null" :
                message);
    }

    public static void wtf(String message) {
        if (BuildConfig.IS_DEBUG) android.util.Log.wtf(LOG_TAG, message == null ? "null" : message);
    }


}
