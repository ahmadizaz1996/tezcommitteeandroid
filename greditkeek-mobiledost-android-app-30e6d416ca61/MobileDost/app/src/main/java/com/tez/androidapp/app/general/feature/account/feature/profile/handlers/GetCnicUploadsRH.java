package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetCnicUploadsCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.GetCnicUploadsResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.UpdateUserBasicProfileResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

public class GetCnicUploadsRH extends BaseRH<GetCnicUploadsResponse> {

    private final GetCnicUploadsCallback getCnicUploadsCallback;

    public GetCnicUploadsRH(BaseCloudDataStore baseCloudDataStore,
                            GetCnicUploadsCallback getCnicUploadsCallback) {
        super(baseCloudDataStore);
        this.getCnicUploadsCallback = getCnicUploadsCallback;
    }

    @Override
    protected void onSuccess(Result<GetCnicUploadsResponse> value) {
        GetCnicUploadsResponse getCnicUploadsResponse = value.response().body();
        if (getCnicUploadsResponse!=null && getCnicUploadsResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            getCnicUploadsCallback.onGetCnicUploadsCallbackSuccess(getCnicUploadsResponse.getUserProfile());
        else onFailure(getCnicUploadsResponse.getStatusCode(),
                getCnicUploadsResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        getCnicUploadsCallback.onGetCnicUploadsCallbackFailure(errorCode, message);
    }
}
