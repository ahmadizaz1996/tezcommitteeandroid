package com.tez.androidapp.rewamp.profile.complete.presenter;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.PersonalQuestionCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdatePersonalInfoCallback;

public interface IPersonalInformationFragmentInteractorOutput
        extends PersonalQuestionCallback, UpdatePersonalInfoCallback {
}
