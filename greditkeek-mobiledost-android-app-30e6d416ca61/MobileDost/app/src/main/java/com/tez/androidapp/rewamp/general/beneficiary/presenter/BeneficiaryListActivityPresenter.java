package com.tez.androidapp.rewamp.general.beneficiary.presenter;

import android.view.View;

import com.tez.androidapp.app.vertical.bima.add.beneficiary.models.network.dto.request.AdvanceBimaBeneficiaryRequest;
import com.tez.androidapp.rewamp.general.beneficiary.entity.Beneficiary;
import com.tez.androidapp.rewamp.general.beneficiary.interactor.BeneficiaryListActivityInteractor;
import com.tez.androidapp.rewamp.general.beneficiary.interactor.IBeneficiaryListActivityInteractor;
import com.tez.androidapp.rewamp.general.beneficiary.view.IBeneficiaryListActivityView;

import java.util.List;

public class BeneficiaryListActivityPresenter implements IBeneficiaryListActivityPresenter, IBeneficiaryListActivityInteractorOutput {

    private final IBeneficiaryListActivityView iBeneficiaryListActivityView;
    private final IBeneficiaryListActivityInteractor iBeneficiaryListActivityInteractor;

    public BeneficiaryListActivityPresenter(IBeneficiaryListActivityView iBeneficiaryListActivityView) {
        this.iBeneficiaryListActivityView = iBeneficiaryListActivityView;
        iBeneficiaryListActivityInteractor = new BeneficiaryListActivityInteractor(this);
    }

    @Override
    public void getBeneficiaries() {
        iBeneficiaryListActivityView.setClContentVisibility(View.GONE);
        iBeneficiaryListActivityView.showShimmer();
        iBeneficiaryListActivityInteractor.getBeneficiaries();
    }

    @Override
    public void onGetBeneficiariesSuccess(List<Beneficiary> beneficiaries) {
        iBeneficiaryListActivityView.hideShimmer();
        iBeneficiaryListActivityView.initAdapter(beneficiaries);
        iBeneficiaryListActivityView.setClContentVisibility(View.VISIBLE);
    }

    @Override
    public void onGetBeneficiariesFailure(int errorCode, String message) {
        iBeneficiaryListActivityView.hideShimmer();
        iBeneficiaryListActivityView.showError(errorCode,
                (dialog, which) -> iBeneficiaryListActivityView.finishActivity());
    }

    @Override
    public void setAdvanceBimaBeneficiary(int beneficiaryId, int mobileUserInsurancePolicyBeneficiaryId) {
        iBeneficiaryListActivityView.showTezLoader();
        AdvanceBimaBeneficiaryRequest request = new AdvanceBimaBeneficiaryRequest();
        request.setBeneficiaryId(beneficiaryId);
        request.setMobileUserInsurancePolicyBeneficiaryId(mobileUserInsurancePolicyBeneficiaryId);
        iBeneficiaryListActivityInteractor.setAdvanceBimaBeneficiary(request);
    }

    @Override
    public void onSetAdvanceBimaBeneficiarySuccess() {
        iBeneficiaryListActivityView.setTezLoaderToBeDissmissedOnTransition();
        iBeneficiaryListActivityView.finishActivityWithResultOk();
    }

    @Override
    public void onSetAdvanceBimaBeneficiaryFailure(int statusCode, String message) {
        iBeneficiaryListActivityView.dismissTezLoader();
        iBeneficiaryListActivityView.showError(statusCode);
    }

    @Override
    public void setDefaultBeneficiary(int beneficiaryId) {
        iBeneficiaryListActivityView.showTezLoader();
        iBeneficiaryListActivityInteractor.setDefaultBeneficiary(beneficiaryId);
    }

    @Override
    public void onSetDefaultBeneficiarySuccess() {
        iBeneficiaryListActivityView.setTezLoaderToBeDissmissedOnTransition();
        iBeneficiaryListActivityView.finishActivityWithResultOk();
    }

    @Override
    public void onSetDefaultBeneficiaryFailure(int errorCode, String message) {
        iBeneficiaryListActivityView.dismissTezLoader();
        iBeneficiaryListActivityView.showError(errorCode);
    }
}
