package com.tez.androidapp.repository.network.store;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.account.feature.beneficiary.callbacks.SetDefaultBeneficiaryCallback;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.handlers.SetDefaultBeneficiaryRH;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetAdvanceBimaBeneficiaryCallback;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.handlers.SetAdvanceBimaBeneficiaryRH;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.models.network.dto.request.AdvanceBimaBeneficiaryRequest;
import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetActiveInsurancePolicyCallback;
import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetActiveInsurancePolicyDetailsCallback;
import com.tez.androidapp.app.vertical.bima.policies.handlers.GetActiveInsurancePolicyDetailsRH;
import com.tez.androidapp.app.vertical.bima.policies.handlers.GetActiveInsurancePolicyRH;
import com.tez.androidapp.app.vertical.bima.shared.api.client.TezBimaAPI;
import com.tez.androidapp.rewamp.advance.request.callback.LoanRemainingStepsCallback;
import com.tez.androidapp.rewamp.advance.request.response.LoanRemainingStepsRH;
import com.tez.androidapp.rewamp.bima.products.callback.ProductDetailTermConditionCallback;
import com.tez.androidapp.rewamp.bima.products.callback.ProductListCallback;
import com.tez.androidapp.rewamp.bima.products.callback.ProductPolicyCallback;
import com.tez.androidapp.rewamp.bima.products.callback.ProductTermConditionsCallback;
import com.tez.androidapp.rewamp.bima.products.policy.RH.InitiatePurchasePolicyRH;
import com.tez.androidapp.rewamp.bima.products.policy.RH.InsuranceProductDetailsRH;
import com.tez.androidapp.rewamp.bima.products.policy.RH.PurchasePolicyRH;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InitiatePurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InsuranceProductDetailCallback;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.PurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.request.PurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.response.handler.ProductDetailTermConditionRH;
import com.tez.androidapp.rewamp.bima.products.response.handler.ProductListRH;
import com.tez.androidapp.rewamp.bima.products.response.handler.ProductPolicyRH;
import com.tez.androidapp.rewamp.bima.products.response.handler.ProductTermConditionsRH;
import com.tez.androidapp.rewamp.general.beneficiary.listener.GetBeneficiariesListener;
import com.tez.androidapp.rewamp.general.beneficiary.response.BeneficiariesRH;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created  on 8/25/2017.
 */

public class BimaCloudDataStore extends BaseAuthCloudDataStore {

    private static BimaCloudDataStore bimaCloudDataStore;
    private TezBimaAPI tezBimaAPI;

    private BimaCloudDataStore() {
        super();
        tezBimaAPI = tezAPIBuilder.build().create(TezBimaAPI.class);
    }

    public static void clear() {
        bimaCloudDataStore = null;
    }

    public static BimaCloudDataStore getInstance() {
        if (bimaCloudDataStore == null)
            bimaCloudDataStore = new BimaCloudDataStore();
        return bimaCloudDataStore;
    }

    public void setDefaultBeneficiary(int beneficiaryId, SetDefaultBeneficiaryCallback callback) {
        tezBimaAPI.setDefaultBeneficiary(beneficiaryId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).
                subscribe(new SetDefaultBeneficiaryRH(this, callback));
    }

    public void setAdvanceBimaBeneficiary(AdvanceBimaBeneficiaryRequest advanceBimaBeneficiaryRequest, SetAdvanceBimaBeneficiaryCallback setAdvanceBimaBeneficiaryCallback) {
        tezBimaAPI.setAdvanceBimaBeneficiary(advanceBimaBeneficiaryRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).
                subscribe(new SetAdvanceBimaBeneficiaryRH(this, setAdvanceBimaBeneficiaryCallback));
    }

    public void getBeneficiaries(GetBeneficiariesListener beneficiariesListener) {
        tezBimaAPI.getBeneficiaries().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).
                subscribe(new BeneficiariesRH(this, beneficiariesListener));
    }

    public void getInsurancePolicyDetails(int policyId,
                                          GetActiveInsurancePolicyDetailsCallback getActiveInsurancePolicyDetailsCallback) {
        tezBimaAPI.getInsurancePolicyDetails(policyId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new GetActiveInsurancePolicyDetailsRH(this, getActiveInsurancePolicyDetailsCallback));
    }

    public void getProductPolicy(int policyId, @NonNull ProductPolicyCallback callback) {
        tezBimaAPI.getProductPolicy(policyId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ProductPolicyRH(this, callback));
    }

    public void getInsuranceProducts(@NonNull ProductListCallback callback) {
        tezBimaAPI.getInsuranceProducts().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ProductListRH(this, callback));
    }

    public void getInsurancePolicies(@NonNull GetActiveInsurancePolicyCallback callback) {
        tezBimaAPI.getInsurancePolicies().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new GetActiveInsurancePolicyRH(this, callback));
    }

    public void getProductTermConditions(int productId, @NonNull ProductTermConditionsCallback callback) {
        tezBimaAPI.getProductTermConditions(productId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ProductTermConditionsRH(this, callback));
    }

    public void getProductTermCondition(int productId, @NonNull ProductDetailTermConditionCallback callback) {
        tezBimaAPI.getProductTermCondition(productId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ProductDetailTermConditionRH(this, callback));
    }

    public void getInsuranceRemainingSteps(LoanRemainingStepsCallback callback) {
        tezBimaAPI.getInsuranceRemainingSteps().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(
                new LoanRemainingStepsRH(this, callback));
    }

    public void getInsuranceCoverage(int productId, InsuranceProductDetailCallback insuranceProductDetailCallback) {
        tezBimaAPI.getInsuranceProductDetail(productId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(
                new InsuranceProductDetailsRH(this, insuranceProductDetailCallback));
    }

    public void initiatePurchasePolicy(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest, InitiatePurchasePolicyCallback initiatePurchasePolicyCallback) {
        tezBimaAPI.initiatePurchasePolicy(initiatePurchasePolicyRequest).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new InitiatePurchasePolicyRH(this, initiatePurchasePolicyCallback));
    }

    public void purchasePolicy(PurchasePolicyRequest purchasePolicyRequest, PurchasePolicyCallback purchasePolicyCallback) {
        tezBimaAPI.purchasePolicy(purchasePolicyRequest).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new PurchasePolicyRH(this, purchasePolicyCallback));

    }

}
