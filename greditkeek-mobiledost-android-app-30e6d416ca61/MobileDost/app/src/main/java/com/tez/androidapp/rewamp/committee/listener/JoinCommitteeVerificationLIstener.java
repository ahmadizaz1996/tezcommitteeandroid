package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeVerificationResponse;

import java.util.List;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface JoinCommitteeVerificationLIstener {
    void onJoinCommitteeVerificationSuccess(JoinCommitteeVerificationResponse joinCommitteeVerificationResponse);

    void onJoinCommitteeVerificationFailure(int errorCode, String message);

}
