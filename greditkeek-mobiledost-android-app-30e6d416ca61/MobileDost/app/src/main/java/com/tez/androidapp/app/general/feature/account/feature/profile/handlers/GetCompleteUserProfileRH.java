package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetCompleteUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.GetCompleteProfileResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.GetUserProfileResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

public class GetCompleteUserProfileRH extends BaseRH<GetCompleteProfileResponse> {

    private GetCompleteUserProfileCallback getCompleteUserProfileCallback;

    public GetCompleteUserProfileRH(BaseCloudDataStore baseCloudDataStore, GetCompleteUserProfileCallback getCompleteUserProfileCallback) {
        super(baseCloudDataStore);
        this.getCompleteUserProfileCallback = getCompleteUserProfileCallback;
    }

    @Override
    protected void onSuccess(Result<GetCompleteProfileResponse> value) {
        GetCompleteProfileResponse getUserProfileResponse = value.response().body();
        if (getUserProfileResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            getCompleteUserProfileCallback.onCompleteUserProfileSuccess(getUserProfileResponse.getUserProfile());
        else onFailure(getUserProfileResponse.getStatusCode(), getUserProfileResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.getCompleteUserProfileCallback.onCompleteUserProfileFailure(errorCode, message);
    }
}
