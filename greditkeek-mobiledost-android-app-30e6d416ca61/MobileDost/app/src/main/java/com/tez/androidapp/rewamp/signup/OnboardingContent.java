package com.tez.androidapp.rewamp.signup;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

import java.io.Serializable;

/**
 * Created by VINOD KUMAR on 5/28/2019.
 */
public class OnboardingContent implements Serializable {

    @DrawableRes
    private int illustration;

    @StringRes
    private int title;

    @StringRes
    private int message;

    public OnboardingContent(@DrawableRes int illustration,
                             @StringRes int title,
                             @StringRes int message) {
        this.illustration = illustration;
        this.title = title;
        this.message = message;
    }

    @DrawableRes
    public int getIllustration() {
        return illustration;
    }

    @StringRes
    public int getTitle() {
        return title;
    }

    @StringRes
    public int getMessage() {
        return message;
    }
}
