package com.tez.androidapp.rewamp.advance.repay.interactor;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.InitiateRepaymentCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.InitiateRepaymentRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.InitiateRepaymentResponse;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.advance.repay.presenter.IVerifyJazzRepaymentActivityInteractorOutput;
import com.tez.androidapp.rewamp.advance.repay.presenter.IVerifyRepaymentActivityInteractorOutput;

public class VerifyJazzRepaymentActivityInteractor extends VerifyRepaymentActivityInteractor implements IVerifyJazzRepaymentActivityInteractor{

    private final IVerifyJazzRepaymentActivityInteractorOutput iVerifyJazzRepaymentActivityInteractorOutput;

    public VerifyJazzRepaymentActivityInteractor(IVerifyJazzRepaymentActivityInteractorOutput iVerifyRepaymentActivityInteractorOutput) {
        super(iVerifyRepaymentActivityInteractorOutput);
        iVerifyJazzRepaymentActivityInteractorOutput = iVerifyRepaymentActivityInteractorOutput;
    }

    @Override
    public void initiateRepayment(@NonNull InitiateRepaymentRequest initiateRepaymentRequest,
                                  @NonNull String pin, int mobileAccountId,
                                  double amount, @Nullable Location location) {
        LoanCloudDataStore.getInstance().initiateRepayment(initiateRepaymentRequest, new InitiateRepaymentCallback() {
            @Override
            public void onInitiateRepaymentSuccess(InitiateRepaymentResponse initiateRepaymentResponse) {
                iVerifyJazzRepaymentActivityInteractorOutput.onInitiateRepaymentSuccess(initiateRepaymentResponse,
                        pin, mobileAccountId, amount, location);
            }

            @Override
            public void onInitiateRepaymentFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    initiateRepayment(initiateRepaymentRequest);
                else
                    iVerifyJazzRepaymentActivityInteractorOutput.onInitiateRepaymentFailure(errorCode, message);
            }
        });
    }
}
