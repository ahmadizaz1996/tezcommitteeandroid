package com.tez.androidapp.repository.network.store;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.transactions.callbacks.GetLoanTransactionDetailCallback;
import com.tez.androidapp.app.general.feature.transactions.handlers.GetUserTransactionDetailRH;
import com.tez.androidapp.app.general.feature.transactions.models.network.dto.request.GetLoanTransactionDetailRequest;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.callbacks.CancelLoanLimitCallback;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.handlers.CancelLoanLimitRH;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.request.CancelLoanLimitRequest;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.request.LoanLimitRequest;
import com.tez.androidapp.app.vertical.advance.loan.confirmation.callbacks.GenerateDisbursementReceiptCallback;
import com.tez.androidapp.app.vertical.advance.loan.confirmation.handlers.GenerateDisbursementReceiptRH;
import com.tez.androidapp.app.vertical.advance.loan.questions.callbacks.UploadDataStatusCallback;
import com.tez.androidapp.app.vertical.advance.loan.questions.hanlders.UploadDataStatusRH;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.InitiateRepaymentCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.RepayLoanCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.RepayReceiptCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.handlers.InitiateRepaymentRH;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.handlers.RepayLoanRH;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.handlers.RepayReceiptRH;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.InitiateRepaymentRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.RepayLoanRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.RepayReceiptRequest;
import com.tez.androidapp.app.vertical.advance.loan.shared.api.client.MobileDostLoanAPI;
import com.tez.androidapp.app.vertical.advance.loan.shared.limit.callbacks.LoanLimitCallback;
import com.tez.androidapp.app.vertical.advance.loan.shared.limit.handlers.LoanLimitRH;
import com.tez.androidapp.rewamp.advance.request.callback.LoanApplyCallback;
import com.tez.androidapp.rewamp.advance.request.callback.LoanPricingDetailsCallback;
import com.tez.androidapp.rewamp.advance.request.callback.LoanRemainingStepsCallback;
import com.tez.androidapp.rewamp.advance.request.callback.ProcessingFeesCallback;
import com.tez.androidapp.rewamp.advance.request.callback.TenureDetailsCallback;
import com.tez.androidapp.rewamp.advance.request.request.LoanApplyRequest;
import com.tez.androidapp.rewamp.advance.request.response.LoanApplyRH;
import com.tez.androidapp.rewamp.advance.request.response.LoanPricingDetailsRH;
import com.tez.androidapp.rewamp.advance.request.response.LoanRemainingStepsRH;
import com.tez.androidapp.rewamp.advance.request.response.ProcessingFeesRH;
import com.tez.androidapp.rewamp.advance.request.response.TenureDetailsRH;
import com.tez.androidapp.rewamp.general.feedback.callback.LoanFeedbackCallback;
import com.tez.androidapp.rewamp.general.feedback.request.LoanFeedbackRequest;
import com.tez.androidapp.rewamp.general.feedback.response.handler.LoanFeedbackRH;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Data store to make loan related network calls
 * <p>
 * Created  on 2/17/2017.
 */

public class LoanCloudDataStore extends BaseAuthCloudDataStore {

    private static LoanCloudDataStore loanCloudDataStore;
    private MobileDostLoanAPI mobileDostLoanAPI;

    private LoanCloudDataStore() {
        super();
        mobileDostLoanAPI = tezAPIBuilder.build().create(MobileDostLoanAPI.class);
    }

    public static void clear() {
        loanCloudDataStore = null;
    }

    public static LoanCloudDataStore getInstance() {
        if (loanCloudDataStore == null)
            loanCloudDataStore = new LoanCloudDataStore();
        return loanCloudDataStore;
    }


    public void postLoanLimit(LoanLimitRequest loanLimitRequest, LoanLimitCallback loanLimitCallback) {
        mobileDostLoanAPI.postLoanLimit(loanLimitRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new LoanLimitRH(this, loanLimitCallback));
    }

    public void getLoanLimit(LoanLimitCallback loanLimitCallback) {
        mobileDostLoanAPI.getLoanLimit().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new LoanLimitRH(this, loanLimitCallback));
    }

    public void cancelLoanLimit(CancelLoanLimitRequest cancelLoanLimitRequest, CancelLoanLimitCallback cancelLoanLimitCallback) {
        mobileDostLoanAPI.cancelLoanLimit(cancelLoanLimitRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe
                (new CancelLoanLimitRH(this, cancelLoanLimitCallback));
    }

    public void getProcessingFeesDetails(ProcessingFeesCallback processingFeesCallback) {
        mobileDostLoanAPI.getProcessingFeesDetails().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ProcessingFeesRH(this, processingFeesCallback));
    }

    public void getTenuresDetails(TenureDetailsCallback tenureDetailsCallback) {
        mobileDostLoanAPI.getTenureDetails().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new TenureDetailsRH(this, tenureDetailsCallback));
    }

    public void getLoanPricingDetails(LoanPricingDetailsCallback loanPricingDetailsCallback) {
        mobileDostLoanAPI.getLoanPricingDetails().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new LoanPricingDetailsRH(this, loanPricingDetailsCallback));
    }


    public void applyForLoan(LoanApplyRequest loanApplyRequest, LoanApplyCallback loanApplyCallback) {
        mobileDostLoanAPI.applyForLoan(loanApplyRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new LoanApplyRH(this, loanApplyCallback));
    }


    public void uploadDataStatus(UploadDataStatusCallback uploadDataStatusCallback) {
        mobileDostLoanAPI.getUploadDataStatus().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new UploadDataStatusRH(this, uploadDataStatusCallback));
    }

    public void getRepayReceipt(RepayReceiptRequest repayReceiptRequest, RepayReceiptCallback repayReceiptCallback) {
        mobileDostLoanAPI.getRepayReceipt(repayReceiptRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new RepayReceiptRH(this, repayReceiptCallback));
    }

    public void initiateRepayment(InitiateRepaymentRequest initiateRepaymentRequest, InitiateRepaymentCallback initiateRepaymentCallback) {
        mobileDostLoanAPI.initiateRepayment(initiateRepaymentRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new InitiateRepaymentRH(this, initiateRepaymentCallback));
    }

    public void repayLoan(RepayLoanRequest repayLoanRequest, RepayLoanCallback repayLoanCallback) {
        mobileDostLoanAPI.repayLoan(repayLoanRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new RepayLoanRH(this, repayLoanCallback));
    }


    public void getLoanTransactionDetail(GetLoanTransactionDetailRequest getLoanTransactionDetailRequest, GetLoanTransactionDetailCallback getLoanTransactionDetailCallback) {
        mobileDostLoanAPI.getLoanTransactionDetail(getLoanTransactionDetailRequest).observeOn(AndroidSchedulers
                .mainThread()).subscribeOn(Schedulers.io()).subscribe(new
                GetUserTransactionDetailRH(this, getLoanTransactionDetailCallback));
    }

    public void generateDisbursementReceipt(GenerateDisbursementReceiptCallback generateDisbursementReceiptCallback) {
        mobileDostLoanAPI.generateDisbursementReceipt().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(
                new GenerateDisbursementReceiptRH(this, generateDisbursementReceiptCallback));
    }

    public void getLoanRemainingSteps(LoanRemainingStepsCallback callback) {
        mobileDostLoanAPI.getLoanRemainingSteps().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(
                new LoanRemainingStepsRH(this, callback));
    }

    public void submitLoanFeedback(@NonNull LoanFeedbackRequest loanFeedbackRequest, @NonNull LoanFeedbackCallback loanFeedbackCallback) {
        mobileDostLoanAPI.submitLoanFeedback(loanFeedbackRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new LoanFeedbackRH(this, loanFeedbackCallback));
    }
}
