package com.tez.androidapp.rewamp.bima.products.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.bima.products.model.InsuranceProduct;

import java.util.List;

public class ProductListResponse extends BaseResponse {

    private List<InsuranceProduct> products;

    public List<InsuranceProduct> getProducts() {
        return products;
    }
}
