package com.tez.androidapp.rewamp.general.beneficiary.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetAdvanceBimaBeneficiaryCallback;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.models.network.dto.request.AdvanceBimaBeneficiaryRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.IEditBeneficiaryActivityInteractorOutput;

public class EditBeneficiaryActivityInteractor implements IEditBeneficiaryActivityInteractor {

    private final IEditBeneficiaryActivityInteractorOutput iEditBeneficiaryActivityInteractorOutput;

    public EditBeneficiaryActivityInteractor(IEditBeneficiaryActivityInteractorOutput iEditBeneficiaryActivityInteractorOutput) {
        this.iEditBeneficiaryActivityInteractorOutput = iEditBeneficiaryActivityInteractorOutput;
    }

    @Override
    public void setAdvanceBimaBeneficiary(@NonNull AdvanceBimaBeneficiaryRequest request) {
        BimaCloudDataStore.getInstance().setAdvanceBimaBeneficiary(request, new SetAdvanceBimaBeneficiaryCallback() {
            @Override
            public void onSetAdvanceBimaBeneficiarySuccess() {
                iEditBeneficiaryActivityInteractorOutput.onSetAdvanceBimaBeneficiarySuccess();
            }

            @Override
            public void onSetAdvanceBimaBeneficiaryFailure(int statusCode, String message) {
                if (Utility.isUnauthorized(statusCode))
                    setAdvanceBimaBeneficiary(request);
                else
                    iEditBeneficiaryActivityInteractorOutput.onSetAdvanceBimaBeneficiaryFailure(statusCode, message);
            }
        });
    }
}
