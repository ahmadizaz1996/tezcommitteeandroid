package com.tez.androidapp.app.general.feature.account.feature.pin.code.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 9/15/2017.
 */

public class ChangeTemporaryPinRequest extends BaseRequest {
    public static final String METHOD_NAME = "v2/user/changeTempPin";

    private String pin;

    public ChangeTemporaryPinRequest(String pin) {
        this.pin = pin;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
