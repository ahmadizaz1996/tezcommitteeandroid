package com.tez.androidapp.rewamp.general.wallet;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.ToolbarActivity;
import com.tez.androidapp.app.general.feature.playback.TezCountDownTimer;
import com.tez.androidapp.commons.receivers.SMSReceiver;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezPinView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.general.wallet.presenter.IVerifyWalletOtpActivityPresenter;
import com.tez.androidapp.rewamp.general.wallet.presenter.VerifyWalletOtpActivityPresenter;
import com.tez.androidapp.rewamp.general.wallet.router.VerifyWalletOtpActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.router.WalletAddedActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.view.IVerifyWalletOtpActivityView;
import com.tez.androidapp.rewamp.profile.trust.router.CompleteYourProfileActivityRouter;
import com.tez.androidapp.rewamp.util.DialogUtil;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import net.tez.viewbinder.library.core.BindView;

public class VerifyWalletOtpActivity extends ToolbarActivity implements IVerifyWalletOtpActivityView, TezPinView.OnPinEnteredListener {

    private final IVerifyWalletOtpActivityPresenter iVerifyWalletOtpActivityPresenter;

    @BindView(R.id.tvResendCode)
    private TezTextView tvResendCode;

    @BindView(R.id.pinViewOTP)
    private TezPinView pinViewOtp;

    private final SMSReceiver smsReceiver = new SMSReceiver(this::onOtpReceived);

    public VerifyWalletOtpActivity() {
        iVerifyWalletOtpActivityPresenter = new VerifyWalletOtpActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_wallet);
        this.tezToolbar.setToolbarTitle(R.string.my_wallet);
        init();
    }

    private void init() {
        iVerifyWalletOtpActivityPresenter.addWallet(getServiceProviderIdFromIntent(),
                getMobileAccountNumberFromIntent(),
                getPinFromIntent(),
                getIsDefaultFromIntent());
        initOnClickListener();
    }

    private void initOnClickListener() {
        tvResendCode.setDoubleTapSafeOnClickListener(view -> iVerifyWalletOtpActivityPresenter.resendAddWalletOtp(getServiceProviderIdFromIntent(), getMobileAccountNumberFromIntent()));
        pinViewOtp.setOnPinEnteredListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsReceiver.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsReceiver.unregister(this);
    }

    @Override
    public void showEasyPaisaError(@StringRes int message) {
        String messageString = getString(message);
        messageString = messageString.replaceAll(ResponseStatusCode.PlaceHolders.WALLET_NAME.getValue(), getString(R.string.easypaisa));
        messageString = messageString.replaceAll(ResponseStatusCode.PlaceHolders.WALLET_SUPPORT_NUMBER.getValue(), Constants.EASYPASIA_CALL_CENTER_NUMBER);

        DialogUtil.showActionableDialog(this, R.string.error, messageString, (dialog, which) -> {
            if (which == DialogInterface.BUTTON_POSITIVE)
                Utility.openPlayStoreForApp(this, Constants.EASYPAISA_PACKAGE_NAME);
            finish();
        });
    }

    @Override
    public void showWalletDoesNotExistError(int serviceProviderId, @StringRes int message) {
        DialogUtil.showActionableDialog(this,
                getString(R.string.wallet_not_found, Utility.getWalletName(serviceProviderId)),
                getFormattedErrorMessage(serviceProviderId, message),
                R.string.create_new_wallet,
                (dialog, which) -> {
                    if (which == DialogInterface.BUTTON_POSITIVE)
                        Utility.openPlayStoreForApp(this, Utility.getWalletAppPackageName(serviceProviderId));
                    finish();
                });
    }

    @Override
    public void showWalletDoesNotMatchCnicError(int serviceProviderId, @StringRes int message) {
        DialogUtil.showActionableDialog(this,
                getString(R.string.wallet_does_not_match_with_cnic, Utility.getWalletName(serviceProviderId)),
                getFormattedErrorMessage(serviceProviderId, message),
                R.string.create_new_wallet,
                (dialog, which) -> {
                    if (which == DialogInterface.BUTTON_POSITIVE)
                        Utility.openPlayStoreForApp(this, Utility.getWalletAppPackageName(serviceProviderId));
                    finish();
                });
    }

    @Override
    public void routeToCompleteYourProfileActivity(){
        CompleteYourProfileActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public String getFormattedErrorMessage(int serviceProviderId, @StringRes int message) {
        String messageString = getString(message);
        messageString = messageString.replaceAll(ResponseStatusCode.PlaceHolders.WALLET_NAME.getValue(), Utility.getWalletName(serviceProviderId));
        return messageString.replaceAll(ResponseStatusCode.PlaceHolders.WALLET_SUPPORT_NUMBER.getValue(), Utility.getWalletProviderNumber(serviceProviderId));
    }

    @Override
    public void setPinViewOtpClearText() {
        pinViewOtp.clearText();
    }

    @Override
    public void startWalletAddedActivity() {
        WalletAddedActivityRouter.createInstance().setDependenciesAndRoute(this, getServiceProviderIdFromIntent());
        finish();
    }

    @Override
    public void setPinViewOtpEnabled(boolean enabled) {
        pinViewOtp.setEnabled(enabled);
    }

    @Override
    public void setTvResendCodeEnabled(boolean enabled) {
        tvResendCode.setEnabled(enabled);
    }

    @Override
    public void startTimer() {
        int totalTime = 45 * 1000;
        int oneSecond = 1000;
        setTvResendCodeEnabled(false);
        new TezCountDownTimer(totalTime, oneSecond) {

            @Override
            public void onTick(long millisUntilFinished) {
                String second = "00:" + (int) millisUntilFinished / 1000;
                tvResendCode.setText(second);
            }

            @Override
            public void onFinish() {
                tvResendCode.setText(R.string.resend_code);
                setTvResendCodeEnabled(true);
            }
        }.start();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onPinEntered(@NonNull String pin) {
        iVerifyWalletOtpActivityPresenter.verifyAddWalletOtp(getServiceProviderIdFromIntent(), getMobileAccountNumberFromIntent(), pin);
    }

    private int getServiceProviderIdFromIntent() {
        return getIntent().getIntExtra(VerifyWalletOtpActivityRouter.SERVICE_PROVIDER_ID, -1);
    }

    private String getMobileAccountNumberFromIntent() {
        return getIntent().getStringExtra(VerifyWalletOtpActivityRouter.MOBILE_ACCOUNT_NUMBER);
    }

    private String getPinFromIntent() {
        return getIntent().getStringExtra(VerifyWalletOtpActivityRouter.PIN);
    }

    private boolean getIsDefaultFromIntent() {
        return getIntent().getBooleanExtra(VerifyWalletOtpActivityRouter.IS_DEFAULT, false);
    }

    private void onOtpReceived(String otp) {
        this.pinViewOtp.setText(otp);
    }

    @Override
    protected String getScreenName() {
        return "VerifyWalletOtpActivity";
    }
}
