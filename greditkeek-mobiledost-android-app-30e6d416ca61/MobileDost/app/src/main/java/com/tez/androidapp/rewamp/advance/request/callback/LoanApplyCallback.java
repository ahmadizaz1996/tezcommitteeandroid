package com.tez.androidapp.rewamp.advance.request.callback;

/**
 * Created  on 3/22/2017.
 */

public interface LoanApplyCallback {

    void onLoanApplySuccess(int statusCode);

    void onLoanApplyFailure(int errorCode, String message);
}
