package com.tez.androidapp.rewamp.advance.request.interactor;

import com.tez.androidapp.app.vertical.advance.loan.confirmation.callbacks.GenerateDisbursementReceiptCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.advance.request.presenter.ILoanReceiptActivityInteractorOutput;

public class LoanReceiptActivityInteractor implements ILoanReceiptActivityInteractor {

    private final ILoanReceiptActivityInteractorOutput iLoanReceiptActivityInteractorOutput;

    public LoanReceiptActivityInteractor(ILoanReceiptActivityInteractorOutput iLoanReceiptActivityInteractorOutput) {
        this.iLoanReceiptActivityInteractorOutput = iLoanReceiptActivityInteractorOutput;
    }

    @Override
    public void generateDisbursementReceipt() {
        LoanCloudDataStore.getInstance().generateDisbursementReceipt(new GenerateDisbursementReceiptCallback() {
            @Override
            public void onGenerateDisbursementReceiptSuccess(LoanDetails loanDetails) {
                iLoanReceiptActivityInteractorOutput.onGenerateDisbursementReceiptSuccess(loanDetails);
                MDPreferenceManager.setDisbursementReceiptViewed(true);
            }

            @Override
            public void onGenerateDisbursementReceiptFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    generateDisbursementReceipt();
                else
                    iLoanReceiptActivityInteractorOutput.onGenerateDisbursementReceiptFailure(errorCode, message);
            }
        });
    }
}
