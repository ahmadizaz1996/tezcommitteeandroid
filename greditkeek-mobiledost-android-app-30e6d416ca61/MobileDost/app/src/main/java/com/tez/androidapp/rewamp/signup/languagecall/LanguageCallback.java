package com.tez.androidapp.rewamp.signup.languagecall;

/**
 * Created by VINOD KUMAR on 8/9/2019.
 */
public interface LanguageCallback {

    void onLanguageSuccess();

    void onLanguageFailure(int errorCode, String errorDescription);
}
