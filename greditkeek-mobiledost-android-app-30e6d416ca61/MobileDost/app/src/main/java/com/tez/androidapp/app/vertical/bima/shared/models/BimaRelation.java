package com.tez.androidapp.app.vertical.bima.shared.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

/**
 * Created  on 8/28/2017.
 */

public class BimaRelation implements Parcelable {

    private int relationShipId;
    @DrawableRes
    private int relationShipIcon;
    private String relationship;
    @StringRes
    private int relationshipNameId;

    public BimaRelation(int relationShipId, String relationship) {
        this.relationShipId = relationShipId;
        this.relationship = relationship;
    }

    public BimaRelation(int relationShipId, int relationShipIcon, String relationship) {
        this.relationShipId = relationShipId;
        this.relationShipIcon = relationShipIcon;
        this.relationship = relationship;
    }

    public BimaRelation(int relationShipId, @StringRes int relationshipNameId, @DrawableRes int relationShipIcon) {
        this.relationShipId = relationShipId;
        this.relationshipNameId = relationshipNameId;
        this.relationShipIcon = relationShipIcon;
    }

    protected BimaRelation(Parcel in) {
        relationShipId = in.readInt();
        relationShipIcon = in.readInt();
        relationship = in.readString();
        relationshipNameId = in.readInt();
    }

    public static final Creator<BimaRelation> CREATOR = new Creator<BimaRelation>() {
        @Override
        public BimaRelation createFromParcel(Parcel in) {
            return new BimaRelation(in);
        }

        @Override
        public BimaRelation[] newArray(int size) {
            return new BimaRelation[size];
        }
    };

    @StringRes
    public int getRelationshipNameId() {
        return relationshipNameId;
    }

    public int getRelationShipId() {
        return relationShipId;
    }

    public String getRelationship() {
        return relationship;
    }

    public int getRelationShipIcon() {
        return relationShipIcon;
    }

    public void setRelationShipId(int relationShipId) {
        this.relationShipId = relationShipId;
    }

    public void setRelationShipIcon(int relationShipIcon) {
        this.relationShipIcon = relationShipIcon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(relationShipId);
        dest.writeInt(relationShipIcon);
        dest.writeString(relationship);
        dest.writeInt(relationshipNameId);
    }
}
