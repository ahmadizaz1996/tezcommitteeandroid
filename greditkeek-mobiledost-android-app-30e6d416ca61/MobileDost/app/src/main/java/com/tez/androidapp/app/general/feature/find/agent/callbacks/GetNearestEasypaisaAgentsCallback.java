package com.tez.androidapp.app.general.feature.find.agent.callbacks;

import com.tez.androidapp.app.general.feature.find.agent.models.network.EasypaisaAgent;

import java.util.List;

/**
 * Created by Rehman Murad Ali on 1/18/2018.
 */

public interface GetNearestEasypaisaAgentsCallback {

    void onGetNearestEasypaisaAgentsSuccess(List<EasypaisaAgent> easypaisaAgentList);

    void onGetNearestEasypaisaAgentsFailure(int errorCode, String message);
}
