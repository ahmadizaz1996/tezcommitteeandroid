package com.tez.androidapp.repository.network.store;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.ChangePinCallback;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.ChangeTemporaryPinCallback;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserVerifyCallback;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.handlers.ChangePinRH;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.handlers.ChangeTemporaryPinRH;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.handlers.UserVerifyRH;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.models.network.dto.request.ChangePinRequest;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.models.network.dto.request.ChangeTemporaryPinRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.AccountLinkCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetCnicUploadsCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetCompleteUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.ProfileBasicCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.SetUserPictureCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateCnicPictureCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdatePersonalInfoCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateProfileWithCnicCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.ValidateInfoCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.AccountLinkRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.GetCnicUploadsRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.GetCompleteUserProfileRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.GetUserProfileRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.ProfileBasicRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.SetUserPictureRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.UpdateCnicPictureRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.UpdatePersonalInfoRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.UpdateProfileWithCnicRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.UpdateUserBasicProfileRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.UpdateUserProfileRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.ValidateInfoRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.AccountLinkRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.SetUserPictureRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateProfileWithCnicRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserBasicProfileRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserProfileRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.ValidateInfoRequest;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.SuspendAccountCallback;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.SuspendAccountVerifyOTPCallback;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.UserReActivateAccountOTPCallback;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.UserReActivateAccountResendOTPCallback;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.handlers.SuspendAccountRH;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.handlers.SuspendAccountVerifyOTPRH;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.handlers.UserReActivateAccountRH;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.handlers.UserReActivateAccountResendOTPRH;
import com.tez.androidapp.app.general.feature.authentication.signin.callbacks.UserLogoutCallback;
import com.tez.androidapp.app.general.feature.authentication.signin.handlers.UserLogoutRH;
import com.tez.androidapp.app.general.feature.inivite.user.callback.GetReferralCodeCallback;
import com.tez.androidapp.app.general.feature.inivite.user.handler.GetReferralCodeRH;
import com.tez.androidapp.app.general.feature.notification.callbacks.UserNotificationsCallback;
import com.tez.androidapp.app.general.feature.notification.handlers.UserNotificationsRH;
import com.tez.androidapp.app.general.feature.transactions.callbacks.GetUserTransactionsCallback;
import com.tez.androidapp.app.general.feature.transactions.handlers.GetUserTransactionsRH;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks.AddWalletCallback;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks.AddWalletResendOTPCallback;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks.AddWalletVerifyOTPCallback;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.handlers.AddWalletRH;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.handlers.AddWalletResendOTPRH;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.handlers.AddWalletVerifyOTPRH;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request.AddWalletRequest;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.DeleteWalletCallback;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.GetAllWalletCallback;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.SetDefaultWalletCallback;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.handlers.DeleteWalletRH;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.handlers.GetAllWalletRH;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.handlers.SetDefaultWalletRH;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.network.dto.request.SetDefaultWalletRequest;
import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetActiveInsurancePolicyCallback;
import com.tez.androidapp.app.vertical.bima.policies.handlers.GetActiveInsurancePolicyRH;
import com.tez.androidapp.commons.api.client.MobileDostUserAPI;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.dto.request.UserLogoutRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserReActivateAccountOTPRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserVerifyRequest;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.handlers.UserStatusRH;
import com.tez.androidapp.rewamp.dashboard.callbacks.DashboardActionCardCallback;
import com.tez.androidapp.rewamp.dashboard.callbacks.DashboardAdvanceCardCallback;
import com.tez.androidapp.rewamp.dashboard.callbacks.UserProfileStatusCallback;
import com.tez.androidapp.rewamp.dashboard.response.DashboardActionCardRH;
import com.tez.androidapp.rewamp.dashboard.response.DashboardAdvanceCardRH;
import com.tez.androidapp.rewamp.dashboard.response.UserProfileStatusRH;
import com.tez.androidapp.rewamp.general.notification.data.source.NotificationDataSource;
import com.tez.androidapp.rewamp.general.suspend.account.FeedbackCallback;
import com.tez.androidapp.rewamp.general.suspend.account.FeedbackRH;
import com.tez.androidapp.rewamp.general.suspend.account.FeedbackRequest;
import com.tez.androidapp.rewamp.signup.languagecall.LanguageCallback;
import com.tez.androidapp.rewamp.signup.languagecall.LanguageRH;

import net.tez.fragment.util.optional.Optional;

import java.io.File;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;

/**
 * Data store to make user related authenticated network calls
 * <p>
 * Created  on 6/14/2017.
 */

public class UserAuthCloudDataStore extends BaseAuthCloudDataStore {

    private static UserAuthCloudDataStore userAuthCloudDataStore;
    private MobileDostUserAPI mobileDostUserAPI;

    private UserAuthCloudDataStore() {
        super();
        mobileDostUserAPI = tezAPIBuilder.build().create(MobileDostUserAPI.class);
    }

    public static void clear() {
        userAuthCloudDataStore = null;
    }

    public static UserAuthCloudDataStore getInstance() {
        if (userAuthCloudDataStore == null)
            userAuthCloudDataStore = new UserAuthCloudDataStore();
        return userAuthCloudDataStore;
    }

    public void verify(UserVerifyRequest userVerifyRequest, UserVerifyCallback userVerifyCallback) {
        mobileDostUserAPI.verify(userVerifyRequest).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new UserVerifyRH(this, userVerifyCallback));
    }

    public void addWallet(AddWalletRequest addWalletRequest, AddWalletCallback addWalletCallback) {
        mobileDostUserAPI.addWallet(Utility.getAPIKey(), addWalletRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new AddWalletRH(this, addWalletCallback));
    }

    public void getDashboardAdvanceCard(DashboardAdvanceCardCallback callback) {
        mobileDostUserAPI.getDashboardAdvanceCard().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new DashboardAdvanceCardRH(this, callback));
    }

    public void getDashboardActionCard(DashboardActionCardCallback callback) {
        mobileDostUserAPI.getDashboardActionCard().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new DashboardActionCardRH(this, callback));
    }

    public void getUserProfileStatus(UserProfileStatusCallback callback) {
        mobileDostUserAPI.getUserProfileStatus().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new UserProfileStatusRH(this, callback));
    }

    public void getReferralCode(GetReferralCodeCallback getReferralCodeCallback) {
        mobileDostUserAPI.getReferralCode().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).
                subscribe(new GetReferralCodeRH(this, getReferralCodeCallback));
    }

    public void addWalletResendOTP(String mobileAccountNumber, int walletId, AddWalletResendOTPCallback addWalletResendOTPCallback) {
        mobileDostUserAPI.addWalletResendOTP(Utility.getAPIKey(), mobileAccountNumber, walletId).observeOn(AndroidSchedulers.mainThread()).subscribeOn
                (Schedulers.io())
                .subscribe(new AddWalletResendOTPRH(this, addWalletResendOTPCallback));
    }

    public void addWalletVerifyOTP(String otp, int walletId, String mobileAccountNumber, AddWalletVerifyOTPCallback addWalletVerifyOTPCallback) {
        mobileDostUserAPI.addWalletVerifyOTP(Utility.getAPIKey(), mobileAccountNumber, walletId, otp).observeOn(AndroidSchedulers.mainThread()).subscribeOn
                (Schedulers.io())
                .subscribe(new AddWalletVerifyOTPRH(this, addWalletVerifyOTPCallback));
    }

    public void getCompleteUserProfile(GetCompleteUserProfileCallback getCompleteUserProfileCallback) {
        mobileDostUserAPI.getCompleteUserProfile().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new GetCompleteUserProfileRH(this, getCompleteUserProfileCallback));
    }

    public void updateUserProfileWithCnic(UpdateProfileWithCnicRequest updateProfileWithCnicRequest, UpdateProfileWithCnicCallback updateProfileWithCnicCallback) {
        mobileDostUserAPI.updateProfileWithCnic(updateProfileWithCnicRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new UpdateProfileWithCnicRH(this, updateProfileWithCnicCallback));
    }


    public void logout(UserLogoutCallback userLogoutCallback) {
        Optional.ifPresent(MDPreferenceManager.getAccessToken(), accessToken -> {
                    mobileDostUserAPI.logout(new UserLogoutRequest(accessToken.getRefreshToken()))
                            .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                            .subscribe(new UserLogoutRH(this, userLogoutCallback));
                },
                () -> userLogoutCallback.onUserLogoutFailure(-100, "Auth token is null"));
    }


    public void getUserNotifications(int startPage, UserNotificationsCallback userNotificationsCallback) {
        mobileDostUserAPI.getUserNotifications(startPage, NotificationDataSource.PAGE_SIZE).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new UserNotificationsRH(this, userNotificationsCallback));
    }

    public void suspendAccount(SuspendAccountCallback suspendAccountCallback) {
        mobileDostUserAPI.suspendAccount().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new SuspendAccountRH(this, suspendAccountCallback));
    }

    public void suspendAccountVerifyOTP(String otp, SuspendAccountVerifyOTPCallback suspendAccountVerifyOTPCallback) {
        mobileDostUserAPI.suspendAccountVerifyOTP(otp).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).
                subscribe(new SuspendAccountVerifyOTPRH(this, suspendAccountVerifyOTPCallback));
    }

    public void feedback(FeedbackRequest feedbackRequest, FeedbackCallback feedbackCallback) {
        mobileDostUserAPI.feedback(feedbackRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).
                subscribe(new FeedbackRH(this, feedbackCallback));
    }

    public void changePin(ChangePinRequest changePinRequest, ChangePinCallback changePinCallback) {
        mobileDostUserAPI.changePin(Utility.getAPIKey(), changePinRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ChangePinRH(this, changePinCallback));
    }


    public void setUserPicture(@NonNull File profilePicture, SetUserPictureCallback setUserPictureCallback) {
        MultipartBody.Part profilePicturePart = MultipartBody.Part.createFormData(SetUserPictureRequest.Params.PROFILE_PICTURE, profilePicture.getName(),
                MultipartBody.create(MediaType.parse(Utility.getMimeTypeFromLocalFileUri(profilePicture.getAbsolutePath())), profilePicture));
        mobileDostUserAPI.setUserPicture(profilePicturePart).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new
                SetUserPictureRH(this, setUserPictureCallback));
    }

    public void getManageBeneficiaryPolicies(GetActiveInsurancePolicyCallback getActiveInsurancePolicyCallback) {
        mobileDostUserAPI.getManageBeneficiaryPolicies().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new
                GetActiveInsurancePolicyRH(this, getActiveInsurancePolicyCallback));
    }

    public void getUserTransactions(GetUserTransactionsCallback getUserTransactionsCallback) {
        mobileDostUserAPI.getUserTransactions().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new
                GetUserTransactionsRH(this, getUserTransactionsCallback));
    }

    public void updateUserProfile(UpdateUserProfileRequest updateUserProfileRequest, UpdateUserProfileCallback updateUserProfileCallback) {
        mobileDostUserAPI.updateUserProfile(updateUserProfileRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new UpdateUserProfileRH(this, updateUserProfileCallback));
    }

    public void validateInfo(ValidateInfoCallback validateInfoCallback, ValidateInfoRequest validateInfoRequest) {
        mobileDostUserAPI.validateInfo(validateInfoRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ValidateInfoRH(this, validateInfoCallback));
    }

    public void getUserProfile(GetUserProfileCallback getUserProfileCallback) {
        mobileDostUserAPI.getUserProfile().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new GetUserProfileRH
                (this, getUserProfileCallback));
    }

    public void updateUserCnicPicture(MultipartBody.Part image,
                                      MultipartBody.Part isAutoDetected,
                                      UpdateCnicPictureCallback updateCnicPictureCallback) {
        mobileDostUserAPI.updateCnicPictures(image, isAutoDetected).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new UpdateCnicPictureRH(this, updateCnicPictureCallback));
    }

    public void updateUserPersonalInfo(MultipartBody.Part image,
                                       MultipartBody.Part personalInfo,
                                       UpdatePersonalInfoCallback updatePersonalInfoCallback) {
        mobileDostUserAPI.updatePersonalInfo(image, personalInfo)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new UpdatePersonalInfoRH(this, updatePersonalInfoCallback));
    }

    public void getCnicUploads(GetCnicUploadsCallback getCnicUploadsCallback) {
        mobileDostUserAPI.getCnicUploads()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new GetCnicUploadsRH(this, getCnicUploadsCallback));
    }

    public void updateUserBasicProfile(UpdateUserBasicProfileRequest updateUserProfileRequest, UpdateUserProfileCallback updateUserProfileCallback) {
        mobileDostUserAPI.updateUserBasicProfile(updateUserProfileRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new UpdateUserBasicProfileRH(this, updateUserProfileCallback));
    }

    public void getUserBasicProfile(ProfileBasicCallback profileBasicCallback) {
        mobileDostUserAPI.getUserBasicProfile()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProfileBasicRH(this, profileBasicCallback));
    }

    public void linkUserAccount(AccountLinkRequest accountLinkRequest, AccountLinkCallback accountLinkCallback) {
        mobileDostUserAPI.linkUserSocialAccount(accountLinkRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AccountLinkRH(this, accountLinkCallback));
    }


    public void changeTemporaryPin(String pin, ChangeTemporaryPinCallback changeTemporaryPinCallback) {
        mobileDostUserAPI.changeTemporaryPin(new ChangeTemporaryPinRequest(pin)).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new ChangeTemporaryPinRH(this, changeTemporaryPinCallback));
    }

    public void userReActivateAccount(String otp, UserReActivateAccountOTPCallback userReActivateAccountOTPCallback) {
        mobileDostUserAPI.reActivateAccount(new UserReActivateAccountOTPRequest(otp))
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new UserReActivateAccountRH(this, userReActivateAccountOTPCallback));
    }

    public void userReActivateAccountResendOTP(UserReActivateAccountResendOTPCallback userReActivateAccountResendOTPCallback) {
        mobileDostUserAPI.resendOTPToReActivateAccount().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new UserReActivateAccountResendOTPRH(this, userReActivateAccountResendOTPCallback));
    }

    public void getAllWallet(GetAllWalletCallback getAllWalletCallback) {
        mobileDostUserAPI.getAllWallet().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new GetAllWalletRH(this, getAllWalletCallback));
    }


    public void setDefaultWallet(SetDefaultWalletRequest setDefaultWalletRequest, SetDefaultWalletCallback setDefaultWalletCallback) {
        mobileDostUserAPI.setDefaultWallet(setDefaultWalletRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new SetDefaultWalletRH(this, setDefaultWalletCallback));
    }


    public void deleteWallet(Integer mobileAccountId, DeleteWalletCallback deleteWalletCallback) {
        mobileDostUserAPI.deleteWallet(mobileAccountId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new DeleteWalletRH(this, deleteWalletCallback));
    }

    public void language(String languageCode, LanguageCallback languageCallback) {
        mobileDostUserAPI.language(languageCode).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new LanguageRH(this, languageCallback));
    }


    public void updateUserStatus() {
        mobileDostUserAPI.getUserStatus().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new UserStatusRH(this));
    }
}
