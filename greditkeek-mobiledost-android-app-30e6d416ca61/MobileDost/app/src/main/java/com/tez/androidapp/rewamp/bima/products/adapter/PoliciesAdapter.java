package com.tez.androidapp.rewamp.bima.products.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.fragment.util.optional.TextUtil;
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class PoliciesAdapter extends GenericRecyclerViewAdapter<Policy, PoliciesAdapter.PolicyListener, PoliciesAdapter.PolicyViewHolder> {

    public PoliciesAdapter(@NonNull List<Policy> items, @Nullable PolicyListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public PolicyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.list_item_policy, parent);
        return new PolicyViewHolder(view);
    }

    public interface PolicyListener extends BaseRecyclerViewListener {

        void onClickPolicy(@NonNull Policy policy);

    }

    static class PolicyViewHolder extends BaseViewHolder<Policy, PolicyListener> {

        @BindView(R.id.tvTitle)
        private TezTextView tvTitle;

        @BindView(R.id.tvDescription)
        private TezTextView tvDescription;

        @BindView(R.id.ivIcon)
        private TezImageView ivIcon;

        @BindView(R.id.tvIssueDate)
        private TezTextView tvIssueDate;

        @BindView(R.id.tvActivationDate)
        private TezTextView tvActivationDate;

        @BindView(R.id.tvExpiryDate)
        private TezTextView tvExpiryDate;

        @BindView(R.id.tllActivationDate)
        private TezLinearLayout tllActivationDate;

        private PolicyViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(Policy item, @Nullable PolicyListener listener) {
            super.onBind(item, listener);
            ivIcon.setImageResource(Utility.getInsuranceProductIcon(item.getId()));
            tvTitle.setText(item.getPolicyName());
            tvDescription.setText(item.getPolicyNumber());
            tvIssueDate.setText(item.getDateApplied());
            tvExpiryDate.setText(item.getExpiryDate());
            tvActivationDate.setText(item.getActivationDate() == null ? "-" : item.getActivationDate());
            tllActivationDate.setVisibility(item.getActivationDate() == null ? View.GONE : View.VISIBLE);

            if (listener != null)
                this.setItemViewOnClickListener(v -> listener.onClickPolicy(item));
        }
    }
}
