package com.tez.androidapp.rewamp.bima.products.model;

import androidx.annotation.Nullable;

import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;

import java.io.Serializable;

public class ProductPolicy implements Serializable {

    @Nullable
    private Double dailyCoverage;
    @Nullable
    private String activationDate;
    @Nullable
    private String insuranceStatus;
    private Integer id;
    private String policyNumber;
    private String issueDate;
    private String expiryDate;
    private String insuranceServiceProviderCode;
    private InsuranceServiceProviderContact contact;


    public Integer getId() {
        return id;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    @Nullable
    public Double getDailyCoverage() {
        return dailyCoverage;
    }

    public String getIssueDate() {
        return Utility.getFormattedDateWithUpdatedTimeZone(issueDate, Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT);
    }

    public String getExpiryDate() {
        return Utility.getFormattedDateWithUpdatedTimeZone(expiryDate, Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT);
    }

    @Nullable
    public String getActivationDate() {
        return Utility.getFormattedDateWithUpdatedTimeZone(activationDate, Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT);
    }

    @Nullable
    public String getInsuranceStatus() {
        return insuranceStatus;
    }

    public String getInsuranceServiceProviderCode() {
        return insuranceServiceProviderCode;
    }

    public InsuranceServiceProviderContact getContact() {
        return contact;
    }
}
