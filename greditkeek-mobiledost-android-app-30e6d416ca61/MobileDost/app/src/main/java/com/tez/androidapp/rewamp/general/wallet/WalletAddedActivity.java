package com.tez.androidapp.rewamp.general.wallet;

import android.os.Bundle;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.general.wallet.router.WalletAddedActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

public class WalletAddedActivity extends BaseActivity {

    @BindView(R.id.tvMessage)
    private TezTextView tvMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_added);
        ViewBinder.bind(this);
        tvMessage.setText(getString(R.string.string_mobile_wallet_added_successfully, Utility.getWalletName(getServiceProviderIdFromIntent())));
    }

    private void goBackToWallet() {
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.btContinue)
    private void onClickDone() {
        goBackToWallet();
    }

    @Override
    public void onBackPressed() {

    }

    private int getServiceProviderIdFromIntent() {
        return getIntent().getIntExtra(WalletAddedActivityRouter.SERVICE_PROVIDER_ID, -1);
    }

    @Override
    protected String getScreenName() {
        return "WalletAddedActivity";
    }
}
