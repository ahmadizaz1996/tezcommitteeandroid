package com.tez.androidapp.rewamp.advance.request.presenter;

import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.callbacks.CancelLoanLimitCallback;

public interface ICancelLimitActivityInteractorOutput extends CancelLoanLimitCallback {
}
