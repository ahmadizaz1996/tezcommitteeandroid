package com.tez.androidapp.rewamp.committee.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.tez.androidapp.app.base.response.BaseResponse;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeCreateResponse extends BaseResponse implements Serializable {

    private String message;
    private Data data;

    public static class Data implements Serializable {
        private int id;
        private String name;
        private int tenor;
        private int frequency;
        private int amount;
        private String startDate;
        private String dueDate;
        private String receivingDate;
        private int installment;
        private long userId;
        private List<Object> invites;
        private List<Member> members;


        public static class Member implements Serializable {
            public int userId;
            public String name;
            public String mobilNumber;
            public String joiningDate;


            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMobilNumber() {
                return mobilNumber;
            }

            public void setMobilNumber(String mobilNumber) {
                this.mobilNumber = mobilNumber;
            }

            public String getJoiningDate() {
                return joiningDate;
            }

            public void setJoiningDate(String joiningDate) {
                this.joiningDate = joiningDate;
            }
        }

        public long getUserId() {
            return userId;
        }

        public void setUserId(long userId) {
            this.userId = userId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getTenor() {
            return tenor;
        }

        public void setTenor(int tenor) {
            this.tenor = tenor;
        }

        public int getFrequency() {
            return frequency;
        }

        public void setFrequency(int frequency) {
            this.frequency = frequency;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getDueDate() {
            return dueDate;
        }

        public void setDueDate(String dueDate) {
            this.dueDate = dueDate;
        }

        public String getReceivingDate() {
            return receivingDate;
        }

        public void setReceivingDate(String receivingDate) {
            this.receivingDate = receivingDate;
        }

        public int getInstallment() {
            return installment;
        }

        public void setInstallment(int installment) {
            this.installment = installment;
        }

        public List<Object> getInvites() {
            return invites;
        }

        public void setInvites(List<Object> invites) {
            this.invites = invites;
        }

        public List<Member> getMembers() {
            return members;
        }

        public void setMembers(List<Member> members) {
            this.members = members;
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
