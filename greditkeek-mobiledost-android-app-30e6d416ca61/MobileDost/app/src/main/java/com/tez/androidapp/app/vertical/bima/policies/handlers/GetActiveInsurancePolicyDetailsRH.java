package com.tez.androidapp.app.vertical.bima.policies.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetActiveInsurancePolicyDetailsCallback;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response.GetActiveInsurancePolicyDetailsResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by FARHAN DHANANI on 7/18/2018.
 */
public class GetActiveInsurancePolicyDetailsRH extends BaseRH<GetActiveInsurancePolicyDetailsResponse> {

    private GetActiveInsurancePolicyDetailsCallback getActiveInsurancePolicyDetailsCallback;

    public GetActiveInsurancePolicyDetailsRH(BaseCloudDataStore baseCloudDataStore,
                                             @Nullable GetActiveInsurancePolicyDetailsCallback getActiveInsurancePolicyDetailsCallback) {
        super(baseCloudDataStore);
        this.getActiveInsurancePolicyDetailsCallback = getActiveInsurancePolicyDetailsCallback;
    }

    @Override
    protected void onSuccess(Result<GetActiveInsurancePolicyDetailsResponse> value) {
        GetActiveInsurancePolicyDetailsResponse getActiveInsurancePolicyDetailsResponse =
                value.response().body();
        if (getActiveInsurancePolicyDetailsResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            if (this.getActiveInsurancePolicyDetailsCallback != null)
                this.getActiveInsurancePolicyDetailsCallback.onGetActiveInsurancePolicyDetailsSuccess(
                        getActiveInsurancePolicyDetailsResponse.getMobileUserPolicyDto());
        } else
            onFailure(getActiveInsurancePolicyDetailsResponse.getStatusCode(),
                    getActiveInsurancePolicyDetailsResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.getActiveInsurancePolicyDetailsCallback.onGetActiveInsurancePolicyDetailsFailure(errorCode, message);
    }
}
