package com.tez.androidapp.rewamp.dashboard.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeLoginLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeMetadataLIstener;
import com.tez.androidapp.rewamp.committee.request.CommitteeLoginRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeMetaDataResponse;
import com.tez.androidapp.rewamp.committee.response.CommittteeLoginResponse;
import com.tez.androidapp.rewamp.dashboard.callbacks.DashboardActionCardCallback;
import com.tez.androidapp.rewamp.dashboard.callbacks.DashboardAdvanceCardCallback;
import com.tez.androidapp.rewamp.dashboard.callbacks.UserProfileStatusCallback;
import com.tez.androidapp.rewamp.dashboard.entity.Action;
import com.tez.androidapp.rewamp.dashboard.entity.DashboardCards;
import com.tez.androidapp.rewamp.dashboard.entity.Advance;
import com.tez.androidapp.rewamp.dashboard.presenter.IDashboardActivityInteractorOutput;
import com.tez.androidapp.rewamp.dashboard.response.DashboardActionCardResponse;
import com.tez.androidapp.rewamp.dashboard.response.DashboardAdvanceCardResponse;
import com.tez.androidapp.rewamp.dashboard.response.UserProfileStatusResponse;

public class DashboardActivityInteractor implements IDashboardActivityInteractor {

    private final IDashboardActivityInteractorOutput iDashboardActivityInteractorOutput;


    public DashboardActivityInteractor(IDashboardActivityInteractorOutput iDashboardActivityInteractorOutput) {
        this.iDashboardActivityInteractorOutput = iDashboardActivityInteractorOutput;
    }


    @Override
    public void getDashboardAdvanceCard() {
        UserAuthCloudDataStore.getInstance().getDashboardAdvanceCard(new DashboardAdvanceCardCallback() {
            @Override
            public void onDashboardAdvanceCardSuccess(DashboardAdvanceCardResponse dashboardAdvanceCardResponse) {
                setAdvanceCardInPreference(dashboardAdvanceCardResponse.getAdvance());
                iDashboardActivityInteractorOutput.onDashboardAdvanceCardSuccess(dashboardAdvanceCardResponse);
            }

            @Override
            public void onDashboardAdvanceCardFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getDashboardAdvanceCard();
                else
                    iDashboardActivityInteractorOutput.onDashboardAdvanceCardFailure(errorCode, message);
            }
        });
    }

    @Override
    public void getDashboardActionCard() {
        UserAuthCloudDataStore.getInstance().getDashboardActionCard(new DashboardActionCardCallback() {
            @Override
            public void onDashboardActionCardSuccess(DashboardActionCardResponse dashboardActionCardResponse) {
                setActionCardInPreference(dashboardActionCardResponse.getAction());
                iDashboardActivityInteractorOutput.onDashboardActionCardSuccess(dashboardActionCardResponse);
            }

            @Override
            public void onDashboardActionCardFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getDashboardActionCard();
                else
                    iDashboardActivityInteractorOutput.onDashboardActionCardFailure(errorCode, message);
            }
        });
    }

    @Override
    public void getUserProfileStatus() {
        UserAuthCloudDataStore.getInstance().getUserProfileStatus(new UserProfileStatusCallback() {
            @Override
            public void onUserProfileStatusSuccess(UserProfileStatusResponse userProfileStatusResponse) {
                setProfileStatusInPreference(userProfileStatusResponse.getProfileStatus());
                iDashboardActivityInteractorOutput.onUserProfileStatusSuccess(userProfileStatusResponse);
            }

            @Override
            public void onUserProfileStatusFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getUserProfileStatus();
                else
                    iDashboardActivityInteractorOutput.onUserProfileStatusFailure(errorCode, message);
            }
        });
    }

    @Override
    public void getCommitteeMetada() {
        CommitteeAuthCloudDataStore.getInstance().getCommitteeMetadata(new CommitteeMetadataLIstener() {
            @Override
            public void onCommitteeMetadataSuccess(CommitteeMetaDataResponse committeeMetaDataResponse) {
                iDashboardActivityInteractorOutput.onCommitteeMetadataSuccess(committeeMetaDataResponse);
            }

            @Override
            public void onCommitteeMetadataFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getCommitteeMetada();
                else
                    iDashboardActivityInteractorOutput.onCommitteeMetadataFailure(errorCode, message);
            }
        });
    }

    public void loginCall(CommitteeLoginRequest committeeLoginRequest) {
        UserCloudDataStore.getInstance().loginCall(committeeLoginRequest,new CommitteeLoginLIstener() {
            @Override
            public void onCommitteeLoginSuccess(CommittteeLoginResponse committteeLoginResponse) {
                iDashboardActivityInteractorOutput.onCommitteeLoginSuccess(committteeLoginResponse);
            }

            @Override
            public void onCommitteeLoginFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    loginCall(committeeLoginRequest);
                else
                    iDashboardActivityInteractorOutput.onCommitteeLoginFailure(errorCode, message);
            }
        });
    }

    private void setAdvanceCardInPreference(Advance advance) {
        DashboardCards cards = getPersistedCards();
        cards.setAdvance(advance);
        persistCards(cards);
    }

    private void setActionCardInPreference(Action action) {
        DashboardCards cards = getPersistedCards();
        cards.setAction(action);
        persistCards(cards);
    }

    private void setProfileStatusInPreference(String profileStatus) {
        DashboardCards cards = getPersistedCards();
        cards.setProfileStatus(profileStatus);
        persistCards(cards);
    }

    @NonNull
    private DashboardCards getPersistedCards() {
        DashboardCards cards = MDPreferenceManager.getDashboardCards();
        return cards != null ? cards : new DashboardCards();
    }

    private void persistCards(@NonNull DashboardCards cards) {
        MDPreferenceManager.setDashboardCards(cards);
    }

}
