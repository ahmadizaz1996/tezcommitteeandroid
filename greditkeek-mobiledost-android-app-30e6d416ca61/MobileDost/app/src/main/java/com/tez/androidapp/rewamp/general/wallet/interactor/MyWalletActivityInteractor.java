package com.tez.androidapp.rewamp.general.wallet.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.DeleteWalletCallback;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.GetAllWalletCallback;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.SetDefaultWalletCallback;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.network.dto.request.SetDefaultWalletRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.general.wallet.presenter.IMyWalletActivityInteractorOutput;

import java.util.List;

public class MyWalletActivityInteractor implements IMyWalletActivityInteractor {

    private final IMyWalletActivityInteractorOutput iMyWalletActivityInteractorOutput;

    public MyWalletActivityInteractor(IMyWalletActivityInteractorOutput iMyWalletActivityInteractorOutput) {
        this.iMyWalletActivityInteractorOutput = iMyWalletActivityInteractorOutput;
    }

    @Override
    public void getAllWallets() {
        UserAuthCloudDataStore.getInstance().getAllWallet(new GetAllWalletCallback() {
            @Override
            public void onGetAllWalletSuccess(List<Wallet> wallets) {
                iMyWalletActivityInteractorOutput.onGetAllWalletSuccess(wallets);
            }

            @Override
            public void onGetAllWalletFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getAllWallets();
                else
                    iMyWalletActivityInteractorOutput.onGetAllWalletFailure(errorCode, message);
            }
        });
    }

    @Override
    public void setDefaultWallet(@NonNull Wallet wallet) {
        UserAuthCloudDataStore.getInstance().setDefaultWallet(
                new SetDefaultWalletRequest(wallet.getServiceProviderId(), wallet.getMobileAccountNumber()),
                new SetDefaultWalletCallback() {
                    @Override
                    public void setDefaultWalletSuccess(BaseResponse baseResponse) {
                        iMyWalletActivityInteractorOutput.setDefaultWalletSuccess(baseResponse);
                    }

                    @Override
                    public void setDefaultWalletFailure(int errorCode, String message) {
                        if (Utility.isUnauthorized(errorCode))
                            setDefaultWallet(wallet);
                        else {
                            iMyWalletActivityInteractorOutput.setDefaultWalletFailure(errorCode, message);
                        }
                    }
                });
    }

    @Override
    public void deleteWallet(int mobileAccountId) {
        UserAuthCloudDataStore.getInstance().
                deleteWallet(mobileAccountId, new DeleteWalletCallback() {
                    @Override
                    public void onDeleteWalletSuccess(BaseResponse response) {
                        iMyWalletActivityInteractorOutput.onDeleteWalletSuccess(response);
                    }

                    @Override
                    public void onDeleteWalletFailure(int errorCode, String message) {
                        if (Utility.isUnauthorized(errorCode))
                            deleteWallet(mobileAccountId);
                        else
                            iMyWalletActivityInteractorOutput.onDeleteWalletFailure(errorCode, message);
                    }
                });
    }
}
