package com.tez.androidapp.rewamp.signup.presenter;

import com.tez.androidapp.commons.models.network.dto.request.UserSignUpRequest;

public interface ISignupPermissionActivityPresenter {
    void submitSignUpRequest(UserSignUpRequest userSignUpRequest);
}
