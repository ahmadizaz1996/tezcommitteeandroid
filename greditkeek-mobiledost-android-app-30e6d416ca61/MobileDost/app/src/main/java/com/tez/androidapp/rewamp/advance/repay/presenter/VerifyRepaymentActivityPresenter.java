package com.tez.androidapp.rewamp.advance.repay.presenter;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.InitiateRepaymentCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.InitiateRepaymentRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.RepayLoanRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.InitiateRepaymentResponse;
import com.tez.androidapp.rewamp.advance.repay.view.IVerifyRepaymentActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

public class VerifyRepaymentActivityPresenter extends VerifyEasyPaisaRepaymentActivityPresenter implements IVerifyRepaymentActivityPresenter {

    private final IVerifyRepaymentActivityView iVerifyRepaymentActivityView;

    public VerifyRepaymentActivityPresenter(IVerifyRepaymentActivityView iVerifyRepaymentActivityView) {
        super(iVerifyRepaymentActivityView);
        this.iVerifyRepaymentActivityView = iVerifyRepaymentActivityView;
    }

    @Override
    public void onInitiateRepaymentSuccess(InitiateRepaymentResponse initiateRepaymentResponse) {
        super.onInitiateRepaymentSuccess(initiateRepaymentResponse);
        iVerifyRepaymentActivityView.startTimer();
    }

    @Override
    public void resendCode(int loanId, int mobileAccountId) {
        iVerifyRepaymentActivityView.setPinViewOtpEnabled(false);
        iVerifyRepaymentActivityView.setTvResendCodeEnabled(false);
        iVerifyRepaymentActivityView.setPinViewOtpClearText();
        iVerifyRepaymentActivityView.showTezLoader();

        InitiateRepaymentRequest request = new InitiateRepaymentRequest(loanId, mobileAccountId);

        iVerifyRepaymentActivityInteractor.resendCode(request, new InitiateRepaymentCallback() {
            @Override
            public void onInitiateRepaymentSuccess(InitiateRepaymentResponse initiateRepaymentResponse) {
                repaymentId = initiateRepaymentResponse.getRepaymentId();
                iVerifyRepaymentActivityView.setPinViewOtpEnabled(true);
                iVerifyRepaymentActivityView.dismissTezLoader();
                iVerifyRepaymentActivityView.startTimer();
            }

            @Override
            public void onInitiateRepaymentFailure(int errorCode, String message) {
                iVerifyRepaymentActivityView.dismissTezLoader();
                iVerifyRepaymentActivityView.showError(errorCode,
                        iVerifyRepaymentActivityView.getFormattedErrorMessage(ResponseStatusCode.getDescriptionFromErrorCode(errorCode)),
                        (dialog, which) -> iVerifyRepaymentActivityView.startTimer());
            }
        });
    }

    @Override
    protected void setPinInRequest(@NonNull RepayLoanRequest request, @NonNull String pin) {
        request.setOtp(pin);
    }
}
