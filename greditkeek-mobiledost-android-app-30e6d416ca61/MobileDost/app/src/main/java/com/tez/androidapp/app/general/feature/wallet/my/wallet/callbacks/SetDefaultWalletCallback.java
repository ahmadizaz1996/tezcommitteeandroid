package com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by Rehman Murad Ali on 12/26/2017.
 */

public interface SetDefaultWalletCallback {

    void setDefaultWalletSuccess(BaseResponse baseResponse);

    void setDefaultWalletFailure(int errorCode, String message);
}
