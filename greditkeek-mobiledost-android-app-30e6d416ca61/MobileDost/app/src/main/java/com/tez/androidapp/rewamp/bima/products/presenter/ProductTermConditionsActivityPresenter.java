package com.tez.androidapp.rewamp.bima.products.presenter;

import android.text.Html;
import android.view.View;

import com.tez.androidapp.rewamp.bima.products.interactor.IProductTermConditionsActivityInteractor;
import com.tez.androidapp.rewamp.bima.products.interactor.IProductTermConditionsActivityInteractorOutput;
import com.tez.androidapp.rewamp.bima.products.interactor.ProductTermConditionsActivityInteractor;
import com.tez.androidapp.rewamp.bima.products.response.ProductTermConditionsResponse;
import com.tez.androidapp.rewamp.bima.products.view.IProductTermConditionsActivityView;
import com.tez.androidapp.rewamp.signup.TermsAndCondition;

import java.util.ArrayList;
import java.util.List;

public class ProductTermConditionsActivityPresenter implements IProductTermConditionsActivityPresenter,
        IProductTermConditionsActivityInteractorOutput {

    private final IProductTermConditionsActivityView view;
    private final IProductTermConditionsActivityInteractor interactor;

    private String htmlTerms;

    public ProductTermConditionsActivityPresenter(IProductTermConditionsActivityView view) {
        this.view = view;
        this.interactor = new ProductTermConditionsActivityInteractor(this);
    }

    @Override
    public void getProductTermConditions(int productId) {
        view.setNsvContentVisibility(View.GONE);
        view.setShimmerVisibility(View.VISIBLE);
        interactor.getProductTermConditions(productId);
    }

    @Override
    public void getProductDetailedTermCondition(int productId) {
        if (htmlTerms == null) {
            view.showTezLoader();
            interactor.getProductDetailedTermCondition(productId);
        } else
            view.setTermCondition(htmlTerms);
    }

    @Override
    public void onGetTermsConditionsSuccess(ProductTermConditionsResponse response) {
        view.setShimmerVisibility(View.GONE);
        view.setNsvContentVisibility(View.VISIBLE);
        List<TermsAndCondition> termsAndConditions = new ArrayList<>();
        for (String term : response.getTermsConditions())
            termsAndConditions.add(new TermsAndCondition(term));
        view.setAdapter(termsAndConditions);
    }

    @Override
    public void onGetTermsConditionsFailure(int errorCode, String message) {
        view.setShimmerVisibility(View.GONE);
        view.showError(errorCode, ((dialog, which) -> view.finishActivity()));
    }

    @Override
    public void onGetProductDetailTermConditionSuccess(String htmlTermCondition) {
        this.htmlTerms = htmlTermCondition;
        view.setTermCondition(htmlTermCondition);
        view.dismissTezLoader();
    }

    @Override
    public void onGetProductDetailTermConditionFailure(int errorCode, String message) {
        view.dismissTezLoader();
        view.showError(errorCode);
    }
}
