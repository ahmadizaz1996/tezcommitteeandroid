package com.tez.androidapp.rewamp.profile.trust.presenter;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetCnicUploadsCallback;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.profile.complete.router.CompleteProfileCnicUploadActivityRouter;

import net.tez.logger.library.utils.TextUtil;

public interface IProfileTrustActivityInteractorOutput extends GetCnicUploadsCallback {

    default boolean[] getFlagsForPicturesNeedsToUploaded(String[] cnicUploads){
        boolean [] flagsForPicturesNeedToUploaded = new boolean[3];
        for(String s: cnicUploads){
            if(TextUtil.equals(s, CompleteProfileCnicUploadActivityRouter.IS_CNIC_FRONT_PIC_TO_BE_UPLOADED)){
                flagsForPicturesNeedToUploaded[0] = true;
            } else if(TextUtil.equals(s, CompleteProfileCnicUploadActivityRouter.IS_CNIC_BACK_PIC_TO_BE_UPLOADED)){
                flagsForPicturesNeedToUploaded[1] = true;
            } else if(TextUtil.equals(s, CompleteProfileCnicUploadActivityRouter.IS_CNIC_SELFIE_TO_BE_UPLOADED)){
                flagsForPicturesNeedToUploaded[2] = true;
            }
        }
        return flagsForPicturesNeedToUploaded;
    }
}
