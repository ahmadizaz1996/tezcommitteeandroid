package com.tez.androidapp.rewamp.general.profile.interactor;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.AccountLinkRequest;

public interface IMyProfileActivityInteractor {
    void getProfileDetails();

    void linkUserAccount(AccountLinkRequest accountLinkRequest);
}
