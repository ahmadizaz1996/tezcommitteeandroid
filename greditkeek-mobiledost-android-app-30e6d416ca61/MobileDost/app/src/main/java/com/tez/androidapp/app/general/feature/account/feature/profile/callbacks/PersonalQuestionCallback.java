package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Question;

import java.util.List;

public interface PersonalQuestionCallback {

    void onPersonalQuestionSuccess(List<Question> data);
    void onPersonalQuestionFailure(int errorCode, String message);
}
