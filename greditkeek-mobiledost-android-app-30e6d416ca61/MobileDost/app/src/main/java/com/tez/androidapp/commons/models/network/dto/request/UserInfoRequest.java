package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.BuildConfig;
import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 2/7/2017.
 */

public class UserInfoRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/info";
//    public static final String METHOD_NAME = "v2/user/info";
    private String principalName;
    private String socialId;
    private String mobileNumber;
    private String appVersion;
    private Integer principalTypeId;

    public UserInfoRequest(String mobileNumber, String socialId, Integer principalTypeId) {
        this.mobileNumber = mobileNumber;
        this.socialId = socialId;
        this.principalName = mobileNumber;
        this.principalTypeId = principalTypeId;
        appVersion = BuildConfig.VERSION_NAME;
    }

    public Integer getPrincipalTypeId() {
        return principalTypeId;
    }

    public String getSocialId() {
        return socialId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}
