package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.InsuranceClaimDocumentFileDto;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.InsuranceDocumentDto;

/**
 * Created by FARHAN DHANANI on 7/2/2018.
 */
public class UploadClaimDocumentResponse extends BaseResponse {

    private InsuranceClaimDocumentFileDto insuranceClaimDocumentFileDto;
    private InsuranceDocumentDto insuranceDocumentDto;

    public InsuranceDocumentDto getInsuranceDocumentDto() {
        return insuranceDocumentDto;
    }

    public InsuranceClaimDocumentFileDto getInsuranceClaimDocumentFileDto() {
        return insuranceClaimDocumentFileDto;
    }
}
