package com.tez.androidapp.commons.models.extract;

import android.accounts.Account;

/**
 * Created  on 12/8/2016.
 */

public class ExtractedAccount {

    private String accountID;
    private String type;

    public ExtractedAccount(String accountID, String type) {
        this.accountID = accountID;
        this.type = type;
    }

    public ExtractedAccount(Account account) {
        this.accountID = account.name;
        this.type = account.type;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
