package com.tez.androidapp.rewamp.committee.view;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.rewamp.committee.listener.InviteBottomSheetListener;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.presenter.CommitteeInviteActivityPresenter;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeInviteContactsActivityRouter;
import com.tez.androidapp.rewamp.committee.shared.api.client.CommitteeAPI;
import com.tez.androidapp.rewamp.util.RetrofitClient;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter.COMMITTEE_DATA;

public class CommitteeInviteActivity extends BaseActivity implements DoubleTapSafeOnClickListener,
        ICommitteeInviteActivityView, InviteBottomSheetListener {

    public static final int REQUEST_CODE = 1478;


    @BindView(R.id.inviteMember)
    TezButton inviteButton;

    private final CommitteeInviteActivityPresenter iCommitteeActivityPresenter;
    private CommitteePackageModel committeePackage;

    public CommitteeInviteActivity() {
        iCommitteeActivityPresenter = new CommitteeInviteActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee_invitation);
        ViewBinder.bind(this);
        fetchExtras();
        initClickListeners();
    }

    private void fetchExtras() {
        Intent intent = getIntent();
        if (intent != null
                && intent.getExtras() != null
                && intent.getExtras().containsKey(COMMITTEE_DATA)) {
            committeePackage = (CommitteePackageModel) intent.getExtras().getSerializable(COMMITTEE_DATA);
        }
    }

    private void initClickListeners() {
        inviteButton.setDoubleTapSafeOnClickListener(this);
    }

    @Override
    protected String getScreenName() {
        return CommitteeInviteActivity.class.getSimpleName();
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {
            case R.id.inviteMember:
//                InviteBottomSheetDialog inviteBottomSheetDialog = new InviteBottomSheetDialog();
//                inviteBottomSheetDialog.setListener(this);
//                inviteBottomSheetDialog.show(getSupportFragmentManager(), InviteBottomSheetDialog.class.getSimpleName());
                CommitteeInviteContactsActivityRouter.createInstance().setDependenciesAndRoute(this, committeePackage);
                break;
        }
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void onInvite() {
        CommitteeCompletionActivityRouter.createInstance().setDependenciesAndRoute(this, committeePackage);
    }

    @Override
    public void onDialogOptionClick(InviteBottomSheetDialog.InviteOption contact) {
        Utility.openContacts(this, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK
                && requestCode == CommitteeInviteActivity.REQUEST_CODE) {
            Cursor cursor = null;
            String phoneNumber = "";
            List<String> allNumbers = new ArrayList<String>();
            int phoneIdx = 0;
            Uri result = data.getData();
            String id = result.getLastPathSegment();
            cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                    new String[]{id}, null);
            phoneIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    phoneNumber = cursor.getString(phoneIdx);
                    allNumbers.add(phoneNumber);
                    cursor.moveToNext();
                }
            } else {
                //no results actions
            }

            if (allNumbers != null && allNumbers.size() > 0) {
/*                List<CommitteeInviteRequest> committeeInviteRequestArrayList = new ArrayList<>();
                CommitteeInviteRequest committeeInviteRequest = new CommitteeInviteRequest();
                committeeInviteRequest.contactNumber = allNumbers.get(0);
                committeeInviteRequestArrayList.add(committeeInviteRequest);*/

//                CommitteeInviteRequest.PersonInfo personInfo = new CommitteeInviteRequest.PersonInfo();
//                personInfo.contactNumber = allNumbers.get(0);
//                committeeInviteRequest.personInfo.add(personInfo);

//                iCommitteeActivityPresenter.getInvite(committeeInviteRequestArrayList);
//                CommitteeCompletionActivityRouter.createInstance().setDependenciesAndRoute(this, committeePackage);
            }
        }
        if (resultCode == RESULT_OK
                && requestCode == CommitteeInviteContactsActivityRouter.CONTACT_FETCH) {
            CommitteeInviteRequest committeeInviteRequestArrayList = data.getParcelableExtra(CommitteeInviteContactsActivity.INVITEES_LIST);

            if (committeeInviteRequestArrayList != null) {
                CommitteeCompletionActivityRouter.createInstance().setDependenciesAndRoute(CommitteeInviteActivity.this, committeePackage, committeeInviteRequestArrayList);

/*                CommitteeAPI committeeAPI = RetrofitClient.createAPI();
                Call<CommitteeInviteResponse> responseCall = committeeAPI.getInvites("Bearer " + MDPreferenceManager.getAccessToken().getAccessToken(),
                        committeeInviteRequestArrayList);
                responseCall.enqueue(new Callback<CommitteeInviteResponse>() {
                    @Override
                    public void onResponse(Call<CommitteeInviteResponse> call, Response<CommitteeInviteResponse> response) {
                        Log.d("RESPONSE", response.toString());
                        if (response.body().getStatusCode() == 2001)
                            CommitteeCompletionActivityRouter.createInstance().setDependenciesAndRoute(CommitteeInviteActivity.this, committeePackage);
                        else
                            Toast.makeText(CommitteeInviteActivity.this, response.body().message, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<CommitteeInviteResponse> call, Throwable t) {
                        Log.d("RESPONSE", t.toString());
                        Toast.makeText(CommitteeInviteActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });*/
            } else {
                Toast.makeText(this, "Failed to send Invites", Toast.LENGTH_SHORT).show();
            }
        }
    }
}