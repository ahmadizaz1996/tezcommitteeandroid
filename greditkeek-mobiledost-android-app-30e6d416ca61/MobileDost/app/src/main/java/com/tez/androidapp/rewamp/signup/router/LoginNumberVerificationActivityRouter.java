package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.view.LoginNumberVerificationActivity;

public class LoginNumberVerificationActivityRouter extends BaseActivityRouter {

    public static final String SOCIAL_ID = "SOCIAL_ID";
    public static final String SOCIAL_TYPE = "SOCIAL_TYPE";
    public static final String PIN = "PIN";

    public static LoginNumberVerificationActivityRouter createInstance() {
        return new LoginNumberVerificationActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @Nullable String mobileNumber,
                                        @NonNull String pin,
                                        @Nullable String socialId,
                                        @Nullable Integer socialType) {
        Intent intent = createIntent(from);
        intent.putExtra(NumberVerificationActivityRouter.MOBILE_NUMBER, mobileNumber);
        intent.putExtra(NumberVerificationActivityRouter.PRINCIPLE, mobileNumber);
        intent.putExtra(SOCIAL_ID, socialId);
        intent.putExtra(SOCIAL_TYPE, socialType);
        intent.putExtra(PIN, pin);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, LoginNumberVerificationActivity.class);
    }
}
