package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeActivity;
import com.tez.androidapp.rewamp.committee.view.JoinCommitteeVerificationActivity;

public class JoinCommitteeVerificationActivityRouter extends BaseActivityRouter {


    public static final String COMMITTEE_CHECK_INVITES_RESPONSE = "COMMITTEE_CHECK_INVITES_RESPONSE";

    public static JoinCommitteeVerificationActivityRouter createInstance() {
        return new JoinCommitteeVerificationActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteeCheckInvitesResponse committeeCheckInvitesResponse) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putParcelable(COMMITTEE_CHECK_INVITES_RESPONSE, committeeCheckInvitesResponse);
        intent.putExtras(bundle);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, JoinCommitteeVerificationActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

}
