package com.tez.androidapp.rewamp.bima.products.policy.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.bima.products.policy.view.HospitalCoverageActivity;

public class HospitalCoverageActivityRouter extends BaseProductActivityRouter {


    public static HospitalCoverageActivityRouter createInstance() {
        return new HospitalCoverageActivityRouter();
    }

    @NonNull
    @Override
    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, HospitalCoverageActivity.class);
    }
}
