package com.tez.androidapp.commons.utils.file.util;

import android.content.DialogInterface;

import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

/**
 * Created by FARHAN DHANANI on 7/10/2018.
 */
public interface IFilePickerUtilityPermissionCallBack extends DialogInterface.OnCancelListener, MultiplePermissionsListener {
    void onPickPhotoClicked();
    void onPickDocClicked();
    void onCameraClicked();
}
