package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface CommitteeInviteListener {

    void onInviteSuccess(CommitteeInviteResponse committeeInviteResponse);

    void onInviteFailure(int errorCode, String message);
}
