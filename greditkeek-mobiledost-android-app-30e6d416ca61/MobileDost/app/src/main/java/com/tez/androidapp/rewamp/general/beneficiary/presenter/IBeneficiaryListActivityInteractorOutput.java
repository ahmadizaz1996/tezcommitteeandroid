package com.tez.androidapp.rewamp.general.beneficiary.presenter;

import com.tez.androidapp.app.general.feature.account.feature.beneficiary.callbacks.SetDefaultBeneficiaryCallback;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetAdvanceBimaBeneficiaryCallback;
import com.tez.androidapp.rewamp.general.beneficiary.listener.GetBeneficiariesListener;

public interface IBeneficiaryListActivityInteractorOutput extends GetBeneficiariesListener, SetAdvanceBimaBeneficiaryCallback, SetDefaultBeneficiaryCallback {
}
