package com.tez.androidapp.commons.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;

import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.extract.ExtractionUtil;

import net.tez.validator.library.utils.TextUtil;

/**
 * WifiReceiver to fetch all the WiFis the device has connected to for DataLift.
 * <p>
 * Created  on 1/23/2017.
 */

public class WifiReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            if (TextUtil.equals(WifiManager.WIFI_STATE_CHANGED_ACTION, intent.getAction())) {
                WifiManager wifiManager = (WifiManager) MobileDostApplication.getInstance()
                        .getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                if (wifiManager != null
                        && wifiManager.isWifiEnabled()
                        && (System.currentTimeMillis() - Constants.WEEK_MILLIS >= MDPreferenceManager.getLastSentWiFiTime())) {
                    ExtractionUtil extractionUtil = new ExtractionUtil(context);
                    String extractedWifi = extractionUtil.extractWiFisUnsafe();
                    if (!extractedWifi.isEmpty()) {
                        MDPreferenceManager.setLastSentWiFiTime(System.currentTimeMillis());
                        MDPreferenceManager.setWiFiData(extractedWifi);
                    }
                }
            }
        } catch (Exception e){
            //Unable to start wifiReciever
        }
    }

    public void register(Context context) {
        context.registerReceiver(this, new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));
    }
}
