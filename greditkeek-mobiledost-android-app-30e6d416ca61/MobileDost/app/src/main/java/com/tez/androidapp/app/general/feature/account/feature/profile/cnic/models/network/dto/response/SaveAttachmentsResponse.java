package com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;

import java.util.List;

/**
 * Created  on 2/17/2017.
 */

public class SaveAttachmentsResponse extends BaseResponse {

    private List<String> persistedFiles;

    public List<String> getPersistedFiles() {
        return persistedFiles;
    }

    public void setPersistedFiles(List<String> persistedFiles) {
        this.persistedFiles = persistedFiles;
    }

    public boolean areAllFilesPersisted() {
        return isFrontCNICPersisted() && isBackCNICPersisted() && isSelfiePersisted();
    }

    private boolean isFrontCNICPersisted() {
        final String FRONT_CNIC = "frontCnicCopy";
        return persistedFiles.contains(FRONT_CNIC);
    }

    private boolean isBackCNICPersisted() {
        final String BACK_CNIC = "backCnicCopy";
        return persistedFiles.contains(BACK_CNIC);
    }

    private boolean isSelfiePersisted() {
        final String SELFIE = "selfie";
        return persistedFiles.contains(SELFIE);
    }
}
