package com.tez.androidapp.rewamp.advance.limit.presenter;

public interface ILimitAssignedActivityPresenter {

    void getLoanLimit();
}
