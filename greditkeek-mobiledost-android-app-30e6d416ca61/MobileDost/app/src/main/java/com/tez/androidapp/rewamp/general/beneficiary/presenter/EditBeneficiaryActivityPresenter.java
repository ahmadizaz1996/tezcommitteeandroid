package com.tez.androidapp.rewamp.general.beneficiary.presenter;

import androidx.annotation.Nullable;

import com.tez.androidapp.app.vertical.bima.add.beneficiary.models.network.dto.request.AdvanceBimaBeneficiaryRequest;
import com.tez.androidapp.rewamp.general.beneficiary.interactor.EditBeneficiaryActivityInteractor;
import com.tez.androidapp.rewamp.general.beneficiary.interactor.IEditBeneficiaryActivityInteractor;
import com.tez.androidapp.rewamp.general.beneficiary.view.IEditBeneficiaryActivityView;

public class EditBeneficiaryActivityPresenter implements IEditBeneficiaryActivityPresenter, IEditBeneficiaryActivityInteractorOutput {

    private final IEditBeneficiaryActivityView iEditBeneficiaryActivityView;
    private final IEditBeneficiaryActivityInteractor iEditBeneficiaryActivityInteractor;

    public EditBeneficiaryActivityPresenter(IEditBeneficiaryActivityView iEditBeneficiaryActivityView) {
        this.iEditBeneficiaryActivityView = iEditBeneficiaryActivityView;
        this.iEditBeneficiaryActivityInteractor = new EditBeneficiaryActivityInteractor(this);
    }

    @Override
    public void setAdvanceBimaBeneficiary(@Nullable Integer beneficiaryId,
                                          @Nullable Integer mobileUserInsurancePolicyBeneficiaryId,
                                          Integer relationShipId,
                                          String name,
                                          String mobileNumber,
                                          boolean informCheck) {
        iEditBeneficiaryActivityView.showTezLoader();
        AdvanceBimaBeneficiaryRequest request = new AdvanceBimaBeneficiaryRequest();
        request.setBeneficiaryId(beneficiaryId);
        request.setMobileUserInsurancePolicyBeneficiaryId(mobileUserInsurancePolicyBeneficiaryId);
        request.setRelationshipId(relationShipId);
        request.setName(name);
        request.setMobileNumber(mobileNumber);
        request.setInformCheck(informCheck);
        iEditBeneficiaryActivityInteractor.setAdvanceBimaBeneficiary(request);
    }

    @Override
    public void onSetAdvanceBimaBeneficiarySuccess() {
        iEditBeneficiaryActivityView.finishActivity();
    }

    @Override
    public void onSetAdvanceBimaBeneficiaryFailure(int statusCode, String message) {
        iEditBeneficiaryActivityView.dismissTezLoader();
        iEditBeneficiaryActivityView.showError(statusCode);
    }
}
