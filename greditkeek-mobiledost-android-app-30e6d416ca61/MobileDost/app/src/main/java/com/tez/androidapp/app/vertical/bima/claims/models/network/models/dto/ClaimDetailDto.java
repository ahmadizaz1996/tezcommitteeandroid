package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by FARHAN DHANANI on 6/29/2018.
 */
public class ClaimDetailDto implements Serializable {
    private List<InsuranceAnswerDto> insuranceAnswerDtoList;

    private List<ClaimedDocumentDetail> insuranceDocumentDtoList;

    private Double remainingAmount;

    private Boolean amountSelectionRequired;

    public Double getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(Double remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public List<InsuranceAnswerDto> getInsuranceAnswerDtoList() {
        return insuranceAnswerDtoList;
    }

    public void setInsuranceAnswerDto(List<InsuranceAnswerDto> insuranceAnswerDto) {
        this.insuranceAnswerDtoList = insuranceAnswerDto;
    }

    public List<ClaimedDocumentDetail> getInsuranceDocumentDtoList() {
        return insuranceDocumentDtoList;
    }

    public void setInsuranceDocumentDtoList(List<ClaimedDocumentDetail> insuranceDocumentDtoList) {
        this.insuranceDocumentDtoList = insuranceDocumentDtoList;
    }

    public Boolean getAmountSelectionRequired() {
        return amountSelectionRequired;
    }

    public void setAmountSelectionRequired(Boolean amountSelectionRequired) {
        this.amountSelectionRequired = amountSelectionRequired;
    }
}
