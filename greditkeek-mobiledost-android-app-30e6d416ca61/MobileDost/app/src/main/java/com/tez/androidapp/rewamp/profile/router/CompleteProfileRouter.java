package com.tez.androidapp.rewamp.profile.router;

import android.content.Intent;

import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public abstract class CompleteProfileRouter extends BaseActivityRouter {
    public final static String ROUTE_KEY = "ROUTE_KEY";
    public final static int ROUTE_TO_LOAN = 1;
    public final static int ROUTE_TO_INSURANCE = 2;
    public final static int ROUTE_TO_DEFAULT = -1;

    protected void addAfterCompletionRoute(Intent intent, int routeKey) {
        intent.putExtra(ROUTE_KEY, routeKey);
    }

    protected void addAfterCompletionRoute(Intent intent) {
        intent.putExtra(ROUTE_KEY, ROUTE_TO_DEFAULT);
    }
}
