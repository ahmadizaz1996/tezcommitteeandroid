package com.tez.androidapp.rewamp.signup.presenter;

public interface IIntroActivityPresenter {
    void callUserInfo(String mobileNumber, String socialId, int socialType);
}
