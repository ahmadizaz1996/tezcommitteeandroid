package com.tez.androidapp.rewamp;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleableRes;
import androidx.appcompat.content.res.AppCompatResources;

/**
 * Created by VINOD KUMAR on 7/26/2019.
 */
public final class TezDrawableHelper {

    private TezDrawableHelper() {
    }

    @Nullable
    public static Drawable getDrawable(@NonNull Context context,
                                       @NonNull TypedArray typedArray,
                                       @StyleableRes int index) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            return typedArray.getDrawable(index);
        else {
            int resId = typedArray.getResourceId(index, -1);
            if (resId != -1)
                return AppCompatResources.getDrawable(context, resId);
        }
        return null;
    }

    public static Drawable getDrawable(@NonNull Context context, @DrawableRes int resId) {
        return AppCompatResources.getDrawable(context, resId);
    }
}
