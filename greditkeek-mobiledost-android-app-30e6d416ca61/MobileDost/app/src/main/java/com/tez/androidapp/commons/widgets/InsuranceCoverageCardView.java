package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class InsuranceCoverageCardView extends TezCardView implements View.OnClickListener {

    private static final double STEP_SIZE = 1000;

    @BindView(R.id.tvAmount)
    private TezTextView tvAmount;

    @BindView(R.id.layoutDecrease)
    private TezLinearLayout layoutDecrease;

    @BindView(R.id.layoutIncrease)
    private TezLinearLayout layoutIncrease;

    @BindView(R.id.ivDecrease)
    private TezImageView ivDecrease;

    @BindView(R.id.ivIncrease)
    private TezImageView ivIncrease;

    @BindView(R.id.tvHeading)
    private TezTextView tvHeading;

    @BindView(R.id.tvAdvanceLimit)
    private TezTextView tvAdvanceLimit;

    @BindView(R.id.groupAmount)
    private Group groupAmount;

    private AmountChangeListener amountChangeListener;

    private double maxCoverage;
    private double minCoverage;
    private double coverage;

    public InsuranceCoverageCardView(@NonNull Context context) {
        super(context);
    }

    public InsuranceCoverageCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public InsuranceCoverageCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        inflate(context, R.layout.card_view_advance_request, this);
        init();
    }

    public void setHeading(String text) {
        tvHeading.setText(text);
    }

    public void setGroupAmountVisibility(int visibility) {
        groupAmount.setVisibility(visibility);
    }

    private void init() {
        ViewBinder.bind(this, this);
        TezTextView tvHeading = findViewById(R.id.tvHeading);
        tvHeading.setText(R.string.enter_daily_coverage);
        this.tvAmount.setText(getContext().getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(1000)));
        this.layoutDecrease.setOnClickListener(this);
        this.layoutIncrease.setOnClickListener(this);
    }

    public double getCoverage() {
        return coverage;
    }

    public void setRequestAdvanceTextChangeListener(AmountChangeListener listener) {
        this.amountChangeListener = listener;
    }

    public void setDetails(@Nullable String description, double minCoverage, double maxCoverage) {
        this.minCoverage = minCoverage;
        this.maxCoverage = maxCoverage;
        this.coverage = minCoverage;
        this.tvAmount.setText(getContext().getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(coverage)));
        boolean isNotMinMaxSame = minCoverage != maxCoverage;
        this.setIvIncreaseEnabled(isNotMinMaxSame);
        this.setIvDecreaseEnabled(isNotMinMaxSame);

        String splitter = "$";
        if (description != null) {
            int start = description.indexOf(splitter);
            if (start != -1) {
                int end = description.lastIndexOf(splitter) - 1;
                description = description.replace(splitter, "");
                Spannable spannable = new SpannableString(description);
                spannable.setSpan(new ForegroundColorSpan(Utility.getColorFromResource(R.color.textViewTextColorGreen)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvAdvanceLimit.setText(spannable);
            } else
                tvAdvanceLimit.setText(description);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.layoutDecrease:
                if (coverage - STEP_SIZE >= minCoverage)
                    updateCoverageAmount(coverage - STEP_SIZE);
                break;

            case R.id.layoutIncrease:
                if (coverage + STEP_SIZE <= maxCoverage)
                    updateCoverageAmount(coverage + STEP_SIZE);
                break;
        }
    }

    private void updateCoverageAmount(double amount) {
        this.coverage = amount;
        tvAmount.setText(getContext().getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(amount)));
        amountChangeListener.onAmountChange(amount);
        this.setIvIncreaseEnabled(amount + STEP_SIZE <= maxCoverage);
        this.setIvDecreaseEnabled(amount - STEP_SIZE >= minCoverage);
    }

    private void setIvIncreaseEnabled(boolean enabled) {
        this.ivIncrease.setImageResource(enabled ? R.drawable.ic_increase : R.drawable.ic_increase_disabled);
    }

    private void setIvDecreaseEnabled(boolean enabled) {
        this.ivDecrease.setImageResource(enabled ? R.drawable.ic_decrease : R.drawable.ic_decrease_disabled);
    }

    @FunctionalInterface
    public interface AmountChangeListener {
        void onAmountChange(double amount);
    }
}

