package com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.LoanQuestion;

/**
 * Created  on 2/21/2017.
 */

public class LoanQuestionsResponse extends BaseResponse {

    private LoanQuestion data;

    public LoanQuestion getLoanQuestions() {
        return data;
    }

    public void setLoanQuestions(LoanQuestion loanQuestions) {
        this.data = loanQuestions;
    }
}
