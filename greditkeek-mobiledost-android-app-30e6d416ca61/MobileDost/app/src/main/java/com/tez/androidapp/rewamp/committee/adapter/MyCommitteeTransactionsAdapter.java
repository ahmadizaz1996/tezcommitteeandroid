package com.tez.androidapp.rewamp.committee.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponseMemberList;
import com.tez.androidapp.rewamp.committee.response.CommitteeResponseFilterTransactionList;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;

import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/

public class MyCommitteeTransactionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> /*GenericRecyclerViewAdapter<CommitteeFilterResponseMemberList,
        MyCommitteeUpdatesAdapter.MyCommitteeInviteesListener,
        MyCommitteeUpdatesAdapter.MyCommitteeMemberViewHolder>*/ {

    public static final int HEADER_ITEM = 0;
    public static final int NORMAL_ITEM = 1;
    private final List<CommitteeResponseFilterTransactionList> items;


    public MyCommitteeTransactionsAdapter(@NonNull List<CommitteeResponseFilterTransactionList> items) {
        this.items = items;
    }

    @Override
    public int getItemViewType(int position) {
        if (this.items.get(position).isHeader())
            return HEADER_ITEM;
        else
            return NORMAL_ITEM;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == NORMAL_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_committee_invitees, parent, false);
            return new MyCommitteeMemberViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate
                    (R.layout.header_date_item, parent, false);
            return new MyCommitteeMemberHeaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CommitteeResponseFilterTransactionList committeeResponseFilterTransactionList = this.items.get(position);
        if (holder instanceof MyCommitteeMemberViewHolder) {
            MyCommitteeMemberViewHolder myCommitteeMemberViewHolder = (MyCommitteeMemberViewHolder) holder;

            myCommitteeMemberViewHolder.tvName.setText(committeeResponseFilterTransactionList.getUsername());
            myCommitteeMemberViewHolder.tvMessage.setVisibility(View.GONE);
            myCommitteeMemberViewHolder.tvStatus.setVisibility(View.GONE);
            myCommitteeMemberViewHolder.tvDate.setText(committeeResponseFilterTransactionList.getTransactionDate().split("T")[0]);
        } else {
            MyCommitteeMemberHeaderViewHolder myCommitteeMemberHeaderViewHolder = (MyCommitteeMemberHeaderViewHolder) holder;

            myCommitteeMemberHeaderViewHolder.datTxtView.setText(committeeResponseFilterTransactionList.getJoiningDate());
        }
    }

    public interface MyCommitteeInviteesListener extends BaseRecyclerViewListener {
        void onClickInvitees(@NonNull MyCommitteeResponse.Committee.Invite invite);
    }

    static class MyCommitteeMemberViewHolder extends /*BaseViewHolder<CommitteeFilterResponseMemberList, MyCommitteeInviteesListener>*/ RecyclerView.ViewHolder {

        @BindView(R.id.contactCardView)
        private TezCardView contactCardView;

        @BindView(R.id.ivImage)
        private TezImageView ivImage;

        @BindView(R.id.tv_name)
        private TezTextView tvName;

        @BindView(R.id.tv_message)
        private TezTextView tvMessage;

        @BindView(R.id.tv_date)
        private TezTextView tvDate;

        @BindView(R.id.tv_status)
        private TezTextView tvStatus;


        public MyCommitteeMemberViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        /*@Override
        public void onBind(CommitteeFilterResponseMemberList item, @Nullable MyCommitteeInviteesListener listener) {
            super.onBind(item, listener);
        }*/
    }

    static class MyCommitteeMemberHeaderViewHolder extends /*BaseViewHolder<String, MyCommitteeInviteesListener>*/ RecyclerView.ViewHolder {

        @BindView(R.id.date_txtView)
        private TezTextView datTxtView;


        public MyCommitteeMemberHeaderViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        /*@Override
        public void onBind(String item, @Nullable MyCommitteeInviteesListener listener) {
            super.onBind(item, listener);


        }*/
    }
}
