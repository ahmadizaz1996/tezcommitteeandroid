package com.tez.androidapp.rewamp.committee.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeCreationLIstener;
import com.tez.androidapp.rewamp.committee.listener.JoinCommitteeVerificationLIstener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class JoinCommitteeVerificationRH extends BaseRH<JoinCommitteeVerificationResponse> {

    private final JoinCommitteeVerificationLIstener listener;

    public JoinCommitteeVerificationRH(BaseCloudDataStore baseCloudDataStore, JoinCommitteeVerificationLIstener joinCommitteeVerificationLIstener) {
        super(baseCloudDataStore);
        this.listener = joinCommitteeVerificationLIstener;
    }

    @Override
    protected void onSuccess(Result<JoinCommitteeVerificationResponse> value) {
        JoinCommitteeVerificationResponse joinCommitteeVerificationResponse = value.response().body();
        if (joinCommitteeVerificationResponse != null && joinCommitteeVerificationResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onJoinCommitteeVerificationSuccess(joinCommitteeVerificationResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onJoinCommitteeVerificationFailure(errorCode, message);
    }
}
