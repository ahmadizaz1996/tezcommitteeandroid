package com.tez.androidapp.rewamp.committee.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeContributeListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeInviteListener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeWalletsRH extends BaseRH<CommitteeWalletResponse> {

    private final CommitteeContributeListener listener;

    public CommitteeWalletsRH(BaseCloudDataStore baseCloudDataStore, CommitteeContributeListener committeeContributeListener) {
        super(baseCloudDataStore);
        this.listener = committeeContributeListener;
    }

    @Override
    protected void onSuccess(Result<CommitteeWalletResponse> value) {
        CommitteeWalletResponse committeeWalletResponse = value.response().body();
        if (committeeWalletResponse != null && committeeWalletResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onGetWalletSuccess(committeeWalletResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onGetWalletFailure(errorCode, message);
    }
}
