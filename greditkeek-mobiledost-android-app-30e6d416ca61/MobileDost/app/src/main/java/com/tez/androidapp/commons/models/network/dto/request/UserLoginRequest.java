package com.tez.androidapp.commons.models.network.dto.request;

import android.text.TextUtils;

import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 12/15/2016.
 */

public class UserLoginRequest extends BaseRequest {

    public static final String METHOD_NAME = "v2/user/login";

    private String principalName;
    private String password;
    private String imei;
    private String deviceBrand;
    private String deviceModel;
    private String networkOperator = Constants.NETWORK_NO_SIM;
    private String deviceOs;
    private String defaultBrowser;
    private String appVersion;
    private String deviceKey;
    private Double appInstalledLat;
    private Double appInstalledLong;
    private String socialId;
    private Integer socialType;
    private String languageCode;

    public String getSocialId(){
        return this.socialId;
    }

    public void setSocialId(String socialId){
        this.socialId = socialId;
    }

    public  Integer getSocialType(){
        return this.socialType;
    }

    public void setSocialType(Integer socialType){
        this.socialType = socialType;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getDeviceBrand() {
        return deviceBrand;
    }

    public void setDeviceBrand(String deviceBrand) {
        this.deviceBrand = deviceBrand;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getNetworkOperator() {
        return networkOperator;
    }

    public void setNetworkOperator(String networkOperator) {
        if (!TextUtils.isEmpty(networkOperator))
            this.networkOperator = networkOperator;
    }

    public String getDeviceOs() {
        return deviceOs;
    }

    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    public String getDefaultBrowser() {
        return defaultBrowser;
    }

    public void setDefaultBrowser(String defaultBrowser) {
        this.defaultBrowser = defaultBrowser;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public Double getLatitude() {
        return appInstalledLat;
    }

    public void setLatitude(Double lat) {
        this.appInstalledLat = lat;
    }

    public Double getLongitude() {
        return appInstalledLong;
    }

    public void setLongitude(Double lng) {
        this.appInstalledLong = lng;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }
}
