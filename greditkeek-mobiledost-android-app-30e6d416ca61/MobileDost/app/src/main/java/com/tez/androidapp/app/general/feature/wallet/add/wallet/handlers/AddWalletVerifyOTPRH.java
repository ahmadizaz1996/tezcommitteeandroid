package com.tez.androidapp.app.general.feature.wallet.add.wallet.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks.AddWalletVerifyOTPCallback;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;

/**
 * Created  on 2/15/2017.
 */

public class AddWalletVerifyOTPRH extends DashboardCardsRH<DashboardCardsResponse> {

    private AddWalletVerifyOTPCallback addWalletVerifyOTPCallback;

    public AddWalletVerifyOTPRH(BaseCloudDataStore baseCloudDataStore, @Nullable AddWalletVerifyOTPCallback addWalletVerifyOTPCallback) {
        super(baseCloudDataStore);
        this.addWalletVerifyOTPCallback = addWalletVerifyOTPCallback;
    }

    @Override
    protected void onSuccess(Result<DashboardCardsResponse> value) {
        super.onSuccess(value);
        BaseResponse baseResponse = value.response().body();
        if (baseResponse != null) {
            if (isErrorFree(baseResponse))
                addWalletVerifyOTPCallback.onAddWalletVerifyOTPSuccess();
            else
                onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        addWalletVerifyOTPCallback.onAddWalletVerifyOTPFailure(errorCode, message);
    }
}
