package com.tez.androidapp.rewamp.signup.view;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.models.network.DeviceInfo;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.advance.limit.router.LimitAssignedActivityRouter;
import com.tez.androidapp.rewamp.advance.request.router.LoanReceiptActivityRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.profile.trust.router.ProfileTrustActivityRouter;

import net.tez.fragment.util.optional.Optional;

public interface IWelcomeBackActivityView extends IBaseView {

    void routeToDashboard();

    void askPermission(String pin);

    String userPushRoute();

    void makeCall();

    void startValidation(LocationAvailableCallback callback);


    void showUnexpectedErrorDialog();

    void showAppOutDatedDialog(@StringRes int message);

    DeviceInfo getDeviceInfo();

    void createSinchVerification(String mobileNumber,boolean callDeviceKey, String pin);

    void startChangeTemporaryPinActivity();

    void navigateToReactivateAccountActivity();

    default void onPushRoute(@NonNull String routeTo) {
        Optional.ifPresent(getHostActivity(), activity->{
            switch (routeTo) {

                case PushNotificationConstants.LIMIT_ASSIGNED_ACTIVITY:
                    LimitAssignedActivityRouter.createInstance().setDependenciesAndRouteWithClearAndNewTask(activity);
                    break;

                case PushNotificationConstants.LOAN_RECEIPT_ACTIVITY:
                    LoanReceiptActivityRouter.createInstance().setDependenciesAndRouteWithClearAndNewTask(activity);
                    break;

                case PushNotificationConstants.PROFILE_TRUST_ACTIVITY:
                    ProfileTrustActivityRouter.createInstance().setDependenciesAndRouteWithClearAndNewTask(activity);
                    break;

                default:
                    DashboardActivityRouter.createInstance().setDependenciesAndRoute(activity);
                    break;
            }
        });
    }
}
