package com.tez.androidapp.rewamp.bima.products.policy.RH;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InsuranceProductDetailCallback;
import com.tez.androidapp.rewamp.bima.products.policy.response.InsuranceProductDetailResponse;

public class InsuranceProductDetailsRH extends BaseRH<InsuranceProductDetailResponse> {

    private InsuranceProductDetailCallback insuranceProductDetailCallback;

    public InsuranceProductDetailsRH(BaseCloudDataStore baseCloudDataStore, InsuranceProductDetailCallback insuranceProductDetailCallback) {
        super(baseCloudDataStore);
        this.insuranceProductDetailCallback = insuranceProductDetailCallback;
    }

    @Override
    protected void onSuccess(Result<InsuranceProductDetailResponse> value) {
        InsuranceProductDetailResponse response = value.response().body();

        if (response != null) {
            if (isErrorFree(response))
                this.insuranceProductDetailCallback.onSuccessInsuranceProductDetail(response);
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());
        }
        else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.insuranceProductDetailCallback.onFailureInsuranceProductDetal(errorCode, message);
    }
}
