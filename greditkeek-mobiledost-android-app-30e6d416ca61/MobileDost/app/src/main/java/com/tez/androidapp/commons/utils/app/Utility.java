package com.tez.androidapp.commons.utils.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.ScrollView;

import androidx.annotation.ArrayRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.core.graphics.drawable.DrawableCompat;

import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.crashlytics.android.Crashlytics;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.scottyab.rootbeer.RootBeer;
import com.tez.androidapp.BuildConfig;
import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.push.PushNotificationHandlerActivity;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.GetUserPictureRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.DocumentFile;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.GetClaimDocumentRequest;
import com.tez.androidapp.commons.dialog.activity.DialogActivity;
import com.tez.androidapp.commons.force.update.ForceUpdateActivity;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.managers.NativeKeysManager;
import com.tez.androidapp.commons.models.app.FacebookData;
import com.tez.androidapp.commons.models.app.Permission;
import com.tez.androidapp.commons.models.network.AccessToken;
import com.tez.androidapp.commons.models.network.DeviceInfo;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserSignUpRequest;
import com.tez.androidapp.commons.utils.authorized.alert.image.popup.DefaultUIForAlertImage;
import com.tez.androidapp.commons.utils.authorized.alert.image.popup.RenderAlertImageOnUI;
import com.tez.androidapp.commons.utils.file.util.FileUtil;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezPinEditText;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.rewamp.committee.listener.IPermissionListener;
import com.tez.androidapp.rewamp.dashboard.ActionTypes;
import com.tez.androidapp.rewamp.dashboard.entity.DashboardCards;
import com.tez.androidapp.rewamp.signup.NewIntroActivity;
import com.tez.androidapp.rewamp.util.DialogUtil;

import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener;
import net.tez.fragment.util.optional.Optional;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility methods for Tez Financial Services
 * <p>
 * Created  on 12/5/2016.
 */

public final class Utility {

    private Utility() {
    }

    public enum PrincipalType {
        FACEBOOK,
        GOOGLE,
        MOBILE_NUMBER;

        public int getValue() {
            return ordinal() + 1;
        }
    }


    /**
     * Get the database encryption key for Realm
     *
     * @param context
     * @return byte[] encrypted key
     */
    public static byte[] getDBKey(Context context) {
        String secureId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        String packageName = context.getPackageName();
        String key = Base64.encodeToString(secureId.getBytes(), Base64.DEFAULT) + Base64.encodeToString(packageName.getBytes(), Base64.DEFAULT) + Base64.encodeToString
                (secureId.getBytes(), Base64.DEFAULT) + Base64.encodeToString(packageName.getBytes(), Base64.DEFAULT);
        byte[] data = key.getBytes();
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        byte[] bytes = base64.getBytes();
        bytes = Arrays.copyOfRange(bytes, 0, 64);
        return bytes;
    }

    public static List<String> getSignupPermissions(Context context) {
        List<String> requiredPermissions = PermissionCheckUtil.getSignupMissingPermissions(context);
        requiredPermissions.addAll(PermissionCheckUtil.getMobileVerificationMissingPermission(context));
        return requiredPermissions;
    }

    @NonNull
    public static List<String> getOptionalPermissions(Context context) {
        return PermissionCheckUtil.getOptionalMissingPermissions(context);
    }

    public static boolean isSignupPermissionGranted(Context context) {
        return getSignupPermissions(context).isEmpty();
    }

    public static boolean isAllPermissionGranted(Context context) {
        return isSignupPermissionGranted(context) && getOptionalPermissions(context).isEmpty();
    }


    /**
     * Checks if a string is null or empty
     *
     * @param data Input string
     * @return true if null or empty, false otherwise.
     */
    public static boolean isEmpty(@Nullable String data) {
        return data == null || data.length() == 0;
    }

    /**
     * Checks if a string is not null and is not empty
     *
     * @param data Input string
     * @return true if not null and not empty, false otherwise.
     */
    public static boolean isNotEmpty(@Nullable String data) {
        return !isEmpty(data);
    }

    /**
     * Formats the time in millis as server format, that is yyyy-MM-dd HH:mm:ss
     *
     * @param dateTime Input time in millis
     * @return Formatted date string
     */
    public static String getFormattedDate(long dateTime) {
        return getFormattedDate(dateTime, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * Formats the time in millis into the passed format
     *
     * @param dateTime Input time in millis
     * @param format   The format in which the date is to be formatted
     * @return Formatted date string
     */
    public static String getFormattedDate(long dateTime, @NonNull String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
        return simpleDateFormat.format(new Date(dateTime));
    }

    public static Date getFormattedDate(@NonNull String dateString, @NonNull String format) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getDefault());
        return simpleDateFormat.parse(dateString);
    }

    /**
     * Shows the keyboard, using the passed view as the focused view for which the keyboard is shown
     *
     * @param view The currently focused view, which would like to receive soft keyboard input
     */
    public static void showKeyboard(View view) {
        if (view.requestFocus()) {
            InputMethodManager inputMethodManager = (InputMethodManager) MobileDostApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }


    /**
     * Get the month offset-ed with 1, as the java data class months start with 0
     *
     * @param month
     * @return Input month + 1
     */
    public static int getCorrectOffsetMonth(int month) {
        if (month < 0) return 1;
        if (month > 11) return 12;
        return month + 1;
    }

    public static short compareDates(String date1, String date2, String format) {
        final short DATE1_AFTER_DATE2 = 1;
        final short DATE1_BEFORE_DATE2 = -1;
        final short DATE1_EQUALS_DATE2 = 0;
        final short FAILURE_CODE = -2;

        if (date1 == null || date2 == null)
            return FAILURE_CODE;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
        try {
            Date a = simpleDateFormat.parse(date1);
            Date b = simpleDateFormat.parse(date2);
            if (a.after(b))
                return DATE1_AFTER_DATE2;

            if (a.before(b))
                return DATE1_BEFORE_DATE2;

            return DATE1_EQUALS_DATE2;

        } catch (ParseException e) {
            Crashlytics.logException(e);
            return FAILURE_CODE;
        }
    }

    /**
     * Get the month as 2 digit number
     *
     * @param month
     * @return The month string prepended with a '0' if it is less than 10, the month itself otherwise.
     */
    public static String getTwoDigitInteger(int month) {
        String monthString;
        if (month < 10) monthString = "0" + month;
        else monthString = String.valueOf(month);
        return monthString;
    }

    public static double getPatternAmount(@NonNull String formattedAmount) {
        Matcher m = Pattern.compile(Constants.AMOUNT_PATTERN).matcher(formattedAmount);
        return Double.parseDouble(m.find() ? m.group(0) : "-1");
    }

    public static double getPatternAmount(@NonNull String formattedAmount, @NonNull String defaultValue) {
        Matcher m = Pattern.compile(Constants.AMOUNT_PATTERN).matcher(formattedAmount);
        return Double.parseDouble(m.find() ? m.group(0) : defaultValue);
    }

    /**
     * Moves the cursor to the end of the TezEditTextView
     *
     * @param tezEditTextView
     */
    public static void moveCursorToEnd(TezEditTextView tezEditTextView) {
        tezEditTextView.setSelection(tezEditTextView.getText().length());
    }


    private static String getAPIKey(Context context, String principalName, boolean isProduction) {
        if (principalName == null || context == null) return null;
        String packageName = context.getPackageName();
        if (packageName.split("\\.").length > 3) {
            int trimOffset = packageName.lastIndexOf('.');
            packageName = packageName.substring(0, trimOffset);
        }
        packageName = !isProduction ? packageName :
                new String(Base64.decode(
                        NativeKeysManager.getInstance().getTezApiKey1() +
                                NativeKeysManager.getInstance().getTezApiKey2() +
                                NativeKeysManager.getInstance().getTezApiKey3() +
                                NativeKeysManager.getInstance().getTezApiKey4()
                        , Base64.DEFAULT));
        String apiKey = principalName + packageName;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            byte[] data = messageDigest.digest(apiKey.getBytes());
            return Base64.encodeToString(data, Base64.DEFAULT).trim();
        } catch (NoSuchAlgorithmException e) {
            Crashlytics.logException(e);
        }
        return null;
    }

    /**
     * Get the encrypted API Key using the principal name
     *
     * @param mobileNumber name
     * @return Encrypted API Key string
     */
    public static String getAPIKey(String mobileNumber) {
        return getAPIKey(MobileDostApplication.getInstance().getApplicationContext(), mobileNumber == null ? "" : mobileNumber.replaceAll("-", ""), MobileDostApplication.getInstance().isProductionBuildChangeable());
    }

    /**
     * Get the encrypted API Key using the principal name, which is fetched from preferences
     *
     * @return Encrypted API Key string
     */
    public static String getAPIKey() {
        return getAPIKey(MDPreferenceManager.getPrincipalName());
    }

    public static String getMobileNumber() {
        User user = MDPreferenceManager.getUser();
        if (user != null)
            return user.getMobileNumber();
        return null;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth)
                inSampleSize *= 2;
        }
        return inSampleSize;
    }

    public static int getScreenWidthPixels() {
        return MobileDostApplication.getInstance().getApplicationContext().getResources().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeightPixels() {
        return MobileDostApplication.getInstance().getApplicationContext().getResources().getDisplayMetrics().heightPixels;
    }

    /**
     * Gets the resized image, using the given width and height
     */
    public static File getResizedImageFile(File file, int newWidth, int newHeight) {
        Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
        int width = bm.getWidth();
        int height = bm.getHeight();
        if (width <= 800 || height <= 800) return file;
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        String fileName = new SimpleDateFormat(Constants.NEW_FILE_DATE_FORMAT, Locale.US).format(new Date()) + ".jpg";
        return saveBitmap(resizedBitmap, fileName);
    }

    /**
     * Saves the bitmap at the given path
     *
     * @param bitmap   Image bitmap
     * @param fileName file Name
     */
    private static File saveBitmap(Bitmap bitmap, String fileName) {
        if (bitmap != null) {
            String path = FileUtil.getPicturesRoot(MobileDostApplication.getInstance()).getAbsolutePath();
            try (FileOutputStream outputStream = new FileOutputStream(path + fileName)) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                return new File(path + fileName);
            } catch (IOException e) {
                Crashlytics.logException(e);
            }
        }
        return null;
    }

    public static void compressFile(@NonNull String path) {
        compressFile(path, 70);
    }

    public static void compressFile(@NonNull String path, int quality) {
        compressFile(new File(path), quality);
    }

    @DrawableRes
    public static int getInsuranceProductIcon(int productId) {
        switch (productId) {

            case Constants.TEZ_LIFE_INSURANCE:
                return R.drawable.ic_life_insurance;

            case Constants.TEZ_HEALTH_INSURANCE_1:
            case Constants.TEZ_HEALTH_INSURANCE_2:
                return R.drawable.ic_health_insurance;

            case Constants.HOSPITAL_COVERAGE_PLAN_ID:
                return R.drawable.ic_hospitalization_plan;

            case Constants.DIGITAL_OPD_PLAN_ID:
                return R.drawable.ic_digital_opd;

            case Constants.CORONA_DEFENSE_PLAN_ID:
                return R.drawable.ic_covid_19;

            case Constants.TEZ_INSURANCE_PLAN_ID:
            default:
                return R.drawable.ic_free_isurance;
        }
    }

    public static void compressFile(@NonNull File file, int quality) {
            /*
            Getting the old ExiFInterface information as because after compression this information is
            lost and old information will be used to update new information after compression.
        */
        ExifInterface oldExifInterface;
        try {
            oldExifInterface = new ExifInterface(file.getAbsolutePath());
        } catch (IOException e) {
            return;
        }
        final String orientation = oldExifInterface.getAttribute(ExifInterface.TAG_ORIENTATION);
        Bitmap bitmap = loadBitmapWithRespectToMemory(file, Math.max(getScreenWidthPixels(), getScreenHeightPixels()));
        if (bitmap != null) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, bos);
            try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
                fileOutputStream.write(bos.toByteArray());
                if (orientation != null) {
                    ExifInterface newExif = new ExifInterface(file.getAbsolutePath());
                    newExif.setAttribute(ExifInterface.TAG_ORIENTATION, orientation);
                    newExif.saveAttributes();
                }
            } catch (IOException ignore) {
                //ignore
            }
        }
    }

    public static void loadProfilePicture(@NonNull final ImageView imageView) {
        Uri serverUri = Uri.parse(GetUserPictureRequest.METHOD_NAME);
        String signature = String.valueOf(MDPreferenceManager.getProfilePictureLastModifiedTime());
        DefaultUIForAlertImage uiForAlertImage = new DefaultUIForAlertImage(imageView, 80, 80);
        uiForAlertImage.setErrorImage(R.drawable.ic_img_user_placholder);
        uiForAlertImage.setPlaceholder(R.drawable.ic_img_user_placholder);
        uiForAlertImage.setTransformation(new CenterCrop(), new RoundedCorners(dpToPx(MobileDostApplication.getAppContext(), 2)));
        RenderAlertImageOnUI.renderDefaultImageUIAlert(uiForAlertImage, serverUri, signature);
    }

    public static void persistDashboardCards(@NonNull DashboardCards newCards) {
        DashboardCards persistedCards = MDPreferenceManager.getDashboardCards();

        if (persistedCards != null) {

            if (newCards.getAction() != null
                    && newCards.getAction().getActionType() != null
                    && persistedCards.getAction() == null
                    && ActionTypes.valueOf(newCards.getAction().getActionType()) == ActionTypes.PREVIOUS_ACTION)
                newCards.setAction(null);

            if (newCards.getProfileStatus() == null)
                newCards.setProfileStatus(persistedCards.getProfileStatus());

            if (newCards.getAdvance() == null)
                newCards.setAdvance(persistedCards.getAdvance());
        }

        MDPreferenceManager.setDashboardCards(newCards);
    }

    /**
     * get the string from given resource id
     *
     * @return string extracted from resource id
     */

    public static String getStringFromResource(int resourceId) {
        return MobileDostApplication.getInstance().getString(resourceId);
    }

    @NonNull
    public static FacebookData extractJsonDataFromFacebook(GraphResponse response) {
        FacebookData facebookData = null;
        try {
            facebookData = new Gson().fromJson(response.getRawResponse(), FacebookData.class);
            setFacebookDataFromResponse(response, facebookData);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        if (facebookData == null)
            facebookData = setFacebookDataFromProfile();
        return facebookData;
    }

    private static void setFacebookDataFromResponse(GraphResponse response, FacebookData facebookData) throws JSONException {
        JSONObject responseObject = response.getJSONObject();
        if (responseObject.has("name")) {
            facebookData.setName(responseObject.getString("name"));
        }
        if (responseObject.has("email")) {
            facebookData.setName(responseObject.getString("email"));
        }
        if (responseObject.has("birthday")) {
            facebookData.setName(responseObject.getString("birthday"));
        }
    }

    private static FacebookData setFacebookDataFromProfile() {
        FacebookData facebookData = new FacebookData();
        Profile profile = Profile.getCurrentProfile();
        if (profile != null) {
            facebookData.setId(profile.getId());
            facebookData.setName(profile.getName());
        } else {
            facebookData.setId(com.facebook.AccessToken.getCurrentAccessToken().getUserId());
        }

        return facebookData;
    }


    /**
     * Make the activity full screen, removing the actionbar and the status bar
     *
     * @param appCompatActivity
     */
    public static void makeActivityFullScreen(AppCompatActivity appCompatActivity) {
        View decorView = appCompatActivity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        if (appCompatActivity.getSupportActionBar() != null)
            appCompatActivity.getSupportActionBar().hide();
    }

    /**
     * Gets the authorization header value from preferences
     *
     * @return Authorization header value string
     */
    public static String getAuthorizationHeaderValue() {
        AccessToken accessToken = MDPreferenceManager.getAccessToken();
        if (accessToken != null)
            return accessToken.getTokenType() + " " + accessToken.getAccessToken();
        return null;
    }

    public static boolean checkIsEmulator() {
        return Constants.IS_EXTERNAL_BUILD && isEmulator();
    }

    private static boolean isEmulator() {
        String generic = "generic";
        return Build.FINGERPRINT.startsWith(generic)
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith(generic) && Build.DEVICE.startsWith(generic))
                || "google_sdk".equals(Build.PRODUCT);
    }

    /**
     * Creates a {@link UserLoginRequest} using the provided principalName and password
     *
     * @param context
     * @param principalName
     * @param password
     * @return UserLoginRequest object with all the fields populated
     */
    public static UserLoginRequest createLoginRequest(Context context, String principalName, String password) {
        UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setPrincipalName(principalName.replaceAll("-", ""));
        userLoginRequest.setPassword(password);
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        userLoginRequest.setDeviceBrand(Build.BRAND);
        userLoginRequest.setDeviceModel(Build.MODEL);
        userLoginRequest.setDeviceOs(Build.VERSION.RELEASE);
        userLoginRequest.setLanguageCode(LocaleHelper.getSelectedLanguageForBackend(context));
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"));
        ResolveInfo resolveInfo = context.getPackageManager().resolveActivity(browserIntent, PackageManager.MATCH_DEFAULT_ONLY);
        if (resolveInfo != null)
            userLoginRequest.setDefaultBrowser(resolveInfo.activityInfo.packageName);
        String imei = Utility.getImei(context);
        if (checkIsEmulator() && BuildConfig.IS_DEBUG && imei == null) imei = "000000000000000";
        userLoginRequest.setImei(imei);
        userLoginRequest.setNetworkOperator(telephonyManager.getNetworkOperatorName());
        userLoginRequest.setAppVersion(BuildConfig.VERSION_NAME);
        userLoginRequest.setDeviceKey(MDPreferenceManager.getFirebaseKey());
        //FirebaseInstanceId.newInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> userLoginRequest.setDeviceKey(instanceIdResult.getToken()));
        if (isLocationValidInSharedPreferences()) {
            userLoginRequest.setLatitude(Double.valueOf(MDPreferenceManager.getLastLocation()[0]));
            userLoginRequest.setLongitude(Double.valueOf(MDPreferenceManager.getLastLocation()[1]));
        }
        return userLoginRequest;
    }

    /**
     * Format the amount using the given formatter
     *
     * @param numberFormat
     * @param amount
     * @return Formatted amount using the given formatter
     */
    public static String getFormattedAmount(NumberFormat numberFormat, double amount) {
        return "PKR " + numberFormat.format(amount).replace("PKR", "");
    }


    /**
     * Format the amount using the default formatter
     *
     * @param amount
     * @return Formatted amount using the default formatter
     */
    public static String getFormattedAmount(double amount) {
        return getFormattedAmount(getDefaultNumberFormat(), amount);
    }

    public static boolean isProdReleaseBuild() {
        return Constants.IS_EXTERNAL_BUILD && MobileDostApplication.getInstance().isProductionBuildChangeable();
    }

    public static String getFormattedAmountWithoutCurrencyLabel(double amount) {
        return getDefaultNumberFormat().format(amount).replace("PKR", "");
    }

    public static String getFormattedAmountWithoutCurrencyLabelAndDecimals(double amount) {
        return getDefaultNumberFormatWithoutDecimals().format(amount).replace("PKR", "");
    }


    public static String getFormattedAmountWithoutDecimals(double amount) {
        return getFormattedAmount(getDefaultNumberFormatWithoutDecimals(), amount);
    }


    /**
     * Get the default number formatter
     *
     * @return Default Number Formatter, that is with PKR
     */
    public static NumberFormat getDefaultNumberFormat() {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.UK);
        numberFormat.setCurrency(Currency.getInstance("PKR"));
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setMinimumFractionDigits(0);
        return numberFormat;
    }

    public static NumberFormat getDefaultNumberFormatWithoutDecimals() {
        NumberFormat numberFormat = getDefaultNumberFormat();
        numberFormat.setMaximumFractionDigits(0);
        return numberFormat;
    }

    public static boolean isTouchOutSideViewBoundary(@NonNull View view, @NonNull MotionEvent event) {
        Rect rect = new Rect();
        int[] location = new int[2];
        view.getDrawingRect(rect);
        view.getLocationOnScreen(location);
        rect.offset(location[0], location[1]);
        return !rect.contains((int) event.getRawX(), (int) event.getRawY());
    }

    public static void hideKeyboard(Context context, View view) {
        try {
            if (context != null && view != null) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception ignored) {

        }
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null)
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        return false;
    }


    public static void openContacts(@NonNull BaseActivity baseActivity, int requestCode) {
        Dexter.withActivity(baseActivity).withPermission(Manifest.permission.READ_CONTACTS).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                baseActivity.startActivityForResult(intent, requestCode);
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                DialogUtil.showInformativeMessage(baseActivity,
                        baseActivity.getString(R.string.permissions_required),
                        (dialog, which) -> {
                            if (response.isPermanentlyDenied())
                                openSettings(baseActivity);
                        });
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }

    public static void fetchContacts(@NonNull BaseActivity baseActivity, IPermissionListener listener) {
        Dexter.withActivity(baseActivity).withPermission(Manifest.permission.READ_CONTACTS).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                listener.onPermissionGranted();
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                DialogUtil.showInformativeMessage(baseActivity,
                        baseActivity.getString(R.string.permissions_required),
                        (dialog, which) -> {
                            if (response.isPermanentlyDenied())
                                openSettings(baseActivity);
                        });
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }


    public static void openSettings(@NonNull BaseActivity baseActivity) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts
                ("package", baseActivity.getPackageName(), null));
        baseActivity.startActivity(intent);
    }

    @NonNull
    public static String getFormattedDate(@Nullable String inputDate,
                                          @NonNull String inputFormat,
                                          @NonNull String outputFormat) {
        try {
            SimpleDateFormat outputDateFormat = new SimpleDateFormat(outputFormat, Locale.US);
            SimpleDateFormat inputDateFormat = new SimpleDateFormat(inputFormat, Locale.US);
            return inputDate == null ? "" : outputDateFormat.format(inputDateFormat.parse(inputDate));
        } catch (ParseException e) {
            Crashlytics.logException(e);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return "";
    }

    public static String getFormattedDateWithUpdatedTimeZone(@Nullable String inputDate, String inputFormat, String outputFormat) {
        if (inputDate == null)
            return null;

        try {
            SimpleDateFormat outputDateFormat = new SimpleDateFormat(outputFormat, Locale.US);
            SimpleDateFormat inputDateFormat = new SimpleDateFormat(inputFormat, Locale.US);
            outputDateFormat.setTimeZone(TimeZone.getDefault());
            Date date = inputDateFormat.parse(inputDate);
            return outputDateFormat.format(date);
        } catch (ParseException e) {
            Crashlytics.logException(e);
        }
        return "";
    }

    public static boolean doesUserExist() {
        return MDPreferenceManager.isUserSignedUp() && MDPreferenceManager.getUser() != null;
    }

    public static void showSessionExpireDialogFromNonActivityClass() {
        Intent intent = new Intent();
        intent.putExtra(DialogActivity.MESSAGE, getStringFromResource(R.string.session_expired_message));
        intent.putExtra(DialogActivity.IS_SESSION_TIMEOUT, true);
        showDialogFromNonActivityClass(intent);
    }

    public static void showAppOutdatedDialogFromNonActivityClass() {
        Context appContext = MobileDostApplication.getAppContext();
        Intent intent = new Intent(appContext, ForceUpdateActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        appContext.startActivity(intent);
    }

    private static void showDialogFromNonActivityClass(Intent intent) {
        MobileDostApplication mobileDostApplication = MobileDostApplication.getInstance();
        if (mobileDostApplication != null) {
            Context appContext = mobileDostApplication.getApplicationContext();
            intent.setClass(appContext, DialogActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            appContext.startActivity(intent);
        }
    }

    public static void showChoiceDialog(Context context, @ArrayRes int itemsId, DoubleTapSafeDialogClickListener onClickListener) {
        try {
            if (context != null) {
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(context);
                builder.setItems(itemsId, onClickListener);
                if (context instanceof Activity) {
                    Activity activity = (Activity) context;
                    if (!activity.isFinishing()) {
                        builder.show();
                    }
                }
            }
        } catch (Exception e) {
            Log.e(e);
        }
    }


    public static String readFromAssets(Context context, String filename) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(context.getAssets().open(filename)))) {
            StringBuilder sb = new StringBuilder();
            String mLine = reader.readLine();
            while (mLine != null) {
                sb.append(mLine);
                mLine = reader.readLine();
            }
            return sb.toString();
        }
    }

    public static boolean isUnauthorized(BaseResponse baseResponse) {
        return baseResponse.getStatusCode() == Constants.UNAUTHORIZED_ERROR_CODE;
    }

    public static boolean isUnauthorized(int statusCode) {
        return statusCode == Constants.UNAUTHORIZED_ERROR_CODE;
    }

    public static void initNotificationChannels(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            @SuppressLint("WrongConstant") NotificationChannel channel = new NotificationChannel("default", "Channel name", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Tez Notification Channel");
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }

    public static void openPlayStoreForApp(Activity activity, String appPackageName) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException e) {
            try {
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google" + ".com/store/apps/details?id=" + appPackageName)));
            } catch (Exception e1) {
                //Left
            }
        }
    }

    public static void logoutUser() {
        Optional.ifPresent((NotificationManager) MobileDostApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE),
                NotificationManager::cancelAll);
        deletePicture();
        MDPreferenceManager.clearAllPreferences();
        LoginManager.getInstance().logOut();
    }

    public static void directUserToIntroScreenAtGlobalLevel() {
        Intent intent = new Intent(MobileDostApplication.getInstance(), NewIntroActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        logoutUser();
        MobileDostApplication.getInstance().startActivity(intent);
    }

    public static void deletePicture() {
        Context context = MobileDostApplication.getInstance().getApplicationContext();
        if (context != null) {
            final File file = new File(FileUtil.getPicturesRoot(context).getAbsolutePath() + NativeKeysManager.getInstance().getProfilePictureFilePath());
            FileUtil.deleteFile(file);
        }
    }

    public static Drawable changeTintOfDrawable(Drawable drawable, Integer color) {
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, color);
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
        return drawable;
    }

    public static void getDataFromFacebook(com.facebook.AccessToken accessToken, GraphRequest.GraphJSONObjectCallback callback) {
        if (Profile.getCurrentProfile() == null) {
            Profile.fetchProfileForCurrentAccessToken();
        }
        GraphRequest request = GraphRequest.newMeRequest(accessToken, callback);
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id," +
                "first_name," +
                "last_name," +
                "middle_name," +
                "name," +
                "email," +
                "age_range," +
                "cover," +
                "gender," +
                "link," +
                "timezone," +
                " updated_time," +
                "birthday," +
                "picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @DrawableRes
    public static int getBeneficiaryRelationImage(Integer relationshipId) {
        relationshipId = relationshipId != null ? relationshipId : 50;
        int image;
        switch (relationshipId) {

            case Constants.BENEFICIARY_FATHER:
                image = R.drawable.ic_father;
                break;

            case Constants.BENEFICIARY_MOTHER:
                image = R.drawable.ic_mother;
                break;

            case Constants.BENEFICIARY_HUSBAND:
                image = R.drawable.ic_husband;
                break;

            case Constants.BENEFICIARY_WIFE:
                image = R.drawable.ic_wife;
                break;

            case Constants.BENEFICIARY_SON:
                image = R.drawable.ic_son;
                break;

            case Constants.BENEFICIARY_DAUGHTER:
                image = R.drawable.ic_daughter;
                break;

            case Constants.BENEFICIARY_BROTHER:
                image = R.drawable.ic_brother;
                break;

            case Constants.BENEFICIARY_SISTER:
                image = R.drawable.ic_sister;
                break;

            default:
            case Constants.BENEFICIARY_OTHER:
                image = R.drawable.ic_other;
                break;
        }

        return image;
    }

    @StringRes
    public static int getBeneficiaryRelationName(Integer relationshipId) {
        relationshipId = relationshipId != null ? relationshipId : 50;
        int image;
        switch (relationshipId) {

            case Constants.BENEFICIARY_FATHER:
                image = R.string.father;
                break;

            case Constants.BENEFICIARY_MOTHER:
                image = R.string.mother;
                break;

            case Constants.BENEFICIARY_HUSBAND:
                image = R.string.husband;
                break;

            case Constants.BENEFICIARY_WIFE:
                image = R.string.wife;
                break;

            case Constants.BENEFICIARY_SON:
                image = R.string.son;
                break;

            case Constants.BENEFICIARY_DAUGHTER:
                image = R.string.daughter;
                break;

            case Constants.BENEFICIARY_BROTHER:
                image = R.string.brother;
                break;

            case Constants.BENEFICIARY_SISTER:
                image = R.string.sister;
                break;

            default:
            case Constants.BENEFICIARY_OTHER:
                image = R.string.other;
                break;
        }
        return image;
    }


    public static void logScreenChangeEvent(Activity activity, String screenName) {
        Utility.getFirebaseAnalyticsInstance().setCurrentScreen(activity, screenName, screenName);
    }


    public static String getNumberFromContacts(Intent data, Activity activity) {
        Uri contactData = data.getData();
        String phoneNumber = "";
        if (activity != null && activity.getContentResolver() != null && contactData != null) {
            Cursor cursor = activity.getContentResolver().query(contactData, null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    String contactId = cursor.getString(cursor.getColumnIndex(
                            ContactsContract.Contacts._ID));
                    int hasPhone = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
                    if (hasPhone > 0) {
                        // You know have the number so now query it like this
                        Cursor phones = activity.getContentResolver().query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                                null, null);
                        if (phones != null && phones.getCount() > 0) {
                            while (phones.moveToNext()) {
                                phoneNumber = phones.getString(
                                        phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                            }
                            phones.close();
                        }
                    }
                }
                cursor.close();
            }
        }

        return phoneNumber;
    }

    public static String getFormattedMobileNumberWithDashes(String number) {
        if (number == null) return number;
        number = number.replaceAll("[-\\[\\]^/,'*:.!> <~@#$%=?|\"\\\\()]+", "");
        number = number.replace("+92", "0");
        if (number.length() > 5)
            number = number.substring(0, 4) + "-" + number.substring(4);
        return number;
    }

    @NonNull
    public static String getFormattedMobileNumberWithOutDashes(@NonNull String number) {
        number = number.replaceAll("[-\\[\\]^/,'*:.!> <~@#$%=?|\"\\\\()]+", "");
        if (!number.contains("+92")) {
            number = "+92" + number.substring(1);
        }
        return number;
    }


    @NonNull
    public static String getFormattedMobileNumber(@NonNull String number) {
        number = number.replaceAll("[-\\[\\]^/,'*:.!> <~@#$%=?|\"\\\\()]+", "");
        if (number.startsWith("92") && number.length() > 2)
            number = "0" + number.substring(2);

        else if (number.startsWith("+92") && number.length() > 3)
            number = "0" + number.substring(3);

        else if (number.length() > 1)
            number = "0" + number.substring(1);

        return number;
    }

    private static int isDateValid(String inputDateString, String dateFormat, Date givenDateToCompare) {
        try {

            if (inputDateString == null || dateFormat == null) return 2;
            Date dateEntered = Utility.getFormattedDate(inputDateString, dateFormat);
            Date dateToCompare;
            if (givenDateToCompare == null) {
                String dateStringToCompare = Utility.getFormattedDate(System.currentTimeMillis(), dateFormat);
                dateToCompare = Utility.getFormattedDate(dateStringToCompare, dateFormat);
            } else dateToCompare = givenDateToCompare;

            return dateEntered.compareTo(dateToCompare);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return 2;
    }

    /**
     * Checks if the input Date  is Expired from GivenDate/Current Date
     * if givenDateToCompare is null then function will check with Today's Date
     *
     * @return true if the Input Date is expired
     */
    public static boolean isDateExpired(String inputDateString, String dateFormat, Date givenDateToCompare) {
        try {
            if (inputDateString == null || dateFormat == null) return false;
            Date dateEntered = Utility.getFormattedDate(inputDateString, dateFormat);
            Date dateToCompare;
            if (givenDateToCompare == null) {
                String dateStringToCompare = Utility.getFormattedDate(System.currentTimeMillis(), dateFormat);
                dateToCompare = Utility.getFormattedDate(dateStringToCompare, dateFormat);
            } else dateToCompare = givenDateToCompare;
            return dateEntered.compareTo(dateToCompare) < 0;
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return true;
    }

    public static long getNYearAheadDate(int year) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, year);
        return cal.getTimeInMillis();
    }

    /**
     * @param dateString Input Date
     * @param dateFormat Input Date Format
     * @return true if input date invalid or date difference from Current Date is less than 18 Years
     */
    public static boolean isDateOfBirthInvalidOrLessThanEightYears(String dateString, String dateFormat) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -Constants.YEAR_TO_GO_BACK); // to get previous year add -1
        Date dateToCompare = cal.getTime();
        return !Utility.isDateExpired(dateString, dateFormat, dateToCompare);
    }

    public static boolean isDateInValid(String dateString, String dateFormat, int yearsTimePeriod, boolean lessThanToday) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, yearsTimePeriod); // to get previous year add -1
        Date dateToCompare = cal.getTime();
        int result = Utility.isDateValid(dateString, dateFormat, dateToCompare);
        return lessThanToday ? result > 0 : result < 0;
    }

    public static int getColorFromResource(int colorId) {
        return MobileDostApplication.getInstance().getResources().getColor(colorId);
    }

    public static int getWalletIconResourceId(int serviceProviderId) {
        switch (serviceProviderId) {
            case Constants.SIMSIM_ID:
                return R.drawable.logo_simsim;
            case Constants.EASYPAISA_ID:
                return R.drawable.logo_easypaisa;
            case Constants.UBL_OMNI_ID:
                return R.drawable.logo_omni;
            case Constants.JAZZ_CASH_ID:
                return R.drawable.logo_jazzcash;
            default:
                return 0;
        }
    }

    public static int getWalletIconResourceIdLinked(int serviceProviderId) {
        switch (serviceProviderId) {
            case Constants.SIMSIM_ID:
                return R.drawable.img_simsim_color;
            case Constants.EASYPAISA_ID:
                return R.drawable.img_easypaisa_color;
            case Constants.UBL_OMNI_ID:
                return R.drawable.img_omni_color;
            case Constants.JAZZ_CASH_ID:
                return R.drawable.img_jazzcash_color;
            default:
                return 0;
        }
    }

    public static String getWalletName(int serviceProviderId) {
        switch (serviceProviderId) {
            case Constants.SIMSIM_ID:
                return getStringFromResource(R.string.simsim);
            case Constants.EASYPAISA_ID:
                return getStringFromResource(R.string.easypaisa);
            case Constants.UBL_OMNI_ID:
                return getStringFromResource(R.string.ubl_omni);
            case Constants.JAZZ_CASH_ID:
                return getStringFromResource(R.string.jazz_cash);
            default:
                return "";

        }
    }

    public static String getWalletBankName(int serviceProviderId) {
        switch (serviceProviderId) {
            case Constants.SIMSIM_ID:
                return getStringFromResource(R.string.finca_microfinance_bank_limited);
            case Constants.EASYPAISA_ID:
                return getStringFromResource(R.string.telenor_microfinance_bank_limited);
            case Constants.UBL_OMNI_ID:
                return getStringFromResource(R.string.united_bank_limited);
            case Constants.JAZZ_CASH_ID:
                return getStringFromResource(R.string.mobilink_microfinance_bank_limited);
            default:
                return "";

        }
    }

    public static String getWalletProviderNumber(int serviceProviderId) {
        switch (serviceProviderId) {
            case Constants.SIMSIM_ID:
                return Constants.SIMSIM_CALL_CENTER_NUMBER;
            case Constants.EASYPAISA_ID:
                return Constants.EASYPASIA_CALL_CENTER_NUMBER;
            case Constants.UBL_OMNI_ID:
                return Constants.UBL_CALL_CENTER_NUMBER;
            case Constants.JAZZ_CASH_ID:
                return "";
            default:
                return "";

        }
    }

    public static String getWalletAppPackageName(int serviceProviderId) {
        switch (serviceProviderId) {
            case Constants.SIMSIM_ID:
                return Constants.SIM_SIM_PACKAGE_NAME;
            case Constants.EASYPAISA_ID:
                return Constants.EASYPAISA_PACKAGE_NAME;
            case Constants.UBL_OMNI_ID:
                return Constants.UBL_OMNI_PACKAGE_NAME;
            case Constants.JAZZ_CASH_ID:
                return Constants.JAZZ_CASH_PACKAGE_NAME;
            default:
                return Constants.EASYPAISA_PACKAGE_NAME;

        }
    }

    public static int dpToPx(Context context, float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    @DrawableRes
    public static int getInsuranceProviderLogo(@NonNull String insuranceProviderCode) {

        switch (insuranceProviderCode) {

            case Constants.INSURANCE_PROVIDER_EFU:
                return R.drawable.ic_efu_logo;

            case Constants.INSURANCE_PROVIDER_JUBILEE:
                return R.drawable.ic_jubilee_logo;

            case Constants.INSURANCE_PROVIDER_TPL:
                return R.drawable.ic_tpl_logo;

            case Constants.INSURANCE_PROVIDER_WEBDOC:
                return R.drawable.ic_webdoc_logo;

            default:
                return 0;
        }
    }

    @Nullable
    public static List<Address> getAddressesFromLatLng(double latitude, double longitude) {
        try {
            Context context = MobileDostApplication.getInstance().getApplicationContext();
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            return geocoder.getFromLocation(latitude, longitude, 1);

        } catch (IOException | NullPointerException e) {
            Crashlytics.logException(e);
            return Collections.emptyList();
        }
    }

    public static void logCrashlytics(@NonNull String message) {
        try {
            Crashlytics.log(message);
        } catch (Exception ignore) {
            //ignore
        }
    }

    public static void setUserForCrashlytics() {
        User user = MDPreferenceManager.getUser();
        if (user != null) {
            Crashlytics.setUserIdentifier(user.getId());
            Crashlytics.setString("user_id", user.getId());
        }
    }

    public static void setUserForFirebase() {
        User user = MDPreferenceManager.getUser();
        if (user != null)
            getFirebaseAnalyticsInstance().setUserId(user.getId());
    }

    public static void setUserForFacebook() {
        User user = MDPreferenceManager.getUser();
        if (user != null)
            AppEventsLogger.setUserID(user.getId());
    }

    public static FirebaseAnalytics getFirebaseAnalyticsInstance() {
        return FirebaseAnalytics.getInstance(MobileDostApplication.getInstance().getApplicationContext());
    }

    public static AppEventsLogger getAppEventsLoggerInstance() {
        return AppEventsHelperUtil.getInstance();
    }

    public static boolean isValidName(String textFromEditText) {
        if (textFromEditText == null || textFromEditText.trim().isEmpty()) return false;
        Pattern p = Pattern.compile("[-/@#$':;'\"><?///|!*%*%^&_+=()]+");
        Matcher m = p.matcher(textFromEditText);
        return !m.find();
    }

    @NonNull
    public static String formatMobileNumberInitials(@NonNull String mobileNumber) {
        if (mobileNumber.charAt(0) == '0') {
            return "+92" + mobileNumber.substring(1);
        }
        return mobileNumber;
    }

    public static boolean isDeviceRooted(Context context) {
        if (!Constants.IS_EXTERNAL_BUILD) return false;

        boolean isRooted = true;
        RootBeer rootBeer = new RootBeer(context);
        try {
            isRooted = rootBeer.isRootedWithoutBusyBoxCheck();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return isRooted;
    }

    public static boolean isLocationValidInSharedPreferences() {
        String[] location = MDPreferenceManager.getLastLocation();
        try {
            double latitude = Double.parseDouble(location[0]);
            double longitude = Double.parseDouble(location[1]);
            return !(location[0].isEmpty() || location[1].isEmpty() || latitude == 0 || longitude == 0);
        } catch (Exception ignore) {
            return false;
        }
    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    @Nullable
    private static String getImeiFromSystem(final Context context) {
        if (!Constants.IS_EXTERNAL_BUILD && isEmulator()) return generateRandomImeiNumber();

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Crashlytics.log(1, "IMEI", "Returning IMEI as null because of missing permission!");
            return null;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            return null;
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            return telephonyManager.getImei() != null ? telephonyManager.getImei() : telephonyManager.getMeid();
        else
            return telephonyManager.getDeviceId();

    }

    /*
     *  @Param imei - stores imei in file and if file already exists, it overwrites the file.
     */
    private static void storeImeiInFile(@NonNull final String imei) {
        if (!Environment.getExternalStorageDirectory().exists())
            return;

        File file = new File(Environment.getExternalStorageDirectory(), NativeKeysManager.getInstance().getImeiFilePath());
        FileUtil.createDirectory(file.getParentFile());
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(AESEncryptionUtil.getInstance().encrypt(imei).getBytes());
        } catch (Exception e) {
            Crashlytics.logException(e);
            Crashlytics.log("Cannot set IMEI to File");
        }
    }

    @Nullable
    public static String getImei(final Context context) {

        if (!Environment.getExternalStorageDirectory().exists())
            return getImeiFromSystem(context);

        String imeiFromFile = getImeiFromFile();

        if (imeiFromFile != null)
            return imeiFromFile;

        String imeiFromSystem = getImeiFromSystem(context);

        if (imeiFromSystem != null) {
            storeImeiInFile(imeiFromSystem);
            return imeiFromSystem;
        }

        return null;
    }

    /*
        getImei tries to get IMEI from file first and on failure it reads from system and return.
        It also stores IMEI (read from system) in file only if IMEI is not null. In this case, file
        will always be in a consistent state.
     */

    @Nullable
    private static String getImeiFromFile() {
        File file = new File(Environment.getExternalStorageDirectory().toString(), NativeKeysManager.getInstance().getImeiFilePath());
        if (file.exists()) {
            StringBuilder text = new StringBuilder();

            try (BufferedReader br = new BufferedReader(new FileReader(file))) {

                String line;
                while ((line = br.readLine()) != null)
                    text.append(line);

                if (!text.toString().isEmpty())
                    return AESEncryptionUtil.getInstance().decrypt(text.toString());
                return null;

            } catch (IOException e) {
                return null;
            }
        }
        return null;
    }

    public static UserSignUpRequest createSignupRequest(@NonNull Context context,
                                                        @Nullable Location location,
                                                        @NonNull String mobileNumber,
                                                        @Nullable String referralCode,
                                                        @Nullable String socialId,
                                                        int socialType) {
        UserSignUpRequest userSignUpRequest = new UserSignUpRequest();
        userSignUpRequest.setMobileNumber(mobileNumber);
        userSignUpRequest.setReferralCode(referralCode);
        userSignUpRequest.setSocialId(socialId);
        userSignUpRequest.setSocialType(socialType);
        userSignUpRequest.setLanguageCode(LocaleHelper.getSelectedLanguageForBackend(context));
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> userSignUpRequest.setDeviceKey(instanceIdResult.getToken()));
        userSignUpRequest.setLatitude(location == null ? 0 : location.getLatitude());
        userSignUpRequest.setLongitude(location == null ? 0 : location.getLongitude());
        userSignUpRequest.setDeviceInfo(Utility.getUserDeviceInfo(context));
        return userSignUpRequest;
    }


    public static DeviceInfo getUserDeviceInfo(final Context context) {
        TelephonyManager telephonyManager = MobileDostApplication.getTelephonyManagerService();
        ResolveInfo resolveInfo = MobileDostApplication.getActivityResolverForInternet();
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setDeviceBrand(Build.BRAND);
        deviceInfo.setDeviceModel(Build.MODEL);
        deviceInfo.setDeviceOs(Build.VERSION.RELEASE);
        deviceInfo.setImei(getImei(context));
        if (resolveInfo != null)
            deviceInfo.setDefaultBrowser(resolveInfo.activityInfo.packageName);
        if (telephonyManager != null)
            deviceInfo.setNetworkOperator(telephonyManager.getNetworkOperatorName());
        return deviceInfo;
    }

    public static DeviceInfo getUserDeviceInfo() {
        TelephonyManager telephonyManager = MobileDostApplication.getTelephonyManagerService();
        ResolveInfo resolveInfo = MobileDostApplication.getActivityResolverForInternet();
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setDeviceBrand(Build.BRAND);
        deviceInfo.setDeviceModel(Build.MODEL);
        deviceInfo.setDeviceOs(Build.VERSION.RELEASE);
        deviceInfo.setImei(getImei(MobileDostApplication.getAppContext()));
        if (resolveInfo != null)
            deviceInfo.setDefaultBrowser(resolveInfo.activityInfo.packageName);
        if (telephonyManager != null)
            deviceInfo.setNetworkOperator(telephonyManager.getNetworkOperatorName());
        return deviceInfo;
    }

    public static void sendNotification(Intent intent, String message) {
        Utility.initNotificationChannels(MobileDostApplication.getInstance());
        intent.setClass(MobileDostApplication.getInstance().getApplicationContext(), PushNotificationHandlerActivity.class);
        Bitmap largeIcon = BitmapFactory.decodeResource(MobileDostApplication.getInstance().getResources(), R.drawable.icon_notification_logo);
        String title = intent.getStringExtra(PushNotificationConstants.KEY_TITLE);
        title = Utility.isEmpty(title) ? "Tez Financial Services" : title;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(MobileDostApplication.getInstance(), "default")
                .setSmallIcon(R.drawable.ic_notif_icon).setLargeIcon(largeIcon).setContentTitle(title).setContentText(message).setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message));
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(MobileDostApplication.getInstance());
        stackBuilder.addParentStack(PushNotificationHandlerActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) MobileDostApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        if (Build.VERSION.SDK_INT >= 21) mBuilder.setVibrate(new long[]{10, 5, 10});
        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify((new Random()).nextInt(), notification);
    }

    public static String formatDate(String date, String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            Date formattedDate = simpleDateFormat.parse(date);
            SimpleDateFormat simpleDateFormatOutput = new SimpleDateFormat(Constants.BACKEND_DATE_FORMAT);
            return simpleDateFormatOutput.format(formattedDate);
        } catch (ParseException e) {
            Crashlytics.log("Parse Error: Date format does not match with given format");
            return null;
        } catch (Exception ignore) {
            return null;
        }
    }

    private static String generateRandomImeiNumber() {
        Random rand = new Random();
        StringBuilder accumulator = new StringBuilder(Integer.toString(1 + rand.nextInt(9)));
        for (int i = 0; i < 15; i++) {
            accumulator.append(rand.nextInt(10));
        }
        return accumulator.toString();
    }

    public static void scrollToView(@NonNull final ScrollView scrollViewParent,
                                    @Nullable final View view) {
        if (view == null) return;
        Point childOffset = new Point();
        getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset);
        scrollViewParent.smoothScrollTo(0, childOffset.y);
    }

    private static void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) return;
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }

    public static File getResizedImageFileUsingSampleSizeTemp(File file, boolean shouldFlip) {
        try {
            int rotate = 0;
            if (shouldFlip)
                rotate = getFileRotation(file);
            Matrix matrix = new Matrix();
            matrix.postRotate(rotate);
            Bitmap originalBitmap = loadBitmapWithRespectToMemory(file, 600D);

            if (originalBitmap == null)
                return null;

            Bitmap croppedBitmap = Bitmap.createBitmap(originalBitmap, 0, 0, originalBitmap.getWidth(), originalBitmap.getHeight(), matrix, false);
            if (originalBitmap != croppedBitmap)
                originalBitmap.recycle();
            String fileName = new SimpleDateFormat(Constants.NEW_FILE_DATE_FORMAT, Locale.US).format(new Date()) + ".jpg";
            return saveBitmap(croppedBitmap, fileName);
        } catch (Exception e) {
            Crashlytics.logException(e);
            return null;
        }
    }

    private static int getFileRotation(File file) {
        int rotate;
        try {

            ExifInterface exif = new ExifInterface(file.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
                default:
                    rotate = 0;
                    break;
            }
            return rotate;
        } catch (IOException e) {
            return 0;
        }
    }

    /**
     * Load Bitmap to memory according memory available
     *
     * @param file
     * @param greatestSideSize
     * @return
     */
    public static Bitmap loadBitmapWithRespectToMemory(File file, double greatestSideSize) {

        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file.getAbsolutePath(), options);
            double[] scaledWidthAndHeight = getScaledWidthAndHeight(greatestSideSize, options.outWidth, options.outHeight);
            if (greatestSideSize > 0) {
                options.inSampleSize = calculateInSampleSize(options, (int) scaledWidthAndHeight[0], (int) scaledWidthAndHeight[1]);
            } else greatestSideSize = 1024D;
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        } catch (OutOfMemoryError e) {
            android.util.Log.e("OutOfMemory", "greatest size: " + greatestSideSize);
            if (greatestSideSize <= 500) return null;
            return loadBitmapWithRespectToMemory(file, greatestSideSize - 100);
        }
    }

    private static double[] getScaledWidthAndHeight(double greatestSideSize, int width, int height) {
        int newWidth;
        int newHeight;
        if (width <= greatestSideSize || height <= greatestSideSize) {
            newHeight = height;
            newWidth = width;
        } else {
            boolean isWidthGreater = width > height;
            double aspectRatio = isWidthGreater ? (width / height) : (height / width);
            if (isWidthGreater) {
                newWidth = (int) greatestSideSize;
                int temp = (int) ((width - greatestSideSize) / aspectRatio);
                newHeight = height - temp;
            } else {
                newHeight = (int) greatestSideSize;
                int temp = (int) ((height - greatestSideSize) / aspectRatio);
                newWidth = width - temp;
            }
        }
        return new double[]{newWidth, newHeight};
    }

    public static <T> List<T> deleteDuplicatesFromList(List<T> blockData) {
        return new ArrayList<>(new HashSet<>(blockData));
    }

    public static File rotateImageFile(File file, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        Bitmap originalBitmap = loadBitmapWithRespectToMemory(file, 1024D);
        assert originalBitmap != null;
        Bitmap croppedBitmap = Bitmap.createBitmap(originalBitmap, 0, 0, originalBitmap.getWidth(), originalBitmap.getHeight(), matrix, false);
        if (originalBitmap != croppedBitmap)
            originalBitmap.recycle();
        String fileName = new SimpleDateFormat(Constants.NEW_FILE_DATE_FORMAT, Locale.US).format(new Date()) + ".jpg";
        return saveBitmap(croppedBitmap, fileName);
    }

    @NonNull
    public static Bitmap rotateBitmap(@NonNull Bitmap bitmap, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static void deleteFirebaseInstanceId() {
        new Thread(() -> {
            try {
                FirebaseInstanceId.getInstance().deleteInstanceId();
            } catch (IOException e) {
                Crashlytics.logException(e);
            } finally {
                FirebaseInstanceId.getInstance().getInstanceId();
            }
        }).start();
    }

    public static boolean isBimaEnabled() {
        return true;
    }

    public static <T> String convertListToJsonString(List<T> list) {
        Gson gson = new Gson();
        return gson.toJson(list, new TypeToken<List<T>>() {
        }.getType());
    }

    public static <T> List<T> retriveConvertedListFromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, new TypeToken<List<T>>() {
        }.getType());
    }

    public static File getFile(@Nullable String path) {
        return path == null ? null : new File(path);
    }

    public static String formatDecimalDigits(Double decimalNumber) {
        return new DecimalFormat("#,###").format(decimalNumber);
    }

    public static String camelCase(String stringToConvert) {
        if (TextUtils.isEmpty(stringToConvert)) {
            return "";
        }
        return Character.toUpperCase(stringToConvert.charAt(0)) +
                stringToConvert.substring(1).toLowerCase();
    }

    public static <T> boolean checkListIsEmpty(List<T> list) {
        return list == null || list.isEmpty();
    }

    public static String getMimeTypeFromLocalFileUri(String fileUri) {
        return MimeTypeMap.getSingleton().
                getMimeTypeFromExtension(getExtentionFromFileUri(fileUri));
    }

    public static String getMimeTypeFromExtension(String extension) {
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }

    public static String getExtentionFromFileUri(String fileUri) {
        return MimeTypeMap.getFileExtensionFromUrl(fileUri);
    }

    @Nullable
    public static Date getFormattedDateBackendWithZeroTime(@NonNull String date) {
        try {
            return getZeroTimeDate(getFormattedDate(date, Constants.INPUT_DATE_FORMAT));
        } catch (ParseException e) {
            return null;
        }
    }

    public static Date getZeroTimeDate(Date fecha) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(fecha);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static List<Uri> createImagesUrlAtServer(List<DocumentFile> documentFileList) {
        List<Uri> uriList = new ArrayList<>();
        for (DocumentFile documentFile : documentFileList) {
            Uri uri = Uri.parse(MDPreferenceManager.getBaseURL() +
                    GetClaimDocumentRequest.METHOD_NAME +
                    documentFile.getDocFileId());
            uriList.add(uri);
        }
        return uriList;
    }

    public static String getExtentionFromFileUri(Uri uri) {
        return getExtentionFromFileUri(uri.toString());
    }

    public static void resetFailureCounter() {
        MDPreferenceManager.setFlashCallVerificationFailureCount(0);
        MDPreferenceManager.setOtpVerificationFailureCount(0);
    }

    public static String getMimeTypeFromLocalFileUri(Uri fileUri) {
        return MimeTypeMap.getSingleton().
                getMimeTypeFromExtension(getExtentionFromFileUri(fileUri.toString()));
    }

    @NonNull
    public static String getGreetings(@NonNull Context context) {

        Calendar c = Calendar.getInstance();
        int hourOfDay = c.get(Calendar.HOUR_OF_DAY);

        String greetings;

        if (hourOfDay >= 5 && hourOfDay <= 11)
            greetings = context.getString(R.string.good_morning);

        else if (hourOfDay >= 12 && hourOfDay <= 16)
            greetings = context.getString(R.string.good_afternoon);

        else
            greetings = context.getString(R.string.good_evening);

        return greetings;
    }

    private static double convertBytesToKB(long bytes) {
        return bytes / 1024.0;
    }

    public static double convertBytesToMB(long bytes) {
        return convertBytesToKB(bytes) / 1024.0;
    }

    public static String getBaseURL(String flavor) {
        String baseUrl;
        switch (flavor) {
            case "qa":
                baseUrl = NativeKeysManager.getInstance().getBaseUrlQA();
                break;
            case "uat":
                baseUrl = NativeKeysManager.getInstance().getBaseUrlUAT();
                break;
            case "prod":
                baseUrl = NativeKeysManager.getInstance().getBaseUrlPROD();
                break;
            case "dev":
                baseUrl = NativeKeysManager.getInstance().getBaseUrlDev();
                break;
            case "mock":
                baseUrl = NativeKeysManager.getInstance().getBaseUrlMock();
                break;
            default:
                baseUrl = NativeKeysManager.getInstance().getBaseUrlQA();
        }
        return baseUrl;
    }

    public static void setErrorOnTezViews(View view, String error) {
        Optional.isInstanceOf(view, TezTextInputLayout.class, v -> v.setError(error));
        Optional.isInstanceOf(view, TezPinEditText.class, v -> v.setError(error));
        Optional.isInstanceOf(view, TezEditTextView.class, v -> v.setError(error));
    }
}
