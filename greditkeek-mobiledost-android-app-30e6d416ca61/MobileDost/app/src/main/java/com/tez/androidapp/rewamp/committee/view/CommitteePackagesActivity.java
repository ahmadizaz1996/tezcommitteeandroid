package com.tez.androidapp.rewamp.committee.view;

import android.content.Intent;
import android.os.Bundle;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.committee.adapter.CommitteePackagesAdapter;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.router.CommitteeInviteActivityRouter;
import com.tez.androidapp.rewamp.util.PackageListing;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.tez.androidapp.rewamp.committee.router.CommitteePackagesActivityRouter.SELECTED_AMOUNT;
import static com.tez.androidapp.rewamp.committee.router.CommitteePackagesActivityRouter.SELECTED_MEMBER;

public class CommitteePackagesActivity extends BaseActivity implements CommitteePackagesAdapter.PackageListener {

//    @BindView(R.id.tvWalletName)
//    private TezTextView tvWalletName;
//
//    @BindView(R.id.tvWalletNo)
//    private TezTextView tvWalletNo;
//
//    @BindView(R.id.tvAmountValue)
//    private TezTextView tvAmountValue;
//
//    @BindView(R.id.tvTransactionIdValue)
//    private TezTextView tvTransactionIdValue;
//
//    @BindView(R.id.tvDateValue)
//    private TezTextView tvDateValue;
//
//    @BindView(R.id.tvAmountRemainingValue)
//    private TezTextView tvAmountRemainingValue;
//
//    @BindView(R.id.tvDueDateValue)
//    private TezTextView tvDueDateValue;
//
//    @BindView(R.id.tvRepayNote)
//    private TezTextView tvRepayNote;
//
//    @BindView(R.id.groupRemainingAmount)
//    private Group groupRemainingAmount;
//
//    @BindView(R.id.btDone)
//    private TezButton btDone;

    @BindView(R.id.recyclerView)
    RecyclerView rvPackages;

    Integer amount = 0;
    Integer members = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee_packagesf);
        ViewBinder.bind(this);
        fetchExtras();
        fillPackagesRecylerView();
    }

    private void fetchExtras() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {

            if (intent.getExtras().containsKey(SELECTED_AMOUNT)) {
                amount = intent.getExtras().getInt(SELECTED_AMOUNT);
            }

            if (intent.getExtras().containsKey(SELECTED_MEMBER)) {
                members = intent.getExtras().getInt(SELECTED_MEMBER);
            }
        }
    }

    private void fillPackagesRecylerView() {
        CommitteePackagesAdapter committeePackagesAdapter = new CommitteePackagesAdapter(PackageListing.createInstallmentPackages(amount, members), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        this.rvPackages.setLayoutManager(linearLayoutManager);
        this.rvPackages.setAdapter(committeePackagesAdapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected String getScreenName() {
        return "CommitteePackagesActivity";
    }

    public static List<CommitteePackageModel> createInstallmentPackages(int totalAmount, int totalMembers) {
        List<CommitteePackageModel> packages = new ArrayList<>();
        if (isValid(totalAmount, totalMembers)) {
            //create Monthly
            int monthylInstallment = totalAmount / totalMembers;
            int duration = totalMembers;
            CommitteePackageModel monthlyPackage = new CommitteePackageModel(1, totalMembers, monthylInstallment);
            monthlyPackage.setSelectedAmount(totalAmount);
            monthlyPackage.setSelectedMembers(totalMembers);
            int biweeklyInstallment = monthylInstallment / 2;

            //create bi monthly
            CommitteePackageModel biweeklyPackage = new CommitteePackageModel(2, duration, biweeklyInstallment);
            biweeklyPackage.setSelectedAmount(totalAmount);
            biweeklyPackage.setSelectedMembers(totalMembers);
            packages.add(monthlyPackage);
            packages.add(biweeklyPackage);
        }

        return packages;
    }

    private static boolean isValid(int totalAmount, int totalMembers) {
        return totalAmount % totalMembers == 0 ? true : false;

    }

    @Override
    public void onClickPackage(@NonNull CommitteePackageModel packageModel) {
        CommitteeInviteActivityRouter.createInstance().setDependenciesAndRoute(this, packageModel);
    }
}
