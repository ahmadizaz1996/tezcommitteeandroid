package com.tez.androidapp.app.general.feature.find.agent.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.find.agent.callbacks.GetNearestEasypaisaAgentsCallback;
import com.tez.androidapp.app.general.feature.find.agent.models.network.dto.response.GetNearestEasypaisaAgentsResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by Rehman Murad Ali on 1/18/2018.
 */

public class GetNearestEasypaisaAgentsRH  extends BaseRH<GetNearestEasypaisaAgentsResponse> {

    private GetNearestEasypaisaAgentsCallback getNearestEasypaisaAgentsCallback;

    public GetNearestEasypaisaAgentsRH(BaseCloudDataStore baseCloudDataStore, GetNearestEasypaisaAgentsCallback getNearestEasypaisaAgentsCallback) {
        super(baseCloudDataStore);
        this.getNearestEasypaisaAgentsCallback = getNearestEasypaisaAgentsCallback;
    }


    @Override
    protected void onSuccess(Result<GetNearestEasypaisaAgentsResponse> value) {
        GetNearestEasypaisaAgentsResponse getNearestEasypaisaAgentsResponse= value.response().body();
        if(getNearestEasypaisaAgentsCallback!=null) {
            if (getNearestEasypaisaAgentsResponse != null)
                getNearestEasypaisaAgentsCallback.onGetNearestEasypaisaAgentsSuccess(getNearestEasypaisaAgentsResponse.getAgents());
            else
                getNearestEasypaisaAgentsCallback.onGetNearestEasypaisaAgentsSuccess(null);
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (getNearestEasypaisaAgentsCallback != null)
            getNearestEasypaisaAgentsCallback.onGetNearestEasypaisaAgentsFailure(errorCode,message);
    }
}
