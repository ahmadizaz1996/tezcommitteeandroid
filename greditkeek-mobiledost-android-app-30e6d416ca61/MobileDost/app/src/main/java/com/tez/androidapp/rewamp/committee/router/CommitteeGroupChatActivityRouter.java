package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeActivity;
import com.tez.androidapp.rewamp.committee.view.CommitteeGroupChatActivity;

import static com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter.MY_COMMITTEE_RESPONSE;

public class CommitteeGroupChatActivityRouter extends BaseActivityRouter {


    public static CommitteeGroupChatActivityRouter createInstance() {
        return new CommitteeGroupChatActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, @NonNull MyCommitteeResponse committeeResponse) {
        Intent intent = createIntent(from);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putSerializable(MY_COMMITTEE_RESPONSE, committeeResponse);
        intent.putExtras(bundle);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeGroupChatActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

}
