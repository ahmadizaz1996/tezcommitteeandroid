package com.tez.androidapp.app.vertical.bima.policies.models.network;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.ColorRes;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;

import java.io.Serializable;

/**
 * Created  on 9/1/2017.
 */

public class Policy implements Serializable, Parcelable {

    private Double coverage;
    private Boolean actionAsBuyPolicy = false;
    private Boolean mandatoryAdvance = false;
    private Boolean claimable;
    private Double remainingAmount;
    private String message = "";
    private Integer id;
    private Integer remainingDays;
    private Integer mobileUserInsurancePolicyId;
    private Integer mobileUserId;
    private Integer policyCount;
    private Integer claimCount;
    private String policyName;
    private String policyCategory;
    private String claimDetails;
    private String dateApplied;
    private String expiryDate;
    private String insuranceStatus;
    private String policyNumber;
    private String policyHolderName;
    private String insuranceProvider;
    private Boolean amountSelectionRequired;
    @Nullable
    private String activationDate;

    public Policy() {
    }

    protected Policy(Parcel in) {
        if (in.readByte() == 0) {
            coverage = null;
        } else {
            coverage = in.readDouble();
        }
        byte tmpActionAsBuyPolicy = in.readByte();
        actionAsBuyPolicy = tmpActionAsBuyPolicy == 0 ? null : tmpActionAsBuyPolicy == 1;
        byte tmpMandatoryAdvance = in.readByte();
        mandatoryAdvance = tmpMandatoryAdvance == 0 ? null : tmpMandatoryAdvance == 1;
        byte tmpClaimable = in.readByte();
        claimable = tmpClaimable == 0 ? null : tmpClaimable == 1;
        if (in.readByte() == 0) {
            remainingAmount = null;
        } else {
            remainingAmount = in.readDouble();
        }
        message = in.readString();
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            remainingDays = null;
        } else {
            remainingDays = in.readInt();
        }
        if (in.readByte() == 0) {
            mobileUserInsurancePolicyId = null;
        } else {
            mobileUserInsurancePolicyId = in.readInt();
        }
        if (in.readByte() == 0) {
            mobileUserId = null;
        } else {
            mobileUserId = in.readInt();
        }
        if (in.readByte() == 0) {
            policyCount = null;
        } else {
            policyCount = in.readInt();
        }
        if (in.readByte() == 0) {
            claimCount = null;
        } else {
            claimCount = in.readInt();
        }
        policyName = in.readString();
        policyCategory = in.readString();
        claimDetails = in.readString();
        dateApplied = in.readString();
        expiryDate = in.readString();
        insuranceStatus = in.readString();
        policyNumber = in.readString();
        policyHolderName = in.readString();
        insuranceProvider = in.readString();
        byte tmpAmountSelectionRequired = in.readByte();
        amountSelectionRequired = tmpAmountSelectionRequired == 0 ? null : tmpAmountSelectionRequired == 1;
        activationDate = in.readString();
    }

    public static final Creator<Policy> CREATOR = new Creator<Policy>() {
        @Override
        public Policy createFromParcel(Parcel in) {
            return new Policy(in);
        }

        @Override
        public Policy[] newArray(int size) {
            return new Policy[size];
        }
    };

    public Boolean getAmountSelectionRequired() {
        return amountSelectionRequired;
    }

    public void setAmountSelectionRequired(Boolean amountSelectionRequired) {
        this.amountSelectionRequired = amountSelectionRequired;
    }

    public String getInsuranceProvider() {
        return insuranceProvider;
    }

    public void setInsuranceProvider(String insuranceProvider) {
        this.insuranceProvider = insuranceProvider;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getInsuranceStatus() {
        return insuranceStatus;
    }

    @ColorRes
    public int getInsuranceStatusColor() {
        if (isLapsed() || isClaimInProcess())
            return R.color.editTextBottomLineColorGrey;

        else if (isClaimed())
            return R.color.textViewTitleColorBlue;

        else
            return R.color.textViewTextColorGreen;
    }

    public void setInsuranceStatus(String insuranceStatus) {
        this.insuranceStatus = insuranceStatus;
    }

    public Integer getMobileUserInsurancePolicyId() {
        return mobileUserInsurancePolicyId;
    }

    public void setMobileUserInsurancePolicyId(Integer mobileUserInsurancePolicyId) {
        this.mobileUserInsurancePolicyId = mobileUserInsurancePolicyId;
    }

    public boolean isHealthInsurance() {
        return getPolicyCategory().equalsIgnoreCase("HEALTH");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public String getPolicyCategory() {
        return policyCategory;
    }

    public void setPolicyCategory(String policyCategory) {
        this.policyCategory = policyCategory;
    }

    public String getDateApplied() {
        return Utility.getFormattedDateWithUpdatedTimeZone(dateApplied, Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT);
    }

    @Nullable
    public String getActivationDate() {
        return Utility.getFormattedDateWithUpdatedTimeZone(activationDate, Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT);
    }

    public void setDateApplied(String dateApplied) {
        this.dateApplied = dateApplied;
    }

    public Integer getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(Integer remainingDays) {
        this.remainingDays = remainingDays;
    }

    public Double getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(Double coverage) {
        this.remainingAmount = coverage;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRemainingAmountAsString() {
        if (remainingAmount == null)
            return "";

        return Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(remainingAmount);
    }

    public boolean isClaimable() {
        return claimable;
    }

    public void setClaimable(Boolean claimable) {
        this.claimable = claimable;
    }

    public String getPolicyHolderName() {
        return policyHolderName;
    }

    public void setPolicyHolderName(String policyHolderName) {
        this.policyHolderName = policyHolderName;
    }

    public boolean isClaimInProcess() {
        return insuranceStatus != null && insuranceStatus.equalsIgnoreCase(Constants.CLAIM_IN_PROCESS);
    }

    public boolean isActive() {
        return insuranceStatus != null && insuranceStatus.equalsIgnoreCase(Constants.ACTIVE);
    }

    public boolean isLapsed() {
        return insuranceStatus != null && insuranceStatus.equalsIgnoreCase(Constants.LAPSED);
    }

    public boolean isClaimed() {
        return insuranceStatus != null && insuranceStatus.equalsIgnoreCase(Constants.CLAIMED);
    }

    public Double getCoverage() {
        return coverage;
    }

    public void setCoverage(Double coverage) {
        this.coverage = coverage;
    }

    public Boolean getActionAsBuyPolicy() {
        return actionAsBuyPolicy;
    }

    public void setActionAsBuyPolicy(Boolean actionAsBuyPolicy) {
        this.actionAsBuyPolicy = actionAsBuyPolicy;
    }

    public Boolean getMandatoryAdvance() {
        return mandatoryAdvance;
    }

    public void setMandatoryAdvance(Boolean mandatoryAdvance) {
        this.mandatoryAdvance = mandatoryAdvance;
    }

    public Integer getMobileUserId() {
        return mobileUserId;
    }

    public void setMobileUserId(Integer mobileUserId) {
        this.mobileUserId = mobileUserId;
    }

    public Integer getPolicyCount() {
        return policyCount;
    }

    public void setPolicyCount(Integer policyCount) {
        this.policyCount = policyCount;
    }

    public Integer getClaimCount() {
        return claimCount;
    }

    public void setClaimCount(Integer claimCount) {
        this.claimCount = claimCount;
    }

    public String getClaimDetails() {
        return claimDetails;
    }

    public void setClaimDetails(String claimDetails) {
        this.claimDetails = claimDetails;
    }

    public String getExpiryDate() {
        return Utility.getFormattedDateWithUpdatedTimeZone(expiryDate, Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT);
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (coverage == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(coverage);
        }
        dest.writeByte((byte) (actionAsBuyPolicy == null ? 0 : actionAsBuyPolicy ? 1 : 2));
        dest.writeByte((byte) (mandatoryAdvance == null ? 0 : mandatoryAdvance ? 1 : 2));
        dest.writeByte((byte) (claimable == null ? 0 : claimable ? 1 : 2));
        if (remainingAmount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(remainingAmount);
        }
        dest.writeString(message);
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (remainingDays == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(remainingDays);
        }
        if (mobileUserInsurancePolicyId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(mobileUserInsurancePolicyId);
        }
        if (mobileUserId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(mobileUserId);
        }
        if (policyCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(policyCount);
        }
        if (claimCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(claimCount);
        }
        dest.writeString(policyName);
        dest.writeString(policyCategory);
        dest.writeString(claimDetails);
        dest.writeString(dateApplied);
        dest.writeString(expiryDate);
        dest.writeString(insuranceStatus);
        dest.writeString(policyNumber);
        dest.writeString(policyHolderName);
        dest.writeString(insuranceProvider);
        dest.writeByte((byte) (amountSelectionRequired == null ? 0 : amountSelectionRequired ? 1 : 2));
        dest.writeString(activationDate);
    }
}
