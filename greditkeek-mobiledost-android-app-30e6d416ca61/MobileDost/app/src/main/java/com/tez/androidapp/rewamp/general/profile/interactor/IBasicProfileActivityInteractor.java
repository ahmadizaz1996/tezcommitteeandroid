package com.tez.androidapp.rewamp.general.profile.interactor;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.ProfileBasicCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserBasicProfileRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserProfileRequest;

public interface IBasicProfileActivityInteractor {
    void updateUserBasicProfile(UpdateUserBasicProfileRequest updateUserProfileRequest);

    void getListOfOccupation(String lang);
}
