package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 6/14/2017.
 */

public class UserTokenRefreshRequest extends BaseRequest {
    public static final String METHOD_NAME = "v2/user/token/refresh";
    private String refreshToken;

    public UserTokenRefreshRequest(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
