package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.listener.CommitteeCheckInvitesLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeLoginLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeMetadataLIstener;
import com.tez.androidapp.rewamp.committee.listener.JoinCommitteeVerificationLIstener;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface IJoinVerificationCommitteeActivityInteractorOutput extends JoinCommitteeVerificationLIstener {
}
