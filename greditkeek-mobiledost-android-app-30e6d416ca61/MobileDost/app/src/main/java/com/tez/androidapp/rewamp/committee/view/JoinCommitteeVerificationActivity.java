package com.tez.androidapp.rewamp.committee.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Toast;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezPinView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.interactor.JoinCommitteeVerificationActivityInteractor;
import com.tez.androidapp.rewamp.committee.presenter.JoinComitteeVerificationPresenter;
import com.tez.androidapp.rewamp.committee.request.JoinCommitteeVerificationRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeVerificationResponse;
import com.tez.androidapp.rewamp.committee.router.JoinCommitteeInvitationActivityRouter;
import com.tez.androidapp.rewamp.committee.router.JoinCommitteeVerificationActivityRouter;
import com.tez.androidapp.rewamp.general.suspend.account.router.SuspendAccountFeedbackActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class JoinCommitteeVerificationActivity extends BaseActivity implements TezPinView.OnPinEnteredListener, IJoinCommitteeVerificationActivityView, DoubleTapSafeOnClickListener {


    @BindView(R.id.tvResendCode)
    private TezTextView tvResendCode;

    @BindView(R.id.pinViewOTP)
    private TezPinView pinViewOtp;

    @BindView(R.id.btContinue)
    private TezButton btnContinue;


    private final JoinComitteeVerificationPresenter joinCommitteeVerificationActivityPresenter;
    private CommitteeCheckInvitesResponse checkInvitesResponse;
    private String pin;

    public JoinCommitteeVerificationActivity() {
        this.joinCommitteeVerificationActivityPresenter = new JoinComitteeVerificationPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_committee_verification);
        ViewBinder.bind(this);
        fetchExtras();
        initOnClickListener();
    }

    private void fetchExtras() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey(JoinCommitteeVerificationActivityRouter.COMMITTEE_CHECK_INVITES_RESPONSE)) {
                checkInvitesResponse = getIntent().getExtras().getParcelable(JoinCommitteeVerificationActivityRouter.COMMITTEE_CHECK_INVITES_RESPONSE);
            }
        }
    }

    @Override
    protected String getScreenName() {
        return JoinCommitteeVerificationActivity.class.getSimpleName();
    }

    @Override
    public void onPinEntered(@NonNull String pin) {
        this.pin = pin;
        btnContinue.setButtonNormal();
    }

    private void initOnClickListener() {
//        tvResendCode.setDoubleTapSafeOnClickListener(view -> iSuspendAccountOTPActivityPresenter.resendCode());
        pinViewOtp.setOnPinEnteredListener(this);
        btnContinue.setDoubleTapSafeOnClickListener(this);
    }


    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void showLoader() {
        showTezLoader();
    }

    @Override
    public void hideLoader() {
        dismissTezLoader();
    }

    @Override
    public void onOtpVerification(JoinCommitteeVerificationResponse joinCommitteeVerificationResponse) {
        JoinCommitteeInvitationActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {
            case R.id.btContinue:
                sendOtpVerificationRequest();
                break;
        }
    }

    private void sendOtpVerificationRequest() {
        if (pinViewOtp.getValueText().length() == 4) {
            JoinCommitteeVerificationRequest joinCommitteeVerificationRequest = new JoinCommitteeVerificationRequest();
            joinCommitteeVerificationRequest.setOtp(Integer.parseInt(pin));
            joinCommitteeVerificationRequest.setCommiteeId(checkInvitesResponse.getCommitteeId());
            joinCommitteeVerificationRequest.setInviteId(checkInvitesResponse.getInviteId());

            joinCommitteeVerificationActivityPresenter.verifyCommitteeOtp(joinCommitteeVerificationRequest);
        } else {
            Toast.makeText(this, "Can not leave empty fields", Toast.LENGTH_SHORT).show();
        }
    }
}