package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.view.CommitteeSummaryActivity;
import com.tez.androidapp.rewamp.committee.view.MyCommitteeActivity;

public class MyCommitteeActivityRouter extends BaseActivityRouter {


    public static MyCommitteeActivityRouter createInstance() {
        return new MyCommitteeActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, MyCommitteeActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

}
