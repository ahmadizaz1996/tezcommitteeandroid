package com.tez.androidapp.app.general.feature.mobile.verification.handler;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.mobile.verification.callback.MobileVerificationCallback;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.response.MobileVerificationResponse;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by FARHAN DHANANI on 2/25/2019.
 */
public class MobileVerificationRH extends BaseRH<MobileVerificationResponse> {

    private final MobileVerificationCallback mobileNumberVerificationCallback;

    public MobileVerificationRH(BaseCloudDataStore baseCloudDataStore,
                                @Nullable final MobileVerificationCallback mobileNumberVerificationCallback) {
        super(baseCloudDataStore);
        this.mobileNumberVerificationCallback = mobileNumberVerificationCallback;
    }

    @Override
    protected void onSuccess(Result<MobileVerificationResponse> value) {
        Optional.ifPresent(value.response().body(),
                mobileNumberVerificationResponse -> {
                    Optional.doWhen(isErrorFree(mobileNumberVerificationResponse),
                            () -> Optional.ifPresent(this.mobileNumberVerificationCallback,
                                    MobileVerificationCallback::onMobileNumberVerificationSuccess),
                            () -> this.onFailure(mobileNumberVerificationResponse.getStatusCode(), mobileNumberVerificationResponse.getErrorDescription()));
                });
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        Optional.ifPresent(this.mobileNumberVerificationCallback,
                mobileVerificationCallback -> {
                    mobileVerificationCallback.onMobileNumberVerificatoinFailure(errorCode, message);
                });
    }

}
