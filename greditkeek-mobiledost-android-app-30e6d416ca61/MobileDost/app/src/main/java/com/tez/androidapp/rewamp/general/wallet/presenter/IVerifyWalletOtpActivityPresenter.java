package com.tez.androidapp.rewamp.general.wallet.presenter;

public interface IVerifyWalletOtpActivityPresenter {

    void addWallet(int serviceProviderId, String mobileAccountNumber, String pin, boolean isDefault);

    void verifyAddWalletOtp(int serviceProviderId, String mobileAccountNumber, String otp);

    void resendAddWalletOtp(int serviceProviderId, String mobileAccountNumber);
}
