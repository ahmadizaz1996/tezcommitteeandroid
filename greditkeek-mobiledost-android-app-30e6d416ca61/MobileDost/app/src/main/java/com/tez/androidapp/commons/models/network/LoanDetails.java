package com.tez.androidapp.commons.models.network;

import android.os.Parcel;
import android.os.Parcelable;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created  on 4/7/2017.
 */

public class LoanDetails implements Serializable, Parcelable {

    public static final Creator<LoanDetails> CREATOR = new Creator<LoanDetails>() {
        @Override
        public LoanDetails createFromParcel(Parcel in) {
            return new LoanDetails(in);
        }

        @Override
        public LoanDetails[] newArray(int size) {
            return new LoanDetails[size];
        }
    };
    private static final String OUTPUT_DATE_FORMAT = "MMM dd, yyyy";
    private static final String INPUT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private Double amountToReturn;
    private Double amountDue;
    private Double advanceAmount;
    private Double charges;
    private Double processingFee;
    private Double disbursedAmount;
    private Double tenure;
    private Double amountAssigned;
    private Double repayed;
    private Double limit;
    private Double latePaymentCharges;
    private String dateDisbursed;
    private String dueDate;
    private String returnDate;
    private String tenureTitle;
    private String status;
    private Integer loanId;
    private Wallet wallet;
    private LoanDetails loanDetails;
    private Boolean limitCancelLock;
    private Boolean deviceLock;
    private String transactionId;
    private String limitLockTimestamp;

    protected LoanDetails(Parcel in) {
        if (in.readByte() == 0) {
            amountToReturn = null;
        } else {
            amountToReturn = in.readDouble();
        }
        if (in.readByte() == 0) {
            amountDue = null;
        } else {
            amountDue = in.readDouble();
        }
        if (in.readByte() == 0) {
            advanceAmount = null;
        } else {
            advanceAmount = in.readDouble();
        }
        if (in.readByte() == 0) {
            charges = null;
        } else {
            charges = in.readDouble();
        }
        if (in.readByte() == 0) {
            processingFee = null;
        } else {
            processingFee = in.readDouble();
        }
        if (in.readByte() == 0) {
            disbursedAmount = null;
        } else {
            disbursedAmount = in.readDouble();
        }
        if (in.readByte() == 0) {
            tenure = null;
        } else {
            tenure = in.readDouble();
        }
        if (in.readByte() == 0) {
            amountAssigned = null;
        } else {
            amountAssigned = in.readDouble();
        }
        if (in.readByte() == 0) {
            repayed = null;
        } else {
            repayed = in.readDouble();
        }
        if (in.readByte() == 0) {
            limit = null;
        } else {
            limit = in.readDouble();
        }
        if (in.readByte() == 0) {
            latePaymentCharges = null;
        } else {
            latePaymentCharges = in.readDouble();
        }
        dateDisbursed = in.readString();
        dueDate = in.readString();
        returnDate = in.readString();
        tenureTitle = in.readString();
        status = in.readString();
        limitLockTimestamp = in.readString();
        if (in.readByte() == 0) {
            loanId = null;
        } else {
            loanId = in.readInt();
        }
        wallet = in.readParcelable(Wallet.class.getClassLoader());
        loanDetails = in.readParcelable(LoanDetails.class.getClassLoader());
        byte tmpLimitCancelLock = in.readByte();
        limitCancelLock = tmpLimitCancelLock == 0 ? null : tmpLimitCancelLock == 1;
        byte tmpDeviceLock = in.readByte();
        deviceLock = tmpDeviceLock == 0 ? null : tmpDeviceLock == 1;
        transactionId = in.readString();
    }

    public Double getLimit() {
        return limit;
    }

    public void setLimit(Double limit) {
        this.limit = limit;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public LoanDetails getLoanDetails() {
        return loanDetails;
    }

    public void setLoanDetails(LoanDetails loanDetails) {
        this.loanDetails = loanDetails;
    }

    public String getDateDisbursed() {
        return Utility.getFormattedDate(dateDisbursed, INPUT_DATE_FORMAT, OUTPUT_DATE_FORMAT);
    }

    public void setDateDisbursed(String dateDisbursed) {
        this.dateDisbursed = dateDisbursed;
    }

    public String getDueDateFormatted() {
        return Utility.getFormattedDate(dueDate, INPUT_DATE_FORMAT, OUTPUT_DATE_FORMAT);
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getDueDateDDMMMM() {
        return Utility.getFormattedDate(dueDate, INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT_DD_MMMM_YYYY);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Boolean getLimitCancelLock() {
        return limitCancelLock;
    }

    public void setLimitCancelLock(Boolean limitCancelLock) {
        this.limitCancelLock = limitCancelLock;
    }

    public Double getAmountToReturn() {
        return amountToReturn != null ? amountToReturn : 0;
    }

    public void setAmountToReturn(Double amountToReturn) {
        this.amountToReturn = amountToReturn;
    }

    public Double getAmountDue() {
        return amountDue;
    }

    public void setAmountDue(Double amountDue) {
        this.amountDue = amountDue;
    }

    public Double getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(Double advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public Double getCharges() {
        return charges;
    }

    public void setCharges(Double charges) {
        this.charges = charges;
    }

    public Double getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(Double processingFee) {
        this.processingFee = processingFee;
    }

    public Double getDisbursedAmount() {
        return disbursedAmount;
    }

    public void setDisbursedAmount(Double disbursedAmount) {
        this.disbursedAmount = disbursedAmount;
    }

    public Double getTenure() {
        return tenure;
    }

    public void setTenure(Double tenure) {
        this.tenure = tenure;
    }

    public Double getAmountAssigned() {
        return amountAssigned;
    }

    public void setAmountAssigned(Double amountAssigned) {
        this.amountAssigned = amountAssigned;
    }

    public Double getRepayed() {
        return repayed != null ? repayed : 0;
    }

    public void setRepayed(Double repayed) {
        this.repayed = repayed;
    }

    public String getReturnDate() {
        return Utility.getFormattedDate(dueDate, INPUT_DATE_FORMAT, OUTPUT_DATE_FORMAT);
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnDateDDMMMM() {
        return Utility.getFormattedDate(returnDate, INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT_DD_MMMM_YYYY);
    }

    public String getTenureTitle() {
        return tenureTitle;
    }

    public void setTenureTitle(String tenureTitle) {
        this.tenureTitle = tenureTitle;
    }

    public Double getLatePaymentCharges() {
        return latePaymentCharges == null ? 0 : latePaymentCharges;
    }

    public void setLatePaymentCharges(Double latePaymentCharges) {
        this.latePaymentCharges = latePaymentCharges;
    }

    @Override
    public String toString() {
        return "LoanDetails{" +
                "amountToReturn=" + amountToReturn +
                ", amountDue=" + amountDue +
                ", advanceAmount=" + advanceAmount +
                ", charges=" + charges +
                ", processingFee=" + processingFee +
                ", disbursedAmount=" + disbursedAmount +
                ", tenure=" + tenure +
                ", amountAssigned=" + amountAssigned +
                ", repayed=" + repayed +
                ", dateDisbursed='" + dateDisbursed + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", returnDate='" + returnDate + '\'' +
                '}';
    }

    public boolean isTimeStampPassed() {
        if (limitLockTimestamp == null)
            return false;
        try {
            Date timeStampDate = Utility.getFormattedDate(limitLockTimestamp, Constants.INPUT_DATE_FORMAT);
            Date today = Calendar.getInstance().getTime();
            return timeStampDate.before(today);
        } catch (ParseException e) {
            return false;
        }
    }

    public String getDaysRemaining() {
        Date today = Utility.getZeroTimeDate(Calendar.getInstance().getTime());
        Date dueDateConverted = Utility.getFormattedDateBackendWithZeroTime(dueDate);
        if (dueDateConverted != null) {
            long diff = dueDateConverted.getTime() - today.getTime();
            return String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        }
        return "";
    }

    public boolean isDueDateToday() {
        Date dueDateConverted = Utility.getFormattedDateBackendWithZeroTime(dueDate);
        Date today = Utility.getZeroTimeDate(Calendar.getInstance().getTime());
        return dueDateConverted != null && dueDateConverted.equals(today);
    }

    public boolean isDueDatePassed() {
        Date dueDateConverted = Utility.getFormattedDateBackendWithZeroTime(dueDate);
        Date today = Utility.getZeroTimeDate(Calendar.getInstance().getTime());
        return dueDateConverted != null && dueDateConverted.before(today);
    }

    public Boolean getDeviceLock() {
        return deviceLock;
    }

    public void setDeviceLock(Boolean deviceLock) {
        this.deviceLock = deviceLock;
    }

    public String getTransactionId() {
        return transactionId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (amountToReturn == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(amountToReturn);
        }
        if (amountDue == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(amountDue);
        }
        if (advanceAmount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(advanceAmount);
        }
        if (charges == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(charges);
        }
        if (processingFee == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(processingFee);
        }
        if (disbursedAmount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(disbursedAmount);
        }
        if (tenure == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(tenure);
        }
        if (amountAssigned == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(amountAssigned);
        }
        if (repayed == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(repayed);
        }
        if (limit == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(limit);
        }
        if (latePaymentCharges == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(latePaymentCharges);
        }
        dest.writeString(dateDisbursed);
        dest.writeString(dueDate);
        dest.writeString(returnDate);
        dest.writeString(tenureTitle);
        dest.writeString(status);
        dest.writeString(limitLockTimestamp);
        if (loanId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(loanId);
        }
        dest.writeParcelable(wallet, flags);
        dest.writeParcelable(loanDetails, flags);
        dest.writeByte((byte) (limitCancelLock == null ? 0 : limitCancelLock ? 1 : 2));
        dest.writeByte((byte) (deviceLock == null ? 0 : deviceLock ? 1 : 2));
        dest.writeString(transactionId);
    }
}
