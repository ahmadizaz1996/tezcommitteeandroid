package com.tez.androidapp.rewamp.committee.view;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.committee.request.CommitteeWalletRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeInstallmentPayResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeVerificationResponse;
import com.tez.androidapp.rewamp.committee.response.PaymentInitiateResponse;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface ICommitteeWalletVerificationActivityView extends IBaseView {

    void showLoader();

    void hideLoader();

    void onInitiatePayment(PaymentInitiateResponse paymentInitiateResponse);

    void onInstallmentPay(CommitteeInstallmentPayResponse committeeInstallmentPayResponse);

}
