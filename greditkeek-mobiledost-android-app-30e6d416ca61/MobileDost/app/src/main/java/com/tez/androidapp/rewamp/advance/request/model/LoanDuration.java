package com.tez.androidapp.rewamp.advance.request.model;

/**
 * Created by Rehman Murad Ali
 **/
public class LoanDuration {

    private int dayTenure;
    private String title;
    private int minAmount;

    public LoanDuration(int dayTenure, String title, int minAmount) {
        this.dayTenure = dayTenure;
        this.minAmount = minAmount;
        this.title = title;
    }

    public int getDayTenure() {
        return dayTenure;
    }


    public String getTitle() {
        return title;
    }

    public int getMinAmount() {
        return minAmount;
    }

}
