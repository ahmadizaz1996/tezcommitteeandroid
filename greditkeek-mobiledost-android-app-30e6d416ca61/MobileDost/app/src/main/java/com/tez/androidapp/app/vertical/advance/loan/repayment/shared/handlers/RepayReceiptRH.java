package com.tez.androidapp.app.vertical.advance.loan.repayment.shared.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.RepayReceiptCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.RepayReceiptResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 8/29/2017.
 */

public class RepayReceiptRH extends BaseRH<RepayReceiptResponse> {

    private RepayReceiptCallback repayReceiptCallback;

    public RepayReceiptRH(BaseCloudDataStore baseCloudDataStore, RepayReceiptCallback repayReceiptCallback) {
        super(baseCloudDataStore);
        this.repayReceiptCallback = repayReceiptCallback;
    }

    @Override
    protected void onSuccess(Result<RepayReceiptResponse> value) {
        RepayReceiptResponse repayReceiptResponse = value.response().body();
        if (repayReceiptResponse != null) {
            if (isErrorFree(repayReceiptResponse))
                repayReceiptCallback.onRepayReceiptSuccess(repayReceiptResponse.getLoanDetails());
            else
                onFailure(repayReceiptResponse.getStatusCode(), repayReceiptResponse.getErrorDescription());
        } else sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        repayReceiptCallback.onRepayReceiptFailure(errorCode, message);
    }
}
