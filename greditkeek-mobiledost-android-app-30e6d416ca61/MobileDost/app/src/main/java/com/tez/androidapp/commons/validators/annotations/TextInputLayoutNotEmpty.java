package com.tez.androidapp.commons.validators.annotations;


import androidx.annotation.NonNull;

import com.tez.androidapp.commons.validators.filters.TextInputLayoutNotEmptyFilter;
import com.tez.androidapp.commons.validators.filters.TextInputLayoutRegexFilter;

import net.tez.validator.library.annotations.Filterable;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Filterable(TextInputLayoutNotEmptyFilter.class)
public @interface TextInputLayoutNotEmpty {
    int[] value();
}
