package com.tez.androidapp.rewamp.general.suspend.account.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutRegex;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.rewamp.general.suspend.account.router.SuspendAccountOTPActivityRouter;

import net.tez.fragment.util.optional.TextUtil;
import net.tez.validator.library.annotations.Order;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SuspendAccountActivity extends PlaybackActivity implements ValidationListener {

    @BindView(R.id.etMobileNumber)
    private TezEditTextView etMobileNumber;

    @TextInputLayoutRegex(regex = Constants.PHONE_NUMBER_VALIDATOR_REGEX, value = {1, R.string.incorrect_mobile_number})
    @Order(1)
    @BindView(R.id.tilMobileNumber)
    private TezTextInputLayout tilMobileNumber;

    @BindView(R.id.btContinue)
    private TezButton btContinue;

    private CompositeDisposable allDisposables;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suspend_account);
        this.updateToolbar(findViewById(R.id.toolbar));
        btContinue.setDoubleTapSafeOnClickListener(view -> continueSuspendAccount());
    }

    private void listenChangesOnViews() {
        allDisposables.add(RxTextView.textChanges(this.etMobileNumber).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.allDisposables = new CompositeDisposable();
        listenChangesOnViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        allDisposables.dispose();
    }

    private void continueSuspendAccount() {
        FieldValidator.validate(this, new ValidationListener() {
            @Override
            public void validateSuccess() {
                String userNumber = MDPreferenceManager.getPrincipalName();
                String etNumber = etMobileNumber.getValueText();
                if (TextUtil.equals(userNumber, etNumber)) {
                    SuspendAccountOTPActivityRouter.createInstance().setDependenciesAndRoute(SuspendAccountActivity.this);
                    finish();
                } else
                    Utility.setErrorOnTezViews(tilMobileNumber, getString(R.string.enter_your_mobile_number));
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                btContinue.setButtonInactive();
                filterChain.doFilter();
            }
        });
    }

    @OnClick(R.id.tvCancel)
    private void cancelSuspendAccount() {
        finish();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    public void validateSuccess() {
        this.btContinue.setButtonNormal();
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.btContinue.setButtonInactive();
        filterChain.doFilter();
    }

    @Override
    protected String getScreenName() {
        return "SuspendAccountActivity";
    }
}
