package com.tez.androidapp.rewamp.advance.repay.presenter;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.InitiateRepaymentCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.RepayLoanCallback;

public interface IVerifyRepaymentActivityInteractorOutput extends InitiateRepaymentCallback, RepayLoanCallback {
}
