
package com.tez.androidapp.rewamp.committee.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class CommitteeResponseFilterUpdates {

    @SerializedName("committeeId")
    private Long mCommitteeId;
    @SerializedName("creationDate")
    private String mCreationDate;
    @SerializedName("timeline")
    private List<CommitteeResponseTimeline> timeline;
    @SerializedName("startDate")
    private String mStartDate;

    public Long getmCommitteeId() {
        return mCommitteeId;
    }

    public void setmCommitteeId(Long mCommitteeId) {
        this.mCommitteeId = mCommitteeId;
    }

    public String getmCreationDate() {
        return mCreationDate;
    }

    public void setmCreationDate(String mCreationDate) {
        this.mCreationDate = mCreationDate;
    }

    public List<CommitteeResponseTimeline> getTimeline() {
        return timeline;
    }

    public void setTimeline(List<CommitteeResponseTimeline> timeline) {
        this.timeline = timeline;
    }

    public String getmStartDate() {
        return mStartDate;
    }

    public void setmStartDate(String mStartDate) {
        this.mStartDate = mStartDate;
    }
}
