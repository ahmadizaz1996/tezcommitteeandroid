package com.tez.androidapp.rewamp.committee.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteePackagesAdapter extends GenericRecyclerViewAdapter<CommitteePackageModel,
        CommitteePackagesAdapter.PackageListener,
        CommitteePackagesAdapter.PackageViewHolder> {

    public CommitteePackagesAdapter(@NonNull List<CommitteePackageModel> items, @Nullable PackageListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public PackageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.list_item_committee_packages_detail, parent);
        return new PackageViewHolder(view);
    }

    public interface PackageListener extends BaseRecyclerViewListener {
        void onClickPackage(@NonNull CommitteePackageModel packageModel);
    }

    class PackageViewHolder extends BaseViewHolder<CommitteePackageModel, PackageListener> {

        @BindView(R.id.tvAmount)
        private TezTextView tvAmount;

        @BindView(R.id.tvTitleAmount)
        private TezTextView tvTitleAmount;

        @BindView(R.id.tvMembers)
        private TezTextView tvMembers;

        @BindView(R.id.tvDuration)
        private TezTextView tvDuration;

        @BindView(R.id.tvInstallment)
        private TezTextView tvInstallment;

        @BindView(R.id.tvInstallmentType)
        private TezTextView tvInstallmentType;

        @BindView(R.id.availPackageButton)
        private TezButton availPackageButton;


        public PackageViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(CommitteePackageModel item, @Nullable PackageListener listener) {
            super.onBind(item, listener);
            tvAmount.setText("RS " + item.getSelectedAmount());
            if (item.getId() == 1) {
                tvTitleAmount.setText("Monthly Installments");
                tvInstallmentType.setText("Monthly Installments");
            } else {
                tvInstallmentType.setText("Bi Monthly Installments");
                tvTitleAmount.setText("Bi Monthly Installments");
            }
            tvMembers.setText(item.getSelectedMembers() + " Members");
            tvDuration.setText(item.getDurationInMonths() + " Months");
            tvInstallment.setText(" RS " + item.getInstallmentAmount());

            availPackageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null)
                        listener.onClickPackage(item);
                }
            });

        }
    }
}
