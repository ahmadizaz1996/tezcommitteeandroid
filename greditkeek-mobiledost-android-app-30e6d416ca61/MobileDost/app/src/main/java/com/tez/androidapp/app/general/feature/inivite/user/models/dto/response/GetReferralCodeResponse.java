package com.tez.androidapp.app.general.feature.inivite.user.models.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by FARHAN DHANANI on 9/6/2018.
 */
public class GetReferralCodeResponse extends BaseResponse{
    private String referralCode;

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }
}
