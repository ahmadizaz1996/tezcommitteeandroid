package com.tez.androidapp.commons.widgets;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;

/**
 * Created by VINOD KUMAR on 7/23/2019.
 */
public abstract class TezDialog extends Dialog {

    protected TezDialog(@NonNull Context context) {
        this(context, R.style.TezDialog);
    }

    protected TezDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected TezDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (WindowManager.BadTokenException | IllegalArgumentException ignore) {
            //Leave empty
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (WindowManager.BadTokenException | IllegalArgumentException ignore) {
            //Leave empty
        }
    }

    @Override
    public void onBackPressed() {

    }
}
