package com.tez.androidapp.rewamp.general.changepin;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public interface IChangePinActivityPresenter {

    void changePin(String newPin, String oldPin);
}
