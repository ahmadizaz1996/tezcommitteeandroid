package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.ValidateInfoCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.ValidateInfoResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by FARHAN DHANANI on 12/3/2018.
 */
public class ValidateInfoRH extends BaseRH<ValidateInfoResponse> {

    private ValidateInfoCallback validateInfoCallback;

    public ValidateInfoRH(BaseCloudDataStore baseCloudDataStore, ValidateInfoCallback validateInfoCallback) {
        super(baseCloudDataStore);
        this.validateInfoCallback = validateInfoCallback;
    }

    @Override
    protected void onSuccess(Result<ValidateInfoResponse> value) {
        ValidateInfoResponse validateInfoResponse = value.response().body();
        Optional.doWhen(validateInfoResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR,
                () -> validateInfoCallback.onValidateInfoSuccess(validateInfoResponse.getValidateInfo()),
                () -> onFailure(validateInfoResponse.getStatusCode(), validateInfoResponse.getErrorDescription()));
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.validateInfoCallback.onValidateInfoFailure(errorCode, message);
    }
}
