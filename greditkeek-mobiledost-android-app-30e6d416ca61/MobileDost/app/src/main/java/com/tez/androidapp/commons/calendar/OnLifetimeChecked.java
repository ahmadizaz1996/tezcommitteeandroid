package com.tez.androidapp.commons.calendar;

/**
 * Created by VINOD KUMAR on 10/29/2018.
 */
@FunctionalInterface
public interface OnLifetimeChecked {

    void onLifetimeChecked(String value);
}
