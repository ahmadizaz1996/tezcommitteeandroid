package com.tez.androidapp.rewamp.bima.products.policy.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.view.PurchasePolicyActivity;

public class PurchasePolicyActivityRouter extends BaseActivityRouter {

    public static final String TERMS_ACCEPTED = "TERMS_ACCEPTED";
    public static final String PRODUCT_ID = "PRODUCT_ID";
    public static final String PRODUCT_TITLE = "PRODUCT_TITLE";

    public static PurchasePolicyActivityRouter createInstance() {
        return new PurchasePolicyActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, int productId, String title, boolean termsAccepted) {
        Intent intent = createIntent(from);
        intent.putExtra(TERMS_ACCEPTED, termsAccepted);
        intent.putExtra(PRODUCT_ID, productId);
        intent.putExtra(PRODUCT_TITLE, title);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, PurchasePolicyActivity.class);
    }
}
