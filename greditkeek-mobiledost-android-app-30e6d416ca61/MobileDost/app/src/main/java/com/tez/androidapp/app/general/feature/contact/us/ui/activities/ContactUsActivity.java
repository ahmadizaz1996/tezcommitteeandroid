package com.tez.androidapp.app.general.feature.contact.us.ui.activities;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.CustomerSupport;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;


public class ContactUsActivity extends BaseActivity {

    @BindView(R.id.tvTitle)
    protected TezTextView tvTitle;

    @BindView(R.id.tvMessage)
    protected TezTextView tvMessage;

    private final BroadcastReceiver isOnValidatePinActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setResultCode(1);
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ViewBinder.bind(this);
    }

    @OnClick(R.id.btEmail)
    private void openEmail() {
        CustomerSupport.openEmail(this);
    }

    @OnClick(R.id.btCall)
    private void openDialer() {
        CustomerSupport.openDialer(this);
    }

    @OnClick(R.id.btWhatsapp)
    private void openWhatsapp() {
        CustomerSupport.openWhatsapp(this);
    }

    @OnClick(R.id.btMessenger)
    private void openMessenger() {
        CustomerSupport.openMessenger(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            unregisterReceiver(isOnValidatePinActivity);
        } catch (IllegalArgumentException e){
            //igonore
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(isOnValidatePinActivity, new IntentFilter(Constants.BROAD_CAST_RECIEVER_ON_VALIDATE_PIN_ACTIVITY));
    }

    @Override
    protected String getScreenName() {
        return "ContactUsActivity";
    }
}
