package com.tez.androidapp.commons.api.client;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.models.network.dto.request.ChangePinRequest;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.models.network.dto.request.ChangeTemporaryPinRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network.dto.request.SaveAttachmentsRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network.dto.request.SaveCNICPicturesRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network.dto.response.SaveAttachmentsResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network.dto.response.SaveCNICPicturesResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.AccountLinkRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.GetCnicUploadsRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.GetCompleteProfileRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.GetUserProfileRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.ProfileBasicRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.QuestionOptionsRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.SetUserPictureRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateCnicPictureRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateMobileNumberVerifyOTPRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdatePersonalInfoRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateProfileWithCnicRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserBasicProfileRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserProfileRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.ValidateInfoRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.AccountLinkResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.GetCnicUploadsResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.GetCompleteProfileResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.GetUserProfileResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.ProfileBasicResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.QuestionOptionsResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.UpdateCnicPictureResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.UpdatePersonalInfoResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.UpdateProfileWithCnicResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.UpdateUserBasicProfileResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.ValidateInfoResponse;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.models.network.dto.request.SuspendAccountRequest;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.models.network.dto.request.SuspendAccountVerifyOTPRequest;
import com.tez.androidapp.app.general.feature.faq.models.network.dto.request.GetFAQsRequest;
import com.tez.androidapp.app.general.feature.faq.models.network.dto.response.GetFAQsResponse;
import com.tez.androidapp.app.general.feature.inivite.user.models.dto.request.GetReferralCodeRequest;
import com.tez.androidapp.app.general.feature.inivite.user.models.dto.response.GetReferralCodeResponse;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request.MobileVerificationRequest;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request.NumberVerifyGenerateOtpRequest;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request.NumberVerifyOtpRequest;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.response.MobileVerificationResponse;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.response.NumberVerifyGenerateOtpResponse;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.response.NumberVerifyOtpResponse;
import com.tez.androidapp.app.general.feature.transactions.models.network.dto.request.GetUserTransactionsRequest;
import com.tez.androidapp.app.general.feature.transactions.models.network.dto.response.GetUserTransactionsResponse;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request.AddWalletRequest;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request.AddWalletResendOTPRequest;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request.AddWalletVerifyOTPRequest;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request.DeleteWalletRequest;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.network.dto.request.GetAllWalletRequest;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.network.dto.request.SetDefaultWalletRequest;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.network.dto.response.GetAllWalletResponse;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.request.GetActiveInsurancePolicyRequest;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.request.GetManageBeneficiaryPolicyRequest;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response.GetActiveInsurancePolicyResponse;
import com.tez.androidapp.commons.location.models.network.dto.request.GetCitiesRequest;
import com.tez.androidapp.commons.location.models.network.dto.response.GetCitiesResponse;
import com.tez.androidapp.commons.models.network.AccessToken;
import com.tez.androidapp.commons.models.network.dto.request.UserDashboardRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserDeviceKeyRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserInfoRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserLogoutRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserNotificationsRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserReActivateAccountOTPRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserReActivateAccountResendOTPRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserSignUpRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserSignUpSetPinRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserStatusRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserTokenRefreshRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserVerifyMobileNumberConfirmRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserVerifyMobileNumberOTPRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserVerifyRequest;
import com.tez.androidapp.commons.models.network.dto.request.VerifyUserReferralCodeRequest;
import com.tez.androidapp.commons.models.network.dto.response.UserDashboardResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserInfoResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserNotificationsResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserSignUpResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserSignUpSetPinResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserStatusResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserVerifyResponse;
import com.tez.androidapp.commons.models.network.dto.response.VerifyUserReferralCodeResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.models.request.VerifyActiveDeviceRequest;
import com.tez.androidapp.repository.network.models.response.VerifyActiveDeviceResponse;
import com.tez.androidapp.rewamp.dashboard.request.DashboardActionCardRequest;
import com.tez.androidapp.rewamp.dashboard.request.DashboardAdvanceCardRequest;
import com.tez.androidapp.rewamp.dashboard.request.UserProfileStatusRequest;
import com.tez.androidapp.rewamp.dashboard.response.DashboardActionCardResponse;
import com.tez.androidapp.rewamp.dashboard.response.DashboardAdvanceCardResponse;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;
import com.tez.androidapp.rewamp.dashboard.response.UserProfileStatusResponse;
import com.tez.androidapp.rewamp.general.suspend.account.FeedbackRequest;
import com.tez.androidapp.rewamp.signup.languagecall.LanguageRequest;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created  on 2/17/2017.
 */

public interface MobileDostUserAPI {

    @POST(UserInfoRequest.METHOD_NAME)
    Observable<Result<UserInfoResponse>> getUserInfo(@Header(Constants.HEADER_API_KEY) String apiKey,
                                                     @Body UserInfoRequest userInfoRequest);

    @POST(UserSignUpRequest.METHOD_NAME)
    Observable<Result<UserSignUpResponse>> signUp(@Header(Constants.HEADER_API_KEY) String apiKey, @Body UserSignUpRequest userSignUpRequest);

    @POST(UserLoginRequest.METHOD_NAME)
    Observable<Result<UserLoginResponse>> login(@Body UserLoginRequest userLoginRequest);

    @POST(UserVerifyRequest.METHOD_NAME)
    @Headers({"Accept: application/json"})
    Observable<Result<UserVerifyResponse>> verify(@Body UserVerifyRequest userVerifyRequest);

    @GET(VerifyUserReferralCodeRequest.METHOD_NAME)
    Observable<Result<VerifyUserReferralCodeResponse>> verifyRefCode(@Path(VerifyUserReferralCodeRequest.Params.REF_CODE) String refcode);

    @POST(UserTokenRefreshRequest.METHOD_NAME)
    @Headers({"Accept: application/json"})
    Observable<Result<AccessToken>> tokenRefresh(@Body UserTokenRefreshRequest userTokenRefreshRequest);

    @POST(AddWalletRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> addWallet(@Header(Constants.HEADER_API_KEY) String apiKey, @Body AddWalletRequest addWalletRequest);

    @GET(DashboardAdvanceCardRequest.METHOD_NAME)
    Observable<Result<DashboardAdvanceCardResponse>> getDashboardAdvanceCard();

    @GET(DashboardActionCardRequest.METHOD_NAME)
    Observable<Result<DashboardActionCardResponse>> getDashboardActionCard();

    @GET(UserProfileStatusRequest.METHOD_NAME)
    Observable<Result<UserProfileStatusResponse>> getUserProfileStatus();

    @GET(AddWalletResendOTPRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> addWalletResendOTP(@Header(Constants.HEADER_API_KEY) String apiKey,
                                                        @Path(AddWalletResendOTPRequest.Params.MOBILE_ACCOUNT_NUMBER) String mobileAccountNumber,
                                                        @Path(AddWalletResendOTPRequest.Params.SERVICE_PROVIDER_ID) Integer serviceProviderId);

    @GET(AddWalletVerifyOTPRequest.METHOD_NAME)
    Observable<Result<DashboardCardsResponse>> addWalletVerifyOTP(@Header(Constants.HEADER_API_KEY) String apiKey,
                                                        @Path(AddWalletVerifyOTPRequest.Params.MOBILE_ACCOUNT_NUMBER) String mobileAccountNumber,
                                                        @Path(AddWalletVerifyOTPRequest.Params.SERVICE_PROVIDER_ID) Integer serviceProviderId,
                                                        @Path(AddWalletVerifyOTPRequest.Params.OTP) String otp);

    @Multipart
    @PUT(SaveAttachmentsRequest.METHOD_NAME)
    Observable<Result<SaveAttachmentsResponse>> uploadCNICPhotos(@Header(Constants.HEADER_API_KEY) String apiKey, @Part MultipartBody.Part frontCNICPhoto, @Part MultipartBody.Part backCNICPhoto, @Part MultipartBody.Part selfie);

    @Multipart
    @POST(UpdateCnicPictureRequest.METHOD_NAME)
    Observable<Result<UpdateCnicPictureResponse>> updateCnicPictures(@Part MultipartBody.Part image,
                                                                     @Part MultipartBody.Part isAutoDetected);

    @Multipart
    @POST(UpdatePersonalInfoRequest.METHOD_NAME)
    Observable<Result<UpdatePersonalInfoResponse>> updatePersonalInfo(@Part MultipartBody.Part image,
                                                                      @Part MultipartBody.Part personalInfo);

    @GET(GetCnicUploadsRequest.METHOD_NAME)
    Observable<Result<GetCnicUploadsResponse>> getCnicUploads();

    @GET(GetCompleteProfileRequest.METHOD_NAME)
    Observable<Result<GetCompleteProfileResponse>> getCompleteUserProfile();

    @Multipart
    @PUT(SaveCNICPicturesRequest.METHOD_NAME)
    Observable<Result<SaveCNICPicturesResponse>> uploadCNICPictures(@Part MultipartBody.Part frontCNICPhoto, @Part MultipartBody.Part backCNICPhoto, @Part MultipartBody.Part
            selfie);

    @PUT(UpdateProfileWithCnicRequest.METHOD_NAME)
    Observable<Result<UpdateProfileWithCnicResponse>> updateProfileWithCnic(@Body UpdateProfileWithCnicRequest updateProfileWithCnicRequest);

    @GET(GetCitiesRequest.METHOD_NAME)
    Observable<Result<GetCitiesResponse>> getCities();

    @GET(GetFAQsRequest.METHOD_NAME)
    Observable<Result<GetFAQsResponse>> getFAQs(@Query(GetFAQsRequest.Params.LANGUAGE_CODE) String languageCode);

    @GET(GetReferralCodeRequest.METHOD_NAME)
    Observable<Result<GetReferralCodeResponse>> getReferralCode();

    @GET(UserVerifyMobileNumberConfirmRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> confirmMobileNumberToVerifyIt(@Path(UserVerifyMobileNumberConfirmRequest.Params.MOBILE_NUMBER) String mobileNumber);

    @POST(UserLogoutRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> logout(@Body UserLogoutRequest userLogoutRequest);

    @POST(UserSignUpSetPinRequest.METHOD_NAME)
    Observable<Result<UserSignUpSetPinResponse>> setUserPin(@Header(Constants.HEADER_API_KEY) String apiKey, @Body UserSignUpSetPinRequest userSignUpSetPinRequest);

    @GET(UserNotificationsRequest.METHOD_NAME)
    Observable<Result<UserNotificationsResponse>> getUserNotifications(
            @Query(UserNotificationsRequest.Params.START_INDEX)  int page,
            @Query(UserNotificationsRequest.Params.MAX_RECORDS) int maxPage);

    @GET(SuspendAccountRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> suspendAccount();

    @GET(SuspendAccountVerifyOTPRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> suspendAccountVerifyOTP(@Path(SuspendAccountVerifyOTPRequest.Params.OTP) String otp);

    @POST(FeedbackRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> feedback(@Body FeedbackRequest feedbackRequest);

    @POST(ChangePinRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> changePin(@Header(Constants.HEADER_API_KEY) String apiKey, @Body ChangePinRequest changePinRequest);

    @Multipart
    @PUT(SetUserPictureRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> setUserPicture(@Part MultipartBody.Part profilePicture);

    @GET(GetManageBeneficiaryPolicyRequest.METHOD_NAME)
    Observable<Result<GetActiveInsurancePolicyResponse>> getManageBeneficiaryPolicies();

    @POST(UserDeviceKeyRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> setUserDeviceKey(@Header(Constants.HEADER_API_KEY) String apiKey,
                                                      @Body UserDeviceKeyRequest userDeviceKeyRequest);

    @GET(GetUserTransactionsRequest.METHOD_NAME)
    Observable<Result<GetUserTransactionsResponse>> getUserTransactions();

    @PUT(UpdateUserProfileRequest.METHOD_NAME)
    Observable<Result<DashboardCardsResponse>> updateUserProfile(@Body UpdateUserProfileRequest updateUserProfileRequest);

    @POST(ValidateInfoRequest.METHOD_NAME)
    Observable<Result<ValidateInfoResponse>> validateInfo(@Body ValidateInfoRequest validateInfoRequest);

    @GET(UpdateMobileNumberVerifyOTPRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> updateMobileNumberVerifyOTP(@Path(UpdateMobileNumberVerifyOTPRequest.Params.OTP) String otp,
                                                                 @Path(UpdateMobileNumberVerifyOTPRequest.Params.CNIC) String cnic);


    @GET(GetUserProfileRequest.METHOD_NAME)
    Observable<Result<GetUserProfileResponse>> getUserProfile();


    @PUT(UpdateUserBasicProfileRequest.METHOD_NAME)
    Observable<Result<UpdateUserBasicProfileResponse>> updateUserBasicProfile(@Body  UpdateUserBasicProfileRequest updateUserBasicProfileRequest);

    @GET(ProfileBasicRequest.METHOD_NAME)
    Observable<Result<ProfileBasicResponse>> getUserBasicProfile();


    @POST(AccountLinkRequest.METHOD_NAME)
    Observable<Result<AccountLinkResponse>> linkUserSocialAccount(@Body AccountLinkRequest accountLinkRequest);

    @GET(UserDashboardRequest.METHOD_NAME)
    Observable<Result<UserDashboardResponse>> getUserDashboard();

    @POST(ChangeTemporaryPinRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> changeTemporaryPin(@Body ChangeTemporaryPinRequest changeTemporaryPinRequest);

    @POST(UserReActivateAccountOTPRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> reActivateAccount(@Body UserReActivateAccountOTPRequest userReActivateAccountOTPRequest);

    @GET(UserReActivateAccountResendOTPRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> resendOTPToReActivateAccount();

    @GET(GetAllWalletRequest.METHOD_NAME)
    Observable<Result<GetAllWalletResponse>> getAllWallet();

    @POST(SetDefaultWalletRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> setDefaultWallet(@Body SetDefaultWalletRequest setDefaultWalletRequest);

    @DELETE(DeleteWalletRequest.METHOD_NAME)
    Observable<Result<DashboardCardsResponse>> deleteWallet(@Path(DeleteWalletRequest.Params.MOBILE_ACCOUNT_ID) Integer mobileAccountId);

    @PUT(LanguageRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> language(@Path(LanguageRequest.Params.LANGUAGE_CODE) String languageCode);

    @GET(UserStatusRequest.METHOD_NAME)
    Observable<Result<UserStatusResponse>> getUserStatus();

    @POST(VerifyActiveDeviceRequest.METHOD_NAME)
    Observable<Result<VerifyActiveDeviceResponse>> verifyActiveDevice(@Header(Constants.HEADER_API_KEY) String apiKey, @Body VerifyActiveDeviceRequest verifyActiveDeviceRequest);

    @POST(MobileVerificationRequest.METHOD_NAME)
    Observable<Result<MobileVerificationResponse>> mobileNumberVerification(@Header(Constants.HEADER_API_KEY) String apiKey, @Body MobileVerificationRequest mobileVerificationRequest);

    @POST(NumberVerifyGenerateOtpRequest.METHOD_NAME)
    Observable<Result<NumberVerifyGenerateOtpResponse>> numberVerifyGenerateOtp(@Header(Constants.HEADER_API_KEY) String apiKey, @Body NumberVerifyGenerateOtpRequest numberVerifyGenerateOtpRequest);

    @POST(NumberVerifyOtpRequest.METHOD_NAME)
    Observable<Result<NumberVerifyOtpResponse>> numberVerifyOtp(@Header(Constants.HEADER_API_KEY) String apiKey, @Body NumberVerifyOtpRequest numberVerifyOtpRequest);
}
