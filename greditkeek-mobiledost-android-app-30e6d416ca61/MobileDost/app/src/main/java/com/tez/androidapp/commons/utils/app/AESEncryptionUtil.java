package com.tez.androidapp.commons.utils.app;

import android.util.Base64;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.commons.managers.NativeKeysManager;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Rehman Murad Ali on 3/13/2018.
 */

public class AESEncryptionUtil {
    private static final String CHARACTER_ENCODING = "UTF-8";
    private static final String CIPHER_TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private static final String AES_ENCRYPTION_ALGORITHM = "AES";
    private static String aesKey;
    private static AESEncryptionUtil aesEncryptionUtil;
    private SecretKeySpec secretKeySpec;
    private IvParameterSpec ivParameterSpec;

    static {
        aesKey = NativeKeysManager.getInstance().getEncryptionKey();
    }

    private AESEncryptionUtil() {
        byte[] keyBytes = getKeyBytes();
        this.secretKeySpec = new SecretKeySpec(keyBytes, AES_ENCRYPTION_ALGORITHM);
        this.ivParameterSpec = new IvParameterSpec(keyBytes);
    }

    public static AESEncryptionUtil getInstance() {
        if (aesEncryptionUtil == null)
            aesEncryptionUtil = new AESEncryptionUtil();
        return aesEncryptionUtil;
    }

    private byte[] getKeyBytes() {
        byte[] keyBytes = new byte[16];
        byte[] parameterKeyBytes = new byte[0];
        try {
            parameterKeyBytes = aesKey.getBytes(CHARACTER_ENCODING);
        } catch (UnsupportedEncodingException e) {
            Crashlytics.logException(e);
        }
        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
        return keyBytes;
    }

    private byte[] decrypt(byte[] cipherText) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
        cipherText = cipher.doFinal(cipherText);
        return cipherText;
    }

    private byte[] encrypt(byte[] plainText) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        plainText = cipher.doFinal(plainText);
        return plainText;
    }

    public String encrypt(String inputText) {
        try {
            String plainText = Base64.encodeToString(inputText.getBytes(), Base64.DEFAULT);
            plainText = plainText.replaceAll("\n", "");
            byte[] plainTextbytes = plainText.getBytes(CHARACTER_ENCODING);
            byte[] encrypted = encrypt(plainTextbytes);
            return Base64.encodeToString(encrypted, Base64.DEFAULT).replaceAll("\n", "");

        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return "";
    }

    public String decrypt(String encryptedText) {
        try {
            byte[] cipheredBytes = Base64.decode(encryptedText.getBytes(), Base64.DEFAULT);
            byte[] decrypted = decrypt(cipheredBytes);
            return new String(Base64.decode(decrypted, Base64.DEFAULT));
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return "";
    }
}
