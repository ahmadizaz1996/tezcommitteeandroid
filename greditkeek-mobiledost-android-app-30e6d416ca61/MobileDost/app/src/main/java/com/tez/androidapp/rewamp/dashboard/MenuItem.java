package com.tez.androidapp.rewamp.dashboard;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

/**
 * Created by VINOD KUMAR on 6/19/2019.
 */
public class MenuItem {

    @DrawableRes
    private int image;

    @StringRes
    private int name;

    private int index;

    @Nullable
    private DoubleTapSafeOnClickListener onClickListener;

    public MenuItem(@DrawableRes int image,
                    @StringRes int name,
                    int index,
                    @Nullable DoubleTapSafeOnClickListener listener) {
        this.image = image;
        this.name = name;
        this.index = index;
        this.onClickListener = listener;
    }

    public MenuItem(@DrawableRes int image,
                    @StringRes int name,
                    int index) {
        this(image, name, index, null);
    }

    public MenuItem(@StringRes int name, int index) {
        this(0, name, index, null);
    }

    public int getImage() {
        return image;
    }

    public int getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    @Nullable
    public DoubleTapSafeOnClickListener getOnClickListener() {
        return onClickListener;
    }
}
