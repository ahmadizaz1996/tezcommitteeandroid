package com.tez.androidapp.repository.network.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.handlers.callbacks.VerifyActiveDeviceCallback;
import com.tez.androidapp.repository.network.models.response.VerifyActiveDeviceResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by Rehman Murad Ali on 4/19/2018.
 */
public class VerifyActiveDeviceRH extends BaseRH<VerifyActiveDeviceResponse> {

    private VerifyActiveDeviceCallback verifyActiveDeviceCallback;

    public VerifyActiveDeviceRH(BaseCloudDataStore baseCloudDataStore, @Nullable VerifyActiveDeviceCallback verifyActiveDeviceCallback) {
        super(baseCloudDataStore);
        this.verifyActiveDeviceCallback = verifyActiveDeviceCallback;
    }


    @Override
    protected void onSuccess(Result<VerifyActiveDeviceResponse> value) {
        VerifyActiveDeviceResponse verifyActiveDeviceResponse = value.response().body();
        if (isErrorFree(verifyActiveDeviceResponse)) {
            if (verifyActiveDeviceCallback != null)
                verifyActiveDeviceCallback.onVerifyActiveDeviceSuccess(verifyActiveDeviceResponse);
        } else
            onFailure(verifyActiveDeviceResponse.getStatusCode(), verifyActiveDeviceResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (verifyActiveDeviceCallback != null) verifyActiveDeviceCallback.onVerifyActiveDeviceFailure(errorCode,message);
    }
}
