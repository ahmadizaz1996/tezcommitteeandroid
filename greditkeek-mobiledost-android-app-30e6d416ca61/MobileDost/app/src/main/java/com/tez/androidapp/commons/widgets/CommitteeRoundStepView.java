package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.tez.androidapp.R;

import androidx.annotation.FontRes;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;

public class CommitteeRoundStepView extends TezConstraintLayout {

    public CommitteeRoundStepView(Context context) {
        super(context);
    }

    public CommitteeRoundStepView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CommitteeRoundStepView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(@NonNull Context context, AttributeSet attrs) {
        this.setBackgroundColor(getColor(context, R.color.transparent));
        inflate(context, R.layout.round_step_committee, this);
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CommitteeRoundStepView);
            try {
                int step = typedArray.getInt(R.styleable.CommitteeRoundStepView_stepcomm, 0);
                if (step == 0) setStep(Steps.DETAILS);
                else if (step == 1) setStep(Steps.CPACKAGES);
                else if (step == 2) setStep(Steps.INVITES);
                else {
                    setStep(Steps.COMPLETE);
                }
            } finally {
                typedArray.recycle();
            }
        }

    }

    public void setStep(@NonNull Steps step) {

        switch (step) {

            case DETAILS:
                userIsFillingDetails();
                break;
            case COMPLETE:
                userCompleteDetail();
                break;
            case CPACKAGES:
                packages();
                break;
            case INVITES:
                invites();
                break;
        }
    }

    private void packages() {
        userIsFillingDetails();
        TezTextView tvNumber = findViewById(R.id.tvStepNumberPackages);
        TezTextView tvName = findViewById(R.id.tvStepPackagesName);
        TezImageView ivIcon = findViewById(R.id.ivStepPackagesIcon);
        View viewStepSeparator = findViewById(R.id.viewStepSeparator2);
        int colorGreen = getColor(getContext(), R.color.textViewTextColorGreen);
        int colorWhite = getColor(getContext(), R.color.buttonTextColorWhite);
        viewStepSeparator.setBackgroundColor(colorGreen);
        tvNumber.setTextColor(colorWhite);
        tvNumber.setBackgroundResource(R.drawable.circle_green_filled);
        tvName.setTextColor(colorGreen);
        changeTvNameFont(tvName, R.font.barlow_semi_bold);
        ivIcon.setColorFilter(colorGreen);
        unfillinvites();
        unfillCom();
    }
    private void userCompleteDetail() {
        invites();
        TezTextView tvNumber = findViewById(R.id.tvStepNumberComplete);
        TezTextView tvName = findViewById(R.id.tvStepCompleteName);
        TezImageView ivIcon = findViewById(R.id.ivStepCompleteIcon);
        View viewStepSeparator = findViewById(R.id.viewStepSeparator3);
        viewStepSeparator.setBackgroundColor(getColor(getContext(), R.color.roundStepSeparator));
        int colorWhite = getColor(getContext(), R.color.buttonTextColorWhite);
        int colorGreen = getColor(getContext(), R.color.textViewTextColorGreen);
        tvNumber.setTextColor(colorWhite);
        tvNumber.setBackgroundResource(R.drawable.circle_green_filled);
        tvName.setTextColor(colorGreen);
        ivIcon.setColorFilter(colorGreen);
        changeTvNameFont(tvName, R.font.barlow_semi_bold);
    }

    private void invites() {
        packages();
        TezTextView tvNumber = findViewById(R.id.tvStepNumberInvites);
        TezTextView tvName = findViewById(R.id.tvStepInvitesName);
        TezImageView ivIcon = findViewById(R.id.ivStepInvitesIcon);
        View viewStepSeparator = findViewById(R.id.viewStepSeparator3);
        viewStepSeparator.setBackgroundColor(getColor(getContext(), R.color.roundStepSeparator));
        int colorWhite = getColor(getContext(), R.color.buttonTextColorWhite);
        int colorGreen = getColor(getContext(), R.color.textViewTextColorGreen);
        tvNumber.setTextColor(colorWhite);
        tvNumber.setBackgroundResource(R.drawable.circle_green_filled);
        tvName.setTextColor(colorGreen);
        ivIcon.setColorFilter(colorGreen);
        changeTvNameFont(tvName, R.font.barlow_semi_bold);
        unfillCom();

    }

    private void userIsFillingDetails() {
        TezTextView tvNumber = findViewById(R.id.tvStepNumberCnic);
        TezTextView tvName = findViewById(R.id.tvStepCnicName);
        TezImageView ivIcon = findViewById(R.id.ivStepCnicIcon);
        View viewStepSeparator = findViewById(R.id.viewStepSeparator1);
        viewStepSeparator.setBackgroundColor(getColor(getContext(), R.color.roundStepSeparator));
        int colorWhite = getColor(getContext(), R.color.buttonTextColorWhite);
        int colorGreen = getColor(getContext(), R.color.textViewTextColorGreen);
        tvNumber.setTextColor(colorWhite);
        tvNumber.setBackgroundResource(R.drawable.circle_green_filled);
        tvName.setTextColor(colorGreen);
        ivIcon.setColorFilter(colorGreen);
        changeTvNameFont(tvName, R.font.barlow_semi_bold);
        unfillinvites();
        unfillpackages();
        unfillCom();
    }


    private void changeTvNameFont(TezTextView tvName, @FontRes int font) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
            tvName.setTypeface(ResourcesCompat.getFont(getContext(), font));
        else
            tvName.setTypeface(getResources().getFont(font));
    }


    private void unfillCnic() {
        TezTextView tvNumber = findViewById(R.id.tvStepNumberCnic);
        TezTextView tvName = findViewById(R.id.tvStepCnicName);
        TezImageView ivIcon = findViewById(R.id.ivStepCnicIcon);
        tvNumber.setTextColor(getColor(getContext(), R.color.screen_background));
        tvNumber.setBackgroundResource(R.drawable.round_bg_grey);
        tvName.setTextColor(getColor(getContext(), R.color.textViewTextColorBlack));
        changeTvNameFont(tvName, R.font.barlow_medium);
        ivIcon.setColorFilter(getColor(getContext(), R.color.editTextBottomLineColorGrey));
    }

    private void unfillpackages() {
        TezTextView tvNumber = findViewById(R.id.tvStepNumberPackages);
        TezTextView tvName = findViewById(R.id.tvStepPackagesName);
        TezImageView ivIcon = findViewById(R.id.ivStepPackagesIcon);
        tvNumber.setTextColor(getColor(getContext(), R.color.screen_background));
        tvNumber.setBackgroundResource(R.drawable.round_bg_grey);
        tvName.setTextColor(getColor(getContext(), R.color.textViewTextColorBlack));
        changeTvNameFont(tvName, R.font.barlow_medium);
        ivIcon.setColorFilter(getColor(getContext(), R.color.editTextBottomLineColorGrey));
    }

    private void unfillCom() {
        TezTextView tvNumber = findViewById(R.id.tvStepNumberComplete);
        TezTextView tvName = findViewById(R.id.tvStepCompleteName);
        TezImageView ivIcon = findViewById(R.id.ivStepCompleteIcon);
        tvNumber.setTextColor(getColor(getContext(), R.color.screen_background));
        tvNumber.setBackgroundResource(R.drawable.round_bg_grey);
        tvName.setTextColor(getColor(getContext(), R.color.textViewTextColorBlack));
        changeTvNameFont(tvName, R.font.barlow_medium);
        ivIcon.setColorFilter(getColor(getContext(), R.color.editTextBottomLineColorGrey));
    }

    private void unfillinvites() {
        TezTextView tvNumber = findViewById(R.id.tvStepNumberInvites);
        TezTextView tvName = findViewById(R.id.tvStepInvitesName);
        TezImageView ivIcon = findViewById(R.id.ivStepInvitesIcon);
        tvNumber.setTextColor(getColor(getContext(), R.color.screen_background));
        tvNumber.setBackgroundResource(R.drawable.round_bg_grey);
        tvName.setTextColor(getColor(getContext(), R.color.textViewTextColorBlack));
        changeTvNameFont(tvName, R.font.barlow_medium);
        ivIcon.setColorFilter(getColor(getContext(), R.color.editTextBottomLineColorGrey));
    }


    public enum Steps {
        DETAILS,
        COMPLETE,
        CPACKAGES,
        INVITES
    }

}
