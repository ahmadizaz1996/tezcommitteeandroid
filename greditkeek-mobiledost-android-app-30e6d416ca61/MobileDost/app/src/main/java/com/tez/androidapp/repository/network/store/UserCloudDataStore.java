package com.tez.androidapp.repository.network.store;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserLoginCallback;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserSignUpSetPinCallback;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.handlers.UserLoginRH;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.handlers.UserSignUpSetPinRH;
import com.tez.androidapp.app.general.feature.authentication.shared.callbacks.UserInfoCallback;
import com.tez.androidapp.app.general.feature.authentication.shared.handler.UserInfoRH;
import com.tez.androidapp.app.general.feature.authentication.signup.callbacks.UserSignUpCallback;
import com.tez.androidapp.app.general.feature.authentication.signup.handlers.UserSignUpRH;
import com.tez.androidapp.app.general.feature.faq.callbacks.GetFAQsCallback;
import com.tez.androidapp.app.general.feature.faq.handlers.GetFAQsRH;
import com.tez.androidapp.app.general.feature.mobile.verification.callback.MobileVerificationCallback;
import com.tez.androidapp.app.general.feature.mobile.verification.callback.NumberVerificationViaOtp;
import com.tez.androidapp.app.general.feature.mobile.verification.callback.NumberVerifyOtpCallback;
import com.tez.androidapp.app.general.feature.mobile.verification.handler.MobileVerificationRH;
import com.tez.androidapp.app.general.feature.mobile.verification.handler.NumberVerifyGenerateOtpRH;
import com.tez.androidapp.app.general.feature.mobile.verification.handler.NumberVerifyOtpRH;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request.MobileVerificationRequest;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request.NumberVerifyGenerateOtpRequest;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request.NumberVerifyOtpRequest;
import com.tez.androidapp.commons.api.client.MobileDostUserAPI;
import com.tez.androidapp.commons.location.callbacks.GetCitiesCallback;
import com.tez.androidapp.commons.location.handlers.GetCitiesRH;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.AccessToken;
import com.tez.androidapp.commons.models.network.dto.request.UserDeviceKeyRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserInfoRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserSignUpRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserSignUpSetPinRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserTokenRefreshRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.handlers.UserDeviceKeyRH;
import com.tez.androidapp.repository.network.handlers.UserTokenRefreshRH;
import com.tez.androidapp.repository.network.handlers.VerifyActiveDeviceRH;
import com.tez.androidapp.repository.network.handlers.callbacks.UserDeviceKeyCallback;
import com.tez.androidapp.repository.network.handlers.callbacks.VerifyActiveDeviceCallback;
import com.tez.androidapp.repository.network.models.request.VerifyActiveDeviceRequest;
import com.tez.androidapp.rewamp.committee.listener.CommitteeLoginLIstener;
import com.tez.androidapp.rewamp.committee.request.CommitteeLoginRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeLoginRH;
import com.tez.androidapp.rewamp.committee.shared.api.client.CommitteeAPI;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Data store to make user related non authenticated network calls
 * <p>
 * Created  on 12/6/2016.
 */

public class UserCloudDataStore extends EncryptionBaseCloudDataStore {

    private static UserCloudDataStore userCloudDataStore;
    private final CommitteeAPI committeeAPI;
    private MobileDostUserAPI mobileDostUserAPI;


    private UserCloudDataStore() {
        super();
        mobileDostUserAPI = tezAPIBuilder.build().create(MobileDostUserAPI.class);
        committeeAPI = tezAPIBuilder.build().create(CommitteeAPI.class);

    }

    public static void clear() {
        userCloudDataStore = null;
    }

    public static UserCloudDataStore getInstance() {
        if (userCloudDataStore == null)
            userCloudDataStore = new UserCloudDataStore();
        return userCloudDataStore;
    }

    public void getUserInfo(UserInfoRequest userInfoRequest, UserInfoCallback userInfoCallback) {
        mobileDostUserAPI.getUserInfo(
                Utility.getAPIKey(
                        Utility.PrincipalType.FACEBOOK.getValue() == userInfoRequest.getPrincipalTypeId()
                                || Utility.PrincipalType.GOOGLE.getValue() == userInfoRequest.getPrincipalTypeId() ?
                                userInfoRequest.getSocialId() :
                                userInfoRequest.getMobileNumber())
                , userInfoRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new UserInfoRH(this, userInfoCallback));
    }

    public void login(UserLoginRequest userLoginRequest, UserLoginCallback userLoginCallback) {
        mobileDostUserAPI.login(userLoginRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new UserLoginRH(this, userLoginCallback));
    }

    public void signUp(UserSignUpRequest userSignUpRequest, UserSignUpCallback
            userSignUpCallback) {
        mobileDostUserAPI.signUp(Utility.getAPIKey(userSignUpRequest.getMobileNumber()), userSignUpRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new UserSignUpRH(this, userSignUpCallback) {
                    @Override
                    public void updateUserMobileNumber() {
                        MDPreferenceManager.setPrincipalName(userSignUpRequest.getMobileNumber());
                    }
                });
    }

    public void getCities(GetCitiesCallback getCitiesCallback) {
        mobileDostUserAPI.getCities().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new GetCitiesRH(this, getCitiesCallback));
    }

    public void getFAQs(GetFAQsCallback getFAQsCallback, String selectedLanguage) {
        mobileDostUserAPI.getFAQs(selectedLanguage).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new GetFAQsRH(this, getFAQsCallback));
    }

    public void setUserPin(String pin, UserSignUpSetPinCallback userSignUpSetPinCallback) {
        mobileDostUserAPI.setUserPin(Utility.getAPIKey(), new UserSignUpSetPinRequest(pin)).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new UserSignUpSetPinRH(this, userSignUpSetPinCallback));
    }

    public void tokenRefresh(BaseRH baseRH) {
        AccessToken accessToken = MDPreferenceManager.getAccessToken();
        if (accessToken != null)
            mobileDostUserAPI.tokenRefresh(new UserTokenRefreshRequest(accessToken.getRefreshToken())).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new
                    UserTokenRefreshRH(this, baseRH));
        else {
            Utility.showSessionExpireDialogFromNonActivityClass();
            Crashlytics.log("AccessToken is null in preferences! Starting WelcomeBackActivity on session expired!");
        }
    }

    public void setUserDeviceKey(String principalName, String deviceKey, String imei, UserDeviceKeyCallback userDeviceKeyCallback) {
        mobileDostUserAPI.setUserDeviceKey(Utility.getAPIKey(), new UserDeviceKeyRequest(principalName, deviceKey, imei)).observeOn(AndroidSchedulers.mainThread()).subscribeOn
                (Schedulers.io()).subscribe(new UserDeviceKeyRH(this, userDeviceKeyCallback));
    }

    public void verifyActiveDevice(VerifyActiveDeviceRequest verifyActiveDeviceRequest, VerifyActiveDeviceCallback verifyActiveDeviceCallback) {
        mobileDostUserAPI.verifyActiveDevice(Utility.getAPIKey(), verifyActiveDeviceRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn
                (Schedulers.io()).subscribe(new VerifyActiveDeviceRH(this, verifyActiveDeviceCallback));
    }

    public void mobileNumberVerification(final MobileVerificationRequest mobileNumberVerificationRequest,
                                         final MobileVerificationCallback mobileNumberVerificationCallback) {
        mobileDostUserAPI.mobileNumberVerification(Utility.getAPIKey(), mobileNumberVerificationRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(
                new MobileVerificationRH(this, mobileNumberVerificationCallback));
    }

    public void numberVerifyGenerateOtp(final NumberVerifyGenerateOtpRequest numberVerifyGenerateOtpRequest,
                                        final NumberVerificationViaOtp numberVerificationGenerateOtp) {
        numberVerificationGenerateOtp.registerSmsReciever();
        mobileDostUserAPI.numberVerifyGenerateOtp(Utility.getAPIKey(), numberVerifyGenerateOtpRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(
                new NumberVerifyGenerateOtpRH(this, numberVerificationGenerateOtp));
    }

    public void numberVerifyOtp(final NumberVerifyOtpRequest numberVerifyOtpRequest, final NumberVerifyOtpCallback numberVerifyOtpCallback) {
        mobileDostUserAPI.numberVerifyOtp(Utility.getAPIKey(), numberVerifyOtpRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(
                new NumberVerifyOtpRH(this, numberVerifyOtpCallback));
    }

    public void loginCall(CommitteeLoginRequest committeeLoginRequest, CommitteeLoginLIstener committeeLoginLIstener) {
        committeeAPI.committeeLogin(committeeLoginRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeLoginRH(this, committeeLoginLIstener));
    }
}
