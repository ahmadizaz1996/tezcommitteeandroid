package com.tez.androidapp.rewamp.advance.repay.interactor;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.InitiateRepaymentCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.RepayLoanCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.InitiateRepaymentRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.RepayLoanRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.InitiateRepaymentResponse;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.advance.repay.presenter.IVerifyRepaymentActivityInteractorOutput;

public class VerifyRepaymentActivityInteractor implements IVerifyRepaymentActivityInteractor {

    private final IVerifyRepaymentActivityInteractorOutput iVerifyRepaymentActivityInteractorOutput;

    public VerifyRepaymentActivityInteractor(IVerifyRepaymentActivityInteractorOutput iVerifyRepaymentActivityInteractorOutput) {
        this.iVerifyRepaymentActivityInteractorOutput = iVerifyRepaymentActivityInteractorOutput;
    }

    @Override
    public void initiateRepayment(@NonNull InitiateRepaymentRequest initiateRepaymentRequest) {
        LoanCloudDataStore.getInstance().initiateRepayment(initiateRepaymentRequest, new InitiateRepaymentCallback() {
            @Override
            public void onInitiateRepaymentSuccess(InitiateRepaymentResponse initiateRepaymentResponse) {
                iVerifyRepaymentActivityInteractorOutput.onInitiateRepaymentSuccess(initiateRepaymentResponse);
            }

            @Override
            public void onInitiateRepaymentFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    initiateRepayment(initiateRepaymentRequest);
                else
                    iVerifyRepaymentActivityInteractorOutput.onInitiateRepaymentFailure(errorCode, message);
            }
        });
    }

    @Override
    public void repayLoan(@NonNull RepayLoanRequest repayLoanRequest) {
        LoanCloudDataStore.getInstance().repayLoan(repayLoanRequest, new RepayLoanCallback() {
            @Override
            public void onRepayLoanSuccess(LoanDetails loanDetails) {
                iVerifyRepaymentActivityInteractorOutput.onRepayLoanSuccess(loanDetails);
            }

            @Override
            public void onRepayLoanFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    repayLoan(repayLoanRequest);
                else
                    iVerifyRepaymentActivityInteractorOutput.onRepayLoanFailure(errorCode, message);
            }
        });
    }

    @Override
    public void resendCode(@NonNull InitiateRepaymentRequest request, @NonNull InitiateRepaymentCallback callback) {
        LoanCloudDataStore.getInstance().initiateRepayment(request, new InitiateRepaymentCallback() {
            @Override
            public void onInitiateRepaymentSuccess(InitiateRepaymentResponse initiateRepaymentResponse) {
                callback.onInitiateRepaymentSuccess(initiateRepaymentResponse);
            }

            @Override
            public void onInitiateRepaymentFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    resendCode(request, callback);
                else
                    callback.onInitiateRepaymentFailure(errorCode, message);
            }
        });
    }
}
