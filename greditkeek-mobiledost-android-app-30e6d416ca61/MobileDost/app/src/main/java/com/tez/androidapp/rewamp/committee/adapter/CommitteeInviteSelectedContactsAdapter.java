package com.tez.androidapp.rewamp.committee.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.models.extract.ExtractedContact;
import com.tez.androidapp.commons.utils.app.Log;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezCheckBox;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeInviteSelectedContactsAdapter extends GenericRecyclerViewAdapter<ExtractedContact,
        CommitteeInviteSelectedContactsAdapter.ContactsListener,
        CommitteeInviteSelectedContactsAdapter.ContactsViewHolder> {

    public CommitteeInviteSelectedContactsAdapter(@NonNull List<ExtractedContact> items, @Nullable ContactsListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public ContactsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.item_committee_members, parent);
        return new ContactsViewHolder(view);
    }

    public interface ContactsListener extends BaseRecyclerViewListener {
        void onClickContact(@NonNull ExtractedContact contactsModel);
    }

    static class ContactsViewHolder extends BaseViewHolder<ExtractedContact, ContactsListener> {

        @BindView(R.id.tvCount)
        private TezTextView tvCount;

        @BindView(R.id.tvMemberName)
        private TezTextView tvMemberName;

        @BindView(R.id.cv_img_mem)
        private CircleImageView memberImgView;

        @BindView(R.id.tvCross)
        private TezTextView tvCross;

        @BindView(R.id.tvLetters)
        private TezTextView tvLetters;

        public ContactsViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(ExtractedContact item, @Nullable ContactsListener listener) {
            super.onBind(item, listener);

            Log.d("CONTACT_ADAPTER : ", item.getDisplayName() + " selected on top");
            tvCount.setVisibility(View.GONE);
            tvMemberName.setText(item.getDisplayName());
            tvCross.setVisibility(View.VISIBLE);
            tvCross.setDoubleTapSafeOnClickListener(new DoubleTapSafeOnClickListener() {
                @Override
                public void doubleTapSafeOnClick(View view) {
                    listener.onClickContact(item);
                }
            });
            tvLetters.setVisibility(View.VISIBLE);
            String charOne = String.valueOf(item.getDisplayName().charAt(0));
            tvLetters.setText(charOne);
        }
    }
}
