package com.tez.androidapp.app.general.feature.feedback.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by Rehman Murad Ali on 8/30/2017.
 */

public interface UserFeedbackCallback {
    void onUserFeedbackSuccess(BaseResponse baseResponse);
    void onUserFeedbackFailure(int errorCode, String message);
}
