package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.JoinCommitteeVerificationResponse;
import com.tez.androidapp.rewamp.committee.response.PaymentInitiateResponse;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface CommitteeWalletVerificationLIstener {
    void onPaymentInitiateSuccess(PaymentInitiateResponse paymentInitiateResponse);

    void onPaymentInitiateFailure(int errorCode, String message);

}
