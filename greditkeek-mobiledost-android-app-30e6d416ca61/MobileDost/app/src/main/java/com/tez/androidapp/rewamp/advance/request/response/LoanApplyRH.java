package com.tez.androidapp.rewamp.advance.request.response;


import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.advance.request.callback.LoanApplyCallback;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;


/**
 * Created  on 3/22/2017.
 */

public class LoanApplyRH extends DashboardCardsRH<DashboardCardsResponse> {

    private LoanApplyCallback loanApplyCallback;

    public LoanApplyRH(BaseCloudDataStore baseCloudDataStore, LoanApplyCallback loanApplyCallback) {
        super(baseCloudDataStore);
        this.loanApplyCallback = loanApplyCallback;
    }

    @Override
    protected void onSuccess(Result<DashboardCardsResponse> value) {
        super.onSuccess(value);
        DashboardCardsResponse response = value.response().body();
        if (response != null)
            loanApplyCallback.onLoanApplySuccess(response.getStatusCode());
        else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        loanApplyCallback.onLoanApplyFailure(errorCode, message);
    }
}
