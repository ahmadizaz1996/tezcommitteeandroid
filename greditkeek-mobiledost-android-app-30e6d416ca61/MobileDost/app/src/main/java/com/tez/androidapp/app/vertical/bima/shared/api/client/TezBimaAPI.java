package com.tez.androidapp.app.vertical.bima.shared.api.client;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.dto.response.GetAdvanceBimaBeneficiaryResponse;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.dto.response.SetAdvanceBimaBeneficiaryResponse;
import com.tez.androidapp.app.general.feature.questions.models.network.models.dto.response.QuestionsResponse;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.models.network.dto.request.AdvanceBimaBeneficiaryRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.ClaimDocumentListRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.ClaimQuestionsRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.GetSizeForExtentionRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.MyClaimListRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.MyClaimRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.UploadClaimAnswerRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.UploadClaimDocumentRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.ClaimDocumentListResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.GetSizeForExtentionResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.MyClaimListResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.MyClaimResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.UploadClaimAnswerResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.UploadClaimDocumentResponse;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.request.GetActiveInsurancePolicyRequest;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.request.GetActivieInsurancePolicyDetailsRequest;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response.GetActiveInsurancePolicyDetailsResponse;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response.GetActiveInsurancePolicyResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.rewamp.advance.request.request.InsuranceRemainingStepsRequest;
import com.tez.androidapp.rewamp.advance.request.response.LoanRemainingStepsResponse;
import com.tez.androidapp.rewamp.bima.products.policy.request.InsuranceProductDetailRequest;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.request.PurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.response.InsuranceProductDetailResponse;
import com.tez.androidapp.rewamp.bima.products.policy.response.InitiatePurchasePolicyResponse;
import com.tez.androidapp.rewamp.bima.products.policy.response.PurchasePolicyResponse;
import com.tez.androidapp.rewamp.bima.products.request.ProductDetailTermConditionRequest;
import com.tez.androidapp.rewamp.bima.products.request.ProductListRequest;
import com.tez.androidapp.rewamp.bima.products.request.ProductPolicyRequest;
import com.tez.androidapp.rewamp.bima.products.request.ProductTermConditionsRequest;
import com.tez.androidapp.rewamp.bima.products.response.ProductListResponse;
import com.tez.androidapp.rewamp.bima.products.response.ProductPolicyResponse;
import com.tez.androidapp.rewamp.bima.products.response.ProductTermConditionsResponse;
import com.tez.androidapp.rewamp.general.beneficiary.request.BeneficiariesRequest;
import com.tez.androidapp.rewamp.general.beneficiary.response.BeneficiariesResponse;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created  on 8/25/2017.
 */

public interface TezBimaAPI {

    @GET(BeneficiariesRequest.METHOD_NAME)
    Observable<Result<BeneficiariesResponse>> getBeneficiaries();

    @GET(AdvanceBimaBeneficiaryRequest.GET_METHOD_NAME)
    Observable<Result<GetAdvanceBimaBeneficiaryResponse>> getAdvanceBimaBeneficiary();

    @PUT(AdvanceBimaBeneficiaryRequest.PUT_METHOD_NAME)
    Observable<Result<BaseResponse>> setDefaultBeneficiary(@Path(AdvanceBimaBeneficiaryRequest.Params.BENEFICIARY_ID) int beneficiaryId);

    @GET(ClaimQuestionsRequest.METHOD_NAME)
    Observable<Result<QuestionsResponse>> getClaimQuestions(@Path(ClaimQuestionsRequest.Params.INSURANCE_POLICY_ID) int insuarancePolicyId);

    @GET(ClaimDocumentListRequest.METHOD_NAME)
    Observable<Result<ClaimDocumentListResponse>> getClaimDocumentList(@Path(ClaimDocumentListRequest.Params.INSURANCE_POLICY_ID) int insuarancePolicyId);

    @GET(MyClaimListRequest.METHOD_NAME)
    Observable<Result<MyClaimListResponse>> getInsuranceClaims();

    @GET(MyClaimRequest.METHOD_NAME)
    Observable<Result<MyClaimResponse>> getInsuranceClaim(@Path(MyClaimRequest.Params.INSURANCE_CLAIM_ID) int insuranceClaimId);

    @GET(GetActiveInsurancePolicyRequest.METHOD_NAME)
    Observable<Result<GetActiveInsurancePolicyResponse>> getInsurancePolicies();

    @GET(ProductTermConditionsRequest.METHOD_NAME)
    Observable<Result<ProductTermConditionsResponse>> getProductTermConditions(@Path(ProductTermConditionsRequest.Params.PRODUCT_ID) int productId);

    @GET(ProductDetailTermConditionRequest.METHOD_NAME)
    Observable<Result<ResponseBody>> getProductTermCondition(@Path(ProductDetailTermConditionRequest.Params.PRODUCT_ID) int productId);

    @GET(GetActivieInsurancePolicyDetailsRequest.METHOD_NAME)
    Observable<Result<GetActiveInsurancePolicyDetailsResponse>> getInsurancePolicyDetails(@Path(GetActivieInsurancePolicyDetailsRequest.Params.ACTIVE_INSURANCE_POLICY_ID) int insurancePolicyId);

    @GET(ProductPolicyRequest.METHOD_NAME)
    Observable<Result<ProductPolicyResponse>> getProductPolicy(@Path(ProductPolicyRequest.Params.ID) int insurancePolicyId);

    @GET(GetSizeForExtentionRequest.METHOD_NAME)
    Observable<Result<GetSizeForExtentionResponse>> getSizeForExtention();

    @POST(UploadClaimAnswerRequest.METHOD_NAME)
    Observable<Result<UploadClaimAnswerResponse>> uploadClaimAnswers(@Body UploadClaimAnswerRequest uploadClaimAnswerRequest);

    @POST(AdvanceBimaBeneficiaryRequest.POST_METHOD_NAME)
    Observable<Result<SetAdvanceBimaBeneficiaryResponse>> setAdvanceBimaBeneficiary(@Body AdvanceBimaBeneficiaryRequest advanceBimaBeneficiaryRequest);

    @Multipart
    @POST(UploadClaimDocumentRequest.METHOD_NAME)
    Observable<Result<UploadClaimDocumentResponse>> uploadClaimDocument(@Header(Constants.HEADER_API_KEY) String apiKey,
                                                                        @Part MultipartBody.Part docId,
                                                                        @Part List<MultipartBody.Part> partsImages);

    @GET(ProductListRequest.METHOD_NAME)
    Observable<Result<ProductListResponse>> getInsuranceProducts();

    @GET(InsuranceRemainingStepsRequest.METHOD_NAME)
    Observable<Result<LoanRemainingStepsResponse>> getInsuranceRemainingSteps();

    @GET(InsuranceProductDetailRequest.METHOD_NAME)
    Observable<Result<InsuranceProductDetailResponse>> getInsuranceProductDetail(@Path(InsuranceProductDetailRequest.Params.PRODUCT_ID) int productId);

    @POST(InitiatePurchasePolicyRequest.METHOD_NAME)
    Observable<Result<InitiatePurchasePolicyResponse>> initiatePurchasePolicy(@Body InitiatePurchasePolicyRequest initiatePurchasePolicyRequest);

    @POST(PurchasePolicyRequest.METHOD_NAME)
    Observable<Result<PurchasePolicyResponse>> purchasePolicy(@Body PurchasePolicyRequest purchasePolicyRequest);

}
