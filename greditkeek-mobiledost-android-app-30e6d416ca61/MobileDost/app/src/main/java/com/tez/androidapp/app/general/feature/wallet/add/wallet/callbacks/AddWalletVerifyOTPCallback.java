package com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks;

/**
 * Created  on 2/15/2017.
 */

public interface AddWalletVerifyOTPCallback {

    void onAddWalletVerifyOTPSuccess();

    void onAddWalletVerifyOTPFailure(int errorCode, String message);
}
