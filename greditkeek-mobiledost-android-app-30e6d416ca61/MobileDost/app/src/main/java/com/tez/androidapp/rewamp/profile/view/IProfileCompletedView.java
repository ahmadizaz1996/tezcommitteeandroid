package com.tez.androidapp.rewamp.profile.view;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.profile.router.CompleteProfileRouter;

public interface IProfileCompletedView extends IBaseView {

    default void roteTo(int routeKey) {
        if (routeKey == CompleteProfileRouter.ROUTE_TO_LOAN)
            routeToLoanRequiredSteps();
        else if (routeKey == CompleteProfileRouter.ROUTE_TO_INSURANCE)
            routeToInsuranceRequiredSteps();
        else
            routeToDashboard();
    }

    void routeToLoanRequiredSteps();

    void routeToInsuranceRequiredSteps();

    void routeToDashboard();
}
