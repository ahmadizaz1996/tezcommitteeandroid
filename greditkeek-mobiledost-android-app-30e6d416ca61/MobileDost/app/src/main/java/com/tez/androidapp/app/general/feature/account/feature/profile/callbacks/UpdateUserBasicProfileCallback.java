package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

public interface UpdateUserBasicProfileCallback {

    void onUpdateUserBasicProfileSuccess();
    void onUpdateUserBasicProfileFailure(int errorCode, String msg);
}
