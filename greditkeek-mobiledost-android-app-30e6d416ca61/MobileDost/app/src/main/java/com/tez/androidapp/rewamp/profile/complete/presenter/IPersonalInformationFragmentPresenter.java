package com.tez.androidapp.rewamp.profile.complete.presenter;

public interface IPersonalInformationFragmentPresenter {
    void getPersonalQuestions(String lang);

    void updatePersonalInfo(String file, String questionRefName);
}
