package com.tez.androidapp.commons.location.callbacks;

import android.location.Location;
import androidx.annotation.NonNull;

/**
 * Created by Rehman Murad Ali on 1/15/2018.
 */

public interface LocationAvailableCallback {

    default void onLocationPermissionDenied() {}

    default void onLocationRequestInitialized() { }

    default void onLocationFailed(String message) { }

    void onLocationAvailable(@NonNull Location location);
}
