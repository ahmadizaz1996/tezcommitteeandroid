package com.tez.androidapp.app.general.feature.authentication.signup.handlers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.authentication.signup.callbacks.VerifyUserReferralCodeCallback;
import com.tez.androidapp.commons.models.network.dto.response.VerifyUserReferralCodeResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by FARHAN DHANANI on 11/1/2018.
 */
public class VerifyUserReferralCodeRH extends BaseRH<VerifyUserReferralCodeResponse> {
    @NonNull
    private final VerifyUserReferralCodeCallback userReferalCodeCallback;

    public VerifyUserReferralCodeRH(BaseCloudDataStore baseCloudDataStore, @NonNull VerifyUserReferralCodeCallback userReferalCodeCallback) {
        super(baseCloudDataStore);
        this.userReferalCodeCallback = userReferalCodeCallback;
    }

    @Override
    protected void onSuccess(Result<VerifyUserReferralCodeResponse> value) {
        VerifyUserReferralCodeResponse userReferalCodeResponse = value.response().body();
        Optional.ifPresent(userReferalCodeResponse,
                referalCodeResponse -> {
                    Optional.doWhen(referalCodeResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR,
                            this.userReferalCodeCallback::userReferalCodeVerifiedSuccessfully,
                            () -> onFailure(referalCodeResponse.getStatusCode(), referalCodeResponse.getErrorDescription()));
                });
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.userReferalCodeCallback.userReferalCodeVerificationFailed(errorCode, message);
    }
}
