package com.tez.androidapp.commons.utils.cnic.firebase.util;

import android.hardware.Camera;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions.Builder;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionText.Element;
import com.google.firebase.ml.vision.text.FirebaseVisionText.Line;
import com.tez.androidapp.commons.utils.cnic.detection.view.CnicScannerActivity;
import com.tez.androidapp.commons.utils.cnic.firebase.callbacks.ImageFaceRetrieveCallback;
import com.tez.androidapp.commons.utils.cnic.firebase.callbacks.ImageTextRetrieveCallback;

import java.util.ArrayList;
import java.util.List;


public class FirebaseVisionUtil {

    private static FirebaseVisionUtil firebaseVisionUtil;
    private ImageFaceRetrieveCallback imageFaceRetrieveCallback;
    @Nullable
    private ImageTextRetrieveCallback imageTextRetrieveCallback;

    private FirebaseVisionUtil() {
    }

    public static FirebaseVisionUtil getInstance() {
        if (firebaseVisionUtil == null) {
            firebaseVisionUtil = new FirebaseVisionUtil();
        }
        return firebaseVisionUtil;
    }


    public void detectTextFromByteBuffer(ImageTextRetrieveCallback imageTextRetrieveCallback,
                                         byte[] bytes,
                                         Camera camera,
                                         String detectObject) {

        this.imageTextRetrieveCallback = imageTextRetrieveCallback;

        if (detectObject.equals(CnicScannerActivity.CNIC_BACK))
            this.detectBarCodeFromByteBuffer(bytes, camera);

        FirebaseVisionImageMetadata.Builder builder = new FirebaseVisionImageMetadata.Builder()
                .setWidth(camera.getParameters().getPreviewSize().width)
                .setHeight(camera.getParameters().getPreviewSize().height)
                .setFormat(camera.getParameters().getPreviewFormat());

        FirebaseVisionImageMetadata metadata = builder.build();
        FirebaseVisionImage image = FirebaseVisionImage.fromByteArray(bytes, metadata);
        FirebaseVision.getInstance().getOnDeviceTextRecognizer()
                .processImage(image)
                .addOnSuccessListener(this::extractTextFromImage)
                .addOnFailureListener(this::onTextRetrievalFailure);
    }

    private void detectBarCodeFromByteBuffer(byte[] bytes, Camera camera) {

        FirebaseVisionImageMetadata.Builder builder = new FirebaseVisionImageMetadata.Builder()
                .setWidth(camera.getParameters().getPreviewSize().width)
                .setHeight(camera.getParameters().getPreviewSize().height)
                .setFormat(camera.getParameters().getPreviewFormat());

        FirebaseVisionImage image = FirebaseVisionImage.fromByteArray(bytes, builder.build());

        FirebaseVisionBarcodeDetectorOptions options = new FirebaseVisionBarcodeDetectorOptions.Builder()
                .setBarcodeFormats(FirebaseVisionBarcode.FORMAT_QR_CODE)
                .build();

        FirebaseVision.getInstance()
                .getVisionBarcodeDetector(options)
                .detectInImage(image)
                .addOnSuccessListener(this::extractBarCodeData)
                .addOnFailureListener(this::onTextRetrievalFailure);
    }

    public void detectFaceFromByteBuffer(ImageFaceRetrieveCallback imageFaceRetrieveCallback,
                                         byte[] bytes,
                                         Camera camera,
                                         boolean isSensorReversed) {
        this.imageFaceRetrieveCallback = imageFaceRetrieveCallback;

        FirebaseVisionImageMetadata.Builder builder = new FirebaseVisionImageMetadata.Builder()
                .setWidth(camera.getParameters().getPreviewSize().width)
                .setHeight(camera.getParameters().getPreviewSize().height)
                .setFormat(camera.getParameters().getPreviewFormat())
                .setRotation(isSensorReversed ?
                        FirebaseVisionImageMetadata.ROTATION_90 : FirebaseVisionImageMetadata.ROTATION_270);
        FirebaseVisionImageMetadata metadata = builder.build();
        FirebaseVisionImage image = FirebaseVisionImage.fromByteArray(bytes, metadata);

        FirebaseVision.getInstance()
                .getVisionFaceDetector(
                        new Builder()
                                .setPerformanceMode(FirebaseVisionFaceDetectorOptions.ACCURATE)
                                .enableTracking()
                                .build()
                ).detectInImage(image)
                .addOnSuccessListener(this::extractFacesFromImage)
                .addOnFailureListener(imageFaceRetrieveCallback::onFaceRetrieveFailure);
    }


    private void extractBarCodeData(List<FirebaseVisionBarcode> barcodes) {
        List<String> textList = new ArrayList<>();

        for (FirebaseVisionBarcode barcode : barcodes) {
            String text = barcode.getRawValue();
            if (text != null && text.length() == 26)
                textList.add(text.substring(12, 25));
        }

        this.onTextRetrievalSuccess(textList);
    }

    private void extractTextFromImage(FirebaseVisionText firebaseVisionText) {

        ArrayList<String> blockList = new ArrayList<>();

        for (FirebaseVisionText.TextBlock block : firebaseVisionText.getTextBlocks()) {
            for (Line line : block.getLines()) {
                for (Element element : line.getElements()) {
                    blockList.add(element.getText());
                }
            }
        }

        this.onTextRetrievalSuccess(blockList);
    }


    private void extractFacesFromImage(List<FirebaseVisionFace> faces) {
        ArrayList<Integer> faceIdList = new ArrayList<>();
        for (FirebaseVisionFace face : faces) {
            FirebaseVisionFaceLandmark leftEar = face.getLandmark(3);
            if (leftEar != null) {
                leftEar.getPosition();
            }
            if (face.getSmilingProbability() != -1.0f) {
                face.getSmilingProbability();
            }
            if (face.getRightEyeOpenProbability() != -1.0f) {
                face.getRightEyeOpenProbability();
            }
            if (-1 != face.getTrackingId()) {
                faceIdList.add(face.getTrackingId());
            }
        }
        this.imageFaceRetrieveCallback.onFaceRetrieveSuccess(faceIdList);
    }

    private void onTextRetrievalFailure(@NonNull Exception e) {
        if (imageTextRetrieveCallback != null) {
            imageTextRetrieveCallback.onTextRetrieveFailure(e);
            this.imageTextRetrieveCallback = null;
        }
    }

    private void onTextRetrievalSuccess(@NonNull List<String> list) {
        if (imageTextRetrieveCallback != null) {
            imageTextRetrieveCallback.onTextRetrieveSuccess(list);
            this.imageTextRetrieveCallback = null;
        }
    }
}
