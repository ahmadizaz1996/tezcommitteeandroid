package com.tez.androidapp.rewamp.general.beneficiary.presenter;

import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetAdvanceBimaBeneficiaryCallback;

public interface IEditBeneficiaryActivityInteractorOutput extends SetAdvanceBimaBeneficiaryCallback {
}
