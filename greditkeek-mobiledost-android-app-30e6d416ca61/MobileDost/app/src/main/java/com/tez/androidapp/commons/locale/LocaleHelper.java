package com.tez.androidapp.commons.locale;

/*
 * Created by VINOD KUMAR on 5/21/2019.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;

import androidx.core.view.ViewCompat;

import java.util.Locale;

/**
 * This class is used to change your application locale and persist this change for the next time
 * that your app is going to be used.
 * <p/>
 * You can also change the locale of your application on the fly by using the updateLocale method.
 * <p/>
 */
@SuppressWarnings("unused")
public class LocaleHelper {

    public static final String ENGLISH = "en";
    public static final String URDU = "ur";
    public static final String ROMAN = "DEFAULT";

    /*
        For Back-end purpose only
     */
    public static final String BACKEND_ENGLISH = "ENGLISH";
    public static final String BACKEND_URDU = "URDU";
    public static final String BACKEND_ROMAN = "ROMAN";


    private static final String DEFAULT_LANGUAGE = ROMAN;
    private static final String SELECTED_LANGUAGE = "Locale.Helper.Selected.Language";

    public static Context onAttach(Context context) {
        return setLocale(context, getPersistedData(context));
    }

    public static String getSelectedLanguage(Context context) {
        return getPersistedData(context);
    }

    public static String getSelectedLanguageForBackend(Context context) {
        String lang = getSelectedLanguage(context);

        switch (lang) {

            case ENGLISH:
                return BACKEND_ENGLISH;

            case URDU:
                return BACKEND_URDU;

            default:
            case ROMAN:
                return BACKEND_ROMAN;
        }
    }

    public static int getLayoutDirection(Context context) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1
                ? context.getResources().getConfiguration().getLayoutDirection()
                : ViewCompat.LAYOUT_DIRECTION_LTR;
    }

    public static void updateLanguage(Context context, String lang) {
        updateLocale(context, lang);
    }

    public static void setUrduLanguage(Context context) {
        updateLocale(context, URDU);
    }

    public static void setRomanUrduLanguage(Context context) {
        updateLocale(context, ROMAN);
    }

    public static void setEnglishLanguage(Context context) {
        updateLocale(context, ENGLISH);
    }

    private static void updateLocale(Context context, String language) {

        persist(context, language);
        setLocale(context, language);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            setLocale(context.getApplicationContext(), language);
    }

    private static Context setLocale(Context context, String language) {

        return updateResourcesLegacy(context, language);
    }

    private static String getPersistedData(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return preferences.getString(SELECTED_LANGUAGE, DEFAULT_LANGUAGE);
    }

    private static void persist(Context context, String language) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(SELECTED_LANGUAGE, language);
        editor.apply();
    }

    private static Context updateResourcesLegacy(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(language.equals(ROMAN) ? Locale.ROOT : locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.fontScale =1;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            configuration.setLocale(locale);
        else
            configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        return context;
    }
}
