package com.tez.androidapp.app.base.ui.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.tez.androidapp.app.base.ui.BaseFragmentView;
import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.location.handlers.LocationHandler;

import net.tez.fragment.util.actions.Actionable1;
import net.tez.fragment.util.base.AbstractHelperFragment;
import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener;

/**
 * Created by Rehman Murad Ali on 12/12/2017.
 */

public abstract class BaseFragment extends AbstractHelperFragment implements IBaseView, BaseFragmentView {


    @Nullable
    @Override
    public BaseActivity getHostActivity() {
        return isInstanceOfActivity(BaseActivity.class);
    }

    @Override
    public void getCurrentLocation(@NonNull LocationAvailableCallback callback) {
        getBaseFragmentActivity(fragmentActivity -> {
            LocationHandler.requestCurrentLocation(fragmentActivity, callback);
        });
    }

    protected void getBaseActivity(@NonNull Actionable1<BaseActivity> activityActionable1) {
        isInstanceOfActivity(BaseActivity.class, activityActionable1);
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void finishActivity() {
        getBaseFragmentActivity(FragmentActivity::finish);
    }

    @Override
    public void showInformativeDialog(int title, int desc) {

    }

    @Override
    public void showInformativeDialog(int title, int desc, DoubleTapSafeDialogClickListener doubleTapSafeDialogClickListener) {

    }


}
