package com.tez.androidapp.rewamp.bima.products.view;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.bima.products.model.ProductPolicy;

public interface IProductPolicyActivityView extends IBaseView {

    void initPolicyDetails(@NonNull ProductPolicy productPolicy);

    void setGroupCallListener(@NonNull String number);

    void setGroupCallListener(@NonNull String[] numbers);

    void setGroupEmailListener(@NonNull String email);

    void hideTllEmail();

    void hideWhatsapp();

    void setGroupWhatsappListener(@NonNull String number);

    void setInsuranceProviderLogo(@DrawableRes int logoRes);

    void setClContentVisibility(int visibility);
}
