package com.tez.androidapp.rewamp.advance.request.view;

import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.advance.request.presenter.AdvanceTermsAndConditionActivityPresenter;
import com.tez.androidapp.rewamp.advance.request.presenter.IAdvanceTermsAndConditionActivityPresenter;
import com.tez.androidapp.rewamp.advance.request.request.LoanApplyRequest;
import com.tez.androidapp.rewamp.advance.request.router.AdvanceTermsAndConditionActivityRouter;
import com.tez.androidapp.rewamp.advance.request.router.FreeBimaOnBoardingActivityRouter;
import com.tez.androidapp.rewamp.advance.request.router.LoanRequiredStepsActivityRouter;
import com.tez.androidapp.rewamp.advance.request.router.WaitForDisbursementActivityRouter;
import com.tez.androidapp.rewamp.common.BaseTermsAndConditionActivity;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.profile.trust.router.CompleteYourProfileActivityRouter;
import com.tez.androidapp.rewamp.signup.TermsAndCondition;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rehman Murad Ali
 **/
public class AdvanceTermsAndConditionActivity extends BaseTermsAndConditionActivity implements IAdvanceTermsAndConditionActivityView {

    private final IAdvanceTermsAndConditionActivityPresenter iAdvanceTermsAndConditionActivityPresenter;

    public AdvanceTermsAndConditionActivity() {
        iAdvanceTermsAndConditionActivityPresenter = new AdvanceTermsAndConditionActivityPresenter(this);
    }

    @Override
    protected List<TermsAndCondition> getTermAndConditions() {
        return new ArrayList<TermsAndCondition>() {
            {
                add(new TermsAndCondition(getString(R.string.tc_advance_1)));
                add(new TermsAndCondition(getString(R.string.tc_advance_2, String.valueOf(getLatePaymentCharges()).concat("%"))));
                add(new TermsAndCondition(getString(R.string.tc_advance_3)));
                add(new TermsAndCondition(getString(R.string.tc_advance_4)));
                add(new TermsAndCondition(getString(R.string.tc_advance_5)));
            }
        };
    }

    @Override
    public void routeToWaitForDisbursementActivity() {
        WaitForDisbursementActivityRouter.createInstance().setDependenciesAndRoute(this);
        finish();
    }

    @Override
    public void routeToCompleteYourProfileActivity() {
        CompleteYourProfileActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void routeToDashboard() {
        DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
    }

    @Override
    public void routeToLoanRequiredStepsActivity(List<String> steps) {
        setTezLoaderToBeDissmissedOnTransition();
        LoanRequiredStepsActivityRouter.createInstance().setDependenciesAndRoute(this, steps);
    }

    @Override
    public void routeToAddBeneficiaryActivity() {
        FreeBimaOnBoardingActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public LoanApplyRequest getLoanApplyRequest() {
        return getIntent().getParcelableExtra(AdvanceTermsAndConditionActivityRouter.LOAN_APPLY_REQUEST_PARCELABLE);
    }

    private double getLatePaymentCharges() {
        LoanApplyRequest loanApplyRequest = getLoanApplyRequest();
        return loanApplyRequest != null ? loanApplyRequest.getLatePaymentCharges() : 0;
    }

    @Override
    public String getFormattedErrorMessage(@StringRes int message) {
        LoanApplyRequest loanApplyRequest = getLoanApplyRequest();
        int serviceProviderId = loanApplyRequest != null && loanApplyRequest.getWalletServiceProviderId() != null ? loanApplyRequest.getWalletServiceProviderId() : -1;
        String messageString = getString(message);
        messageString = messageString.replaceAll(ResponseStatusCode.PlaceHolders.WALLET_NAME.getValue(), Utility.getWalletName(serviceProviderId));
        return messageString.replaceAll(ResponseStatusCode.PlaceHolders.WALLET_SUPPORT_NUMBER.getValue(), Utility.getWalletProviderNumber(serviceProviderId));
    }

    @Override
    protected void onClickBtMainButton() {
        iAdvanceTermsAndConditionActivityPresenter.checkRemainingSteps();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    protected String getScreenName() {
        return "AdvanceTermsAndConditionActivity";
    }
}