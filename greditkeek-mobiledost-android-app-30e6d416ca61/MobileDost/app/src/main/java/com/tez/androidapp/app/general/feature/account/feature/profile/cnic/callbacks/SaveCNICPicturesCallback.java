package com.tez.androidapp.app.general.feature.account.feature.profile.cnic.callbacks;

import com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network.dto.response.SaveCNICPicturesResponse;

/**
 * Created by VINOD KUMAR on 8/28/2018.
 */
public interface SaveCNICPicturesCallback {

    void onSaveCNICPicturesSuccess(SaveCNICPicturesResponse saveCNICPicturesResponse);

    void onSaveCNICPicturesFailure(int errorCode, String message);
}
