package com.tez.androidapp.commons.models.network.dto.response;

import com.tez.androidapp.commons.models.network.AccessToken;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.utils.app.Constants;

import java.io.Serializable;
import java.util.List;

/**
 * Created  on 2/15/2017.
 */

public class UserLoginResponse extends UserVerifyResponse implements Serializable {

    public static final String MOBILE_ACCOUNT = "ADD_MOBILE_ACCOUNT";
    public static final String CNIC = "UPLOAD_CNIC";
    private String userStatus;
    private String principalName;
    private String referralCode;
    private Integer isNewDevice;
    private Integer isMobileNumberVerified;
    private List<String> requiredSteps;
    private AccessToken token;
    private User userDetails;

    public Integer getIsNewDevice() {
        return isNewDevice;
    }

    public void setIsNewDevice(Integer isNewDevice) {
        this.isNewDevice = isNewDevice;
    }

    public List<String> getRequiredSteps() {
        return requiredSteps;
    }

    public void setRequiredSteps(List<String> requiredSteps) {
        this.requiredSteps = requiredSteps;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public boolean isCNICUploaded() {
        return requiredSteps != null && !requiredSteps.contains(CNIC);
    }

    public boolean isMobileAccountAdded() {
        return !requiredSteps.contains(MOBILE_ACCOUNT);
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public Integer getIsMobileNumberVerified() {
        return isMobileNumberVerified;
    }

    public void setIsMobileNumberVerified(Integer isMobileNumberVerified) {
        this.isMobileNumberVerified = isMobileNumberVerified;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public AccessToken getToken() {
        return token;
    }

    public void setToken(AccessToken token) {
        this.token = token;
    }

    public User getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(User userDetails) {
        this.userDetails = userDetails;
    }

    public boolean isUserStatusReady() {
        return getUserStatus().equalsIgnoreCase(Constants.USER_STATUS_READY);
    }

    public boolean isUserStatusPending() {
        return getUserStatus().equalsIgnoreCase(Constants.USER_STATUS_PENDING);
    }

    public boolean isUserActivationPending(){
        return getUserStatus().equalsIgnoreCase(Constants.USER_STATUS_ACTIVATION_PENDING);
    }

    @Override
    public String toString() {
        return "UserLoginResponse{" +
                "userStatus='" + userStatus + '\'' +
                ", principalName='" + principalName + '\'' +
                ", referralCode='" + referralCode + '\'' +
                ", isNewDevice=" + isNewDevice +
                ", isMobileNumberVerified=" + isMobileNumberVerified +
                ", requiredSteps=" + requiredSteps +
                ", token=" + token +
                ", userDetails=" + userDetails +
                "} " + super.toString();
    }
}
