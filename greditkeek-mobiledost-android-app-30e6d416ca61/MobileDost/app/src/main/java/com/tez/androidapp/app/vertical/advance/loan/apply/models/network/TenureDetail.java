package com.tez.androidapp.app.vertical.advance.loan.apply.models.network;

import java.io.Serializable;

public class TenureDetail implements Serializable {
    private Integer id;
    private String title;
    private Integer duration;
    private Integer minDisbursementAmount;
    private Integer maxDisbursementAmount;
    private Integer currentDuration=7;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDuration() {
        return duration;
    }

    public void incrementCurrentDuration(){
        if(canIncrementDurationMore()){
            currentDuration+=7;
        }
    }

    public boolean canIncrementDurationMore(){
        return currentDuration<duration;
    }

    public void decrementCurrentDuration(){
        if(canDecrementDurationMore()){
            currentDuration-=7;
        }
    }

    public boolean canDecrementDurationMore(){
        return currentDuration>7;
    }

    public String getCurrentTitle(){
        return currentDuration == 7? currentDuration/7 + " week": currentDuration/7 + " weeks";
    }

    public int getCurrentDuration(){
        return this.currentDuration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getMinDisbursementAmount() {
        return minDisbursementAmount;
    }

    public void setMinDisbursementAmount(Integer minDisbursementAmount) {
        this.minDisbursementAmount = minDisbursementAmount;
    }

    public Integer getMaxDisbursementAmount() {
        return maxDisbursementAmount;
    }

    public void setMaxDisbursementAmount(Integer maxDisbursementAmount) {
        this.maxDisbursementAmount = maxDisbursementAmount;
    }
}
