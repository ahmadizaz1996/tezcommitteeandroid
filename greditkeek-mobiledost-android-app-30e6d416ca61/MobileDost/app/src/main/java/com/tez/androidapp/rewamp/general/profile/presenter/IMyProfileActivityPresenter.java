package com.tez.androidapp.rewamp.general.profile.presenter;

import com.tez.androidapp.commons.models.network.UserAddress;

import java.util.List;

public interface IMyProfileActivityPresenter {
    void getProfileDetails();

    void linkUserSocialAccount(String socialId, Integer socialType);

    void setUserAddresses(List<UserAddress> listUserAddresses);
}
