package com.tez.androidapp.rewamp.bima.products.view;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.constraintlayout.widget.Group;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezFrameLayout;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.commons.widgets.TezToolbar;
import com.tez.androidapp.rewamp.AppOpener;
import com.tez.androidapp.rewamp.bima.products.dialog.SelectNumberDialog;
import com.tez.androidapp.rewamp.bima.products.model.ProductPolicy;
import com.tez.androidapp.rewamp.bima.products.presenter.IProductPolicyActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.presenter.ProductPolicyActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.router.ProductPolicyActivityRouter;
import com.tez.androidapp.rewamp.bima.products.router.ProductTermConditionsActivityRouter;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.fragment.util.optional.Optional;
import net.tez.fragment.util.optional.TextUtil;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class ProductPolicyActivity extends BaseActivity implements IProductPolicyActivityView {

    private final IProductPolicyActivityPresenter iProductPolicyActivityPresenter;

    @BindView(R.id.tllContent)
    private View tllContent;

    @BindView(R.id.tvPolicyNumber)
    private TezTextView tvPolicyNumber;

    @BindView(R.id.tvIssueDate)
    private TezTextView tvIssueDate;

    @BindView(R.id.tvExpiryDate)
    private TezTextView tvExpiryDate;

    @BindView(R.id.tvActivationDate)
    private TezTextView tvActivationDate;

    @BindView(R.id.tvDailyCoverage)
    private TezTextView tvDailyCoverage;

    @BindView(R.id.tvPolicyStatus)
    private TezTextView tvPolicyStatus;

    @BindView(R.id.ivInsuranceIcon)
    private TezImageView ivInsuranceIcon;

    @BindView(R.id.tvClaimTitle)
    private TezTextView tvClaimTitle;

    @BindView(R.id.tvClaimDescription)
    private TezTextView tvClaimDescription;

    @BindView(R.id.tvTimingsValue)
    private TezTextView tvTimingsValue;

    @BindView(R.id.groupActivationDate)
    private Group groupActivationDate;

    @BindView(R.id.flPolicyStatus)
    private TezFrameLayout flPolicyStatus;

    @BindView(R.id.tvDailyCoverageText)
    private TezTextView tvDailyCoverageText;

    @BindView(R.id.tvTermsAndCondition)
    private TezTextView tvTermsAndCondition;

    @BindView(R.id.shimmer)
    private View shimmer;

    @BindView(R.id.groupEmail)
    private Group groupEmail;

    @BindView(R.id.groupCall)
    private Group groupCall;

    @BindView(R.id.groupWhatsapp)
    private Group groupWhatsapp;

    @BindView(R.id.toolbar)
    private TezToolbar toolbar;

    private ProductPolicyActivityRouter.Dependencies dependencies;

    public ProductPolicyActivity() {
        iProductPolicyActivityPresenter = new ProductPolicyActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_policy);
        ViewBinder.bind(this);
        init();
    }

    private void init() {
        initDependencies(ProductPolicyActivityRouter.getDependencies(getIntent()));
    }

    private void initDependencies(ProductPolicyActivityRouter.Dependencies dependencies) {
        this.dependencies = dependencies;
        int productId = dependencies.getProductId();
        initClickListener(productId);
        toolbar.setToolbarTitle(dependencies.getPlanName());
        tvClaimTitle.setText(getClaimTitle(productId));
        tvClaimDescription.setText(getClaimDescription(productId));
        tvDailyCoverageText.setText(productId == Constants.DIGITAL_OPD_PLAN_ID ? R.string.unlimited_consultation : R.string.daily_coverage);
        iProductPolicyActivityPresenter.getProductPolicy(dependencies.getPolicyId());
    }

    private void initClickListener(int productId) {
        tvTermsAndCondition.setDoubleTapSafeOnClickListener(v -> ProductTermConditionsActivityRouter
                .createInstance()
                .setDependenciesAndRoute(this, productId));
    }

    @Override
    public void initPolicyDetails(@NonNull ProductPolicy productPolicy) {
        tvPolicyNumber.setText(productPolicy.getPolicyNumber());
        tvExpiryDate.setText(productPolicy.getExpiryDate());
        tvIssueDate.setText(productPolicy.getIssueDate());
        tvActivationDate.setText(productPolicy.getActivationDate());
        Optional.ifPresent(productPolicy.getDailyCoverage(), dailyCoverage -> {
            tvDailyCoverage.setText(getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(dailyCoverage)));
        });
        Optional.ifPresent(productPolicy.getInsuranceStatus(), this::setFlPolicyStatusBackground);
        tvPolicyStatus.setText(productPolicy.getInsuranceStatus());
        tvTimingsValue.setText(productPolicy.getContact().getContactTimings());
        flPolicyStatus.setVisibility(productPolicy.getInsuranceStatus() == null ? View.GONE : View.VISIBLE);
        groupActivationDate.setVisibility(productPolicy.getActivationDate() == null ? View.GONE : View.VISIBLE);
        tvDailyCoverage.setVisibility(productPolicy.getDailyCoverage() == null ? View.GONE : View.VISIBLE);
        tvDailyCoverageText.setVisibility(productPolicy.getDailyCoverage() == null
                && dependencies.getProductId() != Constants.DIGITAL_OPD_PLAN_ID
                ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setGroupCallListener(@NonNull final String number) {
        this.setGroupOnClickListener(groupCall, v -> AppOpener.openDialer(this, number));
    }

    @Override
    public void setGroupCallListener(@NonNull final String[] numbers) {
        this.setGroupOnClickListener(groupCall, v -> {
            SelectNumberDialog dialog = new SelectNumberDialog(this, this);
            dialog.setNumbersArray(numbers);
            dialog.show();
        });
    }

    @Override
    public void setGroupEmailListener(@NonNull final String email) {
        this.setGroupOnClickListener(groupEmail, v -> AppOpener.openEmail(this, email));
    }

    @Override
    public void hideTllEmail() {
        groupEmail.setVisibility(View.GONE);
    }

    @Override
    public void hideWhatsapp() {
        groupWhatsapp.setVisibility(View.GONE);
    }

    @Override
    public void setGroupWhatsappListener(@NonNull final String number) {
        this.setGroupOnClickListener(groupWhatsapp, v -> AppOpener.openWhatsapp(this, number));
    }

    @Override
    public void setInsuranceProviderLogo(@DrawableRes int logoRes) {
        ivInsuranceIcon.setImageResource(logoRes);
    }

    private void setFlPolicyStatusBackground(@NonNull String insuranceStatus) {
        int bgColor = Utility.getColorFromResource(TextUtil.equals(insuranceStatus, "Expired", true) ? R.color.editTextBottomLineColorGrey : R.color.textViewTextColorGreen);
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                new int[]{bgColor, bgColor});
        gd.setCornerRadius(Utility.dpToPx(this, 4));
        flPolicyStatus.setBackground(gd);
    }

    @StringRes
    private int getClaimTitle(int productId) {
        switch (productId) {
            case Constants.HOSPITAL_COVERAGE_PLAN_ID:
                return R.string.claim_hospitalization_expense;

            case Constants.DIGITAL_OPD_PLAN_ID:
            default:
                return R.string.consult_with_a_doctor;

            case Constants.CORONA_DEFENSE_PLAN_ID:
                return R.string.to_lodge_claim_call_tpl;
        }
    }

    @StringRes
    private int getClaimDescription(int productId) {
        switch (productId) {
            case Constants.HOSPITAL_COVERAGE_PLAN_ID:
                return R.string.touch_with_us_via;

            case Constants.DIGITAL_OPD_PLAN_ID:
            default:
                return R.string.get_in_touch_with_pmc;

            case Constants.CORONA_DEFENSE_PLAN_ID:
                return R.string.touch_with_us_via_call_whatsApp;
        }
    }

    private void setGroupOnClickListener(@NonNull Group group, @Nullable DoubleTapSafeOnClickListener listener) {
        int[] ids = group.getReferencedIds();
        for (int id : ids)
            ((IBaseWidget) findViewById(id)).setDoubleTapSafeOnClickListener(listener);
    }

    @Override
    public void setClContentVisibility(int visibility) {
        tllContent.setVisibility(visibility);
    }

    @Override
    public void showTezLoader() {
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissTezLoader() {
        shimmer.setVisibility(View.GONE);
    }

    @Override
    protected String getScreenName() {
        return "ProductPolicyDetailsActivity";
    }
}
