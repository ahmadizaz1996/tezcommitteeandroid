package com.tez.androidapp.app.general.feature.account.feature.beneficiary.callbacks;

import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.BeneficiaryDetails;

/**
 * Created  on 8/25/2017.
 */

public interface GetAdvanceBimaBeneficiaryCallback {

    void onGetAdvanceBimaBeneficiarySuccess(BeneficiaryDetails beneficiaryDetails);

    void onGetAdvanceBimaBeneficiaryFailure(int errorCode, String message);
}
