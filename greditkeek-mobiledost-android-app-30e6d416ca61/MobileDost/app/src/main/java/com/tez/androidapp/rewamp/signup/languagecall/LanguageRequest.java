package com.tez.androidapp.rewamp.signup.languagecall;

import com.tez.androidapp.app.base.request.BaseRequest;

import java.io.Serializable;

/**
 * Created by VINOD KUMAR on 8/9/2019.
 */
public class LanguageRequest extends BaseRequest implements Serializable {

    public static final String METHOD_NAME = "v1/user/language/{" + Params.LANGUAGE_CODE + "}";

    public static final class Params {

        public static final String LANGUAGE_CODE = "languageCode";

        private Params() {
        }
    }

}
