package com.tez.androidapp.rewamp.dashboard;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.NavigationActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.general.beneficiary.router.ManageBeneficiaryActivityRouter;
import com.tez.androidapp.rewamp.general.changepin.ChangePinActivityRouter;
import com.tez.androidapp.rewamp.general.faquestion.FAQActivity;
import com.tez.androidapp.rewamp.general.invite.friend.view.InviteFriendActivity;
import com.tez.androidapp.rewamp.general.suspend.account.router.SuspendAccountActivityRouter;
import com.tez.androidapp.rewamp.general.termsandcond.NavigationTermsAndConditionsActivityRouter;
import com.tez.androidapp.rewamp.signup.router.ChangeLanguageActivityRouter;
import com.tez.androidapp.rewamp.signup.router.ContactUsActivityRouter;

import net.tez.fragment.util.effect.TouchEffectListener;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by VINOD KUMAR on 6/19/2019.
 */
public class NavigationExpandableListAdapter extends BaseExpandableListAdapter {

    @NonNull
    private final NavigationActivity mHostActivity;

    @NonNull
    private final Map<MenuItem, List<MenuItem>> menuItems;

    public NavigationExpandableListAdapter(@NonNull NavigationActivity mHostActivity) {
        this.mHostActivity = mHostActivity;
        this.menuItems = new LinkedHashMap<>();
        this.initMenuList();
    }

    @Override
    public int getGroupCount() {
        return menuItems.keySet().size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        MenuItem groupMenu = getGroupMenu(groupPosition);
        List<MenuItem> childItems = menuItems.get(groupMenu);
        return childItems == null ? 0 : childItems.size();
    }

    @NonNull
    private MenuItem getGroupMenu(int groupPosition) {
        for (MenuItem item : menuItems.keySet()) {
            if (item.getIndex() == groupPosition)
                return item;
        }
        return new MenuItem(0, 0, 0);
    }

    @Override
    public Object getGroup(int groupPosition) {
        return getGroupMenu(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        MenuItem groupMenu = getGroupMenu(groupPosition);
        List<MenuItem> childItems = menuItems.get(groupMenu);
        return childItems == null ? null : childItems.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        final View rowView = getConvertView(R.layout.navigation_list_item_view, convertView, parent);

        int childCount = getChildrenCount(groupPosition);

        MenuItem groupMenu = getGroupMenu(groupPosition);

        if (childCount > 0)
            rowView.setOnClickListener(setExpandGroupOnClickListener((ExpandableListView) parent, groupMenu));
        else
            rowView.setOnClickListener(groupMenu.getOnClickListener());

        TezImageView ivItemIcon = rowView.findViewById(R.id.ivItemIcon);
        TezTextView tvItemName = rowView.findViewById(R.id.tvItemName);

        tvItemName.setText(groupMenu.getName());
        ivItemIcon.setImageResource(groupMenu.getImage());


        TezImageView ivDropDown = rowView.findViewById(R.id.ivDropDown);
        ivDropDown.setVisibility(childCount > 0 ? View.VISIBLE : View.GONE);

        if (isExpanded && childCount > 0)
            setMarginBottom(ivItemIcon, 8);

        else
            setMarginBottom(ivItemIcon, 16);

        return rowView;
    }

    private View.OnClickListener setExpandGroupOnClickListener(ExpandableListView parent, MenuItem item) {
        return view -> {
            if (parent.isGroupExpanded(item.getIndex()))
                parent.collapseGroup(item.getIndex());
            else
                parent.expandGroup(item.getIndex(), true);

            TezImageView ivDropDown = view.findViewById(R.id.ivDropDown);
            ivDropDown.setRotation(ivDropDown.getRotation() == 180 ? 0 : 180);

            if (item.getOnClickListener() != null)
                item.getOnClickListener().doubleTapSafeOnClick(view);
        };
    }

    private View getConvertView(@LayoutRes int layoutId, @Nullable View view, ViewGroup parent) {
        View rootView = view != null
                ? view
                : LayoutInflater
                .from(mHostActivity)
                .inflate(layoutId, parent, false);

        rootView.setOnTouchListener(new TouchEffectOnNavigationMenuListener());

        return rootView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final View rowView = getConvertView(R.layout.navigation_list_item_child_view, convertView, parent);

        MenuItem childMenu = (MenuItem) getChild(groupPosition, childPosition);

        rowView.setOnClickListener(childMenu.getOnClickListener());
        TezImageView ivItemIcon = rowView.findViewById(R.id.ivItemIcon);
        TezTextView tvItemName = rowView.findViewById(R.id.tvItemName);

        ivItemIcon.setImageResource(childMenu.getImage());
        tvItemName.setText(childMenu.getName());

        return rowView;
    }

    private void setMarginBottom(TezImageView view, int marginDp) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof ConstraintLayout.LayoutParams) {
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) layoutParams;
            params.bottomMargin = view.dpToPx(mHostActivity, marginDp);
            view.setLayoutParams(params);
        }
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private void initMenuList() {

        List<MenuItem> emptyList = new ArrayList<>();

        MenuItem home = new MenuItem(R.drawable.ic_menu_home, R.string.home, 0, v -> mHostActivity.closeDrawer());
        MenuItem setting = new MenuItem(R.drawable.ic_menu_settings, R.string.settings, 1);
        MenuItem faqs = new MenuItem(R.drawable.ic_menu_faq, R.string.faqs, 2, start(FAQActivity.class));
        MenuItem manageBeneficiary = new MenuItem(R.drawable.ic_menu_beneficiary, R.string.manage_beneficiary, 3, view -> ManageBeneficiaryActivityRouter.createInstance().setDependenciesAndRoute(mHostActivity));
        MenuItem inviteFriend = new MenuItem(R.drawable.ic_menu_invite, R.string.invite_friend, 4, start(InviteFriendActivity.class));
        MenuItem contactUs = new MenuItem(R.drawable.ic_menu_contact, R.string.contact_us, 5, view -> ContactUsActivityRouter.createInstance().setDependenciesAndRoute(mHostActivity));

        menuItems.put(home, emptyList);
        menuItems.put(setting, getSettingsSubMenu());
        menuItems.put(faqs, emptyList);
        menuItems.put(manageBeneficiary, emptyList);
        menuItems.put(inviteFriend, emptyList);
        menuItems.put(contactUs, emptyList);
    }

    private List<MenuItem> getSettingsSubMenu() {
        List<MenuItem> subMenuList = new ArrayList<>();
        subMenuList.add(new MenuItem(R.drawable.ic_menu_change_pin, R.string.change_pin, 0, view -> ChangePinActivityRouter.createInstance().setDependenciesAndRoute(mHostActivity)));
        subMenuList.add(new MenuItem(R.drawable.ic_menu_change_language2, R.string.change_language, 1, view -> ChangeLanguageActivityRouter.createInstance().setDependenciesAndRoute(mHostActivity)));
        subMenuList.add(new MenuItem(R.drawable.ic_menu_terms_conditions, R.string.terms_and_conditions, 2, view -> NavigationTermsAndConditionsActivityRouter.createInstance().setDependenciesAndRoute(mHostActivity)));
        subMenuList.add(new MenuItem(R.drawable.ic_menu_suspent_account, R.string.suspend_account, 3, view -> SuspendAccountActivityRouter.createInstance().setDependenciesAndRoute(mHostActivity)));
        return subMenuList;
    }

    private DoubleTapSafeOnClickListener start(@NonNull Class<?> activity) {
        return view -> mHostActivity.startActivity(new Intent(mHostActivity, activity));
    }

    private class TouchEffectOnNavigationMenuListener extends TouchEffectListener {

        @Override
        public void onTouchEffect(View view) {
            view.setBackgroundColor(Utility.getColorFromResource(R.color.textViewTitleColorBlue));
            int screenBgColor = Utility.getColorFromResource(R.color.screen_background);
            this.setItemNameColor(view, screenBgColor);
            this.setItemIconColor(view, screenBgColor);
            this.setIvDropDownColor(view, screenBgColor);
        }

        @Override
        public void onNormalEffect(View view) {
            view.setBackgroundColor(Utility.getColorFromResource(R.color.screen_background));
            int etGreyColor = Utility.getColorFromResource(R.color.editTextTextColorGrey);
            this.setItemNameColor(view, etGreyColor);
            this.setItemIconColor(view, etGreyColor);
            this.setIvDropDownColor(view, etGreyColor);
        }

        private void setItemNameColor(View view, int colorId) {
            TezTextView tvItemName = view.findViewById(R.id.tvItemName);
            tvItemName.setTextColor(colorId);
        }

        private void setItemIconColor(View view, int colorId) {
            TezImageView ivItemIcon = view.findViewById(R.id.ivItemIcon);
            ivItemIcon.setColorFilter(colorId);
        }

        private void setIvDropDownColor(View view, int colorId) {
            TezImageView ivDropDown = view.findViewById(R.id.ivDropDown);
            if (ivDropDown != null)
                ivDropDown.setColorFilter(colorId);
        }
    }
}
