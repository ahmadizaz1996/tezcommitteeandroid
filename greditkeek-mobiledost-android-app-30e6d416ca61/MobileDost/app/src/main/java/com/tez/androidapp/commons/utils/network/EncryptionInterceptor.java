package com.tez.androidapp.commons.utils.network;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.commons.utils.app.AESEncryptionUtil;
import com.tez.androidapp.commons.utils.app.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import androidx.annotation.NonNull;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * Created by Rehman Murad Ali on 3/7/2018.
 */

public class EncryptionInterceptor implements Interceptor {

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        Headers headers = request.headers();
        RequestBody oldBody = request.body();
        request = request.newBuilder().headers(headers).header("X-Security-Control", "Y").method(request.method(), oldBody).build();
        if (oldBody != null && oldBody.contentType() != null && !oldBody.contentType().type().contains("multipart")) {
            Buffer buffer = new Buffer();
            oldBody.writeTo(buffer);
            String requestString = buffer.readUtf8();
            Log.d("R&R", requestString.toString());
            MediaType mediaType = MediaType.parse(String.valueOf(oldBody.contentType()));
            String encryptedRequestString = AESEncryptionUtil.getInstance().encrypt(requestString);
            JSONObject jsonObjectRequest = new JSONObject();
            try {
                jsonObjectRequest.put("value", encryptedRequestString);
            } catch (JSONException e) {
                Crashlytics.logException(new Exception("EncryptionInterceptorRequest: " + request.url().toString() + e.getMessage()));
            }
            RequestBody body = RequestBody.create(mediaType, jsonObjectRequest.toString());
            request = request.newBuilder().headers(request.headers()).method(request.method(), body).build();
        }
        Response response = chain.proceed(request);


        String encryptedResponse = response.body().string();
        MediaType contentType = response.body().contentType();
        String encryptedResponseString = "";
        String decryptedResponseString = encryptedResponse;
        try {
            JSONObject jsonObjectResponse = new JSONObject(encryptedResponse);
            encryptedResponseString = jsonObjectResponse.getString("value");
            decryptedResponseString = AESEncryptionUtil.getInstance().decrypt(encryptedResponseString);
            Log.d("R&R", decryptedResponseString.toString());
        } catch (JSONException e) {
            Crashlytics.logException(new Exception("EncryptionInterceptorResponse: " + request.url().toString() + e.getMessage()));
        }

        ResponseBody body = ResponseBody.create(contentType, decryptedResponseString);
        return response.newBuilder().body(body).build();
    }
}
