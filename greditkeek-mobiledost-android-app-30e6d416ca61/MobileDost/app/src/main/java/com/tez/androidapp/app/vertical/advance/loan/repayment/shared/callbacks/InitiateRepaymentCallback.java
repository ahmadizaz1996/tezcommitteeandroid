package com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.InitiateRepaymentResponse;

/**
 * Created  on 7/20/2017.
 */

public interface InitiateRepaymentCallback {

    void onInitiateRepaymentSuccess(InitiateRepaymentResponse initiateRepaymentResponse);

    void onInitiateRepaymentFailure(int errorCode, String message);
}
