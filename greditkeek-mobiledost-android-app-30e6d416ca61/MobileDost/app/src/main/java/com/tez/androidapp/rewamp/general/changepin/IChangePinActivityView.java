package com.tez.androidapp.rewamp.general.changepin;

import com.tez.androidapp.app.base.ui.IBaseView;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public interface IChangePinActivityView extends IBaseView {

    void onPinChangedSuccessfully();

    void setBtChangePinEnabled(boolean enabled);

    void clearPin();
}
