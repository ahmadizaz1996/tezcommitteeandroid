package com.tez.androidapp.rewamp.committee.view;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeWalletResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeFilterActivityView extends IBaseView {
    void showLoader();

    void hideLoader();

    void onFilter(CommitteeFilterResponse committeeFilterResponse);
}
