package com.tez.androidapp.rewamp.general.wallet.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.general.wallet.adapter.ChangeWalletAdapter;
import com.tez.androidapp.rewamp.general.wallet.presenter.ChangeWalletActivityPresenter;
import com.tez.androidapp.rewamp.general.wallet.presenter.IChangeWalletActivityPresenter;
import com.tez.androidapp.rewamp.general.wallet.router.ChangeWalletActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class ChangeWalletActivity extends BaseActivity implements IChangeWalletActivityView, ChangeWalletAdapter.ChangeWalletListener {

    private final IChangeWalletActivityPresenter iChangeWalletActivityPresenter;

    @BindView(R.id.rvWallets)
    private RecyclerView rvWallets;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;


    public ChangeWalletActivity() {
        this.iChangeWalletActivityPresenter = new ChangeWalletActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_wallet);
        ViewBinder.bind(this);
        iChangeWalletActivityPresenter.getAllWallets();
    }

    @Override
    public void initAdapter(@NonNull List<Wallet> walletList) {
        ChangeWalletAdapter adapter = new ChangeWalletAdapter(walletList, this);
        rvWallets.setLayoutManager(new GridLayoutManager(this, 2));
        rvWallets.setAdapter(adapter);
    }

    @Override
    public void setClContentVisibility(int visibility) {
        this.clContent.setVisibility(visibility);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void showShimmer() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideShimmer() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.GONE);
    }

    @Override
    public void onClickWallet(@NonNull Wallet wallet) {
        Intent intent = new Intent();
        intent.putExtra(ChangeWalletActivityRouter.RESULT_DATA_WALLET, (Parcelable) wallet);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected String getScreenName() {
        return "ChangeWalletActivity";
    }
}
