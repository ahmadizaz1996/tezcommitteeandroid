package com.tez.androidapp.rewamp.committee.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.tez.androidapp.app.base.response.BaseResponse;

import java.io.Serializable;
import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ahmad Izaz on 21-Nov-20
 **/
public class MyCommitteeResponse extends BaseResponse implements Parcelable {

    //    public Committee committee;
    public List<Committee> committeeList = new ArrayList<>();

    public MyCommitteeResponse() {

    }


    protected MyCommitteeResponse(Parcel in) {
        in.readList(committeeList, getClass().getClassLoader());
    }

    public static final Creator<MyCommitteeResponse> CREATOR = new Creator<MyCommitteeResponse>() {
        @Override
        public MyCommitteeResponse createFromParcel(Parcel in) {
            return new MyCommitteeResponse(in);
        }

        @Override
        public MyCommitteeResponse[] newArray(int size) {
            return new MyCommitteeResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(committeeList);
    }

    public static class Committee implements Parcelable {

        private int id;
        private String name;
        private int tenor;
        private int frequency;
        private int amount;
        private int installment;
        private String startDate;
        private String dueDate;
        private String receivingDate;
        private long userId;
        private List<Invite> invites = new ArrayList<>();
        private List<Member> members = new ArrayList<>();


        public Committee() {

        }


        protected Committee(Parcel in) {
            id = in.readInt();
            name = in.readString();
            tenor = in.readInt();
            frequency = in.readInt();
            amount = in.readInt();
            startDate = in.readString();
            dueDate = in.readString();
            receivingDate = in.readString();
            userId = in.readLong();
            installment = in.readInt();
            in.readList(invites, getClass().getClassLoader());
            in.readList(members, getClass().getClassLoader());
        }

        public static final Creator<Committee> CREATOR = new Creator<Committee>() {
            @Override
            public Committee createFromParcel(Parcel in) {
                return new Committee(in);
            }

            @Override
            public Committee[] newArray(int size) {
                return new Committee[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(id);
            parcel.writeString(name);
            parcel.writeInt(tenor);
            parcel.writeInt(frequency);
            parcel.writeInt(amount);
            parcel.writeString(startDate);
            parcel.writeString(dueDate);
            parcel.writeString(receivingDate);
            parcel.writeLong(userId);
            parcel.writeInt(installment);
            parcel.writeList(invites);
            parcel.writeList(members);
        }

        public long getUserId() {
            return userId;
        }

        public void setUserId(long userId) {
            this.userId = userId;
        }

        public static class Invite implements Parcelable {
            public String email;
            public String mobilNumber;
            public String status;

            public Invite() {

            }

            protected Invite(Parcel in) {
                email = in.readString();
                mobilNumber = in.readString();
                status = in.readString();
            }

            public static final Creator<Invite> CREATOR = new Creator<Invite>() {
                @Override
                public Invite createFromParcel(Parcel in) {
                    return new Invite(in);
                }

                @Override
                public Invite[] newArray(int size) {
                    return new Invite[size];
                }
            };

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getMobilNumber() {
                return mobilNumber;
            }

            public void setMobilNumber(String mobilNumber) {
                this.mobilNumber = mobilNumber;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeString(email);
                parcel.writeString(mobilNumber);
                parcel.writeString(status);
            }
        }

        public static class Member implements Parcelable {
            public int userId;
            public String name;
            public String mobilNumber;
            public String email;
            public String joingDate;
            public int round;


            protected Member(Parcel in) {
                userId = in.readInt();
                name = in.readString();
                mobilNumber = in.readString();
                email = in.readString();
                joingDate = in.readString();
                round = in.readInt();
            }

            public static final Creator<Member> CREATOR = new Creator<Member>() {
                @Override
                public Member createFromParcel(Parcel in) {
                    return new Member(in);
                }

                @Override
                public Member[] newArray(int size) {
                    return new Member[size];
                }
            };

            public void setRound(int round) {
                this.round = round;
            }

            public int getRound() {
                return round;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMobilNumber() {
                return mobilNumber;
            }

            public void setMobilNumber(String mobilNumber) {
                this.mobilNumber = mobilNumber;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getJoingDate() {
                return joingDate;
            }

            public void setJoingDate(String joingDate) {
                this.joingDate = joingDate;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(userId);
                parcel.writeString(name);
                parcel.writeString(mobilNumber);
                parcel.writeString(email);
                parcel.writeString(joingDate);
                parcel.writeInt(round);
            }
        }


        public int getInstallment() {
            return installment;
        }

        public void setInstallment(int installment) {
            this.installment = installment;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getTenor() {
            return tenor;
        }

        public void setTenor(int tenor) {
            this.tenor = tenor;
        }

        public int getFrequency() {
            return frequency;
        }

        public void setFrequency(int frequency) {
            this.frequency = frequency;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getDueDate() {
            return dueDate;
        }

        public void setDueDate(String dueDate) {
            this.dueDate = dueDate;
        }

        public String getReceivingDate() {
            return receivingDate;
        }

        public void setReceivingDate(String receivingDate) {
            this.receivingDate = receivingDate;
        }

        public List<Invite> getInvites() {
            return invites;
        }

        public void setInvites(List<Invite> invites) {
            this.invites = invites;
        }

        public List<Member> getMembers() {
            return members;
        }

        public void setMembers(List<Member> members) {
            this.members = members;
        }
    }

    public List<Committee> getCommitteeList() {
        return committeeList;
    }

    public void setCommitteeList(List<Committee> committeeList) {
        this.committeeList = committeeList;
    }
}
