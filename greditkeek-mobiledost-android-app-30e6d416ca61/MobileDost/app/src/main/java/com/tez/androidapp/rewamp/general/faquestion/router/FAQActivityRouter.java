package com.tez.androidapp.rewamp.general.faquestion.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.faquestion.FAQActivity;

public class FAQActivityRouter extends BaseActivityRouter {

    public static FAQActivityRouter createInstance() {
        return new FAQActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, FAQActivity.class);
    }
}
