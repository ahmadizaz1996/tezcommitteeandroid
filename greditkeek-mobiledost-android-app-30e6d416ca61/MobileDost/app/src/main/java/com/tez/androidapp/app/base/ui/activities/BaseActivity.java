package com.tez.androidapp.app.base.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.observer.OnAppStart;
import com.tez.androidapp.app.base.observer.SessionTimeOutObserver;
import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.location.handlers.LocationHandler;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.signup.router.WelcomeBackActivityRouter;
import com.tez.androidapp.rewamp.util.DialogUtil;

import net.tez.fragment.util.Constants;
import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener;
import net.tez.fragment.util.optional.Optional;
import net.tez.fragment.util.optional.TextUtil;

/**
 * Created by FARHAN DHANANI on 2/3/2019.
 */
public abstract class BaseActivity extends AppCompatActivity implements OnAppStart, IBaseView {


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private final BroadcastReceiver logClickEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String buttonName = intent.getStringExtra(Constants.BUTTON_NAME);
            if (TextUtil.isNotEmpty(buttonName)) {
                String event = getScreenName() + "_" + buttonName + "_" + LocaleHelper.getSelectedLanguageForBackend(BaseActivity.this);
                logCustomClickEvent(event);
            }
        }
    };

    @Nullable
    protected TezLoader tezLoader;

    @Override
    public void onAppStart(boolean inactiveTimeExceeded) {
        Optional.doWhen(inactiveTimeExceeded && isValidationRequired(), this::startWelcomeBackActivity);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tezLoader = getTezLoader();
        Utility.logCrashlytics(getScreenChangeEvent());
    }

    @Override
    protected void onStart() {
        super.onStart();
        dismissTezLoaderOnTransition();
    }

    @Override
    protected void onResume() {
        super.onResume();
        logScreenChangeEvent(getScreenChangeEvent());
        registerReceiver(logClickEventReceiver, new IntentFilter(Constants.BROADCAST_LOG_CLICK_EVENT));
        this.checkForRoot();
        this.registerObserver();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View currentFocus = getCurrentFocus();
        if (ev.getAction() == MotionEvent.ACTION_DOWN && childConditionOnHideKeyboard(ev) && currentFocus != null && Utility.isTouchOutSideViewBoundary(currentFocus, ev))
            Utility.hideKeyboard(this, getCurrentFocus());
        return super.dispatchTouchEvent(ev);
    }

    protected boolean childConditionOnHideKeyboard(MotionEvent event) {
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterLogEventReceiver();
    }

    private void unregisterLogEventReceiver() {
        try {
            unregisterReceiver(logClickEventReceiver);
        } catch (Exception ignore) {

        }
    }

    @Override
    protected void onStop() {
        performWorkAfterActivityTransition();
        super.onStop();
        dismissTezLoaderOnTransition();
    }

    protected void performWorkAfterActivityTransition() {

    }

    @NonNull
    @Override
    public final BaseActivity getHostActivity() {
        return this;
    }

    @Nullable
    @Override
    public final TezLoader getInitializedLoader() {
        return tezLoader;
    }

    @Nullable
    protected TezLoader getTezLoader() {
        return null;
    }

    @Override
    public void showInformativeDialog(@StringRes int title, @StringRes int desc) {
        DialogUtil.showInformativeDialog(this, title, desc);
    }

    @Override
    public void showInformativeDialog(@StringRes int title, @StringRes int desc,
                                      DoubleTapSafeDialogClickListener doubleTapSafeDialogClickListener) {
        DialogUtil.showActionableDialog(this, R.string.notice, R.string.some_feilds_are_not_entered,
                doubleTapSafeDialogClickListener);
    }

    @NonNull
    protected TezLoader createLoader() {
        return new TezLoader(this);
    }


    public void getCurrentLocation(@NonNull LocationAvailableCallback callback) {
        LocationHandler.requestCurrentLocation(this, callback);
    }

    protected boolean isValidationRequired() {
        return Utility.doesUserExist();
    }

    protected void startActivity(Class<?> activity) {
        startActivity(new Intent(this, activity));
    }

    protected View inflateLayout(@LayoutRes int layoutId) {
        return LayoutInflater
                .from(this)
                .inflate(layoutId, null, false);
    }

    protected void startWelcomeBackActivity() {
        WelcomeBackActivityRouter.createInstance().setDependenciesAndRouteForUserSessionTimeout(this);
    }

    protected void registerObserver() {
        SessionTimeOutObserver.getInstance().setClientObserver(this);
    }

    public void showUnExpectedErrorOccurredDialog() {
        showInformativeMessage(R.string.string_something_unexpected_happened,
                (d, w) -> finish());
    }

    private void checkForRoot() {
        if ((Utility.isDeviceRooted(getApplicationContext()) || Utility.checkIsEmulator())) {
            DialogUtil.showInformativeMessage(this, getString(R.string.string_device_rooted), (dialog, which) -> {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                            finishAffinity();
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            finishAndRemoveTask();
                        }
                        System.exit(0);
                    }
            );
        }
    }


    @Override
    public void finishActivity() {
        finish();
    }

    protected void dismissTezLoaderOnTransition() {
        if (tezLoader != null &&
                tezLoader.isShowing() &&
                tezLoader.isDissmissOnTransition()) {
            tezLoader.setDissmissOnTransition(false);
            tezLoader.dismiss();
        }
    }

    abstract protected String getScreenName();

    public void logCustomClickEvent(String eventName) {
        logEvent(eventName);
        MobileDostApplication.getInstance().trackEvent("Tez", "Click", eventName);
        Utility.logCrashlytics(eventName);
    }

    public void logEvent(String eventName) {
        Bundle bundle = new Bundle();
        Utility.getFirebaseAnalyticsInstance().logEvent(eventName, bundle);
        Utility.getAppEventsLoggerInstance().logEvent(eventName, bundle);
    }

    public void logScreenChangeEvent(String screenName) {
        Utility.getFirebaseAnalyticsInstance().setCurrentScreen(this, screenName, screenName);
        logEvent(screenName);
        MobileDostApplication.getInstance().trackEvent("Tez", "Screen", screenName);
    }

    private String getScreenChangeEvent() {
        return getScreenName() + "_" + LocaleHelper.getSelectedLanguageForBackend(BaseActivity.this);
    }
}
