package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeSendMessageResponse;
import com.tez.androidapp.rewamp.committee.response.GetGroupChatMessagesResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface CommitteeGroupChatSendMessageListener {

    void onSendSuccess(CommitteeSendMessageResponse committeeSendMessageResponse);

    void onSendFailure(int errorCode, String message);

}
