package com.tez.androidapp.rewamp.util;

import android.content.Context;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.commons.widgets.ActionableDialog;
import com.tez.androidapp.commons.widgets.InformativeDialog;

import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener;

/**
 * Created by VINOD KUMAR on 7/23/2019.
 */
@SuppressWarnings("unused")
public final class DialogUtil {

    private DialogUtil() {

    }

    public static void showInformativeDialog(@NonNull Context context,
                                             @StringRes int title,
                                             @StringRes int message) {

        initiateInformativeDialog(context, getString(context, title), getString(context, message), null);
    }

    public static void showInformativeDialog(@NonNull Context context,
                                             @NonNull String title,
                                             @NonNull String message) {

        initiateInformativeDialog(context, title, message, null);
    }

    public static void showInformativeDialog(@NonNull Context context,
                                             @NonNull String title,
                                             @NonNull String message,
                                             DoubleTapSafeDialogClickListener listener) {

        initiateInformativeDialog(context, title, message, listener);
    }

    public static void showInformativeMessage(@NonNull Context context, @NonNull String message) {

        initiateInformativeDialog(context, "", message, null);
    }

    public static void showInformativeMessage(@NonNull Context context,
                                              @NonNull String message,
                                              @NonNull DoubleTapSafeDialogClickListener listener) {

        initiateInformativeDialog(context, "", message, listener);
    }

    public static void showActionableDialog(@NonNull Context context,
                                            @StringRes int title,
                                            @StringRes int message,
                                            @NonNull DoubleTapSafeDialogClickListener listener) {
        initiateActionableDialog(context,
                0,
                title,
                message,
                0,
                0,
                listener);

    }

    public static void showActionableDialog(@NonNull Context context,
                                            @StringRes int title,
                                            @NonNull String message,
                                            @NonNull DoubleTapSafeDialogClickListener listener) {
        initiateActionableDialog(context,
                0,
                getString(context, title),
                message,
                0,
                0,
                listener);

    }

    public static void showActionableDialog(@NonNull Context context,
                                            @StringRes int title,
                                            @StringRes int message,
                                            @StringRes int btOkText,
                                            @NonNull DoubleTapSafeDialogClickListener listener) {
        initiateActionableDialog(context,
                0,
                title,
                message,
                btOkText,
                0,
                listener);
    }

    public static void showActionableDialog(@NonNull Context context,
                                            @NonNull String title,
                                            @NonNull String message,
                                            @StringRes int btOkText,
                                            @NonNull DoubleTapSafeDialogClickListener listener) {
        initiateActionableDialog(context,
                0,
                title,
                message,
                btOkText,
                0,
                listener);
    }

    public static void showActionableDialog(@NonNull Context context,
                                            @StringRes int title,
                                            @StringRes int message,
                                            @StringRes int btOkText,
                                            @StringRes int btCancelText,
                                            @NonNull DoubleTapSafeDialogClickListener listener) {
        initiateActionableDialog(context,
                0,
                title,
                message,
                btOkText,
                btCancelText,
                listener);
    }

    public static void showActionableDialog(@NonNull Context context,
                                            @StringRes int title,
                                            @NonNull String message,
                                            @StringRes int btOkText,
                                            @StringRes int btCancelText,
                                            @NonNull DoubleTapSafeDialogClickListener listener) {
        initiateActionableDialog(context,
                0,
                getString(context, title),
                message,
                btOkText,
                btCancelText,
                listener);
    }

    public static void showActionableDialog(@NonNull Context context,
                                            @DrawableRes int headerImage,
                                            @StringRes int title,
                                            @StringRes int message,
                                            @StringRes int btOkText,
                                            @StringRes int btCancelText,
                                            @NonNull DoubleTapSafeDialogClickListener listener) {

        initiateActionableDialog(context,
                headerImage,
                title,
                message,
                btOkText,
                btCancelText,
                listener);
    }

    private static void initiateInformativeDialog(@NonNull Context context,
                                                  @Nullable String title,
                                                  @Nullable String message,
                                                  @Nullable DoubleTapSafeDialogClickListener listener) {

        InformativeDialog informativeDialog = new InformativeDialog(context);
        informativeDialog.setTitle(title);
        informativeDialog.setMessage(message);
        informativeDialog.setListener(listener);
        informativeDialog.show();
    }

    private static void initiateActionableDialog(@NonNull Context context,
                                                 @DrawableRes int headerImage,
                                                 @StringRes int title,
                                                 @StringRes int message,
                                                 @StringRes int btOkText,
                                                 @StringRes int btCancelText,
                                                 @NonNull DoubleTapSafeDialogClickListener listener) {

        initiateActionableDialog(context, headerImage, getString(context, title), getString(context, message), btOkText, btCancelText, listener);
    }

    private static void initiateActionableDialog(@NonNull Context context,
                                                 @DrawableRes int headerImage,
                                                 @Nullable String title,
                                                 @Nullable String message,
                                                 @StringRes int btOkText,
                                                 @StringRes int btCancelText,
                                                 @NonNull DoubleTapSafeDialogClickListener listener) {

        ActionableDialog actionableDialog = new ActionableDialog(context);
        actionableDialog.setHeaderImageRes(headerImage);
        actionableDialog.setTitle(title);
        actionableDialog.setMessage(message);
        actionableDialog.setBtOkText(getString(context, btOkText));
        actionableDialog.setBtCancelText(getString(context, btCancelText));
        actionableDialog.setListener(listener);
        actionableDialog.show();
    }

    private static String getString(@NonNull Context context, @StringRes int stringRes) {
        if (stringRes == 0 || stringRes == -1)
            return null;
        return context.getResources().getString(stringRes);
    }
}
