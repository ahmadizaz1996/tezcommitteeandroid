package com.tez.androidapp.rewamp.signup;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.util.DialogUtil;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public abstract class PermissionActivity extends BaseActivity implements MultiplePermissionsListener {

    @BindView(R.id.tvMessage1)
    protected TezTextView tvMessage1;

    @BindView(R.id.tvTitle)
    protected TezTextView tvTitle;

    @BindView(R.id.btMainButton)
    private TezButton btMainButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);
        ViewBinder.bind(this);
    }

    @OnClick(R.id.btMainButton)
    private void collectPermissions() {
        this.btMainButton.setEnabled(false);
        List<String> missingPermissions = getRequiredPermissions();
        if (missingPermissions.isEmpty())
            onAllPermissionsGranted();
        else {
            if(isOptionalPermissionNotRequired()) {
                missingPermissions.addAll(Utility.getOptionalPermissions(this));
            }
            Dexter.withActivity(this).withPermissions(missingPermissions).withListener(this).check();
        }
    }

    @OnClick(R.id.tvSecondaryButton)
    private void showPermissionNeededDialog() {
        DialogUtil.showInformativeDialog(this, R.string.why_do_i_have_to_give_these_permission, R.string.permission_need_defined);
    }

    @Override
    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
        token.continuePermissionRequest();
    }

    @Override
    public final void onPermissionsChecked(MultiplePermissionsReport report) {
        Optional.doWhen(checkRelevantPermissionsAreGiven(report),
                this::onAllPermissionsGranted,
                () -> Optional.doWhen(report.isAnyPermissionPermanentlyDenied(),
                        this::onPermissionPermanentDenied,
                        this::onPermissionDenied));
    }

    private boolean checkRelevantPermissionsAreGiven(MultiplePermissionsReport report){
        if(isOptionalPermissionNotRequired()){
            for(PermissionDeniedResponse permissionDeniedResponse :report.getDeniedPermissionResponses()){
                 if(Utility.getSignupPermissions(this).contains(permissionDeniedResponse.getPermissionName())){
                     return false;
                 }
            }
            return true;
        } else{
            return report.areAllPermissionsGranted();
        }
    }

    protected void onAllPermissionsGranted() {
        this.tvTitle.setTextColor(Utility.getColorFromResource(R.color.textViewTitleColorBlue));
        this.tvMessage1.setTextColor(Utility.getColorFromResource(R.color.textViewTextColorBlack));
        this.btMainButton.setEnabled(true);
    }

    protected void onPermissionDenied() {
        this.tvTitle.setTextColor(Utility.getColorFromResource(R.color.stateRejectedColorRed));
        this.tvMessage1.setTextColor(Utility.getColorFromResource(R.color.stateRejectedColorRed));
        showInformativeMessage(R.string.provide_permissions_for_number_verification,
                ((dialog, which) -> this.btMainButton.setEnabled(true)));
    }

    protected void onPermissionPermanentDenied() {
        this.tvTitle.setTextColor(Utility.getColorFromResource(R.color.stateRejectedColorRed));
        this.tvMessage1.setTextColor(Utility.getColorFromResource(R.color.stateRejectedColorRed));
        showInformativeMessage(R.string.provide_permissions_from_settings_and_try_again, (dialog, which) -> {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts
                    ("package", this.getPackageName(), null));
            startActivity(intent);
            finish();
        });
    }

    protected abstract List<String> getRequiredPermissions();

    protected boolean isOptionalPermissionNotRequired(){
        return true;
    }
}
