package com.tez.androidapp.app.vertical.advance.loan.confirmation.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.advance.loan.confirmation.callbacks.GenerateDisbursementReceiptCallback;
import com.tez.androidapp.app.vertical.advance.loan.confirmation.models.network.dto.response.GenerateDisbursementReceiptResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by Rehman Murad Ali on 9/14/2017.
 */

public class GenerateDisbursementReceiptRH extends BaseRH<GenerateDisbursementReceiptResponse> {

    GenerateDisbursementReceiptCallback generateDisbursementReceiptCallback;

    public GenerateDisbursementReceiptRH(BaseCloudDataStore baseCloudDataStore, GenerateDisbursementReceiptCallback generateDisbursementReceiptCallback) {
        super(baseCloudDataStore);
        this.generateDisbursementReceiptCallback = generateDisbursementReceiptCallback;
    }

    @Override
    protected void onSuccess(Result<GenerateDisbursementReceiptResponse> value) {
        GenerateDisbursementReceiptResponse generateDisbursementReceiptResponse = value.response().body();
        if (isErrorFree(generateDisbursementReceiptResponse))
            generateDisbursementReceiptCallback.onGenerateDisbursementReceiptSuccess(generateDisbursementReceiptResponse.getLoanDetails());
        else onFailure(generateDisbursementReceiptResponse.getStatusCode(), generateDisbursementReceiptResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        generateDisbursementReceiptCallback.onGenerateDisbursementReceiptFailure(errorCode, message);
    }
}
