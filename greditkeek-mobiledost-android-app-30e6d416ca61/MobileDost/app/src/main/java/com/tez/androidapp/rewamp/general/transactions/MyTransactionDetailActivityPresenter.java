package com.tez.androidapp.rewamp.general.transactions;

import android.view.View;

import com.tez.androidapp.app.general.feature.transactions.models.network.TransactionDetail;
import com.tez.androidapp.commons.models.network.RepaymentTransaction;
import com.tez.androidapp.commons.utils.app.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VINOD KUMAR on 8/28/2019.
 */
public class MyTransactionDetailActivityPresenter implements IMyTransactionDetailActivityPresenter, IMyTransactionDetailActivityInteractorOutput {

    private final IMyTransactionDetailActivityView iMyTransactionDetailActivityView;
    private final IMyTransactionDetailActivityInteractor iMyTransactionDetailActivityInteractor;

    public MyTransactionDetailActivityPresenter(IMyTransactionDetailActivityView iMyTransactionDetailActivityView) {
        this.iMyTransactionDetailActivityView = iMyTransactionDetailActivityView;
        iMyTransactionDetailActivityInteractor = new MyTransactionDetailActivityInteractor(this);
    }

    @Override
    public void getLoanTransactionDetail(int transactionId) {
        iMyTransactionDetailActivityView.showTezLoader();
        iMyTransactionDetailActivityInteractor.getLoanTransactionDetail(transactionId);
    }

    @Override
    public void onGetLoanTransactionDetailSuccess(TransactionDetail transactionDetail) {
        iMyTransactionDetailActivityView.dismissTezLoader();
        List<RepaymentTransaction> repaymentTransactions = transactionDetail.getRepaymentSummary();
        if (repaymentTransactions != null && !repaymentTransactions.isEmpty()) {
            List<MyTransactionDetail> myTransactionDetailList = new ArrayList<>();
            for (RepaymentTransaction repaymentTransaction : repaymentTransactions) {
                MyTransactionDetail myTransactionDetail = new MyTransactionDetail();
                myTransactionDetail.setTransactionId(String.valueOf(repaymentTransaction.getTxnId()));
                myTransactionDetail.setAmountPaid(Utility.getFormattedAmountWithoutCurrencyLabel(repaymentTransaction.getAmountRepayed()));
                myTransactionDetail.setRepaymentDate(repaymentTransaction.getRepaymentDate());
                myTransactionDetail.setWalletName(Utility.getWalletName(repaymentTransaction.getServiceProviderId()));
                myTransactionDetail.setWalletNumber(repaymentTransaction.getMobileAccountNumber());
                myTransactionDetailList.add(myTransactionDetail);
            }
            iMyTransactionDetailActivityView.setMyTransactionDetailAdapter(myTransactionDetailList);
        } else {
            iMyTransactionDetailActivityView.setRvMyTransactionDetailsVisibility(View.GONE);
            iMyTransactionDetailActivityView.setLayoutEmptyTransactionDetailVisibility(View.VISIBLE);
        }
        iMyTransactionDetailActivityView.setTvRemainingAmountText(transactionDetail.getRemainingAmountString());
        iMyTransactionDetailActivityView.setTvRemainingAmountVisibility();
    }

    @Override
    public void onGetLoanTransactionDetailFailure(int statusCode, String message) {
        iMyTransactionDetailActivityView.setRvMyTransactionDetailsVisibility(View.GONE);
        iMyTransactionDetailActivityView.dismissTezLoader();
        iMyTransactionDetailActivityView.setTransactionSummaryError();
    }
}
