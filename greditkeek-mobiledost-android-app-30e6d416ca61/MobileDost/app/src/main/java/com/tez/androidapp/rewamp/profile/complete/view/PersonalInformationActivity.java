package com.tez.androidapp.rewamp.profile.complete.view;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.util.CollectionUtils;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.utils.app.Constants;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.utils.file.util.FileUtil;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.commons.widgets.UploadImageCardView;
import com.tez.androidapp.rewamp.advance.request.router.LoanRequiredStepsActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.InsuranceRequiredStepsActivityRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.profile.complete.QuestionDropDownAdapter;
import com.tez.androidapp.rewamp.profile.complete.presenter.PersonalInformationFragmentPresenter;
import com.tez.androidapp.rewamp.profile.complete.router.ProfileFinalizedActivityRouter;
import com.tez.androidapp.rewamp.profile.router.CompleteProfileRouter;

import net.tez.camera.cameras.CameraProperties;
import net.tez.camera.request.CameraRequestBuilder;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalInformationActivity
        extends BaseActivity implements IPersonalInformationFragmentView {


    @BindView(R.id.recyclerViewQuestion)
    private RecyclerView recyclerViewQuestion;

    @BindView(R.id.uploadImageCV)
    private UploadImageCardView uploadImageCV;

    @BindView(R.id.btContinue)
    private TezButton btContinue;

    @BindView(R.id.shimmer)
    private TezLinearLayout shimmer;

    @BindView(R.id.tvUploadImage)
    private TezTextView tvUploadImage;

    @BindView(R.id.tezCardView)
    private TezCardView tezCardView;

    private final PersonalInformationFragmentPresenter personalInformationFragmentPresenter;

    public PersonalInformationActivity() {
        this.personalInformationFragmentPresenter = new PersonalInformationFragmentPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_personal_information);
        ViewBinder.bind(this);
        init();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Optional.doWhen(requestCode == CameraRequestBuilder.REQUEST_CODE_CAMERA
                && resultCode == Activity.RESULT_OK, () -> {
            if (data != null && data.getBundleExtra(CameraRequestBuilder.PARAMETERS_BUNDLE)!=null) {
                Bundle bundle = data.getBundleExtra(CameraRequestBuilder.PARAMETERS_BUNDLE);
                List<String> files = data.getStringArrayListExtra(CameraRequestBuilder.RESULT_IMAGE_FILES_ARRAY_LIST);
                Optional.ifPresent(CollectionUtils.isEmpty(files) ? null : files.get(0), file -> {
                    this.uploadImageCV.onImageCaptured(file, R.drawable.img_cnic_front_placeholder);
                    this.setOnClickListenerToBtContinue(v -> personalInformationFragmentPresenter.updatePersonalInfo(file,
                             bundle.getString(Constants.CNIC_REQUEST_FOR_DOCUMENT)));
                });
            }
        });
    }

    private void setOnClickListenerToBtContinue(DoubleTapSafeOnClickListener onClickListener) {
        this.btContinue.setDoubleTapSafeOnClickListener(onClickListener);
    }

    private void init() {
        setVisibillityToShimmer(View.VISIBLE);
        setVisibillityToTezCardView(View.GONE);
        personalInformationFragmentPresenter.getPersonalQuestions(LocaleHelper.getSelectedLanguageForBackend(this));
        setOnClickListenerToBtContinue(v -> this.personalInformationFragmentPresenter.updatePersonalInfo(null, null));
    }

    @Override
    public void setVisibillityToShimmer(int visibillity){
        this.shimmer.setVisibility(visibillity);
    }

    @Override
    public void userProfileCompleted(){
        ProfileFinalizedActivityRouter.createInstance().setDependenciesAndRoute(this,
                getKeyRoute());
    }

    @Override
    public void userProfileIsNotCompleted(){
        roteTo(getKeyRoute());
    }

    @Override
    public void setVisibillityToTezCardView(int visibillity){
        this.tezCardView.setVisibility(visibillity);
    }

    @Override
    public void setAdapterToRecyclerView(QuestionDropDownAdapter questionDropDownAdapter) {
        this.recyclerViewQuestion.setAdapter(questionDropDownAdapter);
    }

    @Override
    public void setVisibillityToUploadImageCardView(int visibillity) {
        this.uploadImageCV.setVisibility(visibillity);
        this.tvUploadImage.setVisibility(visibillity);
    }

    @Override
    public void clearUploadImageCvData(){
        this.uploadImageCV.resetUploadImageCardView();
    }

    @Override
    public void setTextToUploadImageCardView(String text) {
        this.uploadImageCV.setTitle(text);
    }

    @Override
    public void userHasAlreadyUploadedProofOfWork(){
        this.uploadImageCV.setPlaceholderImage(getResources().getDrawable(R.drawable.ic_office_id));
    }

    @Override
    public void setOnClickListenerToUploadImageCardView(DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        this.uploadImageCV.setDoubleTapSafeOnClickListener(doubleTapSafeOnClickListener);
    }

    @Override
    public void startCameraForUploadPicture(String questionRefName) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.CNIC_REQUEST_FOR_DOCUMENT, questionRefName);
        new CameraRequestBuilder()
                .setLensFacing(CameraProperties.LENS_FACING_BACK)
                .setEnableSwitchCamera(false)
                .setEnableGalleryMode(true)
                .setMaxImageCount(1)
                .setRootPath(FileUtil.getDocumentRootPath(this))
                .build()
                .startCamera(this, bundle);
    }
    private int getKeyRoute(){
        return getIntent().getIntExtra(CompleteProfileRouter.ROUTE_KEY, CompleteProfileRouter.ROUTE_TO_DEFAULT);
    }

    @Override
    public void routeToLoanRequiredSteps() {
        LoanRequiredStepsActivityRouter.createInstance().routeToExistingInstanceOfActivity(this, false);
    }

    @Override
    public void routeToInsuranceRequiredSteps() {
        InsuranceRequiredStepsActivityRouter.createInstance().routeToExistingInstanceOfActivity(this, false);
    }

    @Override
    public void routeToDashboard() {
        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    protected String getScreenName() {
        return "PersonalInformationActivity";
    }
}
