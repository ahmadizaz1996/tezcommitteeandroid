package com.tez.androidapp.app.base.interactor;

import android.location.Location;
import androidx.annotation.NonNull;

import com.tez.androidapp.commons.managers.MDPreferenceManager;

/**
 * Created by VINOD KUMAR on 1/17/2019.
 */
public interface IBaseInteractor {

    default void saveLocationInPreference(@NonNull Location location) {
        MDPreferenceManager.setLastLocation(location.getLatitude(), location.getLongitude());
    }
}
