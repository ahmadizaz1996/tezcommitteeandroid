package com.tez.androidapp.rewamp.committee.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class JoinCommitteeVerificationRequest extends BaseRequest {
    public static final String METHOD_NAME = "http://3.21.127.35/api/v1/committee/verifyOtp";

    public int otp;
    public int inviteId;
    public int commiteeId;

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }

    public int getInviteId() {
        return inviteId;
    }

    public void setInviteId(int inviteId) {
        this.inviteId = inviteId;
    }

    public int getCommiteeId() {
        return commiteeId;
    }

    public void setCommiteeId(int commiteeId) {
        this.commiteeId = commiteeId;
    }
}
