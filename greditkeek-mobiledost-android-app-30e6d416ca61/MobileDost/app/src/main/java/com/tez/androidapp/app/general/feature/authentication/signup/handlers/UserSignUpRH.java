package com.tez.androidapp.app.general.feature.authentication.signup.handlers;

import androidx.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.authentication.signup.callbacks.UserSignUpCallback;
import com.tez.androidapp.commons.models.network.dto.response.UserSignUpResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

import okhttp3.ResponseBody;

/**
 * Created  on 2/12/2017.
 */

public abstract class UserSignUpRH extends BaseRH<UserSignUpResponse> {

    private UserSignUpCallback userSignUpCallback;

    public UserSignUpRH(BaseCloudDataStore baseCloudDataStore, @Nullable UserSignUpCallback userSignUpCallback) {
        super(baseCloudDataStore);
        this.userSignUpCallback = userSignUpCallback;
    }

    @Override
    protected void onSuccess(Result<UserSignUpResponse> value) {
        if (value.isError())
            onError(value.response().errorBody());
        else
            onCorrect(value.response().body());
    }

    private void onCorrect(@Nullable UserSignUpResponse userSignUpResponse) {
        if (userSignUpResponse != null) {

            if (userSignUpResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
//                UserSignUpRequest userSignUpRequest = MDPreferenceManager.getUserSignUpRequest();
//                MDPreferenceManager.setPrincipalName(userSignUpRequest != null ? userSignUpRequest.getCnic().replaceAll("-", "") : null);
                updateUserMobileNumber();
                if (userSignUpCallback != null)
                    userSignUpCallback.onUserSignUpSuccess(userSignUpResponse);

            } else {
                onFailure(userSignUpResponse.getStatusCode(), userSignUpResponse.getErrorDescription());
            }

        } else
            onFailure(0, null);
    }

    private void onError(@Nullable ResponseBody responseBody) {
        try {
            if (responseBody != null) {
                BaseResponse baseResponse = new Gson().fromJson(responseBody.toString(), BaseResponse.class);
                onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
            } else
                onFailure(0, null);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        message = message == null ? Constants.ERROR_MESSAGE : message;
        if (userSignUpCallback != null)
            userSignUpCallback.onUserSignUpError(errorCode, message);
    }

    public abstract void updateUserMobileNumber();
}
