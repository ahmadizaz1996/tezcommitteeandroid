package com.tez.androidapp.rewamp.general.profile.view;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Answer;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;

import java.util.List;

public interface IBasicProfileActivityView extends IBaseView {
    void setTextToEtName(String text);

    void setTextToSGender(String text);

    void setTextToEtDOB(String text);

    void setTextToJob(String text);

    void setTextToSubJob(String text);

    void setBtContinueEnabled(boolean enabled);

    String getTextFromEtName();

    void setVisibillityForShimmer(int visibillity);

    void setAdapterToJobSpinner(List<Option> jobs, Option selectedOption);

    void setHintToJob(String text);

    void setHintToSubJob(String text);

    void setAdapterToSubJobSpinner(List<Answer> data, AdapterView.OnItemClickListener listener);

    void setVisibillityForSubJobSpinner(int visibillity);

    void setVisibillityForMainCard(int visibillity);

    String getTextFromEtDob();

    void unlockLimit();

    String getTextFromSGender();

    void setListenerToSGender(List<Option> jobs);
}
