package com.tez.androidapp.rewamp.advance.limit.presenter;

import android.view.View;

import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.response.LoanLimitResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.advance.limit.interactor.ILimitAssignedActivityInteractor;
import com.tez.androidapp.rewamp.advance.limit.interactor.LimitAssignedActivityInteractor;
import com.tez.androidapp.rewamp.advance.limit.view.ILimitAssignedActivityView;

import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class LimitAssignedActivityPresenter implements ILimitAssignedActivityPresenter, ILimitAssignedActivityInteractorOutput {

    private final ILimitAssignedActivityView iLimitAssignedActivityView;
    private final ILimitAssignedActivityInteractor iLimitAssignedActivityInteractor;

    public LimitAssignedActivityPresenter(ILimitAssignedActivityView iLimitAssignedActivityView) {
        this.iLimitAssignedActivityView = iLimitAssignedActivityView;
        iLimitAssignedActivityInteractor = new LimitAssignedActivityInteractor(this);
    }

    @Override
    public void getLoanLimit() {
        iLimitAssignedActivityView.setClContentVisibility(View.GONE);
        iLimitAssignedActivityView.showTezLoader();
        iLimitAssignedActivityInteractor.getLoanLimit();
    }

    @Override
    public void onLoanLimitSuccess(LoanLimitResponse loanLimitResponse) {
        iLimitAssignedActivityView.setClContentVisibility(View.VISIBLE);
        iLimitAssignedActivityView.dismissTezLoader();
        try {
            Date date = Utility.getFormattedDate(loanLimitResponse.getExpiryDate(), Constants.INPUT_DATE_FORMAT);
            long timeRemaining = date.getTime() - System.currentTimeMillis();
            int hrsInt = (int) TimeUnit.MILLISECONDS.toHours(timeRemaining);
            int minsInt = (int) TimeUnit.MILLISECONDS.toMinutes(timeRemaining);

            String hrs = Utility.getTwoDigitInteger(hrsInt);
            String mins = Utility.getTwoDigitInteger(minsInt - hrsInt * 60);
            String secs = Utility.getTwoDigitInteger((int) TimeUnit.MILLISECONDS.toSeconds(timeRemaining) - minsInt * 60);
            String finalString = hrs + ":" + mins + ":" + secs;
            iLimitAssignedActivityView.setTvClockText(finalString);
        } catch (ParseException e) {
            iLimitAssignedActivityView.setTvClockText("");
        }
        iLimitAssignedActivityView.setClickListenerToRouteToSelectLoanActivity();

    }

    @Override
    public void onLoanLimitFailure(int errorCode, String message) {
        iLimitAssignedActivityView.dismissTezLoader();
        iLimitAssignedActivityView.showError(errorCode, (dialog, which) -> iLimitAssignedActivityView.routeToDashboard());
    }
}
