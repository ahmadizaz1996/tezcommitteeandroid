package com.tez.androidapp.rewamp.general.wallet.router;

import android.content.Intent;
import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.MyWalletActivity;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class MyWalletActivityRouter extends BaseActivityRouter {

    public static MyWalletActivityRouter createInstance() {
        return new MyWalletActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, MyWalletActivity.class);
    }
}
