package com.tez.androidapp.rewamp.bima.products.policy.presenter;

import androidx.annotation.NonNull;

import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InitiatePurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.request.PurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.response.PolicyDetails;
import com.tez.androidapp.rewamp.bima.products.policy.view.IInsurancePaymentActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

public class InsurancePaymentActivityPresenter extends EasyPaisaInsurancePaymentActivityPresenter
        implements IInsurancePaymentActivityPresenter {


    private final IInsurancePaymentActivityView iVerifyRepaymentActivityView;

    public InsurancePaymentActivityPresenter(IInsurancePaymentActivityView iInsurancePaymentActivityView) {
        super(iInsurancePaymentActivityView);
        iVerifyRepaymentActivityView = iInsurancePaymentActivityView;
    }

    @Override
    public void initiatePurchasePolicySuccess(PolicyDetails policyDetails) {
        super.initiatePurchasePolicySuccess(policyDetails);
        iVerifyRepaymentActivityView.startTimer();
    }

    @Override
    public void resendCode(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest) {
        iVerifyRepaymentActivityView.setPinViewOtpEnabled(false);
        iVerifyRepaymentActivityView.setTvResendCodeEnabled(false);
        iVerifyRepaymentActivityView.setPinViewOtpClearText();
        iVerifyRepaymentActivityView.showTezLoader();
        iEasyPaisaInsurancePaymentActivityInteractor.resendCode(initiatePurchasePolicyRequest,
                new InitiatePurchasePolicyCallback() {
                    @Override
                    public void initiatePurchasePolicySuccess(PolicyDetails policyDetails) {
                        iVerifyRepaymentActivityView.setPinViewOtpEnabled(true);
                        iVerifyRepaymentActivityView.dismissTezLoader();
                        iVerifyRepaymentActivityView.startTimer();
                    }

                    @Override
                    public void initiatePurchasePolicyFailure(int errorCode, String message) {

                        iVerifyRepaymentActivityView.dismissTezLoader();
                        iVerifyRepaymentActivityView.showError(errorCode,
                                iVerifyRepaymentActivityView.getFormattedErrorMessage(ResponseStatusCode.getDescriptionFromErrorCode(errorCode)),
                                (dialog, which) -> iVerifyRepaymentActivityView.startTimer());

                    }
                });
    }

    @Override
    protected void setPinInRequest(@NonNull PurchasePolicyRequest request, @NonNull String pin) {
        request.setOtp(pin);
    }
}
