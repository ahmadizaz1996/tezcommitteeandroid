package com.tez.androidapp.rewamp.bima.products.presenter;

public interface IProductPolicyActivityPresenter {

    void getProductPolicy(int policyId);
}
