package com.tez.androidapp.rewamp.committee.view;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.cnic.detection.util.DateUtil;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.presenter.CommitteeSummaryActivityPresenter;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.request.JoinCommitteeRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeDeclineResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeLeaveResponse;
import com.tez.androidapp.rewamp.committee.response.GetInvitedPackageResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeResponse;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeTermsandConditionActivityRouter;
import com.tez.androidapp.rewamp.committee.shared.api.client.CommitteeAPI;
import com.tez.androidapp.rewamp.util.RetrofitClient;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter.COMMITTEE_DATA;
import static com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter.COMMITTE_CREATEION;

public class CommitteeSummaryActivity extends BaseActivity implements IBaseView, DoubleTapSafeOnClickListener, ICommitteeSummaryActivityView {

    @BindView(R.id.committeeName)
    private TezTextView committeeName;

    @BindView(R.id.committeeAmount)
    private TezTextView committeeAmount;

    @BindView(R.id.tvCommitteeInstallment)
    private TezTextView tvCommitteeInstallment;

    @BindView(R.id.tvCommittePackage)
    private TezTextView tvCommittePackage;

    @BindView(R.id.tvCommitteeDuration)
    private TezTextView tvCommitteeDuration;

    @BindView(R.id.tvCommitteeMethod)
    private TezTextView tvCommitteeMethod;

    @BindView(R.id.tvNoOfMembers)
    private TezTextView tvNoOfMembers;

    @BindView(R.id.tvCommitteeStartDate)
    private TezTextView tvCommitteeStartDate;

    @BindView(R.id.tvCommitteeFirstInstallment)
    private TezTextView tvCommitteeFirstInstallment;

    @BindView(R.id.tvCommitteeRcvingDate)
    private TezTextView tvCommitteeRcvingDate;

    @BindView(R.id.btDone)
    private TezButton btDone;

    @BindView(R.id.btnJoinCommittee)
    private TezButton btnJoinCommittee;

    @BindView(R.id.btnDeclineInvitation)
    private TezButton btnDeclineInvitation;


    private CommitteePackageModel committeePackage;
    private CommitteeCreateResponse committeeCreationResponse;
    private final CommitteeSummaryActivityPresenter committeeSummaryPresenter;
    private CommitteeInviteRequest committeeInviteRequest;
    private MyCommitteeResponse myCommitteeResponse;
    private MyCommitteeResponse.Committee committeeData;
    private GetInvitedPackageResponse.CommitteeList invitedCommitteePackage;
    private long mUserId;
    private long mLoggedInUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_committee_summary);
            ViewBinder.bind(this);
            fetchExtras();
            fillInfo();
            initClickListeners();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CommitteeSummaryActivity() {
        committeeSummaryPresenter = new CommitteeSummaryActivityPresenter(this);
    }

    private void fillInfo() {
        /*committeeName.setText(committeePackage.getCommitteeName());
        committeeAmount.setText(committeePackage.getSelectedAmount() + "");
        tvCommitteeInstallment.setText(committeePackage.getInstallmentAmount() + "");
        tvCommittePackage.setText(committeePackage.getCommitteeMethod());
        tvCommitteeDuration.setText(committeePackage.getDurationInMonths() + " months");
        tvCommitteeMethod.setText(committeePackage.getCommitteeMethod());
        tvNoOfMembers.setText(committeePackage.getSelectedMembers() + "");
        tvCommitteeStartDate.setText(committeePackage.getStartDate());
        tvCommitteeFirstInstallment.setText(committeePackage.getStartDate());
        tvCommitteeRcvingDate.setText("--");*/

        try {
            if (myCommitteeResponse != null) {
                committeeData = myCommitteeResponse.committeeList.get(0);
                committeeName.setText(committeeData.getName());
                committeeAmount.setText(committeeData.getAmount() + "");
                tvCommitteeInstallment.setText(committeeData.getInstallment() + "");
                tvCommitteeDuration.setText(committeeData.getTenor() + " months");
                if (committeeData.getFrequency() == 1) {
                    tvCommittePackage.setText(getString(R.string.monthly_installment));
                } else {
                    tvCommittePackage.setText(getString(R.string.bi_monthly_installment));
                }
                tvCommitteeMethod.setText(R.string.first_come_first_serve);
                tvNoOfMembers.setText(committeeData.getMembers().size() + "");
                tvCommitteeStartDate.setText(committeeData.getStartDate().split("T")[0]);
                tvCommitteeFirstInstallment.setText(committeeData.getDueDate().split("T")[0]);
                tvCommitteeRcvingDate.setText(committeeData.getReceivingDate().split("T")[0]);

                /*MDPreferenceManager.getCommitteeCreationData().getData().getUserId();*/
                mUserId = committeeData.getUserId();
                mLoggedInUser = Long.parseLong(MDPreferenceManager.getUser().getId());
//                if (mUserId == mLoggedInUser) {
                btDone.setVisibility(View.VISIBLE);
//                } else {
//                    btDone.setText(R.string.leave_this_committee);
//                    btDone.setTextColor(ResourcesCompat.getColor(getResources(), R.color.green, null));
//                    btDone.setBackground(null);
//                }
            } else if (invitedCommitteePackage != null) {
                committeeName.setText(invitedCommitteePackage.getName());
                committeeAmount.setText(String.valueOf(invitedCommitteePackage.getAmount()));
                tvCommitteeInstallment.setText(String.valueOf(invitedCommitteePackage.getInstallment()));
                tvCommitteeDuration.setText(String.valueOf(invitedCommitteePackage.getTenor()));
                if (invitedCommitteePackage.getFrequency() == 1) {
                    tvCommittePackage.setText(getString(R.string.monthly_installment));
                } else {
                    tvCommittePackage.setText(getString(R.string.bi_monthly_installment));
                }
                tvCommitteeMethod.setText(getString(R.string.first_come_first_serve));
                tvNoOfMembers.setText(String.valueOf(invitedCommitteePackage.getMembers().size()));
                tvCommitteeStartDate.setText(invitedCommitteePackage.getStartDate().split("T")[0]);
                tvCommitteeFirstInstallment.setText(invitedCommitteePackage.getDueDate().split("T")[0]);
                tvCommitteeRcvingDate.setText(invitedCommitteePackage.getReceivingDate().split("T")[0]);

                btnJoinCommittee.setVisibility(View.VISIBLE);
                btnDeclineInvitation.setVisibility(View.VISIBLE);
                btnDeclineInvitation.setText(R.string.decline_invitation);
                btnDeclineInvitation.setTextColor(ResourcesCompat.getColor(getResources(), R.color.green, null));
                btnDeclineInvitation.setBackground(null);
                btDone.setVisibility(View.GONE);
                btDone.setDoubleTapSafeOnClickListener(null);
            } else if (committeeCreationResponse != null) {
                committeeName.setText(committeeCreationResponse.getData().getName());
                committeeAmount.setText(committeeCreationResponse.getData().getAmount() + "");
                tvCommitteeInstallment.setText(committeeCreationResponse.getData().getInstallment() + "");
                tvCommitteeDuration.setText(committeeCreationResponse.getData().getTenor() + " " + getString(R.string.months));
                if (committeeCreationResponse.getData().getFrequency() == 1) {
                    tvCommittePackage.setText(getString(R.string.monthly_installment));
                } else {
                    tvCommittePackage.setText(getString(R.string.bi_monthly_installment));
                }
                tvCommitteeMethod.setText(getString(R.string.first_come_first_serve));
                tvNoOfMembers.setText(committeeCreationResponse.getData().getMembers().size() + "");
                tvCommitteeStartDate.setText(committeeCreationResponse.getData().getStartDate().split("T")[0]);
                tvCommitteeFirstInstallment.setText(committeeCreationResponse.getData().getDueDate().split("T")[0]);
                tvCommitteeRcvingDate.setText(committeeCreationResponse.getData().getReceivingDate().split("T")[0]);

                committeeInviteRequest.setCommitteeId(committeeCreationResponse.getData().getId());

                committeeSummaryPresenter.inviteMembers(committeeInviteRequest);


                /*CommitteeAPI committeeAPI = RetrofitClient.createAPI();
                Call<CommitteeInviteResponse> responseCall = committeeAPI.getInvites("Bearer " + MDPreferenceManager.getAccessToken().getAccessToken(),
                        committeeInviteRequest);
                responseCall.enqueue(new Callback<CommitteeInviteResponse>() {
                    @Override
                    public void onResponse(Call<CommitteeInviteResponse> call, Response<CommitteeInviteResponse> response) {
                        Log.d("RESPONSE", response.toString());
                        if (response.body().getStatusCode() == 0)
                            Log.d("RESPONSE", "Invited Success");
                        else
                            Toast.makeText(CommitteeSummaryActivity.this, response.body().message, Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onFailure(Call<CommitteeInviteResponse> call, Throwable t) {
                        Log.d("RESPONSE", t.toString());
                        Toast.makeText(CommitteeSummaryActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchExtras() {
        try {
            Intent intent = getIntent();
            if (intent != null
                    && intent.getExtras() != null) {
                //            committeePackage = (CommitteePackageModel) intent.getExtras().getSerializable(COMMITTEE_DATA);
                committeeCreationResponse = (CommitteeCreateResponse) intent.getExtras().getSerializable(COMMITTE_CREATEION);
                committeeInviteRequest = intent.getExtras().getParcelable(CommitteeInviteContactsActivity.INVITEES_LIST);
                myCommitteeResponse = intent.getExtras().getParcelable(CommitteeSummaryActivityRouter.MY_COMMITTEE_RESPONSE);
                invitedCommitteePackage = intent.getExtras().getParcelable(CommitteeSummaryActivityRouter.GET_INVITED_COMMITTEE_PACKAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initClickListeners() {
        try {
            btDone.setDoubleTapSafeOnClickListener(this);
            btnJoinCommittee.setDoubleTapSafeOnClickListener(this);
            btnDeclineInvitation.setDoubleTapSafeOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected String getScreenName() {
        return "CommitteeSummaryActivity";
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.btDone:
                    if (committeeData != null)
                        // if coming from MyCommittee Screen (Info Icon)
//                        if (mUserId == mLoggedInUser)
                        finish();
//                        else
                        // leave committee
//                            committeeSummaryPresenter.leaveCommittee(String.valueOf(committeeData.getId()));
                    else {
                        CommitteeTermsandConditionActivityRouter.createInstance().setDependenciesAndRoute(this, committeeCreationResponse);
                    }
                    break;
                case R.id.btnJoinCommittee:
                    int inviteId = -1;
                    JoinCommitteeRequest joinCommitteeRequest = new JoinCommitteeRequest();
                    joinCommitteeRequest.setCommiteeId(invitedCommitteePackage.getId());
                    for (GetInvitedPackageResponse.CommitteeList.Invite invites : invitedCommitteePackage.getInvites()) {
                        if (invites.mobilNumber.equalsIgnoreCase(MDPreferenceManager.getUser().getMobileNumber())) {
                            inviteId = invites.getInviteId();
                        }
                    }
                    joinCommitteeRequest.setInviteId(/*invitedCommitteePackage.getInviteId()*/ inviteId);
                    committeeSummaryPresenter.joinCommittee(joinCommitteeRequest);
                    break;
                case R.id.btnDeclineInvitation:
                    int declineInviteId = -1;
                    for (GetInvitedPackageResponse.CommitteeList.Invite invites : invitedCommitteePackage.getInvites()) {
                        if (invites.mobilNumber.equalsIgnoreCase(MDPreferenceManager.getUser().getMobileNumber())) {
                            declineInviteId = invites.getInviteId();
                        }
                    }
                    committeeSummaryPresenter.declineCommittee(String.valueOf(/*invitedCommitteePackage.getId()*/ declineInviteId));
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showLoader() {
        showTezLoader();
    }

    @Override
    public void hideLoader() {
        dismissTezLoader();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onInviteMembers() {

    }


    @Override
    public void onLeaveCommittee(CommitteeLeaveResponse committeeLeaveResponse) {
        CommitteeActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onDeclineCommittee(CommitteeDeclineResponse committeeDeclineResponse) {
        if (committeeDeclineResponse.isResponse()) {
            CommitteeActivityRouter.createInstance().setDependenciesAndRoute(this);
        } else {
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onJoinCommittee(JoinCommitteeResponse joinCommitteeResponse) {
        CommitteeActivityRouter.createInstance().setDependenciesAndRoute(this);
//        CommitteeTermsandConditionActivityRouter.createInstance().setDependenciesAndRoute(this, joinCommitteeResponse);
        Toast.makeText(this, joinCommitteeResponse.getMessage(), Toast.LENGTH_LONG).show();
    }

}
