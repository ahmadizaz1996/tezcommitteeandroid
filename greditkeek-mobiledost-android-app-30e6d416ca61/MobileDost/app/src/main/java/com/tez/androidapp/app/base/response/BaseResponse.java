package com.tez.androidapp.app.base.response;

import java.io.Serializable;

/**
 * All network response classes should extend this
 * <p>
 * Created  on 2/9/2017.
 */

public class BaseResponse implements Serializable {

    private int statusCode;
    private String errorDescription;

    public BaseResponse() {
    }

    public BaseResponse(int statusCode, String errorDescription) {
        this.statusCode = statusCode;
        this.errorDescription = errorDescription;
    }

    public BaseResponse(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "statusCode=" + statusCode +
                ", errorDescription='" + errorDescription + '\'' +
                '}';
    }
}
