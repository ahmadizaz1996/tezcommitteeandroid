package com.tez.androidapp.rewamp.general.beneficiary.listener;

import com.tez.androidapp.rewamp.general.beneficiary.entity.Beneficiary;

import java.util.List;

public interface GetBeneficiariesListener {

    void onGetBeneficiariesSuccess(List<Beneficiary> beneficiaries);

    void onGetBeneficiariesFailure(int errorCode, String message);
}
