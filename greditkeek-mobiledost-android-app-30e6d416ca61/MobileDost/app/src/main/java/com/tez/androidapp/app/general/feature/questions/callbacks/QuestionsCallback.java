package com.tez.androidapp.app.general.feature.questions.callbacks;

import com.tez.androidapp.app.general.feature.questions.models.network.models.BimaQuestion;

import java.util.List;

/**
 * Created  on 2/17/2017.
 */

public interface QuestionsCallback {

    void onQuestionsSuccess(List<BimaQuestion> bimaQuestions);

    void onQuestionsFailure(int errorCode, String message);
}
