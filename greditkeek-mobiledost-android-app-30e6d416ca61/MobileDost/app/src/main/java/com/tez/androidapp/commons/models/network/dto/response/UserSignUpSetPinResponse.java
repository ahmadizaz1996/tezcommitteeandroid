package com.tez.androidapp.commons.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.User;

/**
 * Created  on 6/13/2017.
 */

public class UserSignUpSetPinResponse extends BaseResponse {

    Integer autoLoggedIn;
    User userDetails;

    public Integer getAutoLoggedIn() {
        return autoLoggedIn;
    }

    public User getUserDetails() {
        return userDetails;
    }
}
