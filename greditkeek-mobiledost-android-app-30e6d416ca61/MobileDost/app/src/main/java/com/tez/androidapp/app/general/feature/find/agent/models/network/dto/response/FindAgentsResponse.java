package com.tez.androidapp.app.general.feature.find.agent.models.network.dto.response;

import com.tez.androidapp.app.general.feature.find.agent.models.network.dto.request.Agent;
import com.tez.androidapp.app.base.response.BaseResponse;

import java.util.List;

/**
 * Created  on 3/7/2017.
 */

public class FindAgentsResponse extends BaseResponse {

    private List<Agent> agents;

    public List<Agent> getAgents() {
        return agents;
    }

    public void setAgents(List<Agent> agents) {
        this.agents = agents;
    }

    @Override
    public String toString() {
        return "FindAgentsResponse{" +
                "agents=" + agents +
                "} " + super.toString();
    }
}
