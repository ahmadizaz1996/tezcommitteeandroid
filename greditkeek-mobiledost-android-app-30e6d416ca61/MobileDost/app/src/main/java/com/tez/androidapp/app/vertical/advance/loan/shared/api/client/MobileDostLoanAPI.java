package com.tez.androidapp.app.vertical.advance.loan.shared.api.client;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.PersonalQuestionRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.QuestionOptionsRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.PersonalQuestionResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.QuestionOptionsResponse;
import com.tez.androidapp.app.general.feature.find.agent.models.network.dto.request.FindAgentsRequest;
import com.tez.androidapp.app.general.feature.find.agent.models.network.dto.response.FindAgentsResponse;
import com.tez.androidapp.app.general.feature.transactions.models.network.dto.request.GetLoanTransactionDetailRequest;
import com.tez.androidapp.app.general.feature.transactions.models.network.dto.response.GetLoanTransactionDetailResponse;
import com.tez.androidapp.app.general.feature.wait.models.network.dto.request.LoanStatusRequest;
import com.tez.androidapp.app.general.feature.wait.models.network.dto.response.LoanStatusResponse;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.request.CancelLoanLimitRequest;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.request.LoanLimitRequest;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.response.LoanLimitResponse;
import com.tez.androidapp.app.vertical.advance.loan.confirmation.models.network.dto.request.GenerateDisbursementReceiptRequest;
import com.tez.androidapp.app.vertical.advance.loan.confirmation.models.network.dto.response.GenerateDisbursementReceiptResponse;
import com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.request.LoanQuestionsRequest;
import com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.request.UploadDataStatusRequest;
import com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.response.LoanQuestionsResponse;
import com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.response.UploadDataStatusResponse;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.InitiateRepaymentRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.RepayLoanRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.RepayReceiptRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.InitiateRepaymentResponse;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.RepayLoanResponse;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.RepayReceiptResponse;
import com.tez.androidapp.commons.models.network.dto.request.UserFeedbackRequest;
import com.tez.androidapp.rewamp.advance.request.request.InsuranceRemainingStepsRequest;
import com.tez.androidapp.rewamp.advance.request.request.LoanApplyRequest;
import com.tez.androidapp.rewamp.advance.request.request.LoanPricingDetailsRequest;
import com.tez.androidapp.rewamp.advance.request.request.LoanRemainingStepsRequest;
import com.tez.androidapp.rewamp.advance.request.request.ProcessingFeesRequest;
import com.tez.androidapp.rewamp.advance.request.request.TenureDetailsRequest;
import com.tez.androidapp.rewamp.advance.request.response.LoanPricingDetailsResponse;
import com.tez.androidapp.rewamp.advance.request.response.LoanRemainingStepsResponse;
import com.tez.androidapp.rewamp.advance.request.response.ProcessingFeesResponse;
import com.tez.androidapp.rewamp.advance.request.response.TenureDetailsResponse;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;
import com.tez.androidapp.rewamp.general.feedback.request.LoanFeedbackRequest;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created  on 2/17/2017.
 */

public interface MobileDostLoanAPI {

    @POST(LoanQuestionsRequest.METHOD_NAME)
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    Observable<Result<LoanQuestionsResponse>> getLoanQuestions(@Header("Cookie") String sessionId,
                                                               @Body LoanQuestionsRequest loanQuestionsRequest);

    @POST(LoanQuestionsRequest.METHOD_NAME)
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    Observable<Result<LoanQuestionsResponse>> getLoanQuestions(@Header("access_token") String token);

    @GET(LoanStatusRequest.METHOD_NAME)
    Observable<Result<LoanStatusResponse>> getLoanStatus();

    @GET(UploadDataStatusRequest.METHOD_NAME)
    Observable<Result<UploadDataStatusResponse>> getUploadDataStatus();

    @POST(LoanLimitRequest.METHOD_NAME)
    Observable<Result<LoanLimitResponse>> postLoanLimit(@Body LoanLimitRequest loanLimitRequest);

    @GET(LoanLimitRequest.METHOD_NAME)
    Observable<Result<LoanLimitResponse>> getLoanLimit();

    @POST(FindAgentsRequest.METHOD_NAME)
    Observable<Result<FindAgentsResponse>> findAgents(@Body FindAgentsRequest findAgentsRequest);

    @GET(ProcessingFeesRequest.METHOD_NAME)
    Observable<Result<ProcessingFeesResponse>> getProcessingFeesDetails();

    @GET(TenureDetailsRequest.METHOD_NAME)
    Observable<Result<TenureDetailsResponse>> getTenureDetails();

    @GET(LoanPricingDetailsRequest.METHOD_NAME)
    Observable<Result<LoanPricingDetailsResponse>> getLoanPricingDetails();

    @POST(LoanApplyRequest.METHOD_NAME)
    Observable<Result<DashboardCardsResponse>> applyForLoan(@Body LoanApplyRequest loanApplyRequest);

    @POST(RepayLoanRequest.METHOD_NAME)
    Observable<Result<RepayLoanResponse>> repayLoan(@Body RepayLoanRequest repayLoanRequest);

    @POST(RepayReceiptRequest.METHOD_NAME)
    Observable<Result<RepayReceiptResponse>> getRepayReceipt(@Body RepayReceiptRequest repayReceiptRequest);

    @POST(InitiateRepaymentRequest.METHOD_NAME)
    Observable<Result<InitiateRepaymentResponse>> initiateRepayment(@Body InitiateRepaymentRequest initiateRepaymentRequest);

    @POST(UserFeedbackRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> sendUserFeedBack(@Body UserFeedbackRequest userFeedbackRequest);

    @POST(CancelLoanLimitRequest.METHOD_NAME)
    Observable<Result<DashboardCardsResponse>> cancelLoanLimit(@Body CancelLoanLimitRequest cancelLoanLimitRequest);

    @POST(GetLoanTransactionDetailRequest.METHOD_NAME)
    Observable<Result<GetLoanTransactionDetailResponse>> getLoanTransactionDetail(@Body GetLoanTransactionDetailRequest getLoanTransactionDetailRequest);

    @GET(GenerateDisbursementReceiptRequest.METHOD_NAME)
    Observable<Result<GenerateDisbursementReceiptResponse>> generateDisbursementReceipt();

    @GET(QuestionOptionsRequest.METHOD_NAME)
    Observable<Result<QuestionOptionsResponse>> getOptions(
            @Query(QuestionOptionsRequest.Params.LANG) String lang,
            @Query(QuestionOptionsRequest.Params.REF_NAME) String refName);

    @GET(PersonalQuestionRequest.METHOD_NAME)
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    Observable<Result<PersonalQuestionResponse>> getPersonalQuestions(@Header("access_token") String token, @Query(PersonalQuestionRequest.Params.LANG) String lang);

    @GET(LoanRemainingStepsRequest.METHOD_NAME)
    Observable<Result<LoanRemainingStepsResponse>> getLoanRemainingSteps();

    @POST(LoanFeedbackRequest.METHOD_NAME)
    Observable<Result<DashboardCardsResponse>> submitLoanFeedback(@Body LoanFeedbackRequest loanFeedbackRequest);
}
