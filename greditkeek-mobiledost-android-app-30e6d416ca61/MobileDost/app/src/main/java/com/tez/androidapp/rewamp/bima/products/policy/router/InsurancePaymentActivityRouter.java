package com.tez.androidapp.rewamp.bima.products.policy.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.bima.products.policy.view.InsurancePaymentActivity;

public class InsurancePaymentActivityRouter extends EasyPaisaInsurancePaymentActivityRouter {

    public static InsurancePaymentActivityRouter createInstance() {
        return new InsurancePaymentActivityRouter();
    }

    @Override
    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, InsurancePaymentActivity.class);
    }
}
