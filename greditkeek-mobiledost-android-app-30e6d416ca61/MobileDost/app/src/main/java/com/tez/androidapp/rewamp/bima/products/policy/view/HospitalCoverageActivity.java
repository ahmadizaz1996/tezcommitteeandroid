package com.tez.androidapp.rewamp.bima.products.policy.view;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;

public class HospitalCoverageActivity extends BaseProductActivity {

    @Override
    protected int getContentView() {
        return R.layout.activity_hospital_coverage;
    }

    @Override
    protected int getProductId() {
        return Constants.HOSPITAL_COVERAGE_PLAN_ID;
    }

    @Override
    protected String getScreenName() {
        return "HospitalCoverageActivity";
    }
}
