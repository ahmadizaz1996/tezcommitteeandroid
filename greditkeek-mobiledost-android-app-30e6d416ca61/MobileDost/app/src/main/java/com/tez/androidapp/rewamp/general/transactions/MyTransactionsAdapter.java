package com.tez.androidapp.rewamp.general.transactions;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.transactions.models.network.Transaction;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;

import java.util.List;

/**
 * Created by VINOD KUMAR on 7/8/2019.
 */
public class MyTransactionsAdapter extends GenericRecyclerViewAdapter<Transaction,
        MyTransactionsAdapter.MyTransactionListener,
        BaseTransactionViewHolder> {

    private static final int ADVANCE_VIEW_TYPE = 1;
    private static final int BIMA_VIEW_TYPE = 2;

    MyTransactionsAdapter(@NonNull List<Transaction> items, @NonNull MyTransactionListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public BaseTransactionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        boolean isAdvance = viewType == ADVANCE_VIEW_TYPE;
        View view = inflate(parent.getContext(), isAdvance ? R.layout.list_item_advance_transaction : R.layout.list_item_bima_transaction, parent);
        return isAdvance ? new AdvanceTransactionViewHolder(view) : new BimaTransactionViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return getItems().get(position).isAdvance() ? ADVANCE_VIEW_TYPE : BIMA_VIEW_TYPE;
    }

    public interface MyTransactionListener extends BaseRecyclerViewListener {

        void onClickMyTransaction(@NonNull Transaction myTransaction);
    }
}
