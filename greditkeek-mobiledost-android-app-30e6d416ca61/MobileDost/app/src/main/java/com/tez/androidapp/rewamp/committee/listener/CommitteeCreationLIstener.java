package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface CommitteeCreationLIstener {

    void onCommitteeCreateSuccess(CommitteeCreateResponse committeeCreateResponse);

    void onCommitteeCreateFailed(int errorCode, String message);

}
