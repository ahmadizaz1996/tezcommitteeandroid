package com.tez.androidapp.rewamp.reactivate.account.presenter;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.reactivate.account.interactor.AccountReactivateActivityInteractor;
import com.tez.androidapp.rewamp.reactivate.account.interactor.IAccountReactivateActivityInteractor;
import com.tez.androidapp.rewamp.reactivate.account.view.IAccountReactivateActivityView;

public class AccountReactivateActivityPresenter
        implements IAccountReactivateActivityPresenter,
        IAccountReactivateActivityInteractorOutput{

    private final IAccountReactivateActivityView iAccountReactivateActivityView;
    private final IAccountReactivateActivityInteractor iAccountReactivateActivityInteractor;

    public AccountReactivateActivityPresenter(IAccountReactivateActivityView iAccountReactivateActivityView) {
        this.iAccountReactivateActivityView = iAccountReactivateActivityView;
        this.iAccountReactivateActivityInteractor = new AccountReactivateActivityInteractor(this);
    }

    @Override
    public void verifyOtp(String otp){
        this.iAccountReactivateActivityView.showTezLoader();
        this.iAccountReactivateActivityInteractor.verifyAccountOtp(otp);
    }

    @Override
    public void resendOtp(){
        this.iAccountReactivateActivityView.setTvResendCodeEnabled(false);
        this.iAccountReactivateActivityView.showTezLoader();
        this.iAccountReactivateActivityInteractor.resendAccountReactivationOtp();
    }

    @Override
    public void onUserAccountReActivateResendOTPSuccess(BaseResponse baseResponse) {
        this.iAccountReactivateActivityView.setPinViewOtpClearText();
        this.iAccountReactivateActivityView.dismissTezLoader();
        this.iAccountReactivateActivityView.startTimer();
    }

    @Override
    public void onUserAccountReActivateResendOTPFailure(BaseResponse baseResponse) {
        this.iAccountReactivateActivityView.dismissTezLoader();
        this.iAccountReactivateActivityView.setPinViewOtpClearText();
        this.iAccountReactivateActivityView.showError(baseResponse.getStatusCode());
    }

    @Override
    public void onUserAccountReActivatedSuccess(BaseResponse baseResponse) {
        this.iAccountReactivateActivityView.setTezLoaderToBeDissmissedOnTransition();
        this.iAccountReactivateActivityView.navigateToDashboard();
    }

    @Override
    public void onUserAccountReActivatedFailure(BaseResponse baseResponse) {
        iAccountReactivateActivityView.dismissTezLoader();
        iAccountReactivateActivityView.setPinViewOtpClearText();
        iAccountReactivateActivityView.setPinViewOtpEnabled(true);
        iAccountReactivateActivityView.showError(baseResponse.getStatusCode());
        iAccountReactivateActivityView.registerSmsReceiver();
    }
}
