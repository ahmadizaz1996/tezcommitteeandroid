package com.tez.androidapp.rewamp.general.wallet.presenter;

public interface IChangeWalletActivityPresenter {

    void getAllWallets();
}
