package com.tez.androidapp.commons.models.extract;

import com.tez.androidapp.commons.utils.app.Utility;

import java.io.File;

/**
 * Created  on 12/8/2016.
 */

public class ExtractedFile {

    private String name;
    private String size;
    private String path;
    private String ext;
    private String lastModified;

    public ExtractedFile(String name, String size, String path, String ext, long lastModified) {
        this.name = name;
        this.size = size;
        this.path = path;
        this.ext = ext;
        this.lastModified = Utility.getFormattedDate(lastModified);
    }

    public ExtractedFile(File file) {
        String[] fileNameArray = file.getName().split("\\.");
        this.name = file.getName();
        this.size = String.valueOf(file.length());
        this.path = file.getAbsolutePath();
        this.ext = fileNameArray.length > 0 ? fileNameArray[fileNameArray.length - 1] : "";
        this.lastModified = Utility.getFormattedDate(file.lastModified());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }
}
