package com.tez.androidapp.app.vertical.bima.add.beneficiary.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.BeneficiaryDetails;

/**
 * Created  on 8/25/2017.
 */

public class AdvanceBimaBeneficiaryRequest extends BaseRequest {

    public static final String GET_METHOD_NAME = "v1/user/beneficiary/default";
    public static final String PUT_METHOD_NAME = "v1/user/beneficiary/default/{" + Params.BENEFICIARY_ID + "}";
    public static final String POST_METHOD_NAME = "v1/user/beneficiary";
    private Integer beneficiaryId;
    private Integer mobileUserInsurancePolicyBeneficiaryId;
    private Integer relationshipId;
    private String name;
    private String mobileNumber;
    private Boolean informCheck;

    public void setRelationshipId(Integer relationshipId) {
        this.relationshipId = relationshipId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber.replaceAll("-", "");
    }

    public void setInformCheck(Boolean informCheck) {
        this.informCheck = informCheck;
    }

    public BeneficiaryDetails getBeneficiaryDetails() {
        BeneficiaryDetails beneficiaryDetails = new BeneficiaryDetails();
        beneficiaryDetails.setName(name);
        beneficiaryDetails.setMobileNumber(mobileNumber);
        return beneficiaryDetails;
    }

    @Override
    public String toString() {
        return "AdvanceBimaBeneficiaryRequest{" +
                "relationshipId=" + relationshipId +
                ", name='" + name + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", informCheck=" + informCheck +
                '}';
    }

    public Integer getMobileUserInsurancePolicyBeneficiaryId() {
        return mobileUserInsurancePolicyBeneficiaryId;
    }

    public void setMobileUserInsurancePolicyBeneficiaryId(Integer mobileUserInsurancePolicyBeneficiaryId) {
        this.mobileUserInsurancePolicyBeneficiaryId = mobileUserInsurancePolicyBeneficiaryId;
    }

    public Integer getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(Integer beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    public static final class Params {

        public static final String BENEFICIARY_ID = "beneficiaryId";

        private Params() {
        }
    }
}
