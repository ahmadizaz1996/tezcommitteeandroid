package com.tez.androidapp.rewamp.dashboard.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.iid.FirebaseInstanceId;
import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.ui.activities.NavigationActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.AccessToken;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.DashboardActionCardView;
import com.tez.androidapp.commons.widgets.DashboardAdvanceCardView;
import com.tez.androidapp.commons.widgets.DashboardItemView;
import com.tez.androidapp.commons.widgets.RippleEffectImageView;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezFooterView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.AdvanceStatus;
import com.tez.androidapp.rewamp.advance.limit.router.CheckYourLimitActivityRouter;
import com.tez.androidapp.rewamp.advance.limit.router.DisbursementFailedActivityRouter;
import com.tez.androidapp.rewamp.advance.limit.router.LimitAssignedActivityRouter;
import com.tez.androidapp.rewamp.advance.limit.router.LimitDeniedActivityRouter;
import com.tez.androidapp.rewamp.advance.limit.router.LimitRejectedActivityRouter;
import com.tez.androidapp.rewamp.advance.repay.router.RepayLoanActivityRouter;
import com.tez.androidapp.rewamp.advance.request.router.AdvanceOnBoardingActivityRouter;
import com.tez.androidapp.rewamp.advance.request.router.LoanReceiptActivityRouter;
import com.tez.androidapp.rewamp.advance.request.router.SelectLoanActivityRouter;
import com.tez.androidapp.rewamp.bima.products.router.InsuranceProductsAndPolicyListActivityRouter;
import com.tez.androidapp.rewamp.committee.presenter.CommitteeActivityPresenter;
import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeMetaDataResponse;
import com.tez.androidapp.rewamp.committee.response.CommittteeLoginResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeCreationActivityRouter;
import com.tez.androidapp.rewamp.committee.view.CommitteeActivity;
import com.tez.androidapp.rewamp.committee.view.ICommitteeActivityView;
import com.tez.androidapp.rewamp.dashboard.entity.Action;
import com.tez.androidapp.rewamp.dashboard.entity.Advance;
import com.tez.androidapp.rewamp.dashboard.entity.LoanStatusDto;
import com.tez.androidapp.rewamp.dashboard.presenter.DashboardActivityPresenter;
import com.tez.androidapp.rewamp.dashboard.presenter.IDashboardActivityPresenter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.general.feedback.router.LoanFeedbackActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.router.AddWalletActivityRouter;
import com.tez.androidapp.rewamp.profile.trust.router.ProfileTrustActivityRouter;
import com.tez.androidapp.rewamp.signup.router.ContactUsActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.fragment.util.optional.Optional;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class DashboardActivity extends NavigationActivity implements ICommitteeActivityView, IDashboardActivityView, DashboardAdvanceCardView.LoanStatusListener, SwipeRefreshLayout.OnRefreshListener, DashboardActionCardView.ActionStatusListener {

    private final IDashboardActivityPresenter iDashboardActivityPresenter;
    private CommitteeMetaDataResponse committeeDataResponse;
    private final BroadcastReceiver limitPushNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = intent.getStringExtra(PushNotificationConstants.LOAN_LIMIT_STATUS);
            if (status != null && AdvanceStatus.valueOf(status) == AdvanceStatus.LIMIT_CREATED)
                LimitAssignedActivityRouter.createInstance().setDependenciesAndRoute(DashboardActivity.this);
            else
                iDashboardActivityPresenter.setDashboardFromPreference();
        }
    };

    @BindView(R.id.ivHamburger)
    private RippleEffectImageView ivHamburger;

    @BindView(R.id.ivCustomerCare)
    private RippleEffectImageView ivCustomerCare;

    @BindView(R.id.tvWelcomeName)
    private TezTextView tvWelcomeName;

    @BindView(R.id.tvProfileStatus)
    private TezTextView tvProfileStatus;

    @BindView(R.id.swipeRefreshLayout)
    private SwipeRefreshLayout swipeRefreshLayout;


    @BindView(R.id.cvDashboardAdvanceDetail)
    private DashboardAdvanceCardView cvDashboardAdvanceDetail;

    @BindView(R.id.shimmerDashboardAdvanceDetail)
    private View shimmerDashboardAdvanceDetail;

    @BindView(R.id.errorDashboardAdvanceDetail)
    private View errorDashboardAdvanceDetail;


    @BindView(R.id.cvDashboardAction)
    private DashboardActionCardView cvDashboardAction;

    @BindView(R.id.shimmerDashboardActionCard)
    private View shimmerDashboardActionCard;

    @BindView(R.id.errorDashboardActionCard)
    private View errorDashboardActionCard;

    @BindView(R.id.cvAdvanceCard)
    private DashboardItemView cvAdvanceCard;


    @BindView(R.id.clProductsContainer)
    private TezConstraintLayout clProductsContainer;

    @BindView(R.id.clProductsShimmerContainer)
    private TezConstraintLayout clProductsShimmerContainer;

    @BindView(R.id.footerView)
    private TezFooterView footerView;

    @BindView(R.id.cvCommitteeCard)

    @Nullable
    private Integer loanId;

    public DashboardActivity() {
        iDashboardActivityPresenter = new DashboardActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        openWallet(getIntent());
//        iDashboardActivityPresenter.loginCall();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        openWallet(intent);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        iDashboardActivityPresenter.setDashboardFromPreference();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initName();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PushNotificationConstants.BROADCAST_TYPE_LOAN_LIMIT);
        intentFilter.addAction(PushNotificationConstants.BROADCAST_TYPE_LOAN_DISBURSEMENT);
        intentFilter.addAction(PushNotificationConstants.BROADCAST_TYPE_LOAN_REPAYMENT);
        intentFilter.addAction(PushNotificationConstants.BROADCAST_TYPE_CNIC_VERIFICATION);
        registerReceiver(limitPushNotificationReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(limitPushNotificationReceiver);
        } catch (IllegalArgumentException e) {
            //ignore
        }
    }

    private void init() {
        initListeners();
        initSwipeRefreshLayout();
        iDashboardActivityPresenter.setDashboard();
    }

    private void initListeners() {
        this.ivHamburger.setOnClickListener(view -> openDrawer());
        this.ivCustomerCare.setDoubleTapSafeOnClickListener(view -> ContactUsActivityRouter.createInstance().setDependenciesAndRoute(this));
        this.cvDashboardAdvanceDetail.setLoanStatusListener(this);
        this.cvDashboardAction.setActionStatusListener(this);
    }

    @OnClick(R.id.cvBimaCard)
    private void routeToBimaOnBoarding() {
        InsuranceProductsAndPolicyListActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @OnClick(R.id.cvCommitteeCard)
    private void startCommitteeActivity() {
        CommitteeActivityRouter.createInstance().setDependenciesAndRoute(this);
//        iDashboardActivityPresenter.loginCall();
//        showLoader();
//        CommitteeActivityRouter.createInstance().setDependenciesAndRoute(this, null);
        /*Intent intent = new Intent(MobileDostApplication.getInstance(), CommitteeActivity.class);
        startActivity(intent);*/
    }

    private void initSwipeRefreshLayout() {
        this.swipeRefreshLayout.setEnabled(false);
        this.swipeRefreshLayout.setColorSchemeColors(Utility.getColorFromResource(R.color.textViewTextColorGreen));
        this.swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void openWallet(Intent intent) {
        if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey(DashboardActivityRouter.OPEN_WALLET)) {
            footerView.openWallet();
        }
    }

    private void initName() {

        User user = MDPreferenceManager.getUser();

        if (user != null && user.getFullName() != null)
            tvWelcomeName.setText(getString(R.string.welcome_name, user.getFullName().split(" ")[0]));
        else
            tvWelcomeName.setText(getString(R.string.welcome));
    }

    @Override
    public void onRefresh() {
        iDashboardActivityPresenter.setDashboard();
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setEnabled(false);
    }

    @Override
    public void enableRefresh() {
        swipeRefreshLayout.setEnabled(true);
    }

    @Override
    public void setDashboardAdvanceCardStatus(Advance advance) {
        this.cvDashboardAdvanceDetail.setAdvanceStatus(advance);
    }

    @Override
    public void removeCvAdvanceCardListener() {
        cvAdvanceCard.setDoubleTapSafeOnClickListener(null);
    }

    @Override
    public void setCvAdvanceCardListener(@NonNull LoanStatusDto loanStatusDto) {
        AdvanceStatus advanceStatus = AdvanceStatus.valueOf(loanStatusDto.getStatus());
        DoubleTapSafeOnClickListener listener = null;
        switch (advanceStatus) {

            case LIMIT_LOCKED:
                listener = view -> onUnlockLimit();
                break;

            case LIMIT_FAILED:
            case LIMIT_EXPIRED:
            case LIMIT_CANCELLED:
            case LIMIT_UNLOCKED:
                listener = view -> onCheckYourLimit();
                break;

            case LIMIT_REJECTED:
                listener = view -> Optional.doWhen(loanStatusDto.getLoanDetails().isTimeStampPassed(), this::onCheckYourLimit, this::onActionLimitRejected);
                break;

            case LIMIT_DENIED:
                listener = view -> Optional.doWhen(loanStatusDto.getLoanDetails().isTimeStampPassed(), this::onCheckYourLimit, this::onActionLimitDenied);
                break;

            case LIMIT_CREATED:
            case LOAN_DISBURSEMENT_FAILED:
                listener = view -> onSelectLoan();
                break;

            case LOAN_DISBURSED:
                listener = view -> onActionRepay();
                break;

            case LOAN_REPAYED:
                listener = view -> onActionFeedback();
                break;
        }

        cvAdvanceCard.setDoubleTapSafeOnClickListener(listener);
    }

    @Override
    public void setLoanIdForActionCard(@Nullable Integer loanId) {
        this.loanId = loanId;
    }

    @Override
    public void setProductsShimmerContainerVisibility(int visibility) {
        this.clProductsShimmerContainer.setVisibility(visibility);
    }

    @Override
    public void setProductsContainerVisibility(int visibility) {
        this.clProductsContainer.setVisibility(visibility);
    }

    @Override
    public void setTvProfileStatusSuccess(String text, @DrawableRes int end) {
        this.tvProfileStatus.setText(text);
        this.tvProfileStatus.changeDrawable(tvProfileStatus, 0, 0, end, 0);
    }

    @Override
    public void setTvProfileStatusError() {
        this.tvProfileStatus.setText(R.string.pull_to_refresh);
        this.tvProfileStatus.changeDrawable(tvProfileStatus, R.drawable.ic_error_dashboard, 0, 0, 0);
    }

    @Override
    public void setCvDashboardActionVisibility(int visibility) {
        this.cvDashboardAction.setVisibility(visibility);
    }

    @Override
    public void setDashboardActionState(@NonNull Action action) {
        this.cvDashboardAction.setAction(action);
        this.setCvDashboardActionVisibility(View.VISIBLE);
    }

    @Override
    public void setCvDashboardAdvanceDetailVisibility(int visibility) {
        cvDashboardAdvanceDetail.setVisibility(visibility);
    }

    @Override
    public void setShimmerDashboardAdvanceDetailVisibility(int visibility) {
        shimmerDashboardAdvanceDetail.setVisibility(visibility);
    }

    @Override
    public void setErrorDashboardAdvanceDetailVisibility(int visibility) {
        errorDashboardAdvanceDetail.setVisibility(visibility);
    }

    @Override
    public void setShimmerDashboardActionCardVisibility(int visibility) {
        shimmerDashboardActionCard.setVisibility(visibility);
    }

    @Override
    public void setErrorDashboardActionCardVisibility(int visibility) {
        errorDashboardActionCard.setVisibility(visibility);
    }

    @Override
    public void onUnlockLimit() {
        AdvanceOnBoardingActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onCheckYourLimit() {
        CheckYourLimitActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onSelectLoan() {
        SelectLoanActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onRepayLoan(int loanId) {
        if (MDPreferenceManager.getDisbursementReceiptViewed())
            RepayLoanActivityRouter.createInstance().setDependenciesAndRoute(this, loanId);
        else
            LoanReceiptActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onShareFeedback(int loanId) {
        LoanFeedbackActivityRouter.createInstance().setDependenciesAndRoute(this, loanId);
    }

    @Override
    public void onActionProfile() {
        ProfileTrustActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onActionAddWallet() {
        AddWalletActivityRouter.createInstance().setDependenciesAndRouteForResult(this);
    }

    @Override
    public void onActionLimitRejected() {
        LimitRejectedActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onActionLimitDenied() {
        LimitDeniedActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onActionLimitExpired() {
        onCheckYourLimit();
    }

    @Override
    public void onActionDisbursementFailed() {
        DisbursementFailedActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onActionRepay() {
        Optional.ifPresent(loanId, this::onRepayLoan);
    }

    @Override
    public void onActionFeedback() {
        Optional.ifPresent(loanId, this::onShareFeedback);
    }

    @Override
    protected String getScreenName() {
        return "DashboardActivity";
    }

    @Override
    public void showLoader() {
        showTezLoader();
    }

    @Override
    public void hideLoader() {
        dismissTezLoader();
    }

    @Override
    public void storeMetadata(CommitteeMetaDataResponse committeeMetaDataResponse) {
        this.committeeDataResponse = committeeMetaDataResponse;
    }

    @Override
    public void committeeLoginSuccess(CommittteeLoginResponse committteeLoginResponse) {
//        hideLoader();
//        System.out.println(committteeLoginResponse.getUserDetails().mobileNumber);
        /*AccessToken accessToken = new AccessToken();
        accessToken.setAccessToken(committteeLoginResponse.token.access_token);
        accessToken.setTokenType(committteeLoginResponse.token.token_type);
        MDPreferenceManager.setAccessToken(accessToken);*/
        //iDashboardActivityPresenter.getCommitteeMetadata();
        // cvDashboardAction.setVisibility(View.VISIBLE);
        //shimmerDashboardAdvanceDetail.setVisibility(View.VISIBLE);
        MDPreferenceManager.setAccessToken(Objects.requireNonNull(committteeLoginResponse).getToken());
        MDPreferenceManager.setPrincipalName(committteeLoginResponse.getPrincipalName());
        MDPreferenceManager.setUser(committteeLoginResponse.getUserDetails());
        MDPreferenceManager.setUserSignedUp(true);
        MDPreferenceManager.setReferralCode(committteeLoginResponse.getReferralCode());
        FirebaseInstanceId.getInstance().getInstanceId();
        CommitteeActivityRouter.createInstance().setDependenciesAndRoute(this, null);
    }

    @Override
    public void onCheckInvitesSuccess(List<CommitteeCheckInvitesResponse> committeeCheckInvitesResponse) {

    }
}
