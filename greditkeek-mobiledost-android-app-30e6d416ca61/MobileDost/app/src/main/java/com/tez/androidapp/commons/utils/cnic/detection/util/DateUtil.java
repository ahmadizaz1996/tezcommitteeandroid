package com.tez.androidapp.commons.utils.cnic.detection.util;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.utils.app.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class DateUtil {

    public static final int SECOND_MILLIS = 1000;
    public static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    public static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    public static final int DAY_MILLIS = 24 * HOUR_MILLIS;


    @SuppressLint("SimpleDateFormat")
    private static Date getFormattedDate(@NonNull String dateString, @NonNull String format) {
        try {
            return new SimpleDateFormat(format).parse(dateString);
        } catch (Exception e) {
            return null;
        }
    }

    public static String getServerFormattedDate(@NonNull String strDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(strDate);
            SimpleDateFormat sdfmonth = new SimpleDateFormat("dd/MM/yyyy");
            String monthday = sdfmonth.format(convertedDate);
            return monthday;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date getDateFromText(@NonNull String dateString) {
        List<String> possibleDateFormat = new ArrayList<>();
        possibleDateFormat.add("dd/MM/yyyy");
        possibleDateFormat.add("dd.MM.yyyy");
        possibleDateFormat.add("dd-MM-yyyy");
        for (String formatString : possibleDateFormat) {
            Date date = getFormattedDate(dateString, formatString);
            if (date != null) {
                return date;
            }
        }
        return null;
    }

    static List<Date> getSortedDatesFromListOfString(@NonNull List<String> blockData) {
        blockData = Utility.deleteDuplicatesFromList(blockData);
        List<Date> dateList = new ArrayList<>();
        for (String data : blockData) {
            Date date = getDateFromText(data);
            if (date != null) {
                dateList.add(date);
            }
        }
        Collections.sort(dateList);
        return dateList;
    }

    static List<Date> validateDateList(List<Date> dateList, int maxAgeOfUser, int maxValidDate) {
        ArrayList<Date> tempDateList = new ArrayList<>();
        for (Date date : dateList) {
            Calendar minimumCalendar = Calendar.getInstance();
            Calendar maximumCalendar = Calendar.getInstance();
            minimumCalendar.add(Calendar.YEAR, -maxAgeOfUser);
            maximumCalendar.add(Calendar.YEAR, maxValidDate);
            if (date.compareTo(minimumCalendar.getTime()) >= 0 && date.compareTo(maximumCalendar.getTime()) <= 0) {
                tempDateList.add(date);
            }
        }
        return tempDateList;
    }

    public static Calendar toCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }


    /*public static String getTimeAgo(long time, Context ctx) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = getCurrentTime(ctx);
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }*/
}
