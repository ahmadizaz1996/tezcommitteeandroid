package com.tez.androidapp.rewamp.advance.repay.interactor;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.InitiateRepaymentRequest;

public interface IVerifyJazzRepaymentActivityInteractor extends IVerifyRepaymentActivityInteractor{

    void initiateRepayment(@NonNull InitiateRepaymentRequest initiateRepaymentRequest,
                           @NonNull String pin, int mobileAccountId,
                           double amount, @Nullable Location location);
}
