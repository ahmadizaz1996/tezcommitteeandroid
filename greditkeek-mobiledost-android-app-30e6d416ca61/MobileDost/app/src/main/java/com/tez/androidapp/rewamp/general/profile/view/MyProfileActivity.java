package com.tez.androidapp.rewamp.general.profile.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.app.FacebookData;
import com.tez.androidapp.commons.utils.app.Constants;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezImageButton;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.general.profile.presenter.IMyProfileActivityPresenter;
import com.tez.androidapp.rewamp.general.profile.presenter.MyProfileActivityPresenter;
import com.tez.androidapp.rewamp.general.profile.router.BasicProfileActivityRouter;
import com.tez.androidapp.rewamp.profile.edit.router.EditProfileActivityRouter;
import com.tez.androidapp.rewamp.profile.trust.router.ProfileTrustActivityRouter;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.logger.library.utils.TextUtil;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import org.json.JSONObject;

import java.util.Arrays;

public class MyProfileActivity extends BaseActivity implements IMyProfileActivityView,
        FacebookCallback<LoginResult>, GraphRequest.GraphJSONObjectCallback{

    @BindView(R.id.tclPager)
    private TezConstraintLayout tclPager;

    @BindView(R.id.shimmer)
    private TezLinearLayout tclShimmer;

    @BindView(R.id.tvUserName)
    private TezTextView tvUserName;

    @BindView(R.id.tvMobileNo)
    private TezTextView tvMobileNumber;

    @BindView(R.id.tvDateOfBirth)
    private TezTextView tvDateOfBirth;

    @BindView(R.id.tvCnic)
    private TezTextView tvCnic;

    @BindView(R.id.btCompleteProfile)
    private TezButton btCompleteProfile;

    @BindView(R.id.tvEmailAddress)
    private TezTextView tvEmailAddress;

    @BindView(R.id.tvCurrentAddress)
    private TezTextView tvCurrentAddress;

    @BindView(R.id.tvEditProfile)
    private TezTextView tvEditProfile;

    @BindView(R.id.ivUserImage)
    private TezImageView ivUserImage;

    @BindView(R.id.btGoogle)
    private TezImageButton btGoogle;

    @BindView(R.id.btFacebook)
    private TezImageButton btFacebook;

    @BindView(R.id.facebook_tick)
    private TezImageView facebookTick;

    @BindView(R.id.google_tick)
    private TezImageView googleTick;

    private GoogleSignInClient mGoogleSignInClient;

    private final IMyProfileActivityPresenter iMyProfileActivityPresenter;

    private static final int RC_SIGN_IN = 9001;

    public MyProfileActivity() {
        iMyProfileActivityPresenter = new MyProfileActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        ViewBinder.bind(this);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadProfilePicture();
        initUserDetails();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        showTezLoader();
        init();
    }

    private void initUserDetails(){
        Optional.ifPresent(MDPreferenceManager.getUser(), user -> {
            this.setTextToTvFullName(user.getFullName());
            this.setTextToTvCnic(user.getCnic());
            this.setTextToTvMobileNumber(user.getMobileNumber());
            this.setTextToTvDateOfBirth(user.getDateOfBirth());
            this.setTextToTvEmailAddress(user.getEmail());
            this.iMyProfileActivityPresenter.setUserAddresses(user.getAddresses());
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Optional.doWhen(requestCode == RC_SIGN_IN, () -> handleGoogleSignInResult(data), () -> registeringCallBacksForFacebook(requestCode, resultCode, data));
    }

    @Override
    public void setVisibillityForTezConstraintLayoutPager(int visibillity){
        this.tclPager.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityForTezLinearLayoutShimmer(int visibillity){
        this.tclShimmer.setVisibility(visibillity);
    }

    @Override
    public void setTextToTvFullName(String text){
        this.tvUserName.setText(text==null? "":text);
    }

    @Override
    public void setListenerToBtGoogle(DoubleTapSafeOnClickListener doubleTapSafeOnClickListener){
        this.btGoogle.setDoubleTapSafeOnClickListener(doubleTapSafeOnClickListener);
    }

    @Override
    public void setListenerToBtFacebook(DoubleTapSafeOnClickListener doubleTapSafeOnClickListener){
        this.btFacebook.setDoubleTapSafeOnClickListener(doubleTapSafeOnClickListener);
    }

    @Override
    public void setTextToTvMobileNumber(String text){
        this.tvMobileNumber.setText(TextUtil.isEmpty(text)? "-":text);
    }

    @Override
    public void setTextToTvDateOfBirth(String text){
        this.tvDateOfBirth.setText(TextUtil.isEmpty(text)?"-":text);
    }

    @Override
    public void setTextToTvCnic(String text){
        this.tvCnic.setText(TextUtil.isEmpty(text)? "-":text);
    }

    @Override
    public void setVisibillityToBtCompleteProfile(int visibillity){
        this.btCompleteProfile.setVisibility(visibillity);
    }
    @Override
    public void setVisibillityToIvFacebookTick(int visibillity){
        this.facebookTick.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityToIvGoogleTick(int visibillity){
        this.googleTick.setVisibility(visibillity);
    }

    @Override
    public void setTextToTvEmailAddress(String text){
        this.tvEmailAddress.setText(TextUtil.isEmpty(text)? "-":text);
    }


    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void setTextToTvCurrentAddress(String text){
        this.tvCurrentAddress.setText(text);
    }

    @Override
    public void onClickTezImageButtonFacebook() {
        try {
            LoginManager.getInstance().logInWithReadPermissions(this,
                    Arrays.asList(Constants.KEY_USER_EMAIL,
                            Constants.KEY_USER_PUBLIC_PROFILE));
        } catch (Exception e) {
            showInformativeMessage(R.string.facebook_auth_failed);
        }
    }

    private void onClickBtGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void init(){
        initGoogleSignOn();
        initClickListner();
        this.iMyProfileActivityPresenter.getProfileDetails();
    }

    private void initGoogleSignOn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void registeringCallBacksForFacebook(int requestCode, int resultCode, @Nullable Intent intent) {
        CallbackManager callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, this);
        callbackManager.onActivityResult(requestCode, resultCode, intent);
    }

    private void handleGoogleSignInResult(Intent data) {
        Task<GoogleSignInAccount> completedTask = GoogleSignIn.getSignedInAccountFromIntent(data);
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Optional.ifPresent(account, ac -> {
                this.iMyProfileActivityPresenter.linkUserSocialAccount(ac.getId(),
                        Utility.PrincipalType.GOOGLE.getValue());
            });
        } catch (ApiException e) {
            Optional.doWhen(e.getStatusCode() != 12501, () ->
                    showInformativeMessage(R.string.google_login_error));
        }
    }

    private void loadProfilePicture() {
        Utility.loadProfilePicture(ivUserImage);
    }

    private void initClickListner(){
        this.tvEditProfile.setDoubleTapSafeOnClickListener(view -> startEditProfileActivity());
        this.btFacebook.setDoubleTapSafeOnClickListener(view -> onClickTezImageButtonFacebook());
        this.btGoogle.setDoubleTapSafeOnClickListener(view -> onClickBtGoogle());
        this.btCompleteProfile.setDoubleTapSafeOnClickListener(v -> ProfileTrustActivityRouter.createInstance().setDependenciesAndRoute(this));
        //this.tvUserName.setDoubleTapSafeOnClickListener(v-> ProfileTrustActivityRouter.createInstance().setDependenciesAndRoute(this));
    }


    private void startEditProfileActivity(){
        EditProfileActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        Utility.getDataFromFacebook(loginResult.getAccessToken(), this);
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {
        showInformativeMessage(R.string.facebook_auth_failed);
    }

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        FacebookData facebookData = Utility.extractJsonDataFromFacebook(response);
        this.iMyProfileActivityPresenter.linkUserSocialAccount(facebookData.getId(),
                Utility.PrincipalType.FACEBOOK.getValue());
    }

    @Override
    protected String getScreenName() {
        return "MyProfileActivity";
    }
}
