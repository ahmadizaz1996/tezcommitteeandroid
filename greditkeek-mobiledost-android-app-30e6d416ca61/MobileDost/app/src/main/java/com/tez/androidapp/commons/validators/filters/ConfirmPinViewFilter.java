package com.tez.androidapp.commons.validators.filters;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.View;

import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.validators.annotations.ConfirmPinViewRegex;
import com.tez.androidapp.commons.widgets.TezPinEditText;

import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;

/**
 * Created by FARHAN DHANANI on 6/4/2019.
 */
public class ConfirmPinViewFilter implements Filter<TezPinEditText, ConfirmPinViewRegex> {
    @Override
    public boolean isValidated(@NonNull TezPinEditText view, @NonNull ConfirmPinViewRegex annotation) {
        Context context = view.getContext();
        final boolean[] validated = new boolean[1];
        Optional.isInstanceOf(context, Activity.class, activity -> {
            View password = activity.findViewById(annotation.id());
            Optional.isInstanceOf(password,
                    TezPinEditText.class,
                    pass -> validated[0] = TextUtil.equals(pass.getPin(), view.getPin()));

        });
        return validated[0];
    }
}
