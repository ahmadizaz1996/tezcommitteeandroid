package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.signup.verification.SignupNumberVerificationActivity;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class SignupNumberVerificationActivityRouter extends NumberVerificationActivityRouter {

    public static final String SOCIAL_ID = "SOCIAL_ID";
    public static final String SOCIAL_TYPE = "SOCIAL_TYPE";
    public static final String USER_PIN = "USER_PIN";
    public static final String REF_CODE = "REF_CODE";

    public static SignupNumberVerificationActivityRouter createInstance() {
        return new SignupNumberVerificationActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @NonNull String mobileNumber,
                                        @NonNull String userPin,
                                        @NonNull String socialId,
                                        @NonNull Integer socialType,
                                        @Nullable String refCode) {

        Intent intent = createIntent(from);
        intent.putExtra(NumberVerificationActivityRouter.MOBILE_NUMBER, mobileNumber);
        intent.putExtra(NumberVerificationActivityRouter.PRINCIPLE, mobileNumber);
        intent.putExtra(SignupNumberVerificationActivityRouter.USER_PIN, userPin);
        intent.putExtra(SignupNumberVerificationActivityRouter.SOCIAL_ID, socialId);
        intent.putExtra(SignupNumberVerificationActivityRouter.SOCIAL_TYPE, socialType);
        intent.putExtra(REF_CODE, refCode);
        route(from, intent);
    }


    @Override
    @NonNull
    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, SignupNumberVerificationActivity.class);
    }
}
