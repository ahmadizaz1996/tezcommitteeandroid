package com.tez.androidapp.rewamp.cloud;

import com.tez.androidapp.app.vertical.advance.loan.shared.api.client.MobileDostLoanAPI;
import com.tez.androidapp.rewamp.cloud.client.AdvanceClient;
import com.tez.androidapp.rewamp.cloud.client.AuthClientInterceptor;
import com.tez.androidapp.rewamp.cloud.client.BaseClient;
import com.tez.androidapp.rewamp.cloud.client.EncryptionClientInterceptor;

public class AdvanceCloudDataStore {
    private MobileDostLoanAPI mobileDostLoanAPI;

    private AdvanceCloudDataStore(){
        mobileDostLoanAPI = (MobileDostLoanAPI) getClient().build();
    }

    private BaseClient getClient(){
        AdvanceClient advanceClient =  new AdvanceClient();
        AuthClientInterceptor authClientInterceptor = new AuthClientInterceptor(advanceClient);
        return new EncryptionClientInterceptor(authClientInterceptor);
    }
}
