package com.tez.androidapp.app.general.feature.inivite.user.callback;

/**
 * Created by FARHAN DHANANI on 9/6/2018.
 */
public interface GetReferralCodeCallback {

    void onGetReferralCodeSuccess(String referralCode);

    void onGetReferralCodeFailure(int errorCode, String message);
}
