package com.tez.androidapp.commons.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.commons.utils.app.Utility;

import net.tez.fragment.util.optional.Optional;
import net.tez.validator.library.utils.TextUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Receiver to automatically receive sms and fill input fields.
 * <p>
 * Created  on 12/20/2016.
 */

public class SMSReceiver extends BroadcastReceiver {

    private static final String MSG_SENDER = "8184";

    private static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";

    @NonNull
    private OnSmsReceived onSmsReceived;

    public SMSReceiver(@NonNull OnSmsReceived onSmsReceived) {
        this.onSmsReceived = onSmsReceived;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction() != null && intent.getAction().equals(SMS_RECEIVED_ACTION)) {
            try {
                onRightAction(intent);
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        }
    }

    public void register(Context context) {
        context.registerReceiver(this, new IntentFilter(SMS_RECEIVED_ACTION));
    }

    public void unregister(Context context) {
        try {
            context.unregisterReceiver(this);
        } catch (IllegalArgumentException ignore) {
            //ignore
        }
    }

    private void onRightAction(Intent intent) {
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            SmsMessage[] messages = getMessages((Object[]) bundle.get("pdus"), intent);

            Optional.ifPresent(messages, this::extractMessageAndSend);
        }
    }

    private void extractMessageAndSend(@NonNull SmsMessage[] messages) {
        String messageFrom;
        StringBuilder messageText = new StringBuilder();
        for (SmsMessage message : messages) {

            messageFrom = message.getOriginatingAddress();

            if (messageFrom != null && messageFrom.equals(MSG_SENDER)) {
                messageText.append(message.getMessageBody());
            }
        }
        if (TextUtil.isNotEmpty(messageText.toString())) {
            getMessageType(messageText.toString());
        }
    }

    private SmsMessage[] getMessages(Object[] pdus, Intent intent) {
        SmsMessage[] messages = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            messages = Telephony.Sms.Intents.getMessagesFromIntent(intent);

        else if (pdus != null) {
            messages = new SmsMessage[pdus.length];
            for (int i = 0; i < messages.length; i++)
                messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
        }

        return messages;
    }

    private void getMessageType(String messageBody) {
        extractMessageCode(messageBody, Pattern.compile("OTP: ((\\b(\\d{6})\\b)|(\\b(\\d{4})\\b))", Pattern.CASE_INSENSITIVE));
    }

    private boolean isTypeOfMsg(String text, @StringRes int regex){
        return TextUtil.isValidWithRegex(text, Utility.getStringFromResource(regex));
    }

    private void extractMessageCode(String messageBody, Pattern pattern) {
        Matcher matcher = pattern.matcher(messageBody);
        if (matcher.find()) {
            this.onSmsReceived.onSmsReceived(matcher.group(1));
        }
    }

    @FunctionalInterface
    public interface OnSmsReceived {

        void onSmsReceived(String sms);
    }
}
