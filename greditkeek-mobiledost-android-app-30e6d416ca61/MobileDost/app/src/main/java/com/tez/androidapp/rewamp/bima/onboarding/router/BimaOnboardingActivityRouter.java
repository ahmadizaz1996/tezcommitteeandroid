package com.tez.androidapp.rewamp.bima.onboarding.router;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.bima.onboarding.view.BimaOnboardingActivity;

public class BimaOnboardingActivityRouter extends BaseActivityRouter {

    public static BimaOnboardingActivityRouter createInstance() {
        return new BimaOnboardingActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, BimaOnboardingActivity.class);
    }
}
