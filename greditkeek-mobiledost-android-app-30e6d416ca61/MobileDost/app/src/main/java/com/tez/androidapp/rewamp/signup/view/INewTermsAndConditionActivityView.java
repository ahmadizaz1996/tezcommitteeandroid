package com.tez.androidapp.rewamp.signup.view;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;

public interface INewTermsAndConditionActivityView extends IBaseView {

    void navigateToDashBoardActivity();

    void startLogin();

    UserLoginRequest getLoginrequest(String pin);
}
