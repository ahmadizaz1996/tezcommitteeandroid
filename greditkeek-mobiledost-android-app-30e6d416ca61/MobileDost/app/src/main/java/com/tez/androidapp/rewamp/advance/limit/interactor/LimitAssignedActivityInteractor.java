package com.tez.androidapp.rewamp.advance.limit.interactor;

import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.response.LoanLimitResponse;
import com.tez.androidapp.app.vertical.advance.loan.shared.limit.callbacks.LoanLimitCallback;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.advance.limit.presenter.ILimitAssignedActivityInteractorOutput;

public class LimitAssignedActivityInteractor implements ILimitAssignedActivityInteractor {

    private final ILimitAssignedActivityInteractorOutput iLimitAssignedActivityInteractorOutput;

    public LimitAssignedActivityInteractor(ILimitAssignedActivityInteractorOutput iLimitAssignedActivityInteractorOutput) {
        this.iLimitAssignedActivityInteractorOutput = iLimitAssignedActivityInteractorOutput;
    }

    @Override
    public void getLoanLimit() {
        LoanCloudDataStore.getInstance().getLoanLimit(new LoanLimitCallback() {
            @Override
            public void onLoanLimitSuccess(LoanLimitResponse loanLimitResponse) {
                iLimitAssignedActivityInteractorOutput.onLoanLimitSuccess(loanLimitResponse);
            }

            @Override
            public void onLoanLimitFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getLoanLimit();
                else
                    iLimitAssignedActivityInteractorOutput.onLoanLimitFailure(errorCode, message);
            }
        });
    }
}
