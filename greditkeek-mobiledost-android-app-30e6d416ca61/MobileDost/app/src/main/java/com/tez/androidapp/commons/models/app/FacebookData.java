package com.tez.androidapp.commons.models.app;

import com.facebook.Profile;

import java.io.Serializable;

/**
 * Created  on 1/15/2017.
 */

public class FacebookData implements Serializable {

    private String id;
    private String birthday;
    private String timezone;
    private String email;
    private String link;
    private String name;
    private String first_name;
    private String last_name;
    private String gender;
    private String updated_time;
    private String minAgeRange;
    private String maxAgeRange;
    private String imageUrl;
    private boolean isDefaultImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUpdatedTime() {
        return updated_time;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updated_time = updatedTime;
    }

    public String getMinAgeRange() {
        return minAgeRange;
    }

    public void setMinAgeRange(String minAgeRange) {
        this.minAgeRange = minAgeRange;
    }

    public String getMaxAgeRange() {
        return maxAgeRange;
    }

    public void setMaxAgeRange(String maxAgeRange) {
        this.maxAgeRange = maxAgeRange;
    }

    public String getImageUrl() {
        if (Profile.getCurrentProfile() == null)
            return imageUrl;
        else return Profile.getCurrentProfile().getProfilePictureUri(800, 800).toString();
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isDefaultImage() {
        return isDefaultImage;
    }

    public void setDefaultImage(boolean defaultImage) {
        isDefaultImage = defaultImage;
    }

    public String getFirstName() {
        return first_name;
    }

    public void setFirstName(String firstName) {
        this.first_name = firstName;
    }

    public String getLastName() {
        return last_name;
    }

    public void setLastName(String lastName) {
        this.last_name = lastName;
    }
}
