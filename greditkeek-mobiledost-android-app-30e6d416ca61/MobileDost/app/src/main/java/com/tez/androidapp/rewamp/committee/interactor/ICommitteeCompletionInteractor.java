package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;

import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeCompletionInteractor extends IBaseInteractor {
    void createCommittee(CommitteeCreateRequest committeeInviteRequest);
}
