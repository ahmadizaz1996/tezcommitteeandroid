package com.tez.androidapp.app.vertical.advance.loan.apply.callbacks;

import com.tez.androidapp.app.vertical.advance.loan.apply.models.network.ProcessingFee;

import java.util.List;

public interface ProcessingFeesCallback {

    void onProcessingFeesSuccess(List<ProcessingFee> processingFees);

    void onProcessingFeesFailure(int errorCode, String message);
}
