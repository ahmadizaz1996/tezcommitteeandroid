package com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created  on 8/25/2017.
 */

public class SetAdvanceBimaBeneficiaryResponse extends BaseResponse {
    @Override
    public String toString() {
        return "SetAdvanceBimaBeneficiaryResponse{} " + super.toString();
    }
}
