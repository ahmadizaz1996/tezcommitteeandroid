package com.tez.androidapp.rewamp.bima.products.policy.presenter;

import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InsuranceProductDetailCallback;

public interface IPurchasePolicyActivityInteractorOutput extends InsuranceProductDetailCallback {
}
