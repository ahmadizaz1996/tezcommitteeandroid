package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;
import android.os.Bundle;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeResponse;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeCompletedPopupActivity;
import com.tez.androidapp.rewamp.committee.view.CommitteeCompletionActivity;
import com.tez.androidapp.rewamp.committee.view.CommitteeSummaryActivity;
import com.tez.androidapp.rewamp.committee.view.CommitteeTermsandConditionActivity;

import androidx.annotation.NonNull;

import static com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter.COMMITTEE_DATA;

public class CommitteeTermsandConditionActivityRouter extends BaseActivityRouter {


    public static CommitteeTermsandConditionActivityRouter createInstance() {
        return new CommitteeTermsandConditionActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteePackageModel committeePackageModel) {
        Intent intent = createIntent(from);
        intent.putExtra(COMMITTEE_DATA, committeePackageModel);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteeCreateResponse committeeCreateResponse) {
        Intent intent = createIntent(from);
        intent.putExtra(CommitteeSummaryActivityRouter.COMMITTE_CREATEION, committeeCreateResponse);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, MyCommitteeResponse myCommitteeResponse) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putParcelable(CommitteeSummaryActivityRouter.MY_COMMITTEE_RESPONSE, myCommitteeResponse);
        intent.putExtras(bundle);
        route(from, intent);
    }


    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeTermsandConditionActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }


    public void setDependenciesAndRoute(BaseActivity from, JoinCommitteeResponse joinCommitteeResponse) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putParcelable(CommitteeSummaryActivityRouter.JOIN_COMMITTEE_RESPONSE, joinCommitteeResponse);
        intent.putExtras(bundle);
        route(from, intent);
    }
}
