package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.advance.AdvanceStatus;
import com.tez.androidapp.rewamp.dashboard.entity.Advance;
import com.tez.androidapp.rewamp.signup.DashboardState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VINOD KUMAR on 6/13/2019.
 */
public class DashboardAdvanceCardView extends TezCardView {

    /*
        CardView Style
     */

    private static final int BUTTON = 0;
    private static final int DATE = 1;
    private static final int STATUS = 2;

    private static final int AMOUNT_ASSIGNED = 0;
    private static final int TEZ_ADVANCE = 1;

    private TezTextView tvAmount;
    private TezTextView tvMessage;
    private TezTextView tvDaysRemaining;
    private TezTextView tvDate;
    private TezTextView tvDueDate;
    private TezButton btAction;
    private TezTextView tvStatusValue;
    private TezLinearLayout layoutStatus;
    private TezConstraintLayout layoutDate;
    private TezConstraintLayout layoutAmountAssigned;
    private TezConstraintLayout layoutTezAdvance;
    private DashboardStateLineView stateLineView;
    private TezLinearLayout layoutDaysRemaining;

    private LoanStatusListener loanStatusListener;

    public DashboardAdvanceCardView(@NonNull Context context) {
        super(context);
    }

    public DashboardAdvanceCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public DashboardAdvanceCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    public void setLoanStatusListener(LoanStatusListener loanStatusListener) {
        this.loanStatusListener = loanStatusListener;
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        inflate(context, R.layout.card_view_dashboard_advance, this);
        initViews();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.DashboardAdvanceCardView);
        try {
            int layoutAction = typedArray.getInt(R.styleable.DashboardAdvanceCardView_layout_action, 0);
            setLayoutActionState(layoutAction);


        } finally {
            typedArray.recycle();
        }
    }

    private void setLayoutActionState(int layoutAction) {
        btAction.setDoubleTapSafeOnClickListener(null);
        btAction.setVisibility(layoutAction == 0 ? VISIBLE : GONE);
        tvDueDate.setVisibility(layoutAction == 1 ? VISIBLE : GONE);
        tvDate.setVisibility(layoutAction == 1 ? VISIBLE : GONE);
        layoutDate.setVisibility(layoutAction == 1 ? VISIBLE : GONE);
        layoutStatus.setVisibility(layoutAction == 2 ? VISIBLE : GONE);
        tvDate.setTextColor(getColor(getContext(), R.color.textViewTextColorGreen));
    }

    private void setLayoutAmount(int layoutAmount) {
        layoutAmountAssigned.setVisibility(layoutAmount == 0 ? VISIBLE : GONE);
        layoutTezAdvance.setVisibility(layoutAmount == 1 ? VISIBLE : GONE);
    }

    private void initViews() {
        btAction = findViewById(R.id.btAction);
        layoutDate = findViewById(R.id.layoutDate);
        layoutStatus = findViewById(R.id.layoutStatus);
        tvStatusValue = findViewById(R.id.tvStatusValue);
        layoutAmountAssigned = findViewById(R.id.layoutAmountAssigned);
        layoutTezAdvance = findViewById(R.id.layoutTezAdvance);
        tvAmount = findViewById(R.id.tvAmount);
        tvMessage = findViewById(R.id.tvMessage);
        tvDaysRemaining = findViewById(R.id.tvDaysRemaining);
        tvDate = findViewById(R.id.tvDate);
        tvDueDate = findViewById(R.id.tvDueDate);
        stateLineView = findViewById(R.id.dashboardStateLine);
        layoutDaysRemaining = findViewById(R.id.layoutDaysRemaining);
    }

    public void setAdvanceStatus(Advance advance) {

        AdvanceStatus advanceStatus = AdvanceStatus.valueOf(advance.getLoanStatusDto().getStatus());

        switch (advanceStatus) {

            case LIMIT_LOCKED:
                limitLocked();
                break;

            case LIMIT_UNLOCKED:
                limitUnlocked();
                break;

            case LIMIT_REJECTED:
                limitRejected();
                break;

            case LIMIT_DENIED:
                limitDenied();
                break;

            case LIMIT_PENDING:
                limitPending();
                break;

            case LIMIT_CANCELLED:
                limitCancel();
                break;

            case LIMIT_CREATED:
                limitAssigned(advance);
                break;

            case LIMIT_FAILED:
            case LIMIT_EXPIRED:
                limitExpire();
                break;

            case LOAN_APPLIED:
                loanApplied();
                break;

            case LOAN_DISBURSEMENT_PENDING:
                disbursementPending();
                break;

            case LOAN_DISBURSEMENT_FAILED:
                disbursementFailed();
                break;

            case LOAN_DISBURSED:
                disbursementSuccessful(advance);
                break;

            case LOAN_REPAYED:
                repaymentDone(advance);
                break;
        }
    }

    private void setAmountText(double amount) {
        String formattedAmount = getResources().getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabel(amount));
        tvAmount.setText(formattedAmount);
    }

    private String getString(@StringRes int resId) {
        return getResources().getString(resId);
    }

    private void limitLocked() {
        setLayoutActionState(BUTTON);
        setLayoutAmount(TEZ_ADVANCE);
        btAction.setText(R.string.unlock_limit);
        btAction.setDoubleTapSafeOnClickListener(view -> loanStatusListener.onUnlockLimit());
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.unlock_limit), DashboardState.State.UNLOCKED));
        stateList.add(new DashboardState(getString(R.string.apply_limit), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.select_loan), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.loan_disbursed), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.repay_loan), DashboardState.State.LOCKED));
        stateLineView.setStateList(stateList);
    }

    private void limitUnlocked() {
        setLayoutActionState(BUTTON);
        setLayoutAmount(TEZ_ADVANCE);
        btAction.setText(R.string.check_your_limit);
        btAction.setDoubleTapSafeOnClickListener(view -> loanStatusListener.onCheckYourLimit());
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.apply_limit), DashboardState.State.UNLOCKED));
        stateList.add(new DashboardState(getString(R.string.select_loan), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.loan_disbursed), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.repay_loan), DashboardState.State.LOCKED));
        stateLineView.setStateList(stateList);
    }

    private void limitRejected() {
        setLayoutActionState(STATUS);
        setLayoutAmount(TEZ_ADVANCE);
        tvStatusValue.setText(R.string.limit_rejected);
        tvStatusValue.setTextColor(getColor(getContext(), R.color.stateRejectedColorRed));
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.limit_rejected), DashboardState.State.REJECTED));
        stateList.add(new DashboardState(getString(R.string.apply_limit), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.select_loan), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.loan_disbursed), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.repay_loan), DashboardState.State.LOCKED));
        stateLineView.setStateList(stateList);
    }

    private void limitDenied() {
        setLayoutActionState(STATUS);
        setLayoutAmount(TEZ_ADVANCE);
        tvStatusValue.setText(R.string.limit_denied);
        tvStatusValue.setTextColor(getColor(getContext(), R.color.stateRejectedColorRed));
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.limit_denied), DashboardState.State.REJECTED));
        stateList.add(new DashboardState(getString(R.string.apply_limit), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.select_loan), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.loan_disbursed), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.repay_loan), DashboardState.State.LOCKED));
        stateLineView.setStateList(stateList);
    }

    private void limitPending() {
        setLayoutActionState(STATUS);
        setLayoutAmount(TEZ_ADVANCE);
        tvStatusValue.setText(R.string.limit_pending);
        tvStatusValue.setTextColor(getColor(getContext(), R.color.textViewTextColorGreen));
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.apply_limit), DashboardState.State.PASSED));
        stateList.add(new DashboardState(getString(R.string.select_loan), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.loan_disbursed), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.repay_loan), DashboardState.State.LOCKED));
        stateLineView.setStateList(stateList);
    }

    private void limitCancel() {
        setLayoutActionState(BUTTON);
        setLayoutAmount(TEZ_ADVANCE);
        btAction.setText(R.string.check_your_limit);
        btAction.setDoubleTapSafeOnClickListener(view -> loanStatusListener.onCheckYourLimit());
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.apply_limit), DashboardState.State.UNLOCKED));
        stateList.add(new DashboardState(getString(R.string.select_loan), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.loan_disbursed), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.repay_loan), DashboardState.State.LOCKED));
        stateLineView.setStateList(stateList);
    }

    private void limitAssigned(Advance advance) {
        double amount = advance.getLoanStatusDto().getLoanDetails().getLimit();
        setLayoutActionState(BUTTON);
        setLayoutAmount(AMOUNT_ASSIGNED);
        setAmountText(amount);
        tvMessage.setText(R.string.advance_limit_assigned);
        btAction.setText(R.string.select_loan);
        btAction.setDoubleTapSafeOnClickListener(view -> loanStatusListener.onSelectLoan());
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.apply_limit), DashboardState.State.PASSED));
        stateList.add(new DashboardState(getString(R.string.select_loan), DashboardState.State.UNLOCKED));
        stateList.add(new DashboardState(getString(R.string.loan_disbursed), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.repay_loan), DashboardState.State.LOCKED));
        stateLineView.setStateList(stateList);
    }

    private void limitExpire() {
        setLayoutActionState(BUTTON);
        setLayoutAmount(TEZ_ADVANCE);
        btAction.setText(R.string.check_your_limit);
        btAction.setDoubleTapSafeOnClickListener(view -> loanStatusListener.onCheckYourLimit());
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.apply_limit), DashboardState.State.UNLOCKED));
        stateList.add(new DashboardState(getString(R.string.select_loan), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.loan_disbursed), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.repay_loan), DashboardState.State.LOCKED));
        stateLineView.setStateList(stateList);
    }

    private void loanApplied() {
        setLayoutActionState(STATUS);
        setLayoutAmount(TEZ_ADVANCE);
        tvStatusValue.setText(R.string.waiting_for_approval);
        tvStatusValue.setTextColor(getColor(getContext(), R.color.textViewTextColorGreen));
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.apply_limit), DashboardState.State.PASSED));
        stateList.add(new DashboardState(getString(R.string.select_loan), DashboardState.State.PASSED));
        stateList.add(new DashboardState(getString(R.string.loan_disbursed), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.repay_loan), DashboardState.State.LOCKED));
        stateLineView.setStateList(stateList);
    }

    private void disbursementPending() {
        setLayoutActionState(STATUS);
        setLayoutAmount(TEZ_ADVANCE);
        tvStatusValue.setText(R.string.disbursement_pending);
        tvStatusValue.setTextColor(getColor(getContext(), R.color.textViewTextColorGreen));
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.disbursed), DashboardState.State.PASSED));
        stateList.add(new DashboardState(getString(R.string.repay), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.feedback), DashboardState.State.LOCKED));
        stateLineView.setStateList(stateList);
    }

    private void disbursementFailed() {
        setLayoutActionState(STATUS);
        setLayoutAmount(TEZ_ADVANCE);
        tvStatusValue.setText(R.string.disbursement_failed);
        tvStatusValue.setTextColor(getColor(getContext(), R.color.stateRejectedColorRed));
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.disbursed), DashboardState.State.REJECTED));
        stateList.add(new DashboardState(getString(R.string.repay), DashboardState.State.LOCKED));
        stateList.add(new DashboardState(getString(R.string.feedback), DashboardState.State.LOCKED));
        stateLineView.setStateList(stateList);
    }

    private void disbursementSuccessful(Advance advance) {
        LoanDetails loanDetails = advance.getLoanStatusDto().getLoanDetails();
        double amount = loanDetails.getAmountToReturn();
        setLayoutActionState(DATE);
        setLayoutAmount(AMOUNT_ASSIGNED);
        setAmountText(amount);

        tvMessage.setText(R.string.outstanding_amount);
        String dueDate = loanDetails.getDueDateFormatted();
        tvDate.setText(dueDate);


        DashboardState.State repayState = DashboardState.State.UNLOCKED;

        if (loanDetails.isDueDateToday()) {
            onDueDateIsToday();
            btAction.setDoubleTapSafeOnClickListener(view -> loanStatusListener.onRepayLoan(advance.getLoanStatusDto().getLoanId()));
        } else if (loanDetails.isDueDatePassed()) {
            repayState = DashboardState.State.REJECTED;
            onDueDateIsPassed();
            btAction.setDoubleTapSafeOnClickListener(view -> loanStatusListener.onRepayLoan(advance.getLoanStatusDto().getLoanId()));
        } else {
            String daysRemaining = loanDetails.getDaysRemaining();
            layoutDaysRemaining.setVisibility(VISIBLE);
            tvDaysRemaining.setText(daysRemaining);
        }
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.disbursed), DashboardState.State.PASSED));
        stateList.add(new DashboardState(getString(R.string.repay), repayState));
        stateList.add(new DashboardState(getString(R.string.feedback), DashboardState.State.LOCKED));
        stateLineView.setStateList(stateList);
    }

    private void onDueDateIsToday() {
        layoutDaysRemaining.setVisibility(GONE);
        btAction.setVisibility(VISIBLE);
        btAction.setText(R.string.pay_now);
        tvDate.setText(R.string.today);
    }

    private void onDueDateIsPassed() {
        layoutDaysRemaining.setVisibility(GONE);
        btAction.setVisibility(VISIBLE);
        btAction.setText(R.string.pay_now);
        tvDate.setTextColor(getColor(getContext(), R.color.stateRejectedColorRed));
    }

    private void repaymentDone(Advance advance) {
        setLayoutActionState(BUTTON);
        setLayoutAmount(TEZ_ADVANCE);
        btAction.setText(R.string.share_feedback);
        btAction.setDoubleTapSafeOnClickListener(v -> loanStatusListener.onShareFeedback(advance.getLoanStatusDto().getLoanId()));
        List<DashboardState> stateList = new ArrayList<>();
        stateList.add(new DashboardState(getString(R.string.disbursed), DashboardState.State.PASSED));
        stateList.add(new DashboardState(getString(R.string.repay), DashboardState.State.PASSED));
        stateList.add(new DashboardState(getString(R.string.feedback), DashboardState.State.UNLOCKED));
        stateLineView.setStateList(stateList);
    }

    public interface LoanStatusListener {

        void onUnlockLimit();

        void onCheckYourLimit();

        void onSelectLoan();

        void onRepayLoan(int loanId);

        void onShareFeedback(int loanId);
    }
}
