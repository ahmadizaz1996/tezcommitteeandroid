package com.tez.androidapp.rewamp.bima.products.policy.response;

import androidx.annotation.Nullable;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;

import java.util.List;

public class InsuranceProductDetails {

    private List<InsuranceProductPlan> productPlans;
    @Nullable
    private String description;
    private double minCoverage;
    @Nullable
    private Integer activationDays;
    @Nullable
    private Wallet defaultWallet;
    private String issueDate;
    private double maxCoverage;
    private String insuranceProvider;

    public List<InsuranceProductPlan> getProductPlans() {
        return productPlans;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public double getMinCoverage() {
        return minCoverage;
    }

    public double getMaxCoverage() {
        return maxCoverage;
    }

    @Nullable
    public Integer getActivationDays() {
        return activationDays;
    }

    @Nullable
    public Wallet getDefaultWallet() {
        return defaultWallet;
    }

    public String getIssueDate() {
        return this.issueDate;
    }

    public String getInsuranceProvider() {
        return insuranceProvider;
    }
}
