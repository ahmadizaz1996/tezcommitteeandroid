package com.tez.androidapp.commons.location.models;

import com.tez.androidapp.commons.utils.app.Constants;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Rehman Murad Ali on 5/30/2018.
 */
public class Location {
    String dateTime;
    Double longitude;
    Double latitude;

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Location(Double latitude, Double longitude) {
        this.longitude = longitude;
        this.latitude = latitude;
        SimpleDateFormat formatter = new SimpleDateFormat(Constants.INPUT_DATE_FORMAT);
        Date date = new Date();
        this.dateTime = formatter.format(date);
    }

    @Override
    public String toString() {
        return "Location{" +
                "dateTime='" + dateTime + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}
