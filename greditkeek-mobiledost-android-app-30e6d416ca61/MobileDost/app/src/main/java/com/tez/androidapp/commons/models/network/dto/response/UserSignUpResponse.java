package com.tez.androidapp.commons.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.User;

/**
 * Created  on 2/13/2017.
 */

public class UserSignUpResponse extends BaseResponse {

    Integer autoLoggedIn;
    User userDetails;

    public Integer getAutoLoggedIn() {
        return autoLoggedIn;
    }

    public void setAutoLoggedIn(Integer autoLoggedIn) {
        this.autoLoggedIn = autoLoggedIn;
    }

    public User getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(User userDetails) {
        this.userDetails = userDetails;
    }

    @Override
    public String toString() {
        return "UserSignUpResponse{" +
                "autoLoggedIn=" + autoLoggedIn +
                ", userDetails=" + userDetails +
                "} " + super.toString();
    }
}
