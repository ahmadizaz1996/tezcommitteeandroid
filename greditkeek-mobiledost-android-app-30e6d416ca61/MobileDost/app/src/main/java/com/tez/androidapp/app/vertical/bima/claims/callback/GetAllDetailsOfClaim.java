package com.tez.androidapp.app.vertical.bima.claims.callback;

import com.tez.androidapp.app.general.feature.questions.models.network.models.BimaQuestion;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.ClaimDetailDto;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.ExtentionSize;

import java.util.List;

/**
 * Created by VINOD KUMAR on 12/4/2018.
 */
public interface GetAllDetailsOfClaim {

    void onGetAllDetailsOfClaimSuccess(List<ExtentionSize> extentionSizes,
                                       ClaimDetailDto claimDetailDto);

    void onGetAllDetailsOfClaimFailure(int errorCode, String message);
}
