package com.tez.androidapp.app.general.feature.transactions.models.network;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.models.network.RepaymentTransaction;
import com.tez.androidapp.commons.utils.app.Utility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created  on 9/2/2017.
 */

public class TransactionDetail implements Serializable {

    private Double remainingAmount;
    private Double latePaymentCharges;
    private List<RepaymentTransaction> repaymentSummary;
    private Wallet wallet;

    public Wallet getWallet() {
        return wallet;
    }

    public Double getRemainingAmount() {
        return remainingAmount == null ? 0 : remainingAmount;
    }

    public String getRemainingAmountString() {
        return Utility.getFormattedAmountWithoutCurrencyLabel(getRemainingAmount());
    }

    public List<RepaymentTransaction> getRepaymentSummary() {
        return repaymentSummary == null ? new ArrayList<>() : repaymentSummary;
    }

    public void setRemainingAmount(Double remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public Double getLatePaymentCharges() {
        return latePaymentCharges;
    }

    public void setLatePaymentCharges(Double latePaymentCharges) {
        this.latePaymentCharges = latePaymentCharges;
    }

    public void setRepaymentSummary(List<RepaymentTransaction> repaymentSummary) {
        this.repaymentSummary = repaymentSummary;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }
}
