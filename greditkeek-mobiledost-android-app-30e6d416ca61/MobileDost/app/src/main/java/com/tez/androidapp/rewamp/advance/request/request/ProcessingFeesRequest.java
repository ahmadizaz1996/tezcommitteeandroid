package com.tez.androidapp.rewamp.advance.request.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class ProcessingFeesRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/loan/processingFees";

}
