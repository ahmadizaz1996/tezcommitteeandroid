package com.tez.androidapp.rewamp.advance.request.presenter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.response.LoanLimitResponse;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.advance.request.entity.LoanDetail;
import com.tez.androidapp.rewamp.advance.request.entity.TenureDetail;
import com.tez.androidapp.rewamp.advance.request.interactor.ISelectLoanActivityInteractor;
import com.tez.androidapp.rewamp.advance.request.interactor.SelectLoanActivityInteractor;
import com.tez.androidapp.rewamp.advance.request.model.LoanDuration;
import com.tez.androidapp.rewamp.advance.request.request.LoanApplyRequest;
import com.tez.androidapp.rewamp.advance.request.view.ISelectLoanActivityView;

import java.util.ArrayList;
import java.util.List;

public class SelectLoanActivityPresenter implements ISelectLoanActivityPresenter, ISelectLoanActivityInteractorOutput {

    private static final double MIN_LOAN_AMOUNT = 1000;

    private final ISelectLoanActivityView iSelectLoanActivityView;
    private final ISelectLoanActivityInteractor iSelectLoanActivityInteractor;
    private final LoanApplyRequest loanApplyRequest;
    private double maxLoanLimit;
    private LoanDetail loanDetail;

    public SelectLoanActivityPresenter(ISelectLoanActivityView iSelectLoanActivityView) {
        this.iSelectLoanActivityView = iSelectLoanActivityView;
        this.iSelectLoanActivityInteractor = new SelectLoanActivityInteractor(this);
        this.loanApplyRequest = new LoanApplyRequest();
        this.loanApplyRequest.setDeviceInfo(Utility.getUserDeviceInfo());
    }

    @Override
    public void getLoanLimit() {
        iSelectLoanActivityView.setClContentVisibility(View.GONE);
        iSelectLoanActivityView.showTezLoader();
        iSelectLoanActivityInteractor.getLoanLimit();
    }

    @Override
    public void onLoanLimitSuccess(LoanLimitResponse loanLimitResponse) {
        this.maxLoanLimit = loanLimitResponse.getLoanLimit();
        loanApplyRequest.setLoanId(loanLimitResponse.getLoanId());
        iSelectLoanActivityView.setMaxLoanLimit(loanLimitResponse.getLoanLimit());
        iSelectLoanActivityView.setUserWallet(loanLimitResponse.getWallet());
        iSelectLoanActivityView.setTvCancelMyLimitOnClickListener(loanLimitResponse.getLoanId());
        if (loanDetail == null)
            iSelectLoanActivityInteractor.getLoanDetails();
        else {
            iSelectLoanActivityView.initCalculateAmount();
            iSelectLoanActivityView.dismissTezLoader();
            iSelectLoanActivityView.setClContentVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setMobileAccountId(@Nullable Wallet wallet) {
        this.loanApplyRequest.setMobileAccountId(wallet == null ? null : wallet.getMobileAccountId());
        this.loanApplyRequest.setWalletServiceProviderId(wallet == null ? null : wallet.getServiceProviderId());
    }

    @Override
    public void onLoanLimitFailure(int errorCode, String message) {
        iSelectLoanActivityView.dismissTezLoader();
        iSelectLoanActivityView.showError(errorCode, (dialog, which) -> iSelectLoanActivityView.finishActivity());
    }

    @Override
    public void onGetLoanDetailsSuccess(LoanDetail loanDetail) {
        this.loanDetail = loanDetail;
        iSelectLoanActivityView.dismissTezLoader();
        iSelectLoanActivityView.setClContentVisibility(View.VISIBLE);
        iSelectLoanActivityView.setLoanDurations(getLoanDurations(loanDetail.getTenureDetails()));
        loanApplyRequest.setLatePaymentCharges(loanDetail.getLatePaymentCharges());
    }

    @Override
    public void onGetLoanDetailsFailure(int errorCode, String message) {
        iSelectLoanActivityView.dismissTezLoader();
        iSelectLoanActivityView.showError(errorCode, (dialog, which) -> iSelectLoanActivityView.finishActivity());
    }

    @Override
    public void calculateAllAmounts(@Nullable Wallet wallet, double loanAmount, int dayTenure) {
        int serviceProviderId = wallet == null ? 1 : wallet.getServiceProviderId();
        double processingFees = loanDetail.getProcessingFees(serviceProviderId, loanAmount);
        double pricingAmount = loanDetail.getPricingAmount(serviceProviderId, dayTenure);

        if (processingFees == -1)
            processingFees = 0;

        if (pricingAmount == -1)
            pricingAmount = 0;

        double charges = (loanAmount / 100) * pricingAmount;
        double amountYouReceive = loanAmount - processingFees;
        double amountToReturn = loanAmount + charges;

        iSelectLoanActivityView.setTvProcessingFeeValue(formatAmount(processingFees));
        iSelectLoanActivityView.setTvChargesValue(formatAmount(charges));
        iSelectLoanActivityView.setTvAmountReceiveValue(formatAmount(amountYouReceive));
        iSelectLoanActivityView.setTvAmountReturnValue(formatAmount(amountToReturn));

        loanApplyRequest.setAmount(loanAmount);
        loanApplyRequest.setDuration(dayTenure);
        loanApplyRequest.setInterestRate(pricingAmount);
        loanApplyRequest.setProcessingFee(processingFees);
    }

    @Override
    public void onClickBtContinue(double loanAmount, @Nullable Wallet wallet) {

        if (loanAmount < 0)
            iSelectLoanActivityView.showInformativeMessage(R.string.invalid_amount);

        else if (loanAmount < MIN_LOAN_AMOUNT)
            iSelectLoanActivityView.showInformativeMessage(R.string.loan_min_amount_message);

        else if (loanAmount > maxLoanLimit)
            iSelectLoanActivityView.showMaxLimitPrompt(Utility.getFormattedAmountWithoutCurrencyLabel(maxLoanLimit));

        else if (wallet != null) {
            iSelectLoanActivityView.routeToLoanPurposeActivity(loanApplyRequest);
        }
    }

    private String formatAmount(double amount) {
        return Utility.getFormattedAmountWithoutCurrencyLabel(amount);
    }

    private List<LoanDuration> getLoanDurations(@NonNull List<TenureDetail> tenureDetailList) {
        List<LoanDuration> loanDurationList = new ArrayList<>();
        for (int i = 0; i < tenureDetailList.size(); i++) {
            TenureDetail tenureDetail = tenureDetailList.get(i);
            loanDurationList.add(new LoanDuration(tenureDetail.getDuration(),
                    tenureDetail.getTitle(),
                    tenureDetail.getMinDisbursementAmount()));
        }
        return loanDurationList;
    }
}