package com.tez.androidapp.rewamp.general.beneficiary.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.widgets.BeneficiaryCardView;
import com.tez.androidapp.rewamp.general.beneficiary.entity.Beneficiary;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.List;

public class BeneficiaryListAdapter extends GenericRecyclerViewAdapter<Beneficiary,
        BeneficiaryListAdapter.BeneficiaryListener,
        BeneficiaryListAdapter.BeneficiaryViewHolder> {

    public BeneficiaryListAdapter(@NonNull List<Beneficiary> items, @Nullable BeneficiaryListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public BeneficiaryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.list_item_new_beneficiary, parent);
        return new BeneficiaryViewHolder(view);
    }

    public ArrayList<Integer> getOneTimeAddOnlyAddedRelations() {
        ArrayList<Integer> oneTimeRelationList = new ArrayList<>();
        for (Beneficiary beneficiary : getItems()) {
            if (isOneTimeAddOnlyRelation(beneficiary.getRelationshipId()))
                oneTimeRelationList.add(beneficiary.getRelationshipId());
        }
        return oneTimeRelationList;
    }

    private boolean isOneTimeAddOnlyRelation(int relationId) {
        return relationId == Constants.BENEFICIARY_FATHER
                || relationId == Constants.BENEFICIARY_MOTHER
                || relationId == Constants.BENEFICIARY_HUSBAND
                || relationId == Constants.BENEFICIARY_WIFE;
    }

    public interface BeneficiaryListener extends BaseRecyclerViewListener {

        void onClickBeneficiary(@NonNull Beneficiary beneficiary);

        void onEditBeneficiary(@NonNull Beneficiary beneficiary);
    }

    class BeneficiaryViewHolder extends BaseViewHolder<Beneficiary, BeneficiaryListener> {

        @BindView(R.id.cvBeneficiary)
        private BeneficiaryCardView cvBeneficiary;

        private BeneficiaryViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(Beneficiary item, @Nullable BeneficiaryListener listener) {
            super.onBind(item, listener);
            cvBeneficiary.setDefault(item.isDefault());
            cvBeneficiary.setName(item.getName());
            cvBeneficiary.setRelation(item.getRelationshipId());
            cvBeneficiary.setNumber(item.getMobileNumber());
            cvBeneficiary.setRelationIcon(item.getRelationshipId());
            cvBeneficiary.setOnEditClickListener(view -> {
                if (listener != null)
                    listener.onEditBeneficiary(item);
            });
            this.setItemViewOnClickListener(view -> {
                if (listener != null) {
                    if (item.getMobileNumber() != null)
                        listener.onClickBeneficiary(item);
                    else
                        listener.onEditBeneficiary(item);
                }
            });
        }
    }
}
