package com.tez.androidapp.rewamp.advance.request.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.advance.request.callback.LoanRemainingStepsCallback;

public class LoanRemainingStepsRH extends BaseRH<LoanRemainingStepsResponse> {

    private final LoanRemainingStepsCallback callback;

    public LoanRemainingStepsRH(BaseCloudDataStore baseCloudDataStore, LoanRemainingStepsCallback callback) {
        super(baseCloudDataStore);
        this.callback = callback;
    }

    @Override
    protected void onSuccess(Result<LoanRemainingStepsResponse> value) {
        LoanRemainingStepsResponse response = value.response().body();
        if (response != null) {
            if (isErrorFree(response))
                callback.onGetLoanRemainingStepsSuccess(response.getStepsLeft());
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());

        } else
            this.sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        callback.onGetLoanRemainingStepsFailure(errorCode, message);
    }
}
