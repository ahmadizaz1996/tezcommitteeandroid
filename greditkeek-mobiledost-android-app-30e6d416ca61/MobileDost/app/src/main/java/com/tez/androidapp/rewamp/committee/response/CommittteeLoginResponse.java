package com.tez.androidapp.rewamp.committee.response;

import com.google.gson.annotations.SerializedName;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.User;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class CommittteeLoginResponse extends BaseResponse implements Serializable {

//    public int statusCode;
    public String principalName;
    public String userStatus;
    public int isNewDevice;
    public int isMobileNumberVerified;
    public String referralCode;
    public Token token;
    public UserDetails userDetails;
    public boolean isTemporaryPinSet;


/*    @Override
    public int getStatusCode() {
        return statusCode;
    }

    @Override
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }*/

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public int getIsNewDevice() {
        return isNewDevice;
    }

    public void setIsNewDevice(int isNewDevice) {
        this.isNewDevice = isNewDevice;
    }

    public int getIsMobileNumberVerified() {
        return isMobileNumberVerified;
    }

    public void setIsMobileNumberVerified(int isMobileNumberVerified) {
        this.isMobileNumberVerified = isMobileNumberVerified;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public boolean isTemporaryPinSet() {
        return isTemporaryPinSet;
    }

    public void setTemporaryPinSet(boolean temporaryPinSet) {
        isTemporaryPinSet = temporaryPinSet;
    }

    public class Token {
        public String access_token;
        public String refresh_token;
        public int expires_in;
        public int refresh_expires_in;
        public String token_type;
        @SerializedName("not-before-policy")
        public int notBeforePolicy;
        public String session_state;


        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getRefresh_token() {
            return refresh_token;
        }

        public void setRefresh_token(String refresh_token) {
            this.refresh_token = refresh_token;
        }

        public int getExpires_in() {
            return expires_in;
        }

        public void setExpires_in(int expires_in) {
            this.expires_in = expires_in;
        }

        public int getRefresh_expires_in() {
            return refresh_expires_in;
        }

        public void setRefresh_expires_in(int refresh_expires_in) {
            this.refresh_expires_in = refresh_expires_in;
        }

        public String getToken_type() {
            return token_type;
        }

        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }

        public int getNotBeforePolicy() {
            return notBeforePolicy;
        }

        public void setNotBeforePolicy(int notBeforePolicy) {
            this.notBeforePolicy = notBeforePolicy;
        }

        public String getSession_state() {
            return session_state;
        }

        public void setSession_state(String session_state) {
            this.session_state = session_state;
        }
    }

    public class UserDevice {
        public int id;
        public String imei;
        public String deviceBrand;
        public String deviceModel;
        public String networkOperator;
        public String deviceOs;
        public boolean isActive;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImei() {
            return imei;
        }

        public void setImei(String imei) {
            this.imei = imei;
        }

        public String getDeviceBrand() {
            return deviceBrand;
        }

        public void setDeviceBrand(String deviceBrand) {
            this.deviceBrand = deviceBrand;
        }

        public String getDeviceModel() {
            return deviceModel;
        }

        public void setDeviceModel(String deviceModel) {
            this.deviceModel = deviceModel;
        }

        public String getNetworkOperator() {
            return networkOperator;
        }

        public void setNetworkOperator(String networkOperator) {
            this.networkOperator = networkOperator;
        }

        public String getDeviceOs() {
            return deviceOs;
        }

        public void setDeviceOs(String deviceOs) {
            this.deviceOs = deviceOs;
        }

        public boolean isActive() {
            return isActive;
        }

        public void setActive(boolean active) {
            isActive = active;
        }
    }

    public class City {
        public int id;
        public String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class Address {
        public int id;
        public String address;
        public City city;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public City getCity() {
            return city;
        }

        public void setCity(City city) {
            this.city = city;
        }
    }

    public class UserDetails {
        public int id;
        public String email;
        public String fullName;
        public String mobileNumber;
        public String gender;
        public String dateOfBirth;
        public String cnicDateOfExpiry;
        public String cnicDateOfIssue;
        public String cnic;
        public boolean isCnicVerified;
        public boolean isProfileCompleted;
        public String motherMaidenName;
        public String fatherName;
        public String principalName;
        public int lockAttempts;
        public String referralCode;
        public String isMobileNumberVerified;
        public String userStatus;
        public int placeOfBirthId;
        public String placeOfBirthName;
        public boolean isLoggedIn;
        public boolean isOCRFailed;
        public boolean backupFlow;
        public String preferredLanguage;
        public boolean cnicLifeTimeValid;
        public List<UserDevice> userDevices;
        public List<Address> addresses;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDateOfBirth() {
            return dateOfBirth;
        }

        public void setDateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }

        public String getCnicDateOfExpiry() {
            return cnicDateOfExpiry;
        }

        public void setCnicDateOfExpiry(String cnicDateOfExpiry) {
            this.cnicDateOfExpiry = cnicDateOfExpiry;
        }

        public String getCnicDateOfIssue() {
            return cnicDateOfIssue;
        }

        public void setCnicDateOfIssue(String cnicDateOfIssue) {
            this.cnicDateOfIssue = cnicDateOfIssue;
        }

        public String getCnic() {
            return cnic;
        }

        public void setCnic(String cnic) {
            this.cnic = cnic;
        }

        public boolean isCnicVerified() {
            return isCnicVerified;
        }

        public void setCnicVerified(boolean cnicVerified) {
            isCnicVerified = cnicVerified;
        }

        public boolean isProfileCompleted() {
            return isProfileCompleted;
        }

        public void setProfileCompleted(boolean profileCompleted) {
            isProfileCompleted = profileCompleted;
        }

        public String getMotherMaidenName() {
            return motherMaidenName;
        }

        public void setMotherMaidenName(String motherMaidenName) {
            this.motherMaidenName = motherMaidenName;
        }

        public String getFatherName() {
            return fatherName;
        }

        public void setFatherName(String fatherName) {
            this.fatherName = fatherName;
        }

        public String getPrincipalName() {
            return principalName;
        }

        public void setPrincipalName(String principalName) {
            this.principalName = principalName;
        }

        public int getLockAttempts() {
            return lockAttempts;
        }

        public void setLockAttempts(int lockAttempts) {
            this.lockAttempts = lockAttempts;
        }

        public String getReferralCode() {
            return referralCode;
        }

        public void setReferralCode(String referralCode) {
            this.referralCode = referralCode;
        }

        public String getIsMobileNumberVerified() {
            return isMobileNumberVerified;
        }

        public void setIsMobileNumberVerified(String isMobileNumberVerified) {
            this.isMobileNumberVerified = isMobileNumberVerified;
        }

        public String getUserStatus() {
            return userStatus;
        }

        public void setUserStatus(String userStatus) {
            this.userStatus = userStatus;
        }

        public int getPlaceOfBirthId() {
            return placeOfBirthId;
        }

        public void setPlaceOfBirthId(int placeOfBirthId) {
            this.placeOfBirthId = placeOfBirthId;
        }

        public String getPlaceOfBirthName() {
            return placeOfBirthName;
        }

        public void setPlaceOfBirthName(String placeOfBirthName) {
            this.placeOfBirthName = placeOfBirthName;
        }

        public boolean isLoggedIn() {
            return isLoggedIn;
        }

        public void setLoggedIn(boolean loggedIn) {
            isLoggedIn = loggedIn;
        }

        public boolean isOCRFailed() {
            return isOCRFailed;
        }

        public void setOCRFailed(boolean OCRFailed) {
            isOCRFailed = OCRFailed;
        }

        public boolean isBackupFlow() {
            return backupFlow;
        }

        public void setBackupFlow(boolean backupFlow) {
            this.backupFlow = backupFlow;
        }

        public String getPreferredLanguage() {
            return preferredLanguage;
        }

        public void setPreferredLanguage(String preferredLanguage) {
            this.preferredLanguage = preferredLanguage;
        }

        public boolean isCnicLifeTimeValid() {
            return cnicLifeTimeValid;
        }

        public void setCnicLifeTimeValid(boolean cnicLifeTimeValid) {
            this.cnicLifeTimeValid = cnicLifeTimeValid;
        }

        public List<UserDevice> getUserDevices() {
            return userDevices;
        }

        public void setUserDevices(List<UserDevice> userDevices) {
            this.userDevices = userDevices;
        }

        public List<Address> getAddresses() {
            return addresses;
        }

        public void setAddresses(List<Address> addresses) {
            this.addresses = addresses;
        }

        public UserDetails getUserWithMinProps() {
            UserDetails user = new UserDetails();
//            user.setCnicExpired(isCnicExpired);
            user.setId(id);
            user.setEmail(email);
            user.setMobileNumber(mobileNumber);
            user.setDateOfBirth(dateOfBirth);
            user.setCnic(cnic);
            user.setFullName(fullName);
            user.setPrincipalName(principalName);
            user.setCnicDateOfExpiry(cnicDateOfExpiry);
            return user;
        }

    }
}
