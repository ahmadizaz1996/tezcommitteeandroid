package com.tez.androidapp.rewamp.general.transactions;

import com.tez.androidapp.app.general.feature.transactions.callbacks.GetLoanTransactionDetailCallback;
import com.tez.androidapp.app.general.feature.transactions.models.network.TransactionDetail;
import com.tez.androidapp.app.general.feature.transactions.models.network.dto.request.GetLoanTransactionDetailRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;

/**
 * Created by VINOD KUMAR on 8/28/2019.
 */
public class MyTransactionDetailActivityInteractor implements IMyTransactionDetailActivityInteractor {

    private final IMyTransactionDetailActivityInteractorOutput iMyTransactionDetailActivityInteractorOutput;

    public MyTransactionDetailActivityInteractor(IMyTransactionDetailActivityInteractorOutput iMyTransactionDetailActivityInteractorOutput) {
        this.iMyTransactionDetailActivityInteractorOutput = iMyTransactionDetailActivityInteractorOutput;
    }

    @Override
    public void getLoanTransactionDetail(int transactionId) {
        LoanCloudDataStore.getInstance().getLoanTransactionDetail(new GetLoanTransactionDetailRequest(transactionId), new GetLoanTransactionDetailCallback() {
            @Override
            public void onGetLoanTransactionDetailSuccess(TransactionDetail transactionDetail) {
                iMyTransactionDetailActivityInteractorOutput.onGetLoanTransactionDetailSuccess(transactionDetail);
            }

            @Override
            public void onGetLoanTransactionDetailFailure(int statusCode, String message) {
                if (Utility.isUnauthorized(statusCode))
                    getLoanTransactionDetail(transactionId);
                else
                    iMyTransactionDetailActivityInteractorOutput.onGetLoanTransactionDetailFailure(statusCode, message);
            }
        });
    }
}
