package com.tez.androidapp.rewamp.signup.verification;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.common.util.CollectionUtils;
import com.sinch.verification.Config;
import com.sinch.verification.InitiationResult;
import com.sinch.verification.SinchVerification;
import com.sinch.verification.Verification;
import com.sinch.verification.VerificationListener;
import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.general.feature.playback.TezCountDownTimer;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PermissionCheckUtil;
import com.tez.androidapp.commons.utils.app.Utility;

import net.tez.fragment.util.optional.Optional;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created by FARHAN DHANANI on 6/5/2019.
 */
public class NumberVerificationRetryPresenter implements INumberVerificationRetryPresenter, INumberVerificationRetryInteractorOutput {

    private static final String APPLICATION_KEY = "cd13ffb2-1d91-4c82-a327-7eb1560e539c";
    private final INumberVerificationRetryView iNumberVerificationRetryView;
    private final INumberVerificationRetryInteractor iNumberVerificationRetryInteractor;

    public NumberVerificationRetryPresenter(INumberVerificationRetryView iNumberVerificationRetryView) {
        this.iNumberVerificationRetryView = iNumberVerificationRetryView;
        this.iNumberVerificationRetryInteractor = new NumberVerificationRetryInteractor(this);
    }

    private static boolean isPermissionGrantedForSignUp() {
        return PermissionCheckUtil.getInstance(MobileDostApplication.getInstance().getApplicationContext()).
                getMissingPermissionsForSignUp(MobileDostApplication.getSignupPermissions()).isEmpty();
    }

    @Override
    public void createVerification(@NonNull String mobileNumber,
                                   @NonNull String label,
                                   @NonNull String principalName) {
        if (iNumberVerificationRetryView.isNetworkConnected()) {
            Optional.doWhen(iNumberVerificationRetryView.canStartNumberVerification(), () -> {
                final String mobileNumberWithLocality = Utility.getFormattedMobileNumberWithOutDashes(mobileNumber);
                final int otpFailureCount = MDPreferenceManager.getOtpVerificationFailureCount();
                final int sinchFailureCount = MDPreferenceManager.getFlashCallVerificationFailureCount();
                final int number_of_verification_try = otpFailureCount + sinchFailureCount;

                Optional.doWhen(isPermissionGrantedForSignUp(),
                        () -> Optional.doWhen(sinchFailureCount < NumberVerificationActivity.MAX_TRIES_FOR_FLASH_CALL &&
                                        ArrayUtils.contains(NumberVerificationActivity.SINCH_EXECUTION_ON_TRIES,number_of_verification_try),
                                () -> {
                                    Config config = SinchVerification.config()
                                            .applicationKey(APPLICATION_KEY)
                                            .context(MobileDostApplication.getInstance().getApplicationContext())
                                            .build();
                                    Verification mVerification = SinchVerification.createFlashCallVerification(config,
                                            mobileNumberWithLocality,
                                            label,
                                            getSinchVerificationCallBack(mobileNumber, principalName),
                                            true);
                                    mVerification.initiate();
                                }, () -> Optional.doWhen(otpFailureCount < NumberVerificationActivity.MAX_TRIES_FOR_OTP_SMS,
                                        () -> {
                                            iNumberVerificationRetryView.setVisibiltyForTvTimer(View.GONE);
                                            iNumberVerificationRetryInteractor.requestOtp(principalName, mobileNumber, iNumberVerificationRetryView.getContext());
                                        },
                                        NumberVerificationRetryPresenter.this::onVerificationTriesFinish)),
                        NumberVerificationRetryPresenter.this::onSinchFlashCallPermissionFailure);
            }, iNumberVerificationRetryView::preliminaryNumberVerificationWork);
        } else {
            iNumberVerificationRetryView.retryState();
            iNumberVerificationRetryView.showInformativeMessage(R.string.make_sure_network_signal);
        }
    }

    private VerificationListener getSinchVerificationCallBack(@NonNull final String mobileNumber,
                                                              @Nullable final String cnic) {
        boolean[] isSecondCall = new boolean[1];
        TezCountDownTimer countDownTimer = new TezCountDownTimer(Constants.ONE_MIN, Constants.ONE_SECOND) {
            @Override
            public void onTick(long millisUntilFinished) {
                super.onTick(millisUntilFinished);
                showTime(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                isSecondCall[0] = true;
                iNumberVerificationRetryInteractor.sendFlashCallFailureToServer(cnic, mobileNumber);
                NumberVerificationRetryPresenter.this.onCountDownFinish();
            }
        };
        countDownTimer.start();
        return new VerificationListener() {
            @Override
            public void onInitiated(InitiationResult initiationResult) {
                //Left
            }

            @Override
            public void onInitiationFailed(Exception e) {
                onVerificationFailed(e);
            }

            @Override
            public void onVerified() {
                if (!isSecondCall[0]) {
                    countDownTimer.cancel();
                    iNumberVerificationRetryInteractor.sendFlashCallSuccessToServer(cnic, mobileNumber);
                    NumberVerificationRetryPresenter.this.onVerified();
                }
            }

            @Override
            public void onVerificationFailed(Exception e) {
                if (!isSecondCall[0]) {
                    isSecondCall[0] = true;
                    countDownTimer.cancel();
                    iNumberVerificationRetryInteractor.sendFlashCallFailureToServer(cnic, mobileNumber);
                    initCountDownTimerAfterSinchFlashCallFails();
                }
            }

            @Override
            public void onVerificationFallback() {
                //Left
            }
        };
    }

    private void initCountDownTimerAfterSinchFlashCallFails() {
        iNumberVerificationRetryView.setTextToTvVerifyYourNumber(
                Utility.getStringFromResource(R.string.verification_failed));
        new TezCountDownTimer(Constants.FIVE_SECOND, Constants.ONE_SECOND) {
            @Override
            public void onTick(long millisUntilFinished) {
                super.onTick(millisUntilFinished);
                showTime(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                NumberVerificationRetryPresenter.this.onCountDownFinish();
            }
        }.start();
    }


    @Override
    public void onVerificationTriesFinish() {
        this.iNumberVerificationRetryView.onVerificationTriesFinish();
    }


    @Override
    public void onSinchFlashCallPermissionFailure() {
        this.iNumberVerificationRetryView.onSinchFlashCallPermissionFailure();
    }

    @Override
    public void onCountDownFinish() {
        this.iNumberVerificationRetryView.onCountDownFinish();
    }

    @Override
    public void onVerified() {
        this.iNumberVerificationRetryView.onVerified();
    }

    @Override
    public void onUserSessionTimeOut() {
        this.iNumberVerificationRetryView.onUserSessionTimeOut();
    }

    @Override
    public void showTime(long millisUntilFinished) {
        long secs = (long) Math.ceil((double) millisUntilFinished / 1000);
        long secondsRemaining = secs % 60;
        long minutes = secs / 60;
        String timerText = Utility.getTwoDigitInteger((int) minutes)
                + Utility.getStringFromResource(R.string.string_colon)
                + Utility.getTwoDigitInteger((int) secondsRemaining);
        this.iNumberVerificationRetryView.setTextToTvTimer(timerText);
    }

    @Override
    public void onNumberVerifyGenerateOtpSuccess() {
        this.iNumberVerificationRetryView.setVisibiltyForTvTimer(View.VISIBLE);
    }
}
