package com.tez.androidapp.rewamp.bima.products.policy.presenter;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.rewamp.bima.products.policy.response.PolicyDetails;

public interface IJazzInsuranceActivityInteractorOutput extends IEasyPaisaInsurancePaymentActivityInteractorOutput {
    void initiatePurchasePolicySuccess(PolicyDetails policyDetails, @NonNull String pin,
                                       @Nullable Location location);
}
