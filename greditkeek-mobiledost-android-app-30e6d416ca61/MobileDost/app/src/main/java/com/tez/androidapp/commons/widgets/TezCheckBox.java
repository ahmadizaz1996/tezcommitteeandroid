package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.ColorRes;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.core.widget.CompoundButtonCompat;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.TezDrawableHelper;

/**
 * Created by FARHAN DHANANI on 6/1/2019.
 */
public class TezCheckBox extends AppCompatCheckBox {

    public TezCheckBox(Context context) {
        super(context);
        init(context, null);
    }

    public TezCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TezCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        this.setBackground(null);
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TezCheckBox);
            try {
                Drawable buttonDrawable = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezCheckBox_buttonCompat);
                if (buttonDrawable != null)
                    setButtonDrawable(buttonDrawable);
            } finally {
                typedArray.recycle();
            }
        }
    }


    public void setButtonDrawableColor(@ColorRes int colorRes) {
        this.setTextColor(Utility.getColorFromResource(colorRes));
        CompoundButtonCompat.setButtonTintList(this, ContextCompat.getColorStateList(getContext(), colorRes));
    }
}

