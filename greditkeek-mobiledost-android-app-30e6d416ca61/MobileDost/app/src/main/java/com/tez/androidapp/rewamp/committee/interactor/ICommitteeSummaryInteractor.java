package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeDeclineRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.request.JoinCommitteeRequest;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeSummaryInteractor extends IBaseInteractor {
    void inviteMembers(CommitteeInviteRequest committeeInviteRequest);

    void leaveCommittee(String committeeId);

    void declineCommittee(String committeeId);

    void joinCommittee(JoinCommitteeRequest joinCommitteeRequest);
}
