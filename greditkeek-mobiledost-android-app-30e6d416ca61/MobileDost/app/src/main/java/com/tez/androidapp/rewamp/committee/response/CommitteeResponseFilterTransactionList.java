
package com.tez.androidapp.rewamp.committee.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class CommitteeResponseFilterTransactionList implements Serializable {

    @SerializedName("amount")
    private Long mAmount;
    @SerializedName("round")
    private Long mRound;
    @SerializedName("transactionDate")
    private String mTransactionDate;
    @SerializedName("userId")
    private Long mUserId;
    @SerializedName("username")
    private String mUsername;
    @SerializedName("type")
    private String mtype;

    private boolean header;
    private String joiningDate;


    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public boolean isHeader() {
        return header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }

    public String getMtype() {
        return mtype;
    }

    public void setMtype(String mtype) {
        this.mtype = mtype;
    }

    public Long getAmount() {
        return mAmount;
    }

    public void setAmount(Long amount) {
        mAmount = amount;
    }

    public Long getRound() {
        return mRound;
    }

    public void setRound(Long round) {
        mRound = round;
    }

    public String getTransactionDate() {
        return mTransactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        mTransactionDate = transactionDate;
    }

    public Long getUserId() {
        return mUserId;
    }

    public void setUserId(Long userId) {
        mUserId = userId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

}
