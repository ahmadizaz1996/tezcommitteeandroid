package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeResponseTimeline;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeSummaryActivity;
import com.tez.androidapp.rewamp.committee.view.MyCommitteeActivity;
import com.tez.androidapp.rewamp.committee.view.MyCommitteeUpdatesActivity;

import java.util.ArrayList;
import java.util.List;

public class CommitteeUpdateActivityRouter extends BaseActivityRouter {


    public static final String INVITEES_LIST = "INVITEES_LIST";
    public static final String COMMITTEE_FILTER_RESPONSE = "COMMITTEE_FILTER_RESPONSE";

    public static CommitteeUpdateActivityRouter createInstance() {
        return new CommitteeUpdateActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, MyCommitteeUpdatesActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    public void setDependenciesAndRoute(BaseActivity from, ArrayList<MyCommitteeResponse.Committee.Invite> inviteesList) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(INVITEES_LIST, inviteesList);
        intent.putExtras(bundle);
        route(from, intent);
    }

    public void setDependenciesAndRoute(BaseActivity from, ArrayList<CommitteeResponseTimeline> committeeFilterResponses,Object object) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putSerializable(COMMITTEE_FILTER_RESPONSE, committeeFilterResponses);
        intent.putExtras(bundle);
        route(from, intent);
    }
}
