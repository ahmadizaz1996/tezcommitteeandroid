package com.tez.androidapp.rewamp.bima.products.policy.view;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;

public class DigitalOpdActivity extends BaseProductActivity {

    @Override
    protected int getContentView() {
        return R.layout.activity_digital_opd;
    }

    @Override
    protected int getProductId() {
        return Constants.DIGITAL_OPD_PLAN_ID;
    }

    @Override
    protected String getScreenName() {
        return "DigitalOpdActivity";
    }
}
