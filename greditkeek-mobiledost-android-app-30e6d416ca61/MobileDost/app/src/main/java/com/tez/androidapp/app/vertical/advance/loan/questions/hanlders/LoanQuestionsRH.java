package com.tez.androidapp.app.vertical.advance.loan.questions.hanlders;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.advance.loan.questions.callbacks.LoanQuestionsCallback;
import com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.response.LoanQuestionsResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 2/17/2017.
 */

public class LoanQuestionsRH extends BaseRH<LoanQuestionsResponse> {

    private LoanQuestionsCallback loanQuestionsCallback;

    public LoanQuestionsRH(BaseCloudDataStore baseCloudDataStore, @Nullable LoanQuestionsCallback
            loanQuestionsCallback) {
        super(baseCloudDataStore);
        this.loanQuestionsCallback = loanQuestionsCallback;
    }

    @Override
    protected void onSuccess(Result<LoanQuestionsResponse> value) {
        LoanQuestionsResponse loanQuestionsResponse = value.response().body();
        if (loanQuestionsResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            if (loanQuestionsCallback != null) loanQuestionsCallback.onLoanQuestionsSuccess(loanQuestionsResponse.getLoanQuestions());
        } else onFailure(loanQuestionsResponse.getStatusCode(), loanQuestionsResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (loanQuestionsCallback != null)
            loanQuestionsCallback.onLoanQuestionsFailure(errorCode, message);
    }
}
