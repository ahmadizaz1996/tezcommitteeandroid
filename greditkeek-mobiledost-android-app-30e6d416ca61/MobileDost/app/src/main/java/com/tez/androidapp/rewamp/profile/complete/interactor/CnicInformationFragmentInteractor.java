package com.tez.androidapp.rewamp.profile.complete.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetCompleteUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.QuestionOptionsCallBack;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateProfileWithCnicCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.MobileUserRejectedFeildsDto;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.UserProfile;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.QuestionOptionsRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateProfileWithCnicRequest;
import com.tez.androidapp.commons.location.callbacks.GetCitiesCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.app.City;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.models.network.UserAddress;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.LoanQuestionSessionDataStore;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.profile.complete.presenter.ICnicInformationFragmentInteractorOutput;
import com.tez.androidapp.rewamp.profile.trust.interactor.ProfileTrustActivityInteractor;

import java.util.ArrayList;
import java.util.List;

public class CnicInformationFragmentInteractor extends ProfileTrustActivityInteractor
        implements ICnicInformationFragmentInteractor {

    private final ICnicInformationFragmentInteractorOutput iCnicInformationFragmentInteractorOutput;


    public CnicInformationFragmentInteractor(ICnicInformationFragmentInteractorOutput iCnicInformationFragmentInteractorOutput) {
        super(iCnicInformationFragmentInteractorOutput);
        this.iCnicInformationFragmentInteractorOutput = iCnicInformationFragmentInteractorOutput;
    }

    @Override
    public void getUserProfile(String lang, UserProfile userProfile){
        UserAuthCloudDataStore.getInstance().getCompleteUserProfile(new GetCompleteUserProfileCallback() {
            @Override
            public void onCompleteUserProfileSuccess(User user) {
                getCities(lang, userProfile, user);
            }

            @Override
            public void onCompleteUserProfileFailure(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        ()->getUserProfile(lang, userProfile),
                        ()->iCnicInformationFragmentInteractorOutput.onCompleteUserProfileFailure(errorCode, message));
            }
        });
    }

    @Override
    public void updateUserProfile(User user,
                                  UpdateProfileWithCnicRequest updateProfileWithCnicRequest,
                                  final boolean isPartialRequest, City selectedPlaceOfBirthCity,
                                  City selectedCurrentCity, AnswerSelected selectedMaritalStatus){
        UserAuthCloudDataStore.getInstance().updateUserProfileWithCnic(updateProfileWithCnicRequest, new UpdateProfileWithCnicCallback() {
            @Override
            public void onSuccessUpdateProfileWithCnicCallback() {
                user.setFullName(updateProfileWithCnicRequest.getFullName());
                user.setGender(updateProfileWithCnicRequest.getGender());
                user.setCnic(updateProfileWithCnicRequest.getCnic());
                user.setCnicDateOfIssue(updateProfileWithCnicRequest.getCnicDateOfIssue());
                user.setCnicDateOfExpiry(updateProfileWithCnicRequest.getCnicDateOfExpiry());
                user.setDateOfBirth(updateProfileWithCnicRequest.getDateOfBirth());
                user.setPlaceOfBirthId(updateProfileWithCnicRequest.getPlaceOfBirthId());
                user.setPlaceOfBirthName(selectedPlaceOfBirthCity==null? null:selectedPlaceOfBirthCity.getCityName());
                List<UserAddress> userAddresses = new ArrayList<>();
                UserAddress userAddress = new UserAddress();
                userAddress.setAddress(updateProfileWithCnicRequest.getAddress());
                userAddress.setCity(selectedCurrentCity);
                userAddresses.add(userAddress);
                user.setAddresses(userAddresses);
                user.setFatherName(updateProfileWithCnicRequest.getFatherName());
                user.setHusbandName(updateProfileWithCnicRequest.getHusbandName());
                List<AnswerSelected> personalInformationList= new ArrayList<>();
                personalInformationList.add(selectedMaritalStatus);
                user.setPersonalInformationList(personalInformationList);
                user.setMotherMaidenName(updateProfileWithCnicRequest.getMotherMaidenName());
                user.setMobileUserRejectedFieldsDto(new MobileUserRejectedFeildsDto());
                user.setEditable(false);
                MDPreferenceManager.setUser(user);
                iCnicInformationFragmentInteractorOutput.onSuccessUpdateProfileWithCnicCallback(isPartialRequest, selectedMaritalStatus);
            }

            @Override
            public void onFailureUpdateProfileWithCnicCallback(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        ()->updateUserProfile(user, updateProfileWithCnicRequest, isPartialRequest,
                                selectedPlaceOfBirthCity, selectedCurrentCity, selectedMaritalStatus),
                        ()->iCnicInformationFragmentInteractorOutput.onFailureUpdateProfileWithCnicCallback(errorCode, message));
            }
        });
    }

    private void getOptionsForMaritalStatus(String lang, UserProfile userProfile, User user, List<City> cities) {
        LoanQuestionSessionDataStore.getInstance().getQuestionOptions(lang,
                QuestionOptionsRequest.Params.MARITAL_STATUS, new QuestionOptionsCallBack() {
                    @Override
                    public void onQuestionOptionsSuccess(List<Option> data) {
                        iCnicInformationFragmentInteractorOutput.onCompleteUserProfileSuccess(userProfile, user, cities, data);
                    }

                    @Override
                    public void onQuestionOptionsFailure(int errorCode, String message) {
                        Optional.doWhen(Utility.isUnauthorized(errorCode),
                                () -> getOptionsForMaritalStatus(lang, userProfile, user, cities),
                                () -> iCnicInformationFragmentInteractorOutput.onFailureUpdateProfileWithCnicCallback(errorCode, message));
                    }
                });
    }


    private void getCities(String lang, UserProfile userProfile, User user) {
        UserCloudDataStore.getInstance().getCities(new GetCitiesCallback() {
            @Override
            public void onGetCitiesSuccess(@NonNull List<City> cities) {
                getOptionsForMaritalStatus(lang, userProfile, user, cities);
            }

            @Override
            public void onGetCitiesError(int errorcode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorcode),
                        ()->getCities(lang, userProfile, user),
                        ()->iCnicInformationFragmentInteractorOutput.onCompleteUserProfileFailure(errorcode, message));
            }
        });
    }
}
