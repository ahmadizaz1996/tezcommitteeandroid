package com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 8/29/2017.
 */

public class RepayReceiptRequest extends BaseRequest{
    public static final String METHOD_NAME = "v1/loan/repay/receipt";

    private Integer loanId;

    public RepayReceiptRequest(Integer loanId) {
        this.loanId = loanId;
    }

    public Integer getLoanId() {
        return loanId;
    }
}
