package com.tez.androidapp.app;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;

import androidx.annotation.Nullable;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tez.androidapp.BuildConfig;
import com.tez.androidapp.app.analytics.AnalyticsTrackers;
import com.tez.androidapp.app.base.adapters.TezLogFileStrategy;
import com.tez.androidapp.app.base.observer.SessionTimeOutObserver;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.pref.GenericObjectParser;
import com.tez.androidapp.commons.pref.GenericStringParser;
import com.tez.androidapp.commons.pref.KeyStoreEncryptionStrategy;
import com.tez.androidapp.commons.receivers.BluetoothReceiver;
import com.tez.androidapp.commons.receivers.ConnectivityBroadcastReceiver;
import com.tez.androidapp.commons.receivers.WifiReceiver;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;

import net.tez.fragment.util.optional.Optional;
import net.tez.logger.library.adapters.AndroidLogAdapter;
import net.tez.logger.library.core.TezLog;
import net.tez.logger.library.formats.StyleLogFormatStrategy;
import net.tez.storage.library.adapters.SharedPreferenceAdapter;
import net.tez.storage.library.core.Storage;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

/**
 * Created  on 12/2/2016.
 */

public class MobileDostApplication extends MultiDexApplication {

    private static MobileDostApplication mobileDostApplication;
    private boolean isProductionBuildChangeable = BuildConfig.FLAVOR.equalsIgnoreCase("prod");

    public static MobileDostApplication getInstance() {
        return mobileDostApplication;
    }

    private static void setInstance(MobileDostApplication instance) {
        mobileDostApplication = instance;
    }

    public static Context getAppContext() {
        return MobileDostApplication.getInstance().getApplicationContext();
    }

    @Nullable
    public static TelephonyManager getTelephonyManagerService() {
        return (TelephonyManager) getAppContext().getSystemService(Context.TELEPHONY_SERVICE);
    }

    @Nullable
    public static ResolveInfo getActivityResolverForInternet() {
        return getAppContext().getPackageManager()
                .resolveActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://"))
                        , PackageManager.MATCH_DEFAULT_ONLY);
    }

    public static String[] getSignupPermissions() {
        ArrayList<String> perms = new ArrayList<>();
        perms.add(Manifest.permission.READ_PHONE_STATE);
        perms.add(Manifest.permission.CALL_PHONE);
        perms.add(Manifest.permission.ACCESS_NETWORK_STATE);
        perms.add(Manifest.permission.INTERNET);
        perms.add(Manifest.permission.ACCESS_FINE_LOCATION);
        perms.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        perms.add(Manifest.permission.READ_CALL_LOG);
        perms.add(Manifest.permission.RECEIVE_SMS);
        perms.add(Manifest.permission.READ_SMS);
        return perms.toArray(new String[0]);
    }

    public static String[] OptionalPermissions() {
        ArrayList<String> perms = new ArrayList<>();
        perms.add(Manifest.permission.READ_CONTACTS);
        perms.add(Manifest.permission.READ_CALENDAR);
        return perms.toArray(new String[0]);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setInstance(this);
        Utility.initNotificationChannels(this);
        LocaleHelper.updateLanguage(this, LocaleHelper.getSelectedLanguage(this));
        FirebaseAnalytics.getInstance(this).setAnalyticsCollectionEnabled(enableFireBaseCrashReport());
        ProcessLifecycleOwner.get().getLifecycle().addObserver(SessionTimeOutObserver.getInstance());
        CrashlyticsCore crashlyticsCore = new CrashlyticsCore.Builder()
                .disabled(!enableFireBaseCrashReport())
                .build();
        Fabric.with(this, new Crashlytics.Builder().core(crashlyticsCore).build());
        FirebaseApp.initializeApp(this);
        if (!FacebookSdk.isInitialized())
            FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);
        FacebookSdk.setIsDebugEnabled(Utility.isProdReleaseBuild());
        initTezLoggerAdapter();
        initiallizeAnalyticsTracker();
        initStorage();
        register();
    }

    private boolean enableFireBaseCrashReport(){
        return Utility.isProdReleaseBuild() ||
                (BuildConfig.FLAVOR.equalsIgnoreCase("qa") && !BuildConfig.IS_DEBUG);
    }

    public void initiallizeAnalyticsTracker() {
        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
    }

    public void initTezLoggerAdapter() {
        TezLog.addAdapter(new AndroidLogAdapter(
                StyleLogFormatStrategy.newBuilder()
                        .logStrategy(new TezLogFileStrategy())
                        .build()) {
            @Override
            public boolean isLoggable(int priority, @Nullable String tag) {
                return !Constants.IS_EXTERNAL_BUILD;
            }
        });
    }

    public void initStorage() {
        Storage.setAdapter(new SharedPreferenceAdapter.Builder(this)
                .setFileName(MDPreferenceManager.PREFS_NAME)
                .setMode(Context.MODE_PRIVATE)
                .setTextTransformStrategy(new KeyStoreEncryptionStrategy())
                .setObjectToStringParser(new GenericStringParser())
                .setStringToObjectParser(new GenericObjectParser())
                .build());
    }

    public boolean isProductionBuildChangeable() {
        return isProductionBuildChangeable;
    }

    public void setProductionBuildChangeable(boolean productionBuildChangeable) {
        isProductionBuildChangeable = productionBuildChangeable;
    }

    private void register() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            new WifiReceiver().register(this);
            new BluetoothReceiver().register(this);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            new ConnectivityBroadcastReceiver().register(this);
    }

    public String[] getNumberVerificationPermissions() {
        return new String[]{
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.READ_SMS
        };
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(this, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }

}
