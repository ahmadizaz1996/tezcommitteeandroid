package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;
import android.os.Bundle;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.view.CommitteeCompletionActivity;
import com.tez.androidapp.rewamp.committee.view.CommitteeInviteContactsActivity;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class CommitteeCompletionActivityRouter extends BaseActivityRouter {


    public static final String COMMITTEE_DATA = "COMMITTEE_DATA";

    public static CommitteeCompletionActivityRouter createInstance() {
        return new CommitteeCompletionActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteePackageModel committeePackageModel) {
        Intent intent = createIntent(from);
        intent.putExtra(COMMITTEE_DATA, committeePackageModel);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteePackageModel committeePackage, CommitteeInviteRequest committeeInviteRequestArrayList) {
        Intent intent = createIntent(from);
//        intent.putExtra(COMMITTEE_DATA, committeePackage);
        Bundle args = new Bundle();
        args.putParcelable(CommitteeInviteContactsActivity.INVITEES_LIST, committeeInviteRequestArrayList);
        args.putSerializable(COMMITTEE_DATA, committeePackage);
        intent.putExtras(args);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeCompletionActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

}
