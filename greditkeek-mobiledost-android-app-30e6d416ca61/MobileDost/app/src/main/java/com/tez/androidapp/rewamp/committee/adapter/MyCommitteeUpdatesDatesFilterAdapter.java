package com.tez.androidapp.rewamp.committee.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezCheckBox;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.view.CommitteeFilterActivity;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/

public class MyCommitteeUpdatesDatesFilterAdapter extends GenericRecyclerViewAdapter<String,
        MyCommitteeUpdatesDatesFilterAdapter.CommitteeFilterDatesListener,
        MyCommitteeUpdatesDatesFilterAdapter.MyCommitteeMemberViewHolder> {

    public MyCommitteeUpdatesDatesFilterAdapter(@NonNull ArrayList<String> items, @Nullable CommitteeFilterDatesListener listener) {
        super(items, listener);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public MyCommitteeMemberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.simple_single_string_list_item, parent);
        return new MyCommitteeMemberViewHolder(view);
    }

    public interface CommitteeFilterDatesListener extends BaseRecyclerViewListener {
        void onSelectedDatesFilter(String item, boolean isCheckbox,CompoundButton button);
    }

    static class MyCommitteeMemberViewHolder extends BaseViewHolder<String, CommitteeFilterDatesListener> {

        @BindView(R.id.itemTextView)
        private TezTextView tvName;

        @BindView(R.id.cbWalletCheck)
        private TezCheckBox checkBox;

        public MyCommitteeMemberViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(String item, @Nullable CommitteeFilterDatesListener listener) {
            super.onBind(item, listener);

            tvName.setText(item);

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    checkBox.setOnCheckedChangeListener(null);
                    listener.onSelectedDatesFilter(item, isChecked,buttonView);
                    checkBox.setOnCheckedChangeListener(this::onCheckedChanged);
                }
            });
        }
    }
}
