package com.tez.androidapp.rewamp.general.beneficiary.view;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.ToolbarActivity;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.BeneficiaryDetails;
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezFrameLayout;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.IPolicyDetailActivityPresenter;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.PolicyDetailActivityPresenter;
import com.tez.androidapp.rewamp.general.beneficiary.router.BeneficiaryListActivityRouter;
import com.tez.androidapp.rewamp.general.beneficiary.router.PolicyDetailActivityRouter;
import com.tez.androidapp.rewamp.general.termsandcond.DetailedTermConditionDialog;

import net.tez.viewbinder.library.core.BindView;

public class PolicyDetailActivity extends ToolbarActivity implements IPolicyDetailActivityView {

    private final IPolicyDetailActivityPresenter iPolicyDetailActivityPresenter;

    @BindView(R.id.ivInsuranceIcon)
    private TezImageView ivInsuranceIcon;

    @BindView(R.id.tvInsuranceName)
    private TezTextView tvInsuranceName;

    @BindView(R.id.tvRemainingAmount)
    private TezTextView tvRemainingAmount;

    @BindView(R.id.flPolicyStatus)
    private TezFrameLayout flPolicyStatus;

    @BindView(R.id.tvPolicyNumber)
    private TezTextView tvPolicyNumber;

    @BindView(R.id.tvPolicyPeriod)
    private TezTextView tvPolicyPeriod;

    @BindView(R.id.tvPolicyDate)
    private TezTextView tvPolicyDate;

    @BindView(R.id.tvBeneficiaryName)
    private TezTextView tvBeneficiaryName;

    @BindView(R.id.tvBeneficiaryRelation)
    private TezTextView tvBeneficiaryRelation;

    @BindView(R.id.tvBeneficiaryMobileNo)
    private TezTextView tvBeneficiaryMobileNo;

    @BindView(R.id.ivBeneficiaryImage)
    private TezImageView ivBeneficiaryImage;

    @BindView(R.id.btEditBeneficiary)
    private TezButton btEditBeneficiary;

    @BindView(R.id.tvTermsAndCondition)
    private TezTextView tvTermsAndCondition;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    public PolicyDetailActivity() {
        iPolicyDetailActivityPresenter = new PolicyDetailActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy_detail);
        this.tezToolbar.setToolbarTitle(R.string.manage_beneficiary);
        Optional.ifPresent(getPolicyFromIntent(), this::init, this::finish);
    }

    private Policy getPolicyFromIntent() {
        return getIntent().getParcelableExtra(PolicyDetailActivityRouter.POLICY);
    }

    private void init(@NonNull Policy policy) {
        this.initText(policy);
        this.iPolicyDetailActivityPresenter.getPolicyDetails(policy.getMobileUserInsurancePolicyId());
    }

    private void initText(@NonNull Policy policy) {
        tvInsuranceName.setText(policy.isHealthInsurance() ? R.string.health_insurance : R.string.life_insurance);
        tvRemainingAmount.setText(getString(R.string.rs_value_string, policy.getRemainingAmountAsString()));
        this.setFlPolicyStatusBackground(policy.getInsuranceStatusColor());
        tvPolicyNumber.setText(policy.getPolicyNumber());
        tvPolicyDate.setText(policy.getDateApplied());
        tvPolicyPeriod.setText(getString(R.string.number_of_days, policy.getRemainingDays()));
    }

    private void setFlPolicyStatusBackground(int colorRes) {
        int bgColor = Utility.getColorFromResource(colorRes);
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                new int[]{bgColor, bgColor});
        gd.setCornerRadius(Utility.dpToPx(this, 4));
        flPolicyStatus.setBackground(gd);
    }

    @Override
    public void initBeneficiaryDetails(@NonNull BeneficiaryDetails beneficiaryDetails) {
        ivBeneficiaryImage.setImageResource(Utility.getBeneficiaryRelationImage(beneficiaryDetails.getRelationshipId()));
        tvBeneficiaryRelation.setText(beneficiaryDetails.getRelationshipFormatted());
        tvBeneficiaryName.setText(beneficiaryDetails.getName());
        tvBeneficiaryMobileNo.setText(beneficiaryDetails.getMobileNumber());
        btEditBeneficiary.setDoubleTapSafeOnClickListener(view ->
                BeneficiaryListActivityRouter
                        .createInstance()
                        .setDependenciesAndRouteForResult(this, beneficiaryDetails.getMobileUserInsurancePolicyBeneficiaryId()));
    }

    @Override
    public void setBtTermsAndConditionsClickListener(@NonNull String terms) {
        tvTermsAndCondition.setDoubleTapSafeOnClickListener(view -> {
            DetailedTermConditionDialog termDetailDialog = new DetailedTermConditionDialog(this);
            termDetailDialog.setTerms(terms);
            termDetailDialog.show();
        });
    }

    @Override
    public void setBtEditBeneficiaryVisibility(int visibility) {
        this.btEditBeneficiary.setVisibility(visibility);
    }

    @Override
    public void setClContentVisibility(int visibility) {
        clContent.setVisibility(visibility);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == BeneficiaryListActivityRouter.REQUEST_CODE_BENEFICIARY_LIST) {
            Policy policy = getPolicyFromIntent();
            if (policy != null)
                iPolicyDetailActivityPresenter.getPolicyDetails(policy.getMobileUserInsurancePolicyId());
            else
                finishActivity();
        }
    }

    @Override
    public void showTezLoader() {
        View shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissTezLoader() {
        View shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.GONE);
    }

    @Override
    protected String getScreenName() {
        return "PolicyDetailActivity";
    }
}
