package com.tez.androidapp.rewamp.committee.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteePaymentInitiateRequest extends BaseRequest {
    public static final String METHOD_NAME = "http://3.21.127.35/api/v1/committee/payment/initiate";

    public int committeeId;
    public String otp;
    public double amount;
    public int mobileAccountId;
    public String pin;
    public double lat;
    public double lng;


    public static String getMethodName() {
        return METHOD_NAME;
    }

    public int getCommitteeId() {
        return committeeId;
    }

    public void setCommitteeId(int committeeId) {
        this.committeeId = committeeId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getMobileAccountId() {
        return mobileAccountId;
    }

    public void setMobileAccountId(int mobileAccountId) {
        this.mobileAccountId = mobileAccountId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
