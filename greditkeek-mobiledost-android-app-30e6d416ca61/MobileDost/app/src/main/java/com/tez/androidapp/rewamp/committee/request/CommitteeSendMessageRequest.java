
package com.tez.androidapp.rewamp.committee.request;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CommitteeSendMessageRequest {

    public static final String METHOD_NAME = "http://3.21.127.35/api/v1/committee/chatMessage/send";

    @SerializedName("committeeId")
    private Long mCommitteeId;
    @SerializedName("message")
    private String mMessage;

    public Long getCommitteeId() {
        return mCommitteeId;
    }

    public void setCommitteeId(Long committeeId) {
        mCommitteeId = committeeId;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

}
