package com.tez.androidapp.rewamp.general.invite.friend.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.general.invite.friend.presenter.IInviteFriendActivityPresenter;
import com.tez.androidapp.rewamp.general.invite.friend.presenter.InviteFriendActivityPresenter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;

public class InviteFriendActivity extends PlaybackActivity
        implements IInviteFriendActivityView {

    @BindView(R.id.btContinue)
    private TezButton btContinue;

    @BindView(R.id.shimmer)
    private TezLinearLayout shimmer;

    @BindView(R.id.ivLogo)
    private TezImageView ivLogo;

    @BindView(R.id.textViewHeading)
    private TezTextView tezTextViewHeading;

    @BindView(R.id.tv_Description)
    private TezTextView tezTextViewDescription;

    @BindView(R.id.btContinue)
    private TezButton tezButtonContinue;

    private final IInviteFriendActivityPresenter iInviteFriendActivityPresenter;

    public InviteFriendActivity() {
        this.iInviteFriendActivityPresenter = new InviteFriendActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friend2);
        this.updateToolbar(findViewById(R.id.toolbar));
        init();
    }

    private void init() {
        this.setVisibillityForTezLinearLayoutShimmer(View.VISIBLE);
        this.setVisibillityForBtContinue(View.GONE);
        this.setVisibillityForIvLogo(View.GONE);
        this.setVisibillityForTvDescription(View.GONE);
        this.setVisibillityForTvHeading(View.GONE);
        this.iInviteFriendActivityPresenter.getRefferalCode();
    }

    @Override
    public void setOnClickListnerToBtOnClickListner(DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        this.btContinue.setDoubleTapSafeOnClickListener(doubleTapSafeOnClickListener);
    }

    @Override
    public void startActivityFromIntent(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void setBtContinueEnabled(boolean enabled) {
        this.btContinue.setEnabled(enabled);
    }

    @Override
    public void setVisibillityForTvHeading(int visibillity){
        this.textViewHeading.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityForTezLinearLayoutShimmer(int visibillity){
        this.shimmer.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityForTvDescription(int visibillity){
        this.tezTextViewDescription.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityForIvLogo(int visibillity){
        this.ivLogo.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityForBtContinue(int visibillity){
        this.btContinue.setVisibility(visibillity);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    protected String getScreenName() {
        return "InviteFriendActivity";
    }
}
