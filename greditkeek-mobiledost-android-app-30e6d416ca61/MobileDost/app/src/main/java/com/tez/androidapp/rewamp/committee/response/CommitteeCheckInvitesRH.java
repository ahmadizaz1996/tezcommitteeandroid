package com.tez.androidapp.rewamp.committee.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeCheckInvitesLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeMetadataLIstener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import java.util.List;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class CommitteeCheckInvitesRH extends BaseRH<List<CommitteeCheckInvitesResponse>> {

    private final CommitteeCheckInvitesLIstener listener;

    public CommitteeCheckInvitesRH(BaseCloudDataStore baseCloudDataStore, CommitteeCheckInvitesLIstener committeeCheckInvitesLIstener) {
        super(baseCloudDataStore);
        this.listener = committeeCheckInvitesLIstener;
    }

    @Override
    protected void onSuccess(Result<List<CommitteeCheckInvitesResponse>> value) {
        List<CommitteeCheckInvitesResponse> committeeCheckInvitesResponse = value.response().body();
        if (committeeCheckInvitesResponse != null /*&& committeeCheckInvitesResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR*/)
            this.listener.onCommitteeCheckInvitesSuccess(committeeCheckInvitesResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onCommitteeCheckInvitesFailure(errorCode, message);
    }
}
