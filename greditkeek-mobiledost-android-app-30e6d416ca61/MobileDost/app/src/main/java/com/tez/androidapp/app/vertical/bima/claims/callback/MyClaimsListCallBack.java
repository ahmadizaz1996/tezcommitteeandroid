package com.tez.androidapp.app.vertical.bima.claims.callback;

import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Claim;

import java.util.List;

/**
 * Created by FARHAN DHANANI on 6/23/2018.
 */
public interface MyClaimsListCallBack {

    void onMyClaimsListSuccess(List<Claim> claimList);

    void onMyClaimsListFailure(int errorCode, String message);
}
