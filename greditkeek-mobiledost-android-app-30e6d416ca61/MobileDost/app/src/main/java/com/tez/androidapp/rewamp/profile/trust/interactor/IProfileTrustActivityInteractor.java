package com.tez.androidapp.rewamp.profile.trust.interactor;

public interface IProfileTrustActivityInteractor {
    void getCnicUploads();
}
