package com.tez.androidapp.rewamp.bima.products.response.handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.bima.products.callback.ProductListCallback;
import com.tez.androidapp.rewamp.bima.products.response.ProductListResponse;

public class ProductListRH extends BaseRH<ProductListResponse> {

    @NonNull
    private ProductListCallback callback;

    public ProductListRH(BaseCloudDataStore baseCloudDataStore, @NonNull ProductListCallback callback) {
        super(baseCloudDataStore);
        this.callback = callback;
    }

    @Override
    protected void onSuccess(Result<ProductListResponse> value) {
        ProductListResponse response = value.response().body();
        if (response != null) {

            if (isErrorFree(response))
                callback.onGetProductListSuccess(response);
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());

        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        callback.onGetProductListFailure(errorCode, message);
    }
}
