package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeInstallmentPayLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeWalletVerificationLIstener;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeWalletVerificationActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.request.CommitteeInstallmentPayRequestCommittee;
import com.tez.androidapp.rewamp.committee.request.CommitteePaymentInitiateRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeInstallmentPayResponse;
import com.tez.androidapp.rewamp.committee.response.PaymentInitiateResponse;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class CommitteeWalletVerificationActivityInteractor implements ICommitteeWalletVerificationActivityInteractor {

    private final ICommitteeWalletVerificationActivityInteractorOutput iCommitteeWalletVerificationActivityInteractorOutput;

    public CommitteeWalletVerificationActivityInteractor(ICommitteeWalletVerificationActivityInteractorOutput iCommitteeWalletVerificationActivityInteractorOutput) {
        this.iCommitteeWalletVerificationActivityInteractorOutput = iCommitteeWalletVerificationActivityInteractorOutput;
    }

    @Override
    public void initiatePayment(CommitteePaymentInitiateRequest committeePaymentInitiateRequest) {
        CommitteeAuthCloudDataStore.getInstance().initiatePayment(committeePaymentInitiateRequest, new CommitteeWalletVerificationLIstener() {
            @Override
            public void onPaymentInitiateSuccess(PaymentInitiateResponse paymentInitiateResponse) {
                iCommitteeWalletVerificationActivityInteractorOutput.onPaymentInitiateSuccess(paymentInitiateResponse);
            }

            @Override
            public void onPaymentInitiateFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    initiatePayment(committeePaymentInitiateRequest);
                else
                    iCommitteeWalletVerificationActivityInteractorOutput.onPaymentInitiateFailure(errorCode, message);

            }
        });
    }

    @Override
    public void payInstallment(CommitteeInstallmentPayRequestCommittee committeeInstallmentPayRequestCommittee) {
        CommitteeAuthCloudDataStore.getInstance().payInstallment(committeeInstallmentPayRequestCommittee, new CommitteeInstallmentPayLIstener() {
            @Override
            public void onInstallmentPaySuccess(CommitteeInstallmentPayResponse committeeInstallmentPayResponse) {
                iCommitteeWalletVerificationActivityInteractorOutput.onInstallmentPaySuccess(committeeInstallmentPayResponse);

            }

            @Override
            public void onInstallmentPayFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    payInstallment(committeeInstallmentPayRequestCommittee);
                else
                    iCommitteeWalletVerificationActivityInteractorOutput.onPaymentInitiateFailure(errorCode, message);
            }
        });
    }
}
