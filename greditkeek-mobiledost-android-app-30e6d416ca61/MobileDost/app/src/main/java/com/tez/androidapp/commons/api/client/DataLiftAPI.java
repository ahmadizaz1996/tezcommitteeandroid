package com.tez.androidapp.commons.api.client;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.response.LoanLimitResponse;
import com.tez.androidapp.commons.models.network.dto.request.UploadDeviceDataRequest;
import com.tez.androidapp.commons.models.network.dto.request.UploadDynamicRequest;
import com.tez.androidapp.commons.models.network.dto.request.UploadStaticRequest;
import com.tez.androidapp.commons.utils.app.Constants;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PUT;
import retrofit2.http.Part;

/**
 * Created  on 3/2/2017.
 */

public interface DataLiftAPI {

//    @Multipart
//    @PUT(UploadStaticRequest.METHOD_NAME)
//    Call<BaseResponse> uploadStatic(@Header(Constants.HEADER_API_KEY) String apiKey, @Part MultipartBody.Part principalName,
//                                    @Part MultipartBody.Part file, @Part MultipartBody.Part imei);

    @Multipart
    @PUT(UploadDeviceDataRequest.METHOD_NAME)
    Call<LoanLimitResponse> uploadDeviceData(@Header(Constants.HEADER_API_KEY) String apiKey,
                                             @Part MultipartBody.Part principalName,
                                             @Part MultipartBody.Part imei,
                                             @Part MultipartBody.Part weeklyUpload,
                                             @Part MultipartBody.Part file);

    @Multipart
    @PUT(UploadDeviceDataRequest.METHOD_NAME)
    Call<LoanLimitResponse> uploadDeviceData(@Header(Constants.HEADER_API_KEY) String apiKey,
                                             @Part MultipartBody.Part principalName,
                                             @Part MultipartBody.Part imei,
                                             @Part MultipartBody.Part weeklyUpload,
                                             @Part  MultipartBody.Part lat,
                                             @Part  MultipartBody.Part lng,
                                             @Part MultipartBody.Part file);

    @Multipart
    @PUT(UploadDeviceDataRequest.METHOD_NAME)
    Call<BaseResponse> uploadDeviceDataWithoutLoan(@Header(Constants.HEADER_API_KEY) String apiKey, @Part MultipartBody.Part principalName, @Part MultipartBody.Part file,
                                        @Part MultipartBody.Part imei,@Part MultipartBody.Part weeklyUpload);


//    @Multipart
//    @PUT(UploadDynamicRequest.METHOD_NAME)
//    Call<BaseResponse> uploadDynamic(@Header(Constants.HEADER_API_KEY) String apiKey, @Part MultipartBody.Part principalName, @Part MultipartBody.Part file,
//                                     @Part MultipartBody.Part imei, @Part MultipartBody.Part startDate, @Part MultipartBody.Part endDate);
}
