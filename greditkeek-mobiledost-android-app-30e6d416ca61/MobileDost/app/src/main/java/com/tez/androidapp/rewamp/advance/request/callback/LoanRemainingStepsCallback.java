package com.tez.androidapp.rewamp.advance.request.callback;

import java.util.List;

public interface LoanRemainingStepsCallback {

    void onGetLoanRemainingStepsSuccess(List<String> stepsLeft);

    void onGetLoanRemainingStepsFailure(int errorCode, String message);
}
