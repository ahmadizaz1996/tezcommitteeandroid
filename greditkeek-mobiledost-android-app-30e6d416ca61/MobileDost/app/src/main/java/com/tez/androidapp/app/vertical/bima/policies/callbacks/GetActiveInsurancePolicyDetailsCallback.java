package com.tez.androidapp.app.vertical.bima.policies.callbacks;

import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.MobileUserPolicyDetailsDto;

/**
 * Created by FARHAN DHANANI on 7/18/2018.
 */
public interface GetActiveInsurancePolicyDetailsCallback {
    void onGetActiveInsurancePolicyDetailsSuccess(MobileUserPolicyDetailsDto mobileUserPolicyDetailsDto);
    void onGetActiveInsurancePolicyDetailsFailure(int errorCode, String message);
}
