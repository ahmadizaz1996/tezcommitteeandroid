package com.tez.androidapp.rewamp.advance.request.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.request.presenter.CancelLimitActivityPresenter;
import com.tez.androidapp.rewamp.advance.request.presenter.ICancelLimitActivityPresenter;
import com.tez.androidapp.rewamp.advance.request.router.CancelLimitActivityRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.BindViews;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class CancelLimitActivity extends BaseActivity implements ICancelLimitActivityView, View.OnClickListener {

    private final ICancelLimitActivityPresenter iCancelLimitActivityPresenter;

    @BindViews({
            R.id.tvCheckingLimit,
            R.id.tvHigherLimit,
            R.id.tvMoreThan6Weeks,
            R.id.tvLessThanAWeek,
            R.id.tvExpensiveLoan,
            R.id.tvOtherReason
    })
    private List<TezTextView> tvReasonList;

    @BindView(R.id.btSubmit)
    private TezButton btSubmit;

    public CancelLimitActivity() {
        iCancelLimitActivityPresenter = new CancelLimitActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_limit);
        ViewBinder.bind(this);
        init();
    }

    private void init() {
        initOnClickListeners();
    }

    private void initOnClickListeners() {
        for (TezTextView tezTextView : tvReasonList)
            tezTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int cancellationReasonId = 68;
        switch (v.getId()) {

            case R.id.tvCheckingLimit:
                cancellationReasonId = 67;
                changeBackground(R.id.tvCheckingLimit);
                break;

            case R.id.tvHigherLimit:
                cancellationReasonId = 68;
                changeBackground(R.id.tvHigherLimit);
                break;

            case R.id.tvMoreThan6Weeks:
                cancellationReasonId = 69;
                changeBackground(R.id.tvMoreThan6Weeks);
                break;

            case R.id.tvLessThanAWeek:
                cancellationReasonId = 70;
                changeBackground(R.id.tvLessThanAWeek);
                break;

            case R.id.tvExpensiveLoan:
                cancellationReasonId = 71;
                changeBackground(R.id.tvExpensiveLoan);
                break;

            case R.id.tvOtherReason:
                cancellationReasonId = 72;
                changeBackground(R.id.tvOtherReason);
                break;
        }

        final int finalCancellationReasonID = cancellationReasonId;

        btSubmit.setDoubleTapSafeOnClickListener(view -> iCancelLimitActivityPresenter.cancelLoanLimit(getLoanIdFromIntent(), finalCancellationReasonID));
        btSubmit.setVisibility(View.VISIBLE);
    }

    private void changeBackground(int viewId) {
        int filled = R.drawable.ic_cancel_limit_reason_filled;
        int empty = R.drawable.ic_cancel_limit_reasong_bg_empty;
        for (TezTextView tezTextView : tvReasonList) {
            if (tezTextView.getId() == viewId)
                tezTextView.setBackgroundResource(filled);
            else
                tezTextView.setBackgroundResource(empty);
        }
    }

    private int getLoanIdFromIntent() {
        return getIntent().getIntExtra(CancelLimitActivityRouter.LOAN_ID, -100);
    }

    @Override
    public void routeToDashboardActivity() {
        DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    protected String getScreenName() {
        return "CancelLimitActivity";
    }
}
