package com.tez.androidapp.rewamp.general.feedback.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;
import com.tez.androidapp.rewamp.general.feedback.callback.LoanFeedbackCallback;
import com.tez.androidapp.rewamp.general.feedback.request.LoanFeedbackRequest;

public class LoanFeedbackActivityInteractor implements ILoanFeedbackActivityInteractor {

    private final ILoanFeedbackActivityInteractorOutput iLoanFeedbackActivityInteractorOutput;

    public LoanFeedbackActivityInteractor(ILoanFeedbackActivityInteractorOutput iLoanFeedbackActivityInteractorOutput) {
        this.iLoanFeedbackActivityInteractorOutput = iLoanFeedbackActivityInteractorOutput;
    }

    @Override
    public void submitFeedback(@NonNull LoanFeedbackRequest loanFeedbackRequest) {
        LoanCloudDataStore.getInstance().submitLoanFeedback(loanFeedbackRequest, new LoanFeedbackCallback() {
            @Override
            public void onSubmitFeedbackSuccess(DashboardCardsResponse dashboardCardsResponse) {
                iLoanFeedbackActivityInteractorOutput.onSubmitFeedbackSuccess(dashboardCardsResponse);
            }

            @Override
            public void onSubmitFeedbackFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    submitFeedback(loanFeedbackRequest);
                else
                    iLoanFeedbackActivityInteractorOutput.onSubmitFeedbackFailure(errorCode, message);
            }
        });
    }
}
