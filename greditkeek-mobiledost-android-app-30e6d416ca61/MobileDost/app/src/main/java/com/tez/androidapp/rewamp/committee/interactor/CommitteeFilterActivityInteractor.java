package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeContributeListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeFilterListener;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeContributeActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeFilterActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeWalletResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeFilterActivityInteractor implements ICommitteeFilterInteractor {


    private final ICommitteeFilterActivityInteractorOutput iCommitteeFilterActivityInteractorOutput;

    public CommitteeFilterActivityInteractor(ICommitteeFilterActivityInteractorOutput iCommitteeFilterActivityInteractorOutput) {
        this.iCommitteeFilterActivityInteractorOutput = iCommitteeFilterActivityInteractorOutput;
    }

    @Override
    public void getFilter(String committeeId) {
        CommitteeAuthCloudDataStore.getInstance().getFilter(committeeId,new CommitteeFilterListener() {
            @Override
            public void onGetFilterSuccess(CommitteeFilterResponse committeeFilterResponse) {
                iCommitteeFilterActivityInteractorOutput.onGetFilterSuccess(committeeFilterResponse);
            }

            @Override
            public void onGetFilterFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getFilter(committeeId);
                else
                    iCommitteeFilterActivityInteractorOutput.onGetFilterFailure(errorCode, message);
            }
        });
    }
}
