package com.tez.androidapp.rewamp.general.faquestion.FAQInteractor;

import com.tez.androidapp.app.general.feature.faq.callbacks.GetFAQsCallback;
import com.tez.androidapp.app.general.feature.faq.models.network.FAQ;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.general.faquestion.presenter.IFAQInteractorOutput;

import java.util.List;

public class FAQInteractor implements IFAQInteractor {

    private final IFAQInteractorOutput ifaqInteractorOutput;


    public FAQInteractor(IFAQInteractorOutput ifaqInteractorOutput) {
        this.ifaqInteractorOutput = ifaqInteractorOutput;
    }

    @Override
    public void callForFAQs(String selectedLanguage){
        UserCloudDataStore.getInstance().getFAQs(new GetFAQsCallback(){

            @Override
            public void onGetFAQsSuccess(List<FAQ> faqs) {
                ifaqInteractorOutput.onGetFAQsSuccess(faqs);
            }

            @Override
            public void onGetFAQsError(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        ()->callForFAQs(selectedLanguage),
                        ()->ifaqInteractorOutput.onGetFAQsError(errorCode, message));

            }
        }, selectedLanguage);
    }

}
