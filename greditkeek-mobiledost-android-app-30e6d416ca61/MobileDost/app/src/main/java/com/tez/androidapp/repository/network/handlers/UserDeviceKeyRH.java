package com.tez.androidapp.repository.network.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.handlers.callbacks.UserDeviceKeyCallback;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by Rehman Murad Ali on 8/30/2017.
 */

public class UserDeviceKeyRH extends BaseRH<BaseResponse> {
    private UserDeviceKeyCallback userDeviceKeyCallback;

    public UserDeviceKeyRH(BaseCloudDataStore baseCloudDataStore, UserDeviceKeyCallback userDeviceKeyCallback) {
        super(baseCloudDataStore);
        this.userDeviceKeyCallback = userDeviceKeyCallback;

    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            userDeviceKeyCallback.onUserDeviceKeySuccess(baseResponse);
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        //Left
    }
}
