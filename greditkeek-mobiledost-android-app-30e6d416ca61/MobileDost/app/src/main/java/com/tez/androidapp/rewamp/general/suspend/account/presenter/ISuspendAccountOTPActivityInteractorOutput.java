package com.tez.androidapp.rewamp.general.suspend.account.presenter;

import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.SuspendAccountCallback;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.SuspendAccountVerifyOTPCallback;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public interface ISuspendAccountOTPActivityInteractorOutput extends SuspendAccountCallback, SuspendAccountVerifyOTPCallback {

    void onResendCodeSuccess();

    void onResendCodeFailure(int errorCode);
}
