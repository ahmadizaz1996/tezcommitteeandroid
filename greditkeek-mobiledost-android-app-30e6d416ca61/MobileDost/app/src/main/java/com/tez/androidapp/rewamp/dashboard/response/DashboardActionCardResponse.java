package com.tez.androidapp.rewamp.dashboard.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.dashboard.entity.Action;

public class DashboardActionCardResponse extends BaseResponse {

    private Action action;

    public Action getAction() {
        return action;
    }

    public boolean isSafe() {
        return getAction() != null && getAction().isSafe();
    }
}
