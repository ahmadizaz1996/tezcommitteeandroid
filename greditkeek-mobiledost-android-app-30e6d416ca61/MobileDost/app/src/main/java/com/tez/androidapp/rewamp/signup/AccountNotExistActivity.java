package com.tez.androidapp.rewamp.signup;

import android.os.Bundle;
import android.view.View;

import com.tez.androidapp.R;

public class AccountNotExistActivity extends BaseErrorActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        setTvHeadingText(R.string.account_not_exist);
        setTvMessageText(R.string.account_not_exist_desc);
        setBtMainButtonText(R.string.sign_up);
        setIvLogoImageResource(R.drawable.ic_account);
        setTvNumberVisibility(View.VISIBLE);
        setTvNumberText(getTextForTezTExtViewNumber());
    }


    private String getTextForTezTExtViewNumber() {
        return "+92 345 2134567";
    }

    @Override
    protected String getScreenName() {
        return "AccountNotExistActivity";
    }
}
