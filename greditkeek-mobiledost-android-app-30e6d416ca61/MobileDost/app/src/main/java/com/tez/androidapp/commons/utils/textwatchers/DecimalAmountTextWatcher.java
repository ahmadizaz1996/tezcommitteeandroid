package com.tez.androidapp.commons.utils.textwatchers;

import android.text.Editable;
import android.text.TextWatcher;
import com.tez.androidapp.commons.widgets.TezEditTextView;

import com.crashlytics.android.Crashlytics;

public class DecimalAmountTextWatcher implements TextWatcher {
    private static final int INDEX_NOT_FOUND_KEY = -1;
    private static final String STRING_DECIMAL = ".";
    private static final int START_INDEX = 0;
    private static final int ADJUSTMENT_NUMBER = 1;
    private static final String STRING_APPEND_ZERO_BEFORE_DECIMAL = "0.";
    private TezEditTextView mEditText;
    private int maxDecimalDigitAllowed;
    public DecimalAmountTextWatcher(TezEditTextView tezEditTextView, int maxDecimalDigitAllowed) {
        this.mEditText = tezEditTextView;
        this.maxDecimalDigitAllowed = maxDecimalDigitAllowed;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        //Left

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        try {
            if(s.toString().equals(STRING_DECIMAL)){
                mEditText.setText(STRING_APPEND_ZERO_BEFORE_DECIMAL);
                mEditText.setSelection(STRING_APPEND_ZERO_BEFORE_DECIMAL.length());
            }
            else {
                int decimalLocation = s.toString().indexOf(STRING_DECIMAL);
                int afterDecimalDigit = s.subSequence(decimalLocation + ADJUSTMENT_NUMBER, s.length()).length();
                if (decimalLocation != INDEX_NOT_FOUND_KEY && afterDecimalDigit > maxDecimalDigitAllowed) {
                    String result = s.subSequence(START_INDEX, decimalLocation + maxDecimalDigitAllowed + ADJUSTMENT_NUMBER).toString();
                    mEditText.setText(result);
                    mEditText.setSelection(result.length());
                }
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        //Left
    }
}
