package com.tez.androidapp.rewamp.general.beneficiary.presenter;

public interface IBeneficiaryListActivityPresenter {

    void getBeneficiaries();

    void setAdvanceBimaBeneficiary(int beneficiaryId, int mobileUserInsurancePolicyBeneficiaryId);

    void setDefaultBeneficiary(int beneficiaryId);
}
