package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeCreationLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeInviteListener;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeCompletionActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeInviteActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;

import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeCompletionActivityInteractor implements ICommitteeCompletionInteractor {


    private final ICommitteeCompletionActivityInteractorOutput mICommitteeCompletionActivityInteractorOutput;

    public CommitteeCompletionActivityInteractor(ICommitteeCompletionActivityInteractorOutput mICommitteeCompletionActivityInteractorOutput) {
        this.mICommitteeCompletionActivityInteractorOutput = mICommitteeCompletionActivityInteractorOutput;
    }

    @Override
    public void createCommittee(CommitteeCreateRequest committeeCreateRequest) {
        CommitteeAuthCloudDataStore.getInstance().createCommittee(committeeCreateRequest, new CommitteeCreationLIstener() {
            @Override
            public void onCommitteeCreateSuccess(CommitteeCreateResponse committeeCreateResponse) {
                mICommitteeCompletionActivityInteractorOutput.onCommitteeCreationSuccess(committeeCreateResponse);
            }

            @Override
            public void onCommitteeCreateFailed(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    createCommittee(committeeCreateRequest);
                else
                    mICommitteeCompletionActivityInteractorOutput.onCommitteeCreationFailure(errorCode, message);
            }

        });
    }
}

