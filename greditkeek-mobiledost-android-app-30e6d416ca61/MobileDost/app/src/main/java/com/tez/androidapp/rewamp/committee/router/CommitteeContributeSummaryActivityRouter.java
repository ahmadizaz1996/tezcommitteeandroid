package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeInstallmentPayResponse;
import com.tez.androidapp.rewamp.committee.response.GetInvitedPackageResponse;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeContributeSummaryActivity;
import com.tez.androidapp.rewamp.committee.view.CommitteeInviteContactsActivity;
import com.tez.androidapp.rewamp.committee.view.CommitteeSummaryActivity;

import java.security.spec.PSSParameterSpec;

import static com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter.COMMITTEE_DATA;

public class CommitteeContributeSummaryActivityRouter extends BaseActivityRouter {


    public static final String INSTALLMENT_PAY_RESPONSE = "INSTALLMENT_PAY_RESPONSE";

    public static CommitteeContributeSummaryActivityRouter createInstance() {
        return new CommitteeContributeSummaryActivityRouter();
    }


    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeContributeSummaryActivity.class);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteeInstallmentPayResponse committeeInstallmentPayResponse) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putSerializable(INSTALLMENT_PAY_RESPONSE, committeeInstallmentPayResponse);
        intent.putExtras(bundle);
        route(from, intent);
    }


}
