package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.listener.CommitteeCompletionListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeSummaryListener;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeSummaryActivityInteractorOutput extends CommitteeSummaryListener {
}
