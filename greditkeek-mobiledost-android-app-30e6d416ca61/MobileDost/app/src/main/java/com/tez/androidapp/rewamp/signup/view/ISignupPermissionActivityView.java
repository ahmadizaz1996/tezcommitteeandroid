package com.tez.androidapp.rewamp.signup.view;

import com.tez.androidapp.app.base.ui.IBaseView;

public interface ISignupPermissionActivityView extends IBaseView {

    void createSinchVerification();

    void navigateToMobileNumberAlreadyExistActivity();
}
