package com.tez.androidapp.rewamp.general.transactions;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

/**
 * Created by VINOD KUMAR on 8/28/2019.
 */
public interface IMyTransactionDetailActivityInteractor extends IBaseInteractor {

    void getLoanTransactionDetail(int transactionId);
}
