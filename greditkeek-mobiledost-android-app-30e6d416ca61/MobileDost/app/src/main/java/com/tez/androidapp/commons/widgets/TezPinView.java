package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.textwatchers.TezTextWatcher;

/**
 * Created by VINOD KUMAR on 5/17/2019.
 */
public class TezPinView extends TezEditTextView {


    private final float MIN_LINE_LENGTH = convertDpToPixel(50);
    private float spaceBetweenLines = convertDpToPixel(23);
    private float bottomSpace = convertDpToPixel(30);
    private float circleRadius = convertDpToPixel(4.5f);
    private int numDigits = 4;

    private Paint linePaint;
    private Paint circlePaint;

    @Nullable
    private OnPinEnteredListener onPinEnteredListener;

    public TezPinView(Context context) {
        super(context);
    }

    public TezPinView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TezPinView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @SuppressWarnings("unused")
    public void setOnPinEnteredListener(@Nullable OnPinEnteredListener onPinEnteredListener) {
        this.onPinEnteredListener = onPinEnteredListener;
    }

    public void setNumDigits(int numDigits) {
        this.numDigits = numDigits;
        this.setMaxDigits(numDigits);
        requestLayout();
    }

    @Override
    public void acceptOnlyDigits() {
        this.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        this.setKeyListener(DigitsKeyListener.getInstance(getResources().getString(R.string.digits)));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        switch (widthMode) {

            case MeasureSpec.EXACTLY:
                width = widthSize;
                break;

            case MeasureSpec.UNSPECIFIED:
                width = calculateWidth();
                break;

            case MeasureSpec.AT_MOST:
                width = calculateWidth();
                if (width > widthSize)
                    width = widthSize;
                break;

            default:
                width = widthSize;
        }


        switch (heightMode) {

            case MeasureSpec.EXACTLY:
                height = heightSize;
                break;

            case MeasureSpec.UNSPECIFIED:
                height = calculateHeight();
                break;

            case MeasureSpec.AT_MOST:
                height = calculateHeight();
                if (height > heightSize)
                    height = heightSize;
                break;

            default:
                height = heightSize;
        }

        this.setMeasuredDimension(width, height);
    }


    @Override
    protected void onDraw(Canvas canvas) {

        int leftStartPadding = getLeftStartPadding(this);

        int availableWidth = getWidth() - getRightEndPadding(this) - leftStartPadding;

        float lineSize = (availableWidth - (spaceBetweenLines * (numDigits - 1))) / numDigits;

        int startX = leftStartPadding;

        int startY = getHeight() - getPaddingBottom();

        float stopX = startX + lineSize;

        Editable text = getText();

        if (text != null) {

            int textLength = text.length();

            for (int i = 0; i < numDigits; i++) {

                canvas.drawLine(startX, startY, startX + lineSize, startY, linePaint);

                if (textLength > i) {
                    float middle = (startX + stopX) / 2;
                    canvas.drawCircle(middle, startY - bottomSpace, circleRadius, circlePaint);
                }

                startX += lineSize + spaceBetweenLines;
                stopX = startX + lineSize;
            }
        }
    }

    private void init(Context context, AttributeSet attrs) {
        this.initAttributes(context, attrs);
        this.initLinePaint();
        this.initCirclePaint();
        this.setMaxDigits(numDigits);
        this.disableCopyPaste();
        this.setCursorToLastPositionAlways();
        this.setCallbackToListener();
        this.setBackgroundResource(0);
        this.setCursorVisible(false);
        this.acceptOnlyDigits();
    }

    private void initAttributes(Context context, AttributeSet attrs) {

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TezPinView);

        try {

            this.numDigits = typedArray.getInt(R.styleable.TezPinView_num_digits, 4);

        } finally {
            typedArray.recycle();
        }
    }

    private void initLinePaint() {
        this.linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.linePaint.setColor(getResources().getColor(R.color.editTextBottomLineColorGrey));
        this.linePaint.setStrokeWidth(this.convertDpToPixel(2));
    }

    private void initCirclePaint() {
        this.circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.circlePaint.setColor(getResources().getColor(R.color.pinViewCircle));
    }

    private void setCallbackToListener() {
        this.addTextChangedListener(new TezTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (onPinEnteredListener != null && s.length() == numDigits) {
                    TezPinView.this.setEnabled(false);
                    onPinEnteredListener.onPinEntered(s.toString());
                }
            }
        });
    }

    private int calculateHeight() {
        float circleHeight = circleRadius * 2;
        return (int) (getPaddingBottom() + bottomSpace + circleHeight + getPaddingTop());
    }

    private int calculateWidth() {
        float measuredLinesWidth = (numDigits * MIN_LINE_LENGTH);
        float measuredLinesSpacing = (numDigits - 1) * spaceBetweenLines;
        return (int) (getLeftStartPadding(this) + measuredLinesWidth + measuredLinesSpacing + getRightEndPadding(this));
    }

    private float convertDpToPixel(float dp) {
        return dpToPx(getContext(), dp);
    }


    @FunctionalInterface
    public interface OnPinEnteredListener {

        void onPinEntered(@NonNull String pin);

    }
}
