package com.tez.androidapp.rewamp.signup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.tez.androidapp.BuildConfig;
import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.change.ip.activities.ChangeIPActivity;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.commons.widgets.TezToolbar;
import com.tez.androidapp.rewamp.committee.router.MyCommitteeActivityRouter;
import com.tez.androidapp.rewamp.committee.view.CommitteeActivity;
import com.tez.androidapp.rewamp.dashboard.view.DashboardActivity;
import com.tez.androidapp.rewamp.general.faquestion.router.FAQActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.fragment.util.optional.Optional;
import net.tez.fragment.util.optional.TextUtil;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class SelectLanguageActivity extends BaseActivity implements DoubleTapSafeOnClickListener {

    @BindView(R.id.buttonUrdu)
    private TezButton buttonUrdu;

    @BindView(R.id.buttonRomanUrdu)
    private TezButton buttonRomanUrdu;

    @BindView(R.id.buttonEnglish)
    private TezButton buttonEnglish;

    @BindView(R.id.selectLanguageToolbar)
    private TezToolbar tezToolbar;

    @BindView(R.id.textViewHeading)
    private TezTextView textViewHeading;

    @BindView(R.id.textViewFaq)
    private TezTextView textViewFaq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        ViewBinder.bind(this);
        this.init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isEnableButtonOnStart())
            this.setEnableButtons(true);
        initTextViews();
    }

    private void initTextViews() {
        this.textViewFaq.setText(R.string.faqs);
        this.textViewHeading.setText(R.string.select_language);
    }


    protected final void setEnableButtons(boolean enableButtons) {
        buttonUrdu.setEnabled(enableButtons);
        buttonEnglish.setEnabled(enableButtons);
        buttonRomanUrdu.setEnabled(enableButtons);
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        this.setEnableButtons(false);
        switch (view.getId()) {

            case R.id.buttonUrdu:
                LocaleHelper.setUrduLanguage(this);
                break;

            case R.id.buttonRomanUrdu:
                LocaleHelper.setRomanUrduLanguage(this);
                break;

            case R.id.buttonEnglish:
                ///Intent intent = new Intent(MobileDostApplication.getInstance(), DashboardActivity.class);
                Intent intent = new Intent(MobileDostApplication.getInstance(), NewIntroActivity.class);// call login screen
                startActivity(intent);
//                LocaleHelper.setEnglishLanguage(this);
                break;

            default:
                LocaleHelper.setRomanUrduLanguage(this);
        }

//        this.setSelectedLanguage();
//        MDPreferenceManager.setPrefIsUserLangRecorded(true);
//        TezOnBoardingActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    private void init() {
        this.initClickListeners();
        this.setChangeIpListener();
        this.checkIfSetSelectedLanguage();
    }

    private void initClickListeners() {
        buttonEnglish.setDoubleTapSafeOnClickListener(this);
        buttonRomanUrdu.setDoubleTapSafeOnClickListener(this);
        buttonUrdu.setDoubleTapSafeOnClickListener(this);
        this.textViewFaq.setDoubleTapSafeOnClickListener(this::routeToFaqActivity);
    }

    private void routeToFaqActivity(View v) {
        FAQActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    protected void setSelectedLanguage() {
        String selectedLang = LocaleHelper.getSelectedLanguage(this);
        switch (selectedLang) {

            case LocaleHelper.ENGLISH:
                onEnglishSelected();
                break;

            case LocaleHelper.ROMAN:
                onRomanSelected();
                break;

            case LocaleHelper.URDU:
                onUrduSelected();
                break;
        }
    }

    protected void checkIfSetSelectedLanguage() {
        Optional.doWhen(MDPreferenceManager.getPrefIsUserLangRecorder(), this::setSelectedLanguage);
    }

    private void onEnglishSelected() {
        setButtonSelected(buttonEnglish);
        setButtonWhite(buttonUrdu);
        setButtonWhite(buttonRomanUrdu);
    }

    private void onRomanSelected() {
        setButtonSelected(buttonRomanUrdu);
        setButtonWhite(buttonEnglish);
        setButtonWhite(buttonUrdu);
    }

    private void onUrduSelected() {
        setButtonSelected(buttonUrdu);
        setButtonWhite(buttonRomanUrdu);
        setButtonWhite(buttonEnglish);
    }

    protected boolean isEnableButtonOnStart() {
        return true;
    }

    protected void setButtonSelected(TezButton button) {
        button.setButtonBackgroundStyle(TezButton.ButtonStyle.NORMAL);
    }

    protected void setButtonWhite(TezButton button) {
        button.setButtonBackgroundStyle(TezButton.ButtonStyle.WHITE);
    }

    protected void setChangeIpListener() {
        Optional.doWhen(TextUtil.equals(BuildConfig.FLAVOR, "qa") || TextUtil.equals(BuildConfig.FLAVOR, "dev"),
                () -> this.tezToolbar.setOnClickListener(new View.OnClickListener() {
                    private int taps = 0;

                    @Override
                    public void onClick(View v) {
                        taps++;
                        Optional.doWhen(taps == 5, () -> {
                            taps = 0;
                            startActivity(new Intent(SelectLanguageActivity.this, ChangeIPActivity.class));
                        });
                    }
                }));
    }

    protected void enableToolbarBackButton() {
        this.tezToolbar.showBack(true);

    }

    @Override
    protected String getScreenName() {
        return "SelectLanguageActivity";
    }
}
