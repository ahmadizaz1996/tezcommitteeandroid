package com.tez.androidapp.commons.utils.textwatchers;

import android.content.Context;
import android.graphics.PorterDuff;

import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.widgets.TezImageView;
import android.view.View;
import android.widget.EditText;

import com.tez.androidapp.R;

public class ColourChangeFocusListener implements View.OnFocusChangeListener {
    private TezImageView imageView;

    public ColourChangeFocusListener(TezImageView imageView) {
        this.imageView = imageView;
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        Context context = MobileDostApplication.getAppContext();
        EditText editText = (EditText) view;
        int color = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? context.getColor(R.color
                .colorGreen) : context.getResources().getColor(R.color.colorGreen);
        if (b) {
            imageView.getDrawable().mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        } else if (editText.getText().toString().length() == 0) {
            imageView.getDrawable().mutate().setColorFilter(null);
        }
    }
}
