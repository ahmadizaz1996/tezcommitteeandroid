package com.tez.androidapp.rewamp.signup.view;

import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.general.changepin.router.ChangeTemporaryPinActivityRouter;
import com.tez.androidapp.rewamp.reactivate.account.router.AccountReactivateActivityRouter;
import com.tez.androidapp.rewamp.signup.presenter.INewLoginActivityPresenter;
import com.tez.androidapp.rewamp.signup.presenter.NewLoginActivityPresenter;
import com.tez.androidapp.rewamp.signup.router.LoginNumberVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.router.RequestFailedActivityRouter;
import com.tez.androidapp.rewamp.signup.verification.NumberVerificationActivity;

import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener;

public class LoginNumberVerificationActivity extends NumberVerificationActivity
        implements INewLoginActivityView {

    private final INewLoginActivityPresenter iNewLoginActivityPresenter;
    private boolean userPinIsVerified;
    private boolean isTemporaryPinSet;
    private boolean isUserAccountReactivated;
    private User user;

    @Override
    public void onAppStart(boolean inactiveTimeExceeded) {

    }

    public LoginNumberVerificationActivity() {
        iNewLoginActivityPresenter = new NewLoginActivityPresenter(this);
    }

    @Override
    public void onVerified() {
        super.onVerified();
        if(userPinIsVerified){
            MDPreferenceManager.setUser(user);
            MDPreferenceManager.setUserSignedUp(true);
        }
        performNavigation();
    }


    @Override
    public void onCountDownFinish() {
        super.onCountDownFinish();
        setEnableEtMobileNumber(false);
    }

    @Override
    public boolean canStartNumberVerification() {
        return this.userPinIsVerified;
    }

    @Override
    public void preliminaryNumberVerificationWork() {
        super.preliminaryNumberVerificationWork();
        setEnableEtMobileNumber(false);
        startLogin();
    }

    @Override
    protected String getFlashCallLabel() {
        return "LOGIN-USER";
    }

    private String getPin() {
        return getIntent().getStringExtra(LoginNumberVerificationActivityRouter.PIN);
    }

    private void startLogin() {
        showTezLoader();
        UserLoginRequest loginRequest = Utility.createLoginRequest(this,
                getMobileNumberFromIntent(), getPin());
        loginRequest.setSocialId(getSocialId());
        loginRequest.setSocialType(getSocialType());
        this.iNewLoginActivityPresenter.callLogin(loginRequest);
    }

    private String getSocialId() {
        return getIntent().getStringExtra(LoginNumberVerificationActivityRouter.SOCIAL_ID);
    }

    private Integer getSocialType() {
        return getIntent().getIntExtra(LoginNumberVerificationActivityRouter.SOCIAL_TYPE,
                Utility.PrincipalType.MOBILE_NUMBER.getValue());
    }


    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }


    @Override
    public void startRequestFailedActivity() {
        setTezLoaderToBeDissmissedOnTransition();
        RequestFailedActivityRouter.createInstance().setDependenciesAndRoute(this,
                R.string.seems_you_have_forgotten_pin,
                R.string.try_reentering_after_thirty_minutes);
        finish();
    }

    @Override
    public void onTemporaryPinSet() {
        this.isTemporaryPinSet = true;
        this.userPinIsVerified();
    }

    @Override
    public void onUserReactivateAccount(){
        this.isUserAccountReactivated = true;
        this.userPinIsVerified();
    }

    @Override
    public void userPinIsVerified() {
        this.userPinIsVerified = true;
        this.user = MDPreferenceManager.getUser();
        MDPreferenceManager.removeUser();
        MDPreferenceManager.setUserSignedUp(false);
        startNumberVerification();
    }

    @Override
    public DoubleTapSafeDialogClickListener getDialogOnClickListener() {
        return (dialog, which) -> finishActivity();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    public void onUserSessionTimeOut() {

    }

    private void startDashboard() {
        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void startUserReactiviateAccount(){

        setTezLoaderToBeDissmissedOnTransition();
        AccountReactivateActivityRouter.createInstance().setDependenciesAndRoute(this);
        finish();
    }

    private void startTemporaryPinChange() {
        setTezLoaderToBeDissmissedOnTransition();
        ChangeTemporaryPinActivityRouter.createInstance().setDependenciesAndRoute(this);
        finish();
    }

    private void performNavigation(){
        if(isTemporaryPinSet)
            this.startTemporaryPinChange();
        else if(isUserAccountReactivated)
            this.iNewLoginActivityPresenter.resendOtp();
        else
            this.startDashboard();
    }

    @Override
    protected String getScreenName() {
        return "LoginNumberVerificationActivity";
    }
}


