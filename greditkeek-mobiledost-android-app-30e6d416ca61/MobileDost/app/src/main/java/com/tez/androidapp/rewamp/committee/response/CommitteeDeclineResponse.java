package com.tez.androidapp.rewamp.committee.response;

import com.tez.androidapp.app.base.response.BaseResponse;

import java.io.Serializable;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeDeclineResponse extends BaseResponse implements Serializable {

    private boolean response;

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }
}
