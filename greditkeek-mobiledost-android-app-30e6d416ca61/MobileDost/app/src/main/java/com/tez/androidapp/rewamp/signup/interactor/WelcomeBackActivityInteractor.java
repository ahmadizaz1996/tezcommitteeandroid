package com.tez.androidapp.rewamp.signup.interactor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserLoginCallback;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserVerifyCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserVerifyRequest;
import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserVerifyResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.handlers.callbacks.VerifyActiveDeviceCallback;
import com.tez.androidapp.repository.network.models.request.VerifyActiveDeviceRequest;
import com.tez.androidapp.repository.network.models.response.VerifyActiveDeviceResponse;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.signup.presenter.IWelcomeBackActivityInteractorOutput;

public class WelcomeBackActivityInteractor implements IWelcomeBackActivityInteractor {

    private final IWelcomeBackActivityInteractorOutput iWelcomeBackActivityNumberVerificationInteractorOutput;

    public WelcomeBackActivityInteractor(IWelcomeBackActivityInteractorOutput iWelcomeBackActivityNumberVerificationInteractorOutput){
        this.iWelcomeBackActivityNumberVerificationInteractorOutput = iWelcomeBackActivityNumberVerificationInteractorOutput;
    }

    @Override
    public void verifyUserActiveDevice(@NonNull VerifyActiveDeviceRequest verifyActiveDeviceRequest, String pin) {
        UserCloudDataStore.getInstance().verifyActiveDevice(verifyActiveDeviceRequest, new VerifyActiveDeviceCallback() {
            @Override
            public void onVerifyActiveDeviceSuccess(VerifyActiveDeviceResponse verifyActiveDeviceResponse) {
                iWelcomeBackActivityNumberVerificationInteractorOutput.onVerifyActiveDeviceSuccess(verifyActiveDeviceResponse, pin);
            }

            @Override
            public void onVerifyActiveDeviceFailure(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        ()->verifyUserActiveDevice(verifyActiveDeviceRequest, pin),
                        ()->iWelcomeBackActivityNumberVerificationInteractorOutput.onVerifyActiveDeviceFailure(errorCode, message));
                        //()->iWelcomeBackActivityNumberVerificationInteractorOutput.onVerifyActiveDeviceSuccess(mock(), pin));
            }
        });
    }

    @Override
    public void login(@NonNull UserLoginRequest userLoginRequest, boolean callDeviceKey, String imei) {
        UserCloudDataStore.getInstance().login(userLoginRequest, new UserLoginCallback() {
            @Override
            public void onUserLoginSuccess(UserLoginResponse loginResponse) {
                setUserDeviceKey(callDeviceKey, imei);
                iWelcomeBackActivityNumberVerificationInteractorOutput.onUserLoginSuccess(loginResponse);
            }

            @Override
            public void onUserLoginFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    login(userLoginRequest,callDeviceKey,imei);
                else
                    iWelcomeBackActivityNumberVerificationInteractorOutput.onUserLoginFailure(errorCode, message);
            }
        });
    }

    private void setUserDeviceKey(final boolean callDeviceKey, @Nullable final String imei) {
        String principleName = MDPreferenceManager.getPrincipalName();
        String deviceKey = MDPreferenceManager.getFirebaseKey();
        Optional.doWhen(callDeviceKey && imei !=null,
                ()->UserCloudDataStore.getInstance()
                        .setUserDeviceKey(principleName, deviceKey, imei, this));
    }

    @Override
    public void onUserDeviceKeySuccess(BaseResponse baseResponse) {
        Optional.doWhen(baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR,
                ()->MDPreferenceManager.setDeviceKeySentToServer(true),
                ()->onUserDeviceKeyFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription()));
    }

    @Override
    public void onUserDeviceKeyFailure(int errorCode, String message) {
        MDPreferenceManager.setDeviceKeySentToServer(false);
    }

    @Override
    public void verifyUser(@NonNull UserVerifyRequest userVerifyRequest, boolean callDeviceKey, String imei) {
        UserAuthCloudDataStore.getInstance().verify(userVerifyRequest, new UserVerifyCallback() {
            @Override
            public void onUserVerifySuccess(UserVerifyResponse userVerifyResponse) {
                setUserDeviceKey(callDeviceKey, imei);
                iWelcomeBackActivityNumberVerificationInteractorOutput.onUserVerifySuccess(userVerifyResponse);
            }

            @Override
            public void onUserVerifyFailure(BaseResponse baseResponse) {
                if (Utility.isUnauthorized(baseResponse))
                    verifyUser(userVerifyRequest,callDeviceKey, imei);
                else
                    iWelcomeBackActivityNumberVerificationInteractorOutput.onUserVerifyFailure(baseResponse);
            }
        });
    }
}
