package com.tez.androidapp.commons.utils.network;

import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Log;
import com.tez.androidapp.commons.utils.app.Utility;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created  on 9/9/2017.
 */

public class APIKeyHeaderInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        Log.e("APIKeyHeaderInterceptor",originalRequest.url().toString());
        Log.e("APIKeyHeaderInterceptor", Utility.getAPIKey());
        //Request newRequest = originalRequest.newBuilder().header(Constants.HEADER_API_KEY, Utility.getAPIKey()).build();
        //return chain.proceed(newRequest);
        return null;
    }

}
