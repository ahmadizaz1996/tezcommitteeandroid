package com.tez.androidapp.rewamp.committee.view;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.models.extract.ExtractedContact;
import com.tez.androidapp.commons.utils.app.Log;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.utils.extract.ExtractionUtil;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezCheckBox;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.adapter.CommitteeInviteContactsAdapter;
import com.tez.androidapp.rewamp.committee.adapter.CommitteeInviteSelectedContactsAdapter;
import com.tez.androidapp.rewamp.committee.listener.IPermissionListener;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static android.telephony.PhoneNumberUtils.formatNumber;
import static com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter.COMMITTEE_DATA;

public class CommitteeInviteContactsActivity extends BaseActivity implements CommitteeInviteContactsAdapter.ContactsListener, DoubleTapSafeOnClickListener, CommitteeInviteSelectedContactsAdapter.ContactsListener {

    public static final String INVITEES_LIST = "INVITEES_LIST";
    @BindView(R.id.contacts_recycler)
    protected RecyclerView rvContacts;

    @BindView(R.id.contactsSelection_recycler)
    protected RecyclerView contactSelectionRecyclerView;

    @BindView(R.id.shimmer)
    protected TezLinearLayout shimmerLayout;

    @BindView(R.id.btDone)
    protected TezButton btDone;

    @BindView(R.id.tvContactSelectionCount)
    protected TezTextView tvContactSelectionCount;

    @BindView(R.id.searchEditText)
    protected TezEditTextView searchEditText;


    ArrayList<ExtractedContact> extractedContactArrayList = new ArrayList<>();
    HashMap<String, ExtractedContact> extractedContactHashMap = new HashMap<>();
    private CommitteePackageModel committeePackage;
    private CommitteeInviteSelectedContactsAdapter committeeInviteSelectedContactsAdapter;
    private CommitteeInviteContactsAdapter committeeInviteContactsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee_invite_contacts);
        ViewBinder.bind(this);
        btDone.setDoubleTapSafeOnClickListener(this);
        fetchExtras();
        askPermissionAndFetchContacts();
        addTextWatcher();
    }

    private void addTextWatcher() {
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (committeeInviteContactsAdapter != null)
                    committeeInviteContactsAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void fetchExtras() {
        Intent intent = getIntent();
        if (intent != null
                && intent.getExtras() != null
                && intent.getExtras().containsKey(COMMITTEE_DATA)) {
            committeePackage = (CommitteePackageModel) intent.getExtras().getSerializable(COMMITTEE_DATA);
        }
    }

    private void askPermissionAndFetchContacts() {
        Utility.fetchContacts(this, new IPermissionListener() {
            @Override
            public void onPermissionGranted() {
                setRecyclerView();
                setContactSelectionRecyclerView();
            }
        });
    }

    private List<ExtractedContact> fetchAllContacts() {
        HashMap<String, ExtractedContact> extractedContactHashMap = new HashMap<>();
        ExtractionUtil extractionUtil = new ExtractionUtil(this);
        List<ExtractedContact> extractedContactList = new LinkedList<>();
        for (ExtractedContact extractedContact : extractionUtil.extractContactsObject()) {
            Log.d("Contact", extractedContact.getDisplayName() + " : " + extractedContact.getPhone());
            if (isPhoneNoValid(extractedContact.getPhone())) {
                String number = extractedContact.getPhone().replaceAll("[^0-9]", "").replaceAll(" ", "");
                String formattedNumber = "";
                if (number.startsWith("92"))
                    formattedNumber = number.replaceFirst("92", "0");
                else if (number.startsWith("+92"))
                    formattedNumber = number.replaceFirst("\\+92", "0");
                else if (number.startsWith("03"))
                    formattedNumber = number;
                if (formattedNumber.length() == 11) {
                    extractedContact.setPhone(formattedNumber);
                    extractedContactList.add(extractedContact);
                }
            }
        }
        List<ExtractedContact> filteredExtractedContactList = new LinkedList<>();
        for (ExtractedContact extractedContact : extractedContactList) {
            if (!extractedContactHashMap.containsKey(extractedContact.getPhone())) {
                filteredExtractedContactList.add(extractedContact);
                extractedContactHashMap.put(extractedContact.getPhone(), extractedContact);
            }
        }

        Collections.sort(filteredExtractedContactList, new Comparator<ExtractedContact>() {
            public int compare(ExtractedContact v1, ExtractedContact v2) {
                return v1.getDisplayName().toLowerCase().compareTo(v2.getDisplayName().toLowerCase());
            }
        });


        return filteredExtractedContactList;
    }

    public boolean isPhoneNoValid(String phoneNumber) {
        if (phoneNumber != null
                && phoneNumber.length() > 0
                && (phoneNumber.startsWith("03")
                || phoneNumber.startsWith("92")
                || phoneNumber.startsWith("+92"))) {

            return true;
        }

        return false;
    }

    public void setRecyclerView() {
        ArrayList<ExtractedContact> contactsModelArrayList = new ArrayList<>(fetchAllContacts());
        if (contactsModelArrayList.size() > 0) {
            shimmerLayout.setVisibility(View.GONE);
            rvContacts.setVisibility(View.VISIBLE);
            committeeInviteContactsAdapter = new CommitteeInviteContactsAdapter(getAlphabeticArray(contactsModelArrayList), this, committeePackage.getSelectedMembers() - 1);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            this.rvContacts.setLayoutManager(linearLayoutManager);
            this.rvContacts.setAdapter(committeeInviteContactsAdapter);
        } else {
            shimmerLayout.setVisibility(View.VISIBLE);
            rvContacts.setVisibility(View.GONE);
        }
    }

    private List<ExtractedContact> getAlphabeticArray(ArrayList<ExtractedContact> contactsModelArrayList) {
        ArrayList<ExtractedContact> _extractedContacts = new ArrayList<>();
        String lastAlphabet = "";
        try {
            for (ExtractedContact extractedContact : contactsModelArrayList) {
                if (!(lastAlphabet.equalsIgnoreCase(String.valueOf(extractedContact.getDisplayName().charAt(0))))) {
                    lastAlphabet = String.valueOf(extractedContact.getDisplayName().charAt(0));
                    ExtractedContact extractedContact1 = new ExtractedContact(lastAlphabet, true);
                    _extractedContacts.add(extractedContact1);
                }
                _extractedContacts.add(extractedContact);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _extractedContacts;
    }

    public void setContactSelectionRecyclerView() {
        try {
            this.contactSelectionRecyclerView.setVisibility(View.GONE);
            committeeInviteSelectedContactsAdapter = new CommitteeInviteSelectedContactsAdapter(extractedContactArrayList, this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            this.contactSelectionRecyclerView.setLayoutManager(linearLayoutManager);
            this.contactSelectionRecyclerView.setAdapter(committeeInviteSelectedContactsAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String getScreenName() {
        return CommitteeInviteContactsActivity.class.getSimpleName();
    }

    @Override
    public void onClickContact(@NonNull ExtractedContact contactsModel, TezCheckBox contactCheckBox, boolean isCheckbox) {
        boolean isEligible = extractedContactHashMap.size() < committeePackage.getSelectedMembers() - 1;

/*        if (!isCheckbox) {
            if (contactCheckBox.isChecked()) {
                contactCheckBox.setChecked(false);
                contactsModel.setSelected(false);
            } else {
                if (isEligible) {
                    contactCheckBox.setChecked(true);
                    contactsModel.setSelected(true);
                } else {
                    contactsModel.setSelected(false);
                }
            }
        }*/

        /*if (!isEligible) {
            contactCheckBox.setChecked(false);
            contactsModel.setSelected(false);
        } else {
            contactsModel.setSelected(true);
        }*/

        if (extractedContactHashMap.containsKey(contactsModel.getPhone())) {
            extractedContactHashMap.remove(contactsModel.getPhone());
            committeeInviteSelectedContactsAdapter.remove(contactsModel);
            Log.d("CONTACT_ADAPTER", " : removing from hash");
            if (extractedContactHashMap.size() == 0) {
                this.contactSelectionRecyclerView.setVisibility(View.GONE);
                Log.d("CONTACT_ADAPTER", " : gone");
            }
        } else {
            if (isEligible) {
                extractedContactHashMap.put(contactsModel.getPhone(), contactsModel);
                this.contactSelectionRecyclerView.setVisibility(View.VISIBLE);
                committeeInviteSelectedContactsAdapter.add(contactsModel);
                Log.d("CONTACT_ADAPTER", " : visible");
            } else {
                Toast.makeText(this, R.string.invite_member_warning, Toast.LENGTH_SHORT).show();
            }
        }

        /*if (extractedContactHashMap.size() == 1)
            tvContactSelectionCount.setText(extractedContactHashMap.size() + " Contact Selected");
        else
            tvContactSelectionCount.setText(extractedContactHashMap.size() + " Contacts Selected");*/

    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {
            case R.id.btDone:
                if (extractedContactHashMap.size() == committeePackage.getSelectedMembers() - 1)
                    sendInvite();
                else
                    Toast.makeText(this, "Please select " + (committeePackage.getSelectedMembers() - 1) + " contact", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void sendInvite() {
        CommitteeInviteRequest committeeInviteRequest = new CommitteeInviteRequest();
        for (ExtractedContact extractedContact : extractedContactHashMap.values()) {
            CommitteeInviteRequest.InvitesRequest invitesRequest = new CommitteeInviteRequest.InvitesRequest();
            invitesRequest.contactNumber = extractedContact.getPhone();
            committeeInviteRequest.invitesRequests.add(invitesRequest);
        }

        Intent output = new Intent();
        Bundle args = new Bundle();
        args.putParcelable(INVITEES_LIST, committeeInviteRequest);
        output.putExtras(args);
        setResult(RESULT_OK, output);
        finish();
    }

    @Override
    public void onClickContact(@NonNull ExtractedContact contactsModel) {
        if (extractedContactHashMap.containsKey(contactsModel.getPhone())) {
            extractedContactHashMap.remove(contactsModel.getPhone());
            committeeInviteSelectedContactsAdapter.remove(contactsModel);
            committeeInviteContactsAdapter.deSelect(contactsModel);
            Log.d("CONTACT_ADAPTER", " : removing from hash");
            if (extractedContactHashMap.size() == 0) {
                this.contactSelectionRecyclerView.setVisibility(View.GONE);
                Log.d("CONTACT_ADAPTER", " : gone");
            }
        }
    }
}