package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.TezDrawableHelper;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.fragment.util.optional.TextUtil;

/**
 * Created by FARHAN DHANANI on 1/9/2019.
 */
public class TezButton extends AppCompatButton implements IBaseWidget {

    public TezButton(Context context) {
        super(context);
    }

    public TezButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public TezButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attributeSet) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet, R.styleable.TezButton);
        try {
            Drawable start = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezButton_drawableStartCompat);
            Drawable end = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezButton_drawableEndCompat);
            Drawable top = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezButton_drawableTopCompat);
            Drawable bottom = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezButton_drawableBottomCompat);
            changeDrawable(this, start, top, end, bottom);

            setButtonBackgroundStyle(typedArray.getInt(R.styleable.TezButton_button_style, 0));
            int clickEffect = typedArray.getInt(R.styleable.TezButton_click_effect, NO_EFFECT);
            attachClickEffect(clickEffect, this);
        } finally {
            typedArray.recycle();
        }
    }

    public void setButtonBackgroundStyle(@NonNull ButtonStyle style) {
        setButtonBackgroundStyle(style.ordinal());
    }

    private void setButtonBackgroundStyle(int buttonStyle) {

        switch (buttonStyle) {

            case 1:
                this.setButtonInactive();
                break;

            case 2:
                this.setButtonWhite();
                break;

            case 3:
                this.setButtonWithoutBackground();
                break;

            case 0:
            default:
                this.setButtonNormal();
                break;

        }
    }

    public void setButtonInactive() {
        this.setBackgroundDrawable(TezDrawableHelper.getDrawable(getContext(), R.drawable.tez_button_inactive));
        this.setTextColor(getColor(getContext(), R.color.buttonTextColorWhite));
    }

    public void setButtonWhite() {
        int[][] states = new int[][]{
                new int[]{android.R.attr.state_pressed},  // pressed
                new int[]{-android.R.attr.state_pressed} // enabled
        };
        int[] colors = new int[]{
                getColor(getContext(), R.color.buttonTextColorWhite),
                getColor(getContext(), R.color.buttonTextColorBlack)
        };
        this.setBackgroundDrawable(TezDrawableHelper.getDrawable(getContext(), R.drawable.tez_button_white));
        this.setTextColor(new ColorStateList(states, colors));
    }

    public void setButtonWithoutBackground() {
        int[][] states = new int[][]{
                new int[]{android.R.attr.state_pressed},  // pressed
                new int[]{-android.R.attr.state_pressed} // enabled
        };
        int[] colors = new int[]{
                getColor(getContext(), R.color.buttonTextColorWhite),
                getColor(getContext(), R.color.buttonTextColorWhite)
        };

        this.setBackgroundDrawable(TezDrawableHelper.getDrawable(getContext(), R.drawable.tez_without_bg_button));
        this.setTextColor(new ColorStateList(states, colors));
    }

    public void setButtonNormal() {
        this.setBackgroundDrawable(TezDrawableHelper.getDrawable(getContext(), R.drawable.tez_button_normal));
        this.setTextColor(getColor(getContext(), R.color.buttonTextColorWhite));
    }

    @NonNull
    @Override
    public String getLabel() {
        return TextUtil.isNotEmpty(getText()) ? getText().toString() : "";
    }

    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        super.setOnClickListener(doubleTapSafeOnClickListener);
    }

    public enum ButtonStyle {

        NORMAL,
        INACTIVE,
        WHITE

    }
}
