package com.tez.androidapp.rewamp.bima.products.callback;

import com.tez.androidapp.rewamp.bima.products.response.ProductTermConditionsResponse;

public interface ProductTermConditionsCallback {

    void onGetTermsConditionsSuccess(ProductTermConditionsResponse response);

    void onGetTermsConditionsFailure(int errorCode, String message);
}
