package com.tez.androidapp.rewamp.signup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class NumberVerifyDialogFragment extends DialogFragment {

    @BindView(R.id.tvTimer)
    private TezTextView tvTimer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_number_verify, container, false);
        ViewBinder.bind(this, view);
        return view;
    }
}
