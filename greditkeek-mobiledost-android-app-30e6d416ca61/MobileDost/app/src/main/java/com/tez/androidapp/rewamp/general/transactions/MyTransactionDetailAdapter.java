package com.tez.androidapp.rewamp.general.transactions;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.tezrecycler.adapter.GenericDropDownAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.viewholder.GenericDropDownAdapterViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

/**
 * Created by VINOD KUMAR on 8/29/2019.
 */
public class MyTransactionDetailAdapter extends GenericDropDownAdapter<MyTransactionDetail,
        BaseRecyclerViewListener,
        MyTransactionDetailAdapter.MyTransactionDetailViewHolder> {


    MyTransactionDetailAdapter(@NonNull List<MyTransactionDetail> items) {
        super(items);
    }

    @NonNull
    @Override
    public MyTransactionDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.list_item_my_transaction_detail, parent);
        return new MyTransactionDetailViewHolder(this, view);
    }

    class MyTransactionDetailViewHolder extends GenericDropDownAdapterViewHolder<MyTransactionDetail, BaseRecyclerViewListener> {

        @BindView(R.id.tvTitle)
        private TezTextView tvTitle;

        @BindView(R.id.tvDate)
        private TezTextView tvDate;

        @BindView(R.id.tvPaidAmount)
        private TezTextView tvPaidAmount;

        @BindView(R.id.tvWallet)
        private TezTextView tvWallet;

        @BindView(R.id.tvWalletNumber)
        private TezTextView tvWalletNumber;

        @BindView(R.id.tvTransactionId)
        private TezTextView tvTransactionId;

        @BindView(R.id.ivDropDown)
        private TezImageView ivDropDown;

        @BindView(R.id.cvTransactionDetail)
        private TezCardView cvTransactionDetail;

        @BindView(R.id.cvTransactionOverview)
        private TezCardView cvTransactionOverview;

        private MyTransactionDetailViewHolder(GenericDropDownAdapter adapter, View itemView) {
            super(adapter, itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void bindItem(int position, List<MyTransactionDetail> items, @Nullable BaseRecyclerViewListener listener) {
            super.bindItem(position, items, listener);
            MyTransactionDetail item = items.get(position);
            tvTitle.setText(getTransactionNumber(items.size(), position));
            tvWalletNumber.setText(item.getWalletNumber());
            tvWallet.setText(item.getWalletName());
            tvDate.setText(item.getRepaymentDate());
            tvPaidAmount.setText(itemView.getContext().getString(R.string.rs_value_string, item.getAmountPaid()));
            tvTransactionId.setText(item.getTransactionId());
        }

        private String getTransactionNumber(int size, int position) {
            int transactionNumber = size - position;
            int unitDigit = transactionNumber % 10;
            int tens = transactionNumber % 100;
            if (tens == 11 || tens == 12 || tens == 13)
                unitDigit = tens;
            String transaction = itemView.getContext().getString(R.string.transaction);
            switch (unitDigit) {

                case 1:
                    return transactionNumber + "st " + transaction;

                case 2:
                    return transactionNumber + "nd " + transaction;

                case 3:
                    return transactionNumber + "rd " + transaction;

                default:
                    return transactionNumber + "th " + transaction;
            }
        }

        @NonNull
        @Override
        protected View getHeader() {
            return itemView.findViewById(R.id.cvTransactionOverview);
        }

        @Override
        protected void onOpen() {
            cvTransactionDetail.setVisibility(View.VISIBLE);
            cvTransactionOverview.setCardElevation(Utility.dpToPx(itemView.getContext(), 8));
            ivDropDown.setRotation(180);
            cvTransactionDetail.requestFocus();
        }

        @Override
        protected void onClose() {
            cvTransactionDetail.setVisibility(View.GONE);
            cvTransactionOverview.setCardElevation(Utility.dpToPx(itemView.getContext(), 4));
            ivDropDown.setRotation(0);
        }
    }
}
