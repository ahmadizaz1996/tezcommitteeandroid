package com.tez.androidapp.rewamp.bima.products.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class ProductPolicyRequest extends BaseRequest {

    public static final String METHOD_NAME = "/api/v1/insurance/user/policy/{" + Params.ID + "}";

    public static final class Params {

        public static final String ID = "id";
    }
}
