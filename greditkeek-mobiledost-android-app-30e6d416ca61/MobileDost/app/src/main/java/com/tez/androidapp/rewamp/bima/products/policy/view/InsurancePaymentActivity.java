package com.tez.androidapp.rewamp.bima.products.policy.view;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.playback.TezCountDownTimer;
import com.tez.androidapp.commons.receivers.SMSReceiver;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.advance.repay.presenter.IVerifyRepaymentActivityPresenter;
import com.tez.androidapp.rewamp.advance.repay.presenter.VerifyRepaymentActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.IInsurancePaymentActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.InsurancePaymentActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.router.EasyPaisaInsurancePaymentActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.InsurancePaymentActivityRouter;

public class InsurancePaymentActivity
        extends EasyPaisaInsurancePaymentActivity implements IInsurancePaymentActivityView {

    private final IInsurancePaymentActivityPresenter iInsurancePaymentActivityPresenter;


    private final SMSReceiver smsReceiver = new SMSReceiver(this::onOtpReceived);


    public InsurancePaymentActivity() {
        iInsurancePaymentActivityPresenter = new InsurancePaymentActivityPresenter(this);
        setIVerifyEasyPaisaRepaymentActivityPresenter(iInsurancePaymentActivityPresenter);
    }

    @Override
    protected InitiatePurchasePolicyRequest getInitiatePurchasePolicyRequest() {
        return getIntent().getParcelableExtra(InsurancePaymentActivityRouter.PRODUCT_DETAILS);
    }

    @Override
    protected void initViews() {
        tvEnterCode.setText(getString(R.string.please_enter_otp_sent_by_wallet_provider, Utility.getWalletName(getServiceProviderIdFromIntent())));
        tvResendCode.setDoubleTapSafeOnClickListener(view -> iInsurancePaymentActivityPresenter.resendCode(getInitiatePurchasePolicyRequest()));
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsReceiver.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsReceiver.unregister(this);
    }

    @Override
    public void setTvResendCodeEnabled(boolean enabled) {
        tvResendCode.setEnabled(enabled);
    }

    @Override
    public void startTimer() {
        int totalTime = 45 * 1000;
        int oneSecond = 1000;
        setTvResendCodeEnabled(false);
        new TezCountDownTimer(totalTime, oneSecond) {

            @Override
            public void onTick(long millisUntilFinished) {
                String second = "00:" + (int) millisUntilFinished / 1000;
                tvResendCode.setText(second);
            }

            @Override
            public void onFinish() {
                tvResendCode.setText(R.string.resend_code);
                setTvResendCodeEnabled(true);
            }
        }.start();
    }

    private void onOtpReceived(String otp) {
        this.pinViewOTP.setText(otp);
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    protected String getScreenName() {
        return "InsurancePaymentActivity";
    }

}
