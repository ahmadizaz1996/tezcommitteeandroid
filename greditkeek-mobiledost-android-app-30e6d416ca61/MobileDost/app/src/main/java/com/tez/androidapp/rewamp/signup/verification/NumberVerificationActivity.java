package com.tez.androidapp.rewamp.signup.verification;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.tez.androidapp.BuildConfig;
import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PermissionCheckUtil;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutRegex;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.signup.router.ContactUsActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NewIntroActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NumberVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupActivityRouter;
import com.tez.androidapp.rewamp.util.DialogUtil;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.validator.library.annotations.Order;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public abstract class NumberVerificationActivity extends PlaybackActivity
        implements INumberVerificationRetryView, MultiplePermissionsListener, ValidationListener {

    public static final int MAX_TRIES_FOR_FLASH_CALL = 2;
    public static final int MAX_TRIES_FOR_OTP_SMS = 3;
    public static final int[] SINCH_EXECUTION_ON_TRIES = new int[]{2, 4};

    private final INumberVerificationRetryPresenter iNumberVerificationRetryPresenter;

    @BindView(R.id.btTryAgain)
    private TezButton btTryAgain;

    @BindView(R.id.tvNumTriesLeft)
    private TezTextView tvNumTriesLeft;

    @BindView(R.id.etMobileNumber)
    private TezEditTextView etMobileNumber;

    @BindView(R.id.tvTimer)
    private TezTextView tvTimer;

    @BindView(R.id.tvVerifyingYourNumber)
    private TezTextView tvVerifyYourNumber;

    @BindView(R.id.tllView1)
    private TezLinearLayout tllView1;

    @BindView(R.id.tllView2)
    private TezLinearLayout tllView2;

    @BindView(R.id.tllCustomerSupport)
    private TezLinearLayout tllCustomerSupport;

    @BindView(R.id.tvPrecautionNote)
    private TezTextView tvPrecautionNote;

    @BindView(R.id.tvResetDesc)
    private TezTextView tvResetDesc;

    @BindView(R.id.tvNumberVerifiedFailedDesc)
    private TezTextView tvNumberVerifiedFailedDesc;

    @BindView(R.id.tvNumberVerifyFailed)
    private TezTextView tvNumberVerifiedFailed;

    @BindView(R.id.ivTezLogo2)
    private TezImageView ivTezLogo2;

    @BindView(R.id.ivTezLogo)
    private TezImageView ivTezLogo;

    @BindView(R.id.btMainButton)
    private TezButton btMainButton;

    @BindView(R.id.tvSecondaryButton)
    private TezTextView tvSecondaryButton;

    @BindView(R.id.tvEditNumber)
    protected TezTextView tvEditNumber;

    @BindView(R.id.tvNumber)
    private  TezTextView tvNumber;

    @BindView(R.id.layoutNumber)
    private TezLinearLayout layoutNumber;

    @TextInputLayoutRegex(regex = Constants.PHONE_NUMBER_VALIDATOR_REGEX, value = {1, R.string.string_please_enter_valid_phone_number})
    @Order(1)
    @BindView(R.id.tilMobileNumber)
    private TezTextInputLayout tilMobileNumber;

    private CompositeDisposable allDisposables;

    public NumberVerificationActivity() {
        this.iNumberVerificationRetryPresenter = new NumberVerificationRetryPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number_verification);
        ViewBinder.bind(this);
        updateToolbar(findViewById(R.id.tezToolbar));
        init();
    }

    @OnClick(R.id.tvEditNumber)
    protected void onClickEditNumber() {
        finishActivityAndRouteToNewIntroActivity();
    }

    private void finishActivityAndRouteToNewIntroActivity(){
        finishAffinity();
        NewIntroActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onBackPressed() {
        Optional.doWhen(this.tllView2.getVisibility() == View.VISIBLE || tllCustomerSupport.getVisibility() == View.VISIBLE
                , super::onBackPressed);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.allDisposables = new CompositeDisposable();
        listenChangesOnViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        allDisposables.dispose();
    }

    private void listenChangesOnViews() {
        allDisposables.add(RxTextView.textChanges(this.etMobileNumber).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(NumberVerificationActivity.this, NumberVerificationActivity.this)));

    }

    private void init() {
        setTextToEtMobileNumber(getMobileNumberFromIntent());
        verifyState();
    }

    private void initializeView1() {
        setVisibilityForTvVerifyYourNumber(View.VISIBLE);
        setVisibilityForIvTezLogo(View.VISIBLE);
        setVisibilityForTvPrecautionNote(View.VISIBLE);
        setTextToTvVerifyYourNumber(R.string.verifying_your_number);
        setVisibilityForIvTezLogo2(View.GONE);
        setVisibilityForTvNumberVerifiedFailed(View.GONE);
        setVisibilityForTvNumberVerifiedFailedDesc(View.GONE);
        setVisibilityForTvResetDesc(View.GONE);
        setVisibilityForTllCustomerSupport(View.GONE);
        startNumberVerification();
    }

    protected void startNumberVerification() {
        setVisibiltyForTvTimer(View.VISIBLE);
        this.iNumberVerificationRetryPresenter.createVerification(
                getTextFromEtMobileNumber(), BuildConfig.FLAVOR+ "-" +getFlashCallLabel(), getPrincipalNameFromIntent());
    }

    @Override
    public void onVerified() {
        setVisibiltyForTvTimer(View.GONE);
    }

    @Override
    public void setTextToTvVerifyYourNumber(String text) {
        tvVerifyYourNumber.setText(text);
    }

    public void setTextToTvVerifyYourNumber(@StringRes int text) {
        tvVerifyYourNumber.setText(text);
    }

    protected void initializingView2() {
        int verifyFailCount = getVerifyFailureCount();
        initButtonTryAgain(verifyFailCount);
        initTextViewTvNumTriesLeft(verifyFailCount);
    }

    private void setTextToEtMobileNumber(String text) {
        etMobileNumber.setText(text);
        this.tvNumber.setText(text);
    }

    protected void setEnableEtMobileNumber(boolean enable){
        if(enable){
            this.layoutNumber.setVisibility(View.GONE);
            this.etMobileNumber.setVisibility(View.VISIBLE);
            this.tilMobileNumber.setVisibility(View.VISIBLE);
        } else
            etMobileNumber.setEnabled(enable);
    }


    protected String getMobileNumberFromIntent() {
        return getIntent().getStringExtra(NumberVerificationActivityRouter.MOBILE_NUMBER);
    }

    protected String getTextFromEtMobileNumber() {
        return this.etMobileNumber.getValueText();
    }

    @Override
    public boolean canStartNumberVerification() {
        return true;
    }

    @Override
    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    @Override
    public void preliminaryNumberVerificationWork() {
        setVisibiltyForTvTimer(View.GONE);
    }

    private String getPrincipalNameFromIntent() {
        return getIntent().getStringExtra(NumberVerificationActivityRouter.PRINCIPLE);
    }

    protected abstract String getFlashCallLabel();

    private void initButtonTryAgain(int verifyFailCount) {
        Optional.doWhen(verifyFailCount >= NumberVerificationActivity.MAX_TRIES_FOR_FLASH_CALL
                        + NumberVerificationActivity.MAX_TRIES_FOR_OTP_SMS,
                () -> {
                    setTextToBtTryAgain(R.string.back);
                    setOnClickListnerToBtTryAgain(view -> finish());
                }, () -> {
                    setTextToBtTryAgain(R.string.try_again);
                    setOnClickListnerToBtTryAgain(view -> onClickTryAgain());
                });
    }

    private void onClickTryAgain() {
        FieldValidator.validate(this, new ValidationListener() {
            @Override
            public void validateSuccess() {
                if(isNetworkConnected()) {
                    Optional.doWhen(etMobileNumber.isEnabled(), ()->etMobileNumber.clearFocus());
                    verifyState();
                } else {
                    showInformativeMessage(R.string.make_sure_network_signal);
                }
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                filterChain.doFilter();
            }
        });
    }

    private void setOnClickListnerToBtTryAgain(DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        this.btTryAgain.setDoubleTapSafeOnClickListener(doubleTapSafeOnClickListener);
    }

    private void initTextViewTvNumTriesLeft(int verifyFailCount) {
        int remainingAttempts = getSinchRemiaingAttempts(verifyFailCount);
        String retriesRemainingText = String.format(Utility.getStringFromResource(R.string.num_tries_left), remainingAttempts);
        setTextToTvNumTriesLeft(retriesRemainingText);
    }

    private int getVerifyFailureCount() {
        return MDPreferenceManager.getFlashCallVerificationFailureCount()
                + MDPreferenceManager.getOtpVerificationFailureCount();
    }

    private int getSinchRemiaingAttempts(int sinchFailCount) {
        return NumberVerificationActivity.MAX_TRIES_FOR_FLASH_CALL +
                NumberVerificationActivity.MAX_TRIES_FOR_OTP_SMS - sinchFailCount;
    }

    private void setTextToBtTryAgain(@StringRes int stringId) {
        this.btTryAgain.setText(stringId);
    }

    private void setTextToTvNumTriesLeft(String text) {
        this.tvNumTriesLeft.setText(text);
    }

    @Override
    public void setTextToTvTimer(String text) {
        this.tvTimer.setText(text);
    }


    @Override
    public void onCountDownFinish() {
        retryState();
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void retryState() {
        this.setVisibilityForTllView1(View.GONE);
        this.setVisibilityForTllView2(View.VISIBLE);
        initializingView2();
    }

    private void verifyState() {
        this.setVisibilityForTllView1(View.VISIBLE);
        this.setVisibilityForTllView2(View.GONE);
        initializeView1();
    }

    public void setVisibilityForTllView1(int visibility) {
        this.tllView1.setVisibility(visibility);
    }

    public void setVisibilityForTllView2(int visibility) {
        this.tllView2.setVisibility(visibility);
    }


    @Override
    public void onSinchFlashCallPermissionFailure() {
        List<String> missingPermissions = PermissionCheckUtil.getInstance(getApplicationContext()).getMissingPermissionsForSignUp(MobileDostApplication.getSignupPermissions());
        Dexter.withActivity(this).withPermissions(missingPermissions).withListener(this).check();
    }

    @Override
    public void onPermissionsChecked(MultiplePermissionsReport report) {
        Optional.doWhen(report.areAllPermissionsGranted(),
                this::verifyState,
                () -> DialogUtil.showInformativeMessage(this, report.isAnyPermissionPermanentlyDenied() ?
                                Utility.getStringFromResource(R.string.provide_permissions_from_settings_and_try_again) :
                                Utility.getStringFromResource(R.string.provide_permissions_for_number_verification),
                        (dialog, which) -> finish()));
    }

    @Override
    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
        token.continuePermissionRequest();
    }

    @Override
    public void setVisibiltyForTvTimer(int visibilty) {
        this.tvTimer.setVisibility(visibilty);
    }

    public void setVisibilityForTvVerifyYourNumber(int visibility) {
        this.tvVerifyYourNumber.setVisibility(visibility);
    }

    public void setVisibilityForIvTezLogo(int visibillity) {
        this.ivTezLogo.setVisibility(visibillity);
    }

    private void setVisibilityForIvTezLogo2(int visibility) {
        this.ivTezLogo2.setVisibility(visibility);
    }

    private void setVisibilityForTvNumberVerifiedFailed(int visibility) {
        this.tvNumberVerifiedFailed.setVisibility(visibility);
    }

    private void setVisibilityForTvNumberVerifiedFailedDesc(int visibility) {
        this.tvNumberVerifiedFailedDesc.setVisibility(visibility);
    }

    private void setVisibilityForTvResetDesc(int visibility) {
        this.tvResetDesc.setVisibility(visibility);
    }

    private void setVisibilityForTvPrecautionNote(int visibility) {
        this.tvPrecautionNote.setVisibility(visibility);
    }

    private void setVisibilityForTllCustomerSupport(int visibility) {
        this.tllCustomerSupport.setVisibility(visibility);
    }


    @Override
    public void onVerificationTriesFinish() {

        setVisibiltyForTvTimer(View.GONE);
        setVisibilityForTvVerifyYourNumber(View.GONE);
        setVisibilityForIvTezLogo(View.GONE);
        setVisibilityForTvPrecautionNote(View.GONE);

        setVisibilityForIvTezLogo2(View.VISIBLE);
        setVisibilityForTvNumberVerifiedFailed(View.VISIBLE);
        setVisibilityForTvNumberVerifiedFailedDesc(View.VISIBLE);
        setVisibilityForTvResetDesc(View.VISIBLE);
        setVisibilityForTllCustomerSupport(View.VISIBLE);

        this.btMainButton.setDoubleTapSafeOnClickListener(view -> callCustomerSupport());
        this.tvSecondaryButton.setDoubleTapSafeOnClickListener(view -> startSignUp());

    }

    private void callCustomerSupport() {
        ContactUsActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    private void startSignUp() {
        SignupActivityRouter.createInstance().setDependenciesAndRoute(this,
                getTextFromEtMobileNumber(), getReferralCode());
        finish();
    }

    protected String getReferralCode() {
        return "";
    }

    @Override
    public void validateSuccess() {
        this.btTryAgain.setButtonNormal();
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.btTryAgain.setButtonInactive();
    }
}
