package com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by FARHAN DHANANI on 2/25/2019.
 */
public class NumberVerifyGenerateOtpRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/otp/generate";

    private String principalName;

    private String mobileNumber;

    public NumberVerifyGenerateOtpRequest(String principalName, String mobileNumber) {
        this.principalName = principalName;
        this.mobileNumber = mobileNumber;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }
}
