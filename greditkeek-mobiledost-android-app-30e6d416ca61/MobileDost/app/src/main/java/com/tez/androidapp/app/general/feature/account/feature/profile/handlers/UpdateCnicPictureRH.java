package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateCnicPictureCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.UpdateCnicPictureResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;

public class UpdateCnicPictureRH extends DashboardCardsRH<UpdateCnicPictureResponse> {
    private final UpdateCnicPictureCallback updateUserProfileCallback;

    public UpdateCnicPictureRH(BaseCloudDataStore baseCloudDataStore,
                               UpdateCnicPictureCallback updateUserProfileCallback) {
        super(baseCloudDataStore);
        this.updateUserProfileCallback = updateUserProfileCallback;
    }

    @Override
    protected void onSuccess(Result<UpdateCnicPictureResponse> value) {
        super.onSuccess(value);
        UpdateCnicPictureResponse updateCnicPictureResponse = value.response().body();

        if (updateCnicPictureResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            updateUserProfileCallback.onUpdateCnicPictureSuccess();
        else
            onFailure(updateCnicPictureResponse.getStatusCode(),
                    updateCnicPictureResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        updateUserProfileCallback.onUpdateCnicPictureFailure(errorCode, message);
    }
}
