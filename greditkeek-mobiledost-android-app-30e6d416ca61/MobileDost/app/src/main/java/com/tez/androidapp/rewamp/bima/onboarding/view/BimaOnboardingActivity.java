package com.tez.androidapp.rewamp.bima.onboarding.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.CustomerSupport;

import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

public class BimaOnboardingActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bima_onboarding);
        ViewBinder.bind(this);
    }

    @OnClick(R.id.btCall)
    private void openDialer() {
        CustomerSupport.openDialer(this);
    }

    @OnClick(R.id.btWhatsapp)
    private void openWhatsapp() {
        CustomerSupport.openBimaWhatsapp(this);
    }

    @OnClick(R.id.btBack)
    private void onBackButtonPress(){
        onBackPressed();
    }

    @Override
    protected String getScreenName() {
        return "BimaOnboardingActivity";
    }
}
