package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto;

import com.tez.androidapp.commons.utils.app.Constants;

import java.io.Serializable;

/**
 * Created by farhan on 5/25/18.
 */

public class Claim implements Serializable {
    private Integer id;
    private Integer mobileUserInsurancePolicyId;
    private Double claimAmount;
    private String claimDate;
    private String insurancePolicyName;
    private String claimStatus;
    private String insurancePolicyCategory;
    private Boolean readonly;
    private Integer insurancePolicyId;
    private String claimReferenceNumber;
    private Double remainingAmount;
    private Boolean amountSelectionRequired;

    public Boolean getAmountSelectionRequired() {
        return amountSelectionRequired;
    }

    public void setAmountSelectionRequired(Boolean amountSelectionRequired) {
        this.amountSelectionRequired = amountSelectionRequired;
    }

    public String getClaimReferenceNumber() {
        return claimReferenceNumber;
    }

    public Boolean isReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public Integer getInsurancePolicyId() {
        return insurancePolicyId;
    }

    public void setInsurancePolicyId(Integer insurancePolicyId) {
        this.insurancePolicyId = insurancePolicyId;
    }

    public String getInsurancePolicyName() {
        return insurancePolicyName;
    }

    public void setInsurancePolicyName(String insurancePolicyName) {
        this.insurancePolicyName = insurancePolicyName;
    }

    public String getClaimStatus() {
        return claimStatus;
    }

    public void setClaimStatus(String claimStatus) {
        this.claimStatus = claimStatus;
    }

    public Double getClaimAmount() {
        return claimAmount;
    }

    public void setClaimAmount(Double claimAmount) {
        this.claimAmount = claimAmount;
    }

    public String getClaimDate() {
        return claimDate;
    }

    public void setClaimDate(String claimDate) {
        this.claimDate = claimDate;
    }

    public String getInsurancePolicyCategory() {
        return insurancePolicyCategory;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMobileUserInsurancePolicyId() {
        return mobileUserInsurancePolicyId;
    }

    public void setMobileUserInsurancePolicyId(Integer mobileUserInsurancePolicyId) {
        this.mobileUserInsurancePolicyId = mobileUserInsurancePolicyId;
    }

    public Double getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(Double remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public boolean isRejected() {
        return claimStatus != null && claimStatus.equalsIgnoreCase(Constants.STRING_DOCUMENT_STATUS_REJECTED);
    }

    public boolean isApproved() {
        return claimStatus != null && claimStatus.equalsIgnoreCase(Constants.STRING_DOCUMENT_STATUS_APPROVED);
    }

    public boolean isPending() {
        return claimStatus != null && claimStatus.equalsIgnoreCase(Constants.STRING_CLAIM_STATUS_PENDING);
    }

    public boolean isReviseClaim() {
        return claimStatus != null && claimStatus.equalsIgnoreCase(Constants.STRING_CLAIM_STATUS_REVISE_CLAIM);
    }
}
