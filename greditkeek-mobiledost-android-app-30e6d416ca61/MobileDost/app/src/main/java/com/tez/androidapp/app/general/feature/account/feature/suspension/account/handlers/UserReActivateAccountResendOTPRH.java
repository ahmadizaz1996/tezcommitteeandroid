package com.tez.androidapp.app.general.feature.account.feature.suspension.account.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.UserReActivateAccountResendOTPCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 9/17/2017.
 */

public class UserReActivateAccountResendOTPRH extends BaseRH<BaseResponse> {

    UserReActivateAccountResendOTPCallback userReActivateAccountResendOTPCallback;

    public UserReActivateAccountResendOTPRH(BaseCloudDataStore baseCloudDataStore, UserReActivateAccountResendOTPCallback userReActivateAccountResendOTPCallback) {
        super(baseCloudDataStore);
        this.userReActivateAccountResendOTPCallback = userReActivateAccountResendOTPCallback;
    }


    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (isErrorFree(baseResponse)) userReActivateAccountResendOTPCallback.onUserAccountReActivateResendOTPSuccess(baseResponse);
        else onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        userReActivateAccountResendOTPCallback.onUserAccountReActivateResendOTPFailure(new BaseResponse(errorCode, message));
    }
}
