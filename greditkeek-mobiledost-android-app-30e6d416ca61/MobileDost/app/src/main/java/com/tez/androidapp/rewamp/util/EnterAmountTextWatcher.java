package com.tez.androidapp.rewamp.util;

import android.content.Context;
import android.text.Editable;
import android.text.Selection;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.textwatchers.TezTextWatcher;
import com.tez.androidapp.commons.widgets.TezEditTextView;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Rehman Murad Ali
 **/
public class EnterAmountTextWatcher implements TezTextWatcher {


    private TezEditTextView editTextView;
    private Context context;
    private AmountChangeListener listener;
    private Pattern pattern;

    public EnterAmountTextWatcher(Context context, TezEditTextView editTextView) {
        this.context = context;
        this.editTextView = editTextView;
        this.editTextView.setCursorToLastPositionAlways();
        this.editTextView.disableCopyPaste();
        pattern = Pattern.compile(Constants.AMOUNT_PATTERN);
        String value = this.editTextView.getValueText();
        if (value != null)
            this.editTextView.setSelection(value.length());
    }

    public void setListener(AmountChangeListener listener) {
        this.listener = listener;
    }

    @Override
    public void afterTextChanged(Editable s) {

        String rs = context.getString(R.string.sting_rs);
        if (!s.toString().startsWith(rs)) {
            editTextView.setText(rs);
            this.editTextView.setSelection(rs.length());
        }
        notifyListener();

    }

    private void notifyListener() {
        if (listener != null)
            listener.onAmountChange(getAmount());
    }

    private double getAmount() {
        Matcher m = pattern.matcher(editTextView.getValueText());
        return Double.parseDouble(m.find() ? m.group(0) : "0");
    }


    @FunctionalInterface
    public interface AmountChangeListener {
        void onAmountChange(double amount);
    }


}
