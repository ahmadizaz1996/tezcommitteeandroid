package com.tez.androidapp.commons.managers;

/**
 * Created by Rehman Murad Ali on 4/4/2018.
 * Load all native keys
 */
public class NativeKeysManager {
    private static NativeKeysManager nativeKeysManager;

    // Native library
    static {
        System.loadLibrary("keys");
    }

    //Private Keys
    private String tezApiKey1;
    private String tezApiKey2;
    private String tezApiKey3;
    private String tezApiKey4;
    private String easypaisaAppKey;
    private String easypaisaApiKey;
    private String encryptionKey;
    private String tezSSLpin1;
    private String tezSSLpin2;
    private String tezSSLpinA;
    private String tezSSLpinB;
    private String tezSSLpattern1;
    private String tezSSLpattern2;

    //Backend Environment URLs
    private String baseUrlQA;
    private String baseUrlUAT;
    private String baseUrlPROD;
    private String baseUrlDev;
    private String baseUrlMock;
    private String changeIPCode;

    //Private File Paths
    private String imeiFilePath;
    private String profilePictureFilePath;

    //Private Directory Paths
    private String dataDirectoryPath;
    private String picturesDirectoryPath;
    private String signUpDirectoryPath;
    private String outputDirectoryPath;
    private String documentsDirectoryPath;

    /*
    a = getEasypaisaAPIKey()
    b = getEasypaisaAppId()
    c = getTezApiKey1()
    d = getTezApiKey2()
    e = getTezApiKey3()
    f = getTezApiKey4()
    g = getProfilePictureFilePath()
    h = getEncryptionKey()
    i = getBaseUrlQA()
    j = getBaseUrlUAT()
    k = getBaseUrlPROD()
    l = getChangeIPCode
    m = getBaseUrlDev()
    n = getImeiFilePath()
    o = getDataDirectoryPath()
    p = getPicturesDirectoryPath()
    q = getSignUpDirectoryPath()
    r = getOutputDirectoryPath()
    s = getDocumentsDirectoryPath()
    u = sha256 new first half
    v = sha256 new other half
    w = sha256 old first half
    x = sha256 old other half
    y = sha256 new pattern
    z = sha256 old pattern
     */

    private NativeKeysManager() {
        easypaisaApiKey = a();
        easypaisaAppKey = b();
        tezApiKey1 = c();
        tezApiKey2 = d();
        tezApiKey3 = e();
        tezApiKey4 = f();
        profilePictureFilePath = g();
        encryptionKey = h();
        baseUrlQA = i();
        baseUrlUAT = j();
        baseUrlPROD = k();
        baseUrlDev = m();
        changeIPCode = l();
        imeiFilePath = n();
        dataDirectoryPath = o();
        picturesDirectoryPath = p();
        signUpDirectoryPath = q();
        outputDirectoryPath = r();
        documentsDirectoryPath = s();
        baseUrlMock = t();
        tezSSLpin1 = u();
        tezSSLpin2 = v();
        tezSSLpinA = w();
        tezSSLpinB = x();
        tezSSLpattern1 = y();
        tezSSLpattern2 = z();
    }

    public static NativeKeysManager getInstance() {
        if (nativeKeysManager == null)
            nativeKeysManager = new NativeKeysManager();
        return nativeKeysManager;
    }

    public String getTezApiKey1() {
        return tezApiKey1;
    }

    public String getTezSSLpin1(){
        return tezSSLpin1;
    }

    public String getTezSSLpin2(){
        return tezSSLpin2;
    }

    public String getTezSSLpinA(){
        return tezSSLpinA;
    }

    public String getTezSSLpinB(){
        return tezSSLpinB;
    }

    public String getTezSSLpattern1(){
        return tezSSLpattern1;
    }

    public String getTezSSLpattern2(){
        return tezSSLpattern2;
    }

    public String getTezApiKey2() {
        return tezApiKey2;
    }

    public String getTezApiKey3() {
        return tezApiKey3;
    }

    public String getTezApiKey4() {
        return tezApiKey4;
    }

    public String getEasypaisaAppKey() {
        return easypaisaAppKey;
    }

    public String getEasypaisaApiKey() {
        return easypaisaApiKey;
    }

    public String getEncryptionKey() {
        return encryptionKey;
    }

    public String getBaseUrlQA() {
        return baseUrlQA;
    }

    public String getBaseUrlUAT() {
        return baseUrlUAT;
    }

    public String getBaseUrlPROD() {
        return baseUrlPROD;
    }

    public String getChangeIPCode() {
        return changeIPCode;
    }

    public String getBaseUrlDev() {
        return baseUrlDev;
    }

    public String getBaseUrlMock(){
        return baseUrlMock;
    }

    public String getImeiFilePath() {
        return imeiFilePath;
    }

    public String getDataDirectoryPath() {
        return dataDirectoryPath;
    }

    public String getPicturesDirectoryPath() {
        return picturesDirectoryPath;
    }

    public String getSignUpDirectoryPath() {
        return signUpDirectoryPath;
    }

    public String getOutputDirectoryPath() {
        return outputDirectoryPath;
    }

    public String getDocumentsDirectoryPath() {
        return documentsDirectoryPath;
    }

    public String getProfilePictureFilePath() {
        return profilePictureFilePath;
    }

    private native String a();

    private native String b();

    private native String c();

    private native String d();

    private native String e();

    private native String f();

    private native String g();

    private native String h();

    private native String i();

    private native String j();

    private native String k();

    private native String l();

    private native String m();

    private native String n();

    private native String o();

    private native String p();

    private native String q();

    private native String r();

    private native String s();

    private native String t();

    private native String u();

    private native String v();

    private native String w();

    private native String x();

    private native String y();

    private native String z();
}
