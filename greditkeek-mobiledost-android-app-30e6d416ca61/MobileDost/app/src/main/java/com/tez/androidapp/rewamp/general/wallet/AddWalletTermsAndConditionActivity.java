package com.tez.androidapp.rewamp.general.wallet;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.common.BaseTermsAndConditionActivity;
import com.tez.androidapp.rewamp.general.wallet.router.AddWalletTermsAndConditionActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.router.VerifyWalletOtpActivityRouter;
import com.tez.androidapp.rewamp.signup.TermsAndCondition;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import java.util.ArrayList;
import java.util.List;

public class AddWalletTermsAndConditionActivity extends BaseTermsAndConditionActivity {

    @Override
    protected void setupViews() {
        textViewDescription.setVisibility(View.GONE);
    }

    @Override
    public List<TermsAndCondition> getTermAndConditions() {
        String tc = getString(R.string.tc_wallet_1);
        tc = tc.replaceAll(ResponseStatusCode.PlaceHolders.WALLET_NAME.getValue(), Utility.getWalletName(getServiceProviderIdFromIntent()));
        tc = tc.replaceAll(ResponseStatusCode.PlaceHolders.BANK_NAME.getValue(), Utility.getWalletBankName(getServiceProviderIdFromIntent()));
        List<TermsAndCondition> termsAndConditionList = new ArrayList<>();
        termsAndConditionList.add(new TermsAndCondition(tc));
        return termsAndConditionList;
    }

    @Override
    protected void initTextToTezCheckBox() {
        this.tezCheckBox.setText(getString(R.string.i_accept_these_terms_and_conditions));
    }

    @Override
    public void onClickBtMainButton() {
        int serviceProviderId = getServiceProviderIdFromIntent();
        String mobileAccountNumber = getIntent().getStringExtra(AddWalletTermsAndConditionActivityRouter.MOBILE_ACCOUNT_NUMBER);
        String pin = getIntent().getStringExtra(AddWalletTermsAndConditionActivityRouter.PIN);
        boolean isDefault = getIntent().getBooleanExtra(AddWalletTermsAndConditionActivityRouter.IS_DEFAULT, false);
        if (serviceProviderId != -1 && mobileAccountNumber != null) {
            VerifyWalletOtpActivityRouter.createInstance().setDependenciesAndRoute(this, serviceProviderId, mobileAccountNumber, pin, isDefault);
            finish();
        }
    }

    public int getServiceProviderIdFromIntent() {
        return getIntent().getIntExtra(AddWalletTermsAndConditionActivityRouter.SERVICE_PROVIDER_ID, -1);
    }

    @Override
    public int getAudioId() {
        return 0;
    }

//    @Override
//    protected void init() {
//        super.init();
//        //this.tezCheckBox.setText(getSpannableStringOnTermsAndConditions());
//    }

//    private SpannableString getSpannableStringOnTermsAndConditions() {
//        String string = getString(R.string.i_accept_these_terms_and_conditions);
//        String foregroundString = getString(R.string.tc);
//        SpannableString spannableString = new SpannableString(string);
//        int startIndex = string.indexOf(foregroundString);
//        spannableString.
//                setSpan(new ForegroundColorSpan(Utility.getColorFromResource(R.color.textViewTitleColorBlue)),
//                        startIndex,
//                        startIndex + foregroundString.length(),
//                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        return spannableString;
//    }

    @Override
    protected String getScreenName() {
        return "AddWalletTermsAndConditionActivity";
    }
}
