package com.tez.androidapp.rewamp.committee.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.rewamp.committee.listener.InviteBottomSheetListener;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class InviteBottomSheetDialog extends BottomSheetDialogFragment implements DoubleTapSafeOnClickListener {


    @BindView(R.id.contactImageView)
    TezImageView contactImageView;

    @BindView(R.id.whatsapImageView)
    TezImageView whatsapImageView;


    private InviteBottomSheetListener listener;

    public static InviteBottomSheetDialog newInstance() {
        Bundle args = new Bundle();
        InviteBottomSheetDialog fragment = new InviteBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.committee_invite_bottom_sheet, container, false);
        ViewBinder.bind(this, view);
        initClickListener();
        return view;
    }

    private void initClickListener() {
        contactImageView.setDoubleTapSafeOnClickListener(this);
        whatsapImageView.setDoubleTapSafeOnClickListener(this);
    }

    public void setListener(InviteBottomSheetListener inviteBottomSheetListener) {
        this.listener = inviteBottomSheetListener;
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {
            case R.id.contactImageView:
                if (listener != null)
                    listener.onDialogOptionClick(InviteOption.CONTACT);
                break;
            case R.id.whatsapImageView:
                Toast.makeText(getContext(), "In Progress", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public enum InviteOption {
        CONTACT,
        WHATSAPP
    }
}
