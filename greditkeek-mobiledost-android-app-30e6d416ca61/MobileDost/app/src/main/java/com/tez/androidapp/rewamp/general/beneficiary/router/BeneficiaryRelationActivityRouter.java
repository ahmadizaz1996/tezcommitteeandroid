package com.tez.androidapp.rewamp.general.beneficiary.router;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.beneficiary.view.BeneficiaryRelationActivity;

import java.util.ArrayList;

public class BeneficiaryRelationActivityRouter extends BaseActivityRouter {

    public static final int REQUEST_CODE_BENEFICIARY_RELATION_ACTIVITY = 12092;
    public static final String SELECTED_BENEFICIARY_RELATIONSHIP_ID = "SELECTED_BENEFICIARY_RELATIONSHIP_ID";
    public static final String BIMA_RELATION = "BIMA_RELATION";
    public static final String ONE_TIME_ADD_ONLY_ADDED_RELATION_ARRAY_LIST = "ONE_TIME_ADD_ONLY_ADDED_RELATION_ARRAY_LIST";

    public static BeneficiaryRelationActivityRouter createInstance() {
        return new BeneficiaryRelationActivityRouter();
    }

    public void setDependenciesAndRouteForResult(@NonNull BaseActivity from,
                                                 @Nullable Integer selectedBeneficiaryRelationshipId,
                                                 @NonNull ArrayList<Integer> oneTimeRelationList) {
        Intent intent = createIntent(from);
        intent.putExtra(SELECTED_BENEFICIARY_RELATIONSHIP_ID, selectedBeneficiaryRelationshipId);
        intent.putExtra(ONE_TIME_ADD_ONLY_ADDED_RELATION_ARRAY_LIST, oneTimeRelationList);
        routeForResult(from, intent, REQUEST_CODE_BENEFICIARY_RELATION_ACTIVITY);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, BeneficiaryRelationActivity.class);
    }
}
