package com.tez.androidapp.rewamp.committee.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import androidx.fragment.app.DialogFragment;


public class CommitteeCreationSuccessDialog extends DialogFragment implements DoubleTapSafeOnClickListener {

    @BindView(R.id.tvHeading)
    TezTextView tvHeading;

    @BindView(R.id.tvdate)
    TezTextView tvdate;

    @BindView(R.id.btOkay)
    TezButton btOkay;

    public CommitteeCreationSuccessDialog() {
        // Required empty public constructor
    }

    public static CommitteeCreationSuccessDialog newInstance() {
        CommitteeCreationSuccessDialog fragment = new CommitteeCreationSuccessDialog();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.committee_completed_dialog, container, false);
        ViewBinder.bind(this, view);
        initClickListener();
        return view;
    }

    private void initClickListener() {
        btOkay.setDoubleTapSafeOnClickListener(this);
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {
            case R.id.btOkay:
                break;
        }
    }
}