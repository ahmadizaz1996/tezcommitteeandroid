package com.tez.androidapp.rewamp.signup.presenter;

import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserSignUpSetPinResponse;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.rewamp.signup.interactor.INewTermsAndConditionActivityInteractor;
import com.tez.androidapp.rewamp.signup.interactor.NewTermsAndConditionActivityInterator;
import com.tez.androidapp.rewamp.signup.view.INewTermsAndConditionActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

public class NewTermsAndConditionActivityPresenter implements INewTermsAndConditionActivityPresenter,
        INewTermsAndConditionActivityInteractorOutput {

    private final INewTermsAndConditionActivityInteractor iNewTermsAndConditionActivityInterator;
    private final INewTermsAndConditionActivityView iNewTermsAndConditionActivityView;


    public NewTermsAndConditionActivityPresenter(INewTermsAndConditionActivityView iNewTermsAndConditionActivityView) {
        this.iNewTermsAndConditionActivityView = iNewTermsAndConditionActivityView;
        this.iNewTermsAndConditionActivityInterator = new NewTermsAndConditionActivityInterator(this);
    }

    @Override
    public void callSetPin(String pin) {
        this.iNewTermsAndConditionActivityInterator.callSetPin(pin);
    }

    @Override
    public void onUserSignUpSetPinSuccess(UserSignUpSetPinResponse userSignUpSetPinResponse, String pin) {
        this.callLogin(pin);
    }

    @Override
    public void callLogin(String pin) {
        this.iNewTermsAndConditionActivityInterator.callLogin(this.iNewTermsAndConditionActivityView.getLoginrequest(pin));
    }

    @Override
    public void onUserSignUpSetPinError(int errorCode, String message) {
        Optional.doWhen(errorCode == ResponseStatusCode.SIGNUP_CACHE_TIMEOUT.getCode(),
                () -> iNewTermsAndConditionActivityView.showError(errorCode,
                        (ok, o) -> {
                            iNewTermsAndConditionActivityView.setTezLoaderToBeDissmissedOnTransition();
                            Utility.directUserToIntroScreenAtGlobalLevel();
                        })
                , () -> {
                    this.iNewTermsAndConditionActivityView.dismissTezLoader();
                    this.iNewTermsAndConditionActivityView.showError(errorCode);
                });
    }

    @Override
    public void onUserLoginSuccess(UserLoginResponse loginResponse) {
        this.iNewTermsAndConditionActivityView.setTezLoaderToBeDissmissedOnTransition();
        this.iNewTermsAndConditionActivityView.navigateToDashBoardActivity();
    }

    @Override
    public void onUserLoginFailure(int errorCode, String message) {
        if (errorCode == ResponseStatusCode.SIGNUP_CACHE_TIMEOUT.getCode()) {
            this.iNewTermsAndConditionActivityView.setTezLoaderToBeDissmissedOnTransition();
            this.iNewTermsAndConditionActivityView.showError(errorCode, (dialog, which) -> Utility.directUserToIntroScreenAtGlobalLevel());
        } else {
            this.iNewTermsAndConditionActivityView.dismissTezLoader();
            this.iNewTermsAndConditionActivityView.showError(errorCode, (dialog, which) -> iNewTermsAndConditionActivityView.startLogin());
        }
    }
}
