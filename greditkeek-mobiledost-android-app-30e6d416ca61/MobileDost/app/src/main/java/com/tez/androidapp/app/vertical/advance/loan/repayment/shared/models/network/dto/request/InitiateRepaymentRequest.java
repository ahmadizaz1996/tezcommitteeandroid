package com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 7/20/2017.
 */

public class InitiateRepaymentRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/loan/initiate/repayment";

    private Integer loanId;
    private Integer mobileAccountId;
    private String pin;
    private Double amount;

    public InitiateRepaymentRequest(Integer loanId, Integer mobileAccountId) {
        this.loanId = loanId;
        this.mobileAccountId = mobileAccountId;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getMobileAccountId() {
        return mobileAccountId;
    }

    public void setMobileAccountId(Integer mobileAccountId) {
        this.mobileAccountId = mobileAccountId;
    }

    public Integer getLoanId() {
        return loanId;
    }
}
