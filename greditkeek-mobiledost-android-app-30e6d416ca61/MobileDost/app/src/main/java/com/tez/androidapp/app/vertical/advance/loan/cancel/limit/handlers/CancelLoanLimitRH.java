package com.tez.androidapp.app.vertical.advance.loan.cancel.limit.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.callbacks.CancelLoanLimitCallback;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;

/**
 * Created by Rehman Murad Ali on 8/30/2017.
 */

public class CancelLoanLimitRH extends DashboardCardsRH<DashboardCardsResponse> {

    private CancelLoanLimitCallback cancelLoanLimitCallback;

    public CancelLoanLimitRH(BaseCloudDataStore baseCloudDataStore, CancelLoanLimitCallback cancelLoanLimitCallback) {
        super(baseCloudDataStore);
        this.cancelLoanLimitCallback = cancelLoanLimitCallback;
    }

    @Override
    protected void onSuccess(Result<DashboardCardsResponse> value) {
        super.onSuccess(value);
        BaseResponse baseResponse = value.response().body();
        if (baseResponse != null) {
            if (isErrorFree(baseResponse))
                cancelLoanLimitCallback.onCancelLoanLimitSuccess(baseResponse);
            else
                onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        cancelLoanLimitCallback.onCancelLoanLimitFailure(errorCode, message);
    }
}
