package com.tez.androidapp.rewamp.general.beneficiary.interactor;

import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetActiveInsurancePolicyCallback;
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.IManageBeneficiaryActivityInteractorOutput;

import java.util.List;

/**
 * Created by VINOD KUMAR on 8/30/2019.
 */
public class ManageBeneficiaryActivityInteractor implements IManageBeneficiaryActivityInteractor {

    private final IManageBeneficiaryActivityInteractorOutput iManageBeneficiaryActivityInteractorOutput;

    public ManageBeneficiaryActivityInteractor(IManageBeneficiaryActivityInteractorOutput iManageBeneficiaryActivityInteractorOutput) {
        this.iManageBeneficiaryActivityInteractorOutput = iManageBeneficiaryActivityInteractorOutput;
    }

    @Override
    public void getManageBeneficiaryPolicies() {
        UserAuthCloudDataStore.getInstance().getManageBeneficiaryPolicies(new GetActiveInsurancePolicyCallback() {
            @Override
            public void onGetActiveInsurancePolicySuccess(List<Policy> policies) {
                iManageBeneficiaryActivityInteractorOutput.onGetActiveInsurancePolicySuccess(policies);
            }

            @Override
            public void onGetActiveInsurancePolicyFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getManageBeneficiaryPolicies();
                else
                    iManageBeneficiaryActivityInteractorOutput.onGetActiveInsurancePolicyFailure(errorCode, message);

            }
        });
    }
}
