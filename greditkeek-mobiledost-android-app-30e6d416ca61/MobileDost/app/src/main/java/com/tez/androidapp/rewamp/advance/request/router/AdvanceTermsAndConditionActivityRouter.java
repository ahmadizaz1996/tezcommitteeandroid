package com.tez.androidapp.rewamp.advance.request.router;

import android.content.Intent;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.request.request.LoanApplyRequest;
import com.tez.androidapp.rewamp.advance.request.view.AdvanceTermsAndConditionActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class AdvanceTermsAndConditionActivityRouter extends BaseActivityRouter {

    public static final String LOAN_APPLY_REQUEST_PARCELABLE = "LOAN_APPLY_REQUEST_PARCELABLE";


    public static AdvanceTermsAndConditionActivityRouter createInstance() {
        return new AdvanceTermsAndConditionActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @NonNull LoanApplyRequest loanApplyRequest) {
        Intent intent = createIntent(from);
        intent.putExtra(LOAN_APPLY_REQUEST_PARCELABLE, (Parcelable) loanApplyRequest);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, AdvanceTermsAndConditionActivity.class);
    }
}
