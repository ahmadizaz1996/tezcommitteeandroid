package com.tez.androidapp.rewamp.bima.products.policy.view;

import android.view.View;

import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.router.EasyPaisaInsurancePaymentActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.InsurancePaymentActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.JazzInsurancePaymentActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.ProductRequiredTermConditionsActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.PurchasePolicyActivityRouter;
import com.tez.androidapp.rewamp.bima.products.view.ProductTermConditionsActivity;

public class ProductRequiredTermConditionsActivity extends ProductTermConditionsActivity {


    private ProductRequiredTermConditionsActivityRouter.Dependencies dependencies;

    @Override
    protected void init() {
        super.init();
        this.dependencies = ProductRequiredTermConditionsActivityRouter.getDependencies(getIntent());
        this.btMainButton.setVisibility(View.VISIBLE);
        this.tezCheckBox.setVisibility(View.VISIBLE);
        this.textViewDescription.setVisibility(View.GONE);
        this.tvDetailedTC.setVisibility(View.GONE);
        initTextToTezCheckBox();
        this.tezCheckBox.setOnCheckedChangeListener(this);
    }

    @Override
    protected void onClickBtMainButton() {

        InitiatePurchasePolicyRequest initiatePurchasePolicyRequest = dependencies.getProductDetails();

        if (initiatePurchasePolicyRequest != null) {

            int walletProviderId = ProductRequiredTermConditionsActivityRouter.getDependencies(getIntent()).getWalletProviderId();

            if (walletProviderId == Constants.EASYPAISA_ID)
                EasyPaisaInsurancePaymentActivityRouter.createInstance()
                        .setDependenciesAndRoute(this, walletProviderId, initiatePurchasePolicyRequest);

            else if (walletProviderId == Constants.JAZZ_CASH_ID)
                JazzInsurancePaymentActivityRouter.createInstance()
                        .setDependenciesAndRoute(this, walletProviderId, initiatePurchasePolicyRequest);
            else
                InsurancePaymentActivityRouter.createInstance()
                        .setDependenciesAndRoute(this, walletProviderId, initiatePurchasePolicyRequest);

        } else
            PurchasePolicyActivityRouter.createInstance().setDependenciesAndRoute(this, dependencies.getProductId(), dependencies.getTitle(), true);

        finishActivity();
    }

    @Override
    protected void showDetailedTermsCondition() {
        this.presenter.getProductDetailedTermCondition(dependencies.getProductId());
    }
}
