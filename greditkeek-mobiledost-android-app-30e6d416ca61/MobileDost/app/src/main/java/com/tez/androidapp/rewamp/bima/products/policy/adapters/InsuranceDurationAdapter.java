package com.tez.androidapp.rewamp.bima.products.policy.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class InsuranceDurationAdapter extends
        GenericRecyclerViewAdapter<InsuranceDuration,
                InsuranceDurationAdapter.InsuranceDurationListener,
                InsuranceDurationAdapter.InsuranceDurationViewHolder> {

    private int selectedInsuranceDurationPosition = -1;

    public InsuranceDurationAdapter(@NonNull List<InsuranceDuration> items,
                                    @Nullable InsuranceDurationListener listener) {
        super(items, listener);
    }


    @NonNull
    @Override
    public InsuranceDurationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_loan_duration, parent, false);
        return new InsuranceDurationViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull InsuranceDurationViewHolder holder, int position) {
        holder.onBind(position, getItems(), getListener());
    }

    @Override
    public void setItems(@NonNull List<InsuranceDuration> items) {
        if (selectedInsuranceDurationPosition != -1) {
            InsuranceDuration item = get(selectedInsuranceDurationPosition);
            int index = items.indexOf(item);
            this.selectedInsuranceDurationPosition = index != -1 ? index : 0;
        }
        super.setItems(items);
    }

    public interface InsuranceDurationListener extends BaseRecyclerViewListener {

        void onClickInsuranceDuration(@NonNull InsuranceDuration tenureDetail);
    }

    class InsuranceDurationViewHolder extends BaseViewHolder<InsuranceDuration, InsuranceDurationListener> {

        @BindView(R.id.cardViewContainer)
        private TezCardView cardViewContainer;

        @BindView(R.id.tvTitle)
        private TezTextView tvTitle;


        InsuranceDurationViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(int position, List<InsuranceDuration> items, @Nullable InsuranceDurationListener listener) {
            InsuranceDuration item = items.get(position);
            tvTitle.setText(item.getTitle());
            cardViewContainer.setCardBackgroundColor(Utility.getColorFromResource(R.color.textViewTextColorGreen));
            tvTitle.setTextColor(Utility.getColorFromResource(R.color.colorWhite));
            cardViewContainer.setOnClickListener(v -> {
                if (selectedInsuranceDurationPosition != position) {
                    notifyItemChanged(selectedInsuranceDurationPosition);
                    notifyItemChanged(position);
                    selectedInsuranceDurationPosition = position;
                }
            });

            if (listener != null && selectedInsuranceDurationPosition == position)
                listener.onClickInsuranceDuration(item);

            if (selectedInsuranceDurationPosition == position) {
                cardViewContainer.setCardBackgroundColor(Utility.getColorFromResource(R.color.textViewTextColorGreen));
                tvTitle.setTextColor(Utility.getColorFromResource(R.color.colorWhite));
            } else {
                cardViewContainer.setCardBackgroundColor(Utility.getColorFromResource(R.color.screen_background));
                tvTitle.setTextColor(Utility.getColorFromResource(R.color.editTextTextColorGrey));
            }
        }
    }

}
