package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.ClaimDetailDto;

/**
 * Created by FARHAN DHANANI on 6/29/2018.
 */
public class MyClaimResponse extends BaseResponse {
    private ClaimDetailDto claimDetailDto;

    public ClaimDetailDto getClaimDetailDto() {
        return claimDetailDto;
    }

    public void setClaimDetailDto(ClaimDetailDto claimDetailDto) {
        this.claimDetailDto = claimDetailDto;
    }

}
