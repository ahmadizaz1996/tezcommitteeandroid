package com.tez.androidapp.app.general.feature.playback;

import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.RawRes;

import com.tez.androidapp.R;
import net.tez.fragment.util.optional.Optional;
import net.tez.fragment.util.base.IBaseWidget;
import com.tez.androidapp.commons.widgets.TezAudioTextView;

/**
 * Created by VINOD KUMAR on 5/10/2019.
 */
public interface IAudioPlayback extends AudioPlaybackListener {

    @RawRes
    int getAudioId();

    @Nullable
    default View getViewForAudio() {
        return null;
    }

    @Nullable
    AudioControlInterface getAudioPlayback();

    @Override
    default void onPlaybackStart() {
        this.changeAudioViewDrawable(R.drawable.ic_speaker_on);
    }

    @Override
    default void onPlaybackComplete() {
        this.changeAudioViewDrawable(R.drawable.ic_speaker_off);
        Optional.ifPresent(getViewForAudio(), v -> {
            v.setEnabled(true);
        });
    }

    default void updateAudioId(@RawRes int audioId) {
        AudioControlInterface audioPlayback = this.getAudioPlayback();
        if (audioPlayback != null)
            audioPlayback.setAudioId(audioId);
    }

    default void setListenerOnAudioView() {
        Optional.isInstanceOf(getViewForAudio(),
                IBaseWidget.class,
                iTezWidget -> iTezWidget.setDoubleTapSafeOnClickListener(v -> {
                    v.setEnabled(false);
                    Optional.ifPresent(getAudioPlayback(), AudioControlInterface::startPlayer);
                }));
    }

    default void changeAudioViewDrawable(@DrawableRes int drawableResId) {
        Optional.isInstanceOf(getViewForAudio(),
                TezAudioTextView.class,
                textView -> textView.changeDrawable(drawableResId));
    }
}