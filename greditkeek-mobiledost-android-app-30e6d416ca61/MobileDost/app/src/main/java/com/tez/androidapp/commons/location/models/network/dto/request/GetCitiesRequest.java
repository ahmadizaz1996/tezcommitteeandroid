package com.tez.androidapp.commons.location.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 2/9/2017.
 */

public class GetCitiesRequest extends BaseRequest{
    public static final String METHOD_NAME = "v1/common/cities";
}
