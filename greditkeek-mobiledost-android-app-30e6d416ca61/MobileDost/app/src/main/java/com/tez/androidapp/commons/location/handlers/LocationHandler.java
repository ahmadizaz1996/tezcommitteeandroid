package com.tez.androidapp.commons.location.handlers;

import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.location.LocationRequest;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;

import net.tez.location.callbacks.CurrentLocationCallback;
import net.tez.location.core.TezLocation;

/**
 * Created by Vinod Kumar on 1/18/2019.
 */

public class LocationHandler {

    private LocationHandler() {
    }

    public static void requestCurrentLocation(@NonNull FragmentActivity fragmentActivity,
                                              @NonNull LocationAvailableCallback callback) {
        TezLocation.requestCurrentLocation(fragmentActivity,
                getTezLocationRequest(),
                getTezCurrentLocationCallback(new LocationAvailableCallback[]{callback}));
    }

    public static void requestCurrentLocation(@NonNull Context context,
                                              @NonNull LocationAvailableCallback callback) {
        TezLocation.requestCurrentLocation(context,
                getTezLocationRequest(),
                getTezCurrentLocationCallback(new LocationAvailableCallback[]{callback}));
    }

    private static CurrentLocationCallback getTezCurrentLocationCallback(@NonNull LocationAvailableCallback[] callbacks) {
        return new CurrentLocationCallback() {
            @Override
            public synchronized void onPermissionDenied() {
                if (callbacks[0] != null)
                    callbacks[0].onLocationPermissionDenied();
                callbacks[0] = null;
            }

            @Override
            public synchronized void onInitialized() {
                if (callbacks[0] != null)
                    callbacks[0].onLocationRequestInitialized();
            }

            @Override
            public synchronized void onFailed(String error) {
                if (callbacks[0] != null)
                    callbacks[0].onLocationFailed(error);
                callbacks[0] = null;
            }

            @Override
            public synchronized void onResult(Location location) {
                if (callbacks[0] != null)
                    callbacks[0].onLocationAvailable(location);
                callbacks[0] = null;
            }
        };
    }

    private static LocationRequest getTezLocationRequest() {
        return LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(5000)        // 5 seconds, in milliseconds
                .setNumUpdates(1)
                .setFastestInterval(2000);// 2 seconds, in milliseconds
    }
}
