package com.tez.androidapp.rewamp.advance.repay.presenter;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.InitiateRepaymentResponse;

public interface IVerifyJazzRepaymentActivityInteractorOutput extends IVerifyRepaymentActivityInteractorOutput {

    void onInitiateRepaymentSuccess(InitiateRepaymentResponse initiateRepaymentResponse, @NonNull String pin, int mobileAccountId, double amount, @Nullable Location location);
}
