package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;

import java.util.List;

public class UpdateUserBasicProfileRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/user/profile/basic";

    private String fullName;
    private String gender;
    private String dateOfBirth;

    private List<AnswerSelected> personalInformationList;

    public List<AnswerSelected> getPersonalInformationList() {
        return personalInformationList;
    }

    public void setPersonalInformationList(List<AnswerSelected> personalInformationList) {
        this.personalInformationList = personalInformationList;
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "BasicInformationRequest{" +
                "fullName='" + fullName + '\'' +
                ", gender='" + gender + '\'' +
                ", dateOfBirth='" + dateOfBirth +
                '}';
    }
}
