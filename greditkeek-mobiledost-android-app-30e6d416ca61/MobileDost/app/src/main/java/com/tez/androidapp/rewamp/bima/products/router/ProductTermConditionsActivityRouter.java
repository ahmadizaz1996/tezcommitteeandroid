package com.tez.androidapp.rewamp.bima.products.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.bima.products.view.ProductTermConditionsActivity;

public class ProductTermConditionsActivityRouter extends BaseActivityRouter {

    protected static final String PRODUCT_ID = "PRODUCT_ID";

    public static ProductTermConditionsActivityRouter createInstance() {
        return new ProductTermConditionsActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, int productId) {
        Intent intent = createIntent(from);
        intent.putExtra(PRODUCT_ID, productId);
        route(from, intent);
    }

    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, ProductTermConditionsActivity.class);
    }

    public static Dependencies getDependencies(@NonNull Intent intent) {
        return new Dependencies(intent);
    }

    public static class Dependencies {

        @NonNull
        protected Intent intent;

        protected Dependencies(@NonNull Intent intent) {
            this.intent = intent;
        }

        public int getProductId() {
            return intent.getIntExtra(PRODUCT_ID, -100);
        }
    }
}
