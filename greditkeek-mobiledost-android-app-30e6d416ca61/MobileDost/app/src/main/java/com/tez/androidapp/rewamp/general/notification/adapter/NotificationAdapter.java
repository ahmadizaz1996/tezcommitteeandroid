package com.tez.androidapp.rewamp.general.notification.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.notification.models.netwrok.Notification;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.general.notification.data.source.NetworkState;

import net.tez.logger.library.utils.TextUtil;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class NotificationAdapter
        extends PagedListAdapter<Notification, RecyclerView.ViewHolder> {

    private static final int TYPE_PROGRESS = 0;
    private static final int TYPE_ITEM = 1;

    private NetworkState networkState;
    private final NotificationItemListener notificationItemListener;

    public NotificationAdapter(NotificationItemListener notificationItemListener) {
        super(ITEM_DIFFRENTIATOR);
        this.notificationItemListener = notificationItemListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
            return new NotificationsViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item_shimmer, parent, false);
            return new NetworkStateItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Optional.isInstanceOf(holder, NotificationsViewHolder.class,
                notificationsViewHolder ->
                        Optional.ifPresent(getItem(position), notification -> {
                    setNotificationToViewHolder(notificationsViewHolder, notification);
                }));

        Optional.isInstanceOf(holder, NetworkStateItemViewHolder.class,
                networkStateItemViewHolder ->
                        networkStateItemViewHolder.bindView(networkState));
    }


    private void setNotificationToViewHolder(NotificationsViewHolder viewHolder, Notification notification) {
        viewHolder.setTextToTvTime(notification.getCreatedTimestamp());
        viewHolder.setTextToTvDescription(notification.getMessageText());
    }

    public void setNetworkState(NetworkState newNetworkState) {
        NetworkState previousState = this.networkState;
        boolean previousExtraRow = hasExtraRow();
        this.networkState = newNetworkState;
        boolean newExtraRow = hasExtraRow();
        if (previousExtraRow != newExtraRow) {
            if (previousExtraRow) {
                notifyItemRemoved(getItemCount());
            } else {
                notifyItemInserted(getItemCount());
            }
        } else if (newExtraRow && previousState != newNetworkState) {
            notifyItemChanged(getItemCount() - 1);
        }
        this.notificationItemListener.notificationsAreEmpty(getItemCount()==0);
    }

    private boolean hasExtraRow() {
        return this.networkState != null && this.networkState != NetworkState.LOADED;
    }

    private final class NotificationsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        private TezTextView tv_description;

        @BindView(R.id.tv_time)
        private TezTextView tv_time;

        private NotificationsViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);

        }

        private void setTextToTvTime(String text) {
            this.tv_time.setText(text);
        }

        private void setTextToTvDescription(String text) {
            this.tv_description.setText(text);
        }
    }

    private class NetworkStateItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.shimmerFrameLayout)
        private ShimmerFrameLayout shimmerFrameLayout;

        private NetworkStateItemViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        private void bindView(NetworkState networkState) {
            this.shimmerFrameLayout.setVisibility(
                    networkState != null &&
                            networkState.getStatus() == NetworkState.Status.RUNNING ?
                            View.VISIBLE :
                            View.GONE);

            Optional.doWhen(networkState != null && networkState.getStatus() == NetworkState.Status.FAILED,
                    this::setError);
        }

        private void setError() {
            notificationItemListener.onFailed(networkState.getErrorCode());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return hasExtraRow() && position == getItemCount() - 1?
                TYPE_PROGRESS:
                TYPE_ITEM;
    }

    private static final DiffUtil.ItemCallback<Notification> ITEM_DIFFRENTIATOR =
            new DiffUtil.ItemCallback<Notification>() {
                @Override
                public boolean areItemsTheSame(@NonNull Notification oldItem, @NonNull Notification newItem) {
                    return oldItem.getId().equals(newItem.getId());
                }

                @Override
                public boolean areContentsTheSame(Notification oldItem, Notification newItem) {
                    return TextUtil.equals(oldItem.getMessageText(), newItem.getMessageText(), true)
                            && TextUtil.equals(oldItem.getCreatedTimestamp(), newItem.getCreatedTimestamp(), true);
                }
            };

    public interface NotificationItemListener{
        void onFailed(int message);
        void notificationsAreEmpty(boolean isEmpty);
    }

}
