package com.tez.androidapp.app.general.feature.authentication.shared.callbacks;

import com.tez.androidapp.commons.models.network.dto.response.UserInfoResponse;

/**
 * Created  on 12/15/2016.
 */

public interface UserInfoCallback {

    void onUserInfoSuccess(UserInfoResponse userInfoResponse);

    void onUserInfoError(int errorCode, String message);
}
