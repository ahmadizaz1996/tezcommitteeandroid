package com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by FARHAN DHANANI on 9/18/2018.
 */
public class Question implements Serializable {

    private String refName;
    private String text;
    private Map<String, Answer> answers;

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Map<String, Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Map<String, Answer> answers) {
        this.answers = answers;
    }
}
