package com.tez.androidapp.rewamp.bima.products.policy.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.AddWalletCardView;
import com.tez.androidapp.commons.widgets.InsuranceCoverageCardView;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.commons.widgets.TezToolbar;
import com.tez.androidapp.rewamp.advance.request.router.LoanRequiredStepsActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.adapters.InsuranceDuration;
import com.tez.androidapp.rewamp.bima.products.policy.adapters.InsuranceDurationAdapter;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.IPurchasePolicyActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.PurchasePolicyActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.router.EasyPaisaInsurancePaymentActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.InsurancePaymentActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.InsuranceRequiredStepsActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.JazzInsurancePaymentActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.ProductRequiredTermConditionsActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.PurchasePolicyActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.router.ChangeWalletActivityRouter;

import net.tez.fragment.util.optional.Optional;
import net.tez.fragment.util.optional.TextUtil;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class PurchasePolicyActivity extends BaseActivity
        implements AddWalletCardView.ChangeWalletListener,
        IPurchasePolicyActivityView,
        InsuranceCoverageCardView.AmountChangeListener, InsuranceDurationAdapter.InsuranceDurationListener {

    private final IPurchasePolicyActivityPresenter iPurchasePolicyActivityPresenter;
    @BindView(R.id.rvAdvanceDuration)
    private RecyclerView rvAdvanceDuration;

    @BindView(R.id.cvEnterAmount)
    private InsuranceCoverageCardView cvEnterAmount;

    @BindView(R.id.tvPremiumValue)
    private TezTextView tvPremiumValue;

    @BindView(R.id.tvDiscount)
    private TezTextView tvDiscount;

    @BindView(R.id.tvDiscountValue)
    private TezTextView tvDiscountValue;

    @BindView(R.id.tvFinalPriceValue)
    private TezTextView tvFinalPriceValue;

    @BindView(R.id.tvInsuranceCompValue)
    private TezTextView tvInsuranceCompValue;

    @BindView(R.id.tvExpiryDateValue)
    private TezTextView tvExpiryDateValue;

    @BindView(R.id.activation_date)
    private TezTextView tvActivationDate;

    @BindView(R.id.tvInsuranceComp)
    private TezTextView tvInsuranceComp;

    @BindView(R.id.tvCharges)
    private TezTextView tvCharges;

    @BindView(R.id.tvActivationDateValue)
    private TezTextView tvActivationDateValue;

    @BindView(R.id.tvIssuanceDateValue)
    private TezTextView tvIssuanceDateValue;

    @BindView(R.id.cvMobileWallet)
    private AddWalletCardView cvMobileWallet;

    @BindView(R.id.btRequestAdvance)
    private TezButton btRequestAdvance;

    @BindView(R.id.tvAmountReturn)
    private TezTextView tvAmountReturn;

    @BindView(R.id.toolbar)
    private TezToolbar toolbar;

    @BindView(R.id.ivLeftArrow)
    private TezImageView ivLeftArrow;

    @BindView(R.id.ivRightArrow)
    private TezImageView ivRightArrow;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    @BindView(R.id.greyBackground)
    private TezLinearLayout greyBackground;

    @BindView(R.id.toolbarBottom)
    private TezImageView toolbarBottom;

    private InsuranceDurationAdapter insuranceDurationAdapter;

    public PurchasePolicyActivity() {
        this.iPurchasePolicyActivityPresenter = new PurchasePolicyActivityPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_policy);
        ViewBinder.bind(this);
        init();
    }

    @Override
    public void showTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void setClContentVisibility(int visibility) {
        this.clContent.setVisibility(visibility);
        this.greyBackground.setVisibility(visibility);
    }

    @Override
    public void setInsuranceDurations(@NonNull List<InsuranceDuration> insuranceDurations) {
        if (rvAdvanceDuration.getAdapter() == null) {
            this.insuranceDurationAdapter = new InsuranceDurationAdapter(insuranceDurations, this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
            this.rvAdvanceDuration.setLayoutManager(linearLayoutManager);
            this.rvAdvanceDuration.setAdapter(insuranceDurationAdapter);
            this.setArrowsBehaviour();
        }
    }


    private void setArrowsBehaviour() {
        ivLeftArrow.setOnClickListener(v -> rvAdvanceDuration.smoothScrollToPosition(0));
        ivRightArrow.setOnClickListener(v -> rvAdvanceDuration.smoothScrollToPosition(insuranceDurationAdapter.getItemCount() - 1));
        ivLeftArrow.setAlpha(0.2F);

        Optional.doWhen(!rvAdvanceDuration.canScrollHorizontally(1),
                () -> ivRightArrow.setAlpha(0.2F));

        rvAdvanceDuration.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                Optional.doWhen(rvAdvanceDuration.canScrollHorizontally(1),
                        () -> ivRightArrow.setAlpha(1F),
                        () -> ivRightArrow.setAlpha(0.2F));

                Optional.doWhen(rvAdvanceDuration.canScrollHorizontally(-1),
                        () -> ivLeftArrow.setAlpha(1F),
                        () -> ivLeftArrow.setAlpha(0.2F));
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == LoanRequiredStepsActivityRouter.REQUEST_CODE_LOAN_REQUIRED_STEPS_ACTIVITY)
            iPurchasePolicyActivityPresenter.getInsuranceCoverage(getProductId());

        else if (resultCode == RESULT_OK && data != null && requestCode == ChangeWalletActivityRouter.REQUEST_CODE_CHANGE_WALLET) {
            setUserWallet(data.getParcelableExtra(ChangeWalletActivityRouter.RESULT_DATA_WALLET));
        }
    }


    @Override
    public void setUserWallet(@Nullable Wallet wallet) {
        this.cvMobileWallet.setWallet(wallet);
        checkToEnableButton();
    }

    @Override
    public void setListeners() {
        this.btRequestAdvance.setDoubleTapSafeOnClickListener(v -> {

            Wallet wallet = cvMobileWallet.getWallet();
            if (TextUtil.equals(tvFinalPriceValue.getValueText(), getString(R.string.rs_zero))) {
                showInformativeMessage("Please select Tenures");
            } else if (wallet == null) {
                showInformativeMessage(R.string.wallet_not_found);
            } else {
                InitiatePurchasePolicyRequest initiatePurchasePolicyRequest = new InitiatePurchasePolicyRequest();
                initiatePurchasePolicyRequest.setMobileAccountId(wallet.getMobileAccountId());
                initiatePurchasePolicyRequest.setCoverageAmount(cvEnterAmount.getCoverage());
                initiatePurchasePolicyRequest.setExpiryDate(Utility.getFormattedDate(tvExpiryDateValue.getValueText(), Constants.OUTPUT_DATE_FORMAT, Constants.BACKEND_DATE_FORMAT));
                this.iPurchasePolicyActivityPresenter.completeInitiatePurchasePolicyRequest(initiatePurchasePolicyRequest, getProductId(), wallet.getServiceProviderId());
            }
        });

        this.cvMobileWallet.setChangeWalletListener(this);
        this.cvEnterAmount.setRequestAdvanceTextChangeListener(this);
    }

    private int getProductId() {
        return getIntent().getIntExtra(PurchasePolicyActivityRouter.PRODUCT_ID, -100);
    }

    private boolean isTermsAccepted() {
        return getIntent().getBooleanExtra(PurchasePolicyActivityRouter.TERMS_ACCEPTED, false);
    }

    private String getProductTitle() {
        return getIntent().getStringExtra(PurchasePolicyActivityRouter.PRODUCT_TITLE);
    }

    @Override
    public void routeToTermsAndCondition(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest, int walletServiceProviderId) {

        if (isTermsAccepted()) {

            if (walletServiceProviderId == Constants.EASYPAISA_ID)
                EasyPaisaInsurancePaymentActivityRouter.createInstance()
                        .setDependenciesAndRoute(this, walletServiceProviderId, initiatePurchasePolicyRequest);

            else if (walletServiceProviderId == Constants.JAZZ_CASH_ID)
                JazzInsurancePaymentActivityRouter.createInstance()
                        .setDependenciesAndRoute(this, walletServiceProviderId, initiatePurchasePolicyRequest);
            else
                InsurancePaymentActivityRouter.createInstance()
                        .setDependenciesAndRoute(this, walletServiceProviderId, initiatePurchasePolicyRequest);
        } else
            ProductRequiredTermConditionsActivityRouter.createInstance().setDependenciesAndRoute(this,
                    initiatePurchasePolicyRequest, getProductId(), walletServiceProviderId);
    }

    private void init() {
        int productId = getProductId();
        Optional.doWhen(productId == Constants.DIGITAL_OPD_PLAN_ID, this::onRequestDigitalOpd);
        Optional.doWhen(productId == Constants.CORONA_DEFENSE_PLAN_ID, this::onRequestCoronaDefense);
        toolbar.setToolbarTitle(getProductTitle());
        this.iPurchasePolicyActivityPresenter.getInsuranceCoverage(productId);
    }

    private void onRequestDigitalOpd() {
        cvEnterAmount.setGroupAmountVisibility(View.GONE);
        cvEnterAmount.setHeading(getString(R.string.unlimited_medical_consultataion));
        tvInsuranceComp.setText(R.string.service_provider);
        tvCharges.setText(R.string.service_fee);
        ViewGroup.LayoutParams params = toolbarBottom.getLayoutParams();
        params.height = Utility.dpToPx(this, 60);
        toolbarBottom.setLayoutParams(params);
    }

    private void onRequestCoronaDefense() {
        cvEnterAmount.setVisibility(View.GONE);
        tvCharges.setVisibility(View.GONE);
        tvDiscount.setVisibility(View.GONE);
        tvDiscountValue.setVisibility(View.GONE);
        tvPremiumValue.setVisibility(View.GONE);
        toolbarBottom.setVisibility(View.GONE);
        toolbar.setToolbarBackground(R.drawable.toolbar);
        ViewGroup.LayoutParams params = toolbar.getLayoutParams();
        params.height = Utility.dpToPx(this, 80);
        toolbar.setLayoutParams(params);
        tvAmountReturn.setText(R.string.premium);
    }

    @Override
    public void setDetails(@Nullable String data, double minCoverage, double maxCoverage) {
        cvEnterAmount.setDetails(data, minCoverage, maxCoverage);
    }

    @Override
    protected String getScreenName() {
        return "PurchasePolicyActivity";
    }

    @Override
    public void onChangeWallet() {
        ChangeWalletActivityRouter.createInstance().setDependenciesAndRouteForResult(this);
    }

    @Override
    public void dismissTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.GONE);
    }

    @Override
    public void onAddWallet() {
        InsuranceRequiredStepsActivityRouter.createInstance().setDependenciesAndRouteForResult(this);
    }

    @Override
    public void onAmountChange(double amount) {
        this.insuranceDurationAdapter.setItems(iPurchasePolicyActivityPresenter.getInsuranceTenures((int) amount));
        checkToEnableButton();
    }

    @Override
    public void onClickInsuranceDuration(@NonNull InsuranceDuration tenureDetail) {
        this.iPurchasePolicyActivityPresenter.setValue(tenureDetail);
        tvPremiumValue.setText(getString(R.string.rs_value_string,
                Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(tenureDetail.getPremium())));

        tvDiscountValue.setText(getString(R.string.rs_value_string,
                Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(tenureDetail.getDiscount())));

        tvFinalPriceValue.setText(getString(R.string.rs_value_string,
                Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(tenureDetail.getPremium() - tenureDetail.getDiscount())));
        checkToEnableButton();
    }

    @Override
    public void setIssuanceDate(String date) {
        this.tvIssuanceDateValue.setText(date);
    }

    @Override
    public void setActivationDate(String date) {
        this.tvActivationDateValue.setText(date);
    }

    @Override
    public void setActivationDateVisibility(int visibility) {
        tvActivationDate.setVisibility(visibility);
        tvActivationDateValue.setVisibility(visibility);
    }

    @Override
    public void setTvExpiryDateValue(String date) {
        this.tvExpiryDateValue.setText(date);
    }

    @Override
    public void setTvInsuranceCompValue(String date) {
        this.tvInsuranceCompValue.setText(date);
    }

    private void checkToEnableButton() {
        Wallet wallet = cvMobileWallet.getWallet();
        if (TextUtil.equals(tvFinalPriceValue.getValueText(), getString(R.string.rs_zero)) || wallet == null) {
            btRequestAdvance.setEnabled(false);
            btRequestAdvance.setButtonInactive();

        } else {
            btRequestAdvance.setEnabled(true);
            btRequestAdvance.setButtonNormal();
        }
    }
}
