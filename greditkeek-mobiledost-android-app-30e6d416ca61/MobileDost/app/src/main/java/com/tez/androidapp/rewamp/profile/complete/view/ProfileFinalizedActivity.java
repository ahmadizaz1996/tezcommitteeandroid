package com.tez.androidapp.rewamp.profile.complete.view;

import android.os.Bundle;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.rewamp.advance.request.router.LoanRequiredStepsActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.InsuranceRequiredStepsActivityRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.profile.router.CompleteProfileRouter;
import com.tez.androidapp.rewamp.profile.view.IProfileCompletedView;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class ProfileFinalizedActivity extends BaseActivity implements IProfileCompletedView {

    @BindView(R.id.btNext)
    private TezButton btNext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_finalized);
        ViewBinder.bind(this);
        setOnClickListener();
    }

    private void setOnClickListener() {
        this.btNext.setDoubleTapSafeOnClickListener(v -> roteTo(getKeyRoute()));
    }

    @Override
    public void routeToLoanRequiredSteps() {
        LoanRequiredStepsActivityRouter.createInstance().routeToExistingInstanceOfActivity(this, true);
    }

    @Override
    public void routeToInsuranceRequiredSteps() {
        InsuranceRequiredStepsActivityRouter.createInstance().routeToExistingInstanceOfActivity(this, true);
    }

    @Override
    public void routeToDashboard() {
        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onBackPressed() {

    }

    private int getKeyRoute() {
        return getIntent().getIntExtra(CompleteProfileRouter.ROUTE_KEY, CompleteProfileRouter.ROUTE_TO_DEFAULT);
    }

    @Override
    protected String getScreenName() {
        return "ProfileFinalizedActivity";
    }
}
