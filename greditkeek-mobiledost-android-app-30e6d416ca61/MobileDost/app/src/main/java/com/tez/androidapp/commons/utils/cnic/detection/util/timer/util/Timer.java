package com.tez.androidapp.commons.utils.cnic.detection.util.timer.util;

import android.os.CountDownTimer;

import com.tez.androidapp.app.general.feature.playback.TezCountDownTimer;
import com.tez.androidapp.commons.utils.cnic.detection.util.timer.callback.TimeOutCallback;

public class Timer {
    private static final int COUNT_DOWN_INTERVAL = 1000;
    private TimeOutCallback timeOutCallback;
    private CountDownTimer countDownTimer;

    public Timer(long milliseconds, TimeOutCallback timeOutCallback) {
        this.timeOutCallback = timeOutCallback;
        initCountDown(milliseconds);
    }

    private void initCountDown(long milliseconds) {
        countDownTimer = new TezCountDownTimer(milliseconds, COUNT_DOWN_INTERVAL) {

            public void onFinish() {
                timeOutCallback.onTimeOut();
            }
        };
    }

    public void start() {
        countDownTimer.start();
    }

    public void stop() {
        countDownTimer.cancel();
    }
}
