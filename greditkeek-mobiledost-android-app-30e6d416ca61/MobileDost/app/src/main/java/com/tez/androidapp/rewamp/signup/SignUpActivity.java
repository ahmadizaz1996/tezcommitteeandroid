package com.tez.androidapp.rewamp.signup;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.models.network.dto.request.UserInfoRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserSignUpRequest;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.validators.annotations.ConfirmPinViewRegex;
import com.tez.androidapp.commons.validators.annotations.PinViewRegex;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutRegex;
import com.tez.androidapp.commons.widgets.RippleEffectImageView;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezPinEditText;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.signup.presenter.SignupPermissionActivityPresenter;
import com.tez.androidapp.rewamp.signup.router.AccountAlreadyExistsActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NewIntroActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupNumberVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupPermissionActivityRouter;
import com.tez.androidapp.rewamp.signup.view.ISignupPermissionActivityView;
import com.tez.androidapp.rewamp.util.DialogUtil;

import net.tez.validator.library.annotations.Order;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SignUpActivity extends PlaybackActivity implements ValidationListener,
        LocationAvailableCallback, ISignupPermissionActivityView {

    @BindView(R.id.btCreatePin)
    private TezButton btCreateAccount;

    @BindView(R.id.tvLogin)
    private TezTextView tvLogin;

    @BindView(R.id.etMobileNumber)
    private TezEditTextView etMobileNumber;

    @TextInputLayoutRegex(regex = Constants.PHONE_NUMBER_VALIDATOR_REGEX, value = {1, R.string.string_please_enter_valid_phone_number})
    @Order(1)
    @BindView(R.id.tilMobileNumber)
    private TezTextInputLayout tilMobileNumber;

    @PinViewRegex(regex = Constants.STRING_PIN_VALIDATOR_REGEX, value = {1, R.string.please_enter_valid_pin})
    @Order(2)
    @BindView(R.id.pinEtCreatePin)
    private TezPinEditText pinEtCreatePin;

    @ConfirmPinViewRegex(id = R.id.pinEtCreatePin, value = {1, R.string.please_enter_valid_pin})
    @Order(3)
    @BindView(R.id.pinEtConfirmPin)
    private TezPinEditText pinEtConfirmPin;

    @BindView(R.id.ivEditNumber)
    private RippleEffectImageView ivEditNumber;

    private CompositeDisposable allDisposables;

    private final SignupPermissionActivityPresenter iSignupPermissionActivityPresenter;

    public SignUpActivity() {
        iSignupPermissionActivityPresenter = new SignupPermissionActivityPresenter(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        init();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    private void init() {
        initOnClickListeners();
        handleIntent();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    private void handleIntent() {
        String mobileNumber = getMobileNumberFromIntent();
        this.setTextToEtMobileNumber(mobileNumber);
        this.setTilMobileNumberEnabled(mobileNumber == null);
        this.ivEditNumber.setVisibility(mobileNumber != null ? View.VISIBLE : View.GONE);
        this.pinEtCreatePin.clearText();
        this.pinEtConfirmPin.clearText();
    }

    private void setTextToEtMobileNumber(String text) {
        this.etMobileNumber.setText(text);
        this.etMobileNumber.setSelection(text == null ? 0 : text.length());
    }

    private void setTilMobileNumberEnabled(boolean enabled) {
        this.tilMobileNumber.setEnabled(enabled);
        this.etMobileNumber.setFocusableInTouchMode(enabled);
        this.etMobileNumber.setTextColor(enabled ? Utility.getColorFromResource(R.color.editTextTextColorGrey) : Utility.getColorFromResource(R.color.disabledColor));
        if (enabled)
            this.etMobileNumber.clearTextViewDrawableColor();
        else
            this.etMobileNumber.setTextViewDrawableColor(R.color.disabledColor);
    }

    private String getMobileNumberFromIntent() {
        Intent intent = getIntent();
        return intent.hasExtra(SignupActivityRouter.MOBILE_NUMBER)
                ? intent.getStringExtra(SignupActivityRouter.MOBILE_NUMBER)
                : "";
    }

    private void listenChangesOnViews() {
        allDisposables.add(RxTextView.textChanges(this.etMobileNumber).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(SignUpActivity.this, SignUpActivity.this)));
        allDisposables.add(RxTextView.textChanges(this.pinEtCreatePin.getPinEditText()).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(SignUpActivity.this, SignUpActivity.this)));
        allDisposables.add(RxTextView.textChanges(this.pinEtConfirmPin.getPinEditText()).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(SignUpActivity.this, SignUpActivity.this)));

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.allDisposables = new CompositeDisposable();
        listenChangesOnViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        allDisposables.dispose();
    }

    private void initOnClickListeners() {
        this.ivEditNumber.setOnClickListener(v -> {
            ivEditNumber.setVisibility(View.GONE);
            setTilMobileNumberEnabled(true);
            etMobileNumber.requestFocus();
        });
        this.tvLogin.setDoubleTapSafeOnClickListener(view -> {
            NewIntroActivityRouter.createInstance().setDependenciesAndRoute(this);
            finish();
        });
        this.btCreateAccount.setDoubleTapSafeOnClickListener(view -> onClickBtCreateAccount());
    }

    @OnClick(R.id.tvWhatIsSecretPin)
    private void defineSecretPin() {
        DialogUtil.showInformativeDialog(this, R.string.what_is_pin, R.string.secret_pin_defined);
    }

    private void onClickBtCreateAccount() {
        FieldValidator.validate(this, new ValidationListener() {
            @Override
            public void validateSuccess() {
                startSignupPermissionsActivity();
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                btCreateAccount.setButtonInactive();
                filterChain.doFilter();
            }
        });
    }

    private void startSignupPermissionsActivity() {
        if (Utility.isSignupPermissionGranted(this))
            getCurrentLocation(this);

        else
            SignupPermissionActivityRouter
                    .createInstance()
                    .setDependenciesAndRoute(this, getMobileNumber(), getPin(), getSocialId(), getSocialType(), getReferalCode());
    }

    @Override
    public void onLocationRequestInitialized() {
        showTezLoader();
    }

    @Override
    public void onLocationAvailable(@NonNull Location location) {
        proceedSignupCall(location);
    }

    @Override
    public void onLocationFailed(String message) {
        showTezLoader();
        proceedSignupCall(null);
    }

    private void proceedSignupCall(@Nullable Location location) {
        this.iSignupPermissionActivityPresenter.submitSignUpRequest(getSignupRequest(location));
    }

    private UserSignUpRequest getSignupRequest(@Nullable Location location) {
        return Utility.createSignupRequest(this,
                location,
                getMobileNumber(),
                getReferalCode(),
                getSocialId(),
                getSocialType());
    }

    private String getMobileNumber() {
        return this.etMobileNumber.getValueText();
    }

    private String getPin() {
        return this.pinEtCreatePin.getPin();
    }

    private String getSocialId() {
        return getIntent().getStringExtra(SignupActivityRouter.SOCIAL_ID);
    }

    private int getSocialType() {
        return getIntent().getIntExtra(SignupActivityRouter.SOCIAL_TYPE,
                Utility.PrincipalType.MOBILE_NUMBER.getValue());
    }

    private String getReferalCode(){
        return getIntent().getStringExtra(SignupActivityRouter.REF_CODE);
    }

    @Override
    public void validateSuccess() {
        this.btCreateAccount.setButtonNormal();
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.btCreateAccount.setButtonInactive();
        filterChain.doFilter();
    }

    @Override
    public void createSinchVerification() {
        SignupNumberVerificationActivityRouter
                .createInstance()
                .setDependenciesAndRoute(SignUpActivity.this, getMobileNumber(), getPin(), getSocialId(), getSocialType(),
                        getReferalCode());
    }

    @Override
    public void navigateToMobileNumberAlreadyExistActivity() {
        AccountAlreadyExistsActivityRouter.createInstance().setDependenciesAndRoute(
                SignUpActivity.this,
                getMobileNumber(),
                getSocialId(),
                getSocialType(),
                getReferalCode());
    }

    @Override
    protected String getScreenName() {
        return "SignUpActivity";
    }
}
