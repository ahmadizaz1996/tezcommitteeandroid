package com.tez.androidapp.rewamp.bima.products.policy.interactor;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.InitiateRepaymentCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.InitiateRepaymentRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.InitiateRepaymentResponse;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.advance.repay.presenter.IVerifyJazzRepaymentActivityInteractorOutput;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InitiatePurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.IEasyPaisaInsurancePaymentActivityInteractorOutput;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.IJazzInsuranceActivityInteractorOutput;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.response.PolicyDetails;

public class JazzInsuranceActivityInteractor extends EasyPaisaInsurancePaymentActivityInteractor
        implements IJazzInsuranceActivityInteractor {

    private final IJazzInsuranceActivityInteractorOutput iJazzInsuranceActivityInteractorOutput;


    public JazzInsuranceActivityInteractor(IJazzInsuranceActivityInteractorOutput iEasyPaisaInsurancePaymentActivityInteractorOutput) {
        super(iEasyPaisaInsurancePaymentActivityInteractorOutput);
        iJazzInsuranceActivityInteractorOutput = iEasyPaisaInsurancePaymentActivityInteractorOutput;
    }


    @Override
    public void initiatePurchasePolicy(@NonNull InitiatePurchasePolicyRequest initiatePurchasePolicyRequest,
                                       @NonNull String pin, @Nullable Location location) {

        BimaCloudDataStore.getInstance().initiatePurchasePolicy(initiatePurchasePolicyRequest, new InitiatePurchasePolicyCallback() {
            @Override
            public void initiatePurchasePolicySuccess(PolicyDetails policyDetails) {
                iJazzInsuranceActivityInteractorOutput.initiatePurchasePolicySuccess(policyDetails, pin, location);
            }

            @Override
            public void initiatePurchasePolicyFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    initiatePurchasePolicy(initiatePurchasePolicyRequest);
                else
                    iJazzInsuranceActivityInteractorOutput.initiatePurchasePolicyFailure(errorCode, message);
            }
        });
    }
}
