package com.tez.androidapp.rewamp.general.wallet.presenter;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.GetAllWalletCallback;

public interface IChangeWalletActivityInteractorOutput extends GetAllWalletCallback {
}
