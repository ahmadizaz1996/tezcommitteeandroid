package com.tez.androidapp.rewamp.general.transactions;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.transactions.models.network.Transaction;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezFooterView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class MyTransactionsActivity extends BaseActivity implements IMyTransactionsActivityView, MyTransactionsAdapter.MyTransactionListener {

    private final IMyTransactionsActivityPresenter iMyTransactionsActivityPresenter;

    @BindView(R.id.rvMyActivities)
    private RecyclerView rvMyActivities;

    @BindView(R.id.layoutEmptyTransaction)
    private TezConstraintLayout layoutEmptyTransaction;

    public MyTransactionsActivity() {
        iMyTransactionsActivityPresenter = new MyTransactionsActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_transactions);
        ViewBinder.bind(this);
        iMyTransactionsActivityPresenter.getTransactions();
    }

    @Override
    public void setMyTransactionsAdapter(@NonNull List<Transaction> myTransactionList) {
        MyTransactionsAdapter myTransactionsAdapter = new MyTransactionsAdapter(myTransactionList, this);
        rvMyActivities.setLayoutManager(new LinearLayoutManager(this));
        rvMyActivities.setAdapter(myTransactionsAdapter);
    }

    @Override
    public void setLayoutEmptyTransactionVisibility(int visibility) {
        layoutEmptyTransaction.setVisibility(visibility);
    }

    @Override
    public void setRvMyActivitiesVisibility(int visibility) {
        rvMyActivities.setVisibility(visibility);
    }

    @Override
    public void showTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.GONE);
        ViewGroup nestedScrollView = findViewById(R.id.rootViewGroup);
        nestedScrollView.removeView(shimmer);
    }

    @Override
    public void onClickMyTransaction(@NonNull Transaction myTransaction) {
        MyTransactionDetailActivityRouter.createInstance().setDependenciesAndRoute(this, myTransaction);
    }

    @Override
    protected String getScreenName() {
        return "MyTransactionsActivity";
    }
}
