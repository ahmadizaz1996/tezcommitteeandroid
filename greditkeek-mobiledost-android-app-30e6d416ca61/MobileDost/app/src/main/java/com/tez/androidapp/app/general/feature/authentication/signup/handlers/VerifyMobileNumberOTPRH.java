package com.tez.androidapp.app.general.feature.authentication.signup.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.authentication.signup.callbacks.VerifyMobileNumberOTPCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 3/22/2017.
 */

public class VerifyMobileNumberOTPRH extends BaseRH<BaseResponse> {

    VerifyMobileNumberOTPCallback verifyMobileNumberOTPCallback;

    public VerifyMobileNumberOTPRH(BaseCloudDataStore baseCloudDataStore, VerifyMobileNumberOTPCallback
            verifyMobileNumberOTPCallback) {
        super(baseCloudDataStore);
        this.verifyMobileNumberOTPCallback = verifyMobileNumberOTPCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            verifyMobileNumberOTPCallback.onVerifyMobileNumberOTPSuccess();
        } else {
            onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
        }

    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        verifyMobileNumberOTPCallback.onVerifyMobileNumberOTPFailure(message);
    }
}
