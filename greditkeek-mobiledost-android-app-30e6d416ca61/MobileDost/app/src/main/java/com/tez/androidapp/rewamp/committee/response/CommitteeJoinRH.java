package com.tez.androidapp.rewamp.committee.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeDeclineListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeJoinListener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeJoinRH extends BaseRH<JoinCommitteeResponse> {

    private final CommitteeJoinListener listener;

    public CommitteeJoinRH(BaseCloudDataStore baseCloudDataStore, CommitteeJoinListener committeeJoinListener) {
        super(baseCloudDataStore);
        this.listener = committeeJoinListener;
    }

    @Override
    protected void onSuccess(Result<JoinCommitteeResponse> value) {
        JoinCommitteeResponse joinCommitteeResponse = value.response().body();
        if (joinCommitteeResponse != null && joinCommitteeResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onCommitteeJoinSuccess(joinCommitteeResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onCommitteeJoinFailure(errorCode, message);
    }
}
