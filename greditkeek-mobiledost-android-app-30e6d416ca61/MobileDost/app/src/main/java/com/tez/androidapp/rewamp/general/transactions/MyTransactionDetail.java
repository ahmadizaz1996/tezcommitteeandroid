package com.tez.androidapp.rewamp.general.transactions;

import net.tez.tezrecycler.model.GenericDropDownListModel;

/**
 * Created by VINOD KUMAR on 8/29/2019.
 */
class MyTransactionDetail extends GenericDropDownListModel {

    private String amountPaid;
    private String repaymentDate;
    private String walletName;
    private String walletNumber;
    private String transactionId;

    String getAmountPaid() {
        return amountPaid;
    }

    void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    String getRepaymentDate() {
        return repaymentDate;
    }

    void setRepaymentDate(String repaymentDate) {
        this.repaymentDate = repaymentDate;
    }

    String getWalletName() {
        return walletName;
    }

    void setWalletName(String walletName) {
        this.walletName = walletName;
    }

    String getWalletNumber() {
        return walletNumber;
    }

    void setWalletNumber(String walletNumber) {
        this.walletNumber = walletNumber;
    }

    String getTransactionId() {
        return transactionId;
    }

    void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
