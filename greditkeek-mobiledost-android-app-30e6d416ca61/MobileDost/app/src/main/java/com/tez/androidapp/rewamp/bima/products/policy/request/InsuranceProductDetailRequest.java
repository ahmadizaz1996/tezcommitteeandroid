package com.tez.androidapp.rewamp.bima.products.policy.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class InsuranceProductDetailRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/insurance/product/{"
            + InsuranceProductDetailRequest.Params.PRODUCT_ID
            + "}";

    public abstract static class Params {
        public static final String PRODUCT_ID = "id";
    }
}

