package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommittteeLoginResponse;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface CommitteeLoginLIstener {

    void onCommitteeLoginSuccess(CommittteeLoginResponse committteeLoginResponse);

    void onCommitteeLoginFailure(int errorCode, String message);

}
