package com.tez.androidapp.rewamp.profile.edit.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserProfileRequest;
import com.tez.androidapp.commons.models.network.User;

import java.io.File;

public interface IEditProfileActivityInteractor {
    void getCities();

    void setUserPicture(@NonNull File file);

    void updateUserProfile(@NonNull UpdateUserProfileRequest updateUserProfileRequest);

    void validateInfo(final @NonNull String mobileNumber,
                      final String userAddress,
                      final String userEmail,
                      final int userId);
}
