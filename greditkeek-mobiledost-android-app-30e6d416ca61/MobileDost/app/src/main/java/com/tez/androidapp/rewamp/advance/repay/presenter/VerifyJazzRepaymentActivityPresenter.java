package com.tez.androidapp.rewamp.advance.repay.presenter;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.InitiateRepaymentRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.InitiateRepaymentResponse;
import com.tez.androidapp.rewamp.advance.repay.interactor.IVerifyJazzRepaymentActivityInteractor;
import com.tez.androidapp.rewamp.advance.repay.interactor.VerifyJazzRepaymentActivityInteractor;
import com.tez.androidapp.rewamp.advance.repay.view.IVerifyEasyPaisaRepaymentActivityView;

public class VerifyJazzRepaymentActivityPresenter extends VerifyEasyPaisaRepaymentActivityPresenter implements IVerifyJazzRepaymentActivityInteractorOutput, IVerifyJazzRepaymentActivityPresenter {

    private final IVerifyJazzRepaymentActivityInteractor verifyJazzRepaymentActivityInteractor;

    public VerifyJazzRepaymentActivityPresenter(IVerifyEasyPaisaRepaymentActivityView iVerifyEasyPaisaRepaymentActivityView) {
        super(iVerifyEasyPaisaRepaymentActivityView);
        this.verifyJazzRepaymentActivityInteractor = new VerifyJazzRepaymentActivityInteractor(this);
    }

    @Override
    public void initiateRepayment(@NonNull String pin, int mobileAccountId,
                                  double amount, @Nullable Location location, int loanId) {
        iVerifyEasyPaisaRepaymentActivityView.setPinViewOtpEnabled(false);
        iVerifyEasyPaisaRepaymentActivityView.showTezLoader();
        InitiateRepaymentRequest request = new InitiateRepaymentRequest(loanId, mobileAccountId);
        request.setPin(pin);
        request.setAmount(amount);
        verifyJazzRepaymentActivityInteractor.initiateRepayment(request, pin, mobileAccountId, amount, location);
    }

    @Override
    public void onInitiateRepaymentSuccess(InitiateRepaymentResponse initiateRepaymentResponse, @NonNull String pin, int mobileAccountId, double amount, @Nullable Location location) {
        this.repaymentId = initiateRepaymentResponse.getRepaymentId();
        repayLoan(pin, mobileAccountId, amount, initiateRepaymentResponse.getRepaymentCorrelationId(), location);
    }
}
