package com.tez.androidapp.app.vertical.advance.loan.repayment.shared.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.RepayLoanCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.RepayLoanResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created  on 6/16/2017.
 */

public class RepayLoanRH extends DashboardCardsRH<RepayLoanResponse> {

    private RepayLoanCallback repayLoanCallback;

    public RepayLoanRH(BaseCloudDataStore baseCloudDataStore, RepayLoanCallback repayLoanCallback) {
        super(baseCloudDataStore);
        this.repayLoanCallback = repayLoanCallback;
    }

    @Override
    protected void onSuccess(Result<RepayLoanResponse> value) {
        super.onSuccess(value);
        RepayLoanResponse repayLoanResponse = value.response().body();
        if (repayLoanResponse != null) {
            if (isErrorFree(repayLoanResponse))
                repayLoanCallback.onRepayLoanSuccess(repayLoanResponse.getLoanDetails());
            else
                onFailure(repayLoanResponse.getStatusCode(), repayLoanResponse.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        repayLoanCallback.onRepayLoanFailure(errorCode, message);
    }
}
