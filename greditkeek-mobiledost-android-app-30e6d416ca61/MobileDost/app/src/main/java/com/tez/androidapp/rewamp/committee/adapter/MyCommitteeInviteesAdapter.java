package com.tez.androidapp.rewamp.committee.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.cnic.detection.util.DateUtil;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class MyCommitteeInviteesAdapter extends GenericRecyclerViewAdapter<MyCommitteeResponse.Committee.Invite,
        MyCommitteeInviteesAdapter.MyCommitteeInviteeListener,
        MyCommitteeInviteesAdapter.MyCommitteeMemberViewHolder> {

    private final String startDate;

    public MyCommitteeInviteesAdapter(@NonNull List<MyCommitteeResponse.Committee.Invite> items, String startDate, @Nullable MyCommitteeInviteeListener listener) {
        super(items, listener);
        this.startDate = startDate;
    }

    @NonNull
    @Override
    public MyCommitteeMemberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.item_committee_invitees, parent);
        return new MyCommitteeMemberViewHolder(view, startDate);
    }

    public interface MyCommitteeInviteeListener extends BaseRecyclerViewListener {
        void onClickInvitee(@NonNull MyCommitteeResponse.Committee.Invite invite);
    }

    static class MyCommitteeMemberViewHolder extends BaseViewHolder<MyCommitteeResponse.Committee.Invite, MyCommitteeInviteeListener> {

        private final String startDate;


        @BindView(R.id.tv_name)
        private TezTextView tvName;

        @BindView(R.id.tv_message)
        private TezTextView tvMessage;

        @BindView(R.id.tv_date)
        private TezTextView tvDate;

        @BindView(R.id.tv_status)
        private TezTextView tvStatus;

        public MyCommitteeMemberViewHolder(View itemView, String startDate) {
            super(itemView);
            ViewBinder.bind(this, itemView);
            this.startDate = startDate;
        }

        @Override
        public void onBind(MyCommitteeResponse.Committee.Invite item, @Nullable MyCommitteeInviteeListener listener) {
            super.onBind(item, listener);

            if (getAdapterPosition() == 0) {
                tvName.setText(item.getMobilNumber());
                tvMessage.setText("Lets invite members");
                tvStatus.setVisibility(View.INVISIBLE);
                tvDate.setText(startDate.split("T")[0]);
            } else {
                tvName.setText(item.getMobilNumber());
                tvMessage.setText("Member invited to join");
                tvStatus.setText(item.getStatus());
                tvDate.setText(startDate.split("T")[0]);
            }
        }
    }
}
