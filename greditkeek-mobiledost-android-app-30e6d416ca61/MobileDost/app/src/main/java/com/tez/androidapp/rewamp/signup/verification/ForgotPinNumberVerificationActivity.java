package com.tez.androidapp.rewamp.signup.verification;

import com.tez.androidapp.BuildConfig;
import com.tez.androidapp.rewamp.signup.router.Create4DigitPinActivityRouter;

public class ForgotPinNumberVerificationActivity extends SignupNumberVerificationActivity {
    @Override
    protected String getFlashCallLabel() {
        return BuildConfig.FLAVOR + "-forgot-pin";
    }

    @Override
    public void onVerified() {
        Create4DigitPinActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onUserSessionTimeOut() {

    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    protected String getScreenName() {
        return "ForgotPinNumberVerificationActivity";
    }
}
