package com.tez.androidapp.app.general.feature.find.agent.models.network;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Rehman Murad Ali on 1/18/2018.
 */

public class EasypaisaAgent {
    private int id;
    private String latlng;
    private String name;
    private String phone;
    private String address;
    private String group_name;
    private String distance;
    private Integer LATITUDE = 1;
    private Integer LONGITUDE = 2;
    private LatLng latLngObject;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGroupName() {
        return group_name;
    }

    public void setGroupName(String groupName) {
        this.group_name = groupName;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public LatLng getLatLong() {
        if (latLngObject == null && latlng != null) {
            String[] spilt = latlng.split(",");
            double lat = Double.parseDouble(spilt[0]);
            double lng = Double.parseDouble(spilt[1]);
            latLngObject = new LatLng(lat, lng);
        }
        return latLngObject;
    }

    public void setLatLngObject(LatLng latLngObject) {
        this.latLngObject = latLngObject;
    }

    public Integer getLatitude() {
        return LATITUDE;
    }

    public void setLatitude(Integer latitude) {
        this.LATITUDE = latitude;
    }

    public Integer getLongitude() {
        return LONGITUDE;
    }

    public void setLongitude(Integer longitude) {
        this.LONGITUDE = longitude;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }
}
