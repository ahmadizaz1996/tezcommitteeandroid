package com.tez.androidapp.rewamp.advance.repay.presenter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.rewamp.advance.repay.interactor.IRepayLoanActivityInteractor;
import com.tez.androidapp.rewamp.advance.repay.interactor.RepayLoanActivityInteractor;
import com.tez.androidapp.rewamp.advance.repay.view.IRepayLoanActivityView;

public class RepayLoanActivityPresenter implements IRepayLoanActivityPresenter, IRepayLoanActivityInteractorOutput {

    private final IRepayLoanActivityView iRepayLoanActivityView;
    private final IRepayLoanActivityInteractor iRepayLoanActivityInteractor;

    public RepayLoanActivityPresenter(IRepayLoanActivityView iRepayLoanActivityView) {
        this.iRepayLoanActivityView = iRepayLoanActivityView;
        this.iRepayLoanActivityInteractor = new RepayLoanActivityInteractor(this);
    }

    @Override
    public void getRepayReceipt(int loanId) {
        if (loanId != -100) {
            iRepayLoanActivityView.showTezLoader();
            iRepayLoanActivityView.setClContentVisibility(View.GONE);
            iRepayLoanActivityInteractor.getRepayReceipt(loanId);
        } else
            iRepayLoanActivityView.finishActivity();
    }

    @Override
    public void onRepayReceiptSuccess(LoanDetails loanDetails) {
        iRepayLoanActivityView.dismissTezLoader();
        iRepayLoanActivityView.setClContentVisibility(View.VISIBLE);
        iRepayLoanActivityView.initDetails(loanDetails);
        if (loanDetails.getLatePaymentCharges() <= 0)
            iRepayLoanActivityView.setTvLatePaymentChargesVisibility(View.GONE);
    }

    @Override
    public void onRepayReceiptFailure(int errorCode, String message) {
        iRepayLoanActivityView.dismissTezLoader();
        iRepayLoanActivityView.showError(errorCode, (dialog, which) -> iRepayLoanActivityView.finishActivity());
    }

    @Override
    public void onClickRepayAdvance(@NonNull LoanDetails loanDetails, @Nullable Wallet wallet, double amount) {
        if (amount < 0)
            iRepayLoanActivityView.showInformativeMessage(R.string.enter_repayment_amount);

        else if (amount == 0)
            iRepayLoanActivityView.showInformativeMessage(R.string.you_should_pay_amount_greater_than_zero);

        else if (amount > loanDetails.getAmountToReturn())
            iRepayLoanActivityView.showInformativeMessage(R.string.you_are_repaying_greater_than_remaining_amount);

        else if (wallet != null) {
            if (wallet.getServiceProviderId() == Constants.EASYPAISA_ID)
                iRepayLoanActivityView.routeToVerifyEasyPaisaRepaymentActivity(loanDetails, wallet);
            else if (wallet.getServiceProviderId() == Constants.JAZZ_CASH_ID)
                iRepayLoanActivityView.routeToVerifyJazzRepaymentActivity(loanDetails, wallet);
            else
                iRepayLoanActivityView.routeToVerifyRepaymentActivity(loanDetails, wallet);
        }
    }
}
