package com.tez.androidapp.commons.utils.cnic.detection.view;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.cnic.detection.contracts.CnicDetectorWithFileCallback;
import com.tez.androidapp.commons.utils.cnic.detection.contracts.CnicScannerPresenter;
import com.tez.androidapp.commons.utils.cnic.detection.contracts.CnicScannerView;
import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;
import com.tez.androidapp.commons.utils.cnic.detection.presenter.CnicScannerPresenterImp;
import com.tez.androidapp.commons.utils.cnic.detection.util.Camera;
import com.tez.androidapp.commons.utils.cnic.detection.view.surface.view.view.CameraPreview;
import com.tez.androidapp.commons.utils.cnic.firebase.callbacks.ImageFaceRetrieveCallback;
import com.tez.androidapp.commons.widgets.TezFrameLayout;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.io.File;
import java.util.List;

/**
 *
 */
public class CnicScannerActivity extends AppCompatActivity implements CnicDetectorWithFileCallback, ImageFaceRetrieveCallback, CnicScannerView, DoubleTapSafeOnClickListener {

    public static final String CNIC_ISSUE_DATE = "CNIC_ISSUE_DATE";
    public static final String CNIC_EXPIRY_DATE = "CNIC_EXPIRY_DATE";
    public static final String CNIC_DATE_OF_BIRTH = "CNIC_DATE_OF_BIRTH";
    public static final String CNIC_NUMBER = "CNIC_NUMBER";
    public static final String IMAGE_PATH = "IMAGE_PATH";
    public static final String DETECT_OBJECT = "DETECT_OBJECT";
    public static final String CNIC_FRONT = "CNIC_FRONT";
    public static final String CNIC_BACK = "CNIC_BACK";
    public static final String FACE = "FACE";
    public static final String FAILURE_REASON = "FAILURE_REASON";
    public static final String CNIC_DETECTION_FAILED = "CNIC_DETECTION_FAILED";
    public static final String FACE_DETECTION_FAIILED = "FACE_DETECTION_FAILED";

    @BindView(R.id.frameLayoutCameraPreview)
    TezFrameLayout frameLayoutCameraPreview;
    @BindView(R.id.imageViewCNICSquareCamera)
    TezImageView imageViewCNICSquare;
    @BindView(R.id.textViewHoldYourCNIC)
    TezTextView textViewHoldYourCNIC;
    @BindView(R.id.imageViewCapture)
    TezImageView imageViewCapture;
    @BindView(R.id.imageViewRetake)
    TezImageView imageViewRetake;
    @BindView(R.id.imageViewCaptureDone)
    TezImageView imageViewCaptureDone;
    @BindView(R.id.imageViewSelfiePreview)
    TezImageView imageViewSelfiePreview;

    private CameraPreview cameraPreview;
    private CnicScannerPresenter cnicScannerPresenter;
    private String detectObject;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpViews();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        ViewBinder.bind(this);
        setDoubleTapSafeOnClickListener();
    }

    private void setDoubleTapSafeOnClickListener() {
        imageViewCapture.setDoubleTapSafeOnClickListener(this);
        imageViewRetake.setDoubleTapSafeOnClickListener(this);
        imageViewCaptureDone.setDoubleTapSafeOnClickListener(this);
    }

    private void setUpIntentData(Intent intentData) {
        this.detectObject = intentData != null ? getIntent().getStringExtra(CnicScannerActivity.DETECT_OBJECT) : null;
    }


    @Override
    protected void onStart() {
        super.onStart();
        grantCameraAndStoragePermission();
    }


    @Override
    protected void onResume() {
        super.onResume();
        setupViewsForCameraScannerScreen();
    }

    private void grantCameraAndStoragePermission() {
        if (cnicScannerPresenter.isCameraAndStorageAllowed(this))
            startDetection();
    }


    private void setUpViews() {
        setContentView(R.layout.activity_real_time_camera);
        setUpIntentData(getIntent());
        cnicScannerPresenter = new CnicScannerPresenterImp(this);
    }


    @Override
    public void setCameraAndStartPreview(android.hardware.Camera mCamera, String detectObject) {

        cameraPreview = new CameraPreview(this, mCamera, this, detectObject);
        frameLayoutCameraPreview.addView(cameraPreview);
    }

    @Override
    public void setVisibilityOfTextViewHoldCnic(int visibility) {
        textViewHoldYourCNIC.setVisibility(visibility);
    }

    @Override
    public void onPictureTaken(String filePath) {
        setVisibilityOfImageViewRetake(View.VISIBLE);
        setVisibilityOfImageViewCaptureDone(View.VISIBLE);
        setVisibilityOfImageViewCapture(View.GONE);
        setEnableModeForImageViewCapture(true);
        imageViewCapture.setEnabled(true);
    }

    @Override
    public void onPictureTakenFailure() {
        Intent intent = new Intent();
        intent.putExtra(FAILURE_REASON, FACE_DETECTION_FAIILED);
        setResult(RESULT_CANCELED);
        finishActivity();
    }

    private void setEnableModeForImageViewCapture(boolean b) {
        imageViewCapture.setEnabled(b);
    }

    private void setVisibilityOfImageViewCaptureDone(int visible) {
        imageViewCaptureDone.setVisibility(visible);
    }


    @Override
    public void setVisibilityOfImageViewSelfiePreview(int visibility) {
        imageViewSelfiePreview.setVisibility(visibility);
    }

    @Override
    public void setVisibilityOfImageViewCapture(int visibility) {
        imageViewCapture.setVisibility(visibility);
    }

    @Override
    public void setVisibilityOfImageViewRetake(int visibility) {
        imageViewRetake.setVisibility(visibility);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        Camera.autoFocus();
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLivePreview();
    }

    private void stopLivePreview() {
        if (cameraPreview != null) {
            cameraPreview.resetSettings();
            cameraPreview.setCamera(null);
        }
        Camera.closeCamera();
    }

    @Override
    public Intent setIntentForActivity(String filePath, RetumDataModel retumDataModel) {
        Intent intent = new Intent();
        intent.putExtra(CNIC_NUMBER, retumDataModel.getCnicNumber());
        intent.putExtra(CNIC_DATE_OF_BIRTH, retumDataModel.getDateOfBirth());
        intent.putExtra(CNIC_ISSUE_DATE, retumDataModel.getCnicIssueDate());
        intent.putExtra(CNIC_EXPIRY_DATE, retumDataModel.getCnicExpiryDate());
        intent.putExtra(IMAGE_PATH, filePath);
        return intent;
    }

    @Override
    public void setResultOK(Intent intent) {
        setResult(RESULT_OK, intent);
    }


    @Override
    public void finishActivity() {
        finish();
        setScreenOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void startDetection() {
        cnicScannerPresenter.setUpDisplayAndCamera(detectObject);
    }


    @Override
    public void onCNICDetectionSuccess(@NonNull File file, @NonNull RetumDataModel retumDataModel) {
        Intent intent = setIntentForActivity(file.getAbsolutePath(), retumDataModel);
        setResult(RESULT_OK, intent);
        finishActivity();
    }

    @Override
    public void onCNICDetectionFailure() {
        Intent intent = new Intent();
        intent.putExtra(FAILURE_REASON, CNIC_DETECTION_FAILED);
        setResult(RESULT_CANCELED, intent);
        finishActivity();
    }


    @Override
    public void setScreenOrientation(int orientation) {
        setRequestedOrientation(orientation);
    }

    @Override
    public void setVisibilityOfCnicSquare(int visibility) {
        imageViewCNICSquare.setVisibility(visibility);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        cameraPreview.resetSettings();
        setResult(RESULT_CANCELED);
        finishActivity();
    }

    @Override
    public void onFaceRetrieveFailure(Exception exception) {
        setVisibilityOfImageViewCapture(View.GONE);
    }

    @Override
    public void onFaceRetrieveSuccess(List<Integer> list) {
        setVisibilityOfImageViewCapture(View.VISIBLE);
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewRetake:
                setUpViewsForRetakeAndStartDetection();
                break;
            case R.id.imageViewCapture:
                clickPicture();
                break;
            case R.id.imageViewCaptureDone:
                cnicScannerPresenter.setIntentForFaceAndFinish();
                break;
            default:
                //Left
        }
    }

    private void setUpViewsForRetakeAndStartDetection() {
        cnicScannerPresenter.resumeCamera();
        setupViewsForCameraScannerScreen();
    }

    private void setupViewsForCameraScannerScreen() {
        setVisibilityOfImageViewCapture(View.GONE);
        setVisibilityOfImageViewRetake(View.GONE);
        setVisibilityOfImageViewCaptureDone(View.GONE);
    }

    private void clickPicture() {
        setEnableModeForImageViewCapture(false);
        cnicScannerPresenter.clickPicture(this);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        cnicScannerPresenter.handlePermissionResult(requestCode, permissions, grantResults);
    }


}
