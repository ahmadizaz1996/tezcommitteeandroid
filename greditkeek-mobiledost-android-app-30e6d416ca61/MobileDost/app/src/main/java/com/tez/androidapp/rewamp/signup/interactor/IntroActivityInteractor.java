package com.tez.androidapp.rewamp.signup.interactor;

import com.tez.androidapp.app.general.feature.authentication.shared.callbacks.UserInfoCallback;
import com.tez.androidapp.commons.models.network.dto.request.UserInfoRequest;
import com.tez.androidapp.commons.models.network.dto.response.UserInfoResponse;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.signup.presenter.IIntroActivityInteractorOutput;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

public class IntroActivityInteractor implements IIntroActivityInteractor {
    private final IIntroActivityInteractorOutput iIntroActivityInteractorOutput;

    public IntroActivityInteractor(IIntroActivityInteractorOutput iIntroActivityInteractorOutput) {
        this.iIntroActivityInteractorOutput = iIntroActivityInteractorOutput;
    }

    @Override
    public void callUserInfo(String mobileNumber, String socialId, int socialType){
        UserCloudDataStore.getInstance().getUserInfo(new UserInfoRequest(
                        mobileNumber,
                        socialId,
                        socialType)
                , new UserInfoCallback() {
                    @Override
                    public void onUserInfoSuccess(UserInfoResponse userInfoResponse) {
                        iIntroActivityInteractorOutput.onUserInfoSuccess(mobileNumber,
                                socialId, userInfoResponse.getPrincipalName(),socialType, userInfoResponse.isUserRegistered());
                    }

                    @Override
                    public void onUserInfoError(int errorCode, String message) {
                        if(Utility.isUnauthorized(errorCode)){
                            callUserInfo(mobileNumber, socialId, socialType);
                        } else {
                            iIntroActivityInteractorOutput.onUserInfoFailure(errorCode, message);
                        }
                    }
                });
    }
}
