package com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks;

import com.tez.androidapp.commons.models.network.dto.response.UserSignUpSetPinResponse;

/**
 * Created  on 6/12/2017.
 */

public interface UserSignUpSetPinCallback {

    void onUserSignUpSetPinSuccess(UserSignUpSetPinResponse userSignUpSetPinResponse);

    void onUserSignUpSetPinError(int errorCode, String message);

}
