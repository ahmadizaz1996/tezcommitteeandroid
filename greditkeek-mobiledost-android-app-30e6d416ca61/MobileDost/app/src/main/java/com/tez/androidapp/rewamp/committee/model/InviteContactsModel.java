package com.tez.androidapp.rewamp.committee.model;

import com.tez.androidapp.commons.models.extract.ExtractedContact;

import java.io.Serializable;

import me.everything.providers.android.contacts.Contact;

/**
 * Created by Ahmad Izaz on 21-Nov-20
 **/
public class InviteContactsModel extends ExtractedContact implements Serializable {
    private boolean isSelected;

    public InviteContactsModel(String displayName, String phone) {
        super(displayName, phone);
    }

    public InviteContactsModel(Contact contact) {
        super(contact);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

}
