package com.tez.androidapp.rewamp.committee.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Log;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezCheckBox;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeFilterActivity;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/

public class MyCommitteeUpdatesTransactionFilterAdapter extends GenericRecyclerViewAdapter<String,
        MyCommitteeUpdatesTransactionFilterAdapter.CommitteeFilterTransactionListener,
        MyCommitteeUpdatesTransactionFilterAdapter.MyCommitteeMemberViewHolder> {

    public MyCommitteeUpdatesTransactionFilterAdapter(@NonNull ArrayList<String> items, @Nullable CommitteeFilterTransactionListener listener) {
        super(items, listener);
    }


    @NonNull
    @Override
    public MyCommitteeMemberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.simple_single_string_list_item, parent);
        return new MyCommitteeMemberViewHolder(view);
    }

    public interface CommitteeFilterTransactionListener extends BaseRecyclerViewListener {
        void onSelectedTransactionFilter(String item, boolean isChecked, CompoundButton button);
    }

    static class MyCommitteeMemberViewHolder extends BaseViewHolder<String, CommitteeFilterTransactionListener> {

        @BindView(R.id.itemTextView)
        private TezTextView tvName;

        @BindView(R.id.cbWalletCheck)
        private TezCheckBox checkBox;

        public MyCommitteeMemberViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(String item, @Nullable CommitteeFilterTransactionListener listener) {
            super.onBind(item, listener);

            Log.d("LOL:", item);
            tvName.setText(item);

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    checkBox.setOnCheckedChangeListener(null);
                    listener.onSelectedTransactionFilter(item, isChecked, buttonView);
                    checkBox.setOnCheckedChangeListener(this::onCheckedChanged);
                }
            });


        }
    }
}
