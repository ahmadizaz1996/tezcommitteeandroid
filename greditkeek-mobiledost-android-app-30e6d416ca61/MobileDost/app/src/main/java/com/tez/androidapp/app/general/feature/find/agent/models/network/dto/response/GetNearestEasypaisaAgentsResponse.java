package com.tez.androidapp.app.general.feature.find.agent.models.network.dto.response;

import com.tez.androidapp.app.general.feature.find.agent.models.network.EasypaisaAgent;

import java.util.List;

/**
 * Created by Rehman Murad Ali on 1/18/2018.
 */

public class GetNearestEasypaisaAgentsResponse {
    Integer code;
    List<EasypaisaAgent> agents;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<EasypaisaAgent> getAgents() {
        return agents;
    }

    public void setAgents(List<EasypaisaAgent> agents) {
        this.agents = agents;
    }
}
