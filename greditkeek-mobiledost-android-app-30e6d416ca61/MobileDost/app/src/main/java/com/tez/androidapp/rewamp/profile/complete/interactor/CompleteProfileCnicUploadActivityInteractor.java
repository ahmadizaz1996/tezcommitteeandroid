package com.tez.androidapp.rewamp.profile.complete.interactor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateCnicPictureCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateCnicPictureRequest;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.profile.complete.presenter.ICompleteProfileCnicUploadActivityInteractorOutput;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;

public class CompleteProfileCnicUploadActivityInteractor implements ICompleteProfileCnicUploadActivityInteractor {

    private final ICompleteProfileCnicUploadActivityInteractorOutput iCompleteProfileCnicUploadActivityInteractorOutput;

    public CompleteProfileCnicUploadActivityInteractor(ICompleteProfileCnicUploadActivityInteractorOutput iCompleteProfileCnicUploadActivityInteractorOutput) {
        this.iCompleteProfileCnicUploadActivityInteractorOutput = iCompleteProfileCnicUploadActivityInteractorOutput;
    }

    @Override
    public void updateUserCnicPicture(boolean isAutoDetected, int requestCode, @NonNull String imageType,
                                      @Nullable String imagePath, @Nullable RetumDataModel retumDataModel){
        MultipartBody.Part imagePart = null;
        MultipartBody.Part isAutoDetectedPart = null;
        if(imagePath != null){
            File image = new File(imagePath);
            imagePart = MultipartBody.Part.createFormData(imageType, image.getName(),
                    MultipartBody.create(MediaType.parse(Constants.JPEG_MIME_TYPE), image));
        }
        isAutoDetectedPart = MultipartBody.Part.createFormData(UpdateCnicPictureRequest.AUTODETECTED, new Gson().toJson(!isAutoDetected));
        UserAuthCloudDataStore.getInstance().updateUserCnicPicture(imagePart, isAutoDetectedPart, new UpdateCnicPictureCallback() {
            @Override
            public void onUpdateCnicPictureSuccess() {
                iCompleteProfileCnicUploadActivityInteractorOutput
                        .onUpdateCnicPictureSuccess(requestCode, imagePath, retumDataModel);
            }

            @Override
            public void onUpdateCnicPictureFailure(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        ()-> updateUserCnicPicture(isAutoDetected, requestCode, imageType, imagePath, retumDataModel),
                        ()-> iCompleteProfileCnicUploadActivityInteractorOutput
                                .onUpdateCnicPictureFailure(requestCode,errorCode, message));
            }
        });
    }

}
