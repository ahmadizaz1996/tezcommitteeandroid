package com.tez.androidapp.repository.network.handlers;

import androidx.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.MobileUserInfo;
import com.tez.androidapp.commons.models.network.dto.response.UserStatusResponse;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;

/**
 * Created by Rehman Murad Ali on 3/1/2018.
 */

public class UserStatusRH extends BaseRH<UserStatusResponse> {

    public UserStatusRH(BaseCloudDataStore baseCloudDataStore) {
        super(baseCloudDataStore);
    }

    @Override
    protected void onSuccess(Result<UserStatusResponse> value) {
        UserStatusResponse userStatusResponse = value.response().body();
        if (userStatusResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            MobileUserInfo mobileUserInfo = userStatusResponse.getMobileUserInfo();
            if (mobileUserInfo != null) {

            }
        }
        else
            onFailure(userStatusResponse.getStatusCode(),userStatusResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (Utility.isUnauthorized(errorCode)) UserAuthCloudDataStore.getInstance().updateUserStatus();
        Crashlytics.logException(new Exception(message));
    }
}
