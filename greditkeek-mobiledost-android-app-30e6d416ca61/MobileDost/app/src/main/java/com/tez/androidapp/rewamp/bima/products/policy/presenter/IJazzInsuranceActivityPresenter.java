package com.tez.androidapp.rewamp.bima.products.policy.presenter;

import android.location.Location;

import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;

public interface IJazzInsuranceActivityPresenter extends IEasyPaisaInsurancePaymentActivityPresenter{
    void initiatePurchasePolicy(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest,
                                Location location,
                                String pin);
}
