package com.tez.androidapp.app.general.feature.transactions.callbacks;

import com.tez.androidapp.app.general.feature.transactions.models.network.dto.response.GetUserTransactionsResponse;

/**
 * Created  on 9/2/2017.
 */

public interface GetUserTransactionsCallback {

    void onGetUserTransactionsSuccess(GetUserTransactionsResponse getUserTransactionsResponse);

    void onGetUserTransactionsFailure(int statusCode, String message);

}
