package com.tez.androidapp.commons.utils.cnic.detection.view.surface.view.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.commons.utils.cnic.detection.contracts.CNICDetectionCallback;
import com.tez.androidapp.commons.utils.cnic.detection.contracts.CnicDetectorWithFileCallback;
import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;
import com.tez.androidapp.commons.utils.cnic.detection.util.timer.util.Timer;
import com.tez.androidapp.commons.utils.cnic.detection.view.CnicScannerActivity;
import com.tez.androidapp.commons.utils.cnic.detection.view.surface.view.contracts.CameraPreviewPresenter;
import com.tez.androidapp.commons.utils.cnic.detection.view.surface.view.contracts.CameraPreviewView;
import com.tez.androidapp.commons.utils.cnic.detection.view.surface.view.presenter.CameraPreviewPresenterImpl;
import com.tez.androidapp.commons.utils.cnic.firebase.callbacks.ImageFaceRetrieveCallback;
import com.tez.androidapp.commons.utils.file.util.FileUtil;
import com.tez.androidapp.rewamp.util.DialogUtil;

import net.tez.camera.util.ImageUtil;

import java.io.File;
import java.util.List;

/**
 * A basic Camera preview class
 */
@SuppressLint("ViewConstructor")
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback, CameraPreviewView, CNICDetectionCallback, ImageFaceRetrieveCallback/*, Runnable*/ {
    public static final int MAX_DETECTION_TIME_MILLISECONDS = 10000;
    private final CameraPreviewPresenter cameraPreviewPresenter;
    private SurfaceHolder mHolder;
    private Camera mCamera;
    private Activity activity;
    private CnicDetectorWithFileCallback cnicDetectorWithFileCallback;
    private ImageFaceRetrieveCallback faceRetrieveCallback;
    private Timer timer;
    private String detectObject;
    private int mRatioWidth = 0;
    private int mRatioHeight = 0;


    public CameraPreview(Context context, Camera camera, Activity activity, String detectObject) {
        super(context);
        cameraPreviewPresenter = new CameraPreviewPresenterImpl(this, this, detectObject);
        setUpMemberVariables(camera, activity);
        this.detectObject = detectObject;
        setUpCountDownTimer(detectObject.equals(CnicScannerActivity.FACE));
    }

    public void setCamera(Camera mCamera) {
        this.mCamera = mCamera;
    }

    private void setUpMemberVariables(Camera camera, Activity activity) {
        mCamera = camera;
        this.activity = activity;
        this.cnicDetectorWithFileCallback = (CnicDetectorWithFileCallback) activity;
        this.faceRetrieveCallback = (ImageFaceRetrieveCallback) activity;
        mHolder = getHolder();
        mHolder.addCallback(this);

        //noinspection deprecation
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    private void setUpCountDownTimer(boolean isFace) {
        timer = new Timer(MAX_DETECTION_TIME_MILLISECONDS, cameraPreviewPresenter);
        if (isFace) return;
        timer.start();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        if (0 == mRatioWidth || 0 == mRatioHeight) {
            setMeasuredDimension(width, height);
        } else {
            if (width > height * mRatioWidth / mRatioHeight) {
                setMeasuredDimension(width, width * mRatioHeight / mRatioWidth);
            } else {
                setMeasuredDimension(height * mRatioWidth / mRatioHeight, height);
            }
        }
    }

    public void setAspectRatio(int width, int height) {
        if (width >= 0 && height >= 0) {
            mRatioWidth = width;
            mRatioHeight = height;
            requestLayout();
        }
    }

    private void setPreviewSize(@NonNull Camera.Parameters parameters, int width, int height) {
        Camera.Size fullScreenSize = ImageUtil.chooseOptimalSize(parameters.getSupportedPictureSizes(), width, height);
        Point point = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(point);
        Camera.Size bestSize = ImageUtil.chooseOptimalSize(parameters.getSupportedPreviewSizes(), width, height, point.x, point.y, fullScreenSize);
        parameters.setPreviewSize(bestSize.width, bestSize.height);
        if (detectObject.equals(CnicScannerActivity.FACE))
            //noinspection SuspiciousNameCombination
            setAspectRatio(bestSize.height, bestSize.width);
        else
            setAspectRatio(bestSize.width, bestSize.height);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
            mCamera.setPreviewCallback(this);

        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }


    public void surfaceDestroyed(SurfaceHolder holder) {
        //Left
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.
        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }


        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }


        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            Camera.Parameters parameters = mCamera.getParameters();
            setPreviewSize(parameters, w, h);
            mCamera.setPreviewCallback(this);
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }


    @Override
    public void onPreviewFrame(byte[] bytes, Camera camera) {
        cameraPreviewPresenter.processByteArray(camera, bytes);
    }


    @Override
    public void resetSettings() {
        try {
            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
            }
        } catch (Exception e) {
            //left
        }
        timer.stop();
        mHolder.removeCallback(this);
    }

    @Override
    public void showAlertDialog(String message) {
        DialogUtil.showInformativeMessage(activity, message);
    }


    @Override
    public void onCNICDetectionSuccess(byte[] imageBytes, int width, int height, int previewFormat, RetumDataModel retumDataModel) {
        File file = FileUtil.getResizedFileFromByteArray(imageBytes,
                width,
                height,
                previewFormat,
                activity
        );
        if (file == null)
            cnicDetectorWithFileCallback.onCNICDetectionFailure();
        else
            cnicDetectorWithFileCallback.onCNICDetectionSuccess(file, retumDataModel);
    }

    @Override
    public void onCNICDetectionFailure() {
        cnicDetectorWithFileCallback.onCNICDetectionFailure();
    }

    @Override
    public void onFaceRetrieveFailure(Exception exception) {
        faceRetrieveCallback.onFaceRetrieveFailure(exception);
    }

    @Override
    public void onFaceRetrieveSuccess(List<Integer> list) {
        faceRetrieveCallback.onFaceRetrieveSuccess(list);
    }
}
