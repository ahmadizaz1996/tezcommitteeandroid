package com.tez.androidapp.rewamp.advance.repay.presenter;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.InitiateRepaymentRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.RepayLoanRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response.InitiateRepaymentResponse;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.rewamp.advance.repay.interactor.IVerifyRepaymentActivityInteractor;
import com.tez.androidapp.rewamp.advance.repay.interactor.VerifyRepaymentActivityInteractor;
import com.tez.androidapp.rewamp.advance.repay.view.IVerifyEasyPaisaRepaymentActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

public class VerifyEasyPaisaRepaymentActivityPresenter implements IVerifyEasyPaisaRepaymentActivityPresenter, IVerifyRepaymentActivityInteractorOutput {

    final IVerifyRepaymentActivityInteractor iVerifyRepaymentActivityInteractor;
    final IVerifyEasyPaisaRepaymentActivityView iVerifyEasyPaisaRepaymentActivityView;

    Integer repaymentId;

    public VerifyEasyPaisaRepaymentActivityPresenter(IVerifyEasyPaisaRepaymentActivityView iVerifyEasyPaisaRepaymentActivityView) {
        this.iVerifyEasyPaisaRepaymentActivityView = iVerifyEasyPaisaRepaymentActivityView;
        iVerifyRepaymentActivityInteractor = new VerifyRepaymentActivityInteractor(this);
    }

    @Override
    public void initiateRepayment(int loanId, int mobileAccountId) {
        iVerifyEasyPaisaRepaymentActivityView.setPinViewOtpEnabled(false);
        iVerifyEasyPaisaRepaymentActivityView.showTezLoader();
        InitiateRepaymentRequest request = new InitiateRepaymentRequest(loanId, mobileAccountId);
        iVerifyRepaymentActivityInteractor.initiateRepayment(request);
    }

    @Override
    public void onInitiateRepaymentSuccess(InitiateRepaymentResponse initiateRepaymentResponse) {
        this.repaymentId = initiateRepaymentResponse.getRepaymentId();
        iVerifyEasyPaisaRepaymentActivityView.dismissTezLoader();
        iVerifyEasyPaisaRepaymentActivityView.setPinViewOtpEnabled(true);
    }

    @Override
    public void onInitiateRepaymentFailure(int errorCode, String message) {
        iVerifyEasyPaisaRepaymentActivityView.dismissTezLoader();
        iVerifyEasyPaisaRepaymentActivityView.showError(errorCode,
                iVerifyEasyPaisaRepaymentActivityView.getFormattedErrorMessage(ResponseStatusCode.getDescriptionFromErrorCode(errorCode)),
                (dialog, which) -> iVerifyEasyPaisaRepaymentActivityView.finishActivity());
    }

    @Override
    public void repayLoan(@NonNull String pin, int mobileAccountId, double amount, @Nullable String repaymentCorrelationId, @Nullable Location location) {
        RepayLoanRequest request = new RepayLoanRequest();
        request.setAmount(amount);
        request.setRepaymentId(repaymentId);
        request.setMobileAccountId(mobileAccountId);
        request.setRepaymentCorrelationId(repaymentCorrelationId);
        if (location != null) {
            request.setLat(location.getLatitude());
            request.setLng(location.getLongitude());
        }
        setPinInRequest(request, pin);
        iVerifyRepaymentActivityInteractor.repayLoan(request);
    }

    @Override
    public void onRepayLoanSuccess(LoanDetails loanDetails) {
        iVerifyEasyPaisaRepaymentActivityView.routeToRepaymentSuccessFullActivity(loanDetails);
    }

    @Override
    public void onRepayLoanFailure(int errorCode, String message) {
        iVerifyEasyPaisaRepaymentActivityView.dismissTezLoader();
        iVerifyEasyPaisaRepaymentActivityView.setPinViewOtpClearText();
        iVerifyEasyPaisaRepaymentActivityView.showError(errorCode,
                iVerifyEasyPaisaRepaymentActivityView.getFormattedErrorMessage(ResponseStatusCode.getDescriptionFromErrorCode(errorCode)),
                (d, v) -> iVerifyEasyPaisaRepaymentActivityView.navigateToDashboardWithNewStack());
    }

    protected void setPinInRequest(@NonNull RepayLoanRequest request, @NonNull String pin) {
        request.setPin(pin);
    }
}
