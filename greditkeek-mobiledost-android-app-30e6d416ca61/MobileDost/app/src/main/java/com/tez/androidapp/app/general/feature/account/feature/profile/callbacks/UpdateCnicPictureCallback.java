package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

public interface UpdateCnicPictureCallback {
    void onUpdateCnicPictureSuccess();
    void onUpdateCnicPictureFailure(int errorCode, String message);
}
