package com.tez.androidapp.app.vertical.bima.policies.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetActiveInsurancePolicyCallback;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response.GetActiveInsurancePolicyResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Log;

/**
 * Created by Rehman Murad Ali on 8/25/2017.
 */

public class GetActiveInsurancePolicyRH extends BaseRH<GetActiveInsurancePolicyResponse> {

    private GetActiveInsurancePolicyCallback getActiveInsurancePolicyCallback;

    public GetActiveInsurancePolicyRH(BaseCloudDataStore baseCloudDataStore, GetActiveInsurancePolicyCallback getActiveInsurancePolicyCallback) {
        super(baseCloudDataStore);
        this.getActiveInsurancePolicyCallback = getActiveInsurancePolicyCallback;
    }

    @Override
    protected void onSuccess(Result<GetActiveInsurancePolicyResponse> value) {
        GetActiveInsurancePolicyResponse getActiveInsurancePolicyResponse = value.response().body();
        if (getActiveInsurancePolicyResponse!=null && getActiveInsurancePolicyResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            if (getActiveInsurancePolicyCallback != null)
                getActiveInsurancePolicyCallback.onGetActiveInsurancePolicySuccess(getActiveInsurancePolicyResponse.getPolicies());
        } else {
            onFailure(getActiveInsurancePolicyResponse==null?0:getActiveInsurancePolicyResponse.getStatusCode(), getActiveInsurancePolicyResponse==null?"":getActiveInsurancePolicyResponse.getErrorDescription());
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if(getActiveInsurancePolicyCallback!=null)
            getActiveInsurancePolicyCallback.onGetActiveInsurancePolicyFailure(errorCode,message);
    }
}
