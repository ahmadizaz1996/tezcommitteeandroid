package com.tez.androidapp.rewamp.advance.request.presenter;

import android.view.View;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.rewamp.advance.request.interactor.ILoanRequiredStepsActivityInteractor;
import com.tez.androidapp.rewamp.advance.request.interactor.LoanRequiredStepsActivityInteractor;
import com.tez.androidapp.rewamp.advance.request.view.ILoanRequiredStepsActivityView;

import java.util.List;

public class LoanRequiredStepsActivityPresenter implements ILoanRequiredStepsActivityPresenter, ILoanRequiredStepsActivityInteractorOutput {

    private final ILoanRequiredStepsActivityView iLoanRequiredStepsActivityView;
    protected  ILoanRequiredStepsActivityInteractor iLoanRequiredStepsActivityInteractor;

    private List<String> stepsLeft;

    public LoanRequiredStepsActivityPresenter(ILoanRequiredStepsActivityView iLoanRequiredStepsActivityView) {
        this.iLoanRequiredStepsActivityView = iLoanRequiredStepsActivityView;
        iLoanRequiredStepsActivityInteractor = new LoanRequiredStepsActivityInteractor(this);
    }

    @Override
    public void getLoanRemainingSteps() {
        iLoanRequiredStepsActivityView.setClContentVisibility(View.GONE);
        iLoanRequiredStepsActivityView.showShimmer();
        iLoanRequiredStepsActivityInteractor.getLoanRemainingSteps();
    }

    @Override
    public void removeStep(@NonNull String step) {
        this.stepsLeft.remove(step);
        this.setLoanRemainingSteps(stepsLeft);
    }

    @Override
    public void setLoanRemainingSteps(List<String> stepsLeft) {
        this.stepsLeft = stepsLeft;
        if (stepsLeft.isEmpty())
            iLoanRequiredStepsActivityView.finishActivityWithResultOk();

        else if (stepsLeft.size() == 1 && stepsLeft.get(0).equals(Constants.BENEFICIARY))
            iLoanRequiredStepsActivityView.routeToAddBeneficiaryActivity();

        else {
            iLoanRequiredStepsActivityView.setClContentVisibility(View.VISIBLE);
            iLoanRequiredStepsActivityView.setBtContinueOnClickListener(stepsLeft.get(0));
            iLoanRequiredStepsActivityView.setCompleteProfileCompleted(!stepsLeft.contains(Constants.COMPLETE_PROFILE));
            iLoanRequiredStepsActivityView.setMobileWalletCompleted(!stepsLeft.contains(Constants.MOBILE_WALLET));
        }
    }

    @Override
    public void onGetLoanRemainingStepsSuccess(List<String> stepsLeft) {
        iLoanRequiredStepsActivityView.hideShimmer();
        this.setLoanRemainingSteps(stepsLeft);
    }

    @Override
    public void onGetLoanRemainingStepsFailure(int errorCode, String message) {
        iLoanRequiredStepsActivityView.hideShimmer();
        iLoanRequiredStepsActivityView.showError(errorCode, (dialog, which) -> iLoanRequiredStepsActivityView.finishActivity());
    }

    @Override
    public void onClickBtContinue(@NonNull String nextStep) {
        switch (nextStep) {

            case Constants.COMPLETE_PROFILE:
                iLoanRequiredStepsActivityView.routeToCompleteProfile();
                break;

            case Constants.MOBILE_WALLET:
                iLoanRequiredStepsActivityView.routeToAddWallet();
                break;
        }
    }
}
