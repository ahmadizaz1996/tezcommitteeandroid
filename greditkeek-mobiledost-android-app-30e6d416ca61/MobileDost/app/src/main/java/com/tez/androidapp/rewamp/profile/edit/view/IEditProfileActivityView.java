package com.tez.androidapp.rewamp.profile.edit.view;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.commons.models.app.City;
import com.tez.androidapp.commons.models.network.User;

import java.io.File;
import java.util.List;

public interface IEditProfileActivityView extends IBaseView {
    void setDoubleTapSafeOnClickListenerOnTvEditPicture();

    void populateDropDown(@NonNull List<City> cities);

    void setDoubleTapSafeOnClickListenerToAutoCompleteTextViewCity();

    void setOnItemClickListenerToAutoCompleteTextViewCurrentCity(ArrayAdapter<City> cities);

    void setVisibillityToTilMobileNumber(int visibillity);

    void setVisibillityToTilEmail(int visibillity);

    void setVisibillityToTilCurrentAddress(int visibillity);

    void setVisibillityToAutoCompleteTextViewCurrentCity(int visibillity);

    void setVisibillityToBtContinue(int visibillity);

    void setVisibillityToTlShimmer(int visibillity);

    void setTextToEtMobileNumber(String text);

    void setTextToEtEmail(String text);

    void setTextToEtCurrentAddress(String text);

    void setCityTextToAutoCompleteTextViewCurrentCity(City city);

    void disableCustomInputsOfUserOnAutoCompleteTextCurrentCity(List<City> cities);

    void setBtContinueEnabled(boolean enabled);

    void setDoubleTapOnClickListenerOnBtContinue(User user);

    String getTextFromEtMobileNumber();

    String getTextFromEtEmail();

    String getTextFromEtCurrentAddress();

    Integer getSelectedCityId();

    void loadImageIntoIvUserImage(File file);

    void loadMyProfileActivity();

    void navigateToEditProfileVerificationActivity(@NonNull String mobileNumber,
                                                   String userAddress,
                                                   String userEmail,
                                                   int userId);
}
