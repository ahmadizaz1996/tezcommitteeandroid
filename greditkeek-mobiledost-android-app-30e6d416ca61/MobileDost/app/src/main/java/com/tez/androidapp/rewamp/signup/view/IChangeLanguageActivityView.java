package com.tez.androidapp.rewamp.signup.view;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;

/**
 * Created by VINOD KUMAR on 8/9/2019.
 */
public interface IChangeLanguageActivityView extends IBaseView {

    void onUpdateLanguageSuccess(@NonNull String language);

    void onUpdateLanguageFailure();
}
