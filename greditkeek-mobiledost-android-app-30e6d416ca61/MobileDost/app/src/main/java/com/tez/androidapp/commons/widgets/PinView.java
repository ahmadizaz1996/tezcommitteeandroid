package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;


import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

/**
 * Custom pin view to handle pin input
 * <p>
 * Created  on 5/3/2017.
 */
public class PinView extends TezLinearLayout implements DoubleTapSafeOnClickListener {

    TezEditTextView editTextPin;
    TezImageView[] imageViews = new TezImageView[6];
    View[] viewGap = new View[5];
    int defaultImage;
    int filledImage;
    int pinLength;


    public PinView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(context);
        View rootView = inflater.inflate(R.layout.view_four_digit_pin, this);
        editTextPin = rootView.findViewById(R.id.editTextPin);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PinView);
        float imageWidthHeight = typedArray.getDimension(R.styleable.PinView_indicatorSize, 40);
        float imageGap = typedArray.getDimension(R.styleable.PinView_indicatorGap, 40);
        pinLength = typedArray.getInt(R.styleable.PinView_pinLength, 2);
        if (pinLength < 2) pinLength = 2;
        else if (pinLength > 6) pinLength = 6;
        defaultImage = typedArray.getResourceId(R.styleable.PinView_indicatorDefault, 0);
        filledImage = typedArray.getResourceId(R.styleable.PinView_indicatorFilled, 0);
        TezLinearLayout.LayoutParams layoutParamsImageView = new LayoutParams((int) imageWidthHeight, (int)
                imageWidthHeight);
        TezLinearLayout.LayoutParams layoutParamsViewGap = new LayoutParams((int) imageGap, 1);
        imageViews[0] = rootView.findViewById(R.id.imageViewPin0);
        imageViews[1] = rootView.findViewById(R.id.imageViewPin1);
        imageViews[2] = rootView.findViewById(R.id.imageViewPin2);
        imageViews[3] = rootView.findViewById(R.id.imageViewPin3);
        imageViews[4] = rootView.findViewById(R.id.imageViewPin4);
        imageViews[5] = rootView.findViewById(R.id.imageViewPin5);
        viewGap[0] = rootView.findViewById(R.id.viewGap0);
        viewGap[1] = rootView.findViewById(R.id.viewGap1);
        viewGap[2] = rootView.findViewById(R.id.viewGap2);
        viewGap[3] = rootView.findViewById(R.id.viewGap3);
        viewGap[4] = rootView.findViewById(R.id.viewGap4);
        updateViewsForPinLength();
        for (TezImageView imageView : imageViews) {
            imageView.setDoubleTapSafeOnClickListener(this);
            imageView.setLayoutParams(layoutParamsImageView);
        }
        for (View view : viewGap) {
            view.setLayoutParams(layoutParamsViewGap);
        }
        typedArray.recycle();
        updateImageViews(editTextPin.getText().length());
        editTextPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Left
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateImageViews(s.length());
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Left
            }
        });
    }

    public void setImageViewsOnClickListeners(@Nullable DoubleTapSafeOnClickListener onClickListeners) {
        for (TezImageView imageView : imageViews)
            imageView.setDoubleTapSafeOnClickListener(onClickListeners);
    }

    private void updateViewsForPinLength() {
        for (int i = 0; i < 6; i++) {
            imageViews[i].setVisibility(i < pinLength ? VISIBLE : GONE);
            if (i > 0) viewGap[i - 1].setVisibility(i < pinLength ? VISIBLE : GONE);
        }
        editTextPin.setFilters(new InputFilter[]{new InputFilter.LengthFilter(pinLength)});
    }

    public int getPinLength() {
        return pinLength;
    }

    public void setPinLength(int pinLength) {
        if (pinLength < 2) pinLength = 2;
        else if (pinLength > 6) pinLength = 6;
        this.pinLength = pinLength;
        updateViewsForPinLength();
    }

    private void updateImageViews(int length) {
        for (int i = 0; i < imageViews.length; i++) {
            if (i < length) {
                imageViews[i].setImageResource(filledImage);
            } else {
                imageViews[i].setImageResource(defaultImage);
            }
        }
    }

    public CharSequence getText() {
        return editTextPin.getText();
    }

    public TezEditTextView getEditTextPin() {
        return editTextPin;
    }

    @Override
    public void doubleTapSafeOnClick(View v) {
        Utility.showKeyboard(editTextPin);
    }

    @Override
    public boolean performClick() {
        imageViews[0].performClick();
        return super.performClick();
    }
}
