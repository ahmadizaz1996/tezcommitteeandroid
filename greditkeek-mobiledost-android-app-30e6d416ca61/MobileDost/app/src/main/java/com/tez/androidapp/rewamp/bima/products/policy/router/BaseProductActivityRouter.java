package com.tez.androidapp.rewamp.bima.products.policy.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public abstract class BaseProductActivityRouter extends BaseActivityRouter {

    public static final String TITLE = "TITLE";

    public void setDependenciesAndRoute(@NonNull BaseActivity from, String title) {
        Intent intent = createIntent(from);
        intent.putExtra(TITLE, title);
        route(from, intent);
    }

    @NonNull
    protected abstract Intent createIntent(@NonNull BaseActivity from);
}
