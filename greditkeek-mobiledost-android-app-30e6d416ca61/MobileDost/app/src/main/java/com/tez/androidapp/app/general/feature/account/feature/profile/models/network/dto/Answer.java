package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto;

import androidx.annotation.NonNull;

import java.util.List;

public class Answer {
    private String refName;
    private String text;
    private String nextQuestionRefName;


    public String getNextQuestionRefName() {
        return nextQuestionRefName;
    }

    public void setNextQuestionRefName(String nextQuestionRefName) {
        this.nextQuestionRefName = nextQuestionRefName;
    }

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @NonNull
    @Override
    public String toString() {
        return text;
    }
}
