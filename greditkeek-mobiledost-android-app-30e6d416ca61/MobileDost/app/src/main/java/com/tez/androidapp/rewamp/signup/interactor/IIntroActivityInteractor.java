package com.tez.androidapp.rewamp.signup.interactor;

public interface IIntroActivityInteractor {
    void callUserInfo(String mobileNumber, String socialId, int socialType);
}
