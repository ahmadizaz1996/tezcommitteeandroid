package com.tez.androidapp.rewamp.general.profile.presenter;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.AccountLinkCallback;
import com.tez.androidapp.commons.models.network.User;

public interface IMyProfileActivityInteractorOutput {

    void onGetUserProfileSuccess(@NonNull User user);

    void onGetUserProfileFailure(int errorCode, String message);

    void accountLinkSuccess(Integer accountId);
    void accountLinkFailure(int errorCode, String message);
}
