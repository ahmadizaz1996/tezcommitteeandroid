package com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto;

import java.io.Serializable;

/**
 * Created by FARHAN DHANANI on 10/16/2018.
 */
public class DataUploadStatus implements Serializable {

    private boolean dataUpload;

    public boolean isDataUpload()
    {
        return dataUpload;
    }
}
