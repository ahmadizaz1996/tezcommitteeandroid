package com.tez.androidapp.rewamp.general.beneficiary.presenter;

import android.view.View;

import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy;
import com.tez.androidapp.rewamp.general.beneficiary.interactor.IManageBeneficiaryActivityInteractor;
import com.tez.androidapp.rewamp.general.beneficiary.interactor.ManageBeneficiaryActivityInteractor;
import com.tez.androidapp.rewamp.general.beneficiary.view.IManageBeneficiaryActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import java.util.List;

/**
 * Created by VINOD KUMAR on 8/30/2019.
 */
public class ManageBeneficiaryActivityPresenter implements IManageBeneficiaryActivityPresenter, IManageBeneficiaryActivityInteractorOutput {

    private final IManageBeneficiaryActivityView iManageBeneficiaryActivityView;
    private final IManageBeneficiaryActivityInteractor iManageBeneficiaryActivityInteractor;

    public ManageBeneficiaryActivityPresenter(IManageBeneficiaryActivityView iManageBeneficiaryActivityView) {
        this.iManageBeneficiaryActivityView = iManageBeneficiaryActivityView;
        iManageBeneficiaryActivityInteractor = new ManageBeneficiaryActivityInteractor(this);
    }

    @Override
    public void getManageBeneficiaryPolicies() {
        iManageBeneficiaryActivityView.showTezLoader();
        iManageBeneficiaryActivityInteractor.getManageBeneficiaryPolicies();
    }

    @Override
    public void onGetActiveInsurancePolicySuccess(List<Policy> policies) {
        if (policies != null && !policies.isEmpty())
            iManageBeneficiaryActivityView.setManageBeneficiaryAdapter(policies);
        else {
            iManageBeneficiaryActivityView.setRvPoliciesVisibility(View.GONE);
            iManageBeneficiaryActivityView.setLayoutEmptyBeneficiaryVisibility(View.VISIBLE);
        }
        iManageBeneficiaryActivityView.dismissTezLoader();
    }

    @Override
    public void onGetActiveInsurancePolicyFailure(int errorCode, String message) {
        iManageBeneficiaryActivityView.dismissTezLoader();
        if (errorCode == ResponseStatusCode.PROFILE_COMPLETION_REQUIRED.getCode())
            iManageBeneficiaryActivityView.showError(errorCode, (dialog, which) -> iManageBeneficiaryActivityView.onProfileIncompleteError());
        else
            iManageBeneficiaryActivityView.showError(errorCode, (dialog, which) -> iManageBeneficiaryActivityView.finishActivity());
    }
}
