package com.tez.androidapp.commons.models.network;



import com.tez.androidapp.commons.location.models.network.City;

import java.io.Serializable;

/**
 * Created  on 2/14/2017.
 */

public class UserAddress implements Serializable {

    private String postalCode;
    private String address;
    //private Integer cityId;
    private City city;

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

//    public Integer getCityId() {
//        return cityId;
//    }
//
//    public void setCityId(Integer cityId) {
//        this.cityId = cityId;
//    }

    public com.tez.androidapp.commons.models.app.City getCity() {
        return transformCity(city);
    }

    public void setCity(com.tez.androidapp.commons.models.app.City city){
        this.city = city == null? null: new City(city.getCityId(), city.getCityName());
    }

    private com.tez.androidapp.commons.models.app.City transformCity(City city) {
        return city== null? null: new com.tez.androidapp.commons.models.app.City(city);
    }

    @Override
    public String toString() {
        return "UserAddress{" +
                "postalCode='" + postalCode + '\'' +
                ", address='" + address + '\'' +
//                ", cityId=" + cityId +
                '}';
    }
}
