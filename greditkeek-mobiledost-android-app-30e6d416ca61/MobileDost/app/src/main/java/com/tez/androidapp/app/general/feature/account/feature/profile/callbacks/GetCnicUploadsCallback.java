package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.UserProfile;

public interface GetCnicUploadsCallback {
    void onGetCnicUploadsCallbackSuccess(UserProfile userProfile);
    void onGetCnicUploadsCallbackFailure(int errorCode, String message);
}
