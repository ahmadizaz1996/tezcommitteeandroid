package com.tez.androidapp.rewamp.dashboard.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.dashboard.entity.DashboardCards;

public class DashboardCardsResponse extends BaseResponse {

    private DashboardCards cards;

    DashboardCards getCards() {
        return cards;
    }
}
