package com.tez.androidapp.commons.models.app;

/**
 * Created by Rehman Murad Ali on 8/16/2017.
 */

public class OnBoardingPage {

    private int imageId;
    private int titleId;
    private int descriptionId;

    public OnBoardingPage(int imageId, int titleId, int descriptionId) {
        this.imageId = imageId;
        this.titleId = titleId;
        this.descriptionId = descriptionId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getTitleId() {
        return titleId;
    }

    public void setTitleId(int titleId) {
        this.titleId = titleId;
    }

    public int getDescriptionId() {
        return descriptionId;
    }

    public void setDescriptionId(int descriptionId) {
        this.descriptionId = descriptionId;
    }
}
