package com.tez.androidapp.rewamp.cloud.client;

import androidx.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.commons.utils.app.AESEncryptionUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

public class EncryptionClientInterceptor extends DecoratorClient implements Interceptor {

    public EncryptionClientInterceptor(BaseClient baseClient) {
        super(baseClient);
    }

    @Override
    public OkHttpClient.Builder buildOkHttpClient() {
        return baseClient.buildOkHttpClient().addInterceptor(this);
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        RequestBody oldBody = request.body();
        request = request.newBuilder().headers(request.headers()).header("X-Security-Control", "Y").method(request.method(), oldBody).build();
        MediaType mediaType = oldBody==null? null:oldBody.contentType();
        if (mediaType!=null && !mediaType.type().contains("multipart")) {
            Buffer buffer = new Buffer();
            oldBody.writeTo(buffer);
            String requestString = buffer.readUtf8();
            mediaType = MediaType.parse(String.valueOf(mediaType));
            String encryptedRequestString = AESEncryptionUtil.getInstance().encrypt(requestString);
            JSONObject jsonObjectRequest = new JSONObject();
            try {
                jsonObjectRequest.put("value", encryptedRequestString);
            } catch (JSONException e) {
                Crashlytics.logException(new Exception("EncryptionInterceptorRequest: " + request.url().toString() + e.getMessage()));
            }
            RequestBody body = RequestBody.create(mediaType, jsonObjectRequest.toString());
            request = request.newBuilder().headers(request.headers()).method(request.method(), body).build();
        }
        Response response = chain.proceed(request);


        String encryptedResponse = response.body().string();
        MediaType contentType = response.body().contentType();
        String encryptedResponseString = "";
        String decryptedResponseString = encryptedResponse;
        try {
            JSONObject jsonObjectResponse = new JSONObject(encryptedResponse);
            encryptedResponseString = jsonObjectResponse.getString("value");
            decryptedResponseString = AESEncryptionUtil.getInstance().decrypt(encryptedResponseString);
        } catch (JSONException e) {
            Crashlytics.logException(new Exception("EncryptionInterceptorResponse: " + request.url().toString() + e.getMessage()));
        }

        ResponseBody body = ResponseBody.create(contentType, decryptedResponseString);
        return response.newBuilder().body(body).build();
    }
}
