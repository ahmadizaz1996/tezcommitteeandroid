package com.tez.androidapp.rewamp.advance.repay.router;

import android.content.Intent;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.rewamp.advance.repay.view.RepaymentSuccessfulActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class RepaymentSuccessfulActivityRouter extends BaseActivityRouter {

    public static final String LOAN_DETAILS = "LOAN_DETAILS";

    public static RepaymentSuccessfulActivityRouter createInstance() {
        return new RepaymentSuccessfulActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, @NonNull LoanDetails loanDetails) {
        Intent intent = createIntent(from);
        intent.putExtra(LOAN_DETAILS, (Parcelable) loanDetails);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, RepaymentSuccessfulActivity.class);
    }
}
