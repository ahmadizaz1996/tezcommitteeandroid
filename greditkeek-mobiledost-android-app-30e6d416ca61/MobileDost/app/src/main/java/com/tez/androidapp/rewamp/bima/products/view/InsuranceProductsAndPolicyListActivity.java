package com.tez.androidapp.rewamp.bima.products.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.base.ui.fragments.BaseFragment;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.commons.widgets.TezToolbar;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class InsuranceProductsAndPolicyListActivity extends BaseActivity implements InsuranceProductsAndPolicyListCallback {

    @BindView(R.id.tvPackages)
    private TezTextView tvPackages;

    @BindView(R.id.tvMyPolicy)
    private TezTextView tvMyPolicy;

    @BindView(R.id.toolbar)
    private TezToolbar toolbar;

    @BindView(R.id.ivIllustration)
    private TezImageView ivIllustration;

    private ProductsFragment productsFragment;

    private PoliciesFragment policiesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance_products_and_policy_list);
        ViewBinder.bind(this);
        init();
    }

    private void init() {
        initFragments();
        initListeners();
        onClickProducts();
    }

    private void initFragments() {
        this.productsFragment = ProductsFragment.newInstance();
        this.policiesFragment = PoliciesFragment.newInstance();
    }

    private void initListeners() {
        tvPackages.setDoubleTapSafeOnClickListener(v -> onClickProducts());
        tvMyPolicy.setDoubleTapSafeOnClickListener(v -> onClickPolicies());
    }

    private void onClickProducts() {
        tvPackages.clearTextViewDrawableColor();
        tvPackages.setTextColor(Utility.getColorFromResource(R.color.textViewTextColorBlack));
        tvMyPolicy.clearTextViewDrawableColor();
        tvMyPolicy.setTextColor(Utility.getColorFromResource(R.color.disabledColor));
        replaceFragment(productsFragment);
    }

    private void onClickPolicies() {
        tvPackages.setTextViewDrawableColor(R.color.disabledColor);
        tvPackages.setTextColor(Utility.getColorFromResource(R.color.disabledColor));
        tvMyPolicy.setTextViewDrawableColor(R.color.textViewTextColorGreen);
        tvMyPolicy.setTextColor(Utility.getColorFromResource(R.color.textViewTextColorBlack));
        replaceFragment(policiesFragment);
    }

    private void replaceFragment(@NonNull BaseFragment baseFragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, baseFragment)
                .commit();
    }

    @Override
    public void showProductsFragment() {
        onClickProducts();
    }

    @Override
    public void setIllustrationVisibility(int visibility) {
        ivIllustration.setVisibility(visibility);
        toolbar.setToolbarTitle(visibility == View.GONE ? getResources().getString(R.string.tez_bima) : "");
    }

    @Override
    protected String getScreenName() {
        return "InsurancePackagesAndPolicyListActivity";
    }
}
