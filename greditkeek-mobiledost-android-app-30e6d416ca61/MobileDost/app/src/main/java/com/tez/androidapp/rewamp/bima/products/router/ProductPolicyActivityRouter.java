package com.tez.androidapp.rewamp.bima.products.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.bima.products.view.ProductPolicyActivity;

public class ProductPolicyActivityRouter extends BaseActivityRouter {

    private static final String POLICY_ID = "POLICY_ID";
    private static final String PRODUCT_ID = "PRODUCT_ID";
    private static final String PLAN_NAME = "PLAN_NAME";

    public static ProductPolicyActivityRouter createInstance() {
        return new ProductPolicyActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, int policyId, int productId, @NonNull String planName) {
        Intent intent = new Intent(from, ProductPolicyActivity.class);
        intent.putExtra(POLICY_ID, policyId);
        intent.putExtra(PRODUCT_ID, productId);
        intent.putExtra(PLAN_NAME, planName);
        route(from, intent);
    }

    @NonNull
    public static Dependencies getDependencies(@NonNull Intent intent) {
        return new Dependencies(intent);
    }

    public static class Dependencies {

        @NonNull
        private Intent intent;

        private Dependencies(@NonNull Intent intent) {
            this.intent = intent;
        }

        public int getProductId() {
            return intent.getIntExtra(PRODUCT_ID, -100);
        }

        public int getPolicyId() {
            return intent.getIntExtra(POLICY_ID, -100);
        }

        public String getPlanName() {
            return intent.getStringExtra(PLAN_NAME);
        }
    }
}
