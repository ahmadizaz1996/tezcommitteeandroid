package com.tez.androidapp.rewamp.signup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.DeviceInfo;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.commons.utils.app.Utility;

import net.tez.fragment.util.optional.Optional;

import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezPinView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.CustomerSupport;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.general.changepin.router.ChangeTemporaryPinActivityRouter;
import com.tez.androidapp.rewamp.general.faquestion.router.FAQActivityRouter;
import com.tez.androidapp.rewamp.reactivate.account.router.AccountReactivateActivityRouter;
import com.tez.androidapp.rewamp.signup.presenter.IWelcomeBackActivityPresenter;
import com.tez.androidapp.rewamp.signup.presenter.WelcomeBackActivityPresenter;
import com.tez.androidapp.rewamp.signup.router.ContactUsActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NewIntroActivityRouter;
import com.tez.androidapp.rewamp.signup.router.WelcomeBackActivityNumberVerificationRouter;
import com.tez.androidapp.rewamp.signup.router.WelcomeBackActivityRouter;
import com.tez.androidapp.rewamp.signup.router.WelcomeBackPermissionsActivityRouter;
import com.tez.androidapp.rewamp.signup.view.IWelcomeBackActivityView;
import com.tez.androidapp.rewamp.util.DialogUtil;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class WelcomeBackActivity extends BaseActivity
        implements TezPinView.OnPinEnteredListener, IBaseView, IWelcomeBackActivityView {

    @BindView(R.id.ivUserImage)
    private TezImageView ivUserImage;

    @BindView(R.id.pinView)
    private TezPinView pinView;

    @BindView(R.id.tvNumber)
    private TezTextView tvNumber;

    @BindView(R.id.tvGreetings)
    private TezTextView tvGreetings;

    @BindView(R.id.tvForgotPin)
    private TezTextView tvForgotPin;

    @BindView(R.id.tvFaq)
    private TezTextView tvFaq;

    @BindView(R.id.tvWelcome)
    private TezTextView tvWelcome;

    private User user;

    private final IWelcomeBackActivityPresenter iWelcomeBackActivityPresenter;

    private final BroadcastReceiver isOnValidatePinActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setResultCode(1);
        }
    };

    public WelcomeBackActivity() {
        iWelcomeBackActivityPresenter = new WelcomeBackActivityPresenter(this);
    }

    @Override
    public void onAppStart(boolean inactiveTimeExceeded) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_back);
        ViewBinder.bind(this);
        init();
    }

    private void init() {
        initUserData();
        initOnClickListeners();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.USER_SESSION_EXPIRED_REQUEST_CODE && resultCode == RESULT_OK) {
            finish();
        }
    }

    private void initUserData() {
        Optional.ifPresent(MDPreferenceManager.getUser(), user -> {
            Optional.doWhen(MDPreferenceManager.getAccessToken() != null,
                    () -> Utility.loadProfilePicture(ivUserImage));
            this.user = user;
            Optional.ifPresent(user.getFullName(), name -> {
                String greetings = Utility.getGreetings(this) + ", " + name;
                this.tvGreetings.setText(greetings);
                this.tvGreetings.setVisibility(View.VISIBLE);
                this.tvWelcome.setVisibility(View.GONE);
                this.tvNumber.setVisibility(View.GONE);
            }, () -> {
                this.tvWelcome.setVisibility(View.VISIBLE);
                this.tvNumber.setVisibility(View.VISIBLE);
                this.tvGreetings.setVisibility(View.GONE);
                this.tvNumber.setText(user.getMobileNumber());
            });
        });
    }

    private void initOnClickListeners() {
        this.pinView.setOnPinEnteredListener(this);
        this.tvFaq.setDoubleTapSafeOnClickListener(v -> FAQActivityRouter.createInstance().setDependenciesAndRoute(this));
        this.tvForgotPin.setDoubleTapSafeOnClickListener(v -> ContactUsActivityRouter.createInstance().setDependenciesAndRoute(this));
    }

    @Override
    public void onPinEntered(@NonNull String pin) {
        this.tvFaq.setEnabled(false);
        Utility.hideKeyboard(this, this.pinView);
        this.pinView.clearText();
        this.iWelcomeBackActivityPresenter.validatePin(pin, user.getMobileNumber());
    }


    @Override
    public void routeToDashboard() {
        Optional.doWhen(isUserSessionExpired(), this::finish,
                () -> Optional.ifPresent(userPushRoute(),
                        this::onPushRoute,
                        () -> DashboardActivityRouter.createInstance().setDependenciesAndRoute(this)));
    }

    private boolean isUserSessionExpired() {
        return getIntent().getBooleanExtra(WelcomeBackActivityRouter.USER_SESSION_TIMEOUT, false);
    }

    @Override
    public String userPushRoute() {
        return getIntent().getStringExtra(PushNotificationConstants.KEY_ROUTE_TO);
    }

    @Override
    public void makeCall() {
        CustomerSupport.openDialer(this);
    }

    @Override
    public void startValidation(LocationAvailableCallback callback) {
        getCurrentLocation(callback);
    }

    @Override
    public void dismissTezLoader() {
        super.dismissTezLoader();
        this.tvFaq.setEnabled(true);
    }

    @Override
    protected void performWorkAfterActivityTransition() {
        super.performWorkAfterActivityTransition();
        this.pinView.setEnabled(true);
    }

    @Override
    public void finishActivity() {
        this.pinView.setEnabled(true);
    }

    @Override
    public void showUnexpectedErrorDialog() {
        this.showInformativeMessage(R.string.string_something_unexpected_happened, (dialog, which) -> {
            NewIntroActivityRouter.createInstance().setDependenciesAndRoute(this);
            finish();
        });
    }

    @Override
    public void showAppOutDatedDialog(@StringRes int message) {
        DialogUtil.showActionableDialog(this,
                R.string.update_app,
                message,
                R.string.update,
                (dialog, which) -> {
                    if (which == DialogInterface.BUTTON_POSITIVE)
                        Utility.openPlayStoreForApp(this, this.getPackageName());
                });
    }

    @Override
    public DeviceInfo getDeviceInfo() {
        return Utility.getUserDeviceInfo(this);
    }

    @Override
    public void createSinchVerification(String mobileNumber, boolean callDeviceKey, String pin) {
        WelcomeBackActivityNumberVerificationRouter.createInstance().setDependenciesAndRoute(this,
                mobileNumber, pin, callDeviceKey, isUserSessionExpired(), userPushRoute());
    }

    @Override
    public void startChangeTemporaryPinActivity() {
        ChangeTemporaryPinActivityRouter.createInstance().setDependenciesAndRoute(getHostActivity());
    }

    @Override
    public void askPermission(String pin) {
        this.tvFaq.setEnabled(true);
        WelcomeBackPermissionsActivityRouter.createInstance().setDependenciesAndRoute(
                this, user.getMobileNumber(), pin, isUserSessionExpired(), userPushRoute());
    }

    @Override
    public void navigateToReactivateAccountActivity() {
        AccountReactivateActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onBackPressed() {
        if (isUserSessionExpired()) {
            moveTaskToBack(true);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(isOnValidatePinActivity, new IntentFilter(Constants.BROAD_CAST_RECIEVER_ON_VALIDATE_PIN_ACTIVITY));
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(isOnValidatePinActivity);
        } catch (IllegalArgumentException e) {
            //ignore
        }

    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    protected String getScreenName() {
        return "WelcomeBackActivity";
    }
}
