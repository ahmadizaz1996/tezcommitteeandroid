package com.tez.androidapp.rewamp.cloud.client;

import com.crashlytics.android.Crashlytics;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.readystatesoftware.chuck.ChuckInterceptor;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.services.TLSSocketFactory;
import com.tez.androidapp.rewamp.cloud.ApiClient;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class BaseClient {
    public abstract ApiClient build();

    protected abstract ApiClient build(OkHttpClient.Builder okBuilder);

    protected  <T> T  buildClient(Class<T> apiInterface, OkHttpClient.Builder okBuilder){
        return buildRetrofitClient(okBuilder).build().create(apiInterface);
    }

    protected OkHttpClient.Builder buildOkHttpClient(){
        //ChuckInterceptor chuckInterceptor = new ChuckInterceptor(MobileDostApplication.getInstance());
        //chuckInterceptor.showNotification(!(MobileDostApplication.getInstance().isProductionBuildChangeable() && Constants.IS_EXTERNAL_BUILD));
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        //okHttpClientBuilder.addInterceptor(chuckInterceptor);
        okHttpClientBuilder.followRedirects(true).followSslRedirects(true);
        okHttpClientBuilder.connectTimeout(Constants.SOCKET_TIME_OUT_LIMIT_SECONDS, TimeUnit.SECONDS);
        okHttpClientBuilder.readTimeout(Constants.SOCKET_TIME_OUT_LIMIT_SECONDS, TimeUnit.SECONDS);
        okHttpClientBuilder.writeTimeout(Constants.SOCKET_TIME_OUT_LIMIT_SECONDS, TimeUnit.SECONDS);
        try {
            TLSSocketFactory tlsSocketFactory = new TLSSocketFactory();
            okHttpClientBuilder.sslSocketFactory(tlsSocketFactory, tlsSocketFactory.systemDefaultTrustManager());
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            Crashlytics.logException(e);
        }
        return okHttpClientBuilder;
    }


    protected Retrofit.Builder buildRetrofitClient(OkHttpClient.Builder okHttpBuilder){
        return new Retrofit.Builder()
                .baseUrl(MDPreferenceManager.getBaseURL())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).client(buildClientWithCertificatePinner(okHttpBuilder));
    }

    private CertificatePinner getCertificatePinner(){
        return new CertificatePinner.Builder()
                .add("api.tezapp.pk", "sha256/XNC8oqf2eNe+zKDH6tjhD5KkvFmFLRSAJCWqOhAtmFM=")
                .add("*.tezapp.pk", "sha256/okPzl7NoFrWc4l54zQdr3Nw6/8NdS27jvsfM4yFePYg=")
                .build();
    }

    private OkHttpClient buildClientWithCertificatePinner(OkHttpClient.Builder okHttpClientBuilder){
        return okHttpClientBuilder.certificatePinner(getCertificatePinner()).build();
    }
}
