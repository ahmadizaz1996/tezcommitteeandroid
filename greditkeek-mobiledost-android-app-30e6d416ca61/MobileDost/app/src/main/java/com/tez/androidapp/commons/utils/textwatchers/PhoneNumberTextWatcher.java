package com.tez.androidapp.commons.utils.textwatchers;

import android.text.Editable;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.widgets.TezEditTextView;


public class PhoneNumberTextWatcher implements TezTextWatcher {

    private TezEditTextView mEditText;


    public PhoneNumberTextWatcher(@NonNull TezEditTextView mEditText) {
        this.mEditText = mEditText;
    }


    @Override
    public void afterTextChanged(Editable s) {
        String initials = "0";
        String text = s.toString();
        int beginIndex = -1;

        if (text.startsWith("3"))
            beginIndex = 0;

        else if (text.startsWith("92"))
            beginIndex = 2;

        else if (text.startsWith("0092"))
            beginIndex = 4;


        if (beginIndex != -1) {
            String finalText = initials.concat(text.substring(beginIndex));
            mEditText.setText(finalText);
            mEditText.setSelection(Math.min(finalText.length(), 11));
        }
    }
}
