package com.tez.androidapp.rewamp.general.faquestion.presenter;

public interface IFAQPresenter {
    void callForFAQS(String selectedLanguage);
}
