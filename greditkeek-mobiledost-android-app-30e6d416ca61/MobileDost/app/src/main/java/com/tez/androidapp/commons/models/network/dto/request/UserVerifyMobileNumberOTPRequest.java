package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 3/7/2017.
 */

public class UserVerifyMobileNumberOTPRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/verify/mobileNumberOtp/{" + Params.OTP + "}";

    public abstract static class Params {
        public static final String OTP = "otp";
    }
}
