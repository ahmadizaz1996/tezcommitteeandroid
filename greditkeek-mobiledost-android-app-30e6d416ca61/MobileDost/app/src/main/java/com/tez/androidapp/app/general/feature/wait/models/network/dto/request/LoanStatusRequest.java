package com.tez.androidapp.app.general.feature.wait.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 4/6/2017.
 */

public class LoanStatusRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/loan/status";
}
