package com.tez.androidapp.rewamp.advance.request.view;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;

public interface ILoanRequiredStepsActivityView extends IBaseView {

    void setCompleteProfileCompleted(boolean completed);

    void setMobileWalletCompleted(boolean completed);

    void setBtContinueOnClickListener(@NonNull String nextStep);

    void routeToAddBeneficiaryActivity();

    void routeToCompleteProfile();

    void routeToAddWallet();

    void setClContentVisibility(int visibility);

    void finishActivityWithResultOk();

    void showShimmer();

    void hideShimmer();
}
