package com.tez.androidapp.commons.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.notification.models.netwrok.Notification;

import java.util.ArrayList;
import java.util.List;

/**
 * Created  on 8/22/2017.
 */

public class UserNotificationsResponse extends BaseResponse {


    private int recordCount;
    private List<Notification> notifications;

    public int getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }

    public List<Notification> getNotifications() {
        return notifications == null ? new ArrayList<>() : notifications;

    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    @Override
    public String toString() {
        return "UserNotificationsResponse{" +
                "recordCount=" + recordCount +
                ", notifications=" + notifications +
                "} " + super.toString();
    }
}
