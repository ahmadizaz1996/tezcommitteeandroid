package com.tez.androidapp.rewamp.profile.edit.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.profile.edit.view.EditProfileVerificationActivity;
import com.tez.androidapp.rewamp.signup.router.NumberVerificationActivityRouter;

public class EditProfileVerificationActivityRouter extends BaseActivityRouter {

    public static final String CURRENT_ADDRESS = "currentAddress";
    public static final String EMAIL = "email";
    public static final String SELECTED_CITY = "selectedCity";

    public static EditProfileVerificationActivityRouter createInstance() {
        return new EditProfileVerificationActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @NonNull final String mobileNumebr,
                                        final String currentAddress,
                                        final String email,
                                        final int selectedCity) {
        Intent intent = createIntent(from);
        intent.putExtra(NumberVerificationActivityRouter.MOBILE_NUMBER, mobileNumebr);
        intent.putExtra(NumberVerificationActivityRouter.PRINCIPLE, MDPreferenceManager.getPrincipalName());
        intent.putExtra(CURRENT_ADDRESS, currentAddress);
        intent.putExtra(EMAIL, email);
        intent.putExtra(SELECTED_CITY, selectedCity);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, EditProfileVerificationActivity.class);
    }
}
