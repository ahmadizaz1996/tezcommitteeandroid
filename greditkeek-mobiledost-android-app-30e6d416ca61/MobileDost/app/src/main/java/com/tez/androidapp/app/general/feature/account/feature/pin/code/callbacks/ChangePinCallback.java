package com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by Rehman Murad Ali on 8/25/2017.
 */

public interface ChangePinCallback {

    void onChangePinSuccess(BaseResponse baseResponse);

    void onChangePinFailure(int errCode, String message);
}
