package com.tez.androidapp.rewamp.dashboard.entity;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class DashboardCards implements Serializable {

    @Nullable
    private Advance advance;

    @Nullable
    private Action action;

    @Nullable
    private String profileStatus;


    @Nullable
    public Advance getAdvance() {
        return advance;
    }

    @Nullable
    public Action getAction() {
        return action;
    }

    @Nullable
    public String getProfileStatus() {
        return profileStatus;
    }

    public void setAdvance(@Nullable Advance advance) {
        this.advance = advance;
    }

    public void setAction(@Nullable Action action) {
        this.action = action;
    }

    public void setProfileStatus(@Nullable String profileStatus) {
        this.profileStatus = profileStatus;
    }
}
