package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeLoginRequest;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface ICommitteeActivityInteractor extends IBaseInteractor {

    void getCommitteeMetada();

    void checkInvites(String mobileNumber);

    //void loginCall(CommitteeLoginRequest committeeLoginRequest);
}
