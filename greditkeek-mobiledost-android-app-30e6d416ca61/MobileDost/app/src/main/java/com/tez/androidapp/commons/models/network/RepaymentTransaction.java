package com.tez.androidapp.commons.models.network;

import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;

import java.io.Serializable;

/**
 * Created  on 9/2/2017.
 */

public class RepaymentTransaction implements Serializable {

    private Double amountRepayed;
    private String repaymentDate;
    private  int serviceProviderId;
    private String mobileAccountNumber;
    private Integer txnId;

    public int getServiceProviderId() {
        return serviceProviderId;
    }

    public String getMobileAccountNumber() {
        return mobileAccountNumber;
    }

    public Double getAmountRepayed() {
        return amountRepayed;
    }

    public String getRepaymentDate() {
        return Utility.getFormattedDateWithUpdatedTimeZone(repaymentDate, Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT);

    }

    public Integer getTxnId() {
        return txnId;
    }
}
