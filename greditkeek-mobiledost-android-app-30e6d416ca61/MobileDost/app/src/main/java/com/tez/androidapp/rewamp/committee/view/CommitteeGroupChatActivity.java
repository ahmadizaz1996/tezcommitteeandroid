package com.tez.androidapp.rewamp.committee.view;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.provider.FontRequest;
import androidx.emoji.bundled.BundledEmojiCompatConfig;
import androidx.emoji.text.EmojiCompat;
import androidx.emoji.text.FontRequestEmojiCompatConfig;
import androidx.emoji.widget.EmojiAppCompatEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.RelativeLayout;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Log;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.committee.adapter.CommitteeGroupChatAdapter;
import com.tez.androidapp.rewamp.committee.presenter.CommitteeFilterActivityPresenter;
import com.tez.androidapp.rewamp.committee.presenter.CommitteeGroupChatActivityPresenter;
import com.tez.androidapp.rewamp.committee.request.CommitteeSendMessageRequest;
import com.tez.androidapp.rewamp.committee.response.ChatMessage;
import com.tez.androidapp.rewamp.committee.response.CommitteeSendMessageResponse;
import com.tez.androidapp.rewamp.committee.response.GetGroupChatMessagesResponse;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;

import net.tez.filepicker.utils.Util;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

import static com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter.MY_COMMITTEE_RESPONSE;

public class CommitteeGroupChatActivity extends BaseActivity implements ICommitteeGroupChatActivityView, CommitteeGroupChatAdapter.ChatMessageListener, DoubleTapSafeOnClickListener {


    private static final String TAG = "CommitteeGroupChatActiv";

    @BindView(R.id.chatMessage_recyclerView)
    RecyclerView chatMessageRecyclerView;
    @BindView(R.id.sendIv)
    TezImageView sendMessageImageView;
    @BindView(R.id.messageTextEditText)
    EmojiAppCompatEditText messageTextEditText;
    @BindView(R.id.root)
    RelativeLayout root;
    @BindView(R.id.smileyIv)
    TezImageView smileyIv;

    private final CommitteeGroupChatActivityPresenter committeeGroupChatPresenter;
    private List<ChatMessage> messagesList = new ArrayList<>();
    private CommitteeGroupChatAdapter committeeGroupChatAdapter;
    private Handler mHandler;
    private boolean isTimer = false;
    private EmojIconActions emojIcon;
    private MyCommitteeResponse myCommitteeResponse;
    private int mCommitteeId;

    public CommitteeGroupChatActivity() {
        committeeGroupChatPresenter = new CommitteeGroupChatActivityPresenter(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

/*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            initEmojiCompat();
        }
*/

        setContentView(R.layout.activity_committee_group_chat);
        ViewBinder.bind(this);


//        emojIcon = new EmojIconActions(this, root, messageTextEditText, smileyIv);
//        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.ic_add_circle);

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey(MY_COMMITTEE_RESPONSE)) {
            myCommitteeResponse = (MyCommitteeResponse) getIntent().getExtras().getSerializable(MY_COMMITTEE_RESPONSE);

            if (myCommitteeResponse != null) {
                mCommitteeId = myCommitteeResponse.committeeList.get(0).getId();
            }
        }

        sendMessageImageView.setDoubleTapSafeOnClickListener(this);
        committeeGroupChatPresenter.getMessages(String.valueOf(mCommitteeId));
        smileyIv.setDoubleTapSafeOnClickListener(this);

        mHandler = new Handler(Looper.getMainLooper());
        startRepeatingTask();
    }

    @Override
    protected String getScreenName() {
        return CommitteeGroupChatActivity.class.getSimpleName();
    }

    @Override
    public void showLoader() {
        if (!isTimer) {
            showTezLoader();
        }
    }

    @Override
    public void hideLoader() {
        if (!isTimer)
            dismissTezLoader();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        if (!isTimer) {
            return createLoader();
        } else {
            return null;
        }
    }

    @Override
    public void onGetMessage(GetGroupChatMessagesResponse getGroupChatMessagesResponse) {
        isTimer = false;
        messagesList.clear();
        messagesList.addAll(getGroupChatMessagesResponse.getChatMessages());

        committeeGroupChatAdapter = new CommitteeGroupChatAdapter(getGroupChatMessagesResponse.getChatMessages(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, true);
        this.chatMessageRecyclerView.setLayoutManager(linearLayoutManager);
        this.chatMessageRecyclerView.setAdapter(committeeGroupChatAdapter);
        this.chatMessageRecyclerView.scrollToPosition(messagesList.size() - 1);
    }

    @Override
    public void onMessageSent(CommitteeSendMessageResponse committeeSendMessageResponse) {

        messageTextEditText.setText("");
        committeeGroupChatPresenter.getMessages(String.valueOf(mCommitteeId));
    }


    @Override
    public void onClickMessage() {

    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {
            case R.id.sendIv:
                sendMessage();
                break;
            case R.id.smileyIv:
//                emojIcon.ShowEmojIcon();
                Utility.showKeyboard(root);
                break;
        }
    }

    private void sendMessage() {
        String message = messageTextEditText.getText().toString();


        CommitteeSendMessageRequest committeeSendMessageRequest = new CommitteeSendMessageRequest();
        committeeSendMessageRequest.setCommitteeId(153L);
        committeeSendMessageRequest.setMessage(message);

        committeeGroupChatPresenter.sendMessage(committeeSendMessageRequest);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                isTimer = true;
                committeeGroupChatPresenter.getMessages(String.valueOf(mCommitteeId)); //this function can change value of mInterval.
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, 10000);
            }
        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }

    private void initEmojiCompat() {
        final EmojiCompat.Config config;
        if (true) {
            // Use the bundled font for EmojiCompat
            config = new BundledEmojiCompatConfig(getApplicationContext());
        } else {
            // Use a downloadable font for EmojiCompat
            final androidx.core.provider.FontRequest fontRequest = new FontRequest(
                    "com.google.android.gms.fonts",
                    "com.google.android.gms",
                    "Noto Color Emoji Compat",
                    R.array.com_google_android_gms_fonts_certs);
            config = new FontRequestEmojiCompatConfig(getApplicationContext(), fontRequest);
        }

        config.setReplaceAll(true)
                .registerInitCallback(new EmojiCompat.InitCallback() {
                    @Override
                    public void onInitialized() {
                        Log.i(TAG, "EmojiCompat initialized");
                    }

                    @Override
                    public void onFailed(@Nullable Throwable throwable) {
                        Log.e(TAG, "EmojiCompat initialization failed");
                    }
                });

        EmojiCompat.init(config);
    }
}