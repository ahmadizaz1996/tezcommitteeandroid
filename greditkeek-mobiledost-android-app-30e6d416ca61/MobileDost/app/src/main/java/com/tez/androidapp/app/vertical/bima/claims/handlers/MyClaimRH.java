package com.tez.androidapp.app.vertical.bima.claims.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.bima.claims.callback.MyClaimCallBack;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.MyClaimResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by FARHAN DHANANI on 6/29/2018.
 */
public class MyClaimRH extends BaseRH<MyClaimResponse> {

    private MyClaimCallBack myClaimCallBack;

    public MyClaimRH(BaseCloudDataStore baseCloudDataStore,
                     @Nullable MyClaimCallBack myClaimCallBack) {
        super(baseCloudDataStore);
        this.myClaimCallBack = myClaimCallBack;
    }

    @Override
    protected void onSuccess(Result<MyClaimResponse> value) {
        MyClaimResponse myClaimResponse = value.response().body();
        if (myClaimResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            if (this.myClaimCallBack != null)
                this.myClaimCallBack.onMyClaimSuccess(myClaimResponse.getClaimDetailDto());
        } else
            onFailure(myClaimResponse.getStatusCode(), myClaimResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (this.myClaimCallBack != null) this.myClaimCallBack.onMyClaimFailure(errorCode, message);
    }
}
