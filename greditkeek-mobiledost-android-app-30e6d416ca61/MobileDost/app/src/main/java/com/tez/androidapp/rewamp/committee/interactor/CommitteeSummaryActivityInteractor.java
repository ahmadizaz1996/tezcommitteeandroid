package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeCreationLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeDeclineListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeInviteListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeJoinListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeLeaveListener;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeCompletionActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeSummaryActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeDeclineRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.request.JoinCommitteeRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeDeclineResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeLeaveResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeSummaryActivityInteractor implements ICommitteeSummaryInteractor {


    private final ICommitteeSummaryActivityInteractorOutput mICommitteeSummaryActivityInteractorOutput;

    public CommitteeSummaryActivityInteractor(ICommitteeSummaryActivityInteractorOutput mICommitteeSummaryActivityInteractorOutput) {
        this.mICommitteeSummaryActivityInteractorOutput = mICommitteeSummaryActivityInteractorOutput;
    }

    @Override
    public void inviteMembers(CommitteeInviteRequest committeeInviteRequest) {
        CommitteeAuthCloudDataStore.getInstance().getInvite(committeeInviteRequest, new CommitteeInviteListener() {
            @Override
            public void onInviteSuccess(CommitteeInviteResponse committeeInviteResponse) {
                mICommitteeSummaryActivityInteractorOutput.onCommitteeInviteSuccess(committeeInviteResponse);
            }

            @Override
            public void onInviteFailure(int errorCode, String message) {
                mICommitteeSummaryActivityInteractorOutput.onCommitteeInviteFailure(errorCode, message);
            }
        });
    }

    @Override
    public void leaveCommittee(String committeeId) {
        CommitteeAuthCloudDataStore.getInstance().leaveCommittee(committeeId, new CommitteeLeaveListener() {
            @Override
            public void onCommitteeLeaveSuccess(CommitteeLeaveResponse committeeLeaveResponse) {
                mICommitteeSummaryActivityInteractorOutput.onCommitteeLeaveSuccess(committeeLeaveResponse);
            }

            @Override
            public void onCommitteeLeaveFailure(int errorCode, String message) {
                mICommitteeSummaryActivityInteractorOutput.onCommitteeInviteFailure(errorCode, message);
            }

        });
    }

    @Override
    public void declineCommittee(String committeeId) {
        CommitteeAuthCloudDataStore.getInstance().declineCommittee(committeeId, new CommitteeDeclineListener() {

            @Override
            public void onCommitteeDeclineSuccess(CommitteeDeclineResponse committeeDeclineResponse) {
                mICommitteeSummaryActivityInteractorOutput.onCommitteeDeclineSuccess(committeeDeclineResponse);
            }

            @Override
            public void onCommitteeDeclineeFailure(int errorCode, String message) {
                mICommitteeSummaryActivityInteractorOutput.onCommitteeDeclineFailure(errorCode, message);
            }

        });
    }

    @Override
    public void joinCommittee(JoinCommitteeRequest joinCommitteeRequest) {
        CommitteeAuthCloudDataStore.getInstance().joinCommittee(joinCommitteeRequest, new CommitteeJoinListener() {

            @Override
            public void onCommitteeJoinSuccess(JoinCommitteeResponse joinCommitteeResponse) {
                mICommitteeSummaryActivityInteractorOutput.onCommitteeJoinSuccess(joinCommitteeResponse);
            }

            @Override
            public void onCommitteeJoinFailure(int errorCode, String message) {
                mICommitteeSummaryActivityInteractorOutput.onCommitteeJoinFailure(errorCode, message);
            }
        });
    }

    /*@Override
    public void createCommittee(CommitteeCreateRequest committeeCreateRequest) {
        CommitteeAuthCloudDataStore.getInstance().createCommittee(committeeCreateRequest, new CommitteeCreationLIstener() {
            @Override
            public void onCommitteeCreateSuccess(CommitteeCreateResponse committeeCreateResponse) {
                mICommitteeCompletionActivityInteractorOutput.onCommitteeCreationSuccess(committeeCreateResponse);
            }

            @Override
            public void onCommitteeCreateFailed(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    createCommittee(committeeCreateRequest);
                else
                    mICommitteeCompletionActivityInteractorOutput.onCommitteeCreationFailure(errorCode, message);
            }

        });
    }*/
}

