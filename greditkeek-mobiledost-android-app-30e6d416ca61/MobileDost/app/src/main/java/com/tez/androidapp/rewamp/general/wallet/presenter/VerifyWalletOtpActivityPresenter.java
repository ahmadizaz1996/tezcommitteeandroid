package com.tez.androidapp.rewamp.general.wallet.presenter;

import androidx.annotation.StringRes;

import com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request.AddWalletRequest;
import com.tez.androidapp.rewamp.general.wallet.interactor.IVerifyWalletOtpActivityInteractor;
import com.tez.androidapp.rewamp.general.wallet.interactor.VerifyWalletOtpActivityInteractor;
import com.tez.androidapp.rewamp.general.wallet.view.IVerifyWalletOtpActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

public class VerifyWalletOtpActivityPresenter implements IVerifyWalletOtpActivityPresenter, IVerifyWalletOtpActivityInteractorOutput {

    private final IVerifyWalletOtpActivityView iVerifyWalletOtpActivityView;
    private final IVerifyWalletOtpActivityInteractor iVerifyWalletOtpActivityInteractor;

    public VerifyWalletOtpActivityPresenter(IVerifyWalletOtpActivityView iVerifyWalletOtpActivityView) {
        this.iVerifyWalletOtpActivityView = iVerifyWalletOtpActivityView;
        iVerifyWalletOtpActivityInteractor = new VerifyWalletOtpActivityInteractor(this);
    }

    @Override
    public void addWallet(int serviceProviderId, String mobileAccountNumber, String pin, boolean isDefault) {
        AddWalletRequest request = new AddWalletRequest();
        request.setWalletServiceProviderId(serviceProviderId);
        request.setMobileAccountNumber(mobileAccountNumber);
        request.setPin(pin);
        request.setDefault(isDefault);
        iVerifyWalletOtpActivityView.showTezLoader();
        iVerifyWalletOtpActivityInteractor.addWallet(request);
    }

    @Override
    public void onAddWalletSuccess(int serviceProviderId, String mobileAccountNumber) {
        iVerifyWalletOtpActivityView.startTimer();
        iVerifyWalletOtpActivityView.dismissTezLoader();
    }

    @Override
    public void onAddWalletFailure(int serviceProviderId, int errorCode, String message) {
        iVerifyWalletOtpActivityView.setPinViewOtpClearText();
        iVerifyWalletOtpActivityView.dismissTezLoader();

        @StringRes int errorDescription = ResponseStatusCode.getDescriptionFromErrorCode(errorCode);

        if (errorCode == ResponseStatusCode.INVALID_WALLET_PIN_LINKING.getCode())
            iVerifyWalletOtpActivityView.showEasyPaisaError(errorDescription);

        else if (errorCode == ResponseStatusCode.WALLET_CUSTOMER_DOES_NOT_EXIST.getCode())
            iVerifyWalletOtpActivityView.showWalletDoesNotExistError(serviceProviderId, errorDescription);

        else if (errorCode == ResponseStatusCode.CNIC_DOES_NOT_MATCH_WALLET.getCode())
            iVerifyWalletOtpActivityView.showWalletDoesNotMatchCnicError(serviceProviderId, errorDescription);

        else if (errorCode == ResponseStatusCode.WALLET_CANNOT_BE_ADDED.getCode())
            iVerifyWalletOtpActivityView.showError(errorCode,
                    (d, v) -> this.iVerifyWalletOtpActivityView.routeToCompleteYourProfileActivity());

        else {
            String errorMessage = iVerifyWalletOtpActivityView.getFormattedErrorMessage(serviceProviderId, errorDescription);
            iVerifyWalletOtpActivityView.showError(errorCode, errorMessage, (dialog, which) -> iVerifyWalletOtpActivityView.finishActivity());
        }
    }

    @Override
    public void verifyAddWalletOtp(int serviceProviderId, String mobileAccountNumber, String otp) {
        if (serviceProviderId != -1 && mobileAccountNumber != null) {
            iVerifyWalletOtpActivityView.showTezLoader();
            iVerifyWalletOtpActivityInteractor.verifyAddWalletOtp(serviceProviderId, mobileAccountNumber, otp);
        } else {
            iVerifyWalletOtpActivityView.setPinViewOtpClearText();
            iVerifyWalletOtpActivityView.setPinViewOtpEnabled(true);
        }
    }

    @Override
    public void onAddWalletVerifyOTPSuccess() {
        iVerifyWalletOtpActivityView.startWalletAddedActivity();
    }

    @Override
    public void onAddWalletVerifyOTPFailure(int serviceProviderId, int errorCode) {
        iVerifyWalletOtpActivityView.dismissTezLoader();
        iVerifyWalletOtpActivityView.setPinViewOtpClearText();
        if (errorCode == ResponseStatusCode.WALLET_CANNOT_BE_ADDED.getCode()) {
            iVerifyWalletOtpActivityView.showError(errorCode,
                    (d, v) -> this.iVerifyWalletOtpActivityView.routeToCompleteYourProfileActivity());
        } else {
            iVerifyWalletOtpActivityView.setPinViewOtpEnabled(true);
            String message = iVerifyWalletOtpActivityView.getFormattedErrorMessage(serviceProviderId, ResponseStatusCode.getDescriptionFromErrorCode(errorCode));
            iVerifyWalletOtpActivityView.showError(errorCode, message);
        }
    }

    @Override
    public void resendAddWalletOtp(int serviceProviderId, String mobileAccountNumber) {
        if (serviceProviderId != -1 && mobileAccountNumber != null) {
            iVerifyWalletOtpActivityView.setTvResendCodeEnabled(false);
            iVerifyWalletOtpActivityView.showTezLoader();
            iVerifyWalletOtpActivityInteractor.resendAddWalletOtp(serviceProviderId, mobileAccountNumber);
        }
    }

    @Override
    public void onAddWalletResendOTPSuccess() {
        iVerifyWalletOtpActivityView.setPinViewOtpClearText();
        iVerifyWalletOtpActivityView.dismissTezLoader();
        iVerifyWalletOtpActivityView.startTimer();
    }

    @Override
    public void onAddWalletResendOTPFailure(int errorCode, String message) {
        iVerifyWalletOtpActivityView.dismissTezLoader();
        iVerifyWalletOtpActivityView.setPinViewOtpClearText();
        if (errorCode == ResponseStatusCode.WALLET_CANNOT_BE_ADDED.getCode()) {
            iVerifyWalletOtpActivityView.showError(errorCode,
                    (d, v) -> this.iVerifyWalletOtpActivityView.routeToCompleteYourProfileActivity());
        } else {
            iVerifyWalletOtpActivityView.showError(errorCode);
            iVerifyWalletOtpActivityView.startTimer();
        }
    }
}
