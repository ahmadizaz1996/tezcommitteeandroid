package com.tez.androidapp.rewamp.general.wallet.presenter;

import android.view.View;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.rewamp.general.wallet.interactor.ChangeWalletActivityInteractor;
import com.tez.androidapp.rewamp.general.wallet.interactor.IChangeWalletActivityInteractor;
import com.tez.androidapp.rewamp.general.wallet.view.IChangeWalletActivityView;

import java.util.List;

public class ChangeWalletActivityPresenter implements IChangeWalletActivityPresenter, IChangeWalletActivityInteractorOutput {

    private final IChangeWalletActivityView iChangeWalletActivityView;
    private final IChangeWalletActivityInteractor iChangeWalletActivityInteractor;

    public ChangeWalletActivityPresenter(IChangeWalletActivityView iChangeWalletActivityView) {
        this.iChangeWalletActivityView = iChangeWalletActivityView;
        iChangeWalletActivityInteractor = new ChangeWalletActivityInteractor(this);
    }

    @Override
    public void getAllWallets() {
        iChangeWalletActivityView.setClContentVisibility(View.GONE);
        iChangeWalletActivityView.showShimmer();
        iChangeWalletActivityInteractor.getAllWallets();
    }

    @Override
    public void onGetAllWalletSuccess(List<Wallet> wallets) {
        iChangeWalletActivityView.initAdapter(wallets);
        iChangeWalletActivityView.hideShimmer();
        iChangeWalletActivityView.setClContentVisibility(View.VISIBLE);
    }

    @Override
    public void onGetAllWalletFailure(int errorCode, String message) {
        iChangeWalletActivityView.hideShimmer();
        iChangeWalletActivityView.showError(errorCode, (dialog, which) -> iChangeWalletActivityView.finishActivity());
    }
}
