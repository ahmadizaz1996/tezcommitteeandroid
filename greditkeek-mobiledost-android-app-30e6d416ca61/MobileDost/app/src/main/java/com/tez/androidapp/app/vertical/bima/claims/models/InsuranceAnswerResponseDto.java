package com.tez.androidapp.app.vertical.bima.claims.models;

import com.tez.androidapp.commons.utils.app.Constants;

import java.io.Serializable;

/**
 * Created by VINOD KUMAR on 11/13/2018.
 */
public class InsuranceAnswerResponseDto implements Serializable {

    protected String response;
    protected Double latitude;
    protected Double longitude;
    String additionalInfo;

    public void setAnswer(CommonAnswers insuranceAnswerDto) {
        //It will be overridden by sub classes
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public InsuranceAnswerResponseDto getInsuranceAnswerResponseDto(String questionType) {
        if (questionType.equalsIgnoreCase(Constants.STRING_MAP))
            return new MapTypeInsuranceAnswerResponseDto(response, additionalInfo, latitude, longitude);

        else if (questionType.equalsIgnoreCase(Constants.STRING_DATE))
            return new DateChoiceInsuranceAnswerResponseDto(response);
        
        else
            return new MultiChoiceInsuranceAnswerResponseDto(response);
    }
}
