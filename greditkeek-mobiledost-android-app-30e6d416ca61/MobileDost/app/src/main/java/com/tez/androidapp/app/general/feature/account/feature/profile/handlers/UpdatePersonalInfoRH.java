package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdatePersonalInfoCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.UpdatePersonalInfoResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;

public class UpdatePersonalInfoRH extends DashboardCardsRH<UpdatePersonalInfoResponse> {

    private final UpdatePersonalInfoCallback updatePersonalInfoCallback;

    public UpdatePersonalInfoRH(BaseCloudDataStore baseCloudDataStore,
                                UpdatePersonalInfoCallback updatePersonalInfoCallback) {
        super(baseCloudDataStore);
        this.updatePersonalInfoCallback = updatePersonalInfoCallback;
    }

    @Override
    protected void onSuccess(Result<UpdatePersonalInfoResponse> value) {
        super.onSuccess(value);
        UpdatePersonalInfoResponse updatePersonalInfoResponse = value.response().body();
        if (updatePersonalInfoResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            updatePersonalInfoCallback.onUpdatePersonalInfoSuccess(updatePersonalInfoResponse.isProfileCompleted());
        else
            onFailure(updatePersonalInfoResponse.getStatusCode(), updatePersonalInfoResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        updatePersonalInfoCallback.onUpdatePersonalInfoFailure(errorCode, message);
    }
}
