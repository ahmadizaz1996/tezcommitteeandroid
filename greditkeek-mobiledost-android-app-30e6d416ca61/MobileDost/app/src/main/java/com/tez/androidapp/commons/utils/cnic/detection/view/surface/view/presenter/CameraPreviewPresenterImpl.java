package com.tez.androidapp.commons.utils.cnic.detection.view.surface.view.presenter;

import android.hardware.Camera;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.commons.utils.cnic.detection.contracts.CNICDetectionCallback;
import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;
import com.tez.androidapp.commons.utils.cnic.detection.util.CNICTextClassificationUtil;
import com.tez.androidapp.commons.utils.cnic.detection.view.CnicScannerActivity;
import com.tez.androidapp.commons.utils.cnic.detection.view.surface.view.contracts.CameraPreviewPresenter;
import com.tez.androidapp.commons.utils.cnic.detection.view.surface.view.contracts.CameraPreviewView;
import com.tez.androidapp.commons.utils.cnic.firebase.callbacks.ImageFaceRetrieveCallback;
import com.tez.androidapp.commons.utils.cnic.firebase.callbacks.ImageTextRetrieveCallback;
import com.tez.androidapp.commons.utils.cnic.firebase.util.FirebaseVisionUtil;

import java.util.List;

public class CameraPreviewPresenterImpl implements CameraPreviewPresenter, ImageTextRetrieveCallback, ImageFaceRetrieveCallback {
    private static final int MINIMUM_REQUIRED_FIELD = 1;
    private final CameraPreviewView cameraPreviewView;
    private final CNICDetectionCallback cnicDetectionCallback;
    private final ImageFaceRetrieveCallback faceRetrieveCallback;
    private byte[] tempImageByte;
    private byte[] finalImageBytes;
    private RetumDataModel retumDataModel;
    private boolean areAllFieldsDetected = false;
    private boolean isImageInProcess = false;
    private boolean isAutoFocusing = false;
    private String detectObject;
    private int width;
    private int height;
    private int previewFormat;


    public <T extends CNICDetectionCallback & ImageFaceRetrieveCallback> CameraPreviewPresenterImpl(CameraPreviewView cameraPreviewView, T caller, String detectObject) {
        this.cameraPreviewView = cameraPreviewView;
        this.cnicDetectionCallback = caller;
        this.faceRetrieveCallback = caller;
        this.detectObject = detectObject;
    }

    @Override
    public void sendImageToFirebase(byte[] bytes, Camera camera, boolean isLive, boolean isFace) {
        this.tempImageByte = bytes;
        this.width = camera.getParameters().getPreviewSize().width;
        this.height = camera.getParameters().getPreviewSize().height;
        previewFormat = camera.getParameters().getPreviewFormat();
        if (!isFace)
            FirebaseVisionUtil.getInstance().detectTextFromByteBuffer(this, bytes, camera, detectObject);
        else
            FirebaseVisionUtil.getInstance().detectFaceFromByteBuffer(this, bytes, camera, isReversePortrait());
    }

    private boolean isReversePortrait() {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_FRONT, info);
        return info.orientation == 90;
    }

    @Override
    public void processByteArray(Camera mCamera, byte[] bytes) {
        if (!isImageInProcess) {
            if (!isAutoFocusing) {
                try {
                    mCamera.autoFocus((success, camera1) -> isAutoFocusing = false);
                    isAutoFocusing = true;
                } catch (RuntimeException e) {
                    cameraPreviewView.showAlertDialog("Could not auto-focus");

                }
            }
            setImageInProgress(true);
            try {
                sendImageToFirebase(bytes, mCamera, true, detectObject.equals(CnicScannerActivity.FACE));
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        }
    }


    @Override
    public void onTextRetrieveFailure(Exception exception) {
        setImageInProgress(false);
    }

    @Override
    public void onTextRetrieveSuccess(List<String> list) {
        RetumDataModel retumDataModel1 = classifyText(list);
        this.retumDataModel = this.retumDataModel == null ? retumDataModel1 : this.retumDataModel;
        this.finalImageBytes = this.finalImageBytes == null ? tempImageByte : finalImageBytes;
        if (RetumDataModel.compareTo(retumDataModel1, this.retumDataModel) > 0) {
            this.finalImageBytes = tempImageByte;
            this.retumDataModel = RetumDataModel.mergeFirebaseDataModel(retumDataModel1, this.retumDataModel);
        }
        if (RetumDataModel.getFirebaseDataModelScore(this.retumDataModel) == RetumDataModel.MAX_SCORE) {
            resetSettingsForSuccessfulIdentification();
            areAllFieldsDetected = true;
        }
        setImageInProgress(false);
    }

    private void setImageInProgress(boolean imageInProgress) {
        this.isImageInProcess = imageInProgress;
    }

    private RetumDataModel classifyText(List<String> blockTextList) {
        RetumDataModel retumDataModel1 = new RetumDataModel();
        CNICTextClassificationUtil cnicTextClassificationUtil = new CNICTextClassificationUtil(blockTextList);
        String cnic = cnicTextClassificationUtil.getCnicNumber();
        String cnicDateOfIssue = cnicTextClassificationUtil.getCnicDateOfIssue();
        String cnicDateOfExpiry = cnicTextClassificationUtil.getCnicDateOfExpiry();
        String cnicDateOfBirth = cnicTextClassificationUtil.getCnicDateOfBirth();
        retumDataModel1.setCnicNumber(cnic);
        retumDataModel1.setCnicIssueDate(cnicDateOfIssue);
        retumDataModel1.setCnicExpiryDate(cnicDateOfExpiry);
        retumDataModel1.setDateOfBirth(cnicDateOfBirth);
        return retumDataModel1;
    }

    private void resetSettingsForSuccessfulIdentification() {
        cameraPreviewView.resetSettings();
        if (finalImageBytes == null)
            cnicDetectionCallback.onCNICDetectionFailure();
        else
            cnicDetectionCallback.onCNICDetectionSuccess(finalImageBytes,
                width,
                height,
                previewFormat,
                retumDataModel);
    }


    @Override
    public void onTimeOut() {
        if (RetumDataModel.getFirebaseDataModelScore(this.retumDataModel) >= MINIMUM_REQUIRED_FIELD) {
            if (!areAllFieldsDetected)
                resetSettingsForSuccessfulIdentification();
        } else {
            cameraPreviewView.resetSettings();
            cnicDetectionCallback.onCNICDetectionFailure();

        }
    }

    @Override
    public void onFaceRetrieveFailure(Exception exception) {
        setImageInProgress(false);
        faceRetrieveCallback.onFaceRetrieveFailure(exception);
    }

    @Override
    public void onFaceRetrieveSuccess(List<Integer> faceListId) {
        setImageInProgress(false);
        if (!faceListId.isEmpty())
            faceRetrieveCallback.onFaceRetrieveSuccess(faceListId);
        else
            faceRetrieveCallback.onFaceRetrieveFailure(new Exception("No face detected"));
    }
}
