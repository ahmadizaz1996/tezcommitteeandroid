package com.tez.androidapp.rewamp.advance.repay.view;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.models.network.LoanDetails;

public interface IRepayLoanActivityView extends IBaseView {

    void initDetails(@NonNull LoanDetails loanDetails);

    void routeToVerifyEasyPaisaRepaymentActivity(@NonNull LoanDetails loanDetails, @NonNull Wallet wallet);

    void routeToVerifyJazzRepaymentActivity(@NonNull LoanDetails loanDetails, @NonNull Wallet wallet);

    void routeToVerifyRepaymentActivity(@NonNull LoanDetails loanDetails, @NonNull Wallet wallet);

    void setTvLatePaymentChargesVisibility(int visibility);

    void setClContentVisibility(int visibility);
}
