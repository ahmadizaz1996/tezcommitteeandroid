package com.tez.androidapp.rewamp.dashboard.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.callbacks.DashboardActionCardCallback;

public class DashboardActionCardRH extends BaseRH<DashboardActionCardResponse> {

    private final DashboardActionCardCallback callback;

    public DashboardActionCardRH(BaseCloudDataStore baseCloudDataStore, DashboardActionCardCallback callback) {
        super(baseCloudDataStore);
        this.callback = callback;
    }

    @Override
    protected void onSuccess(Result<DashboardActionCardResponse> value) {
        DashboardActionCardResponse response = value.response().body();
        if (response != null && response.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.callback.onDashboardActionCardSuccess(response);
        else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.callback.onDashboardActionCardFailure(errorCode, message);
    }
}
