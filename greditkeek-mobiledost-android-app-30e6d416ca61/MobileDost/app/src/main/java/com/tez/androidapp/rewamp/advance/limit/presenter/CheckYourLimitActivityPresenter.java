package com.tez.androidapp.rewamp.advance.limit.presenter;

import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.response.LoanLimitResponse;
import com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.response.UploadDataStatusResponse;
import com.tez.androidapp.rewamp.advance.limit.interactor.CheckYourLimitActivityInteractor;
import com.tez.androidapp.rewamp.advance.limit.interactor.ICheckYourLimitActivityInteractor;
import com.tez.androidapp.rewamp.advance.limit.view.ICheckYourLimitActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import static net.tez.fragment.util.optional.Optional.doWhen;

public class CheckYourLimitActivityPresenter implements ICheckYourLimitActivityPresenter, ICheckYourLimitActivityInteractorOutput {

    private final ICheckYourLimitActivityView iCheckYourLimitActivityView;
    private final ICheckYourLimitActivityInteractor iCheckYourLimitActivityInteractor;

    public CheckYourLimitActivityPresenter(ICheckYourLimitActivityView iCheckYourLimitActivityView) {
        this.iCheckYourLimitActivityView = iCheckYourLimitActivityView;
        iCheckYourLimitActivityInteractor = new CheckYourLimitActivityInteractor(this);
    }

    @Override
    public void applyLoanLimit() {
        iCheckYourLimitActivityView.stopTimer();
        iCheckYourLimitActivityView.showTezLoader();
        iCheckYourLimitActivityInteractor.checkUploadDataStatus();
    }

    @Override
    public void onUploadDataStatusSuccess(UploadDataStatusResponse uploadDataStatusResponse) {
        doWhen(uploadDataStatusResponse.getDataUploadStatus().isDataUpload(),
                iCheckYourLimitActivityView::startExtractionIntentServiceToUploadUserData);
        iCheckYourLimitActivityInteractor.postLoanLimit();
    }

    @Override
    public void onUploadDataStatusFailure(int errorCode, String message) {
        iCheckYourLimitActivityView.dismissTezLoader();
        iCheckYourLimitActivityView.showError(errorCode, (dialog, which) -> iCheckYourLimitActivityView.finishActivity());
    }

    @Override
    public void onLoanLimitSuccess(LoanLimitResponse loanLimitResponse) {
        iCheckYourLimitActivityView.startTimer();
        iCheckYourLimitActivityView.dismissTezLoader();
    }

    @Override
    public void onLoanLimitFailure(int errorCode, String message) {
        iCheckYourLimitActivityView.dismissTezLoader();

        if (errorCode == ResponseStatusCode.INVALID_DEVICE.getCode())
            iCheckYourLimitActivityView.onDeviceNotValidError(errorCode);

        else if (errorCode == ResponseStatusCode.CNIC_EXPIRED.getCode())
            iCheckYourLimitActivityView.onCnicExpiredError(errorCode);

        else {
            iCheckYourLimitActivityView.sendLimitFailureNotification();
            iCheckYourLimitActivityView.showError(errorCode, (dialog, which) -> iCheckYourLimitActivityView.finishActivity());
        }
    }
}
