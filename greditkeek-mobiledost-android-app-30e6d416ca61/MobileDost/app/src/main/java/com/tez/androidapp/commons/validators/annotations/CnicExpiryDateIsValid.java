package com.tez.androidapp.commons.validators.annotations;

import com.tez.androidapp.commons.validators.filters.CnicExpiryDateIsValidFilter;

import net.tez.validator.library.annotations.Filterable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by FARHAN DHANANI on 11/25/2018.
 */

@SuppressWarnings("unused")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Filterable(CnicExpiryDateIsValidFilter.class)
public @interface CnicExpiryDateIsValid {
    String dateFormat();

    int[] value();
}
