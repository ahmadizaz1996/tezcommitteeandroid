package com.tez.androidapp.app.vertical.bima.shared.vertical.push.reciever;

import com.tez.androidapp.app.base.push.reciever.BasePushNotificationReceiver;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;

import java.util.Map;

/**
 * Created by FARHAN DHANANI on 7/5/2018.
 */
public class InsuranceFireBasePushReceiver extends BasePushNotificationReceiver {

    public InsuranceFireBasePushReceiver(Map<String, String> notificationMap) {
        super(notificationMap);
    }

    @Override
    public void process() {
        String type = this.getNotificationMap().get(PushNotificationConstants.KEY_TYPE);

        if (type != null)
            sendNotification();
    }
}
