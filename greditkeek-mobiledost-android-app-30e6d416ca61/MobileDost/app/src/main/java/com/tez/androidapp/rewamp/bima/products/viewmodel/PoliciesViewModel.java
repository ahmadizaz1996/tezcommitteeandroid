package com.tez.androidapp.rewamp.bima.products.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetActiveInsurancePolicyCallback;
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.rewamp.general.network.model.NetworkCall;
import com.tez.androidapp.rewamp.general.network.viewmodel.NetworkBoundViewModel;

import java.util.List;

public class PoliciesViewModel extends NetworkBoundViewModel {

    private MutableLiveData<List<Policy>> policyListLiveData;


    public MutableLiveData<List<Policy>> getPolicyListLiveData() {
        if (policyListLiveData == null) {
            policyListLiveData = new MutableLiveData<>();
            getInsurancePolicies();
        }
        return policyListLiveData;
    }

    private void getInsurancePolicies() {
        networkCallMutableLiveData.setValue(new NetworkCall(NetworkCall.State.LOADING));
        BimaCloudDataStore.getInstance().getInsurancePolicies(new GetActiveInsurancePolicyCallback() {
            @Override
            public void onGetActiveInsurancePolicySuccess(List<Policy> policies) {
                policyListLiveData.setValue(policies);
                networkCallMutableLiveData.setValue(new NetworkCall(NetworkCall.State.SUCCESS));
            }

            @Override
            public void onGetActiveInsurancePolicyFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getInsurancePolicies();
                else
                    networkCallMutableLiveData.setValue(new NetworkCall(NetworkCall.State.FAILURE, errorCode));
            }
        });
    }
}
