package com.tez.androidapp.rewamp.signup.presenter;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.tez.androidapp.commons.models.network.dto.request.UserSignUpRequest;
import com.tez.androidapp.rewamp.signup.interactor.ISignupPermissionActivityInteractor;
import com.tez.androidapp.rewamp.signup.interactor.SignupPermissionActivityInteractor;
import com.tez.androidapp.rewamp.signup.view.ISignupPermissionActivityView;

public class SignupPermissionActivityPresenter implements
        ISignupPermissionActivityPresenter, ISignupPermissionActivityInteractorOutput {

    protected final ISignupPermissionActivityView iSignupPermissionActivityView;
    protected final ISignupPermissionActivityInteractor iSignupPermissionActivityInteractor;

    public SignupPermissionActivityPresenter(ISignupPermissionActivityView signUpActivityView) {
        this.iSignupPermissionActivityView = signUpActivityView;
        this.iSignupPermissionActivityInteractor = new SignupPermissionActivityInteractor(this);
    }

    @Override
    public void submitSignUpRequest(UserSignUpRequest userSignUpRequest) {
        this.iSignupPermissionActivityInteractor.submitUserSignUpRequest(userSignUpRequest);
    }

    @Override
    public void onSubmitSignUpRequestSuccess() {
        iSignupPermissionActivityView.setTezLoaderToBeDissmissedOnTransition();
        this.iSignupPermissionActivityView.createSinchVerification();
    }

    @Override
    public void onSubmitSignUpRequestFailure(int errorCode, String message) {
        iSignupPermissionActivityView.dismissTezLoader();
        this.iSignupPermissionActivityView.showError(errorCode);
    }

    @Override
    public void mobileNumberAlreadyExistForSignUp() {
        iSignupPermissionActivityView.setTezLoaderToBeDissmissedOnTransition();
        this.iSignupPermissionActivityView.navigateToMobileNumberAlreadyExistActivity();
    }
}
