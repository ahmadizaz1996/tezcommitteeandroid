package com.tez.androidapp.app.general.feature.authentication.signup.callbacks;

import com.tez.androidapp.commons.models.network.dto.response.UserSignUpResponse;

/**
 * Created  on 2/12/2017.
 */

public interface UserSignUpCallback {

    void onUserSignUpSuccess(UserSignUpResponse responseBody);

    void onUserSignUpError(int errorCode, String message);
}
