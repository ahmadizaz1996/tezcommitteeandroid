package com.tez.androidapp.commons.widgets;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewParent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.optional.Optional;

import com.tez.androidapp.rewamp.TezDrawableHelper;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.general.notification.view.NotificationActivity;
import com.tez.androidapp.rewamp.general.profile.router.MyProfileActivityRouter;
import com.tez.androidapp.rewamp.general.transactions.MyTransactionsActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.router.MyWalletActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VINOD KUMAR on 6/17/2019.
 */
public class TezFooterView extends View implements IBaseWidget {

    private List<TezImageView> ivList;
    private List<TezTextView> tvList;
    private int activeTab;
    private int activeRoundItem;
    private List<TezLinearLayout> layoutItemList;
    private BaseActivity baseActivity;

    public TezFooterView(Context context) {
        super(context);
    }

    public TezFooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public TezFooterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        if (context instanceof BaseActivity)
            this.baseActivity = (BaseActivity) context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TezFooterView);

        try {

            activeTab = typedArray.getInt(R.styleable.TezFooterView_active_tab, -1);
            activeRoundItem = typedArray.getInt(R.styleable.TezFooterView_active_round_item, -1);

        } finally {
            typedArray.recycle();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (parent instanceof ConstraintLayout) {
            ConstraintLayout layout = (ConstraintLayout) parent;
            inflate(getContext(), R.layout.view_tez_footer, layout);
            initViews(layout);
            setVisibility(GONE);
            if (activeTab != -1) {
                changeColor(activeTab);
                disableClickOnNavigationItem(activeTab);
            }

            if (activeRoundItem != -1) {
                changeBackground(activeRoundItem);
                disableClickOnRoundItem(activeRoundItem);
            }
        }
    }

    private void initViews(ConstraintLayout layout) {
        Group groupAllViews = layout.findViewById(R.id.groupAllViews);
        groupAllViews.setVisibility(getVisibility());
        ivList = new ArrayList<>();
        tvList = new ArrayList<>();
        layoutItemList = new ArrayList<>();

        ivList.add(layout.findViewById(R.id.ivProfile));
        ivList.add(layout.findViewById(R.id.ivActivity));
        ivList.add(layout.findViewById(R.id.ivWallet));
        ivList.add(layout.findViewById(R.id.ivNotification));

        tvList.add(layout.findViewById(R.id.tvProfile));
        tvList.add(layout.findViewById(R.id.tvActivity));
        tvList.add(layout.findViewById(R.id.tvWallet));
        tvList.add(layout.findViewById(R.id.tvNotification));

        layoutItemList.add(layout.findViewById(R.id.layoutItemBima));
        layoutItemList.add(layout.findViewById(R.id.layoutItemAdvance));
        layoutItemList.add(layout.findViewById(R.id.layoutItemHome));
        layoutItemList.add(layout.findViewById(R.id.layoutItemGold));
        layoutItemList.add(layout.findViewById(R.id.layoutItemCommittee));

        DoubleTapSafeOnClickListener openProfile = view -> {
            enterReveal(ivList.get(0));
            MyProfileActivityRouter.createInstance().setDependenciesAndRoute(baseActivity);
        };

        DoubleTapSafeOnClickListener openTransactions = view -> {
            enterReveal(ivList.get(1));
            MyTransactionsActivityRouter.createInstance().setDependenciesAndRoute(baseActivity);
        };

        DoubleTapSafeOnClickListener openWallet = view -> {
            enterReveal(ivList.get(2));
            MyWalletActivityRouter.createInstance().setDependenciesAndRoute(baseActivity);
        };

        DoubleTapSafeOnClickListener openNotification = view -> {
            enterReveal(ivList.get(3));
            baseActivity.startActivity(new Intent(baseActivity, NotificationActivity.class));
        };

        ivList.get(0).setDoubleTapSafeOnClickListener(openProfile);
        ivList.get(1).setDoubleTapSafeOnClickListener(openTransactions);
        ivList.get(2).setDoubleTapSafeOnClickListener(openWallet);
        ivList.get(3).setDoubleTapSafeOnClickListener(openNotification);

        tvList.get(0).setDoubleTapSafeOnClickListener(openProfile);
        tvList.get(1).setDoubleTapSafeOnClickListener(openTransactions);
        tvList.get(2).setDoubleTapSafeOnClickListener(openWallet);
        tvList.get(3).setDoubleTapSafeOnClickListener(openNotification);

        if (activeRoundItem != 2) {
            TezImageView ivFooterCircle = layout.findViewById(R.id.ivTezFooterCirce);
            ivFooterCircle.setDoubleTapSafeOnClickListener(view -> onClickFooterCircle());
        }
    }

    private void onClickFooterCircle() {
        Optional.ifPresent(baseActivity, baseActivity -> {
            DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(baseActivity);
        });
    }

    private void enterReveal(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            // get the center for the clipping circle
            int cx = view.getMeasuredWidth() / 2;
            int cy = view.getMeasuredHeight() / 2;

            // get the final radius for the clipping circle
            int finalRadius = Math.max(view.getWidth(), view.getHeight()) / 2;

            // create the animator for this view (the start radius is zero)
            Animator anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);

            // make the view visible and start the animation
            view.setVisibility(View.VISIBLE);
            anim.start();
        }
    }

    private void changeColor(int index) {
        int green = getColor(getContext(), R.color.textViewTextColorGreen);
        int black = getColor(getContext(), R.color.textViewTextColorBlack);
        ivList.get(index).setColorFilter(green);
        tvList.get(index).setTextColor(black);
    }

    private void disableClickOnNavigationItem(int index) {
        ivList.get(index).setDoubleTapSafeOnClickListener(null);
        tvList.get(index).setDoubleTapSafeOnClickListener(null);
    }

    private void disableClickOnRoundItem(int index) {
        layoutItemList.get(index).setDoubleTapSafeOnClickListener(null);
    }

    private void changeBackground(int index) {
        layoutItemList.get(index)
                .getChildAt(0)
                .setBackground(TezDrawableHelper.getDrawable(getContext(), R.drawable.bottom_navigation_round_bg_green));
    }

    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {

    }

    public void openWallet() {
        MyWalletActivityRouter.createInstance().setDependenciesAndRoute(baseActivity);
    }
}
