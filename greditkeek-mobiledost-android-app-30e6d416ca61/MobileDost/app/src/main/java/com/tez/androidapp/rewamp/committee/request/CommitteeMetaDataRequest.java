package com.tez.androidapp.rewamp.committee.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Ahmad Izaz on 06-Nov-20
 **/
public class CommitteeMetaDataRequest extends BaseRequest {

    public static final String METHOD_NAME = "http://3.21.127.35/api/v1/committee/metadata";

}
