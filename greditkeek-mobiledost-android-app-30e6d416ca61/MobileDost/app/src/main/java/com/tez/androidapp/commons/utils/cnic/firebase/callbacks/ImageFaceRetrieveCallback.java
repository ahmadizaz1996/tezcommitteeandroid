package com.tez.androidapp.commons.utils.cnic.firebase.callbacks;

import java.util.List;

public interface ImageFaceRetrieveCallback {
    void onFaceRetrieveFailure(Exception exception);

    void onFaceRetrieveSuccess(List<Integer> list);
}
