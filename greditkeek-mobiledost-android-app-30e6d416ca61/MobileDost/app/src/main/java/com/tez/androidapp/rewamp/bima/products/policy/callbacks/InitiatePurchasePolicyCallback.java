package com.tez.androidapp.rewamp.bima.products.policy.callbacks;

import com.tez.androidapp.rewamp.bima.products.policy.response.PolicyDetails;

public interface InitiatePurchasePolicyCallback {
    void initiatePurchasePolicySuccess(PolicyDetails policyDetails);
    void initiatePurchasePolicyFailure(int errorCode, String message);
}
