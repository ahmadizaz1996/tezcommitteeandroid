package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by Rehman Murad Ali on 9/7/2017.
 */

public interface UpdateUserProfileCallback {
    void onUpdateUserProfileSuccess();

    void onUpdateUserProfileFailure(int errorCode,String message);
}
