package com.tez.androidapp.rewamp.dashboard.presenter;

import com.tez.androidapp.rewamp.committee.listener.CommitteeLoginLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeMetadataLIstener;
import com.tez.androidapp.rewamp.dashboard.callbacks.DashboardActionCardCallback;
import com.tez.androidapp.rewamp.dashboard.callbacks.DashboardAdvanceCardCallback;
import com.tez.androidapp.rewamp.dashboard.callbacks.UserProfileStatusCallback;

public interface IDashboardActivityInteractorOutput extends DashboardAdvanceCardCallback, CommitteeMetadataLIstener, CommitteeLoginLIstener,  DashboardActionCardCallback,
        UserProfileStatusCallback {

}
