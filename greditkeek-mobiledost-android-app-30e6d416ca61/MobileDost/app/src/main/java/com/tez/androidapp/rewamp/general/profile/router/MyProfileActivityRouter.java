package com.tez.androidapp.rewamp.general.profile.router;

import android.content.Intent;
import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.profile.view.MyProfileActivity;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class MyProfileActivityRouter extends BaseActivityRouter {

    public static MyProfileActivityRouter createInstance() {
        return new MyProfileActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, MyProfileActivity.class);
    }
}
