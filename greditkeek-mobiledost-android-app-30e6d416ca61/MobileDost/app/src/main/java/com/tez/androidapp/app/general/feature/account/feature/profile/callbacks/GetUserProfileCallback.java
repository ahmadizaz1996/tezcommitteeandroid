package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.GetUserProfileResponse;

/**
 * Created by Rehman Murad Ali on 9/8/2017.
 */

public interface GetUserProfileCallback {

    void onGetUserProfileSuccess(GetUserProfileResponse getUserProfileResponse);

    void onGetUserProfileFailure(int errorCode, String message);

}
