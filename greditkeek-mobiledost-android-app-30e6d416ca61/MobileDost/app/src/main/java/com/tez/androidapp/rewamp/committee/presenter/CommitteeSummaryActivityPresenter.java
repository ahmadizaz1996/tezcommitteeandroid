package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.interactor.CommitteeCompletionActivityInteractor;
import com.tez.androidapp.rewamp.committee.interactor.CommitteeSummaryActivityInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeDeclineRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.request.JoinCommitteeRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeDeclineResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeLeaveResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.ICommitteeCompletionActivityView;
import com.tez.androidapp.rewamp.committee.view.ICommitteeSummaryActivityView;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeSummaryActivityPresenter implements ICommitteeSummaryActivityPresenter, ICommitteeSummaryActivityInteractorOutput {

    private final ICommitteeSummaryActivityView mICommitteeSummaryActivityView;
    private final CommitteeSummaryActivityInteractor mCommitteeSummaryActivityInteractor;

    public CommitteeSummaryActivityPresenter(ICommitteeSummaryActivityView mICommitteeSummaryActivityView) {
        this.mICommitteeSummaryActivityView = mICommitteeSummaryActivityView;
        mCommitteeSummaryActivityInteractor = new CommitteeSummaryActivityInteractor(this);
    }

    @Override
    public void inviteMembers(CommitteeInviteRequest committeeInviteRequest) {
//        mICommitteeSummaryActivityView.showLoader();
        mCommitteeSummaryActivityInteractor.inviteMembers(committeeInviteRequest);
    }

    @Override
    public void leaveCommittee(String committeeId) {
        mICommitteeSummaryActivityView.showLoader();
        mCommitteeSummaryActivityInteractor.leaveCommittee(committeeId);
    }

    @Override
    public void declineCommittee(String committeeId) {
        mICommitteeSummaryActivityView.showLoader();
        mCommitteeSummaryActivityInteractor.declineCommittee(committeeId);
    }

    @Override
    public void joinCommittee(JoinCommitteeRequest joinCommitteeRequest) {
        mICommitteeSummaryActivityView.showLoader();
        mCommitteeSummaryActivityInteractor.joinCommittee(joinCommitteeRequest);
    }

    @Override
    public void onCommitteeInviteSuccess(CommitteeInviteResponse committeeInviteResponse) {
//        mICommitteeSummaryActivityView.hideLoader();
        mICommitteeSummaryActivityView.onInviteMembers();
    }

    @Override
    public void onCommitteeInviteFailure(int errorCode, String message) {
//        mICommitteeSummaryActivityView.hideLoader();
        mICommitteeSummaryActivityView.showError(errorCode,
                (dialog, which) -> mICommitteeSummaryActivityView.finishActivity());
    }

    @Override
    public void onCommitteeLeaveSuccess(CommitteeLeaveResponse committeeLeaveResponse) {
        mICommitteeSummaryActivityView.hideLoader();
        mICommitteeSummaryActivityView.onLeaveCommittee(committeeLeaveResponse);
    }

    @Override
    public void onCommitteeLeaveFailure(int errorCode, String message) {
        mICommitteeSummaryActivityView.hideLoader();
        mICommitteeSummaryActivityView.showError(errorCode,
                (dialog, which) -> mICommitteeSummaryActivityView.finishActivity());
    }

    @Override
    public void onCommitteeDeclineSuccess(CommitteeDeclineResponse committeeDeclineResponse) {
        mICommitteeSummaryActivityView.hideLoader();
        mICommitteeSummaryActivityView.onDeclineCommittee(committeeDeclineResponse);
    }

    @Override
    public void onCommitteeDeclineFailure(int errorCode, String message) {
        mICommitteeSummaryActivityView.hideLoader();
        mICommitteeSummaryActivityView.showError(errorCode,
                (dialog, which) -> mICommitteeSummaryActivityView.finishActivity());
    }

    @Override
    public void onCommitteeJoinSuccess(JoinCommitteeResponse joinCommitteeResponse) {
        mICommitteeSummaryActivityView.hideLoader();
        mICommitteeSummaryActivityView.onJoinCommittee(joinCommitteeResponse);
    }

    @Override
    public void onCommitteeJoinFailure(int errorCode, String message) {
        mICommitteeSummaryActivityView.hideLoader();
        mICommitteeSummaryActivityView.showError(errorCode,
                (dialog, which) -> mICommitteeSummaryActivityView.finishActivity());
    }

    /*@Override
    public void onCommitteeCreationSuccess(CommitteeCreateResponse committeeCreateResponse) {
        mICommitteeSummaryActivityView.hideLoader();
        mICommitteeSummaryActivityView.onInviteMembers(committeeCreateResponse);
    }

    @Override
    public void onCommitteeCreationFailure(int errorCode, String message) {
        mICommitteeSummaryActivityView.hideLoader();
        mICommitteeSummaryActivityView.showError(errorCode,
                (dialog, which) -> mICommitteeSummaryActivityView.finishActivity());
    }

    @Override
    public void createCommittee(CommitteeCreateRequest committeeCreateRequest) {
        mICommitteeSummaryActivityView.showLoader();
        mCommitteeSummaryActivityInteractor.createCommittee(committeeCreateRequest);
    }*/

    /*@Override
    public void getInvite(List<CommitteeInviteRequest> committeeInviteRequest) {
        iCommitteeInviteActivityView.showLoader();
        committeeInviteActivityInteractor.getInvite(committeeInviteRequest);
    }

    @Override
    public void onInviteSuccess() {
        iCommitteeInviteActivityView.hideLoader();
        iCommitteeInviteActivityView.onInvite();
    }

    @Override
    public void onInviteFailure(int errorCode, String message) {
        iCommitteeInviteActivityView.hideLoader();
        iCommitteeInviteActivityView.showError(errorCode,
                (dialog, which) -> iCommitteeInviteActivityView.finishActivity());
    }*/

}
