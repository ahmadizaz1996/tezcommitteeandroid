package com.tez.androidapp.rewamp.general.beneficiary.view;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.general.beneficiary.entity.Beneficiary;

import java.util.List;

public interface IBeneficiaryListActivityView extends IBaseView {

    void setClContentVisibility(int visibility);

    void initAdapter(@NonNull List<Beneficiary> beneficiaryList);

    void finishActivityWithResultOk();

    void showShimmer();

    void hideShimmer();
}
