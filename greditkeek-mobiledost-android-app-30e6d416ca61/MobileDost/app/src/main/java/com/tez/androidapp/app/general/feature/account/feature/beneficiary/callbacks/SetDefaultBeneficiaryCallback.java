package com.tez.androidapp.app.general.feature.account.feature.beneficiary.callbacks;

public interface SetDefaultBeneficiaryCallback {

    void onSetDefaultBeneficiarySuccess();

    void onSetDefaultBeneficiaryFailure(int errorCode, String message);
}
