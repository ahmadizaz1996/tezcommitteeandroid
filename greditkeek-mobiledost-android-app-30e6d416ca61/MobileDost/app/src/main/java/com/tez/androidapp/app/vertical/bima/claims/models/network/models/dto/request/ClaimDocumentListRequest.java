package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request;

/**
 * Created by FARHAN DHANANI on 6/13/2018.
 */
public class ClaimDocumentListRequest {

    public static final String METHOD_NAME = "v1/insurance/claim/{" + ClaimDocumentListRequest.Params.INSURANCE_POLICY_ID + "}/documents";

    public abstract static class Params{
        public static final String INSURANCE_POLICY_ID = "insurancePolicyId";
    }
}
