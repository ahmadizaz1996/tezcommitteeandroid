package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Question;

import java.util.List;

public class PersonalQuestionResponse extends BaseResponse {

    private List<Question> data;

    public List<Question> getData() {
        return data;
    }

    public void setData(List<Question> data) {
        this.data = data;
    }
}
