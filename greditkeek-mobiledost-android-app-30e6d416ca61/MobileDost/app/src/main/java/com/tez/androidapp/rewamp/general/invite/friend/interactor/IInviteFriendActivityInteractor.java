package com.tez.androidapp.rewamp.general.invite.friend.interactor;

public interface IInviteFriendActivityInteractor {
    void getReferralCode();
}
