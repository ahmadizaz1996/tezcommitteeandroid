package com.tez.androidapp.rewamp.advance.limit.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

public interface ICheckYourLimitActivityInteractor extends IBaseInteractor {

    void checkUploadDataStatus();

    void postLoanLimit();
}
