package com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models;

import java.io.Serializable;

/**
 * Created by FARHAN DHANANI on 11/16/2018.
 */
public class LoanQuestion implements Serializable {
    private String sessionId;
    private Question question;
    private String answer;
    private boolean surveyCompleted = false;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Question getQuestion() {
        return question;
    }

    public boolean isSurveyCompleted() {
        return surveyCompleted;
    }

    public void setSurveyCompleted(boolean surveyCompleted) {
        this.surveyCompleted = surveyCompleted;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
