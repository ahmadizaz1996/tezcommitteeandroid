package com.tez.androidapp.app.general.feature.playback;

/**
 * Created by VINOD KUMAR on 5/10/2019.
 */
public interface AudioPlaybackListener {


    void onPlaybackStart();

    void onPlaybackComplete();
}
