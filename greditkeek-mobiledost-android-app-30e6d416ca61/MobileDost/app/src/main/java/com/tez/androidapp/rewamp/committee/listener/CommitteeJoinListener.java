package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeDeclineResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface CommitteeJoinListener {

    void onCommitteeJoinSuccess(JoinCommitteeResponse joinCommitteeResponse);

    void onCommitteeJoinFailure(int errorCode, String message);
}
