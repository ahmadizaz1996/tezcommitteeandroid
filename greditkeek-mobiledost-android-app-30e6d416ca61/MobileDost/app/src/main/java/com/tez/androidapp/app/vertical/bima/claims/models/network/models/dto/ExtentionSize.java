package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto;

import java.io.Serializable;

/**
 * Created by FARHAN DHANANI on 7/20/2018.
 */
public class ExtentionSize implements Serializable {
    private String name;
    private Double value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
