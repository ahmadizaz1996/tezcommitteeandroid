package com.tez.androidapp.rewamp.profile.edit.presenter;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateUserProfileCallback;

public interface IEditProfileVerificationActivityInteractorOutput extends UpdateUserProfileCallback {

    void onValidateInfoSuccess(String mobileNumber);

    void onValidateInfoFailure(final int errorCode, final String message);
}
