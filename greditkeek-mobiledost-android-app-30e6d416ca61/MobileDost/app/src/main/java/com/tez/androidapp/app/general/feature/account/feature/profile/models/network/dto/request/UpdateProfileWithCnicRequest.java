package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;

import java.util.List;

public class UpdateProfileWithCnicRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/user/profileCnic";
    private boolean cnicLifeTimeValid;
    private String fullName;
    private String gender;
    private String fatherName;
    private String husbandName;
    private String cnic;
    private String cnicDateOfIssue;
    private String cnicDateOfExpiry;
    private String dateOfBirth;
    private String address;
    private String motherMaidenName;
    private Integer placeOfBirthId;
    private Integer cityId;
    private Integer cnicFieldsDetectionStatus;
    private List<AnswerSelected> personalInformationList;

    public List<AnswerSelected> getPersonalInformationList() {
        return personalInformationList;
    }

    public void setPersonalInformationList(List<AnswerSelected> personalInformationList) {
        this.personalInformationList = personalInformationList;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public boolean isCnicLifeTimeValid() {
        return cnicLifeTimeValid;
    }

    public void setCnicLifeTimeValid(boolean cnicLifeTimeValid) {
        this.cnicLifeTimeValid = cnicLifeTimeValid;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getHusbandName() {
        return husbandName;
    }

    public void setHusbandName(String husbandName) {
        this.husbandName = husbandName;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getCnicDateOfIssue() {
        return cnicDateOfIssue;
    }

    public void setCnicDateOfIssue(String cnicDateOfIssue) {
        this.cnicDateOfIssue = cnicDateOfIssue;
    }

    public String getCnicDateOfExpiry() {
        return cnicDateOfExpiry;
    }

    public void setCnicDateOfExpiry(String cnicDateOfExpiry) {
        this.cnicDateOfExpiry = cnicDateOfExpiry;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMotherMaidenName() {
        return motherMaidenName;
    }

    public void setMotherMaidenName(String motherMaidenName) {
        this.motherMaidenName = motherMaidenName;
    }

    public Integer getPlaceOfBirthId() {
        return placeOfBirthId;
    }

    public void setPlaceOfBirthId(Integer placeOfBirthId) {
        this.placeOfBirthId = placeOfBirthId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getCnicFieldsDetectionStatus() {
        return cnicFieldsDetectionStatus;
    }

    public void setCnicFieldsDetectionStatus(Integer cnicFieldsDetectionStatus) {
        this.cnicFieldsDetectionStatus = cnicFieldsDetectionStatus;
    }

    public enum CnicFieldsDetectionStatus {
        AUTO_FILLED,
        AUTO_FILLED_EDITED,
        MANUAL_FILLED;
        public int getValue() { return ordinal() + 1; }
    }

}
