package com.tez.androidapp.rewamp.general.suspend.account.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public interface ISuspendAccountFeedbackActivityInteractor extends IBaseInteractor {

    void sendFeedback(String feedback);
}
