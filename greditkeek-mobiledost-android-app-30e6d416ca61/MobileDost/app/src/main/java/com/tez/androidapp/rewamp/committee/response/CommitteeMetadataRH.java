package com.tez.androidapp.rewamp.committee.response;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeMetadataLIstener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import androidx.annotation.Nullable;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class CommitteeMetadataRH extends BaseRH<CommitteeMetaDataResponse> {

    private final CommitteeMetadataLIstener listener;

    public CommitteeMetadataRH(BaseCloudDataStore baseCloudDataStore, CommitteeMetadataLIstener committeeMetadataLIstener) {
        super(baseCloudDataStore);
        this.listener = committeeMetadataLIstener;
    }

    @Override
    protected void onSuccess(Result<CommitteeMetaDataResponse> value) {
        CommitteeMetaDataResponse committeeMetaDataResponse = value.response().body();
        if (committeeMetaDataResponse != null && committeeMetaDataResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onCommitteeMetadataSuccess(committeeMetaDataResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onCommitteeMetadataFailure(errorCode, message);
    }
}
