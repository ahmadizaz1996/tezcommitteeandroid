package com.tez.androidapp.rewamp.general.wallet.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.tez.androidapp.rewamp.general.wallet.MyWalletActivity;
import com.tez.androidapp.rewamp.general.wallet.router.AddWalletTermsAndConditionActivityRouter;

public class AddWalletActivity extends MyWalletActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        resetCards();
        viewFooter.setVisibility(View.GONE);
        setLayoutAccountDetailVisibility(View.GONE);
        setEmptyWalletsVisibility(false);
    }

    @Override
    protected void getAllWallets() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AddWalletTermsAndConditionActivityRouter.REQUEST_CODE_ADD_WALLET && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected String getScreenName() {
        return "AddWalletActivity";
    }
}
