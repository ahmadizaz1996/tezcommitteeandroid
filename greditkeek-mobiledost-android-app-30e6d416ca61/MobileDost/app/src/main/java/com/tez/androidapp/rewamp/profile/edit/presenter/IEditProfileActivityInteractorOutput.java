package com.tez.androidapp.rewamp.profile.edit.presenter;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.SetUserPictureCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateUserProfileCallback;
import com.tez.androidapp.commons.location.callbacks.GetCitiesCallback;
import com.tez.androidapp.commons.models.network.User;

import java.io.File;

public interface IEditProfileActivityInteractorOutput
        extends GetCitiesCallback, UpdateUserProfileCallback {
    void onGetUserProfileSuccess(@NonNull User user);

    void onGetUserProfileFailure(int errorCode, String message);

    void setPictureOnSuccess(File file);
    void setPictureOnFailure(int errCode, String message);

    void onValidateInfoSuccess(final @NonNull String mobileNumber,
                               final String userAddress,
                               final String userEmail,
                               final int userId);
    void onValidateInfoFailure(final int errorCode, final String message);
}
