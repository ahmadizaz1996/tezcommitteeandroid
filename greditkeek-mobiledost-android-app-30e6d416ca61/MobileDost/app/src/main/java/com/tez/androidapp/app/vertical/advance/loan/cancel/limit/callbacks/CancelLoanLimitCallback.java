package com.tez.androidapp.app.vertical.advance.loan.cancel.limit.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by Rehman Murad Ali on 8/30/2017.
 */

public interface CancelLoanLimitCallback {

    void onCancelLoanLimitSuccess(BaseResponse response);

    void onCancelLoanLimitFailure(int errorCode, String message);

}
