package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.BasicInformation;

public class ProfileBasicResponse extends BaseResponse {

    private BasicInformation basicInformation;

    public BasicInformation getBasicInformation() {
        return basicInformation;
    }

    public void setBasicInformation(BasicInformation basicInformation) {
        this.basicInformation = basicInformation;
    }
}
