package com.tez.androidapp.commons.force.update;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezToolbar;
import com.tez.androidapp.rewamp.signup.router.ContactUsActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

public class ForceUpdateActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    private TezToolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_force_update);
        ViewBinder.bind(this);
        toolbar.setOnCustomerCareListener(view -> ContactUsActivityRouter.createInstance().setDependenciesAndRoute(this));
    }

    @OnClick(R.id.btUpdate)
    private void routeToPlayStore() {
        Utility.openPlayStoreForApp(this, getPackageName());
        finish();
    }

    @OnClick(R.id.tvClose)
    private void closeApp() {
        finish();
    }
}
