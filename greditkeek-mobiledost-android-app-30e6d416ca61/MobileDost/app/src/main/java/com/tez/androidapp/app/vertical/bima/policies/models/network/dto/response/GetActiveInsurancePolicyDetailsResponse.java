package com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.MobileUserPolicyDetailsDto;

/**
 * Created by FARHAN DHANANI on 7/18/2018.
 */
public class GetActiveInsurancePolicyDetailsResponse extends BaseResponse{
    private MobileUserPolicyDetailsDto mobileUserPolicyDto;

    public MobileUserPolicyDetailsDto getMobileUserPolicyDto() {
        return mobileUserPolicyDto;
    }

    public void setMobileUserPolicyDto(MobileUserPolicyDetailsDto mobileUserPolicyDto) {
        this.mobileUserPolicyDto = mobileUserPolicyDto;
    }
}
