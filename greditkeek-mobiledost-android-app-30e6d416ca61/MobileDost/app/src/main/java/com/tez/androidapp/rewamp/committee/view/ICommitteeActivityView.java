package com.tez.androidapp.rewamp.committee.view;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeMetaDataResponse;
import com.tez.androidapp.rewamp.committee.response.CommittteeLoginResponse;

import java.util.List;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface ICommitteeActivityView extends IBaseView {

    void showLoader();

    void hideLoader();

    void storeMetadata(CommitteeMetaDataResponse committeeMetaDataResponse);

    void committeeLoginSuccess(CommittteeLoginResponse committteeLoginResponse);

    void onCheckInvitesSuccess(List<CommitteeCheckInvitesResponse> committeeCheckInvitesResponse);

}
