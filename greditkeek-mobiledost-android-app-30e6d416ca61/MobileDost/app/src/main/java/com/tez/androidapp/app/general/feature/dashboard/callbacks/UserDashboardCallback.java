package com.tez.androidapp.app.general.feature.dashboard.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserDashboardResponse;

/**
 * Created  on 9/9/2017.
 */

public interface UserDashboardCallback {

    void onUserDashboardSuccess(UserDashboardResponse userDashboardResponse);

    void onUserDashboardFailure(BaseResponse baseResponse);
}
