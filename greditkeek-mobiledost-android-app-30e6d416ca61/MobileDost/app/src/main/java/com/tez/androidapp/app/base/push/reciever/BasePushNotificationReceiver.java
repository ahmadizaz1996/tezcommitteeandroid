package com.tez.androidapp.app.base.push.reciever;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.commons.utils.app.Utility;

import java.util.Map;

/**
 * Created by farhan on 4/19/18.
 */

public abstract class BasePushNotificationReceiver implements PushNotificationReciever {

    private final Map<String, String> notificationMap;

    public BasePushNotificationReceiver(Map<String, String> notificationMap) {
        this.notificationMap = notificationMap;
    }

    protected Map<String, String> getNotificationMap() {
        return notificationMap;
    }

    protected void sendNotification() {
        String message = getNotificationMap().get(PushNotificationConstants.KEY_MESSAGE_TEXT);
        String title = getNotificationMap().get(PushNotificationConstants.KEY_TITLE);
        Intent intent = new Intent(Constants.EMPTY_NOTIFICATION_INTENT);
        intent.putExtra(PushNotificationConstants.KEY_MESSAGE_TEXT, message);
        intent.putExtra(PushNotificationConstants.KEY_TITLE, title);
        Utility.sendNotification(intent, message);
    }

    protected void sendBroadcast(Intent intent, String message) {
        try {
            MobileDostApplication.getInstance().getApplicationContext().sendBroadcast(intent);
        } catch (NullPointerException e){
            Utility.sendNotification(intent, message);
        }
    }

    protected void sendOrderedBroadcast(Intent intent, BroadcastReceiver secondReceiver, String message) {

        try {
            Context appContext = MobileDostApplication.getInstance().getApplicationContext();
            appContext.sendOrderedBroadcast(intent,
                    null,
                    secondReceiver,
                    null,
                    Activity.RESULT_OK,
                    null,
                    null);
        } catch (NullPointerException e){
            Utility.sendNotification(intent, message);
        }
    }
}
