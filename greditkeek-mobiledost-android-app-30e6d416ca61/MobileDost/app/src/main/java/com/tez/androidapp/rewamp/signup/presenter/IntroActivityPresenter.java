package com.tez.androidapp.rewamp.signup.presenter;

import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.rewamp.signup.interactor.IIntroActivityInteractor;
import com.tez.androidapp.rewamp.signup.interactor.IntroActivityInteractor;
import com.tez.androidapp.rewamp.signup.view.IIntroActivityView;

public class IntroActivityPresenter implements IIntroActivityPresenter, IIntroActivityInteractorOutput {

    private final IIntroActivityInteractor iIntroActivityInteractor;
    private final IIntroActivityView iIntroActivityView;

    public IntroActivityPresenter(IIntroActivityView iIntroActivityView) {
        this.iIntroActivityView = iIntroActivityView;
        iIntroActivityInteractor = new IntroActivityInteractor(this);
    }

    @Override
    public void callUserInfo(String mobileNumber, String socialId, int socialType) {
        iIntroActivityView.showTezLoader();
        this.iIntroActivityInteractor.callUserInfo(mobileNumber, socialId, socialType);
    }

    @Override
    public void onUserInfoSuccess(String mobileNumber, String socialId, String principalName, int socialType, boolean isRegistered) {
        iIntroActivityView.setTezLoaderToBeDissmissedOnTransition();
        Optional.doWhen(isRegistered,
                () -> this.iIntroActivityView
                        .navigateToLoginActivity(principalName, socialId, socialType),
                () -> this.iIntroActivityView
                        .navigateToSignUpActivity(mobileNumber, socialId, socialType));
    }

    @Override
    public void onUserInfoFailure(int errorCode, String message) {
        iIntroActivityView.dismissTezLoader();
        this.iIntroActivityView.showError(errorCode);
    }
}
