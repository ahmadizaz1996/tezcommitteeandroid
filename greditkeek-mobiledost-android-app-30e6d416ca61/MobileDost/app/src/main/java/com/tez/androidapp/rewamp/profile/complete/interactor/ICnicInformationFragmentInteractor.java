package com.tez.androidapp.rewamp.profile.complete.interactor;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.UserProfile;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateProfileWithCnicRequest;
import com.tez.androidapp.commons.models.app.City;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.rewamp.profile.trust.interactor.IProfileTrustActivityInteractor;

public interface ICnicInformationFragmentInteractor extends IProfileTrustActivityInteractor {
    void getUserProfile(String lang, UserProfile userProfile);

    void updateUserProfile(User user, UpdateProfileWithCnicRequest updateProfileWithCnicRequest,
                           final boolean isPartialRequest, City selectedPlaceOfBirthCity, City selectedCurrentCity, AnswerSelected selectedMaritalStatus);
}
