package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.GoogleAuthFailedActivity;

public class GoogleAuthFailedActivityRouter extends BaseActivityRouter {
    public static GoogleAuthFailedActivityRouter createInstance() {
        return new GoogleAuthFailedActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, GoogleAuthFailedActivity.class);
    }
}
