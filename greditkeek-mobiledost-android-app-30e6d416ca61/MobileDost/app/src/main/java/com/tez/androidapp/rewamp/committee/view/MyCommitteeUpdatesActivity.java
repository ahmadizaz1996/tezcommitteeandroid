package com.tez.androidapp.rewamp.committee.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Log;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.committee.adapter.MyCommitteeUpdatesAdapter;
import com.tez.androidapp.rewamp.committee.presenter.CommitteeFilterActivityPresenter;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponseInviteList;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponseMemberList;
import com.tez.androidapp.rewamp.committee.response.CommitteeResponseFilterTransactionList;
import com.tez.androidapp.rewamp.committee.response.CommitteeResponseTimeline;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeFilterActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeUpdateActivityRouter;

import net.danlew.android.joda.JodaTimeAndroid;
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;


import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.chrono.GregorianChronology;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tez.androidapp.rewamp.committee.router.CommitteeUpdateActivityRouter.COMMITTEE_FILTER_RESPONSE;

public class MyCommitteeUpdatesActivity extends BaseActivity implements View.OnClickListener, MyCommitteeUpdatesAdapter.MyCommitteeInviteesListener, ICommitteeFilterActivityView {


    @BindView(R.id.updatesRecyclerView)
    RecyclerView updatesRecyclerView;

    @BindView(R.id.transactionRecyclerView)
    RecyclerView transactionRecyclerView;

    @BindView(R.id.invitesRecyclerView)
    RecyclerView invitesRecyclerView;

    @BindView(R.id.filter)
    ImageButton filter;


    private String time = "Just Now";


    private ArrayList<MyCommitteeResponse.Committee.Invite> inviteesList = new ArrayList<>();
    private final CommitteeFilterActivityPresenter committeeFilterPresenter;
    private CommitteeFilterResponse committeeFilterResponse;
    private List<CommitteeFilterResponseMemberList> committeeFilterResponseMemberLists;
    private List<CommitteeFilterResponseMemberList> _committeeFilterResponseMemberLists;
    private ArrayList<CommitteeFilterResponseInviteList> committeeFilterResponseInviteLists;
    private ArrayList<CommitteeFilterResponseInviteList> _committeeFilterResponseInviteLists;
    private ArrayList<CommitteeResponseFilterTransactionList> committeeFilterResponseTransactionLists;
    private ArrayList<CommitteeResponseFilterTransactionList> _committeeFilterResponseTransactionLists;
    private ArrayList<CommitteeResponseTimeline> finalList = new ArrayList<>();
    private HashMap<String, List<CommitteeResponseTimeline>> hashMap;
    private ArrayList<CommitteeResponseTimeline> finalCommitteeResponseTimeLine = new ArrayList<>();
    private boolean isOnNewIntent;

    public MyCommitteeUpdatesActivity() {
        committeeFilterPresenter = new CommitteeFilterActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_committee_updates);
        ViewBinder.bind(this);
        updatesRecyclerView.setVisibility(View.VISIBLE);
        fetchExtras();
//        setInviteesAdapter();
        filter.setOnClickListener(this);

        committeeFilterPresenter.getCommitteeFilter("153");
        JodaTimeAndroid.init(this);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        try {
            isOnNewIntent = true;
            Log.d("NEWINTENT", " In onNewIntent");
            if (intent != null && intent.getExtras() != null) {
                finalCommitteeResponseTimeLine = (ArrayList<CommitteeResponseTimeline>) intent.getExtras().getSerializable(COMMITTEE_FILTER_RESPONSE);

                if (finalCommitteeResponseTimeLine != null && finalCommitteeResponseTimeLine.size() > 0) {
                    MyCommitteeUpdatesAdapter myCommitteeUpdatesAdapter = new MyCommitteeUpdatesAdapter(finalCommitteeResponseTimeLine, this, this);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
                    this.updatesRecyclerView.setLayoutManager(linearLayoutManager);
                    updatesRecyclerView.setAdapter(myCommitteeUpdatesAdapter);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchExtras() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey(CommitteeUpdateActivityRouter.INVITEES_LIST)) {
            inviteesList = intent.getParcelableArrayListExtra(CommitteeUpdateActivityRouter.INVITEES_LIST);
        }
    }

    private void setInviteesAdapter() {
        if (committeeFilterResponse.getUpdates().getTimeline() != null && committeeFilterResponse.getUpdates().getTimeline().size() > 0) {
            MyCommitteeUpdatesAdapter myCommitteeUpdatesAdapter = new MyCommitteeUpdatesAdapter(evaluate(committeeFilterResponse.getUpdates().getTimeline()), this, this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            this.updatesRecyclerView.setLayoutManager(linearLayoutManager);
            updatesRecyclerView.setAdapter(myCommitteeUpdatesAdapter);


            /*MyCommitteeInviteAdapter myCommitteeInviteesAdapter = new MyCommitteeInviteAdapter(evaluateInviteList(committeeFilterResponse.getUpdates().getTimeline()));
            LinearLayoutManager _linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            this.transactionRecyclerView.setLayoutManager(_linearLayoutManager);
            transactionRecyclerView.setAdapter(myCommitteeInviteesAdapter);

            MyCommitteeTransactionsAdapter myCommitteeTransactionsAdapter = new MyCommitteeTransactionsAdapter(evaluateTransactionList(committeeFilterResponse.getUpdates().getTimeline()));
            LinearLayoutManager __linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            this.updatesRecyclerView.setLayoutManager(__linearLayoutManager);
            updatesRecyclerView.setAdapter(myCommitteeTransactionsAdapter);*/

        }

    }

    private List<CommitteeResponseTimeline> evaluate(List<CommitteeResponseTimeline> timeline) {
        ArrayList<CommitteeResponseTimeline> timeList = new ArrayList<>();
        hashMap = new HashMap<>();

        ArrayList<CommitteeResponseTimeline> committeeFilterResponseTimeline = new ArrayList<>();
//        _committeeFilterResponseMemberLists = new ArrayList<>();
        try {
            for (CommitteeResponseTimeline committeeResponseTimeline : timeline) {
//                hashMap.put(committeeResponseTimeline.getDate(), committeeResponseTimeline.getmMemberList());

/*
                CommitteeFilterResponseMemberList committeeFilterResponseMemberList = new CommitteeFilterResponseMemberList();
                committeeFilterResponseMemberList.setHeader(true);

                committeeFilterResponseMemberList.setJoiningDate(how_long_ago(committeeResponseTimeline.getDate()));
*/

/*                committeeFilterResponseMemberLists.add(committeeFilterResponseMemberList);
                committeeFilterResponseMemberLists.addAll(committeeResponseTimeline.getmMemberList());
                _committeeFilterResponseMemberLists.addAll(committeeResponseTimeline.getmMemberList());*/


//                committeeFilterResponseTimeline.add(committeeResponseTimeline);

                String longAgo = how_long_ago(committeeResponseTimeline.getDate());
                committeeResponseTimeline.setDate(longAgo);
                if (hashMap.containsKey(longAgo)) {
                    hashMap.get(longAgo).add(committeeResponseTimeline);
                } else {
                    ArrayList<CommitteeResponseTimeline> _committeeFilterResponseTimeline = new ArrayList<CommitteeResponseTimeline>();
                    _committeeFilterResponseTimeline.add(committeeResponseTimeline);
                    hashMap.put(longAgo, _committeeFilterResponseTimeline);
                }
            }

            for (Map.Entry<String, List<CommitteeResponseTimeline>> entry : hashMap.entrySet()) {
                String key = entry.getKey();
                List<CommitteeResponseTimeline> value = entry.getValue();
                CommitteeResponseTimeline committeeResponseTimeline = new CommitteeResponseTimeline();
                committeeResponseTimeline.setHeader(true);
                committeeResponseTimeline.setDate(key);
                timeList.add(committeeResponseTimeline);
                timeList.addAll(value);
                finalList.addAll(value);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return timeList;
    }

    private List<CommitteeFilterResponseInviteList> evaluateInviteList(List<CommitteeResponseTimeline> timeline) {
        HashMap<String, List<CommitteeFilterResponseInviteList>> hashMap = new HashMap<>();
        committeeFilterResponseInviteLists = new ArrayList<>();
        _committeeFilterResponseInviteLists = new ArrayList<>();
        try {
            for (CommitteeResponseTimeline committeeResponseTimeline : timeline) {
                hashMap.put(committeeResponseTimeline.getDate(), committeeResponseTimeline.getmInviteList());

                CommitteeFilterResponseInviteList committeeFilterResponseMemberList = new CommitteeFilterResponseInviteList();
                committeeFilterResponseMemberList.setHeader(true);

                committeeFilterResponseMemberList.setJoiningDate(how_long_ago(committeeResponseTimeline.getDate()));

                committeeFilterResponseInviteLists.add(committeeFilterResponseMemberList);
                committeeFilterResponseInviteLists.addAll(committeeResponseTimeline.getmInviteList());
                _committeeFilterResponseInviteLists.addAll(committeeResponseTimeline.getmInviteList());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return committeeFilterResponseInviteLists;
    }

    private List<CommitteeResponseFilterTransactionList> evaluateTransactionList(List<CommitteeResponseTimeline> timeline) {
        HashMap<String, List<CommitteeResponseFilterTransactionList>> hashMap = new HashMap<>();
        committeeFilterResponseTransactionLists = new ArrayList<>();
        _committeeFilterResponseTransactionLists = new ArrayList<>();
        try {
            for (CommitteeResponseTimeline committeeResponseTimeline : timeline) {
                hashMap.put(committeeResponseTimeline.getDate(), committeeResponseTimeline.getmTransactionList());

                CommitteeResponseFilterTransactionList committeeFilterResponseMemberList = new CommitteeResponseFilterTransactionList();
                committeeFilterResponseMemberList.setHeader(true);

                committeeFilterResponseMemberList.setJoiningDate(how_long_ago(committeeResponseTimeline.getDate()));

                committeeFilterResponseTransactionLists.add(committeeFilterResponseMemberList);
                committeeFilterResponseTransactionLists.addAll(committeeResponseTimeline.getmTransactionList());
                _committeeFilterResponseTransactionLists.addAll(committeeResponseTimeline.getmTransactionList());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return committeeFilterResponseTransactionLists;
    }

    private String how_long_ago(String created_at) {
        DateTime sinceGraduation = new DateTime(created_at, GregorianChronology.getInstance());
        DateTime currentDate = new DateTime(); //current date

        Months diffInMonths = Months.monthsBetween(sinceGraduation, currentDate);
        Days diffInDays = Days.daysBetween(sinceGraduation, currentDate);
        Hours diffInHours = Hours.hoursBetween(sinceGraduation, currentDate);
        Minutes diffInMinutes = Minutes.minutesBetween(sinceGraduation, currentDate);
        Seconds seconds = Seconds.secondsBetween(sinceGraduation, currentDate);

        Log.d("since grad", "before if " + sinceGraduation);
        if (diffInDays.isGreaterThan(Days.days(31))) {
            time = diffInMonths.getMonths() + " months ago";
            if (diffInMonths.getMonths() == 1) {
                time = diffInMonths.getMonths() + " month ago";
            } else {
                time = diffInMonths.getMonths() + " months ago";
            }
            return time;
        } else if (diffInHours.isGreaterThan(Hours.hours(24))) {
            if (diffInDays.getDays() == 1) {
                time = diffInDays.getDays() + " day ago";
            } else {
                time = diffInDays.getDays() + " days ago";
            }
            return time;
        } else if (diffInMinutes.isGreaterThan(Minutes.minutes(60))) {
            if (diffInHours.getHours() == 1) {
                time = diffInHours.getHours() + " hour ago";
            } else {
                time = diffInHours.getHours() + " hours ago";
            }
            return time;
        } else if (seconds.isGreaterThan(Seconds.seconds(60))) {
            if (diffInMinutes.getMinutes() == 1) {
                time = diffInMinutes.getMinutes() + " minute ago";
            } else {
                time = diffInMinutes.getMinutes() + " minutes ago";
            }
            return time;
        } else if (seconds.isLessThan(Seconds.seconds(60))) {
            return time;
        }
        Log.d("since grad", "" + sinceGraduation);
        return time;
    }

    private List<MyCommitteeResponse.Committee.Invite> evaluateUpdates(ArrayList<MyCommitteeResponse.Committee.Invite> inviteesList) {
        inviteesList.remove(0);
        return inviteesList;
    }

    @Override
    protected String getScreenName() {
        return MyCommitteeMembersActivity.class.getSimpleName();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.filter:
                CommitteeFilterActivityRouter.createInstance().setDependenciesAndRoute(this, finalList, hashMap);
                break;
        }
    }

    @Override
    public void onClickInvitees(@NonNull MyCommitteeResponse.Committee.Invite invite) {

    }

    @Override
    public void showLoader() {
        showTezLoader();
    }

    @Override
    public void hideLoader() {
        dismissTezLoader();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onFilter(CommitteeFilterResponse committeeFilterResponse) {
        this.committeeFilterResponse = committeeFilterResponse;
        if (!isOnNewIntent)
            setInviteesAdapter();
    }
}