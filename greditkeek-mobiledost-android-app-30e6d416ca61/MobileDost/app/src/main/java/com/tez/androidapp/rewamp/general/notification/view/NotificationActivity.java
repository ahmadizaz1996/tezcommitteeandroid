package com.tez.androidapp.rewamp.general.notification.view;

import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.notification.models.netwrok.Notification;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.rewamp.general.notification.adapter.NotificationAdapter;
import com.tez.androidapp.rewamp.general.notification.data.source.NetworkState;
import com.tez.androidapp.rewamp.general.notification.view.model.NotificationViewModel;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class NotificationActivity extends BaseActivity
        implements NotificationAdapter.NotificationItemListener {

    @BindView(R.id.notification_recycler)
    private RecyclerView recyclerViewItems;

    @BindView(R.id.shimmer)
    private TezLinearLayout shimmerLinearLayout;

    @BindView(R.id.layoutEmptyNotification)
    private TezConstraintLayout layoutEmptyNotification;

    private final NotificationAdapter notificationsAdapter;

    public NotificationActivity() {
        this.notificationsAdapter = new NotificationAdapter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ViewBinder.bind(this);
        init();
    }


    private void setVisibillityForLayoutEmptyNotification(int visibillity){
        this.layoutEmptyNotification.setVisibility(visibillity);
    }

    private void init() {
        this.shimmerLinearLayout.setVisibility(View.VISIBLE);
        initRecyclerView();
    }

    private void initRecyclerView() {
        NotificationViewModel notificationViewModel = ViewModelProviders.of(this).get(NotificationViewModel.class);
        notificationViewModel.setObserverToPagedList(this, this::submitList);

        notificationViewModel.setObserverToNetworkState(this,
                this::setNetworkState);
        this.recyclerViewItems.setAdapter(notificationsAdapter);
    }

    private void setNetworkState(NetworkState networkState) {
        Optional.doWhen(networkState.getStatus() ==
                        NetworkState.Status.FAILED,
                () -> onFailed(networkState.getErrorCode()));
        notificationsAdapter.setNetworkState(networkState);
    }

    private void submitList(PagedList<Notification> notifications) {
        notificationsAdapter.submitList(notifications);
        this.shimmerLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void onFailed(int errorCode) {
        Optional.doWhen(errorCode>0, ()-> showError(errorCode, (d, v) -> finishActivity()));
        this.shimmerLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void notificationsAreEmpty(boolean isEmpty) {
        this.setVisibillityForLayoutEmptyNotification(isEmpty? View.VISIBLE:View.GONE);
    }

    @Override
    protected String getScreenName() {
        return "NotificationActivity";
    }
}
