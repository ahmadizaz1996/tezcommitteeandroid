package com.tez.androidapp.app.general.feature.questions.models.network.models;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.vertical.bima.claims.models.CommonAnswers;
import com.tez.androidapp.commons.utils.app.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by FARHAN DHANANI on 11/16/2018.
 */
public class BimaQuestion implements CommonAnswers, Serializable {

    private Integer id;
    private String question;
    private String questionType;
    private String questionSubType;
    private String answer;
    private String additionalInfo;
    private List<String> options;
    private Double longitude;
    private Double latitude;
    private String status;
    private boolean answered;

    public boolean isAnswered() {
        return answered;
    }

    public void setAnswered(boolean answered) {
        this.answered = answered;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public void setQuestionSubType(String questionSubType) {
        this.questionSubType = questionSubType;
    }

    public List<String> getOptions() {
        if (options == null) {
            options = new ArrayList<>();
            options.add("Yes");
            options.add("No");
            options.add("Maybe");
        }
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    @NonNull
    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", questionType='" + questionType + '\'' +
                ", options=" + options +
                '}';
    }

    @Override
    public String getResponse() {
        return answer;
    }

    @Override
    public void setResponse(String answer) {
        this.answer = answer;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String s) {
        this.additionalInfo = s;
    }

    @Override
    public Double getLongitude() {
        return longitude;
    }

    @Override
    public void setLongitude(Double l) {
        longitude = l;
    }

    @Override
    public Double getLatitude() {
        return latitude;
    }

    @Override
    public void setLatitude(Double l) {
        latitude = l;
    }
}
