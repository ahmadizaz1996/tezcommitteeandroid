package com.tez.androidapp.rewamp.committee.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Log;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponseMemberList;
import com.tez.androidapp.rewamp.committee.response.CommitteeResponseTimeline;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.inflate;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/

public class  MyCommitteeUpdatesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> /*GenericRecyclerViewAdapter<CommitteeFilterResponseMemberList,
        MyCommitteeUpdatesAdapter.MyCommitteeInviteesListener,
        MyCommitteeUpdatesAdapter.MyCommitteeMemberViewHolder>*/ {

    public static final int HEADER_ITEM = 0;
    public static final int NORMAL_ITEM = 1;
    private final List<CommitteeResponseTimeline> items;
    private final Context context;


    public MyCommitteeUpdatesAdapter(@NonNull List<CommitteeResponseTimeline> items, @Nullable MyCommitteeInviteesListener listener, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (this.items.get(position).isHeader())
            return NORMAL_ITEM;
        else
            return HEADER_ITEM;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == NORMAL_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_date_item_1, parent, false);
            return new MyCommitteeMemberViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate
                    (R.layout.header_date_item, parent, false);
            return new MyCommitteeMemberHeaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CommitteeResponseTimeline committeeResponseTimeline = this.items.get(position);
        if (holder instanceof MyCommitteeMemberViewHolder) {
            MyCommitteeMemberViewHolder myCommitteeMemberViewHolder = (MyCommitteeMemberViewHolder) holder;

            myCommitteeMemberViewHolder.dateTxtView.setText(committeeResponseTimeline.getDate());

            /*myCommitteeMemberViewHolder.tvName.setText(committeeFilterResponseMemberList.getUsername());
            myCommitteeMemberViewHolder.tvMessage.setText("Member invited to join");
//            tvStatus.setText(item.getStatus());
            myCommitteeMemberViewHolder.tvDate.setText("2020-12-11");*/
        } else {
            MyCommitteeMemberHeaderViewHolder myCommitteeMemberHeaderViewHolder = (MyCommitteeMemberHeaderViewHolder) holder;
//            myCommitteeMemberHeaderViewHolder.datTxtView.setText(committeeResponseTimeline.getDate());

            if (committeeResponseTimeline.getmMemberList() != null && committeeResponseTimeline.getmMemberList().size() > 0) {
                Log.d("ADAPTER","member is visible");
                myCommitteeMemberHeaderViewHolder.memberRecyclerView.setVisibility(View.VISIBLE);
                MyCommitteeMemberUpdatesAdapter myCommitteeMemberAdapter = new MyCommitteeMemberUpdatesAdapter(committeeResponseTimeline.getmMemberList(), context);
                LinearLayoutManager __linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
                myCommitteeMemberHeaderViewHolder.memberRecyclerView.setLayoutManager(__linearLayoutManager);
                myCommitteeMemberHeaderViewHolder.memberRecyclerView.setAdapter(myCommitteeMemberAdapter);
            } else {
                Log.d("ADAPTER","member is visible");
                myCommitteeMemberHeaderViewHolder.memberRecyclerView.setVisibility(View.VISIBLE);
            }


            if (committeeResponseTimeline.getmInviteList() != null && committeeResponseTimeline.getmInviteList().size() > 0) {
                Log.d("ADAPTER","invitee is visible");
                myCommitteeMemberHeaderViewHolder.inviteRecyclerView.setVisibility(View.VISIBLE);
                MyCommitteeInviteAdapter myCommitteeInviteesAdapter = new MyCommitteeInviteAdapter(committeeResponseTimeline.getmInviteList());
                LinearLayoutManager _linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
                myCommitteeMemberHeaderViewHolder.inviteRecyclerView.setLayoutManager(_linearLayoutManager);
                myCommitteeMemberHeaderViewHolder.inviteRecyclerView.setAdapter(myCommitteeInviteesAdapter);
            } else {
                Log.d("ADAPTER","invitee is gone");
                myCommitteeMemberHeaderViewHolder.inviteRecyclerView.setVisibility(View.VISIBLE);
            }

            if (committeeResponseTimeline.getmTransactionList() != null && committeeResponseTimeline.getmTransactionList().size() > 0) {
                Log.d("ADAPTER","Transaction is visible");
                myCommitteeMemberHeaderViewHolder.transactionRecyclerView.setVisibility(View.VISIBLE);
                MyCommitteeTransactionsAdapter myCommitteeTransactionsAdapter = new MyCommitteeTransactionsAdapter(committeeResponseTimeline.getmTransactionList());
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
                myCommitteeMemberHeaderViewHolder.transactionRecyclerView.setLayoutManager(linearLayoutManager);
                myCommitteeMemberHeaderViewHolder.transactionRecyclerView.setAdapter(myCommitteeTransactionsAdapter);
            } else {
                Log.d("ADAPTER","Transaction is gone");
                myCommitteeMemberHeaderViewHolder.transactionRecyclerView.setVisibility(View.VISIBLE);
            }
        }
    }

    public interface MyCommitteeInviteesListener extends BaseRecyclerViewListener {
        void onClickInvitees(@NonNull MyCommitteeResponse.Committee.Invite invite);
    }

    static class MyCommitteeMemberViewHolder extends /*BaseViewHolder<CommitteeFilterResponseMemberList, MyCommitteeInviteesListener>*/ RecyclerView.ViewHolder {

        @BindView(R.id.date_txtView)
        TezTextView dateTxtView;

        public MyCommitteeMemberViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

    }

    static class MyCommitteeMemberHeaderViewHolder extends /*BaseViewHolder<String, MyCommitteeInviteesListener>*/ RecyclerView.ViewHolder {

        @BindView(R.id.date_txtView)
        private TezTextView datTxtView;

        @BindView(R.id.transactionRecyclerView)
        private RecyclerView transactionRecyclerView;

        @BindView(R.id.inviteRecyclerView)
        private RecyclerView inviteRecyclerView;

        @BindView(R.id.memberRecyclerView)
        private RecyclerView memberRecyclerView;


        public MyCommitteeMemberHeaderViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }
    }
}
