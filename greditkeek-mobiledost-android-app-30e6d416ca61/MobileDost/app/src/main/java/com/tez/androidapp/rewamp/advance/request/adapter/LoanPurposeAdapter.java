package com.tez.androidapp.rewamp.advance.request.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.request.model.LoanPurpose;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class LoanPurposeAdapter extends GenericRecyclerViewAdapter<LoanPurpose,
        LoanPurposeAdapter.LoanPurposeListener,
        LoanPurposeAdapter.LoanPurposeViewHolder> {

    private int selectedPurposeId = -1;
    private int selectedPosition = -1;

    public LoanPurposeAdapter(@NonNull List<LoanPurpose> items, @Nullable LoanPurposeListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public LoanPurposeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.grid_item_loan_purpose, parent);
        return new LoanPurposeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LoanPurposeViewHolder holder, int position) {
        holder.onBind(position, getItems(), getListener());
    }

    public int getSelectedPurposeId() {
        return selectedPurposeId;
    }

    public interface LoanPurposeListener extends BaseRecyclerViewListener {

        void onClickLoanPurpose(@NonNull LoanPurpose loanPurpose);
    }

    class LoanPurposeViewHolder extends BaseViewHolder<LoanPurpose, LoanPurposeListener> {

        @BindView(R.id.ivIcon)
        private TezImageView ivIcon;

        @BindView(R.id.tvTitle)
        private TezTextView tvTitle;

        @BindView(R.id.ivSelectedPurpose)
        private TezImageView ivSelectedPurpose;

        private LoanPurposeViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(int position, List<LoanPurpose> items, @Nullable LoanPurposeListener listener) {
            super.onBind(position, items, listener);
            LoanPurpose item = items.get(position);
            ivIcon.setImageResource(item.getIconRes());
            tvTitle.setText(item.getTitleRes());
            ivSelectedPurpose.setVisibility(selectedPurposeId == item.getId() ? View.VISIBLE : View.GONE);
            itemView.setOnClickListener(v -> {
                if (selectedPosition != position) {
                    int oldPosition = selectedPosition;
                    selectedPosition = position;
                    selectedPurposeId = item.getId();

                    notifyItemChanged(oldPosition);
                    notifyItemChanged(position);
                }
            });

            if (listener != null && selectedPosition == position)
                listener.onClickLoanPurpose(item);
        }
    }
}
