package com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Rehman Murad Ali on 8/28/2017.
 */

public class BeneficiaryDetails implements Serializable, Parcelable {

    public static final Creator<BeneficiaryDetails> CREATOR = new Creator<BeneficiaryDetails>() {
        @Override
        public BeneficiaryDetails createFromParcel(Parcel in) {
            return new BeneficiaryDetails(in);
        }

        @Override
        public BeneficiaryDetails[] newArray(int size) {
            return new BeneficiaryDetails[size];
        }
    };
    private Integer id;
    private String name;
    private String mobileNumber;
    private String relationship;
    private Integer mobileUserInsurancePolicyBeneficiaryId;
    private Integer relationshipId;
    private boolean isEditable;

    public BeneficiaryDetails() {
    }

    protected BeneficiaryDetails(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        mobileNumber = in.readString();
        relationship = in.readString();
        if (in.readByte() == 0) {
            mobileUserInsurancePolicyBeneficiaryId = null;
        } else {
            mobileUserInsurancePolicyBeneficiaryId = in.readInt();
        }
        if (in.readByte() == 0) {
            relationshipId = null;
        } else {
            relationshipId = in.readInt();
        }
        isEditable = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(name);
        dest.writeString(mobileNumber);
        dest.writeString(relationship);
        if (mobileUserInsurancePolicyBeneficiaryId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(mobileUserInsurancePolicyBeneficiaryId);
        }
        if (relationshipId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(relationshipId);
        }
        dest.writeByte((byte) (isEditable ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRelationshipId() {
        return relationshipId;
    }

    public void setRelationshipId(Integer relationshipId) {
        this.relationshipId = relationshipId;
    }

    public Integer getMobileUserInsurancePolicyBeneficiaryId() {
        return mobileUserInsurancePolicyBeneficiaryId;
    }

    public void setMobileUserInsurancePolicyBeneficiaryId(Integer mobileUserInsurancePolicyBeneficiaryId) {
        this.mobileUserInsurancePolicyBeneficiaryId = mobileUserInsurancePolicyBeneficiaryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getRelationshipFormatted() {
        String relationshipLowerCase = this.relationship.toLowerCase();
        return relationshipLowerCase.substring(0, 1).toUpperCase() + relationshipLowerCase.substring(1);
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }
}
