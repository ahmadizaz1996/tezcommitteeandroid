package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto;

public class Rule {
    private String type;
    private String questionRefName;
    private String answerRefName;
    private String nextQuestionRefName;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuestionRefName() {
        return questionRefName;
    }

    public void setQuestionRefName(String questionRefName) {
        this.questionRefName = questionRefName;
    }

    public String getAnswerRefName() {
        return answerRefName;
    }

    public void setAnswerRefName(String answerRefName) {
        this.answerRefName = answerRefName;
    }

    public String getNextQuestionRefName() {
        return nextQuestionRefName;
    }

    public void setNextQuestionRefName(String nextQuestionRefName) {
        this.nextQuestionRefName = nextQuestionRefName;
    }
}
