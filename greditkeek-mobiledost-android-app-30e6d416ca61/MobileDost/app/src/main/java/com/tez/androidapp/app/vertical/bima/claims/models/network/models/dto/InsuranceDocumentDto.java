package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto;

import java.io.Serializable;

/**
 * Created by FARHAN DHANANI on 7/5/2018.
 */
public class InsuranceDocumentDto implements Serializable {
    private int id;
    private String name;
    private String mandatoryOptional;

    public InsuranceDocumentDto(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMandatoryOptional() {
        return mandatoryOptional;
    }

    public void setMandatoryOptional(String mandatoryOptional) {
        this.mandatoryOptional = mandatoryOptional;
    }
}
