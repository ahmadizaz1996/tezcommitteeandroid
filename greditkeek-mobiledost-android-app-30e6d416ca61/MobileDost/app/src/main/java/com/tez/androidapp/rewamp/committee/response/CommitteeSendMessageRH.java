package com.tez.androidapp.rewamp.committee.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeGroupChatListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeGroupChatSendMessageListener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeSendMessageRH extends BaseRH<CommitteeSendMessageResponse> {

    private final CommitteeGroupChatSendMessageListener listener;

    public CommitteeSendMessageRH(BaseCloudDataStore baseCloudDataStore, CommitteeGroupChatSendMessageListener committeeGroupChatSendMessageListener) {
        super(baseCloudDataStore);
        this.listener = committeeGroupChatSendMessageListener;
    }

    @Override
    protected void onSuccess(Result<CommitteeSendMessageResponse> value) {
        CommitteeSendMessageResponse getGroupChatMessagesResponse = value.response().body();
        if (getGroupChatMessagesResponse != null && getGroupChatMessagesResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onSendSuccess(getGroupChatMessagesResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onSendFailure(errorCode, message);
    }
}
