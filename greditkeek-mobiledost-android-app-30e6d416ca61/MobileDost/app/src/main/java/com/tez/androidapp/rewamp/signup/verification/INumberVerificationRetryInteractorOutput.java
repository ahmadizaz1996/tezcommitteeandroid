package com.tez.androidapp.rewamp.signup.verification;

/**
 * Created by FARHAN DHANANI on 6/5/2019.
 */
public interface INumberVerificationRetryInteractorOutput extends VerificationCallback {
    void showTime(long millisUntilFinished);

    void onNumberVerifyGenerateOtpSuccess();
}
