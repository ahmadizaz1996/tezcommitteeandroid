package com.tez.androidapp.rewamp.profile.edit.presenter;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserProfileRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.rewamp.profile.edit.interactor.EditProfileVerificactionActivitInteractor;
import com.tez.androidapp.rewamp.profile.edit.interactor.IEditProfileVerificationActivityInteractor;
import com.tez.androidapp.rewamp.profile.edit.view.IEditProfileVerificationActivityView;

public class EditProfileVerificationActivityPresenter
        implements IEditProfileVerificationActivityPresenter, IEditProfileVerificationActivityInteractorOutput {

    private final IEditProfileVerificationActivityInteractor iEditProfileVerificationActivityInteractor;
    private final IEditProfileVerificationActivityView iEditProfileVerificationActivityView;

    public EditProfileVerificationActivityPresenter(final IEditProfileVerificationActivityView iEditProfileVerificationActivityView) {
        this.iEditProfileVerificationActivityView = iEditProfileVerificationActivityView;
        this.iEditProfileVerificationActivityInteractor = new EditProfileVerificactionActivitInteractor(this);
    }

    @Override
    public void validateUser(@NonNull final String mobileNumber) {
        this.iEditProfileVerificationActivityInteractor.validateInfo(mobileNumber);
    }

    @Override
    public void onValidateInfoSuccess(String mobileNumber) {
        this.iEditProfileVerificationActivityView.createSinchVerification(mobileNumber);
    }

    @Override
    public void onValidateInfoFailure(int errorCode, String message) {
        this.iEditProfileVerificationActivityView.showError(errorCode,
                (d, v) -> this.iEditProfileVerificationActivityView.finishActivity());
    }

    @Override
    public void updateUserProfile() {
        UpdateUserProfileRequest updateUserProfileRequest = new UpdateUserProfileRequest();
        updateUserProfileRequest.setMobileNumber(this.iEditProfileVerificationActivityView.getMobileNumber());
        updateUserProfileRequest.setEmail(this.iEditProfileVerificationActivityView.getEmail());
        updateUserProfileRequest.setAddress(this.iEditProfileVerificationActivityView.getCurrentAddress());
        int cityId = this.iEditProfileVerificationActivityView.getSelectedCityId();
        Optional.doWhen(cityId != -1, () -> updateUserProfileRequest.setCityId(cityId));
        this.iEditProfileVerificationActivityInteractor.updateUserProfile(updateUserProfileRequest);
    }

    @Override
    public void onUpdateUserProfileSuccess() {
        this.iEditProfileVerificationActivityView.setTezLoaderToBeDissmissedOnTransition();
        Utility.directUserToIntroScreenAtGlobalLevel();
    }

    @Override
    public void onUpdateUserProfileFailure(int errorCode, String message) {
        this.iEditProfileVerificationActivityView.dismissTezLoader();
        this.iEditProfileVerificationActivityView.showError(errorCode,
                (d, v) -> this.iEditProfileVerificationActivityView.finishActivity());
    }
}
