package com.tez.androidapp.app.general.feature.account.feature.suspension.account.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 8/22/2017.
 */

public class SuspendAccountVerifyOTPRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/user/suspend/verify/otp/{" + Params.OTP + "}";

    public static final class Params {
        public static final String OTP = "otp";

        private Params() {
        }
    }
}
