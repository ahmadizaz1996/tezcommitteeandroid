package com.tez.androidapp.repository.network.models.request;

/**
 * Created by Rehman Murad Ali on 4/19/2018.
 */
public class VerifyActiveDeviceRequest {

    public static final String METHOD_NAME = "v2/user/verify/activeDevice";

    private String imei;
    private String principalName;

    public VerifyActiveDeviceRequest(String imei, String principalName) {
        this.imei = imei;
        this.principalName = principalName;
    }

    public String getImei() {
        return imei;
    }

    public String getPrincipalName() {
        return principalName;
    }
}
