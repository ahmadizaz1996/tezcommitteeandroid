package com.tez.androidapp.rewamp.committee.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.MyCommitteeListener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class MyCommitteeRH extends BaseRH<MyCommitteeResponse> {

    private final MyCommitteeListener listener;

    public MyCommitteeRH(BaseCloudDataStore baseCloudDataStore, MyCommitteeListener myCommitteeListener) {
        super(baseCloudDataStore);
        this.listener = myCommitteeListener;
    }

    @Override
    protected void onSuccess(Result<MyCommitteeResponse> value) {
        MyCommitteeResponse myCommitteeResponse = value.response().body();
        if (myCommitteeResponse != null && myCommitteeResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onMyCommitteeSuccess(myCommitteeResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onMyCommitteeFailure(errorCode, message);
    }
}
