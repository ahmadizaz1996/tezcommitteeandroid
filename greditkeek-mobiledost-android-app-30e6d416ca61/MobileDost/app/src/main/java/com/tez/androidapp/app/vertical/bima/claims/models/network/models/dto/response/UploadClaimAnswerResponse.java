package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by FARHAN DHANANI on 6/29/2018.
 */
public class UploadClaimAnswerResponse extends BaseResponse{

    @Override
    public String toString() {
        return "UploadClaimAnswerResponse{} " + super.toString();
    }

}
