package com.tez.androidapp.rewamp.committee.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class MyCommitteeMembersAdapter extends GenericRecyclerViewAdapter<MyCommitteeResponse.Committee.Member,
        MyCommitteeMembersAdapter.MyCommitteeMemberListener,
        MyCommitteeMembersAdapter.MyCommitteeMemberViewHolder> {

    public MyCommitteeMembersAdapter(@NonNull List<MyCommitteeResponse.Committee.Member> items, @Nullable MyCommitteeMemberListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public MyCommitteeMemberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.item_committee_members, parent);
        return new MyCommitteeMemberViewHolder(view);
    }

    public interface MyCommitteeMemberListener extends BaseRecyclerViewListener {
        void onClickMembers(@NonNull MyCommitteeResponse.Committee.Member member);
    }

    static class MyCommitteeMemberViewHolder extends BaseViewHolder<MyCommitteeResponse.Committee.Member, MyCommitteeMemberListener> {

        @BindView(R.id.tvCount)
        private TezTextView tvCount;

        @BindView(R.id.tvMemberName)
        private TezTextView tvMemberName;

        @BindView(R.id.cv_img_mem)
        private CircleImageView memberImgView;

        public MyCommitteeMemberViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(MyCommitteeResponse.Committee.Member item, @Nullable MyCommitteeMemberListener listener) {
            super.onBind(item, listener);

            tvCount.setText(getAdapterPosition() + 1 + "");
            tvMemberName.setText(item.name);

        }
    }
}
