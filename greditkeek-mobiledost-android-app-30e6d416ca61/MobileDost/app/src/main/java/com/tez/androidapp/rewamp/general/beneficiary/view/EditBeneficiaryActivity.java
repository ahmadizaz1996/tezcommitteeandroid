package com.tez.androidapp.rewamp.general.beneficiary.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.BeneficiaryDetails;
import com.tez.androidapp.app.vertical.bima.shared.models.BimaRelation;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutRegex;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezCheckBox;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.EditBeneficiaryActivityPresenter;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.IEditBeneficiaryActivityPresenter;
import com.tez.androidapp.rewamp.general.beneficiary.router.BeneficiaryRelationActivityRouter;
import com.tez.androidapp.rewamp.general.beneficiary.router.EditBeneficiaryActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.validator.library.annotations.Order;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;

import java.util.ArrayList;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class EditBeneficiaryActivity extends PlaybackActivity implements IEditBeneficiaryActivityView, DoubleTapSafeOnClickListener, ValidationListener {

    private static final int REQUEST_CODE_CONTACT = 13897;

    private final IEditBeneficiaryActivityPresenter iEditBeneficiaryActivityPresenter;
    Integer relationShipId;

    @TextInputLayoutRegex(regex = Constants.REGEX_NOT_EMPTY, value = {1, R.string.string_required_feild})
    @Order(1)
    @BindView(R.id.tilBeneficiaryRelation)
    private TezTextInputLayout tilBeneficiaryRelation;

    @TextInputLayoutRegex(regex = Constants.REGEX_NOT_EMPTY, value = {1, R.string.string_required_feild})
    @Order(2)
    @BindView(R.id.tilBeneficiaryName)
    private TezTextInputLayout tilBeneficiaryName;

    @TextInputLayoutRegex(regex = Constants.PHONE_NUMBER_VALIDATOR_REGEX, value = {1, R.string.incorrect_mobile_number})
    @Order(3)
    @BindView(R.id.tilBeneficiaryNumber)
    private TezTextInputLayout tilBeneficiaryNumber;

    @BindView(R.id.etBeneficiaryRelation)
    private TezEditTextView etBeneficiaryRelation;

    @BindView(R.id.etBeneficiaryName)
    private TezEditTextView etBeneficiaryName;

    @BindView(R.id.etBeneficiaryNumber)
    private TezEditTextView etBeneficiaryNumber;

    @BindView(R.id.ivPhoneBook)
    private TezImageView ivPhoneBook;

    @BindView(R.id.btContinue)
    private TezButton btContinue;

    @BindView(R.id.cbInformBeneficiary)
    private TezCheckBox cbInformBeneficiary;

    private CompositeDisposable allDisposables;

    public EditBeneficiaryActivity() {
        iEditBeneficiaryActivityPresenter = new EditBeneficiaryActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_beneficiary);
        this.tezToolbar.setToolbarTitle(R.string.manage_beneficiary);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.allDisposables = new CompositeDisposable();
        listenChangesOnViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        allDisposables.dispose();
    }

    private void init() {
        initOnClickListeners();
        BeneficiaryDetails beneficiaryDetails = getIntent().getParcelableExtra(EditBeneficiaryActivityRouter.BENEFICIARY_DETAILS);
        Optional.ifPresent(beneficiaryDetails, this::setForEditBeneficiary, this::setForAddBeneficiary);
    }

    private void initOnClickListeners() {
        etBeneficiaryRelation.setDoubleTapSafeOnClickListener(this);
        ivPhoneBook.setDoubleTapSafeOnClickListener(this);
    }

    private void setForEditBeneficiary(@NonNull BeneficiaryDetails beneficiaryDetails) {
        relationShipId = beneficiaryDetails.getRelationshipId();
        etBeneficiaryRelation.setText(Utility.getBeneficiaryRelationName(relationShipId));
        etBeneficiaryName.setText(beneficiaryDetails.getName());
        etBeneficiaryName.setSelection(beneficiaryDetails.getName().length());
        etBeneficiaryNumber.setText(beneficiaryDetails.getMobileNumber());
        etBeneficiaryNumber.setSelection(beneficiaryDetails.getMobileNumber() != null ? beneficiaryDetails.getMobileNumber().length() : 0);
        btContinue.setDoubleTapSafeOnClickListener(view -> onClickBtContinue(beneficiaryDetails.getId(), beneficiaryDetails.getMobileUserInsurancePolicyBeneficiaryId()));
        this.setEnableViewsWithRespectToRelation(beneficiaryDetails.isEditable());
    }

    private void setEnableViewsWithRespectToRelation(boolean isEditable) {
        int etColor = Utility.getColorFromResource(R.color.editTextTextColorGrey);
        int disabledColor = Utility.getColorFromResource(R.color.disabledColor);
        int drawableColor = Utility.getColorFromResource(R.color.textViewTextColorGreen);
        tilBeneficiaryRelation.setEnabled(isEditable);
        etBeneficiaryRelation.setTextColor(isEditable ? etColor : disabledColor);
        etBeneficiaryRelation.setTextViewDrawableColor(isEditable ? drawableColor : disabledColor);
        tilBeneficiaryName.setEnabled(isEditable);
        etBeneficiaryName.setTextColor(isEditable ? etColor : disabledColor);
        etBeneficiaryRelation.setTextViewDrawableColor(isEditable ? drawableColor : disabledColor);
    }

    private void setForAddBeneficiary() {
        btContinue.setDoubleTapSafeOnClickListener(view -> onClickBtContinue(null, null));
    }

    private void listenChangesOnViews() {
        allDisposables.add(RxTextView.textChanges(this.etBeneficiaryRelation).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));

        allDisposables.add(RxTextView.textChanges(this.etBeneficiaryName).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));

        allDisposables.add(RxTextView.textChanges(this.etBeneficiaryNumber).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
    }

    private ArrayList<Integer> getOneTimeAddOnlyAddedRelationsFromIntent() {
        return getIntent().getIntegerArrayListExtra(EditBeneficiaryActivityRouter.ONE_TIME_ADD_ONLY_ADDED_RELATION_ARRAY_LIST);
    }

    @Override
    public void finishActivity() {
        setResult(RESULT_OK);
        super.finishActivity();
    }

    @Override
    public void doubleTapSafeOnClick(View view) {

        switch (view.getId()) {

            case R.id.etBeneficiaryRelation:
                onClickEtBeneficiaryRelation();
                break;

            case R.id.ivPhoneBook:
                onClickIvPhoneBook();
                break;
        }
    }

    private void onClickEtBeneficiaryRelation() {
        BeneficiaryRelationActivityRouter.createInstance().setDependenciesAndRouteForResult(this, relationShipId, getOneTimeAddOnlyAddedRelationsFromIntent());
    }

    private void onClickIvPhoneBook() {
        etBeneficiaryNumber.requestFocus();
        Utility.openContacts(this, REQUEST_CODE_CONTACT);
    }

    private void onClickBtContinue(@Nullable Integer beneficiaryId, @Nullable Integer mobileUserInsurancePolicyBeneficiaryId) {
        FieldValidator.validate(this, new ValidationListener() {
            @Override
            public void validateSuccess() {
                iEditBeneficiaryActivityPresenter.setAdvanceBimaBeneficiary(beneficiaryId,
                        mobileUserInsurancePolicyBeneficiaryId,
                        relationShipId,
                        etBeneficiaryName.getValueText(),
                        etBeneficiaryNumber.getValueText(),
                        cbInformBeneficiary.isChecked());
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                btContinue.setButtonInactive();
                filterChain.doFilter();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {

            switch (requestCode) {

                case BeneficiaryRelationActivityRouter.REQUEST_CODE_BENEFICIARY_RELATION_ACTIVITY:
                    BimaRelation bimaRelation = data.getParcelableExtra(BeneficiaryRelationActivityRouter.BIMA_RELATION);
                    Optional.ifPresent(bimaRelation, this::setBeneficiaryRelation);
                    break;

                case REQUEST_CODE_CONTACT:
                    String number = Utility.getNumberFromContacts(data, this);
                    if (number != null) {
                        number = Utility.getFormattedMobileNumber(number);
                        etBeneficiaryNumber.setText(number);
                        etBeneficiaryNumber.setSelection(Math.min(number.length(), 11));
                    }
                    break;
            }
        }
    }

    private void setBeneficiaryRelation(@NonNull BimaRelation bimaRelation) {
        etBeneficiaryRelation.setText(bimaRelation.getRelationshipNameId());
        relationShipId = bimaRelation.getRelationShipId();
    }

    @Override
    public void validateSuccess() {
        this.btContinue.setButtonNormal();
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.btContinue.setButtonInactive();
        filterChain.doFilter();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    protected String getScreenName() {
        return "EditBeneficiaryActivity";
    }
}
