package com.tez.androidapp.rewamp.signup.verification;

import androidx.annotation.NonNull;

/**
 * Created by FARHAN DHANANI on 6/5/2019.
 */
public interface INumberVerificationRetryPresenter {
    void createVerification(@NonNull String mobileNumber,
                            @NonNull String label,
                            @NonNull String principalName);
}
