package com.tez.androidapp.app.vertical.bima.claims.models;

/**
 * Created by VINOD KUMAR on 11/13/2018.
 */
public class MapTypeInsuranceAnswerResponseDto extends InsuranceAnswerResponseDto {

    public MapTypeInsuranceAnswerResponseDto(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public MapTypeInsuranceAnswerResponseDto(String response, Double latitude, Double longitude) {
        this.response = response;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public MapTypeInsuranceAnswerResponseDto(String response, String additionalInfo, Double latitude, Double longitude) {
        this.response = response;
        this.latitude = latitude;
        this.longitude = longitude;
        this.additionalInfo = additionalInfo;
    }

    @Override
    public void setAnswer(CommonAnswers insuranceAnswerDto) {
        insuranceAnswerDto.setLongitude(longitude);
        insuranceAnswerDto.setLatitude(latitude);
        insuranceAnswerDto.setResponse(response);
        insuranceAnswerDto.setAdditionalInfo(additionalInfo);
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}
