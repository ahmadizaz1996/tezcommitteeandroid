package com.tez.androidapp.app.general.feature.find.agent.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 3/7/2017.
 */

public class FindAgentsRequest extends BaseRequest{

    public static final String METHOD_NAME = "v1/common/findAgent";

    private Double latitude;
    private Double longitude;
    private Integer cityId;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return "FindAgentsRequest{" +
                "lng=" + latitude +
                ", lat=" + longitude +
                ", cityId=" + cityId +
                '}';
    }
}
