package com.tez.androidapp.rewamp.general.changepin;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public class InAppPinChangedActivityRouter extends BaseActivityRouter {


    public static InAppPinChangedActivityRouter createInstance() {
        return new InAppPinChangedActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, InAppPinChangedActivity.class);
    }
}
