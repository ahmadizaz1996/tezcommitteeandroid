package com.tez.androidapp.rewamp.committee.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Answer;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.commons.calendar.TezCalendar;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezSpinner;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.presenter.CommitteeCompletionActivityPresenter;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter;
import com.tez.androidapp.rewamp.committee.shared.api.client.CommitteeAPI;
import com.tez.androidapp.rewamp.util.RetrofitClient;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.opencensus.internal.StringUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.HEAD;

import static com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter.COMMITTEE_DATA;

public class CommitteeCompletionActivity extends BaseActivity implements IBaseView, DoubleTapSafeOnClickListener, ICommitteeCompletionActivityView {

    @BindView(R.id.committeeTitle)
    TezEditTextView committeeTitle;

    @BindView(R.id.startingDate)
    TezSpinner startingDate;

    @BindView(R.id.committeeMethod)
    TezEditTextView committeeMethod;

    @BindView(R.id.btContinue)
    TezButton btContinue;

    private final CommitteeCompletionActivityPresenter committeeCompletionPresenter;
    private CommitteePackageModel committeePackage;
    private CommitteeInviteRequest mCommitteeInviteRequest;
    private String startingDateValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee_completed_);
        ViewBinder.bind(this);
        fetchExtras();
        spinneritem();
        initClickListener();
    }

    public CommitteeCompletionActivity() {
        committeeCompletionPresenter = new CommitteeCompletionActivityPresenter(this);
    }

    private void fetchExtras() {
        Intent intent = getIntent();
        if (intent != null
                && intent.getExtras() != null
                && intent.getExtras().containsKey(COMMITTEE_DATA)) {
            committeePackage = (CommitteePackageModel) intent.getExtras().getSerializable(COMMITTEE_DATA);
            mCommitteeInviteRequest = intent.getExtras().getParcelable(CommitteeInviteContactsActivity.INVITEES_LIST);
        }
    }

    private void initClickListener() {
        committeeTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!startingDate.getValueText().equals("")) {
                    if (charSequence.toString().equals("")) {
                        btContinue.setButtonInactive();
                        btContinue.setDoubleTapSafeOnClickListener(null);
                    } else {
                        btContinue.setButtonNormal();
                        btContinue.setDoubleTapSafeOnClickListener(CommitteeCompletionActivity.this);

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
//        startingDate.setDoubleTapSafeOnClickListener(this);
//        this.startingDate.setOnClickListener(v -> TezCalendar.showCalendarForExpiryDate(this, startingDate));
    }

    @Override
    protected String getScreenName() {
        return CommitteeCompletionActivity.class.getSimpleName();
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {
            case R.id.btContinue:
//                onContinueClick();
                onCreateCommitteeClick();
                break;
        }
    }

    private void onCreateCommitteeClick() {

        CommitteeCreateRequest committeeCreateRequest = new CommitteeCreateRequest(
                /*committeePackage.getCommitteeName()*/committeeTitle.getText().toString(),
                committeePackage.getDurationInMonths(),
                committeePackage.getSelectedMembers(),
                committeePackage.getId(),
                /*committeePackage.getStartDate()*/ /*startingDate.getValueText()*/startingDateValue,
                committeePackage.getSelectedAmount()
        );
        committeeCompletionPresenter.createCommittee(committeeCreateRequest);
        /*CommitteeAPI committeeAPI = RetrofitClient.createAPI();
        Call<CommitteeCreateResponse> responseCall = committeeAPI.createCommittee("Bearer " + MDPreferenceManager.getAccessToken().getAccessToken(),
                committeeCreateRequest);
        responseCall.enqueue(new Callback<CommitteeCreateResponse>() {
            @Override
            public void onResponse(Call<CommitteeCreateResponse> call, Response<CommitteeCreateResponse> response) {
                Log.d("RESPONSE", response.toString());
                if (response.body().getStatusCode() == 0)
                    CommitteeSummaryActivityRouter.createInstance().setDependenciesAndRoute(CommitteeCompletionActivity.this, response.body(), mCommitteeInviteRequest);
                else
                    Toast.makeText(CommitteeCompletionActivity.this, response.body().message, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<CommitteeCreateResponse> call, Throwable t) {
                Log.d("RESPONSE", t.toString());
                Toast.makeText(CommitteeCompletionActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    private void onContinueClick() {
        if (!committeeTitle.getValueText().equalsIgnoreCase("")) {
            committeePackage.setCommitteeName(committeeTitle.getValueText());
            committeePackage.setCommitteeMethod(committeeMethod.getValueText());
            committeePackage.setStartDate(startingDate.getValueText());
            CommitteeSummaryActivityRouter.createInstance().setDependenciesAndRoute(this, committeePackage);
        } else Toast.makeText(this, "Please write Committee Name", Toast.LENGTH_SHORT).show();
    }

    private void spinneritem() {
        ArrayList<String> dates = currentDate();
        this.startingDate.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, dates));

        startingDate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String[] dateArray = dates.get(i).split("-");
                startingDateValue = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];
                if (committeeTitle.getText().equals("")) {
                    btContinue.setButtonInactive();
                    btContinue.setDoubleTapSafeOnClickListener(null);
                } else {
                    btContinue.setButtonNormal();
                    btContinue.setDoubleTapSafeOnClickListener(CommitteeCompletionActivity.this);
                }
            }
        });


    }

    private ArrayList<String> currentDate() {
        ArrayList<String> spinner = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        /*Date date = new Date();
        String date2 = formatter.format(date);
        String[] dates = date2.split("-");
        int day = Integer.parseInt(dates[0]);
        int month = Integer.parseInt(dates[1]);
        int year = Integer.parseInt(dates[2]);
        int m = month + 1;
        int y = year + 1;
        if (day < 15 && month < 12) {
            spinner.add("15-" + month + "-" + year);
            spinner.add("1-" + m + "-" + year);
        } else if (day > 15 && month < 12) {
            spinner.add("1-" + m + "-" + year);
            spinner.add("15-" + m + "-" + year);
        } else if (day < 15 && month == 12) {
            spinner.add("15-" + month + "-" + year);
            spinner.add("1-" + "1-" + "-" + y);
        } else if (day > 15 && month == 12) {
            spinner.add("1-" + month + "-" + y);
            spinner.add("15-" + month + "-" + y);*/
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, +5);
        Date todate1 = cal.getTime();
        String fromdate = formatter.format(todate1);
        String[] dates = fromdate.split("-");
        int day = Integer.parseInt(dates[2]);
        int month = Integer.parseInt(dates[1]);
        int year = Integer.parseInt(dates[0]);
        int m = month + 1;
        int y = year + 1;
        if (day < 15 && month < 12) {
            spinner.add("16-" + month + "-" + year);
            spinner.add("01-" + m + "-" + year);
        } else if (day > 15 && month < 12) {
            spinner.add("01-" + m + "-" + year);
            spinner.add("16-" + m + "-" + year);
        } else if (day < 15 && month == 12) {
            spinner.add("16-" + month + "-" + year);
            spinner.add("01-" + "1" + "-" + y);
        } else if (day > 15 && month == 12) {
            spinner.add("01-" + month + "-" + y);
            spinner.add("16-" + month + "-" + y);
        }
        return spinner;
        // init();
    }

    @Override
    public void showLoader() {
        showTezLoader();
    }

    @Override
    public void hideLoader() {
        dismissTezLoader();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onCommiteeCreation(CommitteeCreateResponse committeeCreateResponse) {
        try {
//            MDPreferenceManager.setCommitteeCreationData(committeeCreateResponse);
            CommitteeSummaryActivityRouter.createInstance().setDependenciesAndRoute(this, committeeCreateResponse, mCommitteeInviteRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}