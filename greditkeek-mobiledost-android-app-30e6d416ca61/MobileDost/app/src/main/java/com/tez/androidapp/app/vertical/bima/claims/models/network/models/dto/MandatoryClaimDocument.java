package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto;

import android.net.Uri;

import com.tez.androidapp.commons.utils.app.Constants;

import java.util.List;

/**
 * Created by FARHAN DHANANI on 6/14/2018.
 */
public class MandatoryClaimDocument {

    private int id;
    private String name;
    private String status;
    private String mandatoryOptional;
    private Integer fileLoadedStatus = Constants.DOCUMENT_UPLOADED_NEUTRAL_STATUS;
    private boolean readOnly = false;
    private String documentModifiedDate;
    private String comment;
    private String commentDate;
    private List<DocumentFile> documentFiles;

    public List<DocumentFile> getDocumentFiles() {
        return documentFiles;
    }

    public void setDocumentFiles(List<DocumentFile> documentFiles) {
        this.documentFiles = documentFiles;
    }

    public void setDocumentFiles(List<DocumentFile> documentFiles, List<Uri> uriList) {
        this.documentFiles = documentFiles;
        this.setUriOfDocumentList(uriList);
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public String getDocumentModifiedDate() {
        return documentModifiedDate;
    }

    public void setDocumentModifiedDate(String documentModifiedDate) {
        this.documentModifiedDate = documentModifiedDate;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFileLoadedStatus() {
        return fileLoadedStatus;
    }

    public void setFileLoadedStatus(Integer fileLoadedStatus) {
        this.fileLoadedStatus = fileLoadedStatus;
    }

    public String getMandatoryOptional() {
        return mandatoryOptional;
    }

    public void setMandatoryOptional(String mandatoryOptional) {
        this.mandatoryOptional = mandatoryOptional;
    }

    public boolean isRejected() {
        return status != null && status.equalsIgnoreCase(Constants.STRING_DOCUMENT_STATUS_REJECTED);
    }

    public boolean isApproved() {
        return status != null && status.equalsIgnoreCase(Constants.STRING_DOCUMENT_STATUS_APPROVED);
    }

    public boolean isSkipped() {
        return status != null && status.equalsIgnoreCase(Constants.STRING_DOCUMENT_STATUS_SKIPPED);
    }

    public boolean isMandatory() {
        return mandatoryOptional != null && mandatoryOptional.equalsIgnoreCase("MANDATORY");
    }

    public boolean isDocEditable() {
        return !isApproved()
                && !isSkipped()
                && isRejected()
                && getFileLoadedStatus() == Constants.DOCUMENT_UPLOADED_SCCUESSFULLY_STATUS;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    private void setUriOfDocumentList(List<Uri> uriList) {
        if (uriList.size() == getDocumentFiles().size()) {
            int size = uriList.size();
            for (int i = 0; i < size; i++)
                getDocumentFiles().get(i).setDocUri(uriList.get(i));
        }
    }
}
