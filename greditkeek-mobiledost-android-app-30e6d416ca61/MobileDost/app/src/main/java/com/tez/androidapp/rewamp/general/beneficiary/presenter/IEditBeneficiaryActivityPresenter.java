package com.tez.androidapp.rewamp.general.beneficiary.presenter;

import androidx.annotation.Nullable;

public interface IEditBeneficiaryActivityPresenter {

    void setAdvanceBimaBeneficiary(@Nullable Integer beneficiaryId,
                                   @Nullable Integer mobileUserInsurancePolicyBeneficiaryId,
                                   Integer relationShipId,
                                   String name,
                                   String mobileNumber,
                                   boolean informCheck);
}
