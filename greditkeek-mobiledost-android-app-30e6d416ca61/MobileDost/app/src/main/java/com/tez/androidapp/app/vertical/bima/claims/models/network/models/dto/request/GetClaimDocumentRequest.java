package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request;

/**
 * Created by FARHAN DHANANI on 7/2/2018.
 */
public class GetClaimDocumentRequest {
    public static final String METHOD_NAME = "v1/insurance/claim/document/";
}
