package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;
import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

/**
 * Created by VINOD KUMAR on 8/1/2019.
 */
public abstract class NumberVerificationActivityRouter extends BaseActivityRouter {
    public static final String MOBILE_NUMBER = "MOBILE_NUMBER";
    public static final String PRINCIPLE = "GOOGLE";

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @NonNull String mobileNumber) {

        Intent intent = createIntent(from);
        intent.putExtra(MOBILE_NUMBER, mobileNumber);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @NonNull String mobileNumber,
                                        @NonNull String principalName) {

        Intent intent = createIntent(from);
        intent.putExtra(MOBILE_NUMBER, mobileNumber);
        intent.putExtra(PRINCIPLE, principalName);
        route(from, intent);
    }


    @NonNull
    protected abstract Intent createIntent(@NonNull BaseActivity from);
}
