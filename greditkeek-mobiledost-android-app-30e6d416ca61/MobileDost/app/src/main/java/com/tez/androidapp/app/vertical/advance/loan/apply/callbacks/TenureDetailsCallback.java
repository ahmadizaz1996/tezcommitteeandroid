package com.tez.androidapp.app.vertical.advance.loan.apply.callbacks;

import com.tez.androidapp.app.vertical.advance.loan.apply.models.network.TenureDetail;

import java.util.List;

public interface TenureDetailsCallback {

    void onGetTenuresDetailsSuccess(List<TenureDetail> tenureDetailList);
    void onGetTenuresDetailsFailure(int statusCode, String errorDescription);
}
