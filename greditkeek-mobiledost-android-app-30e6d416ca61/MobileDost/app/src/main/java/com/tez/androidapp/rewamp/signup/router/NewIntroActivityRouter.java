package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.NewIntroActivity;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class NewIntroActivityRouter extends BaseActivityRouter {

    public static NewIntroActivityRouter createInstance() {
        return new NewIntroActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, int flags) {
        Intent intent = createIntent(from);
        intent.addFlags(flags);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, NewIntroActivity.class);
    }
}
