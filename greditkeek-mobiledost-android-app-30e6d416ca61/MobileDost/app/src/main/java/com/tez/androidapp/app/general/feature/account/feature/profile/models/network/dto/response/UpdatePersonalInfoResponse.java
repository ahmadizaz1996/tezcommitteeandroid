package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response;

import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;

public class UpdatePersonalInfoResponse extends DashboardCardsResponse {

    private boolean profileCompleted;

    public boolean isProfileCompleted() {
        return profileCompleted;
    }

    public void setProfileCompleted(boolean profileCompleted) {
        this.profileCompleted = profileCompleted;
    }
}
