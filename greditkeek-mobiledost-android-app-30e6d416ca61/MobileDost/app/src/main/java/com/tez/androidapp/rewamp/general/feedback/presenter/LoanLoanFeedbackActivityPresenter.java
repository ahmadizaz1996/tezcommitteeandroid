package com.tez.androidapp.rewamp.general.feedback.presenter;

import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;
import com.tez.androidapp.rewamp.general.feedback.interactor.ILoanFeedbackActivityInteractor;
import com.tez.androidapp.rewamp.general.feedback.interactor.ILoanFeedbackActivityInteractorOutput;
import com.tez.androidapp.rewamp.general.feedback.interactor.LoanFeedbackActivityInteractor;
import com.tez.androidapp.rewamp.general.feedback.request.LoanFeedbackRequest;
import com.tez.androidapp.rewamp.general.feedback.view.ILoanFeedbackActivityView;

import net.tez.fragment.util.optional.TextUtil;

import javax.annotation.Nullable;

public class LoanLoanFeedbackActivityPresenter implements ILoanFeedbackActivityPresenter, ILoanFeedbackActivityInteractorOutput {

    private final ILoanFeedbackActivityView iLoanFeedbackActivityView;
    private final ILoanFeedbackActivityInteractor iLoanFeedbackActivityInteractor;

    public LoanLoanFeedbackActivityPresenter(ILoanFeedbackActivityView iLoanFeedbackActivityView) {
        this.iLoanFeedbackActivityView = iLoanFeedbackActivityView;
        this.iLoanFeedbackActivityInteractor = new LoanFeedbackActivityInteractor(this);
    }

    @Override
    public void submitFeedback(int loanId, int rating, @Nullable Integer cause, @Nullable String comment, boolean fullRating) {
        iLoanFeedbackActivityView.showTezLoader();
        iLoanFeedbackActivityInteractor.submitFeedback(new LoanFeedbackRequest(loanId, rating, fullRating ? null : cause, TextUtil.isEmpty(comment) || fullRating ? null : comment));
    }

    @Override
    public void onSubmitFeedbackSuccess(DashboardCardsResponse dashboardCardsResponse) {
        iLoanFeedbackActivityView.finishActivity();
    }

    @Override
    public void onSubmitFeedbackFailure(int errorCode, String message) {
        iLoanFeedbackActivityView.dismissTezLoader();
        iLoanFeedbackActivityView.showError(errorCode);
    }
}
