package com.tez.androidapp.rewamp.committee.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tez.androidapp.R

class CommitteeLeaveReasonActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_committee_leave_reason)
    }
}