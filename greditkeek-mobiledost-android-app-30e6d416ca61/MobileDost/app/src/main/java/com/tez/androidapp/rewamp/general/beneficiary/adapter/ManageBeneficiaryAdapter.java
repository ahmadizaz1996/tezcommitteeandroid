package com.tez.androidapp.rewamp.general.beneficiary.adapter;

import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezFrameLayout;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

/**
 * Created by VINOD KUMAR on 8/30/2019.
 */
public class ManageBeneficiaryAdapter extends GenericRecyclerViewAdapter<Policy,
        ManageBeneficiaryAdapter.ManageBeneficiaryListener,
        ManageBeneficiaryAdapter.ManageBeneficiaryViewHolder> {

    public ManageBeneficiaryAdapter(@NonNull List<Policy> items, @Nullable ManageBeneficiaryListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public ManageBeneficiaryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.list_item_manage_beneficiary, parent);
        return new ManageBeneficiaryViewHolder(view);
    }

    public interface ManageBeneficiaryListener extends BaseRecyclerViewListener {

        void onPolicyClick(@NonNull Policy policy);
    }

    static class ManageBeneficiaryViewHolder extends BaseViewHolder<Policy, ManageBeneficiaryListener> {

        @BindView(R.id.ivPolicyIcon)
        private TezImageView ivPolicyIcon;

        @BindView(R.id.tvPolicyName)
        private TezTextView tvPolicyName;

        @BindView(R.id.tvMessage)
        private TezTextView tvMessage;

        @BindView(R.id.tvPolicyStatus)
        private TezTextView tvPolicyStatus;

        @BindView(R.id.tvPolicyNumber)
        private TezTextView tvPolicyNumber;

        @BindView(R.id.tvPolicyCoverage)
        private TezTextView tvPolicyCoverage;

        @BindView(R.id.tvPolicyExpireDays)
        private TezTextView tvPolicyExpireDays;

        @BindView(R.id.flPolicyStatus)
        private TezFrameLayout flPolicyStatus;


        private ManageBeneficiaryViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(Policy item, @Nullable ManageBeneficiaryListener listener) {
            super.onBind(item, listener);
            ivPolicyIcon.setImageResource(item.isHealthInsurance() ? R.drawable.ic_health_insurance : R.drawable.ic_life_insurance);
            tvMessage.setText(item.isHealthInsurance() ? R.string.insure_your_health : R.string.insure_your_life);
            tvPolicyName.setText(item.getPolicyName());
            tvPolicyStatus.setText(item.getInsuranceStatus());
            tvPolicyNumber.setText(item.getPolicyNumber());
            tvPolicyCoverage.setText(itemView.getContext().getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(item.getRemainingAmount())));
            String remainingDays = item.getRemainingDays() + " " + itemView.getContext().getString(R.string.days);
            tvPolicyExpireDays.setText(remainingDays);
            this.setItemViewOnClickListener(view -> {
                if (listener != null)
                    listener.onPolicyClick(item);
            });

            this.setFlPolicyStatusBackground(item.getInsuranceStatusColor());
        }

        private void setFlPolicyStatusBackground(int colorRes) {
            int bgColor = Utility.getColorFromResource(colorRes);
            GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                    new int[]{bgColor, bgColor});
            gd.setCornerRadius(Utility.dpToPx(itemView.getContext(), 4));
            flPolicyStatus.setBackground(gd);
        }
    }
}
