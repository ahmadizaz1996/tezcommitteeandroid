package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

public interface AccountLinkCallback {
    void accountLinkSuccess();
    void accountLinkFailure(int errorCode, String message);
}
