package com.tez.androidapp.rewamp.bima.products.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.rewamp.bima.products.callback.ProductPolicyCallback;
import com.tez.androidapp.rewamp.bima.products.response.ProductPolicyResponse;

public class ProductPolicyActivityInteractor implements IProductPolicyActivityInteractor {

    private final IProductPolicyActivityInteractorOutput iProductPolicyActivityInteractorOutput;

    public ProductPolicyActivityInteractor(IProductPolicyActivityInteractorOutput iProductPolicyActivityInteractorOutput) {
        this.iProductPolicyActivityInteractorOutput = iProductPolicyActivityInteractorOutput;
    }

    @Override
    public void getProductPolicy(int policyId) {
        BimaCloudDataStore.getInstance().getProductPolicy(policyId, new ProductPolicyCallback() {
            @Override
            public void onGetProductPolicySuccess(ProductPolicyResponse productPolicyResponse) {
                iProductPolicyActivityInteractorOutput.onGetProductPolicySuccess(productPolicyResponse);
            }

            @Override
            public void onGetProductPolicyFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getProductPolicy(policyId);
                else
                    iProductPolicyActivityInteractorOutput.onGetProductPolicyFailure(errorCode, message);
            }
        });
    }
}
