package com.tez.androidapp.rewamp.advance.repay.interactor;

import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.RepayReceiptCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.RepayReceiptRequest;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.advance.repay.presenter.IRepayLoanActivityInteractorOutput;

public class RepayLoanActivityInteractor implements IRepayLoanActivityInteractor {

    private final IRepayLoanActivityInteractorOutput iRepayLoanActivityInteractorOutput;

    public RepayLoanActivityInteractor(IRepayLoanActivityInteractorOutput iRepayLoanActivityInteractorOutput) {
        this.iRepayLoanActivityInteractorOutput = iRepayLoanActivityInteractorOutput;
    }

    @Override
    public void getRepayReceipt(int loanId) {
        RepayReceiptRequest repayReceiptRequest = new RepayReceiptRequest(loanId);
        LoanCloudDataStore.getInstance().getRepayReceipt(repayReceiptRequest, new RepayReceiptCallback() {
            @Override
            public void onRepayReceiptSuccess(LoanDetails loanDetails) {
                iRepayLoanActivityInteractorOutput.onRepayReceiptSuccess(loanDetails);
            }

            @Override
            public void onRepayReceiptFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getRepayReceipt(loanId);
                else
                    iRepayLoanActivityInteractorOutput.onRepayReceiptFailure(errorCode, message);
            }
        });
    }
}
