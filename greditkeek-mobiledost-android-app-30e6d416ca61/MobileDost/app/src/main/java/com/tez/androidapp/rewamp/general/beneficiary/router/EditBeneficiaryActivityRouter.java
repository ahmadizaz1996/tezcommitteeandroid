package com.tez.androidapp.rewamp.general.beneficiary.router;

import android.content.Intent;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.BeneficiaryDetails;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.beneficiary.view.EditBeneficiaryActivity;

import java.util.ArrayList;

public class EditBeneficiaryActivityRouter extends BaseActivityRouter {

    public static final String BENEFICIARY_DETAILS = "BENEFICIARY_DETAILS";
    public static final int REQUEST_CODE_EDIT_BENEFICIARY = 1391;
    public static final String ONE_TIME_ADD_ONLY_ADDED_RELATION_ARRAY_LIST = "ONE_TIME_ADD_ONLY_ADDED_RELATION_ARRAY_LIST";

    public static EditBeneficiaryActivityRouter createInstance() {
        return new EditBeneficiaryActivityRouter();
    }

    public void setDependenciesAndRouteForResult(@NonNull BaseActivity from,
                                                 @NonNull ArrayList<Integer> oneTimeRelationList) {
        Intent intent = createIntent(from);
        intent.putExtra(ONE_TIME_ADD_ONLY_ADDED_RELATION_ARRAY_LIST, oneTimeRelationList);
        routeForResult(from, intent, REQUEST_CODE_EDIT_BENEFICIARY);
    }

    public void setDependenciesAndRouteForResult(@NonNull BaseActivity from,
                                                 @NonNull ArrayList<Integer> oneTimeRelationList,
                                                 @NonNull BeneficiaryDetails beneficiaryDetails) {
        Intent intent = createIntent(from);
        intent.putExtra(ONE_TIME_ADD_ONLY_ADDED_RELATION_ARRAY_LIST, oneTimeRelationList);
        intent.putExtra(BENEFICIARY_DETAILS, (Parcelable) beneficiaryDetails);
        routeForResult(from, intent, REQUEST_CODE_EDIT_BENEFICIARY);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, EditBeneficiaryActivity.class);
    }
}
