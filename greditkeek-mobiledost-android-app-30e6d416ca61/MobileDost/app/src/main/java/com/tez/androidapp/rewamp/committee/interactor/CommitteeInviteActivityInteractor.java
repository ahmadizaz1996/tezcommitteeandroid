package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeInviteListener;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeInviteActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;

import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeInviteActivityInteractor implements ICommitteeInviteInteractor {


    private final ICommitteeInviteActivityInteractorOutput iCommitteeInviteActivityInteractorOutput;

    public CommitteeInviteActivityInteractor(ICommitteeInviteActivityInteractorOutput committeeInviteActivityInteractorOutput) {
        this.iCommitteeInviteActivityInteractorOutput = committeeInviteActivityInteractorOutput;
    }

    @Override
    public void getInvite(List<CommitteeInviteRequest> committeeInviteRequest) {
        CommitteeAuthCloudDataStore.getInstance().getInvite(committeeInviteRequest, new CommitteeInviteListener() {
            @Override
            public void onInviteSuccess(CommitteeInviteResponse committeeInviteResponse) {
                iCommitteeInviteActivityInteractorOutput.onInviteSuccess(committeeInviteResponse);
            }

            @Override
            public void onInviteFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getInvite(committeeInviteRequest);
                else
                    iCommitteeInviteActivityInteractorOutput.onInviteFailure(errorCode, message);
            }
        });


    }
}
