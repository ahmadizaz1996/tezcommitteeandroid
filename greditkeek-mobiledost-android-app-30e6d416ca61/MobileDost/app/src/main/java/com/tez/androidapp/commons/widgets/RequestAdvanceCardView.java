package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

/**
 * Created by Rehman Murad Ali
 **/
public class RequestAdvanceCardView extends TezCardView implements View.OnClickListener {

    private static final int MIN_LOAN_AMOUNT = 1000;
    private static final double STEP_SIZE = 500;

    @BindView(R.id.tvAmount)
    private TezTextView tvAmount;

    @BindView(R.id.layoutDecrease)
    private TezLinearLayout layoutDecrease;

    @BindView(R.id.layoutIncrease)
    private TezLinearLayout layoutIncrease;

    @BindView(R.id.ivDecrease)
    private TezImageView ivDecrease;

    @BindView(R.id.ivIncrease)
    private TezImageView ivIncrease;

    private AmountChangeListener amountChangeListener;

    private double limitAmount;

    private double loanAmount = 1000;

    public RequestAdvanceCardView(@NonNull Context context) {
        super(context);
    }

    public RequestAdvanceCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public RequestAdvanceCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);

    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        inflate(context, R.layout.card_view_advance_request, this);
        init();
    }


    private void init() {
        ViewBinder.bind(this, this);
        this.tvAmount.setText(getContext().getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(1000)));
        this.layoutDecrease.setOnClickListener(this);
        this.layoutIncrease.setOnClickListener(this);
    }

    public double getAmount() {
        return loanAmount;
    }

    public void setRequestAdvanceTextChangeListener(AmountChangeListener listener) {
        this.amountChangeListener = listener;
    }

    public void setLimitAmount(double limitAmount) {
        setLimitAmount(R.string.get_loan_up_to, limitAmount);
    }

    public void setLimitAmount(@StringRes int stringId, double limitAmount){
        this.limitAmount = limitAmount;
        String limit = getContext().getResources().getString(stringId, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(limitAmount));
        TezTextView tvMaxLimit = findViewById(R.id.tvAdvanceLimit);
        tvMaxLimit.setText(Html.fromHtml(limit));
        if (limitAmount == 1000) {
            this.setIvIncreaseEnabled(false);
            this.setIvDecreaseEnabled(false);
        }
    }

    public void setLimitAmount(String string, double limitAmount){
        this.limitAmount = limitAmount;
        String limit = string +"Rs. " +limitAmount;
        Spannable spannable = new SpannableString(limit);
        spannable.setSpan(new ForegroundColorSpan(Color.GREEN), string.length(), limit.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        TezTextView tvMaxLimit = findViewById(R.id.tvAdvanceLimit);
        tvMaxLimit.setText(spannable, TextView.BufferType.SPANNABLE);
        if (limitAmount == 1000) {
            this.setIvIncreaseEnabled(false);
            this.setIvDecreaseEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.layoutDecrease:
                if (loanAmount - STEP_SIZE >= MIN_LOAN_AMOUNT)
                    updateLoanAmount(loanAmount - STEP_SIZE);
                break;

            case R.id.layoutIncrease:
                if (loanAmount + STEP_SIZE <= limitAmount)
                    updateLoanAmount(loanAmount + STEP_SIZE);
                break;
        }
    }

    private void updateLoanAmount(double amount) {
        this.loanAmount = amount;
        tvAmount.setText(getContext().getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(amount)));
        amountChangeListener.onAmountChange(amount);
        this.setIvIncreaseEnabled(amount + STEP_SIZE <= limitAmount);
        this.setIvDecreaseEnabled(amount - STEP_SIZE >= MIN_LOAN_AMOUNT);
    }

    private void setIvIncreaseEnabled(boolean enabled) {
        this.ivIncrease.setImageResource(enabled ? R.drawable.ic_increase : R.drawable.ic_increase_disabled);
    }

    private void setIvDecreaseEnabled(boolean enabled) {
        this.ivDecrease.setImageResource(enabled ? R.drawable.ic_decrease : R.drawable.ic_decrease_disabled);
    }

    @FunctionalInterface
    public interface AmountChangeListener {
        void onAmountChange(double amount);
    }
}
