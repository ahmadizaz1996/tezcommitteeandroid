package com.tez.androidapp.rewamp.general.wallet.router;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.VerifyWalletOtpActivity;

public class VerifyWalletOtpActivityRouter extends BaseActivityRouter {

    public static final String SERVICE_PROVIDER_ID = "SERVICE_PROVIDER_ID";
    public static final String MOBILE_ACCOUNT_NUMBER = "MOBILE_ACCOUNT_NUMBER";
    public static final String PIN = "PIN";
    public static final String IS_DEFAULT = "IS_DEFAULT";

    public static VerifyWalletOtpActivityRouter createInstance() {
        return new VerifyWalletOtpActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        int serviceProviderId,
                                        @NonNull String mobileAccountNumber,
                                        @Nullable String pin,
                                        boolean isDefault) {
        Intent intent = createIntent(from);
        intent.putExtra(SERVICE_PROVIDER_ID, serviceProviderId);
        intent.putExtra(MOBILE_ACCOUNT_NUMBER, mobileAccountNumber);
        intent.putExtra(PIN, pin);
        intent.putExtra(IS_DEFAULT, isDefault);
        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, VerifyWalletOtpActivity.class);
    }
}
