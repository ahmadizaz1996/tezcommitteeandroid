package com.tez.androidapp.app.vertical.bima.claims.callback;

/**
 * Created by FARHAN DHANANI on 6/29/2018.
 */
public interface UploadClaimAnswerCallback {
    void onUploadClaimAnswerSuccess();

    void onUploadClaimAnswerFailure(int statusCode, String message);
}
