package com.tez.androidapp.commons.utils.authorized.alert.image.popup;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.commons.managers.MDPreferenceManager;

/**
 * Created by FARHAN DHANANI on 7/8/2018.
 */
public interface RenderAlertImageOnUI {
    static void renderImageUIAlert(@NonNull UIForAlertImage uiForAlertImage,
                                   @NonNull ImageLoader imageLoader,
                                   @NonNull Uri url) {
        imageLoader.setUI(uiForAlertImage);
        imageLoader.setUrlToLoad(url);
        imageLoader.loadImage();
    }

    static void renderImageUIAlert(@NonNull UIForAlertImage uiForAlertImage,
                                   @NonNull ImageLoader imageLoader,
                                   @NonNull Uri url,
                                   @Nullable String signature) {
        imageLoader.setUI(uiForAlertImage);
        imageLoader.setUrlToLoad(url);
        imageLoader.setSignatureToFile(signature);
        imageLoader.loadImage();
    }

    static void renderDefaultImageUIAlert(
            @NonNull UIForAlertImage uiForAlertImage,
            @NonNull Uri url,
            @Nullable String status) {
        ImageLoader imageLoader;
        if (url.toString().contains(MDPreferenceManager.getBaseURL())) {
            imageLoader = new DefaultAuthorizeImageLoader();
        } else {
            imageLoader = new DefaultLocalCacheImageLoader();
        }
        try {
            renderImageUIAlert(uiForAlertImage, imageLoader, url, status);
        } catch (OutOfMemoryError error) {
            Crashlytics.logException(error);
        }
    }
}
