package com.tez.androidapp.rewamp.signup;

import android.os.Bundle;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.signup.router.NewLoginActivityRouter;

import net.tez.viewbinder.library.core.OnClick;

public class PinChangedActivity extends PlaybackActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_changed);
        this.updateToolbar(findViewById(R.id.pinChangedToolbar));
    }

    @OnClick(R.id.btOkay)
    protected void onClickBtOkay() {
        Utility.directUserToIntroScreenAtGlobalLevel();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    protected String getScreenName() {
        return "PinChangedActivity";
    }

    @Override
    public void onBackPressed() {
    }
}
