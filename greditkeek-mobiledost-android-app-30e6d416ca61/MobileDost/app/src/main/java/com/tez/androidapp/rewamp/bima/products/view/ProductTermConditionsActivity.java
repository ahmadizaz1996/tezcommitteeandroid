package com.tez.androidapp.rewamp.bima.products.view;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.bima.products.presenter.IProductTermConditionsActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.presenter.ProductTermConditionsActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.router.ProductTermConditionsActivityRouter;
import com.tez.androidapp.rewamp.common.BaseTermsAndConditionActivity;
import com.tez.androidapp.rewamp.general.termsandcond.DetailedTermConditionDialog;
import com.tez.androidapp.rewamp.signup.TermsAndCondition;
import com.tez.androidapp.rewamp.signup.TermsAndConditionAdapter;

import java.util.List;

public class ProductTermConditionsActivity extends BaseTermsAndConditionActivity implements IProductTermConditionsActivityView {

    protected final IProductTermConditionsActivityPresenter presenter;

    public ProductTermConditionsActivity() {
        presenter = new ProductTermConditionsActivityPresenter(this);
    }

    @Override
    protected void init() {
        this.btMainButton.setVisibility(View.GONE);
        this.tezCheckBox.setVisibility(View.GONE);
        this.textViewDescription.setVisibility(View.GONE);
        this.tvDetailedTC.setVisibility(View.VISIBLE);
        int productId = ProductTermConditionsActivityRouter.getDependencies(getIntent()).getProductId();
        presenter.getProductTermConditions(productId);
        this.tvDetailedTC.setDoubleTapSafeOnClickListener(v -> presenter.getProductDetailedTermCondition(productId));
    }

    @Override
    protected String getScreenName() {
        return "PolicyTermsAndConditionsActivity";
    }

    @Override
    public void setAdapter(@NonNull List<TermsAndCondition> termsAndConditionList) {
        TermsAndConditionAdapter termsAndConditionsAdaptor = new TermsAndConditionAdapter(termsAndConditionList);
        this.recyclerViewTermsAndCondition.setAdapter(termsAndConditionsAdaptor);
    }

    @Override
    public void setTermCondition(String termCondition) {
        DetailedTermConditionDialog termDetailDialog = new DetailedTermConditionDialog(this);
        termDetailDialog.setTerms(termCondition);
        termDetailDialog.show();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    protected List<TermsAndCondition> getTermAndConditions() {
        return null;
    }

    @Override
    protected void onClickBtMainButton() {

    }

    @Override
    public int getAudioId() {
        return 0;
    }
}
