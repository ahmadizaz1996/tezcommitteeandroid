package com.tez.androidapp.rewamp.signup.interactor;

import androidx.annotation.NonNull;

/**
 * Created by VINOD KUMAR on 8/9/2019.
 */
public interface IChangeLanguageActivityInteractor {

    void sendUpdatedLanguageCodeToServer(@NonNull final String languageCode, final  @NonNull String lang);
}
