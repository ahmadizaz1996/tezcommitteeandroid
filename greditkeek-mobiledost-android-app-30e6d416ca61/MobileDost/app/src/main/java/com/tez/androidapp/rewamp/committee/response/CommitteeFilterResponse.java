
package com.tez.androidapp.rewamp.committee.response;

import com.google.gson.annotations.SerializedName;
import com.tez.androidapp.app.base.response.BaseResponse;

public class CommitteeFilterResponse extends BaseResponse {

    @SerializedName("updates")
    private CommitteeResponseFilterUpdates mUpdates;

    public CommitteeResponseFilterUpdates getUpdates() {
        return mUpdates;
    }

    public void setUpdates(CommitteeResponseFilterUpdates updates) {
        mUpdates = updates;
    }

}
