package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.listener.CommitteeContributeListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeFilterListener;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeFilterActivityInteractorOutput extends CommitteeFilterListener {
}
