package com.tez.androidapp.app.general.feature.faq.callbacks;

import com.tez.androidapp.app.general.feature.faq.models.network.FAQ;

import java.util.List;

/**
 * Created  on 2/28/2017.
 */

public interface GetFAQsCallback {

    void onGetFAQsSuccess(List<FAQ> faqs);

    void onGetFAQsError(int errorCode, String message);
}
