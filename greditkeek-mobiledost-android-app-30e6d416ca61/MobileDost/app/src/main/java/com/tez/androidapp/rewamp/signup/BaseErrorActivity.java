package com.tez.androidapp.rewamp.signup;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public abstract class BaseErrorActivity extends BaseActivity {
    @BindView(R.id.textViewHeading)
    private TezTextView tezTextViewHeading;

    @BindView(R.id.ivLogo)
    private TezImageView ivLogo;

    @BindView(R.id.tvMessage)
    private TezTextView tvMessage;

    @BindView(R.id.tvNumber)
    private TezTextView tvNumber;

    @BindView(R.id.btMainButton)
    private TezButton btMainButton;

    @BindView(R.id.tvCancel)
    private TezTextView tvCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
        ViewBinder.bind(this);
        setTvCancelIOnClickListener(view -> onBackPressed());
    }

    protected void setTvNumberVisibility(int visibility) {
        this.tvNumber.setVisibility(visibility);
    }

    protected void setTvHeadingText(int textId) {
        this.tezTextViewHeading.setText(textId);
    }

    protected void setTvHeadingText(String text) {
        this.tezTextViewHeading.setText(text);
    }

    protected void setIvLogoImageResource(int imageId) {
        this.ivLogo.setImageResource(imageId);
    }

    protected void setTvMessageText(int textId) {
        this.tvMessage.setText(textId);
    }

    protected void setTvNumberText(String text) {
        this.tvNumber.setText(text);
    }

    protected void setBtMainButtonText(int textId) {
        this.btMainButton.setText(textId);
    }

    protected void setBtMainButtonOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        this.btMainButton.setDoubleTapSafeOnClickListener(doubleTapSafeOnClickListener);
    }

    protected void setTvCancelIOnClickListener(@Nullable DoubleTapSafeOnClickListener listener) {
        this.tvCancel.setDoubleTapSafeOnClickListener(listener);
    }
}
