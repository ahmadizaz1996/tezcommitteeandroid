package com.tez.androidapp.rewamp.general.wallet.presenter;

import com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks.AddWalletResendOTPCallback;

public interface IVerifyWalletOtpActivityInteractorOutput extends AddWalletResendOTPCallback {

    void onAddWalletSuccess(int serviceProviderId, String mobileAccountNumber);

    void onAddWalletFailure(int serviceProviderId, int errorCode, String message);

    void onAddWalletVerifyOTPSuccess();

    void onAddWalletVerifyOTPFailure(int serviceProviderId, int errorCode);
}
