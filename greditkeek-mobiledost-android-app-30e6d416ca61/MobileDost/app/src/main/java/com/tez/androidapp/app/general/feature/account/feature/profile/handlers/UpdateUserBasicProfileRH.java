package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.UpdateUserBasicProfileResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;

public class UpdateUserBasicProfileRH extends DashboardCardsRH<UpdateUserBasicProfileResponse> {
    private final UpdateUserProfileCallback callback;

    public UpdateUserBasicProfileRH(BaseCloudDataStore baseCloudDataStore, UpdateUserProfileCallback updateUserProfileCallback) {
        super(baseCloudDataStore);
        this.callback = updateUserProfileCallback;
    }

    @Override
    protected void onSuccess(Result<UpdateUserBasicProfileResponse> value) {
        super.onSuccess(value);
        UpdateUserBasicProfileResponse updateUserBasicProfileResponse = value.response().body();
        if (updateUserBasicProfileResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            callback.onUpdateUserProfileSuccess();
        else onFailure(updateUserBasicProfileResponse.getStatusCode(),
                updateUserBasicProfileResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        callback.onUpdateUserProfileFailure(errorCode, message);
    }
}
