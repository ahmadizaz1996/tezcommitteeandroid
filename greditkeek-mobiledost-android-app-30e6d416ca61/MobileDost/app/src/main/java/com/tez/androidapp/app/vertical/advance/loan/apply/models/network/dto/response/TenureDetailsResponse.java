package com.tez.androidapp.app.vertical.advance.loan.apply.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.advance.loan.apply.models.network.TenureDetail;

import java.util.List;

public class TenureDetailsResponse extends BaseResponse {
    private List<TenureDetail> tenures;

    public List<TenureDetail> getTenures() {
        return tenures;
    }

    public void setTenures(List<TenureDetail> tenures) {
        this.tenures = tenures;
    }
}
