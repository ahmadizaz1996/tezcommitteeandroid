package com.tez.androidapp.rewamp.general.changepin.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

public interface IChangeTemporaryPinActivityInteractor extends IBaseInteractor {

    void changeTemporaryPin(@NonNull String pin);
}
