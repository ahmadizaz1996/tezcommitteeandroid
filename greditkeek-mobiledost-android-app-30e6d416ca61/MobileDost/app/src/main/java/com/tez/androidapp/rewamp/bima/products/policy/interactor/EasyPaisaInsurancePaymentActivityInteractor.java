package com.tez.androidapp.rewamp.bima.products.policy.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InitiatePurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.PurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.IEasyPaisaInsurancePaymentActivityInteractorOutput;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.request.PurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.response.PolicyDetails;
import com.tez.androidapp.rewamp.bima.products.policy.response.PurchasedInsurancePolicy;

public class EasyPaisaInsurancePaymentActivityInteractor implements IEasyPaisaInsurancePaymentActivityInteractor {

    private IEasyPaisaInsurancePaymentActivityInteractorOutput iEasyPaisaInsurancePaymentActivityInteractorOutput;

    public EasyPaisaInsurancePaymentActivityInteractor(IEasyPaisaInsurancePaymentActivityInteractorOutput iEasyPaisaInsurancePaymentActivityInteractorOutput){
        this.iEasyPaisaInsurancePaymentActivityInteractorOutput = iEasyPaisaInsurancePaymentActivityInteractorOutput;

    }

    @Override
    public void initiatePurchasePolicy(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest){
        BimaCloudDataStore.getInstance().initiatePurchasePolicy(initiatePurchasePolicyRequest, new InitiatePurchasePolicyCallback() {
            @Override
            public void initiatePurchasePolicySuccess(PolicyDetails policyDetails) {
                iEasyPaisaInsurancePaymentActivityInteractorOutput.initiatePurchasePolicySuccess(policyDetails);
            }

            @Override
            public void initiatePurchasePolicyFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    initiatePurchasePolicy(initiatePurchasePolicyRequest);
                else
                    iEasyPaisaInsurancePaymentActivityInteractorOutput.initiatePurchasePolicyFailure(errorCode, message);
            }
        });
    }

    @Override
    public void purchasePolicy(PurchasePolicyRequest purchasePolicyRequest){
        BimaCloudDataStore.getInstance().purchasePolicy(purchasePolicyRequest, new PurchasePolicyCallback() {
            @Override
            public void purchasePolicySuccess(PurchasedInsurancePolicy purchasedInsurancePolicy) {
                iEasyPaisaInsurancePaymentActivityInteractorOutput.purchasePolicySuccess(purchasedInsurancePolicy);
            }

            @Override
            public void purchasePolicyFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    purchasePolicy(purchasePolicyRequest);
                else
                    iEasyPaisaInsurancePaymentActivityInteractorOutput.purchasePolicyFailure(errorCode, message);
            }
        });
    }

    @Override
    public void resendCode(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest, InitiatePurchasePolicyCallback callback){
        BimaCloudDataStore.getInstance().initiatePurchasePolicy(initiatePurchasePolicyRequest, new InitiatePurchasePolicyCallback() {
            @Override
            public void initiatePurchasePolicySuccess(PolicyDetails policyDetails) {
                callback.initiatePurchasePolicySuccess(policyDetails);
            }

            @Override
            public void initiatePurchasePolicyFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    initiatePurchasePolicy(initiatePurchasePolicyRequest);
                else
                    callback.initiatePurchasePolicyFailure(errorCode, message);
            }
        });
    }
}
