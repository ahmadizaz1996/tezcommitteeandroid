package com.tez.androidapp.rewamp.committee.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeFilterListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeGroupChatListener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeGroupChatRH extends BaseRH<GetGroupChatMessagesResponse> {

    private final CommitteeGroupChatListener listener;

    public CommitteeGroupChatRH(BaseCloudDataStore baseCloudDataStore, CommitteeGroupChatListener committeeGroupChatListener) {
        super(baseCloudDataStore);
        this.listener = committeeGroupChatListener;
    }

    @Override
    protected void onSuccess(Result<GetGroupChatMessagesResponse> value) {
        GetGroupChatMessagesResponse getGroupChatMessagesResponse = value.response().body();
        if (getGroupChatMessagesResponse != null && getGroupChatMessagesResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onGetMessageSuccess(getGroupChatMessagesResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onGetMessageFailure(errorCode, message);
    }
}
