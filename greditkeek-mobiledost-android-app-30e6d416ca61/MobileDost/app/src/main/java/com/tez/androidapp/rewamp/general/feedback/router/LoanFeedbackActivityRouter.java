package com.tez.androidapp.rewamp.general.feedback.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.feedback.view.LoanFeedbackActivity;

public class LoanFeedbackActivityRouter extends BaseActivityRouter {

    public static final String LOAN_ID = "LOAN_ID";

    public static LoanFeedbackActivityRouter createInstance() {
        return new LoanFeedbackActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, int loanId) {
        Intent intent = createIntent(from);
        intent.putExtra(LOAN_ID, loanId);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, LoanFeedbackActivity.class);
    }
}
