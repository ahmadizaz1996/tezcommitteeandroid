package com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks;

/**
 * Created  on 8/25/2017.
 */

public interface SetAdvanceBimaBeneficiaryCallback {

    void onSetAdvanceBimaBeneficiarySuccess();

    void onSetAdvanceBimaBeneficiaryFailure(int statusCode, String message);
}
