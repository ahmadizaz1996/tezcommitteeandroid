package com.tez.androidapp.rewamp.dashboard.presenter;

import android.view.View;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.rewamp.committee.request.CommitteeLoginRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeMetaDataResponse;
import com.tez.androidapp.rewamp.committee.response.CommittteeLoginResponse;
import com.tez.androidapp.rewamp.dashboard.entity.DashboardCards;
import com.tez.androidapp.rewamp.dashboard.entity.Advance;
import com.tez.androidapp.rewamp.dashboard.entity.LoanStatusDto;
import com.tez.androidapp.rewamp.dashboard.interactor.DashboardActivityInteractor;
import com.tez.androidapp.rewamp.dashboard.interactor.IDashboardActivityInteractor;
import com.tez.androidapp.rewamp.dashboard.response.DashboardActionCardResponse;
import com.tez.androidapp.rewamp.dashboard.response.DashboardAdvanceCardResponse;
import com.tez.androidapp.rewamp.dashboard.response.UserProfileStatusResponse;
import com.tez.androidapp.rewamp.dashboard.view.IDashboardActivityView;

import net.tez.fragment.util.optional.Optional;
import net.tez.fragment.util.optional.TextUtil;

public class DashboardActivityPresenter implements IDashboardActivityPresenter, IDashboardActivityInteractorOutput {

    private final IDashboardActivityView iDashboardActivityView;
    private final IDashboardActivityInteractor iDashboardActivityInteractor;

    public DashboardActivityPresenter(IDashboardActivityView iDashboardActivityView) {
        this.iDashboardActivityView = iDashboardActivityView;
        this.iDashboardActivityInteractor = new DashboardActivityInteractor(this);
    }

    @Override
    public void setDashboard() {
        getDashboardAdvanceCardDetails();
        getDashboardActionCard();
        getUserProfileStatus();
    }

    @Override
    public void setDashboardFromPreference() {
        Optional.ifPresent(MDPreferenceManager.getDashboardCards(), this::setDashboardCards);
    }

    @Override
    public void onDashboardActionCardSuccess(DashboardActionCardResponse dashboardActionCardResponse) {
        if (dashboardActionCardResponse.isSafe())
            iDashboardActivityView.setDashboardActionState(dashboardActionCardResponse.getAction());
        iDashboardActivityView.setShimmerDashboardActionCardVisibility(View.GONE);
        iDashboardActivityView.setErrorDashboardActionCardVisibility(View.GONE);
    }

    @Override
    public void onDashboardActionCardFailure(int errorCode, String message) {
        iDashboardActivityView.enableRefresh();
        iDashboardActivityView.setErrorDashboardActionCardVisibility(View.VISIBLE);
        iDashboardActivityView.setShimmerDashboardActionCardVisibility(View.GONE);
        iDashboardActivityView.setCvDashboardActionVisibility(View.GONE);
    }

    @Override
    public void onDashboardAdvanceCardSuccess(DashboardAdvanceCardResponse dashboardAdvanceCardResponse) {
        iDashboardActivityView.setProductsContainerVisibility(View.VISIBLE);
        iDashboardActivityView.setCvDashboardAdvanceDetailVisibility(View.VISIBLE);
        iDashboardActivityView.setProductsShimmerContainerVisibility(View.GONE);
        iDashboardActivityView.setErrorDashboardAdvanceDetailVisibility(View.GONE);
        iDashboardActivityView.setShimmerDashboardAdvanceDetailVisibility(View.GONE);
        this.setDashboardAdvanceCard(dashboardAdvanceCardResponse.getAdvance());

    }

    @Override
    public void onDashboardAdvanceCardFailure(int errorCode, String message) {
        iDashboardActivityView.enableRefresh();
        iDashboardActivityView.setProductsContainerVisibility(View.VISIBLE);
        iDashboardActivityView.setErrorDashboardAdvanceDetailVisibility(View.VISIBLE);
        iDashboardActivityView.setProductsShimmerContainerVisibility(View.GONE);
        iDashboardActivityView.setShimmerDashboardAdvanceDetailVisibility(View.GONE);
        iDashboardActivityView.setCvDashboardAdvanceDetailVisibility(View.GONE);
        iDashboardActivityView.removeCvAdvanceCardListener();
    }

    @Override
    public void onUserProfileStatusSuccess(UserProfileStatusResponse userProfileStatusResponse) {
        this.setUserProfileStatus(userProfileStatusResponse.getProfileStatus());
    }

    @Override
    public void onUserProfileStatusFailure(int errorCode, String message) {
        iDashboardActivityView.enableRefresh();
        iDashboardActivityView.setTvProfileStatusError();
    }

    private void getDashboardAdvanceCardDetails() {
        iDashboardActivityView.setProductsShimmerContainerVisibility(View.VISIBLE);
        iDashboardActivityView.setShimmerDashboardAdvanceDetailVisibility(View.VISIBLE);
        iDashboardActivityView.setCvDashboardAdvanceDetailVisibility(View.GONE);
        iDashboardActivityView.setErrorDashboardAdvanceDetailVisibility(View.GONE);
        iDashboardActivityView.setProductsContainerVisibility(View.GONE);
        iDashboardActivityInteractor.getDashboardAdvanceCard();
    }

    private void getDashboardActionCard() {
        iDashboardActivityView.setShimmerDashboardActionCardVisibility(View.VISIBLE);
        iDashboardActivityView.setCvDashboardActionVisibility(View.GONE);
        iDashboardActivityView.setErrorDashboardActionCardVisibility(View.GONE);
        iDashboardActivityInteractor.getDashboardActionCard();
    }

    private void getUserProfileStatus() {
        iDashboardActivityView.setTvProfileStatusSuccess("", 0);
        iDashboardActivityInteractor.getUserProfileStatus();
    }

    private void setDashboardCards(@NonNull DashboardCards cards) {
        Optional.ifPresent(cards.getAction(), iDashboardActivityView::setDashboardActionState, this::hideDashboardActionCard);
        Optional.ifPresent(cards.getAdvance(), this::setDashboardAdvanceCard);
        Optional.ifPresent(cards.getProfileStatus(), this::setUserProfileStatus);
    }

    private void hideDashboardActionCard() {
        iDashboardActivityView.setCvDashboardActionVisibility(View.GONE);
    }

    private void setDashboardAdvanceCard(@NonNull Advance advance) {
        LoanStatusDto loanStatusDto = advance.getLoanStatusDto();
        iDashboardActivityView.setDashboardAdvanceCardStatus(advance);
        iDashboardActivityView.setCvAdvanceCardListener(loanStatusDto);
        iDashboardActivityView.setLoanIdForActionCard(loanStatusDto.getLoanId());
    }

    private void setUserProfileStatus(@NonNull String profileStatus) {
        boolean isVerified = TextUtil.equals(profileStatus, "profile verified", true);
        iDashboardActivityView.setTvProfileStatusSuccess(profileStatus, isVerified ? R.drawable.ic_profile_verified : 0);
    }


    @Override
    public void getCommitteeMetadata() {
        iDashboardActivityView.showLoader();
        iDashboardActivityInteractor.getCommitteeMetada();
    }

    public void loginCall() {
        CommitteeLoginRequest committeeLoginRequest = new CommitteeLoginRequest();
        committeeLoginRequest.principalName = "03322786583";
        committeeLoginRequest.password = "0000";
        committeeLoginRequest.imei = "121212121212136";
        committeeLoginRequest.deviceBrand = "Samsung";
        committeeLoginRequest.deviceModel = "Galaxy Note 4";
        committeeLoginRequest.networkOperator = "Warid";
        committeeLoginRequest.deviceOs = "Android";
        committeeLoginRequest.defaultBrowser = "Chrome";
        committeeLoginRequest.lat = 2.5555;
        committeeLoginRequest.lng = 6.504;
        committeeLoginRequest.languageCode = "ROMAN";
        committeeLoginRequest.deviceKey = "345345345345";
        committeeLoginRequest.appVersion = "7.0.0-RC4";
        iDashboardActivityInteractor.loginCall(committeeLoginRequest);
    }

    @Override
    public void onCommitteeLoginSuccess(CommittteeLoginResponse committteeLoginResponse) {
        iDashboardActivityView.committeeLoginSuccess(committteeLoginResponse);
    }

    @Override
    public void onCommitteeLoginFailure(int errorCode, String message) {
        iDashboardActivityView.showError(errorCode,
                (dialog, which) -> iDashboardActivityView.finishActivity());
    }


    @Override
    public void onCommitteeMetadataSuccess(CommitteeMetaDataResponse committeeMetaDataResponse) {
        iDashboardActivityView.hideLoader();
        iDashboardActivityView.storeMetadata(committeeMetaDataResponse);
    }

    @Override
    public void onCommitteeMetadataFailure(int errorCode, String message) {
        iDashboardActivityView.hideLoader();
        iDashboardActivityView.showError(errorCode,
                (dialog, which) -> iDashboardActivityView.finishActivity());
    }

}