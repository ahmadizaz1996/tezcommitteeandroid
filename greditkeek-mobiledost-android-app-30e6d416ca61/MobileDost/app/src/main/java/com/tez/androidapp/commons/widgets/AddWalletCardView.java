package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.utils.app.Utility;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.fragment.util.optional.TextUtil;

import static net.tez.fragment.util.optional.Optional.ifPresent;

/**
 * Created by Rehman Murad Ali
 **/
public class AddWalletCardView extends TezCardView {

    private TezImageView ivWalletLogo;
    private TezImageView ivChangeOrAddWallet;
    private TezTextView tvWalletName;
    private TezTextView tvWalletNumber;
    private TezTextView tvChangeOrAddWallet;
    private String noWalletDesc;
    @Nullable
    private Wallet wallet;

    private ChangeWalletListener listener;

    public AddWalletCardView(@NonNull Context context) {
        super(context);
    }

    public AddWalletCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public AddWalletCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        inflate(context, R.layout.card_view_add_wallet, this);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.AddWalletCardView);
        try {
            this.noWalletDesc = typedArray.getString(R.styleable.AddWalletCardView_no_wallet_desc);
            this.noWalletDesc = TextUtil.isEmpty(this.noWalletDesc) ?
                    getContext().getString(R.string.add_wallet_to_get_loan) :
                    this.noWalletDesc;
            setupViews();

        } finally {
            typedArray.recycle();
        }
    }

    @NonNull
    @Override
    public String getLabel() {
        return tvWalletName.getLabel() + "-" + tvChangeOrAddWallet.getLabel();
    }

    private void setupViews() {
        ivWalletLogo = findViewById(R.id.ivWalletLogo);
        ivChangeOrAddWallet = findViewById(R.id.ivChangeOrAddWallet);
        tvWalletName = findViewById(R.id.tvWalletName);
        tvWalletNumber = findViewById(R.id.tvWalletNumber);
        tvChangeOrAddWallet = findViewById(R.id.tvChangeOrAddWallet);
        setupViewsForNoWallet();
    }

    @Nullable
    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(@Nullable Wallet wallet) {
        this.wallet = wallet;
        ifPresent(wallet, this::setupViewsForWallet, this::setupViewsForNoWallet);
    }

    private void setupViewsForNoWallet() {
        ivWalletLogo.setImageResource(R.drawable.ic_wallet_grey);
        ivChangeOrAddWallet.setImageResource(R.drawable.ic_add_circle);
        tvWalletName.setText(this.noWalletDesc);
        tvWalletNumber.setVisibility(GONE);
        tvChangeOrAddWallet.setText(getContext().getString(R.string.string_add));
        setChangeOrAddWalletOnClickListener(view -> listener.onAddWallet());
    }

    private void setupViewsForWallet(@NonNull Wallet wallet) {
        ivWalletLogo.setImageResource(Utility.getWalletIconResourceId(wallet.getServiceProviderId()));
        ivChangeOrAddWallet.setImageResource(R.drawable.ic_edit_pencil);
        tvWalletName.setText(Utility.getWalletName(wallet.getServiceProviderId()));
        tvWalletNumber.setVisibility(VISIBLE);
        tvWalletNumber.setText(wallet.getMobileAccountNumber());
        tvChangeOrAddWallet.setText(getContext().getString(R.string.string_change));
        setChangeOrAddWalletOnClickListener(view -> listener.onChangeWallet());
    }

    private void setChangeOrAddWalletOnClickListener(DoubleTapSafeOnClickListener onClickListener) {
        tvChangeOrAddWallet.setDoubleTapSafeOnClickListener(onClickListener);
        ivChangeOrAddWallet.setDoubleTapSafeOnClickListener(onClickListener);
    }

    public void setChangeWalletListener(@Nullable ChangeWalletListener listener) {
        this.listener = listener;
    }

    public interface ChangeWalletListener {

        default void onAddWallet() {

        }

        void onChangeWallet();
    }
}
