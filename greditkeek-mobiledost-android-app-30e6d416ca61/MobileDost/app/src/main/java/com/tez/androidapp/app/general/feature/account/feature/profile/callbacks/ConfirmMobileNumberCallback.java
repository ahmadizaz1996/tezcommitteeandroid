package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

/**
 * Created  on 3/22/2017.
 */

public interface ConfirmMobileNumberCallback {

    void onConfirmMobileNumberSuccess();

    void onConfirmMobileNumberFailure(String message);
}
