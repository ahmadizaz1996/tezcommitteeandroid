package com.tez.androidapp.rewamp.dashboard.router;

import android.content.Intent;
import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.view.CommitteeContributeActivity;
import com.tez.androidapp.rewamp.dashboard.view.DashboardActivity;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class DashboardActivityRouter extends BaseActivityRouter {


    public static final String OPEN_WALLET = "OPEN_WALLET";

    public static DashboardActivityRouter createInstance() {
        return new DashboardActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        route(from, intent);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }


    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, DashboardActivity.class);
    }

    public void setDependenciesAndRoute(BaseActivity baseActivity, boolean openWallet) {
        Intent intent = createIntent(baseActivity);
        intent.putExtra(OPEN_WALLET,openWallet);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        route(baseActivity, intent);
    }
}
