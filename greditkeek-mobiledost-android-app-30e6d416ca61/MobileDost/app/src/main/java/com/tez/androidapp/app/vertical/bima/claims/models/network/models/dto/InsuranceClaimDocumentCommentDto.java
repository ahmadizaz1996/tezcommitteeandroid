package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto;

import java.io.Serializable;

/**
 * Created by VINOD KUMAR on 3/5/2019.
 */
public class InsuranceClaimDocumentCommentDto implements Serializable {

    private String comment;
    private String commentDate;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }
}
