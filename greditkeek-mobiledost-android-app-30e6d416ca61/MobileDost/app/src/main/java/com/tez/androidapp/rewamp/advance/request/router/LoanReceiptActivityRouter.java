package com.tez.androidapp.rewamp.advance.request.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.request.view.LoanReceiptActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class LoanReceiptActivityRouter extends BaseActivityRouter {

    public static LoanReceiptActivityRouter createInstance() {
        return new LoanReceiptActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    public void setDependenciesAndRouteWithClearAndNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, LoanReceiptActivity.class);
    }
}
