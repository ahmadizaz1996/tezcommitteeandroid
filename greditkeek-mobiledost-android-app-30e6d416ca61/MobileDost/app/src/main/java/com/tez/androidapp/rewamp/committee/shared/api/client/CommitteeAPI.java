package com.tez.androidapp.rewamp.committee.shared.api.client;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.rewamp.committee.request.CommitteeCheckInvitesRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeDeclineRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeFilterRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeInstallmentPayRequestCommittee;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeLeaveRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeLoginRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeMetaDataRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeSendMessageRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeWalletRequest;
import com.tez.androidapp.rewamp.committee.request.GetCommitteeGroupChatRequest;
import com.tez.androidapp.rewamp.committee.request.GetInvitedPackageRequest;
import com.tez.androidapp.rewamp.committee.request.JoinCommitteeRequest;
import com.tez.androidapp.rewamp.committee.request.JoinCommitteeVerificationRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteePaymentInitiateRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeDeclineResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeInstallmentPayResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeLeaveResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeMetaDataResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeSendMessageResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeWalletResponse;
import com.tez.androidapp.rewamp.committee.response.CommittteeLoginResponse;
import com.tez.androidapp.rewamp.committee.response.GetGroupChatMessagesResponse;
import com.tez.androidapp.rewamp.committee.response.GetInvitedPackageResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeVerificationResponse;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.response.PaymentInitiateResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Ahmad Izaz on 06-Nov-20
 **/
public interface CommitteeAPI {

    @GET(CommitteeMetaDataRequest.METHOD_NAME)
    Observable<Result<CommitteeMetaDataResponse>> getCommitteeMetadata();

    @Headers("Content-Type: application/json")
    @POST(CommitteeInviteRequest.METHOD_NAME)
    Observable<Result<CommitteeInviteResponse>> getInvite(@Body List<CommitteeInviteRequest> committeeInviteRequest);

    @Headers("Content-Type: application/json")
    @POST(CommitteeInviteRequest.METHOD_NAME)
    Observable<Result<CommitteeInviteResponse>> getInvite(@Body CommitteeInviteRequest committeeInviteRequest);

    @Headers("Content-Type: application/json")
    @GET(CommitteeLeaveRequest.METHOD_NAME)
    Observable<Result<CommitteeLeaveResponse>> leaveCommittee(@Path(CommitteeLeaveRequest.Params.ID) String id);

    @Headers("Content-Type: application/json")
    @GET(CommitteeDeclineRequest.METHOD_NAME)
    Observable<Result<CommitteeDeclineResponse>> declineCommittee(@Path(CommitteeDeclineRequest.Params.COMMITTEE_ID) String committeeId);


    @Headers("Content-Type: application/json")
    @POST(JoinCommitteeRequest.METHOD_NAME)
    Observable<Result<JoinCommitteeResponse>> joinCommittee(@Body JoinCommitteeRequest joinCommitteeRequest);

    @Headers("Content-Type: application/json")
    @POST(CommitteeLoginRequest.METHOD_NAME)
    Observable<Result<CommittteeLoginResponse>> committeeLogin(@Body CommitteeLoginRequest committeeLoginRequest);

    @Headers("Content-Type: application/json")
    @POST(CommitteeInviteRequest.METHOD_NAME)
    Call<CommitteeInviteResponse> getInvites(@Header("Authorization") String auth,
                                             @Body CommitteeInviteRequest committeeInviteRequest);

    @Headers("Content-Type: application/json")
    @POST(CommitteeCreateRequest.METHOD_NAME)
    Observable<Result<CommitteeCreateResponse>> createCommittee(@Body CommitteeCreateRequest committeeCreateRequest);


    @Headers("Content-Type: application/json")
    @POST(JoinCommitteeVerificationRequest.METHOD_NAME)
    Observable<Result<JoinCommitteeVerificationResponse>> verifyOtp(@Body JoinCommitteeVerificationRequest joinCommitteeVerificationRequest);

    @Headers("Content-Type: application/json")
    @POST(CommitteePaymentInitiateRequest.METHOD_NAME)
    Observable<Result<PaymentInitiateResponse>> initiatePayment(@Body CommitteePaymentInitiateRequest committeePaymentInitiateRequest);

    @Headers("Content-Type: application/json")
    @POST(CommitteeInstallmentPayRequestCommittee.METHOD_NAME)
    Observable<Result<CommitteeInstallmentPayResponse>> payInstallment(@Body CommitteeInstallmentPayRequestCommittee committeeInstallmentPayRequestCommittee);

    @Headers("Content-Type: application/json")
    @POST(CommitteeCreateRequest.METHOD_NAME)
    Call<CommitteeCreateResponse> createCommittee(
            @Header("Authorization") String auth,
            @Body CommitteeCreateRequest committeeCreateRequest);

    @Headers("Content-Type: application/json")
    @GET(MyCommitteeRequest.METHOD_NAME)
    Observable<Result<MyCommitteeResponse>> getMyCommittee(/*@Body MyCommitteeRequest myCommitteeRequests*/);


    @Headers("Content-Type: application/json")
    @GET(CommitteeCheckInvitesRequest.METHOD_NAME)
    Observable<Result<List<CommitteeCheckInvitesResponse>>> checkInvites(@Path(CommitteeCheckInvitesRequest.Params.MOBILE_NUMBER) String mobileNumber);

    @Headers("Content-Type: application/json")
    @GET(GetInvitedPackageRequest.METHOD_NAME)
    Observable<Result<GetInvitedPackageResponse>> getInvitedPackage(@Path(GetInvitedPackageRequest.Params.MOBILE_NUMBER) String mobileNumber);

    @Headers("Content-Type: application/json")
    @GET(CommitteeWalletRequest.METHOD_NAME)
    Observable<Result<CommitteeWalletResponse>> getWallets();

    @Headers("Content-Type: application/json")
    @GET(CommitteeFilterRequest.METHOD_NAME)
    Observable<Result<CommitteeFilterResponse>> getFilter(@Path(CommitteeFilterRequest.Params.COMMITTEE_ID) String committeeId);


    @Headers("Content-Type: application/json")
    @GET(GetCommitteeGroupChatRequest.METHOD_NAME)
    Observable<Result<GetGroupChatMessagesResponse>> getMessage(@Path(GetCommitteeGroupChatRequest.Params.COMMITTEE_ID) String committeeId);


    @Headers("Content-Type: application/json")
    @POST(CommitteeSendMessageRequest.METHOD_NAME)
    Observable<Result<CommitteeSendMessageResponse>> sendMessage(@Body CommitteeSendMessageRequest committeeSendMessageRequest);


}
