package com.tez.androidapp.rewamp.general.suspend.account;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public class FeedbackRH extends BaseRH<BaseResponse> {

    private FeedbackCallback feedbackCallback;


    public FeedbackRH(BaseCloudDataStore baseCloudDataStore, FeedbackCallback feedbackCallback) {
        super(baseCloudDataStore);
        this.feedbackCallback = feedbackCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (baseResponse != null) {
            if (baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
                feedbackCallback.onFeedbackSuccess();
            else
                onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
        } else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.feedbackCallback.onFeedbackFailure(errorCode, message);
    }
}
