package com.tez.androidapp.app.vertical.bima.claims.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.bima.claims.callback.UploadClaimAnswerCallback;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.UploadClaimAnswerResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by FARHAN DHANANI on 6/29/2018.
 */
public class UploadClaimAnswerRH extends BaseRH<UploadClaimAnswerResponse> {

    private UploadClaimAnswerCallback uploadClaimAnswerCallback;

    public UploadClaimAnswerRH(BaseCloudDataStore baseCloudDataStore, UploadClaimAnswerCallback uploadClaimAnswerCallback) {
        super(baseCloudDataStore);
        this.uploadClaimAnswerCallback = uploadClaimAnswerCallback;
    }

    @Override
    protected void onSuccess(Result<UploadClaimAnswerResponse> value) {
        UploadClaimAnswerResponse uploadClaimAnswerResponse = value.response().body();
        if (uploadClaimAnswerResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.uploadClaimAnswerCallback.onUploadClaimAnswerSuccess();
        else
            onFailure(uploadClaimAnswerResponse.getStatusCode(), uploadClaimAnswerResponse.getErrorDescription());

    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.uploadClaimAnswerCallback.onUploadClaimAnswerFailure(errorCode, message);
    }
}
