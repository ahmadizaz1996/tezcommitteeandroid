package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.listener.CommitteeFilterListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeGroupChatListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeGroupChatSendMessageListener;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeGroupChatActivityInteractorOutput extends CommitteeGroupChatListener, CommitteeGroupChatSendMessageListener {
}
