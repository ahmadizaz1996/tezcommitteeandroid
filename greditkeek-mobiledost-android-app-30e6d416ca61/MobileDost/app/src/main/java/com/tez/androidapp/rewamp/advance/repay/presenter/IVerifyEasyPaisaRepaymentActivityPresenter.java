package com.tez.androidapp.rewamp.advance.repay.presenter;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public interface IVerifyEasyPaisaRepaymentActivityPresenter {

    void initiateRepayment(int loanId, int mobileAccountId);

    void repayLoan(@NonNull String pin,
                   int mobileAccountId,
                   double amount,
                   @Nullable String repaymentCorrelationId,
                   @Nullable Location location);
}
