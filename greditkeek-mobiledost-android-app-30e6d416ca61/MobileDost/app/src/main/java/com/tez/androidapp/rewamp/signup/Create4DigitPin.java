package com.tez.androidapp.rewamp.signup;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.View;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.validators.annotations.ConfirmPinViewRegex;
import com.tez.androidapp.commons.validators.annotations.PinViewRegex;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezPinEditText;
import com.tez.androidapp.rewamp.signup.router.PinChangedActivityRouter;

import net.tez.validator.library.annotations.Order;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Create4DigitPin extends PlaybackActivity implements ValidationListener {

    @PinViewRegex(regex = Constants.STRING_PIN_VALIDATOR_REGEX, value = {1, R.string.please_enter_valid_pin})
    @Order(1)
    @BindView(R.id.pinEtCreatePin)
    private TezPinEditText pinEtCreatePin;

    @ConfirmPinViewRegex(id = R.id.pinEtCreatePin, value = {1, R.string.please_enter_valid_pin})
    @Order(2)
    @BindView(R.id.pinEtConfirmPin)
    private TezPinEditText pinEtConfirmPin;

    @BindView(R.id.btCreatePin)
    private TezButton btCreatePin;

    private CompositeDisposable allDisposables;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create4_digit_pin);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.allDisposables = new CompositeDisposable();
        listenChangesOnViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        allDisposables.dispose();
    }

    @Override
    public int getAudioId() {
        return 0;
    }


    private void init() {
        initOnClickListners();
    }

    private void initOnClickListners() {
        this.btCreatePin.setDoubleTapSafeOnClickListener(view -> performValidation());
    }

    private void performValidation() {
        FieldValidator.validate(this, new ValidationListener() {
            @Override
            public void validateSuccess() {
                onClickMainButton();
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                filterChain.doFilter();
            }
        });
    }

    protected void onClickMainButton() {
        PinChangedActivityRouter.createInstance().setDependenciesAndRoute(Create4DigitPin.this);
    }

    protected String getPin() {
        return this.pinEtCreatePin.getPin();
    }

    private void listenChangesOnViews() {
        allDisposables.add(RxTextView.textChanges(this.pinEtCreatePin.getPinEditText()).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
        allDisposables.add(RxTextView.textChanges(this.pinEtConfirmPin.getPinEditText()).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));

    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void validateSuccess() {
        this.btCreatePin.setButtonNormal();
        this.pinEtConfirmPin.setError(null);
        this.pinEtCreatePin.setError(null);
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.btCreatePin.setButtonInactive();
        filterChain.doFilter();
    }

    @Override
    protected String getScreenName() {
        return "Create4DigitPin";
    }
}
