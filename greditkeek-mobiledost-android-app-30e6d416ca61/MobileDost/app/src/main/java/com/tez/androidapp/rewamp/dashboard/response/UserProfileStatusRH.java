package com.tez.androidapp.rewamp.dashboard.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.callbacks.UserProfileStatusCallback;

public class UserProfileStatusRH extends BaseRH<UserProfileStatusResponse> {

    private final UserProfileStatusCallback callback;

    public UserProfileStatusRH(BaseCloudDataStore baseCloudDataStore, UserProfileStatusCallback callback) {
        super(baseCloudDataStore);
        this.callback = callback;
    }

    @Override
    protected void onSuccess(Result<UserProfileStatusResponse> value) {
        UserProfileStatusResponse response = value.response().body();
        if (response != null && response.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.callback.onUserProfileStatusSuccess(response);
        else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.callback.onUserProfileStatusFailure(errorCode, message);
    }
}
