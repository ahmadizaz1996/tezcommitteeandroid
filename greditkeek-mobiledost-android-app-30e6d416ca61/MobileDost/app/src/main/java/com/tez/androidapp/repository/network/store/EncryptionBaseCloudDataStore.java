package com.tez.androidapp.repository.network.store;

import com.tez.androidapp.BuildConfig;
import com.tez.androidapp.commons.utils.network.EncryptionInterceptor;

import okhttp3.OkHttpClient;

/**
 * Created by Rehman Murad Ali on 3/15/2018.
 */

public abstract class EncryptionBaseCloudDataStore extends BaseCloudDataStore {
    @Override
    protected OkHttpClient.Builder addMoreFeaturesToClient() {
        okHttpClientBuilder.addInterceptor(new EncryptionInterceptor());
        return okHttpClientBuilder;
    }
}
