package com.tez.androidapp.commons.validators.filters;

import android.widget.EditText;

import androidx.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.validators.annotations.CnicExpiryDateIsValid;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;

import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by FARHAN DHANANI on 11/25/2018.
 */
@SuppressWarnings("unused")
public class CnicExpiryDateIsValidFilter implements Filter<TezTextInputLayout, CnicExpiryDateIsValid> {
    @Override
    public boolean isValidated(@NonNull TezTextInputLayout view, @NonNull CnicExpiryDateIsValid annotation) {
        EditText editText = view.getEditText();
        if (editText == null)
            return true;
        String text = editText.getText().toString();
        return TextUtil.isEmpty(text) || text.equals(MobileDostApplication.getInstance().getApplicationContext().getString(R.string.string_lifetime))
                || validExpiryDate(text, annotation);
    }

    private boolean validExpiryDate(@NonNull String date, @NonNull CnicExpiryDateIsValid annotation) {
        try {
            Calendar calendar = Calendar.getInstance();
            return TextUtil.getFormattedDate(date, annotation.dateFormat())
                    .after(calendar.getTime());
        } catch (ParseException e) {
            Crashlytics.logException(e);
        }
        return false;
    }
}
