package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto;

import android.os.Parcel;
import android.os.Parcelable;
import net.tez.tezrecycler.model.GenericDropDownListModel;

/**
 * Created by VINOD KUMAR on 11/23/2018.
 */
public class RejectedDocument extends GenericDropDownListModel implements Parcelable {

    public static final Creator<RejectedDocument> CREATOR = new Creator<RejectedDocument>() {
        @Override
        public RejectedDocument createFromParcel(Parcel in) {
            return new RejectedDocument(in);
        }

        @Override
        public RejectedDocument[] newArray(int size) {
            return new RejectedDocument[size];
        }
    };
    private String name;
    private Integer id;
    private String comment;
    private String commentDate;

    public RejectedDocument(Integer id, String name, String comment, String commentDate) {
        this.id = id;
        this.comment = comment;
        this.name = name;
        this.commentDate = commentDate;
    }

    private RejectedDocument(Parcel in) {
        name = in.readString();
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        comment = in.readString();
        commentDate = in.readString();
    }

    public String getCommentDate() {
        return commentDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(comment);
        dest.writeString(commentDate);
    }
}
