package com.tez.androidapp.rewamp.advance.request.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.widgets.AddWalletCardView;
import com.tez.androidapp.commons.widgets.RequestAdvanceCardView;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.request.adapter.LoanDurationsAdapter;
import com.tez.androidapp.rewamp.advance.request.model.LoanDuration;
import com.tez.androidapp.rewamp.advance.request.presenter.ISelectLoanActivityPresenter;
import com.tez.androidapp.rewamp.advance.request.presenter.SelectLoanActivityPresenter;
import com.tez.androidapp.rewamp.advance.request.request.LoanApplyRequest;
import com.tez.androidapp.rewamp.advance.request.router.CancelLimitActivityRouter;
import com.tez.androidapp.rewamp.advance.request.router.LoanPurposeActivityRouter;
import com.tez.androidapp.rewamp.advance.request.router.LoanRequiredStepsActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.router.ChangeWalletActivityRouter;

import net.tez.fragment.util.optional.Optional;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.List;

public class SelectLoanActivity extends BaseActivity implements ISelectLoanActivityView, LoanDurationsAdapter.LoanDurationListener, RequestAdvanceCardView.AmountChangeListener, AddWalletCardView.ChangeWalletListener {

    private final ISelectLoanActivityPresenter iSelectLoanActivityPresenter;

    @BindView(R.id.rvAdvanceDuration)
    RecyclerView rvAdvanceDuration;

    @BindView(R.id.cvEnterAmount)
    RequestAdvanceCardView cvEnterAmount;

    @BindView(R.id.tvProcessingFeeValue)
    private TezTextView tvProcessingFeeValue;

    @BindView(R.id.tvAmountReceiveValue)
    private TezTextView tvAmountReceiveValue;

    @BindView(R.id.tvChargesValue)
    private TezTextView tvChargesValue;

    @BindView(R.id.tvAmountReturnValue)
    private TezTextView tvAmountReturnValue;

    @BindView(R.id.cvMobileWallet)
    private AddWalletCardView cvMobileWallet;

    @BindView(R.id.btRequestAdvance)
    private TezButton btRequestAdvance;

    @BindView(R.id.tvCancelMyLimit)
    private TezTextView tvCancelMyLimit;

    @BindView(R.id.ivLeftArrow)
    private TezImageView ivLeftArrow;

    @BindView(R.id.ivRightArrow)
    private TezImageView ivRightArrow;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    @BindView(R.id.greyBackground)
    private TezLinearLayout greyBackground;

    private LoanDurationsAdapter loanDurationsAdapter;

    public SelectLoanActivity() {
        iSelectLoanActivityPresenter = new SelectLoanActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_loan);
        ViewBinder.bind(this);
        init();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == LoanRequiredStepsActivityRouter.REQUEST_CODE_LOAN_REQUIRED_STEPS_ACTIVITY)
            iSelectLoanActivityPresenter.getLoanLimit();

        else if (resultCode == RESULT_OK && data != null && requestCode == ChangeWalletActivityRouter.REQUEST_CODE_CHANGE_WALLET) {
            setUserWallet(data.getParcelableExtra(ChangeWalletActivityRouter.RESULT_DATA_WALLET));
            initCalculateAmount();
        }
    }

    @Override
    public void setMaxLoanLimit(double loanLimit) {
        this.cvEnterAmount.setLimitAmount(loanLimit);
    }

    @Override
    public void showMaxLimitPrompt(String maxLimit) {
        showInformativeMessage(getString(R.string.loan_max_amount_message, maxLimit));
    }

    @Override
    public void setUserWallet(@Nullable Wallet wallet) {
        this.cvMobileWallet.setWallet(wallet);
        iSelectLoanActivityPresenter.setMobileAccountId(wallet);
        this.btRequestAdvance.setButtonBackgroundStyle(wallet == null ? TezButton.ButtonStyle.INACTIVE : TezButton.ButtonStyle.NORMAL);
        this.btRequestAdvance.setEnabled(wallet != null);
    }

    @Override
    public void initCalculateAmount() {
        iSelectLoanActivityPresenter.calculateAllAmounts(cvMobileWallet.getWallet(), cvEnterAmount.getAmount(), loanDurationsAdapter.getSelectedLoanDuration().getDayTenure());
    }

    @Override
    public void setTvCancelMyLimitOnClickListener(int loanId) {
        tvCancelMyLimit.setDoubleTapSafeOnClickListener(view -> CancelLimitActivityRouter.createInstance().setDependenciesAndRoute(this, loanId));
    }

    private void init() {
        cvEnterAmount.setRequestAdvanceTextChangeListener(this);
        cvMobileWallet.setChangeWalletListener(this);
        btRequestAdvance.setDoubleTapSafeOnClickListener(view -> iSelectLoanActivityPresenter.onClickBtContinue(cvEnterAmount.getAmount(), cvMobileWallet.getWallet()));
        iSelectLoanActivityPresenter.getLoanLimit();
    }

    private void setArrowsBehaviour() {
        ivLeftArrow.setOnClickListener(v -> rvAdvanceDuration.smoothScrollToPosition(0));
        ivRightArrow.setOnClickListener(v -> rvAdvanceDuration.smoothScrollToPosition(loanDurationsAdapter.getItemCount() - 1));
        ivLeftArrow.setAlpha(0.2F);

        Optional.doWhen(!rvAdvanceDuration.canScrollHorizontally(1),
                () -> ivRightArrow.setAlpha(0.2F));

        rvAdvanceDuration.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                Optional.doWhen(rvAdvanceDuration.canScrollHorizontally(1),
                        () -> ivRightArrow.setAlpha(1F),
                        () -> ivRightArrow.setAlpha(0.2F));

                Optional.doWhen(rvAdvanceDuration.canScrollHorizontally(-1),
                        () -> ivLeftArrow.setAlpha(1F),
                        () -> ivLeftArrow.setAlpha(0.2F));
            }
        });
    }

    @Override
    public void routeToLoanPurposeActivity(@NonNull LoanApplyRequest loanApplyRequest) {
        LoanPurposeActivityRouter.createInstance().setDependenciesAndRoute(this, loanApplyRequest);
    }

    @Override
    public void setLoanDurations(@NonNull List<LoanDuration> loanDurationList) {
        this.loanDurationsAdapter = new LoanDurationsAdapter(loanDurationList, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        this.rvAdvanceDuration.setLayoutManager(linearLayoutManager);
        this.rvAdvanceDuration.setAdapter(loanDurationsAdapter);
        this.setArrowsBehaviour();
    }

    @Override
    public void setTvProcessingFeeValue(String value) {
        this.tvProcessingFeeValue.setText(getString(R.string.rs_value_string, value));
    }

    @Override
    public void setTvChargesValue(String value) {
        this.tvChargesValue.setText(getString(R.string.rs_value_string, value));
    }

    @Override
    public void setTvAmountReceiveValue(String value) {
        this.tvAmountReceiveValue.setText(getString(R.string.rs_value_string, value));
    }

    @Override
    public void setTvAmountReturnValue(String value) {
        this.tvAmountReturnValue.setText(getString(R.string.rs_value_string, value));
    }

    @Override
    public void setClContentVisibility(int visibility) {
        this.clContent.setVisibility(visibility);
        this.greyBackground.setVisibility(visibility);
    }

    @Override
    public void showTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.GONE);
    }

    @Override
    public void onClickLoanDuration(@NonNull LoanDuration loanDuration) {
        iSelectLoanActivityPresenter.calculateAllAmounts(cvMobileWallet.getWallet(), cvEnterAmount.getAmount(), loanDuration.getDayTenure());
    }

    @Override
    public void onAmountChange(double amount) {
        loanDurationsAdapter.setLoanAmount(amount);
        iSelectLoanActivityPresenter.calculateAllAmounts(cvMobileWallet.getWallet(), amount, loanDurationsAdapter.getSelectedLoanDuration().getDayTenure());
    }

    @Override
    public void onAddWallet() {
        LoanRequiredStepsActivityRouter.createInstance().setDependenciesAndRouteForResult(this);
    }

    @Override
    public void onChangeWallet() {
        ChangeWalletActivityRouter.createInstance().setDependenciesAndRouteForResult(this);
    }

    @Override
    protected String getScreenName() {
        return "SelectLoanActivity";
    }
}