package com.tez.androidapp.rewamp.general.changepin.presenter;

import androidx.annotation.NonNull;

public interface IChangeTemporaryPinActivityPresenter {

    void changeTemporaryPin(@NonNull String pin);
}
