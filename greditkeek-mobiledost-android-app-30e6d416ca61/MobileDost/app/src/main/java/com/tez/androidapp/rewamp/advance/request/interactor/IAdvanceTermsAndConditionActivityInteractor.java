package com.tez.androidapp.rewamp.advance.request.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.rewamp.advance.request.request.LoanApplyRequest;

public interface IAdvanceTermsAndConditionActivityInteractor extends IBaseInteractor {

    void getLoanRemainingSteps();

    void applyLoan(@NonNull LoanApplyRequest loanApplyRequest);
}
