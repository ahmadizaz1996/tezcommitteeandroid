package com.tez.androidapp.rewamp.committee.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeWalletVerificationLIstener;
import com.tez.androidapp.rewamp.committee.listener.JoinCommitteeVerificationLIstener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class PaymentInitiateRH extends BaseRH<PaymentInitiateResponse> {

    private final CommitteeWalletVerificationLIstener listener;

    public PaymentInitiateRH(BaseCloudDataStore baseCloudDataStore, CommitteeWalletVerificationLIstener committeeWalletVerificationLIstener) {
        super(baseCloudDataStore);
        this.listener = committeeWalletVerificationLIstener;
    }

    @Override
    protected void onSuccess(Result<PaymentInitiateResponse> value) {
        PaymentInitiateResponse paymentInitiateResponse = value.response().body();
        if (paymentInitiateResponse != null && paymentInitiateResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onPaymentInitiateSuccess(paymentInitiateResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onPaymentInitiateFailure(errorCode, message);
    }
}
