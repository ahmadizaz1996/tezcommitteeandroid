package com.tez.androidapp.app.vertical.bima.claims.callback;

import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.ExtentionSize;

import java.util.List;

/**
 * Created by FARHAN DHANANI on 7/20/2018.
 */
public interface GetSizeForExtentionCallback {

    void onGetSizeForExtentionSuccess(List<ExtentionSize> extentionSizeList);

    void onGetSizeForExtentionFailure(int errorCode, String message);
}
