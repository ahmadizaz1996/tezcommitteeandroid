package com.tez.androidapp.commons.utils.cnic.detection.view.surface.view.contracts;

public interface CameraPreviewView {



    void resetSettings();

    void showAlertDialog(String message);
}
