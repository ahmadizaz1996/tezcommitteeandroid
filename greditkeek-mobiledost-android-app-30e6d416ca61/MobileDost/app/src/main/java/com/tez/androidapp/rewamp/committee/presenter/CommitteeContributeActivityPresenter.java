package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.interactor.CommitteeContributeActivityInteractor;
import com.tez.androidapp.rewamp.committee.interactor.CommitteeInviteActivityInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeWalletResponse;
import com.tez.androidapp.rewamp.committee.view.ICommitteeContibueActivityView;
import com.tez.androidapp.rewamp.committee.view.ICommitteeInviteActivityView;

import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeContributeActivityPresenter implements ICommitteeContributeActivityPresenter, ICommitteeContributeActivityInteractorOutput {

    private final ICommitteeContibueActivityView iCommitteeContibueActivityView;
    private final CommitteeContributeActivityInteractor committeeContributeActivityInteractor;

    public CommitteeContributeActivityPresenter(ICommitteeContibueActivityView iCommitteeContibueActivityView) {
        this.iCommitteeContibueActivityView = iCommitteeContibueActivityView;
        committeeContributeActivityInteractor = new CommitteeContributeActivityInteractor(this);
    }

    @Override
    public void onGetWalletSuccess(CommitteeWalletResponse committeeWalletResponse) {
        iCommitteeContibueActivityView.hideLoader();
        iCommitteeContibueActivityView.onGetWallets(committeeWalletResponse);
    }

    @Override
    public void onGetWalletFailure(int errorCode, String message) {
        iCommitteeContibueActivityView.hideLoader();
        iCommitteeContibueActivityView.showError(errorCode,
                (dialog, which) -> iCommitteeContibueActivityView.finishActivity());
    }

    @Override
    public void getWallets() {
        iCommitteeContibueActivityView.showLoader();
        committeeContributeActivityInteractor.getWallets();

    }
}
