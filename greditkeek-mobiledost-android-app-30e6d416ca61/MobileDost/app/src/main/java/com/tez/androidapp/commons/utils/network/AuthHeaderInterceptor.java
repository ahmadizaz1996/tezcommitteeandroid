package com.tez.androidapp.commons.utils.network;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.utils.app.Utility;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Interceptor to intercept Auth requests and add the Authorization header to the request
 * <p>
 * Created  on 6/15/2017.
 */

public class AuthHeaderInterceptor implements Interceptor {


    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request originalRequest = chain.request();
        String auth = Utility.getAuthorizationHeaderValue();
        if (auth != null) {
            originalRequest = originalRequest.newBuilder().header("Authorization", auth).build();
        } else {
            Utility.directUserToIntroScreenAtGlobalLevel();
        }
        return chain.proceed(originalRequest);
    }

}
