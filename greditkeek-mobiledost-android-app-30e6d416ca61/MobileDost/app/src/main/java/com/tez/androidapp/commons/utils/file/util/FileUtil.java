package com.tez.androidapp.commons.utils.file.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.os.Environment;
import androidx.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.tez.androidapp.commons.managers.NativeKeysManager;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.utils.extract.ExtractionUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Utility to create the zip file for Data Lift
 * <p>
 * Created  on 1/24/2017.
 */

public class FileUtil {

    /**
     * Get the root directory to store the extracted files
     *
     * @param context get the root path
     * @return Path of the root directory
     */
    private static File getExtractionRoot(Context context) {
        File root = new File(context.getFilesDir(), NativeKeysManager.getInstance().getDataDirectoryPath());
        createDirectory(root);
        return root;
    }


    public static File getPicturesRoot(Context context) {
        File root = new File(context.getFilesDir(), NativeKeysManager.getInstance().getPicturesDirectoryPath());
        createDirectory(root);
        return root;
    }

    public static String getSignUpRootPath(@NonNull Context context) {
        return context.getFilesDir() + NativeKeysManager.getInstance().getSignUpDirectoryPath();
    }


    /**
     * Get the root directory where the zip file will be stored
     *
     * @param context to get the output directory where user data will be stored
     * @return Path of the output directory
     */
    public static File getOutputRoot(Context context) {
        File root = new File(context.getFilesDir(), NativeKeysManager.getInstance().getOutputDirectoryPath());
        createDirectory(root);
        return root;
    }

    @NonNull
    public static String getDocumentRootPath(@NonNull Context context) {
        return context.getFilesDir() + NativeKeysManager.getInstance().getDocumentsDirectoryPath();
    }

    /**
     * Create the zip file for full combined data
     *
     * @param context to get the root directory of zip file
     * @return Path of the newly created data file
     */
    public static File createZipFile(Context context) {
        File root = getOutputRoot(context);
        File zipFile = new File(root, "data.zip");
        deleteFile(zipFile);

        File[] filesToZip = null;

        try (FileOutputStream fileOutputStream = new FileOutputStream(zipFile.getAbsolutePath());
             ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream)) {

            File inputRoot = getExtractionRoot(context);
            filesToZip = inputRoot.listFiles();

            for (File file : filesToZip)
                addToZipFile(file.getAbsolutePath(), zipOutputStream);

        } catch (IOException e) {
            Crashlytics.logException(e);
        } finally {
            if (filesToZip != null)
                for (File file : filesToZip) {
                    deleteFile(file);
                }
        }
        return zipFile;
    }

    private static void addToZipFile(String fileName, ZipOutputStream zipOutputStream) {
        File file = new File(fileName);
        try (FileInputStream fis = new FileInputStream(file)) {
            ZipEntry zipEntry = new ZipEntry(file.getName());
            zipOutputStream.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOutputStream.write(bytes, 0, length);
            }
            zipOutputStream.closeEntry();
        } catch (IOException e) {
            Crashlytics.logException(e);
        }
    }

    /**
     * Create txt file with the passed data
     *
     * @param context get the root path
     * @param type    Type of extracted data passed
     * @param data    Extracted Data
     */
    public static void createFileWithData(Context context, int type, String data) {
        String fileName = getFileNameForType(type);
        if (fileName == null)
            return;
        File file = new File(getExtractionRoot(context), fileName);
        try {
            deleteFile(file);

            createNewFile(file);
            PrintWriter printWriter = new PrintWriter(file.getAbsolutePath());
            printWriter.write(data);
            printWriter.close();
        } catch (IOException e) {
            Crashlytics.logException(e);
        }
    }

    public static void createFileWithObjectData(Context context, int type, Object data) {
        String fileName = getFileNameForType(type);
        if (fileName == null)
            return;

        File file = new File(getExtractionRoot(context), fileName);

        deleteFile(file);
        createNewFile(file);

        try (PrintWriter printWriter = new PrintWriter(file.getAbsolutePath())) {

            Gson gson = new Gson();
            gson.toJson(data, printWriter);

        } catch (IOException | JsonIOException e) {
            Crashlytics.logException(e);
        }
    }


    /**
     * Get file name for the passed type
     *
     * @param type Type as in {@link ExtractionUtil} class
     * @return File name as String
     */
    private static String getFileNameForType(int type) {
        if (type == ExtractionUtil.TYPE_ACCOUNTS) return "accounts_list.txt";
        else if (type == ExtractionUtil.TYPE_APP_LOGS) return "app_log.txt";
        else if (type == ExtractionUtil.TYPE_BLUETOOTH) return "bluetooth_list.txt";
        else if (type == ExtractionUtil.TYPE_CALENDAR) return "calendar_list.txt";
        else if (type == ExtractionUtil.TYPE_CALL_LOG) return "call_log.txt";
        else if (type == ExtractionUtil.TYPE_CONTACTS) return "contacts_list.txt";
        else if (type == ExtractionUtil.TYPE_FILE_DIRECTORY) return "file_sd.txt";
        else if (type == ExtractionUtil.TYPE_MSG_LOG) return "msg_log.txt";
        else if (type == ExtractionUtil.TYPE_WIFI) return "wifi_list.txt";
        else if (type == ExtractionUtil.TYPE_DEVICE_INFO) return "device_info.txt";
        else if (type == ExtractionUtil.TYPE_LOCATION) return "location.txt";
        return null;
    }

    /**
     * Delete file or directory fully
     *
     * @param fileOrDirectory File or directory to delete
     */
    private static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        }

        deleteFile(fileOrDirectory);
    }

    /**
     * Append Object to given File Type
     *
     * @param context Context in which File will be present
     * @param type    Which file to write
     * @param data    Object to append in File
     */
    public static void writeObjectToFile(Context context, int type, Object data, String delimiter) {
        String fileName = getFileNameForType(type);
        if (fileName == null)
            return;

        File file = new File(getExtractionRoot(context), fileName);
        createNewFile(file);

        try (FileOutputStream outputStream = new FileOutputStream(file, true);
             PrintWriter printWriter = new PrintWriter(outputStream)) {

            Gson gson = new Gson();
            gson.toJson(data, printWriter);
            printWriter.println(delimiter);
        } catch (IOException e) {
            Crashlytics.logException(e);
        }
    }

    public static File prepareFileForOCR(byte[] image, Activity activity) {
        File file;

        try {
            file = createImageFile(activity);
        } catch (IOException e) {
            return null;
        }

        try (FileOutputStream fos = new FileOutputStream(file)) {

            fos.write(image);
            return file;

        } catch (IOException e) {
            return null;
        }
    }

    private static File createImageFile(Activity activity) throws IOException {
        if (activity == null) return null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }


    public static File getResizedFileFromByteArray(byte[] bytes, int width, int height, int previewFormat, Activity activity) {
        File file = FileUtil.writeByteArrayToFile(bytes, width, height, previewFormat, activity);
        return Utility.getResizedImageFileUsingSampleSizeTemp(file, true);
    }

    private static File writeByteArrayToFile(byte[] bytes, int width, int height, int imageFormat, Activity activity) {
        YuvImage yuvImage = new YuvImage(bytes, imageFormat, width, height, null);
        File file;
        try {
            file = createImageFile(activity);
        } catch (IOException e) {
            return null;
        }

        try (FileOutputStream fos = new FileOutputStream(file)) {
            yuvImage.compressToJpeg(new Rect(0, 0, width, height), 100, fos);
        } catch (Exception e) {
            Crashlytics.logException(e);
            Crashlytics.log("File cannot be created!");
        }
        return file;
    }

    public static void deleteAllDocuments(@NonNull Context context) {
        File root = new File(getDocumentRootPath(context));
        if (root.exists())
            deleteRecursive(root);
    }

    public static boolean deleteFile(File file) {
        try {
            if (file.exists())
                return file.delete();
            return true;
        } catch (Exception e) {
            Crashlytics.logException(e);
            return false;
        }
    }

    private static boolean createNewFile(File file) {
        try {
            if (!file.exists())
                return file.createNewFile();
            return true;
        } catch (IOException e) {
            Crashlytics.logException(e);
            return false;
        }
    }

    public static boolean createDirectory(File file) {
        try {
            if (!file.exists())
                return file.mkdirs();
            return true;
        } catch (Exception e) {
            Crashlytics.logException(e);
            return false;
        }
    }
}
