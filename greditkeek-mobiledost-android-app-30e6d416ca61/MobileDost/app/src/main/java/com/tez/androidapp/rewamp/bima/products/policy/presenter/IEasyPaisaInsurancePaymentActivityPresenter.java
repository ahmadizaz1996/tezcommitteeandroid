package com.tez.androidapp.rewamp.bima.products.policy.presenter;


import android.location.Location;

import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;

public interface IEasyPaisaInsurancePaymentActivityPresenter {
    void initiatePurchasePolicy(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest);

    void purchasePolicy(String pin, Location location);
}
