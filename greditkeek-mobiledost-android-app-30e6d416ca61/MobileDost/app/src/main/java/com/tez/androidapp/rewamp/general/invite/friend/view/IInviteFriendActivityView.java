package com.tez.androidapp.rewamp.general.invite.friend.view;

import android.content.Intent;

import com.tez.androidapp.app.base.ui.IBaseView;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

public interface IInviteFriendActivityView extends IBaseView {
    void setOnClickListnerToBtOnClickListner(DoubleTapSafeOnClickListener doubleTapSafeOnClickListener);

    void startActivityFromIntent(Intent intent);

    void setBtContinueEnabled(boolean enabled);

    void setVisibillityForTvHeading(int visibillity);

    void setVisibillityForTezLinearLayoutShimmer(int visibillity);

    void setVisibillityForTvDescription(int visibillity);

    void setVisibillityForIvLogo(int visibillity);

    void setVisibillityForBtContinue(int visibillity);
}
