package com.tez.androidapp.rewamp.committee.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.tez.androidapp.app.base.request.BaseRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeLeaveRequest extends BaseRequest {

    public static final String METHOD_NAME = "http://3.21.127.35/api/v1/committee/leave/{" + Params.ID + "}";

    public static final class Params {
        public static final String ID = "id";

        private Params() {
        }
    }

}
