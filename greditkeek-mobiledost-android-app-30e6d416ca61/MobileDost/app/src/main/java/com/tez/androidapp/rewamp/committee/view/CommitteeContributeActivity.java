package com.tez.androidapp.rewamp.committee.view;

import androidx.annotation.BinderThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.widgets.AddWalletCardView;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.presenter.CommitteeContributeActivityPresenter;
import com.tez.androidapp.rewamp.committee.presenter.CommitteeInviteActivityPresenter;
import com.tez.androidapp.rewamp.committee.response.CommitteeWalletResponse;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeContributeSummaryActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeWalletVerificationActivityRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.dashboard.view.DashboardActivity;

import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import static com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter.MY_COMMITTEE_RESPONSE;

public class CommitteeContributeActivity extends BaseActivity implements DoubleTapSafeOnClickListener, ICommitteeContibueActivityView, AddWalletCardView.ChangeWalletListener {


    @BindView(R.id.tvAmount)
    TezTextView tvAmount;

    @BindView(R.id.tvTotalAmount)
    TezTextView tvTotalAmount;

    @BindView(R.id.tvYourInstallment)
    TezTextView tvYourInstallment;

    @BindView(R.id.tvDateofInstallment)
    TezTextView tvDateofInstallment;

    @BindView(R.id.tvReceivingDateValue)
    TezTextView tvReceivingDateValue;

    @BindView(R.id.tvRoundNoValue)
    TezTextView tvRoundNoValue;

    @BindView(R.id.cvMobileWallet)
    AddWalletCardView cvMobileWallet;

    @BindView(R.id.btContinue)
    TezButton btContinue;


    private final CommitteeContributeActivityPresenter committeeContributeActivityPresenter;
    private MyCommitteeResponse myCommitteeResponse;
    private CommitteeWalletResponse committeeWalletResponse;


    public CommitteeContributeActivity() {
        committeeContributeActivityPresenter = new CommitteeContributeActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee_contribute);
        ViewBinder.bind(this);
        fetchExtras();
        setListeners();
        getWallets();

    }

    private void getWallets() {
        try {
            committeeContributeActivityPresenter.getWallets();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListeners() {
        try {
            btContinue.setDoubleTapSafeOnClickListener(this);
            cvMobileWallet.setChangeWalletListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchExtras() {
        try {
            Intent intent = getIntent();
            if (intent != null && intent.getExtras() != null &&
                    intent.getExtras().containsKey(MY_COMMITTEE_RESPONSE)) {
                myCommitteeResponse = intent.getParcelableExtra(MY_COMMITTEE_RESPONSE);

                MyCommitteeResponse.Committee committee = myCommitteeResponse.committeeList.get(0);
                tvAmount.setText(committee.getAmount() + "");
                tvTotalAmount.setText(committee.getAmount() + "");
                tvYourInstallment.setText(committee.getInstallment() + "");
                tvDateofInstallment.setText(committee.getDueDate().split("T")[0]);
                tvReceivingDateValue.setText(committee.getReceivingDate().split("T")[0]);
                int roundNo = -1;
                for (MyCommitteeResponse.Committee.Member member : myCommitteeResponse.committeeList.get(0).getMembers()) {
                    if (MDPreferenceManager.getUser().getId().equalsIgnoreCase(String.valueOf(member.userId))) {
                        roundNo = member.getRound();
                    }
                }
                tvRoundNoValue.setText(roundNo + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {
            case R.id.btContinue:
                CommitteeWalletVerificationActivityRouter.createInstance().setDependenciesAndRoute(this, myCommitteeResponse, committeeWalletResponse);
                break;
        }
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void onGetWallets(CommitteeWalletResponse committeeWalletResponse) {
        try {
            this.committeeWalletResponse = committeeWalletResponse;
            if (committeeWalletResponse != null && committeeWalletResponse.wallets.size() > 0) {
                btContinue.setVisibility(View.VISIBLE);
                cvMobileWallet.setWallet(committeeWalletResponse.wallets.get(0));
            } else {
                btContinue.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String getScreenName() {
        return CommitteeContributeActivity.class.getSimpleName();
    }


    @Override
    public void onChangeWallet() {
        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this, true);
    }

    @Override
    public void onAddWallet() {
        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this, true);
    }

}