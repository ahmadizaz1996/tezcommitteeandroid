package com.tez.androidapp.rewamp.bima.products.model;

import java.io.Serializable;

public class InsuranceProduct implements Serializable {

    private Integer id;
    private String title;
    private String policyTagLine;
    private boolean productPurchased;
    private boolean disabled;
    private Integer policyId;

    public InsuranceProduct(Integer id, String title, String policyTagLine, boolean disabled) {
        this.id = id;
        this.title = title;
        this.policyTagLine = policyTagLine;
        this.disabled = disabled;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPolicyTagLine() {
        return policyTagLine;
    }

    public boolean isProductPurchased() {
        return productPurchased;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public Integer getPolicyId() {
        return policyId;
    }
}
