package com.tez.androidapp.rewamp.committee.view;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeCompletionActivityView extends IBaseView {
    void showLoader();

    void hideLoader();

    void onCommiteeCreation(CommitteeCreateResponse committeeCreateResponse);
}
