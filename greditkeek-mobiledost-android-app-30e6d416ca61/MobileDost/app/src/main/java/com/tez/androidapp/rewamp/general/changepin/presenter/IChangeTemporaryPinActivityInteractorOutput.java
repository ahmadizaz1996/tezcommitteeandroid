package com.tez.androidapp.rewamp.general.changepin.presenter;

import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.ChangeTemporaryPinCallback;

public interface IChangeTemporaryPinActivityInteractorOutput extends ChangeTemporaryPinCallback {
}
