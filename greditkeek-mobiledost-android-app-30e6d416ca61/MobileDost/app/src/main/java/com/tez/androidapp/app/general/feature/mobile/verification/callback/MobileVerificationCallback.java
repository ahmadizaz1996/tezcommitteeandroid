package com.tez.androidapp.app.general.feature.mobile.verification.callback;

/**
 * Created by FARHAN DHANANI on 2/25/2019.
 */
public interface MobileVerificationCallback {

    void onMobileNumberVerificationSuccess();

    void onMobileNumberVerificatoinFailure(int errorCode, String message);
}
