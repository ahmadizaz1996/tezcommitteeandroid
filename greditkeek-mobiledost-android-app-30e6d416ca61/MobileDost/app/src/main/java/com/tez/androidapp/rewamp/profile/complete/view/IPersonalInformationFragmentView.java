package com.tez.androidapp.rewamp.profile.complete.view;

import com.tez.androidapp.rewamp.profile.complete.QuestionDropDownAdapter;
import com.tez.androidapp.rewamp.profile.view.IProfileCompletedView;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

public interface IPersonalInformationFragmentView extends IProfileCompletedView {
    void setVisibillityToShimmer(int visibillity);

    void userProfileCompleted();

    void userProfileIsNotCompleted();

    void setVisibillityToTezCardView(int visibillity);

    void setAdapterToRecyclerView(QuestionDropDownAdapter questionDropDownAdapter);

    void setVisibillityToUploadImageCardView(int visibillity);

    void clearUploadImageCvData();

    void setTextToUploadImageCardView(String text);

    void userHasAlreadyUploadedProofOfWork();

    void setOnClickListenerToUploadImageCardView(DoubleTapSafeOnClickListener doubleTapSafeOnClickListener);

    void startCameraForUploadPicture(String questionRefName);
}
