package com.tez.androidapp.rewamp.committee.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tez.androidapp.app.base.request.BaseRequest;

import java.io.Serializable;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeCreateRequest extends BaseRequest {
        public static final String METHOD_NAME = "http://3.21.127.35/api/v1/committee/add";
//    public static final String METHOD_NAME = "add";

    //    public String title;
//    public int tenure;
//    public int memberSize;
//    public int frequency;
//    public String startDate;
//    public int totalAmount;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("tenure")
    @Expose
    private int tenure;
    @SerializedName("memberSize")
    @Expose
    private int memberSize;
    @SerializedName("frequency")
    @Expose
    private int frequency;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("totalAmount")
    @Expose
    private int totalAmount;

    public CommitteeCreateRequest(String title, int tenure, int memberSize, int frequency, String startDate, int totalAmount) {
        this.title = title;
        this.tenure = tenure;
        this.memberSize = memberSize;
        this.frequency = frequency;
        this.startDate = startDate;
        this.totalAmount = totalAmount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTenure() {
        return tenure;
    }

    public void setTenure(int tenure) {
        this.tenure = tenure;
    }

    public int getMemberSize() {
        return memberSize;
    }

    public void setMemberSize(int memberSize) {
        this.memberSize = memberSize;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }


//    @Override
//    public String toString() {
//        return "committeeadd{" +
//                "frequency='" + frequency + '\'' +
//                ", memberSize='" + memberSize + '\'' +
//                ", tenure='" + tenure + '\'' +
//                ", title='" + title + '\'' +
//                ", totalAmount='" + totalAmount + '\'' +
//                ", startDate='" + startDate +
//                '}';
//    }
}
