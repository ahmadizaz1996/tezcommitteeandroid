package com.tez.androidapp.rewamp.general.suspend.account;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public class FeedbackRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/suspend/feedback";

    private String comment;

    public FeedbackRequest(String comment) {
        this.comment = comment;
    }
}
