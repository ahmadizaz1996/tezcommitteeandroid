package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.User;

/**
 * Created by Rehman Murad Ali on 9/8/2017.
 */

public class GetUserProfileResponse extends BaseResponse {
    User userProfile;

    public User getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(User userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public String toString() {
        return "GetUserProfileResponse{" +
                "userProfile=" + userProfile +
                '}';
    }
}
