package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.MyCommitteeActivity;
import com.tez.androidapp.rewamp.committee.view.MyCommitteeMembersActivity;

import java.util.ArrayList;
import java.util.List;

public class MyCommitteeMembersActivityRouter extends BaseActivityRouter {


    public static final String INVITEES_LIST = "INVITEES_LIST";
    public static final String MEMBERS_LIST = "MEMBERS_LIST";

    public static MyCommitteeMembersActivityRouter createInstance() {
        return new MyCommitteeMembersActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, List<MyCommitteeResponse.Committee.Member> inviteesList) {
        Intent intent = createIntent(from);
        intent.putParcelableArrayListExtra(MEMBERS_LIST, new ArrayList<>(inviteesList));
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, MyCommitteeMembersActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

}
