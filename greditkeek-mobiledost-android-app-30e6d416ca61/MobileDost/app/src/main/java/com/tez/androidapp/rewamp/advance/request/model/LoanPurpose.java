package com.tez.androidapp.rewamp.advance.request.model;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

public class LoanPurpose {

    private int id;

    @DrawableRes
    private int iconRes;

    @StringRes
    private int titleRes;

    public LoanPurpose(int id, int iconRes, int titleRes) {
        this.id = id;
        this.iconRes = iconRes;
        this.titleRes = titleRes;
    }

    public int getId() {
        return id;
    }

    public int getIconRes() {
        return iconRes;
    }

    public int getTitleRes() {
        return titleRes;
    }
}
