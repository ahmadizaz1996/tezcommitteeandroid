package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeActivity;
import com.tez.androidapp.rewamp.committee.view.CommitteeContributeActivity;
import com.tez.androidapp.rewamp.committee.view.CommitteeSummaryActivity;

import static com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter.MY_COMMITTEE_RESPONSE;

public class CommitteeContributeActivityRouter extends BaseActivityRouter {


    public static CommitteeContributeActivityRouter createInstance() {
        return new CommitteeContributeActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, MyCommitteeResponse committeeResponse) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putParcelable(MY_COMMITTEE_RESPONSE, committeeResponse);
        intent.putExtras(bundle);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeContributeActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

}
