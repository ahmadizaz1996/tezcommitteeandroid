package com.tez.androidapp.commons.utils.cnic.detection.contracts;

import android.app.Activity;
import androidx.appcompat.app.AppCompatActivity;

public interface CnicScannerPresenter {

    void setUpDisplayAndCamera(String detectObject);

    void clickPicture(Activity activity);

    boolean isCameraAndStorageAllowed(AppCompatActivity appCompatActivity);

    void handlePermissionResult(int requestCode, String[] permissions, int[] grantResults);

    void resumeCamera();

    void setIntentForFaceAndFinish();
}
