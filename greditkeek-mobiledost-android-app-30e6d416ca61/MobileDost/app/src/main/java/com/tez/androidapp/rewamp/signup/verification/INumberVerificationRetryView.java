package com.tez.androidapp.rewamp.signup.verification;

import android.content.Context;

import com.tez.androidapp.app.base.ui.IBaseView;

/**
 * Created by FARHAN DHANANI on 6/5/2019.
 */
public interface INumberVerificationRetryView extends VerificationCallback, IBaseView {
    void setTextToTvVerifyYourNumber(String text);

    boolean canStartNumberVerification();

    boolean isNetworkConnected();

    void preliminaryNumberVerificationWork();

    void setTextToTvTimer(String text);
    Context getContext();

    void retryState();

    void setVisibiltyForTvTimer(int visibilty);
}
