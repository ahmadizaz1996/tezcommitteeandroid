package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 9/8/2017.
 */

public class UpdateMobileNumberVerifyOTPRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/user/account/otp/verify/{" + Params.OTP + "}/{" + Params.CNIC + "}";

    public static final class Params {
        public static final String OTP = "otp";
        public static final String CNIC = "cnic";

        private Params() {
        }
    }
}
