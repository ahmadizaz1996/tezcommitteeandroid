package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.interactor.CommitteeActivityInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeLoginRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeMetaDataResponse;
import com.tez.androidapp.rewamp.committee.response.CommittteeLoginResponse;
import com.tez.androidapp.rewamp.committee.view.ICommitteeActivityView;

import java.util.List;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class CommitteeActivityPresenter implements ICommitteeActivityPresenter, ICommitteeActivityInteractorOutput {


    private final ICommitteeActivityView iCommitteeActivityView;
    private final CommitteeActivityInteractor committeeActivityInteractor;

    public CommitteeActivityPresenter(ICommitteeActivityView iCommitteeActivityView) {
        this.iCommitteeActivityView = iCommitteeActivityView;
        committeeActivityInteractor = new CommitteeActivityInteractor(this);
    }

    @Override
    public void onCommitteeMetadataSuccess(CommitteeMetaDataResponse committeeMetaDataResponse) {
        iCommitteeActivityView.hideLoader();
        iCommitteeActivityView.storeMetadata(committeeMetaDataResponse);
    }

    @Override
    public void onCommitteeMetadataFailure(int errorCode, String message) {
        iCommitteeActivityView.hideLoader();
        iCommitteeActivityView.showError(errorCode,
                (dialog, which) -> iCommitteeActivityView.finishActivity());
    }

    @Override
    public void getCommitteeMetadata() {
        iCommitteeActivityView.showLoader();
        committeeActivityInteractor.getCommitteeMetada();
    }

    @Override
    public void checkInvites(String mobileNumber) {
//        iCommitteeActivityView.showLoader();
        committeeActivityInteractor.checkInvites(mobileNumber);
    }

    @Override
    public void onCommitteeLoginSuccess(CommittteeLoginResponse committteeLoginResponse) {
        iCommitteeActivityView.committeeLoginSuccess(committteeLoginResponse);
    }

    @Override
    public void onCommitteeLoginFailure(int errorCode, String message) {
        iCommitteeActivityView.showError(errorCode,
                (dialog, which) -> iCommitteeActivityView.finishActivity());
    }

    @Override
    public void onCommitteeCheckInvitesSuccess(List<CommitteeCheckInvitesResponse> committeeCheckInvitesResponse) {
        iCommitteeActivityView.onCheckInvitesSuccess(committeeCheckInvitesResponse);
    }

    @Override
    public void onCommitteeCheckInvitesFailure(int errorCode, String message) {

    }
}
