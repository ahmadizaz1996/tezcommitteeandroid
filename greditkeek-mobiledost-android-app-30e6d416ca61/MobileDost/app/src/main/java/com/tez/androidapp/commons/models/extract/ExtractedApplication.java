package com.tez.androidapp.commons.models.extract;

import android.content.pm.ApplicationInfo;

/**
 * Created  on 12/8/2016.
 */

public class ExtractedApplication {

    String packageName;
    boolean enabled;
    int uid;

    public ExtractedApplication(String packageName, boolean enabled, int uid) {
        this.packageName = packageName;
        this.enabled = enabled;
        this.uid = uid;
    }

    public ExtractedApplication(ApplicationInfo applicationInfo) {
        this.packageName = applicationInfo.packageName;
        this.enabled = applicationInfo.enabled;
        this.uid = applicationInfo.uid;
    }
}
