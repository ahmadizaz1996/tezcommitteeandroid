package com.tez.androidapp.rewamp.committee.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.interactor.JoinCommitteeVerificationActivityInteractor;
import com.tez.androidapp.rewamp.committee.presenter.JoinCommitteeInvitationActivityPresenter;
import com.tez.androidapp.rewamp.committee.response.GetInvitedPackageResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class JoinCommitteeInvitationActivity extends BaseActivity implements IJoinCommitteeInvitationActivityView, DoubleTapSafeOnClickListener {


    @BindView(R.id.tvAmount)
    TezTextView tvAmount;

    @BindView(R.id.availPackageButton)
    TezButton viewDetailsBtn;

    @BindView(R.id.tvTitleAmount)
    TezTextView tvTitleAmount;

    @BindView(R.id.tvMembers)
    TezTextView tvMembers;

    @BindView(R.id.tvDuration)
    TezTextView tvDuration;

    @BindView(R.id.tvInstallment)
    TezTextView tvInstallment;

    private final JoinCommitteeInvitationActivityPresenter joinCommitteeInvitationActivityPresenter;
    private GetInvitedPackageResponse.CommitteeList committeePackage;

    public JoinCommitteeInvitationActivity() {
        this.joinCommitteeInvitationActivityPresenter = new JoinCommitteeInvitationActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_committee_invitation);
        ViewBinder.bind(this);
        try {
            tvTitleAmount.setVisibility(View.GONE);
            viewDetailsBtn.setText(R.string.view_details);
            viewDetailsBtn.setDoubleTapSafeOnClickListener(this);
            joinCommitteeInvitationActivityPresenter.getInvitedPackage(MDPreferenceManager.getUser().getMobileNumber());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String getScreenName() {
        return JoinCommitteeVerificationActivityInteractor.class.getSimpleName();
    }

    @Override
    public void showLoader() {
        showTezLoader();
    }

    @Override
    public void hideLoader() {
        dismissTezLoader();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onGetInvitedPackageSuccess(GetInvitedPackageResponse getInvitedPackageResponse) {
        try {
            if (getInvitedPackageResponse != null && getInvitedPackageResponse.getCommitteeList().size() > 0) {
                GetInvitedPackageResponse.CommitteeList committeePackage = getInvitedPackageResponse.getCommitteeList().get(0);
                this.committeePackage = committeePackage;
                tvAmount.setText("RS. " + committeePackage.getAmount());
                tvInstallment.setText("RS. " + committeePackage.getInstallment());
                tvDuration.setText(committeePackage.getTenor() + " " + getString(R.string.months));
                tvMembers.setText(committeePackage.getMemberSize() + " " + getString(R.string.members));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {
            case R.id.availPackageButton:
                CommitteeSummaryActivityRouter.createInstance().setDependenciesAndRoute(this, committeePackage);
                break;
        }
    }
}