package com.tez.androidapp.rewamp.general.suspend.account.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.general.suspend.account.presenter.ISuspendAccountFeedbackActivityPresenter;
import com.tez.androidapp.rewamp.general.suspend.account.presenter.SuspendAccountFeedbackActivityPresenter;
import com.tez.androidapp.rewamp.signup.router.NewIntroActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;

public class SuspendAccountFeedbackActivity extends PlaybackActivity implements ISuspendAccountFeedbackActivityView {

    private final ISuspendAccountFeedbackActivityPresenter iSuspendAccountFeedbackActivityPresenter;

    @BindView(R.id.btSubmit)
    private TezButton btSubmit;

    @BindView(R.id.etFeedback)
    private TezEditTextView etFeedback;

    public SuspendAccountFeedbackActivity() {
        iSuspendAccountFeedbackActivityPresenter = new SuspendAccountFeedbackActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suspend_account_feedback);
        this.updateToolbar(findViewById(R.id.toolbar));
        init();
    }

    private void init() {
        this.btSubmit.setDoubleTapSafeOnClickListener(view -> onClickSubmit());
    }

    private void onClickSubmit() {
        iSuspendAccountFeedbackActivityPresenter.sendUserFeedBackToServer(etFeedback.getValueText());
    }

    @Override
    public void setBtSubmitEnabled(boolean enabled) {
        this.btSubmit.setEnabled(enabled);
    }

    @Override
    public void takeUserToIntroScreen() {
        Utility.logoutUser();
        NewIntroActivityRouter.createInstance().setDependenciesAndRoute(this, Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onBackPressed() {
        takeUserToIntroScreen();
    }

    @OnClick(R.id.tvSkip)
    private void closeApp() {
        takeUserToIntroScreen();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    protected String getScreenName() {
        return "SuspendAccountFeedbackActivity";
    }
}
