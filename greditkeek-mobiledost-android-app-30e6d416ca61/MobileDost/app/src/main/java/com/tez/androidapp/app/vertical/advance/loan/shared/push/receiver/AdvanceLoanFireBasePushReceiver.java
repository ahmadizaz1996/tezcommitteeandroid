package com.tez.androidapp.app.vertical.advance.loan.shared.push.receiver;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;

import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.push.reciever.BasePushNotificationReceiver;
import com.tez.androidapp.app.general.feature.authentication.signin.callbacks.UserLogoutCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.receivers.PushNotificationSender;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.advance.AdvanceStatus;
import com.tez.androidapp.rewamp.signup.NewIntroActivity;
import com.tez.androidapp.rewamp.signup.WelcomeBackActivity;

import net.tez.fragment.util.optional.Optional;

import java.util.Map;

/**
 * Created by farhan on 4/19/18.
 */

public class AdvanceLoanFireBasePushReceiver extends BasePushNotificationReceiver {

    public AdvanceLoanFireBasePushReceiver(Map<String, String> notificationMap) {
        super(notificationMap);
    }

    @Override
    public void process() {

        String type = getNotificationMap().get(PushNotificationConstants.KEY_TYPE);

        if (type != null) {

            switch (type) {

                case PushNotificationConstants.LOAN_LIMIT_STATUS:
                    this.notificationTypeLoanLimit();
                    break;

                case PushNotificationConstants.LOAN_DISBURSEMENT_STATUS:
                    this.notificationTypeLoanDisbursementStatus();
                    break;

                case PushNotificationConstants.SESSION_EXPIRED:
                    this.notificationTypeSessionExpired();
                    break;

                case PushNotificationConstants.LOAN_REPAYMENT_STATUS:
                    this.notificationTypeLoanRepayment();
                    break;

                case PushNotificationConstants.CNIC_VERIFICATION_STATUS:
                    this.cinicVerification();
                    break;

                default:
                    sendNotification();
            }
        }
    }

    private void notificationTypeLoanLimit() {
        String message = getNotificationMap().get(PushNotificationConstants.KEY_MESSAGE_TEXT);
        String status = getNotificationMap().get(PushNotificationConstants.LOAN_LIMIT_STATUS);
        Intent intent = new Intent(PushNotificationConstants.BROADCAST_TYPE_LOAN_LIMIT);
        if (status != null && AdvanceStatus.valueOf(status) == AdvanceStatus.LIMIT_CREATED)
            intent.putExtra(PushNotificationConstants.KEY_ROUTE_TO, PushNotificationConstants.LIMIT_ASSIGNED_ACTIVITY);
        intent.putExtra(PushNotificationConstants.KEY_MESSAGE_TEXT, message);
        intent.putExtra(PushNotificationConstants.LOAN_LIMIT_STATUS, status);
        this.sendOrderedBroadcast(intent, new PushNotificationSender(), message);
    }

    private void notificationTypeLoanDisbursementStatus() {
        String message = getNotificationMap().get(PushNotificationConstants.KEY_MESSAGE_TEXT);
        String status = getNotificationMap().get(PushNotificationConstants.LOAN_DISBURSEMENT_STATUS);
        Intent intent = new Intent(PushNotificationConstants.BROADCAST_TYPE_LOAN_DISBURSEMENT);
        if (status != null && AdvanceStatus.valueOf(status) == AdvanceStatus.LOAN_DISBURSED)
            intent.putExtra(PushNotificationConstants.KEY_ROUTE_TO, PushNotificationConstants.LOAN_RECEIPT_ACTIVITY);
        intent.putExtra(PushNotificationConstants.KEY_MESSAGE_TEXT, message);
        intent.putExtra(PushNotificationConstants.LOAN_DISBURSEMENT_STATUS, status);
        this.sendOrderedBroadcast(intent, new PushNotificationSender(), message);
    }

    private void notificationTypeLoanRepayment() {
        String message = getNotificationMap().get(PushNotificationConstants.KEY_MESSAGE_TEXT);
        Intent intent = new Intent(PushNotificationConstants.BROADCAST_TYPE_LOAN_REPAYMENT);
        intent.putExtra(PushNotificationConstants.KEY_MESSAGE_TEXT, message);
        this.sendOrderedBroadcast(intent, new PushNotificationSender(), message);
    }


    private void notificationTypeSessionExpired() {
        if (Utility.doesUserExist()) {
            Optional.ifPresent((NotificationManager) MobileDostApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE),
                    NotificationManager::cancelAll);
            String retakePrincipalName = getNotificationMap().get(Constants.RETAKE_PRINCIPAL_NAME);
            if (retakePrincipalName == null
                    || retakePrincipalName.equalsIgnoreCase("false")) {
                startWelcomeBackActivity();
            } else {
                Intent intent = new Intent(MobileDostApplication.getInstance(), NewIntroActivity.class);
                logout(intent);
            }
        }
    }

    private void startWelcomeBackActivity() {
        MDPreferenceManager.removeAccessToken();
        Intent intent = new Intent(MobileDostApplication.getInstance(), WelcomeBackActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setClass(MobileDostApplication.getInstance().getApplicationContext(), WelcomeBackActivity.class);
        MobileDostApplication.getInstance().startActivity(intent);
    }

    private void logout(Intent intent) {
        UserAuthCloudDataStore.getInstance().logout(new UserLogoutCallback() {
            @Override
            public void onUserLogoutSuccess() {
                openIntroActivity(intent);
            }

            @Override
            public void onUserLogoutFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    logout(intent);
                else
                    openIntroActivity(intent);
            }
        });
    }

    private void openIntroActivity(Intent intent) {
//        MDPreferenceManager.setAccessToken(null);
        Optional.ifPresent(MDPreferenceManager.getUser(), user -> {
            Utility.logoutUser();
            MDPreferenceManager.clearAllPreferences();
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setClass(MobileDostApplication.getInstance().getApplicationContext(), NewIntroActivity.class);
            MobileDostApplication.getInstance().startActivity(intent);
        });
    }

    private void cinicVerification() {
        if (MDPreferenceManager.getUser() == null)
            return;
        Intent intent = new Intent(PushNotificationConstants.BROADCAST_TYPE_CNIC_VERIFICATION);
        String message = getNotificationMap().get(PushNotificationConstants.KEY_MESSAGE_TEXT);
        String cnicVerificationStatus = getNotificationMap().get(PushNotificationConstants.CNIC_VERIFICATION_STATUS);
        if (cnicVerificationStatus != null && !cnicVerificationStatus.equals(PushNotificationConstants.APPROVED))
            intent.putExtra(PushNotificationConstants.KEY_ROUTE_TO, PushNotificationConstants.PROFILE_TRUST_ACTIVITY);
        intent.putExtra(PushNotificationConstants.KEY_MESSAGE_TEXT, message);
        this.sendOrderedBroadcast(intent, new PushNotificationSender(), message);
    }
}
