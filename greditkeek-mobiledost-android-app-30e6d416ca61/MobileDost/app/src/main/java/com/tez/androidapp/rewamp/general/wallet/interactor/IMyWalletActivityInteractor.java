package com.tez.androidapp.rewamp.general.wallet.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request.AddWalletRequest;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;

public interface IMyWalletActivityInteractor extends IBaseInteractor {

    void getAllWallets();

    void setDefaultWallet(@NonNull Wallet wallet);

    void deleteWallet(int mobileAccountId);
}
