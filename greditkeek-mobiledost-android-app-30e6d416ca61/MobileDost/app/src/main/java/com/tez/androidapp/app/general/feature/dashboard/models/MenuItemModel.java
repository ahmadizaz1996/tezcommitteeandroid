package com.tez.androidapp.app.general.feature.dashboard.models;

/**
 * Created by Venturedive on 5/17/2017.
 */

public class MenuItemModel {

    private boolean isOpen = false;
    private String iconName = "";
    private int iconImg = -1; // menu icon resource id
    private int id = -1;

    public MenuItemModel(String iconName, int iconImg, int id) {
        this.iconName = iconName;
        this.iconImg = iconImg;
        this.id = id;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIconName() {
        return iconName;
    }

    public int getIconImg() {
        return iconImg;
    }

}