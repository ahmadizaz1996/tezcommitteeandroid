package com.tez.androidapp.rewamp.committee.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.tez.androidapp.app.base.request.BaseRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeInviteRequest extends BaseRequest implements Parcelable {

        public static final String METHOD_NAME = "http://3.21.127.35/api/v1/committee/invite/send";
//    public static final String METHOD_NAME = "invite/send";


    public int committeeId;
    public List<InvitesRequest> invitesRequests = new ArrayList<>();


    public int getCommitteeId() {
        return committeeId;
    }

    public void setCommitteeId(int committeeId) {
        this.committeeId = committeeId;
    }

    public List<InvitesRequest> getInvitesRequests() {
        return invitesRequests;
    }

    public void setInvitesRequests(List<InvitesRequest> invitesRequests) {
        this.invitesRequests = invitesRequests;
    }

    protected CommitteeInviteRequest(Parcel in) {
        committeeId = in.readInt();
        in.readList(invitesRequests, getClass().getClassLoader());
    }
    public CommitteeInviteRequest() {
    }

    public static final Creator<CommitteeInviteRequest> CREATOR = new Creator<CommitteeInviteRequest>() {
        @Override
        public CommitteeInviteRequest createFromParcel(Parcel in) {
            return new CommitteeInviteRequest(in);
        }

        @Override
        public CommitteeInviteRequest[] newArray(int size) {
            return new CommitteeInviteRequest[size];
        }

    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(committeeId);
        parcel.writeList(invitesRequests);
    }


    public static class InvitesRequest implements Parcelable{
        public String contactNumber;
        public String email;

        public InvitesRequest(){}


        protected InvitesRequest(Parcel in) {
            contactNumber = in.readString();
            email = in.readString();
        }

        public static final Creator<InvitesRequest> CREATOR = new Creator<InvitesRequest>() {
            @Override
            public InvitesRequest createFromParcel(Parcel in) {
                return new InvitesRequest(in);
            }

            @Override
            public InvitesRequest[] newArray(int size) {
                return new InvitesRequest[size];
            }
        };

        public String getContactNumber() {
            return contactNumber;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(contactNumber);
            parcel.writeString(email);
        }
    }

/*
        public String contactNumber;

    public CommitteeInviteRequest(Parcel in) {
        contactNumber = in.readString();
    }

    public static final Creator<CommitteeInviteRequest> CREATOR = new Creator<CommitteeInviteRequest>() {
        @Override
        public CommitteeInviteRequest createFromParcel(Parcel in) {
            return new CommitteeInviteRequest(in);
        }

        @Override
        public CommitteeInviteRequest[] newArray(int size) {
            return new CommitteeInviteRequest[size];
        }
    };

    public CommitteeInviteRequest() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(contactNumber);
    }*/
//        public String email;
//    }

}
