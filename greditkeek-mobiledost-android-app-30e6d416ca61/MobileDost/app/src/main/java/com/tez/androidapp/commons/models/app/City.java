package com.tez.androidapp.commons.models.app;

import androidx.annotation.NonNull;

import java.io.Serializable;

/**
 * Created  on 2/9/2017.
 */

public class City implements Comparable<City>, Serializable {

    private int cityId;
    private String cityName;

    public City(int cityId, String cityName) {
        this.cityId = cityId;
        this.cityName = cityName;
    }

    public City(String cityName){
        this.cityName = cityName;
    }

    public City(com.tez.androidapp.commons.location.models.network.City city) {
        this.cityId = city.getCityId();
        this.cityName = city.getCityName();
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public int compareTo(@NonNull City o) {
        return String.CASE_INSENSITIVE_ORDER.compare(this.getCityName(), o.getCityName());
    }

    @Override
    public String toString() {
        return cityName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if(o instanceof City){
            City city = (City) o;
            return cityName.equals(city.cityName);
        }

        if(o instanceof String){
            return cityName.equals(o);
        }
        return false;
    }



    @Override
    public int hashCode() {
        return cityName.hashCode();
    }
}
