package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeSendMessageRequest;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeGroupChatInteractor extends IBaseInteractor {
    void getMessages(String committeeId);
    void sendMessage(CommitteeSendMessageRequest committeeSendMessageRequest);
}
