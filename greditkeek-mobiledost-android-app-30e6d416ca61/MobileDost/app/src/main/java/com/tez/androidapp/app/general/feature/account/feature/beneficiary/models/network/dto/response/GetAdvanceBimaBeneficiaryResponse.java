package com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.BeneficiaryDetails;

/**
 * Created  on 8/25/2017.
 */

public class GetAdvanceBimaBeneficiaryResponse extends BaseResponse {

    BeneficiaryDetails beneficiary;

    public BeneficiaryDetails getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(BeneficiaryDetails beneficiary) {
        this.beneficiary = beneficiary;
    }
}


