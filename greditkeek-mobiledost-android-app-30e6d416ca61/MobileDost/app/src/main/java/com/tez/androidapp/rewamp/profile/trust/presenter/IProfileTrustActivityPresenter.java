package com.tez.androidapp.rewamp.profile.trust.presenter;

import com.tez.androidapp.rewamp.profile.trust.view.IProfileTrustActivityView;

public interface IProfileTrustActivityPresenter {
    void getCnicUploads();
}
