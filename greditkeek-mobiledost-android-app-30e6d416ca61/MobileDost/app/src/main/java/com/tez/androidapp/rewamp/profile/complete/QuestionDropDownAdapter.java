package com.tez.androidapp.rewamp.profile.complete;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.google.android.gms.common.util.CollectionUtils;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Answer;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Question;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Rule;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezSpinner;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;

import net.tez.fragment.util.optional.Optional;
import net.tez.logger.library.utils.TextUtil;
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Rehman Murad Ali
 **/
public class QuestionDropDownAdapter extends GenericRecyclerViewAdapter<Question,
        QuestionDropDownAdapter.QuestionDropDownAdapterOnClickCallback,
        QuestionDropDownAdapter.QuestionDropDownViewHolder> {

    private Map<String, AnswerSelected> selectedAnswerMap = new HashMap<>();
    private Map<String, Question> questionMap = new HashMap<>();
    private List<Question> questions;

    public QuestionDropDownAdapter(@NonNull List<Question> questionDropDownList,
                                   @NonNull QuestionDropDownAdapterOnClickCallback callback) {
        super(callback);
        loadDataSet(questionDropDownList, callback);
    }

    private void loadDataSet(List<Question> questions, @NonNull QuestionDropDownAdapterOnClickCallback callback) {
        Optional.ifPresent(questions, (questionList) -> {
            this.questions = questions;
            for (Question question : questionList) {
                questionMap.put(question.getRefName(), question);
            }
            insertFirstElement();
            parseQuestions(0, callback);

        });
    }

    private void parseQuestions(int position, QuestionDropDownAdapterOnClickCallback callback) {
        Optional.ifPresent(questions.get(position), question -> {
            Optional.ifPresent(questions.get(position).getAnswerRefName(), ref -> {
                for (Answer answer : question.getAnswers()) {
                    Optional.doWhen(TextUtil.equals(answer.getRefName(), ref), () -> onAnswerSelected(answer, position, question, callback));
                }
            });
        });
    }

    private void insertFirstElement() {
        if(!CollectionUtils.isEmpty(this.questions))
            if(TextUtil.equals(this.questions.get(0).getRefName(), "Gender")
                    && TextUtil.isEmpty(this.questions.get(0).getAnswerRefName()))
                getItems().add(this.questions.get(0));
    }

    @NonNull
    public List<AnswerSelected> getAnsweredData() {
        List<AnswerSelected> answerSelectedList = new ArrayList<>();
        for (Map.Entry<String, AnswerSelected> entry : selectedAnswerMap.entrySet()) {
            answerSelectedList.add(entry.getValue());
        }
        return answerSelectedList;
    }

    @NonNull
    @Override
    public QuestionDropDownViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_question_dropdown, parent, false);
        return new QuestionDropDownViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull QuestionDropDownViewHolder holder, int position) {
        Question item = get(position);
        holder.onBind(item, getListener(), position);
    }

    private void onAnswerSelected(Answer answer, int position, Question item, QuestionDropDownAdapterOnClickCallback callback) {
        Optional.doWhen(position < getItemCount() - 1 && selectedAnswerMap.containsKey(item.getRefName()),
                () -> updateList(filteredListToReAnswerQuestion(position, answer), callback));
        selectedAnswerMap.put(item.getRefName(), new AnswerSelected(item.getRefName(), answer));
        String refName = answer.getNextQuestionRefName();
        Optional.doWhen(TextUtil.isNotEmpty(refName) && questionMap.containsKey(refName),
                () -> {
                    Question nextQuestion = questionMap.get(refName);
                    addQuestionItem(nextQuestion, callback);
                    parseQuestions(nextQuestion.getIndex(), callback);
                },
                () -> checkRules(item, callback));
    }

    private List<Question> filteredListToReAnswerQuestion(int position, Answer answer) {
        List<Question> questions = getItems().subList(0, position + 1);
        clearPreFilledAnswers(position, getItems());
        questions.get(position).setAnswerRefName(answer.getRefName());
        return questions;
    }

    private void addQuestionItem(@Nullable Question question, QuestionDropDownAdapterOnClickCallback callback) {
        Optional.ifPresent(question, question1 -> {
            List<Answer> answers = question1.getAnswers();
            Optional.doWhen(answers == null || answers.size() == 0,
                    () -> callback.lastQuestionToUploadPictureIsRemaining(question),
                    () -> addQuestion(question, callback));
        });

    }

    private void addQuestion(Question question, QuestionDropDownAdapterOnClickCallback callback) {
        getItems().add(question);
    }

    private void checkRules(Question item, QuestionDropDownAdapterOnClickCallback callback) {
        Optional.doWhen(item.isRulesExist(), () -> {
            boolean[] ruleMatched = new boolean[1];
            for (Rule rule : item.getRules()) {
                Optional.doWhen(selectedAnswerMap.containsKey(rule.getQuestionRefName())
                                && !ruleMatched[0],
                        () -> Optional.ifPresent(selectedAnswerMap.get(rule.getQuestionRefName()),
                                selectedAnswer -> {
                                    Optional.doWhen(TextUtil.equals(selectedAnswer.getAnswerReference(),
                                            rule.getAnswerRefName()) && questionMap.containsKey(rule.getNextQuestionRefName()),
                                            () -> {
                                                ruleMatched[0] = true;
                                                Question nextQuestion = questionMap.get(rule.getNextQuestionRefName());
                                                addQuestionItem(nextQuestion, callback);
                                                parseQuestions(nextQuestion.getIndex(), callback);
                                            });
                                }));
            }
            Optional.doWhen(!ruleMatched[0] && !item.isLastIfNoRuleMatches(), () -> addSubsequentQuestion(item.getIndex() + 1, callback));
            Optional.doWhen(!ruleMatched[0] && item.isLastIfNoRuleMatches(), callback::allQuestionHasBeenAnswered);
        }, () -> addSubsequentQuestion(item.getIndex() + 1, callback));
    }

    private void clearPreFilledAnswers(int startIndex, List<Question> questions) {
        for (int i = startIndex; i < questions.size(); i++) {
            Question question = questions.get(i);
            question.setAnswerRefName(null);
            selectedAnswerMap.remove(question.getRefName());
        }
    }

    private void addSubsequentQuestion(int index, QuestionDropDownAdapterOnClickCallback callback) {
        Optional.doWhen(questions.size() > index,
                () -> addQuestionItem(questions.get(index), callback),
                callback::allQuestionHasBeenAnswered);
    }

    private void updateList(List<Question> questions, QuestionDropDownAdapterOnClickCallback callback) {
        Optional.ifPresent(callback, callback1 -> {
            callback1.clearPreviouslyAskedQuestion();
            setItems(questions);
        });
    }

    class QuestionDropDownViewHolder extends BaseViewHolder<Question, QuestionDropDownAdapterOnClickCallback> {


        @BindView(R.id.sQuestion)
        TezSpinner sQuestion;

        @BindView(R.id.tilQuestion)
        TezTextInputLayout tilQuestion;

        @BindView(R.id.etOther)
        TezEditTextView etOther;

        @BindView(R.id.tilOther)
        TezTextInputLayout tilOther;


        QuestionDropDownViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }


        public void onBind(Question item, @Nullable QuestionDropDownAdapterOnClickCallback callback,
                           int position) {
            tilQuestion.setHint(item.getText());
            loadOptions(item.getAnswers());
            setAnswers(item);
            sQuestion.setOnItemClickListener((parent, view, answerIndex, id) ->
                    onAnswerSelected(item.getAnswers().get(answerIndex), position, item, callback));

        }

        private void onAnswerSelected(Answer answer, int position, Question item, QuestionDropDownAdapterOnClickCallback callback) {
            Optional.doWhen(position < getItemCount() - 1 && selectedAnswerMap.containsKey(item.getRefName()),
                    () -> updateList(filteredListToReAnswerQuestion(position, answer), callback));
            item.setAnswerRefName(answer.getRefName());
            selectedAnswerMap.put(item.getRefName(), new AnswerSelected(item.getRefName(), answer));
            String refName = answer.getNextQuestionRefName();
            Optional.doWhen(TextUtil.isNotEmpty(refName) && questionMap.containsKey(refName),
                    () -> {
                        Question nextQuestion = questionMap.get(refName);
                        addQuestionItem(nextQuestion, callback);
                        parseQuestions(nextQuestion.getIndex(), callback);
                    },
                    () -> checkRules(item, callback));
        }

        private void parseQuestions(int position, QuestionDropDownAdapterOnClickCallback callback) {
            Optional.ifPresent(questions.get(position), question -> {
                Optional.ifPresent(questions.get(position).getAnswerRefName(), ref -> {
                    for (Answer answer : question.getAnswers()) {
                        Optional.doWhen(TextUtil.equals(answer.getRefName(), ref), () -> onAnswerSelected(answer, position, question, callback));
                    }
                });
            });
        }

        private void loadOptions(List<Answer> answers) {
            sQuestion.setAdapter(new ArrayAdapter<>(itemView.getContext(),
                    android.R.layout.simple_spinner_dropdown_item, answers));
        }

        private void setAnswers(Question item) {
            Optional.ifPresent(item.getAnswerRefName(), ref -> {
                for (Answer answer : item.getAnswers()) {
                    if (TextUtil.equals(answer.getRefName(), ref)) {
                        sQuestion.setText(answer.getText());
                    }
                }
            }, () -> sQuestion.setText(null));
        }

        private void updateList(List<Question> questions, QuestionDropDownAdapterOnClickCallback callback) {
            Optional.ifPresent(callback, callback1 -> {
                callback1.clearPreviouslyAskedQuestion();
                setItems(questions);
            });
        }

        private List<Question> filteredListToReAnswerQuestion(int position, Answer answer) {
            List<Question> questions = getItems().subList(0, position + 1);
            clearPreFilledAnswers(position, getItems());
            questions.get(position).setAnswerRefName(answer.getRefName());
            return questions;
        }

        private void clearPreFilledAnswers(int startIndex, List<Question> questions) {
            for (int i = startIndex; i < questions.size(); i++) {
                Question question = questions.get(i);
                question.setAnswerRefName(null);
                selectedAnswerMap.remove(question.getRefName());
            }
        }

        private void checkRules(Question item, QuestionDropDownAdapterOnClickCallback callback) {
            Optional.doWhen(item.isRulesExist(), () -> {
                boolean[] ruleMatched = new boolean[1];
                for (Rule rule : item.getRules()) {
                    Optional.doWhen(selectedAnswerMap.containsKey(rule.getQuestionRefName())
                                    && !ruleMatched[0],
                            () -> Optional.ifPresent(selectedAnswerMap.get(rule.getQuestionRefName()),
                                    selectedAnswer -> {
                                        Optional.doWhen(TextUtil.equals(selectedAnswer.getAnswerReference(),
                                                rule.getAnswerRefName()) && questionMap.containsKey(rule.getNextQuestionRefName()),
                                                () -> {
                                                    ruleMatched[0] = true;
                                                    Question nextQuestion = questionMap.get(rule.getNextQuestionRefName());
                                                    addQuestionItem(nextQuestion, callback);
                                                    parseQuestions(nextQuestion.getIndex(), callback);
                                                });
                                    }));
                }
                Optional.doWhen(!ruleMatched[0] && !item.isLastIfNoRuleMatches(), () -> addSubsequentQuestion(item.getIndex() + 1, callback));
                Optional.doWhen(!ruleMatched[0] && item.isLastIfNoRuleMatches(), callback::allQuestionHasBeenAnswered);
            }, () -> addSubsequentQuestion(item.getIndex() + 1, callback));
        }

        private void addSubsequentQuestion(int index, QuestionDropDownAdapterOnClickCallback callback) {
            Optional.doWhen(questions.size() > index,
                    () -> addQuestionItem(questions.get(index), callback),
                    callback::allQuestionHasBeenAnswered);
        }

        private void addQuestionItem(@Nullable Question question, QuestionDropDownAdapterOnClickCallback callback) {
            Optional.ifPresent(question, question1 -> {
                List<Answer> answers = question1.getAnswers();
                Optional.doWhen(answers == null || answers.size() == 0,
                        () -> callback.lastQuestionToUploadPictureIsRemaining(question),
                        () -> add(question));
            });

        }
    }

    public interface QuestionDropDownAdapterOnClickCallback extends BaseRecyclerViewListener {
        void allQuestionHasBeenAnswered();

        void lastQuestionToUploadPictureIsRemaining(Question question);

        void clearPreviouslyAskedQuestion();
    }
}
