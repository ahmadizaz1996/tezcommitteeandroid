package com.tez.androidapp.rewamp.general.suspend.account.presenter;

import com.tez.androidapp.rewamp.general.suspend.account.FeedbackCallback;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public interface ISuspendAccountFeedbackActivityInteractorOutput extends FeedbackCallback {
}
