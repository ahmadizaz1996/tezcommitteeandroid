package com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 2/14/2017.
 */

public class AddWalletRequest extends BaseRequest {


    public static final String METHOD_NAME = "v2/user/wallet";

    private String mobileAccountNumber;
    private Integer walletServiceProviderId;
    private String pin;
    private Boolean isDefault;

    public String getPin() { return pin; }

    public void setPin(String pin) { this.pin = pin; }

    public String getMobileAccountNumber() {
        return mobileAccountNumber;
    }

    public void setDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public void setMobileAccountNumber(String mobileAccountNumber) {
        this.mobileAccountNumber = mobileAccountNumber;
    }

    public Integer getWalletServiceProviderId() {
        return walletServiceProviderId;
    }

    public void setWalletServiceProviderId(Integer walletServiceProviderId) {
        this.walletServiceProviderId = walletServiceProviderId;
    }
}
