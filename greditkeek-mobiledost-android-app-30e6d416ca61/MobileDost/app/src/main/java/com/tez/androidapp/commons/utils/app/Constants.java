package com.tez.androidapp.commons.utils.app;

import android.text.format.DateUtils;

import com.tez.androidapp.BuildConfig;

/**
 * Created  on 1/24/2017.
 */

@SuppressWarnings("WeakerAccess")
public final class Constants {

    private Constants() {

    }


    public static final boolean IS_EXTERNAL_BUILD = !BuildConfig.IS_DEBUG;


    public static final String AMOUNT_PATTERN = "[0-9]+(\\.[0-9]+)?$";
    public static final String STRING_EMAIL_VALIDATOR_REGEX = "(^[a-zA-Z0-9.!#$%&’*+\\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)+$)|^$";
    public static final String PHONE_NUMBER_VALIDATOR_REGEX = "^03[0-9]{9}$";
    public static final String STRING_PIN_VALIDATOR_REGEX = "^(\\d{4})$";
    public static final String STRING_PIN_DIGIT_5_VALIDATOR_REGEX = "^(\\d{5})$";
    public static final String STRING_CNIC_VALIDATOR_REGEX = "^[1-9][0-9]{4}[0-9]{7}[0-9]$|^$";
    public static final String REGEX_NOT_EMPTY = "(\\S)+";


    public static final long ONE_MIN = 100000;
    public static final long THIRTY_SECONDS = 30000;
    public static final long ONE_SECOND = 1000;
    public static final long FIVE_SECOND = 5000;


    public static final String DATA_LIFT_JOB_TAG = "DEVICE_DATA_LIFT";
    public static final long TIME_TO_LIFT = DateUtils.DAY_IN_MILLIS * 7;
    public static final long WEEK_MILLIS = 7 * 24 * 60 * 60 * 1000L;
    static final int YEAR_TO_GO_BACK = 18;


    public static final int BENEFICIARY_FATHER = 42;
    public static final int BENEFICIARY_MOTHER = 43;
    public static final int BENEFICIARY_HUSBAND = 44;
    public static final int BENEFICIARY_WIFE = 45;
    public static final int BENEFICIARY_SON = 46;
    public static final int BENEFICIARY_DAUGHTER = 47;
    public static final int BENEFICIARY_BROTHER = 48;
    public static final int BENEFICIARY_SISTER = 49;
    public static final int BENEFICIARY_OTHER = 50;


    public static final int CNIC_REQUEST_CODE_FRONT = 10;
    public static final int CNIC_REQUEST_CODE_BACK = 20;
    public static final int CNIC_REQUEST_CODE_FACE = 30;
    public static final String CNIC_REQUEST_CODE = "requestCameraForCnic";
    public static final String CNIC_REQUEST_FOR_DOCUMENT = "requestCameraFordocument";


    public static final String REF_CODE = "refCode";

    public static final int TEZ_LIFE_INSURANCE = 1;
    public static final int TEZ_HEALTH_INSURANCE_1 = 2;
    public static final int TEZ_HEALTH_INSURANCE_2 = 3;
    public static final int HOSPITAL_COVERAGE_PLAN_ID = 4;
    public static final int DIGITAL_OPD_PLAN_ID = 5;
    public static final int CORONA_DEFENSE_PLAN_ID = 6;
    public static final int TEZ_INSURANCE_PLAN_ID = 7;

    public static final String INSURANCE_PROVIDER_EFU = "EFU";
    public static final String INSURANCE_PROVIDER_JUBILEE = "JUBILEE";
    public static final String INSURANCE_PROVIDER_TPL = "TPL";
    public static final String INSURANCE_PROVIDER_WEBDOC = "WEBDOC";

    public static final int SIMSIM_ID = 1;
    public static final int EASYPAISA_ID = 4;
    public static final int UBL_OMNI_ID = 5;
    public static final int JAZZ_CASH_ID = 6;


    public static final String EASYPASIA_CALL_CENTER_NUMBER = "042-111-003-737";
    public static final String UBL_CALL_CENTER_NUMBER = "021-111-825-888";
    public static final String SIMSIM_CALL_CENTER_NUMBER = "";


    public static final String EASYPAISA_PACKAGE_NAME = "pk.com.telenor.phoenix&hl=en";
    public static final String SIM_SIM_PACKAGE_NAME = "com.finja.simsim";
    public static final String UBL_OMNI_PACKAGE_NAME = "ubl.smschannel";
    public static final String JAZZ_CASH_PACKAGE_NAME = "com.techlogix.mobilinkcustomer";


    public static final String UI_DATE_FORMAT = "dd-MM-yyyy";
    public static final String BACKEND_DATE_FORMAT = "yyyy-MM-dd";
    public static final String STRING_DATE_FORMAT = "%s-%s-%d";
    public static final String INPUT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String OUTPUT_DATE_FORMAT = "dd-MMM-yyyy";
    public static final String OUTPUT_DATE_FORMAT_DD_MMMM_YYYY = "dd-MMMM-yyyy";
    public static final String OUTPUT_DATE_FORMAT_WITH_TWO_DIGIT_YEAR = "dd-MMM-yy";
    public static final String NEW_FILE_DATE_FORMAT = "yyyyMMdd_HHmmss";


    public static final String TEZ_LOGS = "tezlogs.txt";
    public static final String ERROR_MESSAGE = "Moaziz saarif, hum aap ko filhaal services farhaam nahi kar sakte. Baraye meherbani thori dair baad koshish karien.";


    public static final String EMPTY_NOTIFICATION_INTENT = "empyt_notification_intent";
    public static final String STRING_MAP = "MAP";
    public static final String STRING_DATE = "DATE";
    public static final String HEADER_API_KEY = "api_key";


    public static final int SOCKET_TIME_OUT_LIMIT_SECONDS = 60;
    public static final int STATUS_CODE_UNAUTHORIZED = 401;
    public static final int UNAUTHORIZED_ERROR_CODE = -1;
    public static final int ERROR_CODE_NO_ERROR = 0;
    public static final String STRING_UNAUTHORIZED = "Unauthorized";


    public static final String NETWORK_NO_SIM = "NO_SIM";
    public static final String USER_STATUS_ACTIVATION_PENDING = "ACTIVATION_PENDING";
    public static final String USER_STATUS_READY = "READY";


    public static final String BROAD_CAST_RECIEVER_ON_VALIDATE_PIN_ACTIVITY = "broad cast reciever on validate pin activity";
    public static final int DOCUMENT_UPLOADED_SCCUESSFULLY_STATUS = 1;
    public static final int DOCUMENT_UPLOADED_NEUTRAL_STATUS = 0;


    public static final String STRING_DOCUMENT_URI = "document uri";
    public static final String STRING_DOCUMENT_MIME_TYPE = "document mime type";
    public static final String JPEG_MIME_TYPE = "image/jpeg";


    public static final String RETAKE_PRINCIPAL_NAME = "retakePrincipalName";
    public static final String KEY_USER_EMAIL = "email";
    public static final String KEY_USER_PUBLIC_PROFILE = "public_profile";


    public static final String SUCCESS = "SUCCESS";
    public static final String CLAIM_IN_PROCESS = "CLAIM IN PROCESS";
    public static final String ACTIVE = "ACTIVE";
    public static final String LAPSED = "LAPSED";
    public static final String CLAIMED = "CLAIMED";
    public static final String STRING_DOCUMENT_STATUS_APPROVED = "APPROVED";
    public static final String STRING_DOCUMENT_STATUS_REJECTED = "REJECTED";
    public static final String STRING_CLAIM_STATUS_REVISE_CLAIM = "REVISE CLAIM";
    public static final String STRING_DOCUMENT_STATUS_SKIPPED = "SKIPPED";
    public static final String MobileNumberVerificationStatusSuccess = "success";
    public static final String MobileNumberVerificationStatusFailed = "failed";
    public static final String MobileNumberVerificationMethodSinch = "sinch";
    public static final String MobileNumberVerificationMethodOtp = "otp";


    private static final String PENDING = "PENDING";
    public static final String USER_STATUS_PENDING = PENDING;
    public static final String STRING_CLAIM_STATUS_PENDING = PENDING;
    public static final String COMPLETE_PROFILE = "COMPLETE_PROFILE";
    public static final String MOBILE_WALLET = "MOBILE_WALLET";
    public static final String BENEFICIARY = "BENEFICIARY";
    public static final int USER_SESSION_EXPIRED_REQUEST_CODE = 9876;
}
