package com.tez.androidapp.rewamp.general.changepin.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.changepin.view.ChangeTemporaryPinActivity;

public class ChangeTemporaryPinActivityRouter extends BaseActivityRouter {

    public static ChangeTemporaryPinActivityRouter createInstance() {
        return new ChangeTemporaryPinActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, ChangeTemporaryPinActivity.class);
    }
}
