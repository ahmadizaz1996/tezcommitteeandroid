package com.tez.androidapp.commons.utils.file.util;

import android.net.Uri;
import androidx.annotation.Nullable;

/**
 * Created by FARHAN DHANANI on 7/16/2018.
 */
public interface DownloadDocumentsCallBack {
     void onSuccess(Uri uri, String mimeType);
     void onFailure(int errorCode, @Nullable String message);
}
