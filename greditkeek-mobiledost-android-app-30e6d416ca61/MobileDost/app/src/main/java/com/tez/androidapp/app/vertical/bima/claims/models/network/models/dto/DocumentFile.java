package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VINOD KUMAR on 3/6/2019.
 */
public class DocumentFile implements Parcelable {

    public static final int READY = 0;
    public static final int UPLOADING = 1;
    public static final int UPLOADED = 2;
    public static final int ERROR = 3;
    public static final Creator<DocumentFile> CREATOR = new Creator<DocumentFile>() {
        @Override
        public DocumentFile createFromParcel(Parcel in) {
            return new DocumentFile(in);
        }

        @Override
        public DocumentFile[] newArray(int size) {
            return new DocumentFile[size];
        }
    };
    private Integer docFileId;
    private String docFilePath;
    private String docMimeType;
    private Uri docUri;
    private int uploadStatus = READY;

    public DocumentFile(Integer docFileId, String docFilePath, String docMimeType) {
        this.docFileId = docFileId;
        this.docFilePath = docFilePath;
        this.docMimeType = docMimeType;
    }

    public DocumentFile(Integer docFileId, String docFilePath, String docMimeType, int uploadStatus) {
        this.docFileId = docFileId;
        this.docFilePath = docFilePath;
        this.docMimeType = docMimeType;
        this.uploadStatus = uploadStatus;
    }

    public DocumentFile(Integer docFileId, String docFilePath, String docMimeType, Uri docUri) {
        this.docFileId = docFileId;
        this.docFilePath = docFilePath;
        this.docMimeType = docMimeType;
        this.docUri = docUri;
    }

    protected DocumentFile(Parcel in) {
        if (in.readByte() == 0) {
            docFileId = null;
        } else {
            docFileId = in.readInt();
        }
        docFilePath = in.readString();
        docMimeType = in.readString();
        docUri = in.readParcelable(Uri.class.getClassLoader());
        uploadStatus = in.readInt();
    }

    public static List<DocumentFile> parse(@NonNull List<InsuranceClaimDocumentFileDto> fileDtoList) {
        List<DocumentFile> documentList = new ArrayList<>();
        for (InsuranceClaimDocumentFileDto fileDto : fileDtoList)
            documentList.add(new DocumentFile(fileDto.getId(), fileDto.getFilePath(), fileDto.getMimeType(), UPLOADED));
        return documentList;
    }

    public void setUploadStatus(int uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    public Integer getDocFileId() {
        return docFileId;
    }

    String getDocFilePath() {
        return docFilePath;
    }

    public void setDocFilePath(String docFilePath) {
        this.docFilePath = docFilePath;
    }

    public String getDocMimeType() {
        return docMimeType;
    }

    public void setDocMimeType(String docMimeType) {
        this.docMimeType = docMimeType;
    }

    public Uri getDocUri() {
        return docUri;
    }

    void setDocUri(Uri docUri) {
        this.docUri = docUri;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (docFileId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(docFileId);
        }
        parcel.writeString(docFilePath);
        parcel.writeString(docMimeType);
        parcel.writeParcelable(docUri, i);
        parcel.writeInt(uploadStatus);
    }


    public boolean isReady() {
        return uploadStatus == READY;
    }

    public boolean isUploading() {
        return uploadStatus == UPLOADING;
    }

    public boolean isUploaded() {
        return uploadStatus == UPLOADED;
    }

    public boolean isError() {
        return uploadStatus == ERROR;
    }
}
