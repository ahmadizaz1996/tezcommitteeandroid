package com.tez.androidapp.rewamp.bima.products.view;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.fragments.BaseFragment;
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.rewamp.bima.products.adapter.PoliciesAdapter;
import com.tez.androidapp.rewamp.bima.products.router.ProductPolicyActivityRouter;
import com.tez.androidapp.rewamp.bima.products.viewmodel.PoliciesViewModel;
import com.tez.androidapp.rewamp.general.network.model.NetworkCall;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

public class PoliciesFragment extends BaseFragment implements PoliciesAdapter.PolicyListener {

    @BindView(R.id.rvPolicies)
    private RecyclerView rvPolicies;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    @BindView(R.id.layoutEmptyPolicy)
    private View layoutEmptyPolicy;

    @BindView(R.id.shimmer)
    private TezLinearLayout shimmer;

    @Nullable
    private View baseView;

    private InsuranceProductsAndPolicyListCallback callback;

    static PoliciesFragment newInstance() {
        return new PoliciesFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.callback = (InsuranceProductsAndPolicyListCallback) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (baseView == null) {
            baseView = inflater.inflate(R.layout.policies_fragment, container, false);
            ViewBinder.bind(this, baseView);
        }
        return baseView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PoliciesViewModel model = new ViewModelProvider(requireActivity()).get(PoliciesViewModel.class);
        model.getPolicyListLiveData().observe(getViewLifecycleOwner(), policies -> {
            if (rvPolicies.getAdapter() == null) {
                PoliciesAdapter adapter = new PoliciesAdapter(policies, this);
                rvPolicies.setAdapter(adapter);

                layoutEmptyPolicy.setVisibility(policies.isEmpty() ? View.VISIBLE : View.GONE);
                clContent.setVisibility(!policies.isEmpty() ? View.VISIBLE : View.GONE);
            }

            callback.setIllustrationVisibility(policies.isEmpty() ? View.VISIBLE : View.GONE);
        });
        model.getNetworkCallMutableLiveData().observe(getViewLifecycleOwner(), networkCall -> {
            NetworkCall.State state = networkCall.getState();
            switch (state) {

                case LOADING:
                    shimmer.setVisibility(View.VISIBLE);
                    clContent.setVisibility(View.GONE);
                    break;

                case FAILURE:
                    shimmer.setVisibility(View.GONE);
                    clContent.setVisibility(View.GONE);
                    showError(networkCall.getErrorCode());
                    break;

                case SUCCESS:
                    shimmer.setVisibility(View.GONE);
                    clContent.setVisibility(View.VISIBLE);
                    break;
            }
        });
    }

    @OnClick(R.id.btPurchasePolicy)
    private void onClickPurchasePolicy() {
        callback.showProductsFragment();
    }

    @Override
    public void onClickPolicy(@NonNull Policy policy) {
        getBaseActivity(baseActivity -> ProductPolicyActivityRouter
                .createInstance()
                .setDependenciesAndRoute(baseActivity,
                        policy.getMobileUserInsurancePolicyId(),
                        policy.getId(),
                        policy.getPolicyName()));
    }
}
