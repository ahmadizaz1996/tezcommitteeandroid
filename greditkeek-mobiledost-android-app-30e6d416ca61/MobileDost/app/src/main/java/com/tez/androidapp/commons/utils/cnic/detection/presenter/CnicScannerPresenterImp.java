package com.tez.androidapp.commons.utils.cnic.detection.presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.utils.cnic.detection.contracts.CnicScannerPresenter;
import com.tez.androidapp.commons.utils.cnic.detection.contracts.CnicScannerView;
import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;
import com.tez.androidapp.commons.utils.cnic.detection.util.Camera.CameraBuilder;
import com.tez.androidapp.commons.utils.cnic.detection.view.CnicScannerActivity;
import com.tez.androidapp.commons.utils.file.util.FileUtil;

import java.io.File;

import static com.tez.androidapp.commons.utils.cnic.detection.util.Camera.getCorrectRotation;

public class CnicScannerPresenterImp implements CnicScannerPresenter {


    private static final int PERMISSION_CODE = 11;
    private final CnicScannerView cnicScannerView;
    private Camera camera;
    private String filePath;

    public CnicScannerPresenterImp(CnicScannerView cnicScannerView) {
        this.cnicScannerView = cnicScannerView;
    }

    @Override
    public void setUpDisplayAndCamera(String detectObject) {
        if (detectObject == null) return;
        switch (detectObject) {
            case CnicScannerActivity.CNIC_FRONT:
                setDisplayAndCameraForCnic(CnicScannerActivity.CNIC_FRONT);
                break;
            case CnicScannerActivity.CNIC_BACK:
                setDisplayAndCameraForCnic(CnicScannerActivity.CNIC_BACK);
                break;
            case CnicScannerActivity.FACE:
                setDisplayAndCameraForFace();
                break;
            default:
                //Default case should not be implemented
        }
    }

    @Override
    public void clickPicture(Activity activity) {
        try {
            camera.takePicture(null, null, (bytes, camera1) -> {
                File file = FileUtil.prepareFileForOCR(bytes, activity);
                File rotatedFile = Utility.rotateImageFile(file, getCorrectRotation());
                activity.runOnUiThread(() -> {
                    setFilePathForFace(rotatedFile.getAbsolutePath());
                    cnicScannerView.onPictureTaken(rotatedFile.getAbsolutePath());
                });
            });
        } catch (Exception e) {
            Crashlytics.log("Could not take picture!");
            Crashlytics.logException(e);
            cnicScannerView.onPictureTakenFailure();
        }

    }

    private void setFilePathForFace(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public boolean isCameraAndStorageAllowed(AppCompatActivity appCompatActivity) {
        if (ActivityCompat.checkSelfPermission(appCompatActivity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(appCompatActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return true;
        else
            requestCameraAndStoragePermission(appCompatActivity);
        return false;
    }

    @Override
    public void handlePermissionResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == PERMISSION_CODE
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED)
            cnicScannerView.startDetection();


        else
            cnicScannerView.finishActivity();

    }

    @Override
    public void resumeCamera() {
        try {
            camera.startPreview();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    @Override
    public void setIntentForFaceAndFinish() {
        Intent intent = cnicScannerView.setIntentForActivity(this.filePath, new RetumDataModel());
        cnicScannerView.setResultOK(intent);
        cnicScannerView.finishActivity();
    }


    private void requestCameraAndStoragePermission(AppCompatActivity appCompatActivity) {

        ActivityCompat.requestPermissions(appCompatActivity,
                new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_CODE);
    }

    private void setDisplayAndCameraForFace() {
        cnicScannerView.setScreenOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        cnicScannerView.setVisibilityOfCnicSquare(View.GONE);
        cnicScannerView.setVisibilityOfTextViewHoldCnic(View.GONE);
        cnicScannerView.setVisibilityOfImageViewSelfiePreview(View.VISIBLE);
        cnicScannerView.setCameraAndStartPreview(getCameraForFace(), CnicScannerActivity.FACE);

    }

    private void setDisplayAndCameraForCnic(String detectObject) {
        cnicScannerView.setScreenOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        cnicScannerView.setVisibilityOfCnicSquare(View.VISIBLE);
        cnicScannerView.setVisibilityOfTextViewHoldCnic(View.VISIBLE);
        cnicScannerView.setVisibilityOfImageViewSelfiePreview(View.GONE);
        cnicScannerView.setCameraAndStartPreview(getCameraForCNIC(), detectObject);
    }

    private Camera getCameraForFace() {
        camera = new CameraBuilder()
                .setCameraFacing(Camera.CameraInfo.CAMERA_FACING_FRONT)
                .setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO)
                .build();
        return camera;
    }

    private Camera getCameraForCNIC() {
        camera = new CameraBuilder()
                .setCameraFacing(Camera.CameraInfo.CAMERA_FACING_BACK)
                .setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO)
                .setDisplayOrientation(0)
                .build();
        return camera;
    }
}
