package com.tez.androidapp.app.general.feature.transactions.callbacks;

import com.tez.androidapp.app.general.feature.transactions.models.network.TransactionDetail;

/**
 * Created  on 9/2/2017.
 */

public interface GetLoanTransactionDetailCallback {

    void onGetLoanTransactionDetailSuccess(TransactionDetail transactionDetail);

    void onGetLoanTransactionDetailFailure(int statusCode, String message);
}
