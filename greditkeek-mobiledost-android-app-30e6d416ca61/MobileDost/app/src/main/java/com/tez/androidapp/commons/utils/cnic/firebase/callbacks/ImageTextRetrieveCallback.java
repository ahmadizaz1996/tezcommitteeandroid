package com.tez.androidapp.commons.utils.cnic.firebase.callbacks;



import java.util.List;

public interface ImageTextRetrieveCallback {
    void onTextRetrieveFailure(Exception exception);

    void onTextRetrieveSuccess(List<String> list);
}
