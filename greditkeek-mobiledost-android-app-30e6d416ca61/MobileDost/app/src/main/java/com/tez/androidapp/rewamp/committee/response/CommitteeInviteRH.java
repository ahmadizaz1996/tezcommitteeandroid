package com.tez.androidapp.rewamp.committee.response;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeInviteListener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import androidx.annotation.Nullable;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeInviteRH extends BaseRH<CommitteeInviteResponse> {

    private final CommitteeInviteListener listener;

    public CommitteeInviteRH(BaseCloudDataStore baseCloudDataStore, CommitteeInviteListener committeeInviteListener) {
        super(baseCloudDataStore);
        this.listener = committeeInviteListener;
    }

    @Override
    protected void onSuccess(Result<CommitteeInviteResponse> value) {
        CommitteeInviteResponse committeeInviteResponse = value.response().body();
        if (committeeInviteResponse != null && committeeInviteResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onInviteSuccess(committeeInviteResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onInviteFailure(errorCode, message);
    }
}
