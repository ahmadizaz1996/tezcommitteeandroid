package com.tez.androidapp.rewamp.advance.request.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.rewamp.advance.request.request.LoanApplyRequest;
import com.tez.androidapp.rewamp.advance.request.model.LoanDuration;

import java.util.List;

public interface ISelectLoanActivityView extends IBaseView {

    void setMaxLoanLimit(double loanLimit);

    void showMaxLimitPrompt(String maxLimit);

    void setUserWallet(@Nullable Wallet wallet);

    void initCalculateAmount();

    void setTvCancelMyLimitOnClickListener(int loanId);

    void routeToLoanPurposeActivity(@NonNull LoanApplyRequest loanApplyRequest);

    void setLoanDurations(@NonNull List<LoanDuration> loanDurationList);

    void setTvProcessingFeeValue(String value);

    void setTvChargesValue(String value);

    void setTvAmountReceiveValue(String value);

    void setTvAmountReturnValue(String value);

    void setClContentVisibility(int visibility);
}
