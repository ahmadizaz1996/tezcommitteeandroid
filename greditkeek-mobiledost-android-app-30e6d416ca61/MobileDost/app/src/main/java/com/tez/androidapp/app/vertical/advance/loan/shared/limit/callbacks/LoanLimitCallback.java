package com.tez.androidapp.app.vertical.advance.loan.shared.limit.callbacks;

import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.response.LoanLimitResponse;

/**
 * Created  on 3/1/2017.
 */

public interface LoanLimitCallback {

    void onLoanLimitSuccess(LoanLimitResponse loanLimitResponse);

    void onLoanLimitFailure(int errorCode, String message);
}
