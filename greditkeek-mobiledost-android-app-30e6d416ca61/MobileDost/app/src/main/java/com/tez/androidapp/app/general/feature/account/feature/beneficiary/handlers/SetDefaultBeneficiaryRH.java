package com.tez.androidapp.app.general.feature.account.feature.beneficiary.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.callbacks.SetDefaultBeneficiaryCallback;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

public class SetDefaultBeneficiaryRH extends BaseRH<BaseResponse> {

    private final SetDefaultBeneficiaryCallback callback;

    public SetDefaultBeneficiaryRH(BaseCloudDataStore baseCloudDataStore, SetDefaultBeneficiaryCallback callback) {
        super(baseCloudDataStore);
        this.callback = callback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse response = value.response().body();
        if (response != null) {
            if (isErrorFree(response))
                callback.onSetDefaultBeneficiarySuccess();
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        callback.onSetDefaultBeneficiaryFailure(errorCode, message);
    }
}
