package com.tez.androidapp.app.vertical.bima.policies.callbacks;

import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy;

import java.util.List;

/**
 * Created by Rehman Murad Ali on 8/25/2017.
 */

public interface GetActiveInsurancePolicyCallback {

    void onGetActiveInsurancePolicySuccess(List<Policy> policies);

    void onGetActiveInsurancePolicyFailure(int errorCode, String message);
}
