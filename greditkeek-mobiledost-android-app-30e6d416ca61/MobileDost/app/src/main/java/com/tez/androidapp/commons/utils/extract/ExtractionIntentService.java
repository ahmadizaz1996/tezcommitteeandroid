package com.tez.androidapp.commons.utils.extract;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.utils.file.util.FileUtil;
import com.tez.androidapp.repository.network.store.DataLiftCloudDataStore;

import java.io.File;

/**
 * <p>
 * Created  on 12/30/2016.
 */

public class ExtractionIntentService extends IntentService {

    private static final String TAG = ExtractionIntentService.class.getSimpleName();

    public ExtractionIntentService() {
        super(TAG);
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            ExtractionUtil extractionUtil = new ExtractionUtil(this);
            if (Constants.IS_EXTERNAL_BUILD) {
                FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_CALL_LOG, extractionUtil.extractCallsObject());
                FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_CALENDAR, extractionUtil.extractEventsObject());
                FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_FILE_DIRECTORY, extractionUtil.extractFilesObject());
                FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_MSG_LOG, extractionUtil.extractSmsObject());
                FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_ACCOUNTS, extractionUtil.extractAccountsObject());
                FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_APP_LOGS, extractionUtil.extractApplicationsObject());
                FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_CONTACTS, extractionUtil.extractContactsObject());
                FileUtil.createFileWithData(this, ExtractionUtil.TYPE_BLUETOOTH, extractionUtil.extractBluetoothFromDevice());
                FileUtil.createFileWithData(this, ExtractionUtil.TYPE_WIFI, extractionUtil.extractWifiFromDevice());
                FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_DEVICE_INFO, extractionUtil.extractDeviceInfoObject());
            } else {
                FileUtil.createFileWithData(this, ExtractionUtil.TYPE_CALL_LOG, "{}");
                FileUtil.createFileWithData(this, ExtractionUtil.TYPE_CALENDAR, "{}");
                FileUtil.createFileWithData(this, ExtractionUtil.TYPE_FILE_DIRECTORY, "{}");
                FileUtil.createFileWithData(this, ExtractionUtil.TYPE_MSG_LOG, "{}");
                FileUtil.createFileWithData(this, ExtractionUtil.TYPE_ACCOUNTS, "{}");
                FileUtil.createFileWithData(this, ExtractionUtil.TYPE_APP_LOGS, "{}");
                FileUtil.createFileWithData(this, ExtractionUtil.TYPE_CONTACTS, "{}");
                FileUtil.createFileWithData(this, ExtractionUtil.TYPE_BLUETOOTH, "{}");
                FileUtil.createFileWithData(this, ExtractionUtil.TYPE_WIFI, "{}");
                FileUtil.createFileWithData(this, ExtractionUtil.TYPE_DEVICE_INFO, "{}");
            }
            File zipFile = FileUtil.createZipFile(this);
            DataLiftCloudDataStore dataLiftCloudDataStore = DataLiftCloudDataStore.getInstance();
            if (TextUtils.isEmpty(Utility.getImei(getApplicationContext())) || MDPreferenceManager.getPrincipalName() == null || !Utility.doesUserExist()) {
                return;
            }
            MDPreferenceManager.setUploadDeviceDataReady(true);
            dataLiftCloudDataStore.uploadDeviceData(zipFile);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }
}
