package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.QuestionOptionsCallBack;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.GetCnicUploadsResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.QuestionOptionsResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

public class QuestionOptionsRH extends BaseRH<QuestionOptionsResponse> {

    private final QuestionOptionsCallBack questionOptionsCallBack;

    public QuestionOptionsRH(BaseCloudDataStore baseCloudDataStore,
                             QuestionOptionsCallBack questionOptionsCallBack) {
        super(baseCloudDataStore);
        this.questionOptionsCallBack = questionOptionsCallBack;
    }

    @Override
    protected void onSuccess(Result<QuestionOptionsResponse> value) {
        QuestionOptionsResponse questionOptionsResponse = value.response().body();
        if (questionOptionsResponse!=null
                && questionOptionsResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR
                && questionOptionsResponse.getData()!=null)
            questionOptionsCallBack.onQuestionOptionsSuccess(questionOptionsResponse.getData());
        else onFailure(questionOptionsResponse.getStatusCode(),
                questionOptionsResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        questionOptionsCallBack.onQuestionOptionsFailure(errorCode, message);
    }
}
