package com.tez.androidapp.app.general.feature.account.feature.beneficiary.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.callbacks.GetAdvanceBimaBeneficiaryCallback;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.dto.response.GetAdvanceBimaBeneficiaryResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 8/25/2017.
 */

public class GetAdvanceBimaBeneficiaryRH extends BaseRH<GetAdvanceBimaBeneficiaryResponse> {

    GetAdvanceBimaBeneficiaryCallback getAdvanceBimaBeneficiaryCallback;

    public GetAdvanceBimaBeneficiaryRH(BaseCloudDataStore baseCloudDataStore, GetAdvanceBimaBeneficiaryCallback getAdvanceBimaBeneficiaryCallback) {
        super(baseCloudDataStore);
        this.getAdvanceBimaBeneficiaryCallback = getAdvanceBimaBeneficiaryCallback;
    }

    @Override
    protected void onSuccess(Result<GetAdvanceBimaBeneficiaryResponse> value) {
        GetAdvanceBimaBeneficiaryResponse getAdvanceBimaBeneficiaryResponse = value.response().body();
        if (isErrorFree(getAdvanceBimaBeneficiaryResponse)) {
            getAdvanceBimaBeneficiaryCallback.onGetAdvanceBimaBeneficiarySuccess(getAdvanceBimaBeneficiaryResponse.getBeneficiary());
        } else onFailure(getAdvanceBimaBeneficiaryResponse.getStatusCode(), getAdvanceBimaBeneficiaryResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        getAdvanceBimaBeneficiaryCallback.onGetAdvanceBimaBeneficiaryFailure(errorCode, message);
    }
}
