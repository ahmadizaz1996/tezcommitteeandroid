package com.tez.androidapp.commons.calendar;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezEditTextView;

import net.tez.calendar.AbstractAndroidDatePickerDialog;
import net.tez.calendar.CalendarFactory;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Locale;


/**
 * Created by VINOD KUMAR on 10/29/2018.
 */
public interface TezCalendar {

    static void showCalendarTillToday(@NonNull Context context,
                                      @NonNull TextView tezTextView,
                                      @NonNull OnDateSetListener onDateSetListener) {
        AbstractAndroidDatePickerDialog datePickerDialog = CalendarFactory.getAndroidDatePicker(context, (year, month, dayOfMonth) -> {
            String date = String.format(Locale.US, Constants.STRING_DATE_FORMAT, Utility.getTwoDigitInteger(dayOfMonth), Utility.getTwoDigitInteger(month), year);
            tezTextView.setText(date);
            onDateSetListener.onDateSet(date);
        });
        datePickerDialog.show();
        //datePickerDialog.disableDateAfterToday();
        selectCurrentDate(datePickerDialog, tezTextView.getText().toString());
    }

    static void showCalendarForDob(@NonNull Context context, @NonNull TezEditTextView tezEditTextView) {

        AbstractAndroidDatePickerDialog datePickerDialog = CalendarFactory.getAndroidDatePicker(context, (year, month, dayOfMonth) -> {
            tezEditTextView.setText(String.format(Locale.US, Constants.STRING_DATE_FORMAT, Utility.getTwoDigitInteger(dayOfMonth), Utility.getTwoDigitInteger(month), year));
            tezEditTextView.setError(null);
        });
        datePickerDialog.show();
        if (!setDateIfContains(tezEditTextView.getValueText(), datePickerDialog)) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.YEAR, -18);
            datePickerDialog.setDate(calendar.get(Calendar.YEAR), Utility.getCorrectOffsetMonth(calendar.get(Calendar.MONTH)), calendar.get(Calendar.DAY_OF_MONTH));
        }
    }

    static void showCalendarForIssueDate(@NonNull Context context, @NonNull TezEditTextView tezEditTextView) {
        AbstractAndroidDatePickerDialog datePickerDialog = CalendarFactory.getAndroidDatePicker(context, (year, month, dayOfMonth) -> {
            tezEditTextView.setText(String.format(Locale.US, Constants.STRING_DATE_FORMAT, Utility.getTwoDigitInteger(dayOfMonth), Utility.getTwoDigitInteger(month), year));
            tezEditTextView.setError(null);
        });
        datePickerDialog.show();
        setDateIfContains(tezEditTextView.getValueText(), datePickerDialog);
    }

    static void showCalendarForExpiryDate(@NonNull Context context, @NonNull TezEditTextView tezEditTextView) {
        LifetimeDatePickerDialog lifetimeDatePickerDialog = new LifetimeDatePickerDialog(context, (year, month, dayOfMonth) -> {
            tezEditTextView.setText(String.format(Locale.US, Constants.STRING_DATE_FORMAT, Utility.getTwoDigitInteger(dayOfMonth), Utility.getTwoDigitInteger(month), year));
            tezEditTextView.setError(null);
        }, tezEditTextView::setText);
        lifetimeDatePickerDialog.show();
        setDateIfContains(tezEditTextView.getValueText(), lifetimeDatePickerDialog);
    }

    static void selectCurrentDate(AbstractAndroidDatePickerDialog androidDatePickerDialog, TezEditTextView tezEditTextView) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(Utility.getFormattedDate(tezEditTextView.getValueText(), Constants.UI_DATE_FORMAT));
            androidDatePickerDialog.setDate(
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH));
        } catch (ParseException e) {
            Crashlytics.logException(e);
        }
    }

    static void selectCurrentDate(AbstractAndroidDatePickerDialog androidDatePickerDialog, String date) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(Utility.getFormattedDate(date, Constants.UI_DATE_FORMAT));
            androidDatePickerDialog.setDate(
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH));
        } catch (ParseException e) {
            Crashlytics.logException(e);
        }
    }

    static boolean setDateIfContains(String etText, AbstractAndroidDatePickerDialog datePickerDialog) {
        try {
            if (etText != null && !etText.isEmpty()) {
                String[] dateSplit = etText.split("-");
                if (dateSplit.length == 3) {
                    int year = Integer.parseInt(dateSplit[2]);
                    int month = Integer.parseInt(dateSplit[1]);
                    int day = Integer.parseInt((dateSplit[0]));
                    datePickerDialog.setDate(year, month, day);
                    return true;
                }
            }

            return false;
        } catch (Exception ignore) {
            return false;
        }
    }

    interface OnDateSetListener {
        void onDateSet(String date);
    }
}
