package com.tez.androidapp.app.general.feature.account.feature.pin.code.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserSignUpSetPinCallback;
import com.tez.androidapp.commons.models.network.dto.response.UserSignUpSetPinResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 6/12/2017.
 */

public class UserSignUpSetPinRH extends BaseRH<UserSignUpSetPinResponse> {

    private UserSignUpSetPinCallback userSignUpSetPinCallback;

    public  UserSignUpSetPinRH(BaseCloudDataStore baseCloudDataStore, UserSignUpSetPinCallback
            userSignUpSetPinCallback) {
        super(baseCloudDataStore);
        this.userSignUpSetPinCallback = userSignUpSetPinCallback;
    }

    @Override
    protected void onSuccess(Result<UserSignUpSetPinResponse> value) {
        UserSignUpSetPinResponse userSignUpSetPinResponse = value.response().body();
        if (userSignUpSetPinResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            userSignUpSetPinCallback.onUserSignUpSetPinSuccess(userSignUpSetPinResponse);
        } else {
            onFailure(userSignUpSetPinResponse.getStatusCode(), userSignUpSetPinResponse.getErrorDescription());
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        userSignUpSetPinCallback.onUserSignUpSetPinError(errorCode, message);
    }
}
