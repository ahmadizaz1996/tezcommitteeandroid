package com.tez.androidapp.rewamp.signup;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezToolbar;
import com.tez.androidapp.rewamp.signup.router.NewIntroActivityRouter;

import java.util.ArrayList;
import java.util.List;

public class TezOnboardingActivity extends BaseOnboardingActivity {

    @Override
    protected void initViews(TezToolbar onboardingToolbar) {
        btMainButton.setDoubleTapSafeOnClickListener(view -> {
            NewIntroActivityRouter.createInstance().setDependenciesAndRoute(this);
            finish();
        });
    }

    @NonNull
    protected List<OnboardingContent> getOnboardingContentList() {
        List<OnboardingContent> list = new ArrayList<>();
        list.add(new OnboardingContent(R.drawable.img_onboarding_tez_1, R.string.licensed_financial_institution, R.string.license_description));
        list.add(new OnboardingContent(R.drawable.img_onboarding_tez_2, R.string.any_time_any_where, R.string.access_various_financial_services));
        list.add(new OnboardingContent(R.drawable.img_onboarding_tez_3, R.string.safe_and_secure, R.string.we_value_your_trust));
        return list;
    }

    @Override
    protected String getScreenName() {
        return "TezOnboardingActivity";
    }
}