package com.tez.androidapp.app.general.feature.faq.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.faq.models.network.FAQ;

import java.util.List;

/**
 * Created  on 2/28/2017.
 */

public class GetFAQsResponse extends BaseResponse {

    private List<FAQ> faqs;

    public List<FAQ> getFaqs() {
        return faqs;
    }

    public void setFaqs(List<FAQ> faqs) {
        this.faqs = faqs;
    }

    @Override
    public String toString() {
        return "GetFAQsResponse{" +
                "faqs=" + faqs +
                "} " + super.toString();
    }
}
