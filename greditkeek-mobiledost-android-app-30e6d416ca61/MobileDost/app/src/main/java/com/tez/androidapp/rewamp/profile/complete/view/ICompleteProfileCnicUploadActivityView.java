package com.tez.androidapp.rewamp.profile.complete.view;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.commons.utils.app.Constants;

public interface ICompleteProfileCnicUploadActivityView extends IBaseView {
    void startRetumModuleScreen(int requestCode, String detectObject);

    void startBackupCamera(int requestCode, boolean isSelfie);

    void setBtContinueActive(String cnic, String dob,
                             String issueDate, String expiryDate);

    void loadImageInUpCVFrontPart(String imagePath);

    void setLoaderInUpCVFrontPartVisible(boolean visibile);

    void setLoaderInUpCVSelfiePartVisbile(boolean visibile);

    void setLoaderInUpCVBackPart(boolean visible);

    void loadImageInUpCVSelfiePart(String imagePath);

    default boolean isRequestCodeBelongToOCR(int requestCode) {
        return requestCode == Constants.CNIC_REQUEST_CODE_BACK || requestCode == Constants.CNIC_REQUEST_CODE_FACE
                || requestCode == Constants.CNIC_REQUEST_CODE_FRONT;
    }

    void loadImageInUpCVBackPart(String imagePath);

    void updateClickListeners(String cnic, String dob,
                             String issueDate, String expiryDate);
}
