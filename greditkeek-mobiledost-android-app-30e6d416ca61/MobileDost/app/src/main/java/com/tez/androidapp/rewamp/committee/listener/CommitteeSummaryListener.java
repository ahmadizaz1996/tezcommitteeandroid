package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeDeclineResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeLeaveResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface CommitteeSummaryListener {

    void onCommitteeInviteSuccess(CommitteeInviteResponse committeeInviteResponse);

    void onCommitteeInviteFailure(int errorCode, String message);

    void onCommitteeLeaveSuccess(CommitteeLeaveResponse committeeLeaveResponse);

    void onCommitteeLeaveFailure(int errorCode, String message);

    void onCommitteeDeclineSuccess(CommitteeDeclineResponse committeeDeclineResponse);

    void onCommitteeDeclineFailure(int errorCode, String message);

    void onCommitteeJoinSuccess(JoinCommitteeResponse joinCommitteeResponse);

    void onCommitteeJoinFailure(int errorCode, String message);


}
