package com.tez.androidapp.rewamp.committee.presenter;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeFilterActivityPresenter {
    void getCommitteeFilter(String committeeId);
}
