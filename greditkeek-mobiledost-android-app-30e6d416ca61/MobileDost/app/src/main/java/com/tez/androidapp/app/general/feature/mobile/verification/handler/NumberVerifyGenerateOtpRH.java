package com.tez.androidapp.app.general.feature.mobile.verification.handler;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.mobile.verification.callback.NumberVerifyGenerateOtpCallback;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.response.NumberVerifyGenerateOtpResponse;

import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by FARHAN DHANANI on 2/26/2019.
 */
public class NumberVerifyGenerateOtpRH extends BaseRH<NumberVerifyGenerateOtpResponse> {

    @Nullable
    private final NumberVerifyGenerateOtpCallback numberVerifyGenerateOtpCallback;

    public NumberVerifyGenerateOtpRH(BaseCloudDataStore baseCloudDataStore,
                                     @Nullable NumberVerifyGenerateOtpCallback numberVerifyGenerateOtpCallback) {
        super(baseCloudDataStore);
        this.numberVerifyGenerateOtpCallback = numberVerifyGenerateOtpCallback;
    }

    @Override
    protected void onSuccess(Result<NumberVerifyGenerateOtpResponse> value) {
        Optional.ifPresent(value.response().body(),
                numberVerifyGenerateOtpResponse -> {
                    Optional.doWhen(isErrorFree(numberVerifyGenerateOtpResponse),
                            () -> Optional.ifPresent(this.numberVerifyGenerateOtpCallback,
                                    NumberVerifyGenerateOtpCallback::onNumberVerifyGenerateOtpSuccess),
                            () -> this.onFailure(numberVerifyGenerateOtpResponse.getStatusCode(), numberVerifyGenerateOtpResponse.getErrorDescription()));
                });
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        Optional.ifPresent(numberVerifyGenerateOtpCallback, numberVerifyGenerateOtpCallback -> {
            numberVerifyGenerateOtpCallback.onNumberVerifyGenerateOtpFailure(errorCode, message);
        });
    }
}
