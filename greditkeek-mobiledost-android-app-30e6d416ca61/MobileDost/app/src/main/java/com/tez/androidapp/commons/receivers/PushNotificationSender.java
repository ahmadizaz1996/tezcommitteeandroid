package com.tez.androidapp.commons.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.commons.utils.app.Utility;

/**
 * Created by farhan on 4/26/18.
 */

public class PushNotificationSender extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // if (getResultCode() != 1) {
        //   updateDashboardCards(context);
        String message = intent.getStringExtra(PushNotificationConstants.KEY_MESSAGE_TEXT);
        Utility.sendNotification(intent, message);
        //}
    }

    /*private void updateDashboardCards(Context context) {
        context.sendBroadcast(new Intent(Constants.BROAD_CAST_RECEIVER_UPDATE_DASHBOARD_CARDS));
    }*/
}
