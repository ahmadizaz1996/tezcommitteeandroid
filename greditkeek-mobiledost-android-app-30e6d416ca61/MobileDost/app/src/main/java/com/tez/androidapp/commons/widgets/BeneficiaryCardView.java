package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.TezDrawableHelper;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class BeneficiaryCardView extends TezCardView {

    @BindView(R.id.tvMobileNo)
    private TezTextView tvMobileNo;

    @BindView(R.id.tvAddNumber)
    private TezTextView tvAddNumber;

    @BindView(R.id.tvName)
    private TezTextView tvName;

    @BindView(R.id.tvRelation)
    private TezTextView tvRelation;

    @BindView(R.id.ivIcon)
    private TezImageView ivIcon;

    @BindView(R.id.rippleIvEdit)
    private RippleEffectImageView rippleIvEdit;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    public BeneficiaryCardView(@NonNull Context context) {
        super(context);
    }

    public BeneficiaryCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public BeneficiaryCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        inflate(context, R.layout.card_view_beneficiary, this);
        ViewBinder.bind(this, this);
    }

    @NonNull
    @Override
    public String getLabel() {
        return tvRelation.getLabel();
    }

    public void setName(String name) {
        tvName.setText(name);
    }

    public void setNumber(String number) {
        if (number == null) {
            tvAddNumber.setVisibility(VISIBLE);
            tvMobileNo.setVisibility(GONE);
        } else {
            tvMobileNo.setText(number);
            tvAddNumber.setVisibility(GONE);
            tvMobileNo.setVisibility(VISIBLE);
        }
    }

    public void setRelation(Integer relationshipId) {
        tvRelation.setText(Utility.getBeneficiaryRelationName(relationshipId));
    }

    public void setDefault(boolean isDefault) {
        clContent.setBackground(isDefault ? TezDrawableHelper.getDrawable(getContext(), R.drawable.wallet_selected_bg) : null);
    }

    public void setRelationIcon(Integer relationshipId) {
        ivIcon.setImageResource(Utility.getBeneficiaryRelationImage(relationshipId));
    }

    public void setOnEditClickListener(DoubleTapSafeOnClickListener listener) {
        rippleIvEdit.setDoubleTapSafeOnClickListener(listener);
        tvAddNumber.setDoubleTapSafeOnClickListener(listener);
    }
}
