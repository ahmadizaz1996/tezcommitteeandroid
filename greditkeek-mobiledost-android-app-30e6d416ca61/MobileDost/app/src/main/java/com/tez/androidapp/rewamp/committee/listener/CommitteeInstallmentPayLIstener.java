package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeInstallmentPayResponse;
import com.tez.androidapp.rewamp.committee.response.PaymentInitiateResponse;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface CommitteeInstallmentPayLIstener {

    void onInstallmentPaySuccess(CommitteeInstallmentPayResponse committeeInstallmentPayResponse);

    void onInstallmentPayFailure(int errorCode, String message);
}
