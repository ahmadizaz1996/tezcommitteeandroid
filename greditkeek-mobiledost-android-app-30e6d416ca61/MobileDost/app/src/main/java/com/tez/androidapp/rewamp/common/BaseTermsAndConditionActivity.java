package com.tez.androidapp.rewamp.common;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezCheckBox;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.general.termsandcond.NavigationTermsAndConditionsActivityRouter;
import com.tez.androidapp.rewamp.signup.TermsAndCondition;
import com.tez.androidapp.rewamp.signup.TermsAndConditionAdapter;

import net.tez.fragment.util.listener.DoubleTapSafeClickableSpan;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.fragment.util.optional.Optional;
import net.tez.viewbinder.library.core.BindView;

import java.util.List;

public abstract class BaseTermsAndConditionActivity extends PlaybackActivity implements CompoundButton.OnCheckedChangeListener {


    @BindView(R.id.recyclerView)
    protected RecyclerView recyclerViewTermsAndCondition;

    @BindView(R.id.tcCheckbox)
    protected TezCheckBox tezCheckBox;

    @BindView(R.id.btMainButton)
    protected TezButton btMainButton;

    @BindView(R.id.textViewDescription)
    protected TezTextView textViewDescription;

    @BindView(R.id.tvDetailedTC)
    protected TezTextView tvDetailedTC;

    @BindView(R.id.shimmer)
    protected TezLinearLayout shimmer;

    @BindView(R.id.nsvContent)
    protected NestedScrollView nsvContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_terms_and_condition);
        this.updateToolbar(findViewById(R.id.toolbar));
        setupViews();
        init();
    }


    protected void init() {
        setAdapter();
        this.btMainButton.setDoubleTapSafeOnClickListener(null);
        this.tezCheckBox.setOnCheckedChangeListener(this);
        initTextToTezCheckBox();
    }

    protected void initTextToTezCheckBox() {
        this.tezCheckBox.setText(getSpannableStringOnTermsAndConditions());
        this.tezCheckBox.setMovementMethod(LinkMovementMethod.getInstance());
    }

    protected void setAdapter() {
        TermsAndConditionAdapter termsAndConditionsAdaptor = new TermsAndConditionAdapter(this.getTermAndConditions());
        this.recyclerViewTermsAndCondition.setAdapter(termsAndConditionsAdaptor);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Optional.doWhen(isChecked, () -> {
                    this.btMainButton.setButtonNormal();
                    setOnClickListenerOnBtMainButton(view -> onClickBtMainButton());
                },
                () -> {
                    this.btMainButton.setButtonInactive();
                    setOnClickListenerOnBtMainButton(null);
                });
    }


    private void setOnClickListenerOnBtMainButton(DoubleTapSafeOnClickListener onClickListener) {
        this.btMainButton.setDoubleTapSafeOnClickListener(onClickListener);
    }

    public void setBtMainButtonEnabled(boolean enabled) {
        this.btMainButton.setEnabled(enabled);
    }

    private SpannableString getSpannableStringOnTermsAndConditions() {
        String string = getString(R.string.i_accept_these_terms_and_conditions);
        String foregroundString = getString(R.string.tc);
        SpannableString spannableString = new SpannableString(string);
        int startIndex = string.indexOf(foregroundString);
        ClickableSpan foregroundColorSpan = new DoubleTapSafeClickableSpan() {

            @Override
            public void doubleTapSafeSpanOnClick(@NonNull View widget) {
                showDetailedTermsCondition();
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        spannableString.setSpan(foregroundColorSpan, startIndex, startIndex + foregroundString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), startIndex, startIndex + foregroundString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.
                setSpan(new ForegroundColorSpan(Utility.getColorFromResource(R.color.textViewTitleColorBlue)),
                        startIndex,
                        startIndex + foregroundString.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    protected void showDetailedTermsCondition(){
        NavigationTermsAndConditionsActivityRouter.createInstance()
                .setDependenciesAndRoute(BaseTermsAndConditionActivity.this);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    protected void setupViews() {

    }

    public void setShimmerVisibility(int visibility) {
        shimmer.setVisibility(visibility);
    }

    public void setNsvContentVisibility(int visibility) {
        nsvContent.setVisibility(visibility);
    }

    protected abstract List<TermsAndCondition> getTermAndConditions();

    protected abstract void onClickBtMainButton();
}
