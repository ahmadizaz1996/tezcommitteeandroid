package com.tez.androidapp.rewamp.committee.view;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeSendMessageResponse;
import com.tez.androidapp.rewamp.committee.response.GetGroupChatMessagesResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeGroupChatActivityView extends IBaseView {
    void showLoader();

    void hideLoader();

    void onGetMessage(GetGroupChatMessagesResponse getGroupChatMessagesResponse);

    void onMessageSent(CommitteeSendMessageResponse committeeSendMessageResponse);
}
