package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.interactor.CommitteeContributeActivityInteractor;
import com.tez.androidapp.rewamp.committee.interactor.CommitteeFilterActivityInteractor;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeWalletResponse;
import com.tez.androidapp.rewamp.committee.view.ICommitteeContibueActivityView;
import com.tez.androidapp.rewamp.committee.view.ICommitteeFilterActivityView;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeFilterActivityPresenter implements ICommitteeFilterActivityPresenter, ICommitteeFilterActivityInteractorOutput {

    private final ICommitteeFilterActivityView iCommitteeFilterActivityView;
    private final CommitteeFilterActivityInteractor committeeFilterActivityInteractor;

    public CommitteeFilterActivityPresenter(ICommitteeFilterActivityView iCommitteeFilterActivityView) {
        this.iCommitteeFilterActivityView = iCommitteeFilterActivityView;
        committeeFilterActivityInteractor = new CommitteeFilterActivityInteractor(this);
    }

    @Override
    public void onGetFilterSuccess(CommitteeFilterResponse committeeFilterResponse) {
        iCommitteeFilterActivityView.hideLoader();
        iCommitteeFilterActivityView.onFilter(committeeFilterResponse);
    }

    @Override
    public void onGetFilterFailure(int errorCode, String message) {
        iCommitteeFilterActivityView.hideLoader();
        iCommitteeFilterActivityView.showError(errorCode,
                (dialog, which) -> iCommitteeFilterActivityView.finishActivity());
    }

    @Override
    public void getCommitteeFilter(String committeeId) {
        iCommitteeFilterActivityView.showLoader();
        committeeFilterActivityInteractor.getFilter(committeeId);

    }
}
