package com.tez.androidapp.app.base.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.utils.app.Constants;
import net.tez.fragment.util.optional.Optional;

import net.tez.logger.library.core.LogStrategy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by FARHAN DHANANI on 10/31/2018.
 */
public class TezLogFileStrategy implements LogStrategy {
    private final File tezLogFile;
    private boolean fileCreatedSuccessfully;

    public TezLogFileStrategy() {
        this.tezLogFile = new File(MobileDostApplication.getInstance().getFilesDir(), Constants.TEZ_LOGS);
        createFile();
    }

    @Override
    public void log(int priority, @Nullable String tag, @NonNull String message) {
        logFile(priority, tag, message);
    }


    private void logFile(int priority, String tag, String message) {
        Optional.doWhen(this.fileCreatedSuccessfully, () -> {
            try (FileOutputStream stream = new FileOutputStream(tezLogFile, true)) {
                String log = priority + " " + (tag == null ? "TEZ_LOG" : tag) + " " + message + "\n";
                stream.write(log.getBytes());
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        });
    }


    private void createFile() {
        Optional.doWhen(this.tezLogFile.exists(),
                () -> this.fileCreatedSuccessfully = true,
                () -> {
                    try {
                        this.fileCreatedSuccessfully = this.tezLogFile.createNewFile();
                    } catch (IOException e) {
                        Crashlytics.logException(e);
                        this.fileCreatedSuccessfully = false;
                    }
                });
    }
}
