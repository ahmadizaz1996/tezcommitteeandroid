package com.tez.androidapp.rewamp.committee.response;

import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CommitteeResponseTimeline implements Serializable {

    @SerializedName("date")
    private String date;
    @SerializedName("inviteList")
    private List<CommitteeFilterResponseInviteList> mInviteList;
    @SerializedName("memberList")
    private List<CommitteeFilterResponseMemberList> mMemberList;
    @SerializedName("transactionList")
    private List<CommitteeResponseFilterTransactionList> mTransactionList;

    private boolean header;

    public boolean isHeader() {
        return header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<CommitteeFilterResponseInviteList> getmInviteList() {
        return mInviteList;
    }

    public void setmInviteList(List<CommitteeFilterResponseInviteList> mInviteList) {
        this.mInviteList = mInviteList;
    }

    public List<CommitteeFilterResponseMemberList> getmMemberList() {
        return mMemberList;
    }

    public void setmMemberList(List<CommitteeFilterResponseMemberList> mMemberList) {
        this.mMemberList = mMemberList;
    }

    public List<CommitteeResponseFilterTransactionList> getmTransactionList() {
        return mTransactionList;
    }

    public void setmTransactionList(List<CommitteeResponseFilterTransactionList> mTransactionList) {
        this.mTransactionList = mTransactionList;
    }
}
