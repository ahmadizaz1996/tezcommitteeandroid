package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeWalletResponse;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeWalletVerificationActivity;
import com.tez.androidapp.rewamp.committee.view.JoinCommitteeVerificationActivity;

public class CommitteeWalletVerificationActivityRouter extends BaseActivityRouter {


    public static final String MY_COMMITTEE_RESPONSE = "MY_COMMITTEE_RESPONSE";
    public static final String COMMITTEE_WALLET_RESPONSE = "COMMITTEE_WALLET_RESPONSE";

    public static CommitteeWalletVerificationActivityRouter createInstance() {
        return new CommitteeWalletVerificationActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, MyCommitteeResponse committeeResponse, CommitteeWalletResponse committeeWalletResponse) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putParcelable(MY_COMMITTEE_RESPONSE, committeeResponse);
        bundle.putParcelable(COMMITTEE_WALLET_RESPONSE, committeeWalletResponse);
        intent.putExtras(bundle);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeWalletVerificationActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

}
