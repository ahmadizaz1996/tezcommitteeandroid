package com.tez.androidapp.rewamp.committee.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.InsuranceCoverageCardView;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/

public class MyCommitteeMembersDetailedAdapter extends GenericRecyclerViewAdapter<MyCommitteeResponse.Committee.Member,
        MyCommitteeMembersDetailedAdapter.MyCommitteeMemberListener,
        MyCommitteeMembersDetailedAdapter.MyCommitteeMemberViewHolder> {

    public MyCommitteeMembersDetailedAdapter(@NonNull List<MyCommitteeResponse.Committee.Member> items, @Nullable MyCommitteeMemberListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public MyCommitteeMemberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.item_committee_members_detailed, parent);
        return new MyCommitteeMemberViewHolder(view);
    }

    public interface MyCommitteeMemberListener extends BaseRecyclerViewListener {
        void onClickMembers(@NonNull MyCommitteeResponse.Committee.Member member);
    }

    static class MyCommitteeMemberViewHolder extends BaseViewHolder<MyCommitteeResponse.Committee.Member, MyCommitteeMemberListener> {

        @BindView(R.id.tvCount)
        private TezTextView tvCount;

        @BindView(R.id.tv_name)
        private TezTextView tvName;

        @BindView(R.id.tv_message)
        private TezTextView tvMessage;

        @BindView(R.id.cv_img_mem)
        private CircleImageView memberImgView;

        @BindView(R.id.detailDotsImgView)
        private TezImageView detailsDotsImgView;

        public MyCommitteeMemberViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(MyCommitteeResponse.Committee.Member item, @Nullable MyCommitteeMemberListener listener) {
            super.onBind(item, listener);

            tvCount.setText(getAdapterPosition() + 1 + "");
            tvName.setText(item.name);
            tvMessage.setText(/*"Round: 26-Dec-2010"  +*/item.round + " ");
            detailsDotsImgView.setDoubleTapSafeOnClickListener(new DoubleTapSafeOnClickListener() {
                @Override
                public void doubleTapSafeOnClick(View view) {
                    Toast.makeText(getContext(), "Coming Soon", Toast.LENGTH_LONG).show();
                }
            });

        }
    }
}
