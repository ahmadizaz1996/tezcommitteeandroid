package com.tez.androidapp.commons.utils.cnic.detection.model;


import androidx.annotation.NonNull;

import java.io.Serializable;

public class RetumDataModel implements Serializable {
    public static final int MAX_SCORE = 4;
    private String updatedTimeStamp;
    private String imageURL;
    private String cnicIssueDate;
    private String cnicExpiryDate;
    private String dateOfBirth;
    private String cnicNumber;
    private String method;
    private String captureMethod;

    public RetumDataModel() {

    }

    public RetumDataModel(RetumDataModel retumDataModel) {
        updatedTimeStamp = retumDataModel.updatedTimeStamp;
        imageURL = retumDataModel.imageURL;
        cnicIssueDate = retumDataModel.cnicIssueDate;
        cnicExpiryDate = retumDataModel.cnicExpiryDate;
        dateOfBirth = retumDataModel.dateOfBirth;
        cnicNumber = retumDataModel.cnicNumber;
        method = retumDataModel.method;
        captureMethod = retumDataModel.captureMethod;
    }

    public static int getFirebaseDataModelScore(RetumDataModel retumDataModel) {
        int score = 0;
        if (retumDataModel == null) return 0;
        if (retumDataModel.getCnicExpiryDate() != null) score++;
        if (retumDataModel.getCnicIssueDate() != null) score++;
        if (retumDataModel.getDateOfBirth() != null) score++;
        if (retumDataModel.getCnicNumber() != null) score++;
        return score;
    }

    /**
     * Compare the result of Firebase Data Model
     *
     * @return 0 if x==y , 1 if x is better than y, -1 if y is better than x, 0
     */
    public static int compareTo(RetumDataModel x, RetumDataModel y) {
        return Integer.compare(getFirebaseDataModelScore(x), getFirebaseDataModelScore(y));
    }

    /**
     * Merge Firebase Data Model x and y.
     * If both of the model contains not null fields then priority will be given to model x.
     *
     * @param x Previously predicted Cnic Data Model
     * @param y Newly Predicted Cnic Data Model
     * @return RetumDataModel with contains the merged results.
     */
    public static RetumDataModel mergeFirebaseDataModel(@NonNull RetumDataModel x, @NonNull RetumDataModel y) {
        RetumDataModel z = new RetumDataModel(x);
        z.setCnicNumber(x.getCnicNumber() == null ? y.getCnicNumber() : x.getCnicNumber());
        return z;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getCnicIssueDate() {
        return cnicIssueDate;
    }

    public void setCnicIssueDate(String cnicIssueDate) {
        this.cnicIssueDate = cnicIssueDate;
    }

    public String getCnicExpiryDate() {
        return cnicExpiryDate;
    }

    public void setCnicExpiryDate(String cnicExpiryDate) {
        this.cnicExpiryDate = cnicExpiryDate;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCnicNumber() {
        return cnicNumber;
    }

    public void setCnicNumber(String cnicNumber) {
        this.cnicNumber = cnicNumber;
    }
}
