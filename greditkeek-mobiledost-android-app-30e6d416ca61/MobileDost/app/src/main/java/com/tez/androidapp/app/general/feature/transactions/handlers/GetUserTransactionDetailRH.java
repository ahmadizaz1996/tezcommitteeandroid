package com.tez.androidapp.app.general.feature.transactions.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.transactions.callbacks.GetLoanTransactionDetailCallback;
import com.tez.androidapp.app.general.feature.transactions.models.network.dto.response.GetLoanTransactionDetailResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 9/2/2017.
 */

public class GetUserTransactionDetailRH extends BaseRH<GetLoanTransactionDetailResponse> {

    private GetLoanTransactionDetailCallback getLoanTransactionDetailCallback;

    public GetUserTransactionDetailRH(BaseCloudDataStore baseCloudDataStore, GetLoanTransactionDetailCallback getLoanTransactionDetailCallback) {
        super(baseCloudDataStore);
        this.getLoanTransactionDetailCallback = getLoanTransactionDetailCallback;
    }

    @Override
    protected void onSuccess(Result<GetLoanTransactionDetailResponse> value) {
        GetLoanTransactionDetailResponse getLoanTransactionDetailResponse = value.response().body();
        if (isErrorFree(getLoanTransactionDetailResponse)) {
            getLoanTransactionDetailCallback.onGetLoanTransactionDetailSuccess(getLoanTransactionDetailResponse.getTransactionDetail());
        } else onFailure(getLoanTransactionDetailResponse.getStatusCode(), getLoanTransactionDetailResponse.getErrorDescription());

    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        getLoanTransactionDetailCallback.onGetLoanTransactionDetailFailure(errorCode, message);
    }
}
