package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.ValidateInfo;

/**
 * Created by FARHAN DHANANI on 12/3/2018.
 */
public class ValidateInfoResponse extends BaseResponse {
    private ValidateInfo validateInfo;

    public ValidateInfo getValidateInfo() {
        return validateInfo;
    }

    public void setValidateInfo(ValidateInfo validateInfo) {
        this.validateInfo = validateInfo;
    }
}
