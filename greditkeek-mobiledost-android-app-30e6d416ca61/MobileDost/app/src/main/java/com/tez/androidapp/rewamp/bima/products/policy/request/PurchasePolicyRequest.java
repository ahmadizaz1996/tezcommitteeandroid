package com.tez.androidapp.rewamp.bima.products.policy.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class PurchasePolicyRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/insurance/policy/purchase";

    private int policyId;
    private String pin;
    private String otp;
    private String paymentCorrelationId;
    private Double lat;
    private Double lng;

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPaymentCorrelationId() {
        return paymentCorrelationId;
    }

    public void setPaymentCorrelationId(String paymentCorrelationId) {
        this.paymentCorrelationId = paymentCorrelationId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
