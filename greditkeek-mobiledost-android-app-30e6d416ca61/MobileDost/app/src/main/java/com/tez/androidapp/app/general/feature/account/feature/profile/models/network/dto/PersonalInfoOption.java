package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto;

import java.util.List;

public class PersonalInfoOption {

    List<AnswerSelected> personalInformationList;

    public PersonalInfoOption(List<AnswerSelected> personalInformationList){
        this.personalInformationList = personalInformationList;
    }

    public List<AnswerSelected> getPersonalInformationList() {
        return personalInformationList;
    }

    public void setPersonalInformationList(List<AnswerSelected> personalInformationList) {
        this.personalInformationList = personalInformationList;
    }
}
