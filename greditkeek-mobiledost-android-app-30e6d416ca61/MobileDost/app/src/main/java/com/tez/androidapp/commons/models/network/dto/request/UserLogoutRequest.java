package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 4/7/2017.
 */

public class UserLogoutRequest extends BaseRequest {

    public static final String METHOD_NAME = "v2/user/logout";
    private String refreshToken;

    public UserLogoutRequest(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
