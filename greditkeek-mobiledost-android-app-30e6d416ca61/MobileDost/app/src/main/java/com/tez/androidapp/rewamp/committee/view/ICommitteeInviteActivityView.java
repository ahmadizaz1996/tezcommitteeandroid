package com.tez.androidapp.rewamp.committee.view;

import com.tez.androidapp.app.base.ui.IBaseView;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeInviteActivityView extends IBaseView {
    void showLoader();

    void hideLoader();

    void onInvite();
}
