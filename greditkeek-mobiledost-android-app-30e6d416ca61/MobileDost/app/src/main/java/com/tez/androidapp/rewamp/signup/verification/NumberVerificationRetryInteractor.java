package com.tez.androidapp.rewamp.signup.verification;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.general.feature.mobile.verification.callback.NumberVerificationViaOtp;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request.MobileVerificationRequest;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request.NumberVerifyGenerateOtpRequest;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import net.tez.fragment.util.optional.Optional;

/**
 * Created by FARHAN DHANANI on 6/5/2019.
 */
public class NumberVerificationRetryInteractor implements INumberVerificationRetryInteractor {

    private final INumberVerificationRetryInteractorOutput iNumberVerificationRetryInteractorOutput;

    public NumberVerificationRetryInteractor(INumberVerificationRetryInteractorOutput iNumberVerificationRetryInteractorOutput) {
        this.iNumberVerificationRetryInteractorOutput = iNumberVerificationRetryInteractorOutput;
    }


    @Override
    public void requestOtp(@NonNull String principalName, @NonNull String mobileNumber, @NonNull Context context) {
        UserCloudDataStore.getInstance().numberVerifyGenerateOtp(
                new NumberVerifyGenerateOtpRequest(principalName, mobileNumber), new NumberVerificationViaOtp() {

                    @NonNull
                    @Override
                    protected String getPrincipalName() {
                        return principalName;
                    }

                    @NonNull
                    @Override
                    protected String getMobileNumber() {
                        return mobileNumber;
                    }

                    @NonNull
                    @Override
                    protected Context getContext() {
                        return context;
                    }

                    @Override
                    protected void onCountDownFinish() {
                        iNumberVerificationRetryInteractorOutput.onCountDownFinish();
                    }

                    @Override
                    protected void onVerifyOtpSuccess() {
                        iNumberVerificationRetryInteractorOutput.onVerified();
                    }

                    @Override
                    protected void onVerifyOtpFailure() {
                        iNumberVerificationRetryInteractorOutput.onCountDownFinish();
                    }


                    @Override
                    public void onNumberVerifyGenerateOtpSuccess() {
                        iNumberVerificationRetryInteractorOutput.onNumberVerifyGenerateOtpSuccess();
                        startCountDownTimer();
                    }

                    @Override
                    public void userSessionTimeOut() {
                        iNumberVerificationRetryInteractorOutput.onUserSessionTimeOut();
                    }

                    @Override
                    public void countDownTimerTick(long time) {
                        iNumberVerificationRetryInteractorOutput.showTime(time);
                    }

                    @Override
                    public void onNumberVerifyGenerateOtpFailure(int errorCode, String message) {
                        Optional.doWhen(Utility.isUnauthorized(errorCode),
                                this::handleUnAuthorizeGenerateOtpRequest,
                                () -> Optional.doWhen(errorCode == ResponseStatusCode.SIGNUP_CACHE_TIMEOUT.getCode(),
                                        this::userSessionTimeOut,
                                        () -> {
                                            sendOtpFailureToServer(mobileNumber, principalName);
                                            onCountDownFinish();
                                        }));
                    }
                });
    }

    @Override
    public void sendFlashCallSuccessToServer(String cnic, String mobileNumber) {
        MobileVerificationRequest mobileVerificationRequest = new MobileVerificationRequest(cnic, mobileNumber);
        mobileVerificationRequest.setVerificationTry(MDPreferenceManager.getFlashCallVerificationFailureCount()
                + MDPreferenceManager.getOtpVerificationFailureCount() + 1);
        UserCloudDataStore.getInstance().mobileNumberVerification(mobileVerificationRequest,
                null);
        Utility.resetFailureCounter();
    }

    @Override
    public void sendFlashCallFailureToServer(String cnic, String mobileNumber) {
        increaseFlashVerificaionFailureCountByOne();
        MobileVerificationRequest mobileVerificationRequest = new MobileVerificationRequest(cnic, mobileNumber);
        mobileVerificationRequest.setStatus(Constants.MobileNumberVerificationStatusFailed);
        UserCloudDataStore.getInstance().mobileNumberVerification(
                mobileVerificationRequest, null);
    }

    @Override
    public void increaseFlashVerificaionFailureCountByOne() {
        MDPreferenceManager.setFlashCallVerificationFailureCount(
                MDPreferenceManager.getFlashCallVerificationFailureCount() + 1);
    }
}
