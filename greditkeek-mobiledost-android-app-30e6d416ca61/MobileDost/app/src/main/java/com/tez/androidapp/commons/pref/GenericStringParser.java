package com.tez.androidapp.commons.pref;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import net.tez.storage.library.converters.ObjectToStringParser;

/**
 * Created by FARHAN DHANANI on 1/18/2019.
 */
public class GenericStringParser implements ObjectToStringParser {
    @Override
    public String convertToString(@NonNull Object obj) {
        Gson gson = new Gson();
        return gson.toJson(obj);
    }
}
