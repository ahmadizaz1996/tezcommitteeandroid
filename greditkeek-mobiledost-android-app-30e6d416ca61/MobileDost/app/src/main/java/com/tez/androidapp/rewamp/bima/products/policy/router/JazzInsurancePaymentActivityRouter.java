package com.tez.androidapp.rewamp.bima.products.policy.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.bima.products.policy.view.JazzInsurancePaymentActivity;

public class JazzInsurancePaymentActivityRouter extends EasyPaisaInsurancePaymentActivityRouter {


    public static JazzInsurancePaymentActivityRouter createInstance() {
        return new JazzInsurancePaymentActivityRouter();
    }

    @Override
    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, JazzInsurancePaymentActivity.class);
    }
}
