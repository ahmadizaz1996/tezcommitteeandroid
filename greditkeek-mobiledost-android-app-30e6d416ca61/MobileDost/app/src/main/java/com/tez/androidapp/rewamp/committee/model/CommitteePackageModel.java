package com.tez.androidapp.rewamp.committee.model;

import java.io.Serializable;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteePackageModel implements Serializable {


    private String committeeName;
    private String startDate;
    private String committeeMethod;
    private int id;
    private int durationInMonths;
    private int installmentAmount;
    private int selectedAmount;
    private int selectedMembers;


    public CommitteePackageModel() {
    }

    public String getCommitteeName() {
        return committeeName;
    }

    public void setCommitteeName(String committeeName) {
        this.committeeName = committeeName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getCommitteeMethod() {
        return committeeMethod;
    }

    public void setCommitteeMethod(String committeeMethod) {
        this.committeeMethod = committeeMethod;
    }

    public CommitteePackageModel(int id, int durationInMonths, int installmentAmount) {
        this.id = id;
        this.durationInMonths = durationInMonths;
        this.installmentAmount = installmentAmount;
    }


    public int getSelectedAmount() {
        return selectedAmount;
    }

    public void setSelectedAmount(int selectedAmount) {
        this.selectedAmount = selectedAmount;
    }

    public int getSelectedMembers() {
        return selectedMembers;
    }

    public void setSelectedMembers(int selectedMembers) {
        this.selectedMembers = selectedMembers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDurationInMonths() {
        return durationInMonths;
    }

    public void setDurationInMonths(int durationInMonths) {
        this.durationInMonths = durationInMonths;
    }

    public int getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(int installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    @Override
    public String toString() {
        return "Package{" +
                "id=" + id +
                ", durationInMonths=" + durationInMonths +
                ", installmentAmount=" + installmentAmount +
                '}';
    }
}

