package com.tez.androidapp.rewamp.general.timer;

public interface ITimerActivityView {

    void startTimer();

    void stopTimer();
}
