package com.tez.androidapp.rewamp.committee.presenter;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface IJoinCommitteeInvitationActivityPresenter {

    void getInvitedPackage(String mobileNumber);
}
