package com.tez.androidapp.rewamp.signup.presenter;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;
import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;
import com.tez.androidapp.rewamp.signup.interactor.INewLoginActivityInteractor;
import com.tez.androidapp.rewamp.signup.interactor.NewLoginActivityInteractor;
import com.tez.androidapp.rewamp.signup.view.INewLoginActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import net.tez.fragment.util.optional.Optional;

public class NewLoginActivityPresenter implements INewLoginActivityPresenter,
        INewLoginActivityInteractorOutput {
    private final INewLoginActivityView iNewLoginActivityView;
    private final INewLoginActivityInteractor iNewLoginActivityInteractor;

    public NewLoginActivityPresenter(INewLoginActivityView iNewLoginActivityView) {
        this.iNewLoginActivityView = iNewLoginActivityView;
        this.iNewLoginActivityInteractor = new NewLoginActivityInteractor(this);
    }

    @Override
    public void callLogin(UserLoginRequest userLoginRequest) {
        this.iNewLoginActivityInteractor.callLogin(userLoginRequest);
    }

    @Override
    public void onUserLoginSuccess(UserLoginResponse loginResponse) {
        iNewLoginActivityView.dismissTezLoader();
        if(loginResponse.getTemporaryPinSet()){
            this.iNewLoginActivityView.onTemporaryPinSet();
        } else if(loginResponse.isUserActivationPending()){
            this.iNewLoginActivityView.onUserReactivateAccount();
        } else {
            this.iNewLoginActivityView.userPinIsVerified();
        }
    }

    @Override
    public void onUserLoginFailure(int errorCode, String message) {
        Optional.doWhen(ResponseStatusCode.USER_LOCKED.getCode() == errorCode,
                iNewLoginActivityView::startRequestFailedActivity, () -> {
                    iNewLoginActivityView.dismissTezLoader();
                    this.iNewLoginActivityView.showError(errorCode,
                            this.iNewLoginActivityView.getDialogOnClickListener());
                });
    }

    @Override
    public void resendOtp(){
        this.iNewLoginActivityInteractor.resendAccountReactivationOtp();
    }


    @Override
    public void onUserAccountReActivateResendOTPSuccess(BaseResponse baseResponse) {
        this.iNewLoginActivityView.startUserReactiviateAccount();
    }

    @Override
    public void onUserAccountReActivateResendOTPFailure(BaseResponse baseResponse) {
        this.iNewLoginActivityView.showError(baseResponse.getStatusCode());
    }
}
