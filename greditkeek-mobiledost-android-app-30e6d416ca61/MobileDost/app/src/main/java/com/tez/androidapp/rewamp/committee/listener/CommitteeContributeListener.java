package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeWalletResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface CommitteeContributeListener {

    void onGetWalletSuccess(CommitteeWalletResponse committeeWalletResponse);

    void onGetWalletFailure(int errorCode, String message);
}
