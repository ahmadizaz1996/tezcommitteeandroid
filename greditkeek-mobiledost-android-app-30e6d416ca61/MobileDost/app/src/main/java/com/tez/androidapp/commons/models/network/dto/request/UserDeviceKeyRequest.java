package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 8/30/2017.
 */

public class UserDeviceKeyRequest extends BaseRequest {
    public static final String METHOD_NAME = "v2/user/deviceKey";
    private String principalName;
    private String deviceKey;
    private String imei;

    public UserDeviceKeyRequest(String principalName, String deviceKey, String imei) {
        this.principalName = principalName;
        this.deviceKey = deviceKey;
        this.imei = imei;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public abstract static class Params {
        public static final String PRINCIPAL_NAME = "principalName";
        public static final String DEVICE_KEY = "deviceKey";
        public static final String IMEI = "imei";
    }
}
