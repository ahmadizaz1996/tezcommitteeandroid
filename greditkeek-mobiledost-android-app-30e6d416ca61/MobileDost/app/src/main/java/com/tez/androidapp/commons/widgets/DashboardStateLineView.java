package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.signup.DashboardState;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VINOD KUMAR on 6/12/2019.
 */
public class DashboardStateLineView extends View implements IBaseWidget {

    private static final float DEFAULT_CIRCLE_RADIUS = 4;
    private static final float DEFAULT_TEXT_MARGIN_BOTTOM = 8;
    private static final float DEFAULT_MIN_TEXT_GAP = 8;
    private static final float DEFAULT_TEXT_FONT_SIZE = 10;
    private static final float DEFAULT_LINE_STROKE_WIDTH = 1;

    private List<DashboardState> stateList;
    private Paint statePaint;
    private Paint linePaint;
    private int circleRadius = dpToPx(getContext(), DEFAULT_CIRCLE_RADIUS);
    private int textMarginBottom = dpToPx(getContext(), DEFAULT_TEXT_MARGIN_BOTTOM);
    private int minTextGap = dpToPx(getContext(), DEFAULT_MIN_TEXT_GAP);
    private int lineSpace = dpToPx(getContext(), 3);


    public DashboardStateLineView(Context context) {
        super(context);
    }

    public DashboardStateLineView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public DashboardStateLineView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    public void setStateList(@NonNull List<DashboardState> stateList) {
        this.stateList = stateList;
        setTextWidths();
        requestLayout();
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        this.initPaint();
        this.initNewUserStatusList();

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.DashboardStateLineView);

        try {

            float scaleFactor = typedArray.getFloat(R.styleable.DashboardStateLineView_scale_size, 0);
            if (scaleFactor > 0)
                scaleViews(scaleFactor);

            int fontSize = typedArray.getDimensionPixelSize(R.styleable.DashboardStateLineView_font_size, 0);
            if (fontSize > 0)
                statePaint.setTextSize(fontSize);

            int circleRadius = typedArray.getDimensionPixelSize(R.styleable.DashboardStateLineView_circle_radius, 0);
            if (circleRadius > 0)
                this.circleRadius = circleRadius;

            int textMgBtm = typedArray.getDimensionPixelSize(R.styleable.DashboardStateLineView_text_margin_bottom, 0);
            if (textMgBtm > 0)
                this.textMarginBottom = textMgBtm;

            int minTextGap = typedArray.getDimensionPixelSize(R.styleable.DashboardStateLineView_min_text_gap, 0);
            if (minTextGap > 0)
                this.minTextGap = minTextGap;

            int lineStrokeWidth = typedArray.getDimensionPixelSize(R.styleable.DashboardStateLineView_line_stroke_width, 0);
            if (lineStrokeWidth > 0)
                this.linePaint.setStrokeWidth(lineStrokeWidth);

            setTextWidths();

        } finally {

            typedArray.recycle();
        }
    }

    private void initPaint() {
        this.statePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.linePaint.setStrokeWidth(dpToPx(getContext(), DEFAULT_LINE_STROKE_WIDTH));
        this.statePaint.setTextSize(spToPx(getContext(), DEFAULT_TEXT_FONT_SIZE));
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
            this.statePaint.setTypeface(ResourcesCompat.getFont(getContext(), R.font.barlow_medium));
        else
            this.statePaint.setTypeface(getResources().getFont(R.font.barlow_medium));
    }

    private void initNewUserStatusList() {
        stateList = new ArrayList<>();
        for (int i = 0; i < 5; i++)
            stateList.add(new DashboardState("", DashboardState.State.LOCKED));
    }

    private void setTextWidths() {
        Rect rect = new Rect();
        for (DashboardState state : stateList) {
            int maxWidth = 0;
            String[] texts = state.getStatus().split(" ");
            for (String text : texts) {
                int width = getTextWidth(rect, text);
                if (width > maxWidth)
                    maxWidth = width;
            }

            state.setTextWidth(maxWidth);
        }
    }

    private int getTextWidth(Rect bounds, String text) {
        statePaint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.width();
    }

    private int getAllTextHeights(Rect bounds, String[] texts) {
        int height = 0;
        for (String text : texts)
            height += getTextHeight(bounds, text) + lineSpace;
        return height;
    }

    private int getTextHeight(Rect bounds, String text) {
        statePaint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.height();
    }

    private void scaleViews(float scaleFactor) {
        circleRadius = dpToPx(getContext(), DEFAULT_CIRCLE_RADIUS * scaleFactor);
        textMarginBottom = dpToPx(getContext(), DEFAULT_TEXT_MARGIN_BOTTOM * scaleFactor);
        minTextGap = dpToPx(getContext(), DEFAULT_MIN_TEXT_GAP * scaleFactor);
        statePaint.setTextSize(spToPx(getContext(), DEFAULT_TEXT_FONT_SIZE * scaleFactor));
        linePaint.setStrokeWidth(dpToPx(getContext(), DEFAULT_LINE_STROKE_WIDTH * scaleFactor));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        switch (widthMode) {

            case MeasureSpec.UNSPECIFIED:
                width = calculateWidth();
                break;

            case MeasureSpec.AT_MOST:
                width = calculateWidth();
                if (width > widthSize)
                    width = widthSize;
                break;

            default:
            case MeasureSpec.EXACTLY:
                width = widthSize;
        }


        switch (heightMode) {

            case MeasureSpec.UNSPECIFIED:
                height = calculateHeight();
                break;

            case MeasureSpec.AT_MOST:
                height = calculateHeight();
                if (height > heightSize)
                    height = heightSize;
                break;

            default:
            case MeasureSpec.EXACTLY:
                height = heightSize;
        }

        this.setMeasuredDimension(width, height);
    }

    private int calculateWidth() {
        int totalWidth = getTextWidths() + (stateList.size() - 1) * minTextGap;
        return getLeftStartPadding(this)
                + totalWidth
                + getRightEndPadding(this);
    }

    private int getTextWidths() {
        int totalWidth = 0;

        for (DashboardState status : stateList)
            totalWidth += status.getTextWidth();

        return totalWidth;
    }

    private int calculateHeight() {
        return circleRadius * 2
                + calculateMaxHeightOfStatusTexts()
                + textMarginBottom
                + getPaddingTop()
                + getPaddingBottom();
    }

    private int calculateMaxHeightOfStatusTexts() {
        Rect bounds = new Rect();
        int maxHeight = 0;
        for (DashboardState state : stateList) {
            int height = getAllTextHeights(bounds, state.getStatus().split(" "));
            if (height > maxHeight)
                maxHeight = height;
        }
        return maxHeight;
    }

    private boolean isLtr() {
        return ViewCompat.getLayoutDirection(this) == ViewCompat.LAYOUT_DIRECTION_LTR;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (isLtr())
            onDrawLtr(canvas);
        else
            onDrawRtl(canvas);

    }

    private void onDrawLtr(Canvas canvas) {

        final int maxHeight = calculateMaxHeightOfStatusTexts();

        int startX = getLeftStartPadding(this);

        final int startY = Math.abs(getPaddingTop() - getPaddingBottom());

        final int availableWidth = getWidth() - getRightEndPadding(this) - startX;

        final Rect bounds = new Rect();

        int textGap = (availableWidth - getTextWidths()) / (stateList.size() - 1);

        if (textGap < minTextGap)
            textGap = minTextGap;


        for (int i = 0; i < stateList.size(); i++) {

            DashboardState status = stateList.get(i);

            this.setStatusPaintColor(status.getState());

            final String[] texts = status.getStatus().split(" ");

            int centerY = startY + (maxHeight - getAllTextHeights(bounds, texts)) / 2;

            int centerX;

            for (String currentText : texts) {
                int height = getTextHeight(bounds, currentText);
                centerX = startX + (status.getTextWidth() - getTextWidth(bounds, currentText)) / 2;
                canvas.drawText(currentText, centerX, centerY + height, statePaint);
                centerY += height + lineSpace;
            }


            final int cx = startX + (status.getTextWidth() / 2);
            final int cy = startY + maxHeight + circleRadius + textMarginBottom;

            canvas.drawCircle(cx, cy, circleRadius, statePaint);

            if (i + 1 < stateList.size()) {

                this.setLinePaintColor(stateList.get(i + 1).getState());

                final int nextTextMiddlePixel = (status.getTextWidth() / 2) + textGap + (stateList.get(i + 1).getTextWidth() / 2);

                final int stopXLine = cx + nextTextMiddlePixel - circleRadius;

                canvas.drawLine(cx + circleRadius, cy, stopXLine, cy, linePaint);
            }

            startX += status.getTextWidth() + textGap;
        }
    }

    private void onDrawRtl(Canvas canvas) {

        final int maxHeight = calculateMaxHeightOfStatusTexts();

        int startX = getWidth() - getRightEndPadding(this);

        final int startY = Math.abs(getPaddingTop() - getPaddingBottom());

        final int availableWidth = getWidth() - getRightEndPadding(this) - getLeftStartPadding(this);

        final Rect bounds = new Rect();

        int textGap = (availableWidth - getTextWidths()) / (stateList.size() - 1);

        if (textGap < minTextGap)
            textGap = minTextGap;


        for (int i = 0; i < stateList.size(); i++) {

            DashboardState status = stateList.get(i);

            this.setStatusPaintColor(status.getState());

            final String[] texts = status.getStatus().split(" ");

            int centerY = startY + (maxHeight - getAllTextHeights(bounds, texts)) / 2;

            int centerX;

            for (String currentText : texts) {
                int height = getTextHeight(bounds, currentText);
                int width = getTextWidth(bounds, currentText);
                centerX = startX - width - (status.getTextWidth() - width) / 2;
                canvas.drawText(currentText, centerX, centerY + height, statePaint);
                centerY += height + lineSpace;
            }


            final int cx = startX - (status.getTextWidth() / 2);
            final int cy = startY + maxHeight + circleRadius + textMarginBottom;

            canvas.drawCircle(cx, cy, circleRadius, statePaint);

            if (i + 1 < stateList.size()) {

                this.setLinePaintColor(stateList.get(i + 1).getState());

                final int nextTextMiddlePixel = (status.getTextWidth() / 2) + textGap + (stateList.get(i + 1).getTextWidth() / 2);

                final int stopXLine = cx - nextTextMiddlePixel + circleRadius;

                canvas.drawLine(cx - circleRadius, cy, stopXLine, cy, linePaint);
            }

            startX = startX - status.getTextWidth() - textGap;
        }
    }


    private void setStatusPaintColor(DashboardState.State state) {

        switch (state) {

            case LOCKED:
                statePaint.setColor(getColor(getContext(), R.color.editTextBottomLineColorGrey));
                break;

            case UNLOCKED:
                statePaint.setColor(getColor(getContext(), R.color.editTextTextColorGrey));
                break;

            case PASSED:
                statePaint.setColor(getColor(getContext(), R.color.textViewTextColorGreen));
                break;

            case REJECTED:
                statePaint.setColor(getColor(getContext(), R.color.stateRejectedColorRed));
                break;
        }
    }

    private void setLinePaintColor(DashboardState.State next) {

        switch (next) {

            case UNLOCKED:
            case LOCKED:
                linePaint.setColor(getColor(getContext(), R.color.editTextBottomLineColorGrey));
                break;

            case PASSED:
                linePaint.setColor(getColor(getContext(), R.color.textViewTextColorGreen));
                break;

            case REJECTED:
                linePaint.setColor(getColor(getContext(), R.color.stateRejectedColorRed));
                break;
        }
    }

    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {

    }
}
