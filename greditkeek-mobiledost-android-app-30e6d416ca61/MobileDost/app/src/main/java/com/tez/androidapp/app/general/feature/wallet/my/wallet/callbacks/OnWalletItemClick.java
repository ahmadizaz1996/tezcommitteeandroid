package com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;

import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;

/**
 * Created by Rehman Murad Ali on 12/7/2017.
 */

public interface OnWalletItemClick extends BaseRecyclerViewListener {

    void setDefaultWallet(Wallet wallet);

    void deleteWallet(Wallet wallet);

    void selectWalletForPayment(Wallet wallet);
}
