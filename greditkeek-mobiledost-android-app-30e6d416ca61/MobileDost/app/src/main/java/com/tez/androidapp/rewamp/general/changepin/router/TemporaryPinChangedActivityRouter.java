package com.tez.androidapp.rewamp.general.changepin.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.changepin.view.TemporaryPinChangedActivity;

public class TemporaryPinChangedActivityRouter extends BaseActivityRouter {

    public static TemporaryPinChangedActivityRouter createInstance() {
        return new TemporaryPinChangedActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, TemporaryPinChangedActivity.class);
    }
}
