package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.interactor.CommitteeActivityInteractor;
import com.tez.androidapp.rewamp.committee.interactor.JoinCommitteeVerificationActivityInteractor;
import com.tez.androidapp.rewamp.committee.request.JoinCommitteeVerificationRequest;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeVerificationResponse;
import com.tez.androidapp.rewamp.committee.view.ICommitteeActivityView;
import com.tez.androidapp.rewamp.committee.view.IJoinCommitteeVerificationActivityView;

/**
 * Created by Ahmad Izaz on 28-Nov-20
 **/
public class JoinComitteeVerificationPresenter implements IJoinCommitteeVerificationActivityPresenter, IJoinVerificationCommitteeActivityInteractorOutput {


    private final IJoinCommitteeVerificationActivityView mIJoinCommitteeVerificationActivityView;
    private final JoinCommitteeVerificationActivityInteractor mJoinCommitteeVerificationActivityInteractor;


    public JoinComitteeVerificationPresenter(IJoinCommitteeVerificationActivityView iJoinCommitteeVerificationActivityView) {
        this.mIJoinCommitteeVerificationActivityView = iJoinCommitteeVerificationActivityView;
        mJoinCommitteeVerificationActivityInteractor = new JoinCommitteeVerificationActivityInteractor(this);
    }

    @Override
    public void verifyCommitteeOtp(JoinCommitteeVerificationRequest joinCommitteeVerificationRequestp) {
        mIJoinCommitteeVerificationActivityView.showLoader();
        mJoinCommitteeVerificationActivityInteractor.verifyOtp(joinCommitteeVerificationRequestp);
    }

    @Override
    public void onJoinCommitteeVerificationSuccess(JoinCommitteeVerificationResponse joinCommitteeVerificationResponse) {
        mIJoinCommitteeVerificationActivityView.hideLoader();
        if (joinCommitteeVerificationResponse != null)
            mIJoinCommitteeVerificationActivityView.onOtpVerification(joinCommitteeVerificationResponse);
    }

    @Override
    public void onJoinCommitteeVerificationFailure(int errorCode, String message) {
        mIJoinCommitteeVerificationActivityView.showError(errorCode,
                (dialog, which) -> mIJoinCommitteeVerificationActivityView.finishActivity());
    }
}
