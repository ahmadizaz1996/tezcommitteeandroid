package com.tez.androidapp.rewamp.general.notification.view.model;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.tez.androidapp.app.general.feature.notification.models.netwrok.Notification;
import com.tez.androidapp.rewamp.general.notification.data.source.NetworkState;
import com.tez.androidapp.rewamp.general.notification.data.source.NotificationDataSource;
import com.tez.androidapp.rewamp.general.notification.data.source.NotificationDataSourceFactory;

import java.sql.Wrapper;

public class NotificationViewModel extends ViewModel {
    private LiveData<PagedList<Notification>> itemPagedList;
    private LiveData<NetworkState> networkState;

    public NotificationViewModel(){
        NotificationDataSourceFactory itemDataSourceFactory = new NotificationDataSourceFactory();

       networkState = Transformations.switchMap(itemDataSourceFactory.getItemNotificationDataSource(),
               NotificationDataSource::getNetworkState);
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setInitialLoadSizeHint(NotificationDataSource.PAGE_SIZE*2)
                        .setPageSize(NotificationDataSource.PAGE_SIZE).build();
        itemPagedList = (new LivePagedListBuilder(itemDataSourceFactory, pagedListConfig))
                .build();
    }

    public void setObserverToPagedList(LifecycleOwner lifecycleOwner,
                                       Observer<PagedList<Notification>> notificationObserver){
        this.itemPagedList.observe(lifecycleOwner, notificationObserver);
    }

    public void setObserverToNetworkState(LifecycleOwner lifecycleOwner,
                                       @NonNull Observer<NetworkState> notificationObserver){

        this.networkState.observe(lifecycleOwner, notificationObserver);
    }


}
