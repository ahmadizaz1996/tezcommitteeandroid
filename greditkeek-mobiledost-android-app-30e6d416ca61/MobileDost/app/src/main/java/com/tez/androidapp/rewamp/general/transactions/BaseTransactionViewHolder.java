package com.tez.androidapp.rewamp.general.transactions;

import android.view.View;

import com.tez.androidapp.app.general.feature.transactions.models.network.Transaction;

import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.ViewBinder;

abstract class BaseTransactionViewHolder extends BaseViewHolder<Transaction, MyTransactionsAdapter.MyTransactionListener> {

    BaseTransactionViewHolder(View itemView) {
        super(itemView);
        ViewBinder.bind(this, itemView);
    }
}
