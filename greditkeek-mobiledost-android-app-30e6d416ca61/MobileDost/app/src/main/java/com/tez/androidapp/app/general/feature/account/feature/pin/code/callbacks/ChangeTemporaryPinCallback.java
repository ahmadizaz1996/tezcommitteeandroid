package com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created  on 9/15/2017.
 */

public interface ChangeTemporaryPinCallback {

    void onChangeTemporaryPinSuccess();

    void onChangeTemporaryPinFailure(int errorCode, String message);
}
