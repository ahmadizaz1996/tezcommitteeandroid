package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;

import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeCompletionActivityPresenter {
    void createCommittee(CommitteeCreateRequest committeeCreateRequest);
}
