package com.tez.androidapp.rewamp.committee.view;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeDeclineResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeLeaveResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeSummaryActivityView extends IBaseView {
    void showLoader();

    void hideLoader();

    void onInviteMembers();

    void onLeaveCommittee(CommitteeLeaveResponse committeeLeaveResponse);

    void onDeclineCommittee(CommitteeDeclineResponse committeeDeclineResponse);

    void onJoinCommittee(JoinCommitteeResponse joinCommitteeResponse);
}
