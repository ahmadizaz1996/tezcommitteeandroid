package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Claim;

import java.util.Collections;
import java.util.List;

/**
 * Created by FARHAN DHANANI on 6/23/2018.
 */
public class MyClaimListResponse extends BaseResponse {

    private List<Claim> claims;

    public List<Claim> getClaims() {
        return claims;
    }

    public void setClaims(List<Claim> claims) {
        this.claims = claims;
    }

    @Override
    public String toString() {
        return "MyClaimList{" +
                "claims=" + Collections.singletonList(this.claims) +
                "} " + super.toString();
    }
}
