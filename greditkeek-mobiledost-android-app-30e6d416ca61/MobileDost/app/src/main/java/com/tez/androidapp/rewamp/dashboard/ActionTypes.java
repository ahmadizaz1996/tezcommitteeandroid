package com.tez.androidapp.rewamp.dashboard;


public enum ActionTypes {
    PROFILE_STATUS,
    ADD_MOBILE_WALLET,
    REPAY,
    FEEDBACK,
    VERIFICATION_FAILED,
    LIMIT_REJECTED,
    LIMIT_DENIED,
    LIMIT_EXPIRED,
    DISBURSEMENT_FAILED,
    PREVIOUS_ACTION
}
