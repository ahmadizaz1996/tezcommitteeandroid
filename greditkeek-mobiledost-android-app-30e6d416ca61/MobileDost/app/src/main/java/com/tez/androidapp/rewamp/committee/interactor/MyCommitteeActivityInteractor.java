package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.MyCommitteeListener;
import com.tez.androidapp.rewamp.committee.presenter.IMyCommitteeActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class MyCommitteeActivityInteractor implements IMyCommitteeInteractor {


    private final IMyCommitteeActivityInteractorOutput iCommitteeInviteActivityInteractorOutput;

    public MyCommitteeActivityInteractor(IMyCommitteeActivityInteractorOutput iCommitteeInviteActivityInteractorOutput) {
        this.iCommitteeInviteActivityInteractorOutput = iCommitteeInviteActivityInteractorOutput;
    }

    /*@Override
    public void getInvite(List<CommitteeInviteRequest> committeeInviteRequest) {
        CommitteeAuthCloudDataStore.getInstance().getInvite(committeeInviteRequest, new CommitteeInviteListener() {
            @Override
            public void onInviteSuccess() {
                iCommitteeInviteActivityInteractorOutput.onInviteSuccess();
            }

            @Override
            public void onInviteFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getInvite(committeeInviteRequest);
                else
                    iCommitteeInviteActivityInteractorOutput.onInviteFailure(errorCode, message);
            }
        });


    }*/

    @Override
    public void getMyCommittee() {
        CommitteeAuthCloudDataStore.getInstance().getMyCommittee(new MyCommitteeListener() {
            @Override
            public void onMyCommitteeSuccess(MyCommitteeResponse myCommitteeResponse) {
                iCommitteeInviteActivityInteractorOutput.onMyCommitteeSuccess(myCommitteeResponse);
            }

            @Override
            public void onMyCommitteeFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getMyCommittee();
                else
                    iCommitteeInviteActivityInteractorOutput.onMyCommitteeFailure(errorCode, message);
            }
        });
    }
}
