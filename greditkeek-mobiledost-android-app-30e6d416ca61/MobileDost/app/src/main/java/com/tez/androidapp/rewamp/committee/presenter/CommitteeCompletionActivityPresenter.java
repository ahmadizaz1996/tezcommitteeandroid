package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.interactor.CommitteeCompletionActivityInteractor;
import com.tez.androidapp.rewamp.committee.interactor.CommitteeInviteActivityInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.ICommitteeCompletionActivityView;
import com.tez.androidapp.rewamp.committee.view.ICommitteeInviteActivityView;

import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeCompletionActivityPresenter implements ICommitteeCompletionActivityPresenter, ICommitteeCompletionActivityInteractorOutput {

    private final ICommitteeCompletionActivityView mICommitteeCompletionActivityView;
    private final CommitteeCompletionActivityInteractor mCommitteeCompletionActivityInteractor;

    public CommitteeCompletionActivityPresenter(ICommitteeCompletionActivityView mICommitteeCompletionActivityView) {
        this.mICommitteeCompletionActivityView = mICommitteeCompletionActivityView;
        mCommitteeCompletionActivityInteractor = new CommitteeCompletionActivityInteractor(this);
    }

    @Override
    public void onCommitteeCreationSuccess(CommitteeCreateResponse committeeCreateResponse) {
        mICommitteeCompletionActivityView.hideLoader();
        mICommitteeCompletionActivityView.onCommiteeCreation(committeeCreateResponse);
    }

    @Override
    public void onCommitteeCreationFailure(int errorCode, String message) {
        mICommitteeCompletionActivityView.hideLoader();
        mICommitteeCompletionActivityView.showError(errorCode,
                (dialog, which) -> mICommitteeCompletionActivityView.finishActivity());
    }

    @Override
    public void createCommittee(CommitteeCreateRequest committeeCreateRequest) {
        mICommitteeCompletionActivityView.showLoader();
        mCommitteeCompletionActivityInteractor.createCommittee(committeeCreateRequest);
    }

    /*@Override
    public void getInvite(List<CommitteeInviteRequest> committeeInviteRequest) {
        iCommitteeInviteActivityView.showLoader();
        committeeInviteActivityInteractor.getInvite(committeeInviteRequest);
    }

    @Override
    public void onInviteSuccess() {
        iCommitteeInviteActivityView.hideLoader();
        iCommitteeInviteActivityView.onInvite();
    }

    @Override
    public void onInviteFailure(int errorCode, String message) {
        iCommitteeInviteActivityView.hideLoader();
        iCommitteeInviteActivityView.showError(errorCode,
                (dialog, which) -> iCommitteeInviteActivityView.finishActivity());
    }*/

}
