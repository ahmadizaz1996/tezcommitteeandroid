package com.tez.androidapp.rewamp.advance.request.entity;

import java.io.Serializable;

public class ProcessingFee implements Serializable {

    private Integer id;
    private Integer walletProviderId;
    private Double processingFee;
    private Integer minDisbursementAmount;
    private Integer maxDisbursementAmount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWalletProviderId() {
        return walletProviderId;
    }

    public void setWalletProviderId(Integer walletProviderId) {
        this.walletProviderId = walletProviderId;
    }

    public Double getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(Double processingFee) {
        this.processingFee = processingFee;
    }

    public Integer getMinDisbursementAmount() {
        return minDisbursementAmount;
    }

    public void setMinDisbursementAmount(Integer minDisbursementAmount) {
        this.minDisbursementAmount = minDisbursementAmount;
    }

    public Integer getMaxDisbursementAmount() {
        return maxDisbursementAmount;
    }

    public void setMaxDisbursementAmount(Integer maxDisbursementAmount) {
        this.maxDisbursementAmount = maxDisbursementAmount;
    }
}
