package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.ExtentionSize;

import java.util.List;

/**
 * Created by FARHAN DHANANI on 7/20/2018.
 */
public class GetSizeForExtentionResponse extends BaseResponse{

    private List<ExtentionSize> nameValueList;

    public List<ExtentionSize> getNameValueList() {
        return nameValueList;
    }

    public void setNameValueList(List<ExtentionSize> nameValueList) {
        this.nameValueList = nameValueList;
    }
}
