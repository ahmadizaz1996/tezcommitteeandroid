package com.tez.androidapp.rewamp.profile.complete.presenter;

import androidx.annotation.NonNull;

import com.google.android.gms.vision.L;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetCompleteUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateProfileWithCnicCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.UserProfile;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateProfileWithCnicRequest;
import com.tez.androidapp.commons.models.app.City;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.rewamp.profile.trust.interactor.IProfileTrustActivityInteractor;
import com.tez.androidapp.rewamp.profile.trust.presenter.IProfileTrustActivityInteractorOutput;

import java.util.List;

public interface ICnicInformationFragmentInteractorOutput
        extends IProfileTrustActivityInteractorOutput {
    void onCompleteUserProfileSuccess(UserProfile userProfile,
                                      User user,
                                      @NonNull List<City> cities,
                                      @NonNull List<Option> maritalStatusOptions);
    void onCompleteUserProfileFailure(int errorCode, String message);

    void onSuccessUpdateProfileWithCnicCallback(final boolean isPartiallRequest, AnswerSelected answerSelected);

    void onFailureUpdateProfileWithCnicCallback(int errorCode, String message);
}
