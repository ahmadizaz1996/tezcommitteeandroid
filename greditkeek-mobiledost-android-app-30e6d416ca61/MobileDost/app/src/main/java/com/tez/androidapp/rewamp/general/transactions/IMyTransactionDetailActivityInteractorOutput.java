package com.tez.androidapp.rewamp.general.transactions;

import com.tez.androidapp.app.general.feature.transactions.callbacks.GetLoanTransactionDetailCallback;

/**
 * Created by VINOD KUMAR on 8/28/2019.
 */
public interface IMyTransactionDetailActivityInteractorOutput extends GetLoanTransactionDetailCallback {
}
