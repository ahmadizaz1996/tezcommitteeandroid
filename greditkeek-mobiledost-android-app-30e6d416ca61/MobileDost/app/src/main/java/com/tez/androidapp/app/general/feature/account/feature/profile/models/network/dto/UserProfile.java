package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto;

public class UserProfile {
    private Integer requestId;
    private String[] cnicUploads;
    private Boolean isEditable;

    public String[] getCnicUploads() {
        return cnicUploads;
    }

    public void setCnicUploads(String[] cnicUploads) {
        this.cnicUploads = cnicUploads;
    }

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public Boolean getEditable() {
        return isEditable==null? true: isEditable;
    }
}
