package com.tez.androidapp.app.vertical.advance.loan.apply.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

import java.io.Serializable;

public class LoanPricingDetailsRequest extends BaseRequest implements Serializable {
    public static final String METHOD_NAME = "v1/loan/pricing";
}
