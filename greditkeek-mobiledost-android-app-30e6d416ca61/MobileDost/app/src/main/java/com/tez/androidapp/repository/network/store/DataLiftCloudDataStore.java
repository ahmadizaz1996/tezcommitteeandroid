package com.tez.androidapp.repository.network.store;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.response.LoanLimitResponse;
import com.tez.androidapp.commons.api.client.DataLiftAPI;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.dto.request.UploadDeviceDataRequest;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import retrofit2.Response;

/**
 * Data store to make DataLift network calls
 * <p>
 * Created  on 3/2/2017.
 */

public class DataLiftCloudDataStore extends EncryptionBaseCloudDataStore {

    private static final String APP_ZIP_FILE_NAME = "application/x-zip-compressed";
    private static DataLiftCloudDataStore dataLiftCloudDataStore;
    private DataLiftAPI dataLiftAPI;

    private DataLiftCloudDataStore() {
        super();
        dataLiftAPI = tezAPIBuilder.build().create(DataLiftAPI.class);
    }

    public static void clear() {
        dataLiftCloudDataStore = null;
    }

    public static DataLiftCloudDataStore getInstance() {
        if (dataLiftCloudDataStore == null)
            dataLiftCloudDataStore = new DataLiftCloudDataStore();
        return dataLiftCloudDataStore;
    }


    /**
     * This is a synchronized call as it is made from the {@link com.tez.androidapp.commons.utils.extract.ExtractionIntentService ExtractionIntentService }
     *
     * @param zipFile The file to upload
     */
    public void uploadDeviceData(File zipFile) {
        Response<LoanLimitResponse> rawResponse;
        try {
            MultipartBody.Part file = MultipartBody.Part.createFormData(UploadDeviceDataRequest.Params.FILE, zipFile.getName(),
                    MultipartBody.create(MediaType.parse(APP_ZIP_FILE_NAME), zipFile));
            String stringImei = Utility.getImei(getAppContext());
            MultipartBody.Part imei = MultipartBody.Part.createFormData(UploadDeviceDataRequest.Params.IMEI, stringImei == null ? "" : stringImei);
            MultipartBody.Part weeklyUpload = MultipartBody.Part.createFormData("fileUploadFlag", String.valueOf(2));
            MultipartBody.Part principalName = MultipartBody.Part.createFormData(UploadDeviceDataRequest.Params.PRINCIPAL_NAME, MDPreferenceManager.getPrincipalName());
            if (Utility.getImei(this.getAppContext()) == null || MDPreferenceManager.getPrincipalName() == null) {
                throw new IOException();
            }
            String[] location = MDPreferenceManager.getLastLocation();
            if (Utility.isLocationValidInSharedPreferences()) {
                MultipartBody.Part lat = MultipartBody.Part.createFormData(UploadDeviceDataRequest.Params.LATITUDE, location[0]);
                MultipartBody.Part lng = MultipartBody.Part.createFormData(UploadDeviceDataRequest.Params.LONGITUDE, location[1]);
                rawResponse = dataLiftAPI.uploadDeviceData(Utility.getAPIKey(), principalName, imei, weeklyUpload, lat, lng, file).execute();
            } else
                rawResponse = dataLiftAPI.uploadDeviceData(Utility.getAPIKey(), principalName, imei, weeklyUpload, file).execute();
            if (rawResponse.isSuccessful()) {
                MDPreferenceManager.setUploadDeviceDataReady(false);
            }
        } catch (SocketTimeoutException | ConnectException e) {
            MDPreferenceManager.setUploadDeviceDataReady(true);
        } catch (IOException e) {
            if (Utility.getImei(getAppContext()) == null)
                Crashlytics.logException(new Exception("Imei is null on upload data"));

            if (MDPreferenceManager.getPrincipalName() == null)
                Crashlytics.logException(new Exception("Principal Name is null on upload data"));
        }
    }

    public String uploadDeviceDataWithoutLoan(File zipFile) {
        MultipartBody.Part file = MultipartBody.Part.createFormData(UploadDeviceDataRequest.Params.FILE, zipFile.getName(),
                MultipartBody.create(MediaType.parse(APP_ZIP_FILE_NAME), zipFile));
        String stringImei = Utility.getImei(getAppContext());
        MultipartBody.Part imei = MultipartBody.Part.createFormData(UploadDeviceDataRequest.Params.IMEI, stringImei == null ? "" : stringImei);
        MultipartBody.Part weeklyUpload = MultipartBody.Part.createFormData("fileUploadFlag", String.valueOf(3));
        MultipartBody.Part principalName = MultipartBody.Part.createFormData(UploadDeviceDataRequest.Params.PRINCIPAL_NAME, MDPreferenceManager.getPrincipalName());
        Response<BaseResponse> bodyResponse;
        String response;
        try {
            if (Utility.getImei(this.getAppContext()) == null || MDPreferenceManager.getPrincipalName() == null) {
                throw new IOException("Sign out");
            }
            bodyResponse = dataLiftAPI.uploadDeviceDataWithoutLoan(Utility.getAPIKey(), principalName, file, imei, weeklyUpload).execute();
            if (bodyResponse.isSuccessful()) {
                BaseResponse baseResponse = bodyResponse.body();
                if (baseResponse != null && baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
                    response = Constants.SUCCESS;
                else response = new Gson().toJson(baseResponse);
            } else {
                response = bodyResponse.errorBody() == null ? "" : bodyResponse.errorBody().string();
            }
        } catch (IOException e) {
            Crashlytics.logException(e);
            response = e.getMessage();
        }
        return response;
    }

    private Context getAppContext() {
        return MobileDostApplication.getInstance().getApplicationContext();
    }
}
