package com.tez.androidapp.repository.network.handlers.callbacks;

import com.tez.androidapp.repository.network.models.response.VerifyActiveDeviceResponse;

/**
 * Created by Rehman Murad Ali on 4/19/2018.
 */
public interface VerifyActiveDeviceCallback {

    void onVerifyActiveDeviceSuccess(VerifyActiveDeviceResponse verifyActiveDeviceResponse);

    void onVerifyActiveDeviceFailure(int errorCode,String message);
}
