package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesResponse;
import com.tez.androidapp.rewamp.committee.response.GetInvitedPackageResponse;

import java.util.List;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface JoinCommitteeInvitationListener {

    void onGetInvitedPackageSuccess(GetInvitedPackageResponse getInvitedPackageResponse);

    void onGetInvitedPackageFailure(int errorCode, String message);

}
