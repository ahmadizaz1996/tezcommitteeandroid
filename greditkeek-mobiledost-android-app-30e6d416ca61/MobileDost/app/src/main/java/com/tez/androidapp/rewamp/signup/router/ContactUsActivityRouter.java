package com.tez.androidapp.rewamp.signup.router;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.contact.us.ui.activities.ContactUsActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class ContactUsActivityRouter extends BaseActivityRouter {

    public static ContactUsActivityRouter createInstance() {
        return new ContactUsActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    public void setDependenciesAndRoute(@NonNull Context context) {
        context.startActivity(new Intent(context, ContactUsActivity.class));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, ContactUsActivity.class);
    }
}
