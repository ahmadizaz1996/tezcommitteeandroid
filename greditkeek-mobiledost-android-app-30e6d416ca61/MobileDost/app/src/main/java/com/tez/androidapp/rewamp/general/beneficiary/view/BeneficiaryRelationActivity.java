package com.tez.androidapp.rewamp.general.beneficiary.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.app.vertical.bima.shared.models.BimaRelation;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.rewamp.general.beneficiary.adapter.BeneficiaryRelationAdapter;
import com.tez.androidapp.rewamp.general.beneficiary.router.BeneficiaryRelationActivityRouter;

import java.util.ArrayList;
import java.util.List;

public class BeneficiaryRelationActivity extends PlaybackActivity implements BeneficiaryRelationAdapter.BeneficiaryRelationListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beneficiary_relation);
        this.tezToolbar.setToolbarTitle(R.string.manage_beneficiary);
        init();
    }

    private void init() {
        List<BimaRelation> bimaRelationList = getRelationshipList();
        RecyclerView rvBeneficiaryRelations = findViewById(R.id.rvBeneficiaryRelations);
        rvBeneficiaryRelations.setLayoutManager(new GridLayoutManager(this, 3));
        rvBeneficiaryRelations.setNestedScrollingEnabled(false);
        rvBeneficiaryRelations.setAdapter(new BeneficiaryRelationAdapter(bimaRelationList, getSelectedBeneficiaryRelationshipId(), this));
        disableOneTimeAddOnlyAddedRelation(bimaRelationList, getOneTimeAddOnlyAddedRelationsFromIntent());
    }

    private ArrayList<Integer> getOneTimeAddOnlyAddedRelationsFromIntent() {
        ArrayList<Integer> arrayList = getIntent().getIntegerArrayListExtra(BeneficiaryRelationActivityRouter.ONE_TIME_ADD_ONLY_ADDED_RELATION_ARRAY_LIST);
        return arrayList != null ? arrayList : new ArrayList<>();
    }


    private int getSelectedBeneficiaryRelationshipId() {
        return getIntent().getIntExtra(BeneficiaryRelationActivityRouter.SELECTED_BENEFICIARY_RELATIONSHIP_ID, -100);
    }

    @NonNull
    private List<BimaRelation> getRelationshipList() {
        List<BimaRelation> bimaRelationList = new ArrayList<>();
        bimaRelationList.add(new BimaRelation(Constants.BENEFICIARY_FATHER, R.string.father, R.drawable.ic_father));
        bimaRelationList.add(new BimaRelation(Constants.BENEFICIARY_MOTHER, R.string.mother, R.drawable.ic_mother));
        bimaRelationList.add(new BimaRelation(Constants.BENEFICIARY_HUSBAND, R.string.husband, R.drawable.ic_husband));
        bimaRelationList.add(new BimaRelation(Constants.BENEFICIARY_WIFE, R.string.wife, R.drawable.ic_wife));
        bimaRelationList.add(new BimaRelation(Constants.BENEFICIARY_SON, R.string.son, R.drawable.ic_son));
        bimaRelationList.add(new BimaRelation(Constants.BENEFICIARY_DAUGHTER, R.string.daughter, R.drawable.ic_daughter));
        bimaRelationList.add(new BimaRelation(Constants.BENEFICIARY_BROTHER, R.string.brother, R.drawable.ic_brother));
        bimaRelationList.add(new BimaRelation(Constants.BENEFICIARY_SISTER, R.string.sister, R.drawable.ic_sister));
        bimaRelationList.add(new BimaRelation(Constants.BENEFICIARY_OTHER, R.string.other, R.drawable.ic_other));
        return bimaRelationList;
    }

    private void disableOneTimeAddOnlyAddedRelation(@NonNull List<BimaRelation> bimaRelationList,
                                                    @NonNull ArrayList<Integer> oneTimeAddOnlyRelations) {
        for (BimaRelation bimaRelation : bimaRelationList) {
            for (Integer id : oneTimeAddOnlyRelations) {
                if (bimaRelation.getRelationShipId() == id)
                    disableRelation(bimaRelation);
            }
        }
    }

    private void disableRelation(@NonNull BimaRelation bimaRelation) {

        switch (bimaRelation.getRelationShipId()) {

            case Constants.BENEFICIARY_FATHER:
                bimaRelation.setRelationShipIcon(R.drawable.ic_father_disable);
                break;

            case Constants.BENEFICIARY_MOTHER:
                bimaRelation.setRelationShipIcon(R.drawable.ic_mother_disable);
                break;

            case Constants.BENEFICIARY_HUSBAND:
                bimaRelation.setRelationShipIcon(R.drawable.ic_husband_disable);
                break;

            case Constants.BENEFICIARY_WIFE:
                bimaRelation.setRelationShipIcon(R.drawable.ic_wife_disable);
                break;
        }

        bimaRelation.setRelationShipId(-1);
    }

    @Override
    public void onClickBeneficiaryRelation(@NonNull BimaRelation bimaRelation) {
        Intent intent = new Intent();
        intent.putExtra(BeneficiaryRelationActivityRouter.BIMA_RELATION, bimaRelation);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    protected String getScreenName() {
        return "BeneficiaryRelationActivity";
    }
}
