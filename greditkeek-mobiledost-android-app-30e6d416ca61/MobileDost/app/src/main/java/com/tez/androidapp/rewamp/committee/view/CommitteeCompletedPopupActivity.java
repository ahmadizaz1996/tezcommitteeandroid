package com.tez.androidapp.rewamp.committee.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Toast;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.cnic.detection.util.DateUtil;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeResponse;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter;
import com.tez.androidapp.rewamp.committee.router.MyCommitteeActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter.COMMITTEE_DATA;
import static com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter.COMMITTE_CREATEION;
import static com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter.JOIN_COMMITTEE_RESPONSE;

public class CommitteeCompletedPopupActivity extends BaseActivity implements DoubleTapSafeOnClickListener {

    @BindView(R.id.btDone)
    private TezButton btDone;

    @BindView(R.id.tvHeading)
    private TezTextView tvHeading;

    @BindView(R.id.tvdate)
    TezTextView tvdate;

    CommitteePackageModel committeePackage;
    private MyCommitteeResponse myCommitteeResponse;
    private CommitteeCreateResponse committeeCreationResponse;
    private JoinCommitteeResponse joinCommitteeResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.committee_completed_dialog);
        ViewBinder.bind(this);
        initClickListeners();
        fetchExtras();
    }

    @Override
    protected String getScreenName() {
        return CommitteeCompletedPopupActivity.class.getSimpleName();
    }

    private void initClickListeners() {
        btDone.setDoubleTapSafeOnClickListener(this);
    }

    private void fetchExtras() {
        Intent intent = getIntent();
        if (intent != null
                && intent.getExtras() != null
        ) {
            committeePackage = (CommitteePackageModel) intent.getExtras().getSerializable(COMMITTEE_DATA);
            myCommitteeResponse = intent.getExtras().getParcelable(CommitteeSummaryActivityRouter.MY_COMMITTEE_RESPONSE);
            committeeCreationResponse = (CommitteeCreateResponse) intent.getExtras().getSerializable(COMMITTE_CREATEION);
            joinCommitteeResponse = (JoinCommitteeResponse) intent.getExtras().getSerializable(JOIN_COMMITTEE_RESPONSE);

            if (joinCommitteeResponse != null) {
                tvHeading.setVisibility(View.GONE);
                tvdate.setText(joinCommitteeResponse.getStartDate().split("T")[0]);
            } else {
                tvHeading.setText(getString(R.string.congratulations_non_exclamation_mark) + " " + committeeCreationResponse.getData().getName());
                tvdate.setText(committeeCreationResponse.getData().getStartDate().split("T")[0]);
            }

        }
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {
            case R.id.btDone:
//                Toast.makeText(this, "Will be implemented", Toast.LENGTH_SHORT).show();
                CommitteeActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
                MyCommitteeActivityRouter.createInstance().setDependenciesAndRoute(this);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        CommitteeActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
    }
}