package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeFilterListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeGroupChatListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeGroupChatSendMessageListener;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeFilterActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeGroupChatActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.request.CommitteeSendMessageRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeSendMessageResponse;
import com.tez.androidapp.rewamp.committee.response.GetGroupChatMessagesResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeGroupChatActivityInteractor implements ICommitteeGroupChatInteractor {


    private final ICommitteeGroupChatActivityInteractorOutput iCommitteeGroupChatActivityInteractorOutput;

    public CommitteeGroupChatActivityInteractor(ICommitteeGroupChatActivityInteractorOutput iCommitteeGroupChatActivityInteractorOutput) {
        this.iCommitteeGroupChatActivityInteractorOutput = iCommitteeGroupChatActivityInteractorOutput;
    }

    @Override
    public void getMessages(String committeeId) {
        CommitteeAuthCloudDataStore.getInstance().getMessages(committeeId, new CommitteeGroupChatListener() {
            @Override
            public void onGetMessageSuccess(GetGroupChatMessagesResponse getGroupChatMessagesResponse) {
                iCommitteeGroupChatActivityInteractorOutput.onGetMessageSuccess(getGroupChatMessagesResponse);
            }

            @Override
            public void onGetMessageFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getMessages(committeeId);
                else
                    iCommitteeGroupChatActivityInteractorOutput.onGetMessageFailure(errorCode, message);
            }
        });
    }

    @Override
    public void sendMessage(CommitteeSendMessageRequest committeeSendMessageRequest) {
        CommitteeAuthCloudDataStore.getInstance().sendMessage(committeeSendMessageRequest, new CommitteeGroupChatSendMessageListener() {
            @Override
            public void onSendSuccess(CommitteeSendMessageResponse committeeSendMessageResponse) {
                iCommitteeGroupChatActivityInteractorOutput.onSendSuccess(committeeSendMessageResponse);
            }

            @Override
            public void onSendFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    sendMessage(committeeSendMessageRequest);
                else
                    iCommitteeGroupChatActivityInteractorOutput.onSendFailure(errorCode, message);
            }
        });
    }
}
