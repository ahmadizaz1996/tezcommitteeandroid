package com.tez.androidapp.rewamp.general.transactions;

import android.view.View;

import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.transactions.models.network.Transaction;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.viewbinder.library.core.BindView;

class BimaTransactionViewHolder extends BaseTransactionViewHolder {

    @BindView(R.id.ivImage)
    private TezImageView ivImage;

    @BindView(R.id.tvTitle)
    private TezTextView tvTitle;

    @BindView(R.id.tvAmountPaid)
    private TezTextView tvAmountPaid;

    @BindView(R.id.tvPolicyNumber)
    private TezTextView tvPolicyNumber;

    @BindView(R.id.tvWallet)
    private TezTextView tvWallet;

    @BindView(R.id.tvWalletNumber)
    private TezTextView tvWalletNumber;

    BimaTransactionViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBind(Transaction item, @Nullable MyTransactionsAdapter.MyTransactionListener listener) {
        super.onBind(item, listener);
        tvTitle.setText(item.getTitle());
        tvAmountPaid.setText(itemView.getContext().getString(R.string.rs_value_string, item.getStringAmount()));
        tvPolicyNumber.setText(item.getPolicyNumber());
        tvWallet.setText(item.getWalletName());
        tvWalletNumber.setText(item.getWalletNumber());
        ivImage.setImageResource(Utility.getInsuranceProductIcon(item.getProductId()));
    }
}
