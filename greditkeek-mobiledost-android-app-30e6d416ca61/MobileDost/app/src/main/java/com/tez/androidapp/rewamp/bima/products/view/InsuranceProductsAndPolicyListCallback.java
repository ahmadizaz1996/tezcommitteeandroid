package com.tez.androidapp.rewamp.bima.products.view;

public interface InsuranceProductsAndPolicyListCallback {

    void showProductsFragment();

    void setIllustrationVisibility(int visibility);
}
