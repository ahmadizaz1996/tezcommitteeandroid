package com.tez.androidapp.commons.location.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.models.app.City;
import com.tez.androidapp.commons.location.callbacks.GetCitiesCallback;
import com.tez.androidapp.commons.location.models.network.dto.response.GetCitiesResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created  on 2/9/2017.
 */

public class GetCitiesRH extends BaseRH<GetCitiesResponse> {

    private GetCitiesCallback getCitiesCallback;

    public GetCitiesRH(BaseCloudDataStore baseCloudDataStore, @Nullable GetCitiesCallback getCitiesCallback) {
        super(baseCloudDataStore);
        this.getCitiesCallback = getCitiesCallback;
    }

    @Override
    protected void onSuccess(Result<GetCitiesResponse> value) {
        if (getCitiesCallback != null) getCitiesCallback.onGetCitiesSuccess(transformCities(value.response().body()
                .getCities()));
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (getCitiesCallback != null) getCitiesCallback.onGetCitiesError(errorCode,message);
    }

    private List<City> transformCities(List<com.tez.androidapp.commons.location.models.network.City> cities) {
        List<City> citiesOutput = new ArrayList<>();
        for (com.tez.androidapp.commons.location.models.network.City city : cities) {
            citiesOutput.add(new City(city));
        }
        return citiesOutput;
    }
}
