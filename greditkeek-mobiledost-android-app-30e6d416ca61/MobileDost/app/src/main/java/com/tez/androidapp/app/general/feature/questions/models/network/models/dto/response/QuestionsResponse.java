package com.tez.androidapp.app.general.feature.questions.models.network.models.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.questions.models.network.models.BimaQuestion;

import java.util.List;

/**
 * Created  on 2/21/2017.
 */

public class QuestionsResponse extends BaseResponse {

    private List<BimaQuestion> questions;

    public List<BimaQuestion> getLoanQuestionDtos() {
        return questions;
    }

    @Override
    public String toString() {
        return "QuestionsResponse{" +
                "questions=" + questions +
                "} " + super.toString();
    }
}
