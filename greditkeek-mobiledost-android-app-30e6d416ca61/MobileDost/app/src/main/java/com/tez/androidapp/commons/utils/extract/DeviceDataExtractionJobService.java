package com.tez.androidapp.commons.utils.extract;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.text.TextUtils;

import androidx.core.app.ActivityCompat;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.firebase.jobdispatcher.SimpleJobService;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.utils.file.util.FileUtil;
import com.tez.androidapp.repository.network.store.DataLiftCloudDataStore;

import java.io.File;
import java.io.IOException;

/**
 * Created  on 4/12/2017.
 */

public class DeviceDataExtractionJobService extends SimpleJobService {

    @SuppressLint("MissingPermission")
    @Override
    public int onRunJob(JobParameters job) {
        if (isTimeToRunJob() && isPhonePermissionAllowed()) {
            if (TextUtils.isEmpty(Utility.getImei(getApplicationContext())) || MDPreferenceManager.getPrincipalName() == null || !Utility.doesUserExist())
                return JobService.RESULT_SUCCESS;

            try {
                ExtractionUtil extractionUtil = new ExtractionUtil(this);
                if (Constants.IS_EXTERNAL_BUILD) {
                    FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_CALL_LOG, extractionUtil.extractCallsObject());
                    FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_CALENDAR, extractionUtil.extractEventsObject());
                    FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_FILE_DIRECTORY, extractionUtil.extractFilesObject());
                    FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_MSG_LOG, extractionUtil.extractSmsObject());
                    FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_ACCOUNTS, extractionUtil.extractAccountsObject());
                    FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_APP_LOGS, extractionUtil.extractApplicationsObject());
                    FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_CONTACTS, extractionUtil.extractContactsObject());
                    FileUtil.createFileWithData(this, ExtractionUtil.TYPE_BLUETOOTH, extractionUtil.extractBluetoothFromDevice());
                    FileUtil.createFileWithData(this, ExtractionUtil.TYPE_WIFI, extractionUtil.extractWifiFromDevice());
                    FileUtil.createFileWithObjectData(this, ExtractionUtil.TYPE_DEVICE_INFO, extractionUtil.extractDeviceInfoObject());
                } else {
                    FileUtil.createFileWithData(this, ExtractionUtil.TYPE_CALL_LOG, "{}");
                    FileUtil.createFileWithData(this, ExtractionUtil.TYPE_CALENDAR, "{}");
                    FileUtil.createFileWithData(this, ExtractionUtil.TYPE_FILE_DIRECTORY, "{}");
                    FileUtil.createFileWithData(this, ExtractionUtil.TYPE_MSG_LOG, "{}");
                    FileUtil.createFileWithData(this, ExtractionUtil.TYPE_ACCOUNTS, "{}");
                    FileUtil.createFileWithData(this, ExtractionUtil.TYPE_APP_LOGS, "{}");
                    FileUtil.createFileWithData(this, ExtractionUtil.TYPE_CONTACTS, "{}");
                    FileUtil.createFileWithData(this, ExtractionUtil.TYPE_BLUETOOTH, "{}");
                    FileUtil.createFileWithData(this, ExtractionUtil.TYPE_WIFI, "{}");
                    FileUtil.createFileWithData(this, ExtractionUtil.TYPE_DEVICE_INFO, "{}");
                }

                File zipFile = FileUtil.createZipFile(this);
                DataLiftCloudDataStore dataLiftCloudDataStore = DataLiftCloudDataStore.getInstance();
                String response = dataLiftCloudDataStore.uploadDeviceDataWithoutLoan(zipFile);

                if ((response.equalsIgnoreCase("SUCCESS")))
                    MDPreferenceManager.setLastUploadElapsedTime(System.currentTimeMillis());
                else
                    throw new IOException(response);

                return JobService.RESULT_SUCCESS;

            } catch (Exception e) {
                return JobService.RESULT_FAIL_RETRY;
            }
        } else
            return JobService.RESULT_SUCCESS;
    }

    public boolean isTimeToRunJob() {
        return (Math.abs(SystemClock.elapsedRealtime() - MDPreferenceManager.getLastUploadElapsedTime()) >= (Constants.TIME_TO_LIFT));
    }

    private boolean isPhonePermissionAllowed() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }
}
