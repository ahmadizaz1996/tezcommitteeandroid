package com.tez.androidapp.rewamp.general.beneficiary.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

public interface IPolicyDetailActivityInteractor extends IBaseInteractor {

    void getPolicyDetails(int policyId);
}
