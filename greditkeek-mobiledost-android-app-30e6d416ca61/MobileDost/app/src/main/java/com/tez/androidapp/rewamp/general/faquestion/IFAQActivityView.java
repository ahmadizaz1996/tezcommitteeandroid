package com.tez.androidapp.rewamp.general.faquestion;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.general.feature.faq.models.network.FAQ;

import java.util.List;

public interface IFAQActivityView extends IBaseView {
    void renderRecyclerView(List<FAQ> faqs);
}
