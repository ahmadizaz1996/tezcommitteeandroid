package com.tez.androidapp.rewamp.bima.products.policy.RH;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InitiatePurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.response.InitiatePurchasePolicyResponse;

public class InitiatePurchasePolicyRH extends BaseRH<InitiatePurchasePolicyResponse> {

    private InitiatePurchasePolicyCallback initiatePurchasePolicyCallback;

    public InitiatePurchasePolicyRH(BaseCloudDataStore baseCloudDataStore, InitiatePurchasePolicyCallback initiatePurchasePolicyCallback) {
        super(baseCloudDataStore);
        this.initiatePurchasePolicyCallback = initiatePurchasePolicyCallback;
    }

    @Override
    protected void onSuccess(Result<InitiatePurchasePolicyResponse> value) {
        InitiatePurchasePolicyResponse response = value.response().body();

        if (response != null) {
            if (isErrorFree(response))
                this.initiatePurchasePolicyCallback.initiatePurchasePolicySuccess(response.getPolicyDetails());
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());
        }
        else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.initiatePurchasePolicyCallback.initiatePurchasePolicyFailure(errorCode, message);
    }
}
