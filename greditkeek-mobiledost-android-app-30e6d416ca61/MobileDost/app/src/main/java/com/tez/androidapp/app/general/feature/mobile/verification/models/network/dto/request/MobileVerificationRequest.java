package com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created by FARHAN DHANANI on 2/25/2019.
 */
public class MobileVerificationRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/user/status/mobileNumberVerification";

    private String principalName;
    private String mobileNumber;
    private String verificationMethod;
    private int verificationTry;
    private String status;

    public MobileVerificationRequest(final String principalName, final String mobileNumber) {
        this.principalName = principalName;
        this.mobileNumber = mobileNumber;
        this.verificationMethod = Constants.MobileNumberVerificationMethodSinch;
        this.verificationTry = (MDPreferenceManager.getFlashCallVerificationFailureCount() + MDPreferenceManager.getOtpVerificationFailureCount());
        this.status = Constants.MobileNumberVerificationStatusSuccess;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setVerificationMethod(String verificationMethod) {
        this.verificationMethod = verificationMethod;
    }

    public void setVerificationTry(int verificationTry) {
        this.verificationTry = verificationTry;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
