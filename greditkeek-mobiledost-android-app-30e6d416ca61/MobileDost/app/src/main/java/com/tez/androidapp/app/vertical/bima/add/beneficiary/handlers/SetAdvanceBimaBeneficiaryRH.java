package com.tez.androidapp.app.vertical.bima.add.beneficiary.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetAdvanceBimaBeneficiaryCallback;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.dto.response.SetAdvanceBimaBeneficiaryResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 8/25/2017.
 */

public class SetAdvanceBimaBeneficiaryRH extends BaseRH<SetAdvanceBimaBeneficiaryResponse> {

    SetAdvanceBimaBeneficiaryCallback setAdvanceBimaBeneficiaryCallback;

    public SetAdvanceBimaBeneficiaryRH(BaseCloudDataStore baseCloudDataStore, SetAdvanceBimaBeneficiaryCallback setAdvanceBimaBeneficiaryCallback) {
        super(baseCloudDataStore);
        this.setAdvanceBimaBeneficiaryCallback = setAdvanceBimaBeneficiaryCallback;
    }

    @Override
    protected void onSuccess(Result<SetAdvanceBimaBeneficiaryResponse> value) {
        SetAdvanceBimaBeneficiaryResponse setAdvanceBimaBeneficiaryResponse = value.response().body();
        if (setAdvanceBimaBeneficiaryResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) setAdvanceBimaBeneficiaryCallback.onSetAdvanceBimaBeneficiarySuccess();
        else onFailure(setAdvanceBimaBeneficiaryResponse.getStatusCode(), setAdvanceBimaBeneficiaryResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        setAdvanceBimaBeneficiaryCallback.onSetAdvanceBimaBeneficiaryFailure(errorCode, message);
    }
}
