package com.tez.androidapp.rewamp.profile.complete.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Answer;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.UserProfile;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateProfileWithCnicRequest;
import com.tez.androidapp.commons.models.app.City;
import com.tez.androidapp.commons.models.network.User;

import java.util.List;

public interface ICnicInformationFragmentView extends ICompleteProfileCnicUploadActivityView {
    void callForCnicImageInCvFrontPart();

    void setCvFrontPartRejected(boolean isRejected);

    void setCvBackPartRejected(boolean isRejected);

    void setCvSelfiePartRejected(boolean isRejected);

    void populateDropDownForPlaceOfBirth(@NonNull List<City> cities);

    void populateDropDownForCurrentCity(@NonNull List<City> cities);

    @Nullable
    String getSelectedLanguage();

    void routeToPersonalInformationScreen();

    AnswerSelected getSelectedMaritalStatus();

    void setDoubleTapOnClickListenerToBtContinue(UserProfile userProfile, User user);

    void makeCnicImageInCvFrontPartEditable(boolean editable);

    void makeCnicImageInCvBackPartEditable(boolean editable);

    void makeCnicImageInCvSelfiePartEditable(boolean editable);

    void callForCnicImageInCvBackPart();

    void callForCnicImageInCvSelfiePart();

    void setTextToSGender(int position, Option option);

    void setTextToSMaritalStatus(AnswerSelected answerSelected);

    void setEditableSGender(boolean editable);

    void setsGenderRejected(boolean rejected);

    void setEditableSMaritalStatus(boolean editable);

    void setsMaritalStatusRejected(boolean rejected);

    void setTextToAutoCompleteTextViewPlaceOfBirth(String text, Integer id);

    void setEditableAutoCompleteTextViewPlaceOfBirth(boolean editable);

    void setAutoCompleteTextViewPlaceOfBirthRejected(boolean rejected);

    void setTextToAutoCompleteTextViewCurrentCity(City city);

    void setEditableAutoCompleteTextViewCurrentCity(boolean editable);

    void setTextToEtCurrentAddress(String text);

    void setEditableEtCurrentAddress(boolean editable);

    void setTextToEtMothersMaiden(String text);

    void setEditableEtMothersMaiden(boolean editable);

    void setEtMothersMaidenRejected(boolean rejected);

    void setTextToEtDOB(String text);

    void setTextToEtDobFromOcr(String text);

    void setEditableEtDob(boolean editable);

    void setEtDobRejected(boolean rejected);

    void setTextToEtDateOfIssue(String text);

    void setTextToEtDateOfIssueFromOcr(String text);

    void setEditableToEtDateOfIssue(boolean editable);

    void setEtDateOfIssueRejected(boolean rejected);

    void setTextToEtDateOfExpiry(String text);

    void setLifeTimeToEtDateOfExpiry();

    void setTextToEtDateOfExpiryFromOcr(String text);

    void setEditableEtDateOfExpiry(boolean editable);

    void setEtDateOfExpiryRejeced(boolean rejeced);

    void setTextToEtName(String text);

    void setEditableEtName(boolean editable);

    void setEtNameRejected(boolean rejected);

    void setTextToEtFatherName(String text);

    void setEditableEtFatherName(boolean editable);

    void setEtFatherNameRejected(boolean rejected);

    void setTextToEtCnicNumber(String text);

    void setEditableCnicNumber(boolean editable);

    void setEtCnicNumberRejected(boolean rejected);

    void setVisibillityToShimmer(int visibillity);

    void setVisibillityToDetailCardView(int visibillity);

    void onClickListenerForSGender(List<Option> maritalStatus);

    void updateHintToTilFatherName(int position);

    void setHintToTilFatherName(String hint);
}
