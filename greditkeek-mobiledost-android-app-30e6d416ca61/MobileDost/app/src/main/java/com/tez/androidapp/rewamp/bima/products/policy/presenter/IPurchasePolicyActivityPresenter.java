package com.tez.androidapp.rewamp.bima.products.policy.presenter;

import com.tez.androidapp.rewamp.bima.products.policy.adapters.InsuranceDuration;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;

import java.util.List;

public interface IPurchasePolicyActivityPresenter {

    void getInsuranceCoverage(int productId);

    void setValue(InsuranceDuration tenure);

    List<InsuranceDuration> getInsuranceTenures(double coverage);

    void completeInitiatePurchasePolicyRequest(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest,
                                               int productId,
                                               int walletServiceProviderId);
}
