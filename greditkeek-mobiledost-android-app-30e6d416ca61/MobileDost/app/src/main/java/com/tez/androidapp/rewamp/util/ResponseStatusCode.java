package com.tez.androidapp.rewamp.util;

import android.util.SparseArray;

import androidx.annotation.StringRes;

import com.tez.androidapp.R;

/**
 * Created by VINOD KUMAR on 8/7/2019.
 */
@SuppressWarnings("unused")
public enum ResponseStatusCode {

    DEFAULT(-2, R.string.default_response),
    CHANGE_PIN_VERIFICATION_ERROR(1001, R.string.code_1001),
    OTP_VERIFICATION_ERROR(1002, R.string.code_1002),
    USER_ALREADY_LOGGED_IN(1003, R.string.code_1003),
    USER_ALREADY_EXIST_AGAINST_CNIC(1004, R.string.code_1004),
    USER_ALREADY_EXIST_AGAINST_MOBILE_NUMBER(1005, R.string.code_1005),
    USER_ALREADY_EXIST_AGAINST_CNIC_AND_MOBILE_NUMBER(1006, R.string.code_1006),
    OTP_EXPIRED(1007, R.string.code_1007),
    USER_SUSPENDED(1008, R.string.code_1008),
    USER_DECEASED(1009, R.string.code_1009),
    USER_BLACKLISTED(1010, R.string.code_1010),
    APP_NOT_AUTHORIZED(1011, R.string.code_1011),
    API_KEY_MISSING(1012, R.string.code_1012),
    AUTHORIZATION_FAILURE(1013, R.string.code_1013),
    USER_LOCKED(1014, R.string.code_1014),
    WALLET_CUSTOMER_DOES_NOT_EXIST(1015, R.string.code_1015),
    INVALID_WALLET_SERVICE_PROVIDER_ID(1016, R.string.code_1016),
    PROVIDER_CONNECTION_FAILED(1017, R.string.code_1017),
    WALLET_ALREADY_EXIST(1018, R.string.code_1018),
    WALLET_ALREADY_VERIFIED(1019, R.string.code_1019),
    VALIDATION_ERRORS(1020, R.string.code_1020),
    NO_VALID_FILE(1021, R.string.code_1021),
    INVALID_FILE_FORMAT(1022, R.string.code_1022),
    OLDPIN_SAME_AS_NEWPIN_ERROR(1023, R.string.code_1023),
    OLDPIN_NEWPIN_NOT_PROVIDED(1024, R.string.code_1024),
    DEVICE_NOT_REGISTERED(1025, R.string.code_1025),
    MOBILE_ACCOUNT_NOT_FOUND_AGAINST_USER(1026, R.string.code_1026),
    INVALID_REFERRAL_CODE(1027, R.string.code_1027),
    DUPLICATE_REFERRAL_CODE(1028, R.string.code_1028),
    USER_CANNOT_REFER_HIMSELF(1029, R.string.code_1029),
    LOAN_REPAYMENT_PENDING(1030, R.string.code_1030),
    QUOTA_EXCEEDED_ERROR(1031, R.string.code_1031),
    UPLOAD_CNIC_FIRST(1032, R.string.code_1032),
    AMOUNT_EXCEED_LOAN_LIMIT(1033, R.string.code_1033),
    LIMIT_EXPIRED(1034, R.string.code_1034),
    MULTIPLE_LOAN_ERROR(1035, R.string.code_1035),
    USER_DOES_NOT_EXIST(1036, R.string.code_1036),
    DETAILS_NOT_CORRECT(1037, R.string.code_1037),
    MOBILE_NUMBER_NOT_VERIFIED(1038, R.string.code_1038),
    MOBILE_NUMBER_ALREADY_VERIFIED(1039, R.string.code_1039),
    IMEI_LOCKED_ERROR(1040, R.string.code_1040),
    DEVICE_CHANGE_LOCK_ERROR(1041, R.string.code_1041),
    SMS_SERVICE_UNAVAILABLE(1042, R.string.code_1042),
    INVALID_RECEIVER(1043, R.string.code_1043),
    LIMIT_PENDING(1044, R.string.code_1044),
    INVALID_REFERRAL_CODE_AND_DUPLICATE_CNIC(1045, R.string.code_1045),
    SESSION_EXPIRED(1046, R.string.code_1046),
    LOAN_NOT_APPLIED_OR_REPAYED(1047, R.string.code_1047),
    REPAYMENT_GREATER_THAN_APPLICABLE_AMOUNT(1048, R.string.code_1048),
    INVALID_TOKEN(1049, R.string.code_1049),
    TOKEN_EXPIRED(1050, R.string.code_1050),
    JSON_HANDLING_EXCEPTION(1051, R.string.code_1051),
    MESSAGING_ERROR(1052, R.string.code_1052),
    TEMPLATE_DOES_NOT_EXIST(1053, R.string.code_1053),
    LIMIT_ASSIGNMENT_ERROR(1054, R.string.code_1054),
    LIMIT_COOL_DOWN_PERIOD_ERROR(1055, R.string.code_1055),
    LOW_CREDIT_SCORE(1056, R.string.code_1056),
    INVALID_LOAN_ID(1057, R.string.code_1057),
    INVALID_REPAYMENT_ID(1058, R.string.code_1058),
    CANCEL_LIMIT_NOT_ALLOWED(1059, R.string.code_1059),
    INVALID_PIN(1060, R.string.code_1060),
    APP_VERSION_INCOMPATIBLE(1062, R.string.code_1062),
    LOAN_NOT_DISBURSED(1063, R.string.code_1063),
    TEMPORARY_PIN_EXPIRED(1064, R.string.code_1064),
    WALLET_INACTIVE(1065, R.string.code_1065),
    INSUFFICIENT_BALANCE(1066, R.string.code_1066),
    CNIC_DOES_NOT_MATCH_WALLET(1067, R.string.code_1067),
    DEFAULT_WALLET_NOT_FOUND(1068, R.string.code_1068),
    CANNOT_DELETE_DEFAULT_WALLET(1069, R.string.code_1069),
    INVALID_WALLET_PIN_LINKING(1070, R.string.code_1070),
    WALLET_ACCOUNT_BLOCKED(1071, R.string.code_1071),
    INCORRECT_WALLET_DETAILS(1072, R.string.code_1072),
    WALLET_ALREADY_ADDED(1073, R.string.code_1073),
    RULE_MISMATCH_ERROR(1074, R.string.code_1074),
    SIGNUP_CACHE_TIMEOUT(1075, R.string.code_1075),
    INFO_MISMATCH(1076, R.string.code_1076),
    PIN_LENGTH_ERROR(1077, R.string.code_1077),
    WALLET_LIMIT_EXCEEDED(1078, R.string.code_1078),
    LOAN_APPLICATION_ERROR(1079, R.string.code_1079),
    LIMIT_DENIED(1080, R.string.code_1080),
    INSUFFICIENT_DISBURSEMENT_BALANCE(1081, R.string.code_1081),
    CNIC_VERIFICATION_PENDING(1082, R.string.code_1082),
    INVALID_DEVICE(1083, R.string.code_1083),
    LOAN_ALREADY_DISBURSED(1084, R.string.code_1084),
    INSUFFICIENT_FUNDS_IN_TEZ_WALLET(1085, R.string.code_1085),
    REPAYMENT_AMOUNT_ERROR(1086, R.string.code_1086),
    PROCESSING_EXCEPTION(1087, R.string.code_1087),
    INVALID_SESSION(1088, R.string.code_1088),
    WALLET_NOT_FOUND(1089, R.string.code_1089),
    TRANSACTION_NOT_ALLOWED(1090, R.string.code_1090),
    POLICY_NOT_APPLICABLE(1091, R.string.code_1091),
    ACCOUNT_NOT_ACTIVE(1092, R.string.code_1092),
    ILLEGAL_STATE(1093, R.string.code_1093),
    CNIC_CANNOT_BE_UPDATED(1094, R.string.code_1094),
    DATE_OF_BIRTH_CANNOT_BE_UPDATED(1095, R.string.code_1095),
    CNIC_EXPIRY_DATE_CANNOT_BE_UPDATED(1096, R.string.code_1096),
    MOTHER_NAME_CANNOT_BE_UPDATED(1097, R.string.code_1097),
    PLACE_OF_BIRTH_CANNOT_BE_UPDATED(1098, R.string.code_1098),
    FULL_NAME_CANNOT_BE_UPDATED(1099, R.string.code_1099),
    CNIC_ISSUE_DATE_CANNOT_BE_UPDATED(1100, R.string.code_1100),
    BAD_REQUEST(400, R.string.code_400),
    FORBIDDEN_REQUEST(403, R.string.code_403),
    INTERNAL_SERVER_ERROR(500, R.string.code_500),
    POLICY_INACTIVE(3001, R.string.code_3001),
    INSURANCE_DOCUMENT_NOT_FOUND(3002, R.string.code_3002),
    POLICY_CANNOT_CLAIM(3003, R.string.code_3003),
    POLICY_NOT_FOUND(3004, R.string.code_3004),
    CLAIM_AMOUNT_GREATER_THAN_REMAINING_AMOUNT(3005, R.string.code_3005),
    CLAIM_CANNOT_APPROVED(3006, R.string.code_3006),
    CNIC_EXPIRED(1101, R.string.code_1101),
    CNIC_EXPIRED_PORTAL_ALERT(1102, R.string.code_1102),
    CNIC_ABOUT_TO_EXPIRE(1103, R.string.code_1103),
    CNIC_ABOUT_TO_EXPIRE_PORTAL_ALERT(1104, R.string.code_1104),
    UPDATE_CNIC_NOT_EXPIRED(1105, R.string.code_1105),
    UPDATE_MOBILE_NUMBER_FAILED(1106, R.string.code_1106),
    CANCEL_LIMIT_LOCK_ERROR(1107, R.string.code_1107),
    BENEFICIARY_MOBILE_NUMBER_SAME_AS_USER(1108, R.string.code_1108),
    USER_MOBILE_NUMBER_SAME_AS_BENEFICIARY(1109, R.string.code_1109),
    MOBILE_NUMBER_VERIFICATION_PENDING(1200, R.string.code_1200),
    MOBILE_NUMBER_SINCH_VERIFICATION_PENDING(1201, R.string.code_1201),
    DISBURSEMENT_VALIDATION_ERROR(1202, R.string.code_1202),
    SOCIAL_ACCOUNT_ALREADY_LINKED(1203, R.string.code_1203),
    BENEFICIARY_ALREADY_EXIST_ON_MOBILE_NUMBER(1204, R.string.code_1204),
    BENEFICIARY_DETAILS_MISSING(1205, R.string.code_1205),
    CNIC_NOT_ISSUE(1026, R.string.code_1026),
    USER_NOT_ELEGIBLE(1207, R.string.code_1207),
    FATHER_NAME_CANNOT_BE_UPDATED(1208, R.string.code_1208),
    HUSBAND_NAME_CANNOT_BE_UPDATED(1209, R.string.code_1209),
    GENDER_CANNOT_BE_UPDATED(1210, R.string.code_1210),
    BENEFICIARY_TYPE_ALREADY_EXISTS(1211, R.string.code_1211),
    PROFILE_COMPLETION_REQUIRED(1212, R.string.code_1212),
    MAX_CNIC_UPLOAD_RETIRES_REACHED(1213, R.string.code_1213),
    USER_NOT_CONNECTED_TO_INTERNET(851, R.string.code_851),
    CONNECTION_TIMEOUT(850, R.string.code_850),
    MAINTAINANCE_IN_PROGRESS(503, R.string.code_503),
    GATEWAY_TIMEOUT(504, R.string.code_504),
    BACKEND_CRASHED(505, R.string.code_505),
    USER_ACTIVATION_PENDING(1216, R.string.code_1216),
    WALLET_CANNOT_BE_ADDED(1217, R.string.code_1217),
    DISBURSEMENT_DENIED(1218, R.string.code_1218),
    ASYNC_RESPONSE_TIMEOUT(1219, R.string.code_1219),
    LOAN_ALREADY_PAID(2020, R.string.code_2020),
    INVALID_PRODUCT_ID(1234, R.string.code_1234),
    INVALID_PRODUCT_PLAN_ID(1235, R.string.code_1235),
    PRODUCT_AND_PLAN_MISMATCH(1236, R.string.code_1236),
    INVALID_PRODUCT_IDINVALID_COVERAGE_AMOUNT(1237, R.string.code_1237),
    INCORRECT_FINAL_AMOUNT(1238, R.string.code_1238),
    INVALID_WALLET_ID(1239, R.string.code_1239),
    INVALID_PRODUCT_ACTIVATION_DATE(1240, R.string.code_1240),
    INVALID_PRODUCT_EXPIRATION_DATE(1241, R.string.code_1241),
    USER_ALREADY_HAS_POLICY(1242, R.string.code_1242),
    PROFILE_INCOMPLETE(1243, R.string.code_1243),
    USER_AGE_ABOVE_LIMIT(1244, R.string.code_1244),
    INCOMPLETE_STEPS_INSURANCE(1245, R.string.code_1245),
    PROFILE_VERIFICATION_REQUIRED(1246, R.string.code_1246);
    //----------------------------------------------------------
    private int code;
    @StringRes
    private int errorDescription;

    ResponseStatusCode(int code, @StringRes int errorDescription) {
        Holder.MAP.put(code, this);
        this.code = code;
        this.errorDescription = errorDescription;
    }

    @StringRes
    public static int getDescriptionFromErrorCode(int errorCode) {
        ResponseStatusCode responseStatusCode = Holder.MAP.get(errorCode);
        return responseStatusCode != null ? responseStatusCode.getErrorDescription() : DEFAULT.getErrorDescription();
    }

    public int getCode() {
        return this.code;
    }

    @StringRes
    public int getErrorDescription() {
        return this.errorDescription;
    }

    public enum PlaceHolders {

        WALLET_NAME("@wallet_name"),
        BANK_NAME("@bank_name"),
        WALLET_SUPPORT_NUMBER("@wallet_support_number");

        private String value;

        PlaceHolders(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    private static class Holder {
        static final SparseArray<ResponseStatusCode> MAP = new SparseArray<>();
    }
}
