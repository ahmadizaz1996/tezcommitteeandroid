package com.tez.androidapp.rewamp.dashboard.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.dashboard.entity.Advance;

public class DashboardAdvanceCardResponse extends BaseResponse {

    private Advance advance;

    public Advance getAdvance() {
        return advance;
    }
}
