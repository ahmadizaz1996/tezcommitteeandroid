package com.tez.androidapp.rewamp.committee.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class CommitteeFilterRequest extends BaseRequest {

    public static final String METHOD_NAME = "http://3.21.127.35/api/v1/committee/updates/{" + Params.COMMITTEE_ID + "}";

    public static final class Params {
        public static final String COMMITTEE_ID = "committee_id";

        private Params() {

        }
    }

}
