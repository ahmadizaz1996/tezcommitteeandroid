package com.tez.androidapp.rewamp.advance.limit.interactor;

import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.request.LoanLimitRequest;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.response.LoanLimitResponse;
import com.tez.androidapp.app.vertical.advance.loan.questions.callbacks.UploadDataStatusCallback;
import com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.response.UploadDataStatusResponse;
import com.tez.androidapp.app.vertical.advance.loan.shared.limit.callbacks.LoanLimitCallback;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.advance.limit.presenter.ICheckYourLimitActivityInteractorOutput;

public class CheckYourLimitActivityInteractor implements ICheckYourLimitActivityInteractor {

    private final ICheckYourLimitActivityInteractorOutput iCheckYourLimitActivityInteractorOutput;

    public CheckYourLimitActivityInteractor(ICheckYourLimitActivityInteractorOutput iCheckYourLimitActivityInteractorOutput) {
        this.iCheckYourLimitActivityInteractorOutput = iCheckYourLimitActivityInteractorOutput;
    }

    @Override
    public void checkUploadDataStatus() {
        LoanCloudDataStore.getInstance().uploadDataStatus(new UploadDataStatusCallback() {
            @Override
            public void onUploadDataStatusSuccess(UploadDataStatusResponse uploadDataStatusResponse) {
                iCheckYourLimitActivityInteractorOutput.onUploadDataStatusSuccess(uploadDataStatusResponse);
            }

            @Override
            public void onUploadDataStatusFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    checkUploadDataStatus();
                else
                    iCheckYourLimitActivityInteractorOutput.onUploadDataStatusFailure(errorCode, message);
            }
        });
    }

    @Override
    public void postLoanLimit() {
        LoanLimitRequest loanLimitRequest = new LoanLimitRequest();
        loanLimitRequest.setDeviceInfo(Utility.getUserDeviceInfo());

        LoanCloudDataStore.getInstance().postLoanLimit(loanLimitRequest, new LoanLimitCallback() {
            @Override
            public void onLoanLimitSuccess(LoanLimitResponse loanLimitResponse) {
                iCheckYourLimitActivityInteractorOutput.onLoanLimitSuccess(loanLimitResponse);
            }

            @Override
            public void onLoanLimitFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    postLoanLimit();
                else
                    iCheckYourLimitActivityInteractorOutput.onLoanLimitFailure(errorCode, message);
            }
        });
    }
}
