package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request;

/**
 * Created by FARHAN DHANANI on 6/21/2018.
 */
public class UploadClaimDocumentRequest {

    public static final String METHOD_NAME = "v1/insurance/claim/document/upload";

    public abstract static class Params {
        public static final String DOCUMENT_ID = "documentId";
        public static final String ATTACHMENT = "attachment";
    }
}
