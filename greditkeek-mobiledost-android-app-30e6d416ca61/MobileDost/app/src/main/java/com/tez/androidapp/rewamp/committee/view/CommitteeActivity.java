package com.tez.androidapp.rewamp.committee.view;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.presenter.CommitteeActivityPresenter;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeActivityPresenter;
import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeMetaDataResponse;
import com.tez.androidapp.rewamp.committee.response.CommittteeLoginResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeCreationActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeGroupChatActivityRouter;
import com.tez.androidapp.rewamp.committee.router.JoinCommitteeVerificationActivityRouter;
import com.tez.androidapp.rewamp.committee.router.MyCommitteeActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.List;

public class CommitteeActivity extends BaseActivity implements DoubleTapSafeOnClickListener, ICommitteeActivityView {

    private static final String TAG = "CommitteeActivity";

    @BindView(R.id.contCreateCommit)
    private TezCardView contCreateCommit;

    @BindView(R.id.contCommitMarketPlace)
    private TezCardView contCommitMarketPlace;

    @BindView(R.id.contJoinCommit)
    private TezCardView contJoinCommit;

    @BindView(R.id.contMyCommittee)
    private TezCardView contMyCommittee;

    @BindView(R.id.shimmer)
    private TezLinearLayout shimmer;

    @BindView(R.id.mainLayout)
    private LinearLayout mainLayout;

    @BindView(R.id.ivInviteeCount)
    private TezTextView inviteeCount;


    private CommitteeMetaDataResponse committeeDataResponse;
    private final ICommitteeActivityPresenter iCommitteeActivityPresenter;
    private List<CommitteeCheckInvitesResponse> committeeInvitesList = new ArrayList<>();


    public CommitteeActivity() {
        this.iCommitteeActivityPresenter = new CommitteeActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee);
        ViewBinder.bind(this);
        init();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isEnableButtonOnStart())
            this.setEnableButtons(true);
    }

    protected final void setEnableButtons(boolean enableButtons) {
        contCommitMarketPlace.setEnabled(enableButtons);
        contCreateCommit.setEnabled(enableButtons);
        contJoinCommit.setEnabled(enableButtons);
        contMyCommittee.setEnabled(enableButtons);
    }

    protected boolean isEnableButtonOnStart() {
        return true;
    }

    private void initClickListeners() {
        contCommitMarketPlace.setDoubleTapSafeOnClickListener(this);
        contCreateCommit.setDoubleTapSafeOnClickListener(this);
        contJoinCommit.setDoubleTapSafeOnClickListener(this);
        contMyCommittee.setDoubleTapSafeOnClickListener(this);
    }

    private void init() {
        this.initClickListeners();
        shimmer.setVisibility(View.VISIBLE);
        mainLayout.setVisibility(View.GONE);
        iCommitteeActivityPresenter.getCommitteeMetadata();
        iCommitteeActivityPresenter.checkInvites(MDPreferenceManager.getUser().getMobileNumber());
    }

    @Override
    protected String getScreenName() {
        return "Create Committee";
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {

            case R.id.contCreateCommit:
//                Intent intent = new Intent(MobileDostApplication.getInstance(), TezCommitteeActivity.class);
//                startActivity(intent);
//                Toast.makeText(this, "Provide contCreateCommit from settings", Toast.LENGTH_SHORT).show();
                if (this.committeeDataResponse.isHasCommittee()) {
                    Toast.makeText(this, R.string.already_have_an_active_committee, Toast.LENGTH_LONG).show();
                } else
                    CommitteeCreationActivityRouter.createInstance().setDependenciesAndRoute(this, committeeDataResponse);
                break;

            case R.id.contCommitMarketPlace:
                //CommitteeSummaryActivityRouter.createInstance().setDependenciesAndRoute(this);

                Toast.makeText(this, "Will be implemented", Toast.LENGTH_SHORT).show();

                break;

            case R.id.contJoinCommit:
                if (this.committeeDataResponse.isHasCommittee()) {
                    Toast.makeText(this, R.string.already_have_an_active_committee, Toast.LENGTH_LONG).show();
                } else {
                    if (committeeInvitesList != null && committeeInvitesList.size() > 0) {
                        JoinCommitteeVerificationActivityRouter.createInstance().setDependenciesAndRoute(this, committeeInvitesList.get(0));
                    } else
                        Toast.makeText(this, R.string.donot_have_any_invitations, Toast.LENGTH_SHORT).show();
                }
//                CommitteeActivityRouter.createInstance().setDependenciesAndRoute(this, null);
//                finish();
                break;
            case R.id.contMyCommittee:
//                Toast.makeText(this, "Will be implemented contMyCommittee", Toast.LENGTH_SHORT).show();
                // CommitteePackagesActivityRouter.createInstance().setDependenciesAndRoute(this, null);
                if (this.committeeDataResponse.isHasCommittee()) {
                    MyCommitteeActivityRouter.createInstance().setDependenciesAndRoute(this);
                } else
                    Toast.makeText(this, R.string.dont_have_any_active_committee, Toast.LENGTH_LONG).show();

                break;

        }
    }

    @Override
    public void showLoader() {
        showTezLoader();
    }

    @Override
    public void hideLoader() {
        dismissTezLoader();
    }

    @Override
    public void storeMetadata(CommitteeMetaDataResponse committeeMetaDataResponse) {
        shimmer.setVisibility(View.GONE);
        mainLayout.setVisibility(View.VISIBLE);
        this.committeeDataResponse = committeeMetaDataResponse;
    }

    @Override
    public void committeeLoginSuccess(CommittteeLoginResponse committteeLoginResponse) {

    }

    @Override
    public void onCheckInvitesSuccess(List<CommitteeCheckInvitesResponse> committeeCheckInvitesResponse) {
        this.committeeInvitesList = committeeCheckInvitesResponse;
        if (committeeCheckInvitesResponse != null && committeeCheckInvitesResponse.size() > 0) {
            inviteeCount.setVisibility(View.VISIBLE);
            inviteeCount.setText(/*committeeCheckInvitesResponse.size() +*/ "1");
        }
    }
}
