package com.tez.androidapp.rewamp.committee.view;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezPinView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.presenter.CommitteeWalletVerificationPresenter;
import com.tez.androidapp.rewamp.committee.request.CommitteeInstallmentPayRequestCommittee;
import com.tez.androidapp.rewamp.committee.request.CommitteePaymentInitiateRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeInstallmentPayResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeWalletResponse;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.response.PaymentInitiateResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeContributeSummaryActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeWalletVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.SignUpActivity;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import static com.tez.androidapp.rewamp.committee.router.CommitteeWalletVerificationActivityRouter.COMMITTEE_WALLET_RESPONSE;
import static com.tez.androidapp.rewamp.committee.router.CommitteeWalletVerificationActivityRouter.MY_COMMITTEE_RESPONSE;

public class CommitteeWalletVerificationActivity extends BaseActivity implements TezPinView.OnPinEnteredListener, ICommitteeWalletVerificationActivityView, DoubleTapSafeOnClickListener {


    @BindView(R.id.tvResendCode)
    private TezTextView tvResendCode;

    @BindView(R.id.pinViewOTP)
    private TezPinView pinViewOtp;

    @BindView(R.id.btContinue)
    private TezButton btnContinue;


    private final CommitteeWalletVerificationPresenter committeeWalletVerificationPresenter;
    private String pin;
    private MyCommitteeResponse committeeResponse;
    private CommitteeWalletResponse committeeWalletResponse;
    private double latitude;
    private double longitude;

    public CommitteeWalletVerificationActivity() {
        this.committeeWalletVerificationPresenter = new CommitteeWalletVerificationPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee_wallet_verification);
        ViewBinder.bind(this);
        fetchExtras();
        initOnClickListener();
        getCurrentLocation(new LocationAvailableCallback() {
            @Override
            public void onLocationAvailable(@NonNull Location location) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
        });
    }

    private void fetchExtras() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey(MY_COMMITTEE_RESPONSE)) {
                committeeResponse = getIntent().getExtras().getParcelable(MY_COMMITTEE_RESPONSE);
            }

            if (getIntent().getExtras().containsKey(COMMITTEE_WALLET_RESPONSE)) {
                committeeWalletResponse = getIntent().getExtras().getParcelable(COMMITTEE_WALLET_RESPONSE);
            }
        }
    }

    @Override
    protected String getScreenName() {
        return CommitteeWalletVerificationActivity.class.getSimpleName();
    }

    @Override
    public void onPinEntered(@NonNull String pin) {
        this.pin = pin;
        btnContinue.setButtonNormal();
    }

    private void initOnClickListener() {
        pinViewOtp.setOnPinEnteredListener(this);
        btnContinue.setDoubleTapSafeOnClickListener(this);
    }


    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void showLoader() {
        showTezLoader();
    }

    @Override
    public void hideLoader() {
        dismissTezLoader();
    }

    @Override
    public void onInitiatePayment(PaymentInitiateResponse paymentInitiateResponse) {

        if (paymentInitiateResponse.getCommittee().resumeAsyncResponse) {
            MyCommitteeResponse.Committee committee = committeeResponse.getCommitteeList().get(0);
            CommitteeInstallmentPayRequestCommittee committeeInstallmentPayRequestCommittee = new CommitteeInstallmentPayRequestCommittee();
            committeeInstallmentPayRequestCommittee.setCommitteeId(committee.getId());
            committeeInstallmentPayRequestCommittee.setOtp("");
            committeeInstallmentPayRequestCommittee.setAmount(committee.getAmount());
            committeeInstallmentPayRequestCommittee.setMobileAccountId(committeeWalletResponse.getWallets().get(0).getMobileAccountId());
            committeeInstallmentPayRequestCommittee.setPin(pinViewOtp.getValueText().toString());
            committeeInstallmentPayRequestCommittee.setLat(latitude);
            committeeInstallmentPayRequestCommittee.setLng(longitude);

            committeeWalletVerificationPresenter.payInstallment(committeeInstallmentPayRequestCommittee);
        } else {
            Toast.makeText(this, getString(R.string.common_google_play_services_unknown_issue), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onInstallmentPay(CommitteeInstallmentPayResponse committeeInstallmentPayResponse) {
        CommitteeContributeSummaryActivityRouter.createInstance().setDependenciesAndRoute(this, committeeInstallmentPayResponse);
    }


    @Override
    public void doubleTapSafeOnClick(View view) {
        switch (view.getId()) {
            case R.id.btContinue:
                sendOtpVerificationRequest();
                break;
        }
    }

    private void sendOtpVerificationRequest() {
        if (pinViewOtp.getValueText().length() == 5) {
            if (latitude != 0.0 && longitude != 0.0) {

                MyCommitteeResponse.Committee committee = committeeResponse.getCommitteeList().get(0);

                CommitteePaymentInitiateRequest committeePaymentInitiateRequest = new CommitteePaymentInitiateRequest();
                committeePaymentInitiateRequest.setCommitteeId(committee.getId());
                committeePaymentInitiateRequest.setOtp("");
                committeePaymentInitiateRequest.setAmount(committee.getAmount());
                committeePaymentInitiateRequest.setMobileAccountId(committeeWalletResponse.getWallets().get(0).getMobileAccountId());
                committeePaymentInitiateRequest.setPin(pinViewOtp.getValueText().toString());
                committeePaymentInitiateRequest.setLat(latitude);
                committeePaymentInitiateRequest.setLng(longitude);

//                committeeWalletVerificationPresenter.initiatePayment(committeePaymentInitiateRequest);

                CommitteeInstallmentPayResponse committeeInstallmentPayResponse = new CommitteeInstallmentPayResponse();
                committeeInstallmentPayResponse.setMessage("Hello Message jee");
                CommitteeInstallmentPayResponse.PaymentInfo paymentInfo = new CommitteeInstallmentPayResponse.PaymentInfo();
                paymentInfo.setDate("12-12-2020");
                paymentInfo.setCommitteeAmount(1234);
                paymentInfo.setTransactionId("123");
                paymentInfo.setInstallmentAmount(5000);
                paymentInfo.setRound(1);
                paymentInfo.setWalletNumber("123456");
                paymentInfo.setWalletProvider("22558877");
                committeeInstallmentPayResponse.setPaymentInfo(paymentInfo);
                CommitteeContributeSummaryActivityRouter.createInstance().setDependenciesAndRoute(this, committeeInstallmentPayResponse);

            } else {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Can not leave empty fields", Toast.LENGTH_SHORT).show();
        }
    }
}