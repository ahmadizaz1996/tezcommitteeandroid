package com.tez.androidapp.app.general.feature.transactions.models.network;

import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;

import java.io.Serializable;

/**
 * Created  on 9/2/2017.
 */

public class Transaction implements Serializable {

    private static final String TYPE_ADVANCE = "ADVANCE";
    private static final String STATUS_CLEARED = "CLEARED";
    private String title;
    private String paymentStatus;
    private String productType;
    private String dueDate;
    private String tenure;
    private Integer id;
    private Double amount;
    private String policyNumber;
    private String walletName;
    private String walletNumber;
    private String transactionDate;
    private Integer productId;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public String getDueDate() {
        return Utility.getFormattedDateWithUpdatedTimeZone(dueDate, Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStringAmount() {
        return Utility.getFormattedAmountWithoutCurrencyLabel(getAmount());
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public String getWalletName() {
        return walletName;
    }

    public String getWalletNumber() {
        return walletNumber;
    }

    public Integer getProductId() {
        return productId;
    }

    public boolean isAdvance() {
        return productType.equalsIgnoreCase(TYPE_ADVANCE);
    }

    public boolean isCleared() {
        return paymentStatus.equalsIgnoreCase(STATUS_CLEARED);
    }
}
