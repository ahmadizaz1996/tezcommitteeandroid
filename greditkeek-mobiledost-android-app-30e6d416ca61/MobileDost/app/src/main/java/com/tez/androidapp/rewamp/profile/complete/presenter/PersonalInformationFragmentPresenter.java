package com.tez.androidapp.rewamp.profile.complete.presenter;

import android.view.View;

import com.google.android.gms.common.util.CollectionUtils;
import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Question;
import com.tez.androidapp.rewamp.profile.complete.QuestionDropDownAdapter;
import com.tez.androidapp.rewamp.profile.complete.interactor.IPersonalInformationFragmentInteractor;
import com.tez.androidapp.rewamp.profile.complete.interactor.PersonalInformationFragmentInteractor;
import com.tez.androidapp.rewamp.profile.complete.view.IPersonalInformationFragmentView;

import net.tez.fragment.util.optional.Optional;
import net.tez.logger.library.utils.TextUtil;

import java.util.List;

public class PersonalInformationFragmentPresenter implements IPersonalInformationFragmentPresenter,
        IPersonalInformationFragmentInteractorOutput,
        QuestionDropDownAdapter.QuestionDropDownAdapterOnClickCallback {

    private final IPersonalInformationFragmentView iPersonalInformationFragmentView;
    private final IPersonalInformationFragmentInteractor iPersonalInformationFragmentInteractor;
    private QuestionDropDownAdapter questionDropDownAdapter;

    public PersonalInformationFragmentPresenter(IPersonalInformationFragmentView iPersonalInformationFragmentView) {
        this.iPersonalInformationFragmentView = iPersonalInformationFragmentView;
        this.iPersonalInformationFragmentInteractor = new PersonalInformationFragmentInteractor(this);
    }

    @Override
    public void getPersonalQuestions(String lang) {
        this.iPersonalInformationFragmentInteractor.getPersonalQuestions(lang);
    }

    @Override
    public void updatePersonalInfo(String file, String questionRefName) {
        List<AnswerSelected> answerSelectedList = questionDropDownAdapter.getAnsweredData();
        Optional.doWhen(!CollectionUtils.isEmpty(answerSelectedList), () -> {
            this.iPersonalInformationFragmentView.showTezLoader();
            this.iPersonalInformationFragmentInteractor.updatePersonalInfo(file, questionRefName, answerSelectedList);
        }, () -> this.iPersonalInformationFragmentView.showInformativeMessage(R.string.please_fill_questions));
    }


    @Override
    public void onPersonalQuestionSuccess(List<Question> data) {
        this.questionDropDownAdapter = new QuestionDropDownAdapter(data, this);
        this.iPersonalInformationFragmentView.setAdapterToRecyclerView(questionDropDownAdapter);
        this.iPersonalInformationFragmentView.setVisibillityToShimmer(View.GONE);
        this.iPersonalInformationFragmentView.setVisibillityToTezCardView(View.VISIBLE);
    }

    @Override
    public void onPersonalQuestionFailure(int errorCode, String message) {
        this.iPersonalInformationFragmentView.dismissTezLoader();
        this.iPersonalInformationFragmentView.showError(errorCode);
    }

    @Override
    public void allQuestionHasBeenAnswered() {
    }

    @Override
    public void lastQuestionToUploadPictureIsRemaining(Question question) {
        this.iPersonalInformationFragmentView.setVisibillityToUploadImageCardView(View.VISIBLE);
        this.iPersonalInformationFragmentView.setTextToUploadImageCardView(question.getText());
        Optional.doWhen(TextUtil.isNotEmpty(question.getAnswerRefName()),
                iPersonalInformationFragmentView::userHasAlreadyUploadedProofOfWork);
        this.iPersonalInformationFragmentView.setOnClickListenerToUploadImageCardView((v) ->
                iPersonalInformationFragmentView.startCameraForUploadPicture(question.getRefName()));
    }

    @Override
    public void clearPreviouslyAskedQuestion() {
        this.iPersonalInformationFragmentView.setVisibillityToUploadImageCardView(View.GONE);
        this.iPersonalInformationFragmentView.clearUploadImageCvData();
        this.iPersonalInformationFragmentView.setOnClickListenerToUploadImageCardView(null);
    }

    @Override
    public void onUpdatePersonalInfoSuccess(boolean isProfileCompleted) {
        this.iPersonalInformationFragmentView.setTezLoaderToBeDissmissedOnTransition();
        Optional.doWhen(isProfileCompleted,
                this.iPersonalInformationFragmentView::userProfileCompleted,
                this.iPersonalInformationFragmentView::userProfileIsNotCompleted);
    }

    @Override
    public void onUpdatePersonalInfoFailure(int errorCode, String message) {
        this.iPersonalInformationFragmentView.dismissTezLoader();
        this.iPersonalInformationFragmentView.showError(errorCode);
    }
}
