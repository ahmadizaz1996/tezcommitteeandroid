package com.tez.androidapp.rewamp.general.profile.router;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.profile.view.BasicProfileActivity;
import com.tez.androidapp.rewamp.signup.SignUpActivity;
import com.tez.androidapp.rewamp.signup.router.SignupActivityRouter;

public class BasicProfileActivityRouter extends BaseActivityRouter {
    public static BasicProfileActivityRouter createInstance() {
        return new BasicProfileActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, BasicProfileActivity.class);
    }
}
