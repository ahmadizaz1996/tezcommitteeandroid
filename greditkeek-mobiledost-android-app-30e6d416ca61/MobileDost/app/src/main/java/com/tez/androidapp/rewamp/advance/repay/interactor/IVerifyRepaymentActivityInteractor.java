package com.tez.androidapp.rewamp.advance.repay.interactor;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks.InitiateRepaymentCallback;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.InitiateRepaymentRequest;
import com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request.RepayLoanRequest;

public interface IVerifyRepaymentActivityInteractor extends IBaseInteractor {

    void initiateRepayment(@NonNull InitiateRepaymentRequest initiateRepaymentRequest);

    void repayLoan(@NonNull RepayLoanRequest repayLoanRequest);

    void resendCode(@NonNull InitiateRepaymentRequest request, @NonNull InitiateRepaymentCallback callback);
}
