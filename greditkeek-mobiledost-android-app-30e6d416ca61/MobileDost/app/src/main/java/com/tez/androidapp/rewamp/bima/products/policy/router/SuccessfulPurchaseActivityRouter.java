package com.tez.androidapp.rewamp.bima.products.policy.router;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.response.PurchasedInsurancePolicy;
import com.tez.androidapp.rewamp.bima.products.policy.view.SuccessfulPurchaseActivity;

public class SuccessfulPurchaseActivityRouter extends BaseActivityRouter {

    public static final String PURCHASED_POLICY = "purchase_policy";

    public static SuccessfulPurchaseActivityRouter createInstance() {
        return new SuccessfulPurchaseActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, @NonNull PurchasedInsurancePolicy purchasedInsurancePolicy) {
        Intent intent = createIntent(from);
        intent.putExtra(PURCHASED_POLICY, purchasedInsurancePolicy);
        route(from, intent);
    }

    private Intent createIntent(@NonNull AppCompatActivity from) {
        return new Intent(from, SuccessfulPurchaseActivity.class);
    }
}
