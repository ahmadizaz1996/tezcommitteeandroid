package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.verification.WelcomeBackActivityNumberVerification;

import net.tez.fragment.util.optional.Optional;

public class WelcomeBackActivityNumberVerificationRouter extends BaseActivityRouter {

    public static final String USER_PIN = "USER_PIN";
    public static final String CALL_DEVICE_KEY = "CALL_DEVICE_KEY";
    public static final String USER_SESSION_TIMEOUT = "user_session_timeout";

    public static WelcomeBackActivityNumberVerificationRouter createInstance() {
        return new WelcomeBackActivityNumberVerificationRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @NonNull String mobileNumber,
                                        @NonNull String userPin,
                                        @Nullable Boolean callDeviceKey,
                                        boolean userSessionTimeout,
                                        @Nullable String userPushRoute) {

        Intent intent = createIntent(from);
        intent.putExtra(NumberVerificationActivityRouter.MOBILE_NUMBER, mobileNumber);
        intent.putExtra(NumberVerificationActivityRouter.PRINCIPLE, mobileNumber);
        intent.putExtra(USER_PIN, userPin);
        intent.putExtra(CALL_DEVICE_KEY, callDeviceKey);
        intent.putExtra(USER_SESSION_TIMEOUT, userSessionTimeout);
        intent.putExtra(PushNotificationConstants.KEY_ROUTE_TO, userPushRoute);
        Optional.doWhen(userSessionTimeout,
                ()-> routeForResult(from, intent, Constants.USER_SESSION_EXPIRED_REQUEST_CODE),
                ()-> route(from, intent));
    }

    public void setDependenceAndRouteForForwardResult(@NonNull BaseActivity from,
                                                      @NonNull String mobileNumber,
                                                      @NonNull String userPin,
                                                      @Nullable Boolean callDeviceKey,
                                                      boolean userSessionTimeout,
                                                      @Nullable String userPushRoute){
        Intent intent = createIntent(from);
        intent.putExtra(NumberVerificationActivityRouter.MOBILE_NUMBER, mobileNumber);
        intent.putExtra(NumberVerificationActivityRouter.PRINCIPLE, mobileNumber);
        intent.putExtra(USER_PIN, userPin);
        intent.putExtra(CALL_DEVICE_KEY, callDeviceKey);
        intent.putExtra(USER_SESSION_TIMEOUT, userSessionTimeout);
        intent.putExtra(PushNotificationConstants.KEY_ROUTE_TO, userPushRoute);
        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, WelcomeBackActivityNumberVerification.class);
    }
}
