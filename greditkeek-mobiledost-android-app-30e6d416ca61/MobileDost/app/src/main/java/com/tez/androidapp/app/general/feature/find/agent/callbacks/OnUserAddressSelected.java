package com.tez.androidapp.app.general.feature.find.agent.callbacks;

import androidx.annotation.Nullable;

import java.io.Serializable;

/**
 * Created by VINOD KUMAR on 11/1/2018.
 */
@FunctionalInterface
public interface OnUserAddressSelected extends Serializable {

    void onUserAddressSelected(@Nullable String address, double latitude, double longitude);
}
