package com.tez.androidapp.rewamp.general.changepin;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.validators.annotations.ConfirmPinViewRegex;
import com.tez.androidapp.commons.validators.annotations.PinViewRegex;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezPinEditText;

import net.tez.validator.library.annotations.Order;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ChangePinActivity extends PlaybackActivity implements IChangePinActivityView, ValidationListener {


    private final IChangePinActivityPresenter iChangePinActivityPresenter;

    @PinViewRegex(regex = Constants.STRING_PIN_VALIDATOR_REGEX, value = {1, R.string.please_enter_valid_pin})
    @Order(1)
    @BindView(R.id.pinEtOldPin)
    private TezPinEditText pinEtOldPin;

    @PinViewRegex(regex = Constants.STRING_PIN_VALIDATOR_REGEX, value = {1, R.string.please_enter_valid_pin})
    @Order(2)
    @BindView(R.id.pinEtNewPin)
    private TezPinEditText pinEtNewPin;

    @ConfirmPinViewRegex(id = R.id.pinEtNewPin, value = {1, R.string.please_enter_valid_pin})
    @Order(3)
    @BindView(R.id.pinEtConfirmPin)
    private TezPinEditText pinEtConfirmPin;

    @BindView(R.id.btChangePin)
    private TezButton btChangePin;
    private CompositeDisposable allDisposables;

    public ChangePinActivity() {
        this.iChangePinActivityPresenter = new ChangePinActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);
        initOnClickListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.allDisposables = new CompositeDisposable();
        listenChangesOnViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        allDisposables.dispose();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    private void listenChangesOnViews() {
        allDisposables.add(RxTextView.textChanges(this.pinEtOldPin.getPinEditText()).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
        allDisposables.add(RxTextView.textChanges(this.pinEtNewPin.getPinEditText()).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
        allDisposables.add(RxTextView.textChanges(this.pinEtConfirmPin.getPinEditText()).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
    }

    private void initOnClickListeners() {
        this.btChangePin.setDoubleTapSafeOnClickListener(view -> onClickBtChangePin());
    }

    @Override
    public void onPinChangedSuccessfully() {
        finish();
        InAppPinChangedActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void setBtChangePinEnabled(boolean enabled) {
        this.btChangePin.setEnabled(enabled);
    }

    @Override
    public void clearPin() {
        this.pinEtOldPin.clearText();
        this.pinEtNewPin.clearText();
        this.pinEtConfirmPin.clearText();
    }

    private void onClickBtChangePin() {
        FieldValidator.validate(this, new ValidationListener() {
            @Override
            public void validateSuccess() {
                iChangePinActivityPresenter.changePin(pinEtNewPin.getPin(), pinEtOldPin.getPin());
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                btChangePin.setButtonInactive();
                filterChain.doFilter();
            }
        });
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    public void validateSuccess() {
        this.btChangePin.setButtonNormal();
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.btChangePin.setButtonInactive();
        filterChain.doFilter();
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    protected String getScreenName() {
        return "ChangePinActivity";
    }
}
