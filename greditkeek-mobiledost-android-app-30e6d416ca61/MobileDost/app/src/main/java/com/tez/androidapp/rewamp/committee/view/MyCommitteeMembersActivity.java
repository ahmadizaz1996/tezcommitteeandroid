package com.tez.androidapp.rewamp.committee.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.committee.adapter.MyCommitteeInviteesAdapter;
import com.tez.androidapp.rewamp.committee.adapter.MyCommitteeMembersAdapter;
import com.tez.androidapp.rewamp.committee.adapter.MyCommitteeMembersDetailedAdapter;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.router.MyCommitteeMembersActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;

public class MyCommitteeMembersActivity extends BaseActivity implements MyCommitteeMembersAdapter.MyCommitteeMemberListener, MyCommitteeMembersDetailedAdapter.MyCommitteeMemberListener {


    @BindView(R.id.membersRecyclerView)
    RecyclerView membersRecyclerView;

    private ArrayList<MyCommitteeResponse.Committee.Member> mMemberArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_committee_members);
        ViewBinder.bind(this);
        fetchExtras();

    }

    private void fetchExtras() {
        if (getIntent() != null &&
                getIntent().getExtras() != null &&
                getIntent().getExtras().containsKey(MyCommitteeMembersActivityRouter.MEMBERS_LIST)) {
            mMemberArrayList = getIntent().getExtras().getParcelableArrayList(MyCommitteeMembersActivityRouter.MEMBERS_LIST);
            MyCommitteeMembersDetailedAdapter myCommitteeInviteesAdapter = new MyCommitteeMembersDetailedAdapter(mMemberArrayList, this);
            membersRecyclerView.setAdapter(myCommitteeInviteesAdapter);
        }
    }

    @Override
    protected String getScreenName() {
        return MyCommitteeMembersActivity.class.getSimpleName();
    }

    @Override
    public void onClickMembers(@NonNull MyCommitteeResponse.Committee.Member member) {

    }
}