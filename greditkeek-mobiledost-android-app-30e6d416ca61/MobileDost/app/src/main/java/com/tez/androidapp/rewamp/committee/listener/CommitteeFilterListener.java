package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeWalletResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface CommitteeFilterListener {

    void onGetFilterSuccess(CommitteeFilterResponse committeeFilterResponse);

    void onGetFilterFailure(int errorCode, String message);
}
