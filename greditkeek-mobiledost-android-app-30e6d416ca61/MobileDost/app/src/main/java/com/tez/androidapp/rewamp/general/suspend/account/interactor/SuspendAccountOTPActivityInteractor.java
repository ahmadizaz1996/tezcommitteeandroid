package com.tez.androidapp.rewamp.general.suspend.account.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.SuspendAccountCallback;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.SuspendAccountVerifyOTPCallback;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.general.suspend.account.presenter.ISuspendAccountOTPActivityInteractorOutput;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public class SuspendAccountOTPActivityInteractor implements ISuspendAccountOTPActivityInteractor {

    private final ISuspendAccountOTPActivityInteractorOutput iSuspendAccountOTPActivityInteractorOutput;

    public SuspendAccountOTPActivityInteractor(ISuspendAccountOTPActivityInteractorOutput iSuspendAccountOTPActivityInteractorOutput) {
        this.iSuspendAccountOTPActivityInteractorOutput = iSuspendAccountOTPActivityInteractorOutput;
    }

    @Override
    public void resendCode() {
        UserAuthCloudDataStore.getInstance().suspendAccount(new SuspendAccountCallback() {
            @Override
            public void onSuspendAccountSuccess(BaseResponse baseResponse) {
                iSuspendAccountOTPActivityInteractorOutput.onResendCodeSuccess();
            }

            @Override
            public void onSuspendAccountFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    resendCode();
                else
                    iSuspendAccountOTPActivityInteractorOutput.onResendCodeFailure(errorCode);
            }
        });
    }

    @Override
    public void suspendAccount() {
        UserAuthCloudDataStore.getInstance().suspendAccount(new SuspendAccountCallback() {
            @Override
            public void onSuspendAccountSuccess(BaseResponse baseResponse) {
                iSuspendAccountOTPActivityInteractorOutput.onSuspendAccountSuccess(baseResponse);
            }

            @Override
            public void onSuspendAccountFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    suspendAccount();
                else
                    iSuspendAccountOTPActivityInteractorOutput.onSuspendAccountFailure(errorCode, message);
            }
        });
    }

    @Override
    public void suspendAccountVerifyOtp(@NonNull String otp) {
        UserAuthCloudDataStore.getInstance().suspendAccountVerifyOTP(otp, new SuspendAccountVerifyOTPCallback() {
            @Override
            public void onSuspendAccountVerifyOTPSuccess(BaseResponse baseResponse) {
                iSuspendAccountOTPActivityInteractorOutput.onSuspendAccountVerifyOTPSuccess(baseResponse);
            }

            @Override
            public void onSuspendAccountVerifyOTPFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    suspendAccountVerifyOtp(otp);
                else
                    iSuspendAccountOTPActivityInteractorOutput.onSuspendAccountVerifyOTPFailure(errorCode, message);
            }
        });
    }
}
