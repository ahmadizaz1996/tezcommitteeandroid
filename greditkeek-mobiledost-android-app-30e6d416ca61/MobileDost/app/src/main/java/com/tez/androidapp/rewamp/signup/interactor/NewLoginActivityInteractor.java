package com.tez.androidapp.rewamp.signup.interactor;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserLoginCallback;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.UserReActivateAccountResendOTPCallback;
import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;
import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.signup.presenter.INewLoginActivityInteractorOutput;


public class NewLoginActivityInteractor implements INewLoginActivityInteractor {

    private final INewLoginActivityInteractorOutput iNewLoginActivityInteractorOutput;

    public NewLoginActivityInteractor(INewLoginActivityInteractorOutput iNewLoginActivityInteractorOutput) {
        this.iNewLoginActivityInteractorOutput = iNewLoginActivityInteractorOutput;
    }

    @Override
    public void callLogin(UserLoginRequest loginRequest){
        UserCloudDataStore.getInstance().login(loginRequest, new UserLoginCallback() {
            @Override
            public void onUserLoginSuccess(UserLoginResponse loginResponse) {
                iNewLoginActivityInteractorOutput.onUserLoginSuccess(loginResponse);
            }

            @Override
            public void onUserLoginFailure(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        ()->callLogin(loginRequest),
                        ()-> iNewLoginActivityInteractorOutput.onUserLoginFailure(errorCode, message));
            }
        });
    }

    @Override
    public void resendAccountReactivationOtp(){
        UserAuthCloudDataStore.getInstance().userReActivateAccountResendOTP(new UserReActivateAccountResendOTPCallback() {
            @Override
            public void onUserAccountReActivateResendOTPSuccess(BaseResponse baseResponse) {
                iNewLoginActivityInteractorOutput.onUserAccountReActivateResendOTPSuccess(baseResponse);
            }

            @Override
            public void onUserAccountReActivateResendOTPFailure(BaseResponse baseResponse) {
                if(Utility.isUnauthorized(baseResponse.getStatusCode())){
                    resendAccountReactivationOtp();
                } else{
                    iNewLoginActivityInteractorOutput.onUserAccountReActivateResendOTPFailure(baseResponse);
                }
            }
        });
    }

}
