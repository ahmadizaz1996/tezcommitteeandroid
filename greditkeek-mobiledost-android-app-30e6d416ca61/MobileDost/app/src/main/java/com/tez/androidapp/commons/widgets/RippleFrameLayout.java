package com.tez.androidapp.commons.widgets;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.tez.androidapp.R;

/**
 * Created by VINOD KUMAR on 7/30/2019.
 */
public class RippleFrameLayout extends TezFrameLayout {
    public RippleFrameLayout(@NonNull Context context) {
        super(context);
        init(context);
    }

    public RippleFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RippleFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(@NonNull Context context) {
        this.setBackgroundColor(getColor(context, R.color.transparent));
    }
}
