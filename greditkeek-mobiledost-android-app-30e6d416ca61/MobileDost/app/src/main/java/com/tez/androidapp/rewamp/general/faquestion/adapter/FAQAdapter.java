package com.tez.androidapp.rewamp.general.faquestion.adapter;

import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.faq.models.network.FAQ;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.tezrecycler.adapter.GenericDropDownAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.viewholder.GenericDropDownAdapterViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

import io.reactivex.annotations.Nullable;

public class FAQAdapter extends GenericDropDownAdapter<FAQ, BaseRecyclerViewListener, FAQAdapter.FAQViewHolder> {

    public FAQAdapter(List<FAQ> faqs){
        super(faqs);
    }

    @NonNull
    @Override
    public FAQViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_faqs, parent, false);
        return new FAQAdapter.FAQViewHolder(this, view);
    }

    class FAQViewHolder extends GenericDropDownAdapterViewHolder<FAQ, BaseRecyclerViewListener> {

        @BindView(R.id.tv_title)
        private TezTextView textViewTitle;

        @BindView(R.id.v_onclick)
        private View viewOnClick;

        @BindView(R.id.imageViewCaret)
        private TezImageView imageViewCaret;

        @BindView(R.id.tv_Description)
        private TezTextView textViewDescription;

        @BindView(R.id.viewSeparator)
        private TezImageView viewSparator;

        private FAQViewHolder(GenericDropDownAdapter adapter, View itemView) {
            super(adapter, itemView);
            ViewBinder.bind(this, itemView);
        }


        @Override
        public void onBind(int position, List<FAQ> items, @Nullable BaseRecyclerViewListener listener) {
            super.onBind(position, items, listener);
            FAQ faq = items.get(position);
            this.textViewTitle.setText(faq.getTitle());
            this.textViewDescription.setText(Html.fromHtml(faq.getDescription()));
            this.textViewDescription.setMovementMethod(LinkMovementMethod.getInstance());
        }

        @NonNull
        @Override
        protected View getHeader() {
            return this.itemView.findViewById(R.id.v_onclick );
        }

        @Override
        protected void setOnHeaderClickListener(List<FAQ> items, int position) {
            super.setOnHeaderClickListener(items, position);
        }

        @Override
        protected void onOpen() {
            textViewDescription.setVisibility(View.VISIBLE);
            this.imageViewCaret.setRotation(180);
            this.viewSparator.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onClose() {
            textViewDescription.setVisibility(View.GONE);
            this.imageViewCaret.setRotation(0);
            this.viewSparator.setVisibility(View.GONE);
        }
    }
}
