package com.tez.androidapp.rewamp.profile.complete.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.profile.complete.view.CompleteProfileCnicUploadActivity;
import com.tez.androidapp.rewamp.profile.router.CompleteProfileRouter;

public class CompleteProfileCnicUploadActivityRouter extends CompleteProfileRouter {

    public static final String  IS_CNIC_FRONT_PIC_TO_BE_UPLOADED = "FRONT_CNIC";
    public static final String IS_CNIC_BACK_PIC_TO_BE_UPLOADED = "BACK_CNIC";
    public static final String IS_CNIC_SELFIE_TO_BE_UPLOADED = "SELFIE";

    public static CompleteProfileCnicUploadActivityRouter createInstance() {
        return new CompleteProfileCnicUploadActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        boolean isCnicFrontPicToBeUploaded,
                                        boolean isCnicBackPicToBeUploaded,
                                        boolean isCnicSelfieToBeUploaded,
                                        int routeTo) {
        Intent intent = createIntent(from);
        intent.putExtra(IS_CNIC_FRONT_PIC_TO_BE_UPLOADED, isCnicFrontPicToBeUploaded);
        intent.putExtra(IS_CNIC_BACK_PIC_TO_BE_UPLOADED, isCnicBackPicToBeUploaded);
        intent.putExtra(IS_CNIC_SELFIE_TO_BE_UPLOADED, isCnicSelfieToBeUploaded);
        addAfterCompletionRoute(intent, routeTo);
        route(from, intent);
    }



    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CompleteProfileCnicUploadActivity.class);
    }
}
