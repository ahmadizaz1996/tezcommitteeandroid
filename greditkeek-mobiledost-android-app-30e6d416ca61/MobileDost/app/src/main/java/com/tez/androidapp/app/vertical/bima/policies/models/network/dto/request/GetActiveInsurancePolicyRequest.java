package com.tez.androidapp.app.vertical.bima.policies.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 8/25/2017.
 */

public class GetActiveInsurancePolicyRequest extends BaseRequest{

    public static final String METHOD_NAME = "v1/insurance/policy/active";
}
