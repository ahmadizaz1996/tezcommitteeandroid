package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class PersonalQuestionRequest extends BaseRequest {
    public static final String METHOD_NAME = "query/model";

    public static final class Params {
        public static final String LANG = "lang";
        private Params() {
        }
    }
}
