package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;
import android.os.Bundle;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.GetInvitedPackageResponse;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeInviteContactsActivity;
import com.tez.androidapp.rewamp.committee.view.CommitteeSummaryActivity;
import com.tez.androidapp.rewamp.committee.view.JoinCommitteeInvitationActivity;

import androidx.annotation.NonNull;

import static com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter.COMMITTEE_DATA;

public class CommitteeSummaryActivityRouter extends BaseActivityRouter {


    public static final String COMMITTE_CREATEION = "COMMITTE_CREATEION";
    public static final String MY_COMMITTEE_RESPONSE = "MY_COMMITTEE_RESPONSE";
    public static final String GET_INVITED_COMMITTEE_PACKAGE = "GET_INVITED_COMMITTEE_PACKAGE";
    public static final String JOIN_COMMITTEE_RESPONSE = "JOIN_COMMITTEE_RESPONSE";

    public static CommitteeSummaryActivityRouter createInstance() {
        return new CommitteeSummaryActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteePackageModel committeePackageModel) {
        Intent intent = createIntent(from);
        intent.putExtra(COMMITTEE_DATA, committeePackageModel);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteeCreateResponse committeeCreateResponse) {
        Intent intent = createIntent(from);
        intent.putExtra(COMMITTE_CREATEION, committeeCreateResponse);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteeCreateResponse committeeCreateResponse, CommitteeInviteRequest committeeInviteRequest) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putSerializable(COMMITTE_CREATEION, committeeCreateResponse);
        bundle.putParcelable(CommitteeInviteContactsActivity.INVITEES_LIST, committeeInviteRequest);
        intent.putExtras(bundle);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, MyCommitteeResponse myCommitteeResponse) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putParcelable(MY_COMMITTEE_RESPONSE, myCommitteeResponse);
        intent.putExtras(bundle);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeSummaryActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    public void setDependenciesAndRoute(BaseActivity from, GetInvitedPackageResponse.CommitteeList committeePackage) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putParcelable(GET_INVITED_COMMITTEE_PACKAGE, committeePackage);
        intent.putExtras(bundle);
        route(from, intent);
    }
}
