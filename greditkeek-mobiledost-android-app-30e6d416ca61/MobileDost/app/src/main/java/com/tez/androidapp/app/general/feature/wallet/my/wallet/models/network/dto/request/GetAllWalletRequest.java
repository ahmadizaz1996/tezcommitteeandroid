package com.tez.androidapp.app.general.feature.wallet.my.wallet.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 12/14/2017.
 */

public class GetAllWalletRequest extends BaseRequest{
    public static final String METHOD_NAME = "v1/user/wallets";
}
