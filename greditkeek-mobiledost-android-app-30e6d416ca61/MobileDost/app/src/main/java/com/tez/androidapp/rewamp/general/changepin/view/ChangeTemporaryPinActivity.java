package com.tez.androidapp.rewamp.general.changepin.view;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.general.changepin.presenter.ChangeTemporaryPinActivityPresenter;
import com.tez.androidapp.rewamp.general.changepin.presenter.IChangeTemporaryPinActivityPresenter;
import com.tez.androidapp.rewamp.general.changepin.router.TemporaryPinChangedActivityRouter;
import com.tez.androidapp.rewamp.signup.Create4DigitPin;
import com.tez.androidapp.rewamp.signup.router.PinChangedActivityRouter;

public class ChangeTemporaryPinActivity extends Create4DigitPin implements IChangeTemporaryPinActivityView {

    private final IChangeTemporaryPinActivityPresenter iChangeTemporaryPinActivityPresenter;

    public ChangeTemporaryPinActivity() {
        iChangeTemporaryPinActivityPresenter = new ChangeTemporaryPinActivityPresenter(this);
    }

    @Override
    protected void onClickMainButton() {
        iChangeTemporaryPinActivityPresenter.changeTemporaryPin(getPin());
    }

    @Override
    public void onTemporaryPinChangedSuccessfully() {
        setTezLoaderToBeDissmissedOnTransition();
        PinChangedActivityRouter.createInstance().setDependenciesAndRoute(this);
        finish();
    }

    @Override
    protected String getScreenName() {
        return "ChangeTemporaryPinActivity";
    }
}
