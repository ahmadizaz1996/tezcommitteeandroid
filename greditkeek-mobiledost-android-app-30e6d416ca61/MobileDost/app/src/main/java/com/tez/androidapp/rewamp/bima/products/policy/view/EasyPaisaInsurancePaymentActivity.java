package com.tez.androidapp.rewamp.bima.products.policy.view;

import android.location.Location;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezPinView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.repay.router.VerifyEasyPaisaRepaymentRouter;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.EasyPaisaInsurancePaymentActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.policy.presenter.IEasyPaisaInsurancePaymentActivityPresenter;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.response.PurchasedInsurancePolicy;
import com.tez.androidapp.rewamp.bima.products.policy.router.EasyPaisaInsurancePaymentActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.PurchasePolicyActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.SuccessfulPurchaseActivityRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import net.tez.viewbinder.library.core.BindView;

public class EasyPaisaInsurancePaymentActivity extends PlaybackActivity
        implements IEasyPaisaInsurancePaymentActivityView,
        TezPinView.OnPinEnteredListener{

    @BindView(R.id.pinViewOTP)
    protected TezPinView pinViewOTP;

    @BindView(R.id.tvResendCode)
    protected TezTextView tvResendCode;

    @BindView(R.id.tvDidNotReceiveCode)
    protected TezTextView tvDidNotReceiveCode;

    @BindView(R.id.tvEnterCode)
    protected TezTextView tvEnterCode;

    private IEasyPaisaInsurancePaymentActivityPresenter iEasyPaisaInsurancePaymentActivityPresenter;
    private static final int EASY_PAISA_PIN_LENGTH = 5;

    public EasyPaisaInsurancePaymentActivity() {
        iEasyPaisaInsurancePaymentActivityPresenter = new EasyPaisaInsurancePaymentActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_repayment);
        init();
    }

    protected void init(){
        pinViewOTP.setOnPinEnteredListener(this);
        initiateRepayment();
        initViews();
    }

    public void setIVerifyEasyPaisaRepaymentActivityPresenter(IEasyPaisaInsurancePaymentActivityPresenter iEasyPaisaInsurancePaymentActivityPresenter) {
        this.iEasyPaisaInsurancePaymentActivityPresenter = iEasyPaisaInsurancePaymentActivityPresenter;
    }

    @Override
    public void setPinViewOtpClearText() {
        pinViewOTP.clearText();
    }

    @Override
    public void setPinViewOtpEnabled(boolean enabled) {
        pinViewOTP.setEnabled(enabled);
    }

    @Override
    public void navigateToDashboard(){
        DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
    }

    @Override
    public String getFormattedErrorMessage(@StringRes int message) {
        String messageString = getString(message);
        messageString = messageString.replaceAll(ResponseStatusCode.PlaceHolders.WALLET_NAME.getValue(), Utility.getWalletName(getServiceProviderIdFromIntent()));
        return messageString.replaceAll(ResponseStatusCode.PlaceHolders.WALLET_SUPPORT_NUMBER.getValue(), Utility.getWalletProviderNumber(getServiceProviderIdFromIntent()));
    }

    protected void initViews() {
        tvDidNotReceiveCode.setVisibility(View.GONE);
        tvResendCode.setVisibility(View.GONE);
        pinViewOTP.setNumDigits(getPinLength());
        tvEnterCode.setText(getString(R.string.please_enter_your_wallet_pin_to_confirm_transaction,
                Utility.getWalletName(getServiceProviderIdFromIntent())));
    }

    protected int getPinLength(){
        return EASY_PAISA_PIN_LENGTH;
    }

    protected void initiateRepayment() {
        iEasyPaisaInsurancePaymentActivityPresenter.initiatePurchasePolicy(getInitiatePurchasePolicyRequest());
    }

    protected InitiatePurchasePolicyRequest getInitiatePurchasePolicyRequest() {
        return getIntent().getParcelableExtra(EasyPaisaInsurancePaymentActivityRouter.PRODUCT_DETAILS);
    }


    protected int getServiceProviderIdFromIntent() {
        return getIntent().getIntExtra(EasyPaisaInsurancePaymentActivityRouter.WALLET_PROVIDER_ID, -100);
    }


    @Override
    public void routeToRepaymentSuccessFullActivity(@NonNull PurchasedInsurancePolicy purchasedInsurancePolicy) {
        SuccessfulPurchaseActivityRouter.createInstance().setDependenciesAndRoute(this, purchasedInsurancePolicy);
        finish();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    public void onPinEntered(@NonNull String pin) {
        showTezLoader();
        getCurrentLocation(new LocationAvailableCallback() {
            @Override
            public void onLocationAvailable(@NonNull Location location) {
                purchasePolicy(pin, location);
            }

            @Override
            public void onLocationFailed(String message) {
                purchasePolicy(pin, null);
            }

            @Override
            public void onLocationPermissionDenied() {
                purchasePolicy(pin, null);
            }
        });
    }

    protected void purchasePolicy(@NonNull String pin, @Nullable Location location) {
        iEasyPaisaInsurancePaymentActivityPresenter.purchasePolicy(pin,location);
    }

    @Override
    protected String getScreenName() {
        return "EasyPaisaInsurancePaymentActivity";
    }


}
