package com.tez.androidapp.rewamp.signup.presenter;

import android.location.Location;

import androidx.annotation.NonNull;

import com.tez.androidapp.BuildConfig;
import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.dto.request.UserVerifyRequest;
import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserVerifyResponse;
import com.tez.androidapp.commons.utils.app.PermissionCheckUtil;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.models.request.VerifyActiveDeviceRequest;
import com.tez.androidapp.repository.network.models.response.VerifyActiveDeviceResponse;
import com.tez.androidapp.rewamp.signup.interactor.IWelcomeBackActivityInteractor;
import com.tez.androidapp.rewamp.signup.interactor.WelcomeBackActivityInteractor;
import com.tez.androidapp.rewamp.signup.view.IWelcomeBackActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import java.util.List;

public class WelcomeBackActivityPresenter implements IWelcomeBackActivityPresenter,
        IWelcomeBackActivityInteractorOutput {

    private final IWelcomeBackActivityInteractor iWelcomeBackActivityInteractor;
    private final IWelcomeBackActivityView iWelcomeBackActivitynumberVerificationView;

    public WelcomeBackActivityPresenter(IWelcomeBackActivityView iWelcomeBackActivitynumberVerificationView) {
        this.iWelcomeBackActivitynumberVerificationView = iWelcomeBackActivitynumberVerificationView;
        this.iWelcomeBackActivityInteractor = new WelcomeBackActivityInteractor(this);
    }

    @Override
    public void validatePin(String pin, String mobileNumber) {
        List<String> missingPermissions =
                PermissionCheckUtil.getInstance(MobileDostApplication.getInstance())
                        .getMissingPermissionsForSignUp(MobileDostApplication.getSignupPermissions());
        Optional.doWhen(missingPermissions.isEmpty(),
                () -> {
                    this.iWelcomeBackActivitynumberVerificationView.showTezLoader();
                    this.iWelcomeBackActivitynumberVerificationView.startValidation(getLocationCallback(pin, mobileNumber));
                },
                () -> this.iWelcomeBackActivitynumberVerificationView.askPermission(pin));
    }

    private LocationAvailableCallback getLocationCallback(String pin, String mobileNumber) {
        return new LocationAvailableCallback() {
            @Override
            public void onLocationPermissionDenied() {
                iWelcomeBackActivitynumberVerificationView.dismissTezLoader();
                iWelcomeBackActivitynumberVerificationView.showInformativeMessage(R.string.string_location_permission_required);
            }

            @Override
            public void onLocationAvailable(@NonNull Location location) {
                saveLocationInPreference(location);
                verifyUserActiveDevice(pin, mobileNumber);
            }

            @Override
            public void onLocationFailed(String message) {
                verifyUserActiveDevice(pin, mobileNumber);
            }
        };
    }

    @Override
    public void saveLocationInPreference(Location location) {
        iWelcomeBackActivityInteractor.saveLocationInPreference(location);
    }

    @Override
    public void verifyUserActiveDevice(String pin, String mobileNumber) {
        this.iWelcomeBackActivityInteractor.verifyUserActiveDevice(
                new VerifyActiveDeviceRequest(iWelcomeBackActivitynumberVerificationView.getDeviceInfo().getImei(),
                        mobileNumber), pin);
    }

    @Override
    public void onVerifyActiveDeviceSuccess(VerifyActiveDeviceResponse verifyActiveDeviceResponse, String pin) {
        boolean callDeviceKey = !verifyActiveDeviceResponse.getIsDeviceKeyPresent()
                || !MDPreferenceManager.isDeviceKeySentToServer();

        Optional.doWhen(verifyActiveDeviceResponse.getVerifyDevice(),
                () -> requestSinchVerification(verifyActiveDeviceResponse.getMobileNumber(), callDeviceKey, pin),
                () -> this.checkForVerifyOrLogin(callDeviceKey, pin));
    }

    @Override
    public void onVerifyActiveDeviceFailure(int errorCode, String message) {
        if (errorCode == ResponseStatusCode.USER_DOES_NOT_EXIST.getCode()) {
            this.iWelcomeBackActivitynumberVerificationView.setTezLoaderToBeDissmissedOnTransition();
            Utility.directUserToIntroScreenAtGlobalLevel();
        } else {
            this.iWelcomeBackActivitynumberVerificationView.dismissTezLoader();
            this.iWelcomeBackActivitynumberVerificationView.showError(errorCode,
                    ((dialog, which) -> this.iWelcomeBackActivitynumberVerificationView.finishActivity()));
        }
    }

    @Override
    public void checkForVerifyOrLogin(boolean callDeviceKey, String pin) {
        Optional.doWhen(MDPreferenceManager.getAccessToken() != null,
                () -> this.verifyUser(callDeviceKey, pin),
                () -> Optional.ifPresent(MDPreferenceManager.getUser(), user -> {
                    Optional.ifPresent(user.getPrincipalName(), principalName -> {
                        login(principalName, callDeviceKey, pin);
                    }, this::showUnExpectedErrorOccured);
                }, this::showUnExpectedErrorOccured));
    }

    private void login(String principalName, boolean callDeviceKey, String pin) {
        Optional.ifPresent(iWelcomeBackActivitynumberVerificationView.getHostActivity(), baseActivity -> {
            this.iWelcomeBackActivityInteractor.login(
                    Utility.createLoginRequest(baseActivity, principalName, pin),
                    callDeviceKey,
                    Utility.getImei(MobileDostApplication.getInstance()));
        });
    }

    private void showUnExpectedErrorOccured() {
        this.iWelcomeBackActivitynumberVerificationView.setTezLoaderToBeDissmissedOnTransition();
        this.iWelcomeBackActivitynumberVerificationView.showUnexpectedErrorDialog();
        //signOut.
    }

    private void requestSinchVerification(@NonNull String mobileNumber, boolean callDeviceKey, String pin) {
        this.iWelcomeBackActivitynumberVerificationView.setTezLoaderToBeDissmissedOnTransition();
        this.iWelcomeBackActivitynumberVerificationView.createSinchVerification(mobileNumber,
                callDeviceKey, pin);
    }

    private void verifyUser(boolean callDeviceKey, String pin) {
        this.iWelcomeBackActivityInteractor.verifyUser(
                new UserVerifyRequest(pin, BuildConfig.VERSION_NAME,
                        this.iWelcomeBackActivitynumberVerificationView.getDeviceInfo()),
                callDeviceKey, Utility.getImei(MobileDostApplication.getInstance()));
    }


    @Override
    public void onUserLoginSuccess(UserLoginResponse loginResponse) {
        if (loginResponse.getTemporaryPinSet())
            openTemporaryPin();

        else if(loginResponse.isUserActivationPending())
            userReactivateAccount();

        else
            routeToDashboard();
    }

    private void openTemporaryPin() {
        this.iWelcomeBackActivitynumberVerificationView.setTezLoaderToBeDissmissedOnTransition();
        this.iWelcomeBackActivitynumberVerificationView.startChangeTemporaryPinActivity();
    }

    private void userReactivateAccount(){
        this.iWelcomeBackActivitynumberVerificationView.setTezLoaderToBeDissmissedOnTransition();
        this.iWelcomeBackActivitynumberVerificationView.navigateToReactivateAccountActivity();
    }

    private void routeToDashboard() {
        this.iWelcomeBackActivitynumberVerificationView.setTezLoaderToBeDissmissedOnTransition();
        this.iWelcomeBackActivitynumberVerificationView.routeToDashboard();
    }

    @Override
    public void onUserLoginFailure(int errorCode, String message) {
        iWelcomeBackActivitynumberVerificationView.dismissTezLoader();
        if (errorCode == ResponseStatusCode.APP_VERSION_INCOMPATIBLE.getCode())
            this.iWelcomeBackActivitynumberVerificationView.showAppOutDatedDialog(ResponseStatusCode.getDescriptionFromErrorCode(errorCode));

        else if (errorCode == ResponseStatusCode.INVALID_DEVICE.getCode())
            this.iWelcomeBackActivitynumberVerificationView.showError(errorCode,
                    (d, w) -> this.iWelcomeBackActivitynumberVerificationView.finishActivity());

        else {
            this.iWelcomeBackActivitynumberVerificationView.showError(errorCode,
                    (d, w) -> this.iWelcomeBackActivitynumberVerificationView.finishActivity());
        }
    }

    @Override
    public void onUserVerifySuccess(UserVerifyResponse userVerifyResponse) {
        Optional.doWhen(userVerifyResponse.getTemporaryPinSet(),
                this::openTemporaryPin,
                this::routeToDashboard);
    }

    @Override
    public void onUserVerifyFailure(BaseResponse baseResponse) {
        this.iWelcomeBackActivitynumberVerificationView.dismissTezLoader();

        if (baseResponse.getStatusCode() == ResponseStatusCode.APP_VERSION_INCOMPATIBLE.getCode())
            this.iWelcomeBackActivitynumberVerificationView.showAppOutDatedDialog(ResponseStatusCode.getDescriptionFromErrorCode(baseResponse.getStatusCode()));

        else if (baseResponse.getStatusCode() == ResponseStatusCode.USER_SUSPENDED.getCode())
            this.iWelcomeBackActivitynumberVerificationView.showError(baseResponse.getStatusCode(),
                    ((dialog, which) -> iWelcomeBackActivitynumberVerificationView.makeCall()));

        else if (baseResponse.getStatusCode() == ResponseStatusCode.INVALID_DEVICE.getCode())
            iWelcomeBackActivitynumberVerificationView.showError(baseResponse.getStatusCode(),
                    (v, c) -> this.iWelcomeBackActivitynumberVerificationView.finishActivity());

        else if(baseResponse.getStatusCode() == ResponseStatusCode.USER_ACTIVATION_PENDING.getCode())
            iWelcomeBackActivitynumberVerificationView.navigateToReactivateAccountActivity();

        else {
            this.iWelcomeBackActivitynumberVerificationView.showError(baseResponse.getStatusCode(),
                    (v, c) -> this.iWelcomeBackActivitynumberVerificationView.finishActivity());
        }
    }
}
