package com.tez.androidapp.rewamp.general.wallet.adapter;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.WalletCardView;
import com.tez.androidapp.rewamp.TezDrawableHelper;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class ChangeWalletAdapter extends GenericRecyclerViewAdapter<Wallet, ChangeWalletAdapter.ChangeWalletListener, ChangeWalletAdapter.ChangeWalletViewHolder> {


    public ChangeWalletAdapter(@NonNull List<Wallet> items, @Nullable ChangeWalletListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public ChangeWalletViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.grid_item_change_wallet, parent);
        return new ChangeWalletViewHolder(view);
    }

    public interface ChangeWalletListener extends BaseRecyclerViewListener {

        void onClickWallet(@NonNull Wallet wallet);
    }

    class ChangeWalletViewHolder extends BaseViewHolder<Wallet, ChangeWalletListener> {

        @BindView(R.id.walletCard)
        private WalletCardView walletCard;

        ChangeWalletViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(Wallet item, @Nullable ChangeWalletListener listener) {
            super.onBind(item, listener);
            Drawable imgId = TezDrawableHelper.getDrawable(getContext(), Utility.getWalletIconResourceIdLinked(item.getServiceProviderId()));
            walletCard.setActiveImg(imgId);
            walletCard.setInActiveImg(imgId);
            walletCard.setWallet(item);
            walletCard.setCardType(item.isDefault() ? WalletCardView.CardType.DEFAULT : WalletCardView.CardType.ACTIVE);
            walletCard.setOnWalletSelectedListener((viewId, wallet, state) -> {
                if (listener != null)
                    listener.onClickWallet(item);
            });
        }
    }
}
