package com.tez.androidapp.rewamp.advance.repay.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.repay.view.RepayLoanActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class RepayLoanActivityRouter extends BaseActivityRouter {

    public static final String LOAN_ID = "LOAN_ID";

    public static RepayLoanActivityRouter createInstance() {
        return new RepayLoanActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, int loanId) {
        Intent intent = createIntent(from);
        intent.putExtra(LOAN_ID, loanId);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, RepayLoanActivity.class);
    }
}
