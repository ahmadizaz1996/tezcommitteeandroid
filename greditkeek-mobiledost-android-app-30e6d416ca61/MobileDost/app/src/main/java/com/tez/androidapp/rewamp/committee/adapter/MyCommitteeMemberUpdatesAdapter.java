package com.tez.androidapp.rewamp.committee.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponseMemberList;
import com.tez.androidapp.rewamp.committee.response.CommitteeResponseTimeline;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;

import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/

public class MyCommitteeMemberUpdatesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int HEADER_ITEM = 0;
    public static final int NORMAL_ITEM = 1;
    private final List<CommitteeFilterResponseMemberList> items;
    private final Context context;


    public MyCommitteeMemberUpdatesAdapter(@NonNull List<CommitteeFilterResponseMemberList> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        /*if (this.items.get(position).isHeader())
            return HEADER_ITEM;
        else*/
        return NORMAL_ITEM;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == NORMAL_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_committee_invitees, parent, false);
            return new MyCommitteeMemberViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate
                    (R.layout.header_date_item, parent, false);
            return new MyCommitteeMemberHeaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CommitteeFilterResponseMemberList committeeFilterResponseMemberList = this.items.get(position);

        MyCommitteeMemberViewHolder myCommitteeMemberViewHolder = (MyCommitteeMemberViewHolder) holder;
        myCommitteeMemberViewHolder.tvName.setText(committeeFilterResponseMemberList.getUsername());
        myCommitteeMemberViewHolder.tvDate.setText(committeeFilterResponseMemberList.getJoiningDate().split("T")[0]);
        myCommitteeMemberViewHolder.tvStatus.setVisibility(View.GONE);
        myCommitteeMemberViewHolder.tvMessage.setVisibility(View.GONE);


    }

    static class MyCommitteeMemberViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.tv_message)
        TextView tvMessage;

        @BindView(R.id.tv_status)
        TextView tvStatus;

        @BindView(R.id.tv_date)
        TextView tvDate;

        public MyCommitteeMemberViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

    }

    static class MyCommitteeMemberHeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date_txtView)
        private TezTextView datTxtView;

        @BindView(R.id.transactionRecyclerView)
        private RecyclerView transactionRecyclerView;

        @BindView(R.id.inviteRecyclerView)
        private RecyclerView inviteRecyclerView;


        public MyCommitteeMemberHeaderViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }
    }
}
