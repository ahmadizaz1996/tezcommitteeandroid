package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;

import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener;
import net.tez.viewbinder.library.core.ViewBinder;

/**
 * Created by VINOD KUMAR on 7/23/2019.
 */
public class InformativeDialog extends TezDialog {

    private String title;

    private String message;

    @Nullable
    private DoubleTapSafeDialogClickListener listener;

    public InformativeDialog(@NonNull Context context) {
        super(context);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setListener(@Nullable DoubleTapSafeDialogClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.informative_dialog);
        ViewBinder.bind(this);
        setTitle();
        setMessage();
        setOnCloseClickListener();
    }

    private void setTitle() {
        TezTextView tvTitle = findViewById(R.id.tvAlertTitle);
        tvTitle.setText(title);
    }

    private void setMessage() {
        TezTextView tvMessage = findViewById(R.id.tvAlertMessage);
        tvMessage.setText(message);
    }

    private void setOnCloseClickListener() {
        findViewById(R.id.flCancel).setOnClickListener(view -> {
            cancel();
            if (listener != null)
                listener.onClick(this, R.id.flCancel);
        });
    }
}
