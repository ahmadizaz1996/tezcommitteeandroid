package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request;

/**
 * Created by FARHAN DHANANI on 6/29/2018.
 */
public class MyClaimRequest {
    public static final String METHOD_NAME = "v1/insurance/claim/{" + MyClaimRequest.Params.INSURANCE_CLAIM_ID + "}";

    public abstract static class Params {
        public static final String INSURANCE_CLAIM_ID = "insurancePolicyId";
    }
}
