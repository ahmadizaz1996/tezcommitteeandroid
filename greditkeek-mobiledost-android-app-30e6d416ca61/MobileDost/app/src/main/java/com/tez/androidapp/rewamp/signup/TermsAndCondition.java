package com.tez.androidapp.rewamp.signup;

import java.io.Serializable;

/**
 * Created by FARHAN DHANANI on 6/1/2019.
 */
public class TermsAndCondition {

    private String termsAndCondtion;

    public TermsAndCondition(String termsAndCondtion){
        this.termsAndCondtion = termsAndCondtion;
    }

    public String getTermsAndCondtion() {
        return termsAndCondtion;
    }

    public void setTermsAndCondition(String termsAndCondition) {
        this.termsAndCondtion = termsAndCondition;
    }
}
