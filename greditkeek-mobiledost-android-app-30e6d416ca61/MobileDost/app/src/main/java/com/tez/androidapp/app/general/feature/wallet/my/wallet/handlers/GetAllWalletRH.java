package com.tez.androidapp.app.general.feature.wallet.my.wallet.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.GetAllWalletCallback;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.network.dto.response.GetAllWalletResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by Rehman Murad Ali on 12/14/2017.
 */

public class GetAllWalletRH  extends BaseRH<GetAllWalletResponse> {

    private GetAllWalletCallback getAllWalletCallback;

    public GetAllWalletRH(BaseCloudDataStore baseCloudDataStore, GetAllWalletCallback getAllWalletCallback) {
        super(baseCloudDataStore);
        this.getAllWalletCallback = getAllWalletCallback;
    }

    @Override
    protected void onSuccess(Result<GetAllWalletResponse> value) {
        if (getAllWalletCallback != null)
            getAllWalletCallback.onGetAllWalletSuccess(value.response().body().getWalletList());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (getAllWalletCallback != null) getAllWalletCallback.onGetAllWalletFailure(errorCode,message);
    }
}
