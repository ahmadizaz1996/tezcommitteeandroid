package com.tez.androidapp.commons.utils.file.util;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.widget.Toast;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * Created by FARHAN DHANANI on 7/15/2018.
 */
public abstract class DownloadDocuments extends BroadcastReceiver implements IDocumentDownloader {

    private final Context context;
    private DownloadDocumentsCallBack downloadDocumentsCallBack;

    DownloadDocuments(Context context) {
        this.context = context;
        context.registerReceiver(this, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public void downloadDocuments(DownloadDocumentsCallBack downloadDocumentsCallBack) {
        this.downloadDocumentsCallBack = downloadDocumentsCallBack;
        downloadData(this.context,
                getFileUrl(),
                getFileTitle(),
                getFileDescription(),
                showNotification(),
                getAuthorization());
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
        if (referenceId != -1 && downloadManager != null && queryDownloadStatus(referenceId, downloadManager)) {
            Uri uri = getUri();
            if (uri != null) {
                String mimeType = Utility.getMimeTypeFromLocalFileUri(uri);
                context.sendBroadcast(getIntentForDownloadCompletedBroadCastReceiver(uri.toString()));
                downloadDocumentsCallBack.onSuccess(uri, mimeType);
            }
        }
    }

    private void tokenRefresh() {
        UserCloudDataStore.getInstance().tokenRefresh(
                new BaseRH(new BaseCloudDataStore()) {
                    @Override
                    protected void onSuccess(Result value) {
                        //Left
                    }

                    @Override
                    public void onFailure(int errorCode, @Nullable String message) {
                        downloadDocumentsCallBack.onFailure(errorCode, message);
                    }
                });
    }

    private void downloadData(@NonNull Context context,
                              @NonNull Uri fileUrl,
                              @NonNull String fileTitle,
                              @Nullable String fileDescription,
                              boolean showNotificationOnCompleted,
                              boolean addAuthorizationHeader) {

        DownloadManager downloadManager = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
        if (downloadManager != null) {
            DownloadManager.Request request = new DownloadManager.Request(fileUrl);
            setRequestTitle(request, fileTitle);
            setRequestDescription(request, fileDescription == null ? fileTitle : fileDescription);
            setRequestDestinationDirectoryInDownloads(request, getSubPath());
            if (addAuthorizationHeader) {
                addAuthHeaderToRequest(request);
            }
            if (showNotificationOnCompleted) {
                setRequestNotificationVisibilityOnCompleted(request);
            }
            downloadManager.enqueue(request);
        }
    }

    private void setRequestTitle(DownloadManager.Request request, String fileTitle) {
        request.setTitle(fileTitle);
    }

    private void setRequestDescription(DownloadManager.Request request, String fileDescription) {
        request.setDescription(fileDescription);
    }

    private void setRequestDestinationDirectoryInDownloads(DownloadManager.Request request,
                                                           String subPath) {
        request.setDestinationInExternalPublicDir(getDestinationFileDirectory(), subPath);
    }

    private void setRequestNotificationVisibilityOnCompleted(DownloadManager.Request request) {
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
    }

    private void addAuthHeaderToRequest(DownloadManager.Request request) {
        request.addRequestHeader("Authorization", Utility.getAuthorizationHeaderValue());
    }

    private String getSubPath() {
        return getSignature() == null ? getFileTitle() : getSignature();
    }

    private Uri getUri() {
        return Uri.parse("file://" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                .toString() + "/" + getSubPath());
    }

    private boolean queryDownloadStatus(long referenceId, @NonNull DownloadManager downloadManager) {

        DownloadManager.Query downloadQuery = new DownloadManager.Query();
        //set the query filter to our previously Enqueued download
        downloadQuery.setFilterById(referenceId);

        //Query the download manager about downloads that have been requested.
        Cursor cursor = downloadManager.query(downloadQuery);
        if (cursor != null)
            return cursor.moveToFirst() && downloadStatus(cursor);
        return false;
    }

    private boolean downloadStatus(Cursor cursor) {

        int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));

        int reason = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));

        boolean downloadSuccessful = false;

        switch (status) {
            case DownloadManager.STATUS_FAILED:
                this.displayToast();
                context.unregisterReceiver(this);
                switch (reason) {
                    case DownloadManager.ERROR_CANNOT_RESUME:
                        break;

                    case DownloadManager.ERROR_DEVICE_NOT_FOUND:
                        break;

                    case DownloadManager.ERROR_FILE_ALREADY_EXISTS:
                        break;

                    case DownloadManager.ERROR_FILE_ERROR:
                        break;

                    case DownloadManager.ERROR_HTTP_DATA_ERROR:
                        break;

                    case DownloadManager.ERROR_INSUFFICIENT_SPACE:
                        break;

                    case DownloadManager.ERROR_TOO_MANY_REDIRECTS:
                        break;

                    case DownloadManager.ERROR_UNHANDLED_HTTP_CODE:
                        break;

                    case DownloadManager.ERROR_UNKNOWN:
                        break;

                    case Constants.STATUS_CODE_UNAUTHORIZED:
                        this.tokenRefresh();
                        break;

                    default:
                        break;
                }
                break;
            case DownloadManager.STATUS_PAUSED:
                break;

            case DownloadManager.STATUS_PENDING:
                break;

            case DownloadManager.STATUS_RUNNING:
                break;

            case DownloadManager.STATUS_SUCCESSFUL:
                downloadSuccessful = true;
                context.unregisterReceiver(this);
                break;
            default:
                break;
        }
        return downloadSuccessful;
    }

    private void displayToast() {
        Toast.makeText(this.context, R.string.download_failed, Toast.LENGTH_LONG).show();
    }
}
