package com.tez.androidapp.app.base.ui.activities;

import android.view.View;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezFrameLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezToolbar;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

/**
 * Created by VINOD KUMAR on 4/9/2019.
 */
public abstract class ToolbarActivity extends BaseActivity {

    @BindView(R.id.tezToolbar)
    protected TezToolbar tezToolbar;


    @CallSuper
    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        View childLayout = this.inflateLayout(layoutResID);
        View toolbarView = this.setToolbarView(childLayout);
        super.setContentView(toolbarView);
    }

    protected void updateToolbar(@NonNull TezToolbar tezToolbar) {
        TezLinearLayout layout = findViewById(R.id.baseLayout);
        layout.removeView(this.tezToolbar);
        this.tezToolbar = tezToolbar;
    }

    @NonNull
    protected View getBaseLayout() {
        return this.inflateLayout(R.layout.activity_toolbar_layout);
    }

    private View setToolbarView(@NonNull View childLayoutView) {
        View baseLayout = getBaseLayout();
        TezFrameLayout frameLayout = baseLayout.findViewById(R.id.baseToolbarFrameLayout);
        frameLayout.addView(childLayoutView);
        ViewBinder.bind(this, baseLayout);
        return baseLayout;
    }
}