package com.tez.androidapp.commons.models.network;

import java.io.Serializable;

/**
 * Created by Rehman Murad Ali on 9/9/2017.
 */

public class Dashboard implements Serializable {
    private MobileUserInfo mobileUserInfo;
    private LoanDetails loanDetail;
    private Integer unreadNotificationsCount;
    private Alert alert;

    public Alert getAlert() {
        return alert;
    }

    public LoanDetails getLoanDetails() {
        return loanDetail;
    }

    public MobileUserInfo getMobileUserInfo() {
        return mobileUserInfo;
    }

    public void setMobileUserInfo(MobileUserInfo mobileUserInfo) {
        this.mobileUserInfo = mobileUserInfo;
    }

    public Integer getUnreadNotificationsCount() {
        return unreadNotificationsCount;
    }

    public void setUnreadNotificationsCount(Integer unreadNotificationsCount) {
        this.unreadNotificationsCount = unreadNotificationsCount;
    }

    public void setLoanDetail(LoanDetails loanDetail) {
        this.loanDetail = loanDetail;
    }
}
