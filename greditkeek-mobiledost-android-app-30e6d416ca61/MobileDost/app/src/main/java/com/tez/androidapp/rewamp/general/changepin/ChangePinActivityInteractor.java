package com.tez.androidapp.rewamp.general.changepin;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.ChangePinCallback;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.models.network.dto.request.ChangePinRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public class ChangePinActivityInteractor implements IChangePinActivityInteractor {

    private final IChangePinActivityInteractorOutput iChangePinActivityInteractorOutput;

    public ChangePinActivityInteractor(IChangePinActivityInteractorOutput iChangePinActivityInteractorOutput) {
        this.iChangePinActivityInteractorOutput = iChangePinActivityInteractorOutput;
    }

    @Override
    public void changePin(String newPin, String oldPin) {
        UserAuthCloudDataStore.getInstance().changePin(new ChangePinRequest(newPin, oldPin), new ChangePinCallback() {
            @Override
            public void onChangePinSuccess(BaseResponse baseResponse) {
                iChangePinActivityInteractorOutput.onChangePinSuccess(baseResponse);
            }

            @Override
            public void onChangePinFailure(int errCode, String message) {
                if (Utility.isUnauthorized(errCode))
                    changePin(newPin, oldPin);
                else
                    iChangePinActivityInteractorOutput.onChangePinFailure(errCode, message);
            }
        });
    }
}
