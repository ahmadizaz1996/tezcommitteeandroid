package com.tez.androidapp.commons.pref;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import net.tez.storage.library.converters.StringToObjectParser;

import java.lang.reflect.Type;

/**
 * Created by FARHAN DHANANI on 1/18/2019.
 */
public class GenericObjectParser implements StringToObjectParser {
    @Override
    public <T> T convertToObject(@NonNull String data, @NonNull Class<T> clazz) {
        Gson gson = new Gson();
        return gson.fromJson(data, clazz);
    }

    @Override
    public <T> T convertListToObject(@NonNull String data, @NonNull Type type) {
        return new Gson().fromJson(data, type);
    }
}
