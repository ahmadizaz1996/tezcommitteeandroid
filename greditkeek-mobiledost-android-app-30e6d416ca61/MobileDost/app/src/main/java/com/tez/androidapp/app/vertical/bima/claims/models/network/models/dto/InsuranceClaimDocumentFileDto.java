package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by VINOD KUMAR on 3/5/2019.
 */
public class InsuranceClaimDocumentFileDto implements Serializable {

    //For sending to server
    private Integer id;
    private String filePath;
    private String mimeType;

    public InsuranceClaimDocumentFileDto() {
    }


    private InsuranceClaimDocumentFileDto(Integer id, String filePath, String mimeType) {
        this.id = id;
        this.filePath = filePath;
        this.mimeType = mimeType;
    }

    public static List<InsuranceClaimDocumentFileDto> parse(@NonNull List<DocumentFile> documentFiles) {
        List<InsuranceClaimDocumentFileDto> fileDtoList = new ArrayList<>();
        for (DocumentFile documentFile : documentFiles)
            fileDtoList.add(new InsuranceClaimDocumentFileDto(documentFile.getDocFileId(), documentFile.getDocFilePath(), documentFile.getDocMimeType()));
        return fileDtoList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
