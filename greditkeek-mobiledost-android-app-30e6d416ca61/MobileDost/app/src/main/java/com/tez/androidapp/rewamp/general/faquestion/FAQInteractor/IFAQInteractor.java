package com.tez.androidapp.rewamp.general.faquestion.FAQInteractor;

public interface IFAQInteractor {
    void callForFAQs(String selectedLanguage);
}
