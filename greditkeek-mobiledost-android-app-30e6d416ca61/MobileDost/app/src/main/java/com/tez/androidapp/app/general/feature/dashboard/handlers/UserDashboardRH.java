package com.tez.androidapp.app.general.feature.dashboard.handlers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.dashboard.callbacks.UserDashboardCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.commons.models.network.MobileUserInfo;
import com.tez.androidapp.commons.models.network.dto.response.UserDashboardResponse;
import com.tez.androidapp.commons.utils.app.Constants;

import net.tez.fragment.util.actions.Actionable1;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 9/9/2017.
 */

public class UserDashboardRH extends BaseRH<UserDashboardResponse> {

    private UserDashboardCallback userDashboardCallback;

    public UserDashboardRH(BaseCloudDataStore baseCloudDataStore, UserDashboardCallback userDashboardCallback) {
        super(baseCloudDataStore);
        this.userDashboardCallback = userDashboardCallback;
    }

    @Override
    protected void onSuccess(Result<UserDashboardResponse> value) {



    }


    private void setMobileUserInfo(boolean isContainMobileUserInfo, MobileUserInfo mobileUserInfo) {


    }

    private void setLoanDetails(boolean shouldSaveDetails, LoanDetails loanDetails) {

        if (shouldSaveDetails) {



        }
    }


    private void setUserProfileIsValid(boolean valid) {


    }


    private void setUnreadNotificationCount(boolean shouldSaveCount, int notificationCount) {

    }

    private void setMobileUserInfo(String userStatus,
                                   boolean mobileAccountAdded,
                                   boolean cnicVerified) {





    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {

        message = message == null ? Constants.ERROR_MESSAGE : message;

        if (userDashboardCallback != null)
            userDashboardCallback.onUserDashboardFailure(new BaseResponse(errorCode, message));
    }
}
