package com.tez.androidapp.rewamp.committee.response;

import com.tez.androidapp.app.base.response.BaseResponse;

import java.io.Serializable;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class PaymentInitiateResponse extends BaseResponse implements Serializable {

//    public int statusCode;
    public Committee committee;


//    @Override
//    public int getStatusCode() {
//        return statusCode;
//    }
//
//    @Override
//    public void setStatusCode(int statusCode) {
//        this.statusCode = statusCode;
//    }

    public Committee getCommittee() {
        return committee;
    }

    public void setCommittee(Committee committee) {
        this.committee = committee;
    }

    public static class Committee{
        public int committeeId;
        public boolean resumeAsyncResponse;


        public int getCommitteeId() {
            return committeeId;
        }

        public void setCommitteeId(int committeeId) {
            this.committeeId = committeeId;
        }

        public boolean isResumeAsyncResponse() {
            return resumeAsyncResponse;
        }

        public void setResumeAsyncResponse(boolean resumeAsyncResponse) {
            this.resumeAsyncResponse = resumeAsyncResponse;
        }
    }
}



