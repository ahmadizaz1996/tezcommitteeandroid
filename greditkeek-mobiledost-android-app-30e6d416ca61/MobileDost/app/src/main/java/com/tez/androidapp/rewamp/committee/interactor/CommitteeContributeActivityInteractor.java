package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeContributeListener;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeContributeActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeWalletResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeContributeActivityInteractor implements ICommitteeContributeInteractor {


    private final ICommitteeContributeActivityInteractorOutput iCommitteeContributeActivityInteractorOutput;

    public CommitteeContributeActivityInteractor(ICommitteeContributeActivityInteractorOutput iCommitteeContributeActivityInteractorOutput) {
        this.iCommitteeContributeActivityInteractorOutput = iCommitteeContributeActivityInteractorOutput;
    }

    @Override
    public void getWallets() {
        CommitteeAuthCloudDataStore.getInstance().getWallets(new CommitteeContributeListener() {
            @Override
            public void onGetWalletSuccess(CommitteeWalletResponse committeeWalletResponse) {
                iCommitteeContributeActivityInteractorOutput.onGetWalletSuccess(committeeWalletResponse);
            }

            @Override
            public void onGetWalletFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getWallets();
                else
                    iCommitteeContributeActivityInteractorOutput.onGetWalletFailure(errorCode, message);
            }
        });
    }
}
