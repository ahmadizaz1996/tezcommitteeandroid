package com.tez.androidapp.rewamp.general.profile.interactor;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.ProfileBasicCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.QuestionOptionsCallBack;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.BasicInformation;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.QuestionOptionsRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserBasicProfileRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.LoanQuestionSessionDataStore;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.general.profile.presenter.IBasicProfileactivityInteractorOutput;

import java.util.List;

public class BasicProfileActivityInteractor implements IBasicProfileActivityInteractor {
    private final IBasicProfileactivityInteractorOutput iBasicProfileactivityInteractorOutput;

    public BasicProfileActivityInteractor(IBasicProfileactivityInteractorOutput iBasicProfileactivityInteractorOutput) {
        this.iBasicProfileactivityInteractorOutput = iBasicProfileactivityInteractorOutput;
    }

    public void getUserBasicProfile(List<Option> options){
        UserAuthCloudDataStore.getInstance().getUserBasicProfile(new ProfileBasicCallback() {
            @Override
            public void onProfileBasicSuccess(BasicInformation basicInformation) {
                iBasicProfileactivityInteractorOutput.onQuestionOptionsSuccess(options, basicInformation);
            }

            @Override
            public void onProfileBasicFailure(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode), ()-> getUserBasicProfile(options),()->iBasicProfileactivityInteractorOutput.onQuestionOptionsFailure(errorCode, message));
            }
        });
    }

    @Override
    public void updateUserBasicProfile(UpdateUserBasicProfileRequest updateUserProfileRequest){
        UserAuthCloudDataStore.getInstance().updateUserBasicProfile(updateUserProfileRequest, new UpdateUserProfileCallback() {
            @Override
            public void onUpdateUserProfileSuccess() {
                iBasicProfileactivityInteractorOutput.onUpdateUserProfileSuccess();
            }

            @Override
            public void onUpdateUserProfileFailure(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        ()-> updateUserBasicProfile(updateUserProfileRequest),
                        ()-> iBasicProfileactivityInteractorOutput.onUpdateUserProfileFailure(errorCode, message));
            }
        });
    }

    @Override
    public void getListOfOccupation(String lang){
        LoanQuestionSessionDataStore.getInstance().getQuestionOptions(lang,
                QuestionOptionsRequest.Params.OCCUPATION, new QuestionOptionsCallBack() {
                    @Override
                    public void onQuestionOptionsSuccess(List<Option> data) {
                        getUserBasicProfile(data);
                    }

                    @Override
                    public void onQuestionOptionsFailure(int errorCode, String message) {
                        Optional.doWhen(Utility.isUnauthorized(errorCode),
                                ()->getListOfOccupation(lang),
                                ()->iBasicProfileactivityInteractorOutput.onQuestionOptionsFailure(errorCode, message));
                    }
                });
    }
}
