package com.tez.androidapp.rewamp.committee.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.RippleEffectImageView;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezFrameLayout;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.adapter.MyCommitteeInviteesAdapter;
import com.tez.androidapp.rewamp.committee.adapter.MyCommitteeMembersAdapter;
import com.tez.androidapp.rewamp.committee.presenter.MyCommitteeActivityPresenter;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeContributeActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeGroupChatActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeSummaryActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteeUpdateActivityRouter;
import com.tez.androidapp.rewamp.committee.router.MyCommitteeMembersActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MyCommitteeActivity extends BaseActivity implements IBaseView, IMyCommitteeActivityView, MyCommitteeMembersAdapter.MyCommitteeMemberListener, MyCommitteeInviteesAdapter.MyCommitteeInviteeListener, DoubleTapSafeOnClickListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.membersRecyclerView)
    RecyclerView rvMembers;

    @BindView(R.id.inviteesRecyclerView)
    RecyclerView inviteesRecyclerView;

    @BindView(R.id.tvViewAllUpdates)
    TezTextView tvViewAllUpdates;

    @BindView(R.id.tvViewAllMember)
    TezTextView tvViewAllMember;

    @BindView(R.id.btnGroupChat)
    TezButton btnGroupChat;

    @BindView(R.id.tvAmount)
    TezTextView tvAmount;

    @BindView(R.id.tvDuration)
    TezTextView tvDuration;

    @BindView(R.id.tvBiMonthlyInstallment)
    TezTextView tvBiMonthlyInstallment;

    @BindView(R.id.tvBiMonthlyInstallmentText)
    TezTextView tvBiMonthlyInstallmentText;

    @BindView(R.id.membersShimmerLayout)
    TezLinearLayout membersShimmerLayout;

    @BindView(R.id.flCommitteeDetails)
    TezFrameLayout committeeDetailsFramelayout;

    @BindView(R.id.tvInstallmentDueDate)
    TezTextView tvInstallmentDueDate;

    @BindView(R.id.membersRecyclerView)
    RecyclerView membersRecyclerView;

    @BindView(R.id.btnContributeNow)
    TezButton btnContributeNow;

    @BindView(R.id.tvWelcomeName)
    TezTextView tvWelcomeName;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.ivInfo)
    RippleEffectImageView infoImgView;

    @BindView(R.id.ivHamburger)
    RippleEffectImageView ivHamburger;


    private final MyCommitteeActivityPresenter mMyCommitteeActivityPresenter;
    private ArrayList<MyCommitteeResponse.Committee.Invite> inviteesList = new ArrayList<>();
    private List<MyCommitteeResponse.Committee.Member> memberList = new ArrayList<>();
    private MyCommitteeResponse committeeResponse;


    public MyCommitteeActivity() {
        mMyCommitteeActivityPresenter = new MyCommitteeActivityPresenter(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee_dashboard);
        ViewBinder.bind(this);
        init();
//        setRecyclerViews();
    }

    private void init() {
        btnGroupChat.setDoubleTapSafeOnClickListener(this);
        btnContributeNow.setDoubleTapSafeOnClickListener(this);
        tvViewAllMember.setDoubleTapSafeOnClickListener(this);
        tvViewAllUpdates.setDoubleTapSafeOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);
        infoImgView.setDoubleTapSafeOnClickListener(this);
        ivHamburger.setDoubleTapSafeOnClickListener(this);
        mMyCommitteeActivityPresenter.getMyCommittee();
    }

    @Override
    protected String getScreenName() {
        return MyCommitteeActivity.class.getSimpleName();
    }

    @Override
    public void showLoader() {
        showTezLoader();
    }

    @Override
    public void hideLoader() {
        dismissTezLoader();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onMyCommittee(MyCommitteeResponse myCommitteeResponse) {
        if (myCommitteeResponse != null && myCommitteeResponse.getCommitteeList().size() > 0) {
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
            this.committeeResponse = myCommitteeResponse;
            MyCommitteeResponse.Committee committee = myCommitteeResponse.getCommitteeList().get(0);
            tvAmount.setText("Rs." + committee.getAmount());
            tvDuration.setText(committee.getTenor() + " months");
            if (committee.getFrequency() == 1) {
                tvBiMonthlyInstallmentText.setText(R.string.monthly_installment);
            } else {
                tvBiMonthlyInstallmentText.setText(R.string.bi_monthly_installment);
            }
            tvBiMonthlyInstallment.setText(committee.getInstallment() + "");
            tvInstallmentDueDate.setText(committee.getDueDate().split("T")[0]);
            setMembersAdapter(committee.getMembers());
            setInviteesAdapter(committee.getInvites());
        } else {
            showError(1001, getString(R.string.you_do_not_have_any_committee),
                    (dialog, which) -> finishActivity());
        }
    }

    private void setInviteesAdapter(List<MyCommitteeResponse.Committee.Invite> invites) {
        MyCommitteeResponse.Committee.Invite invite = new MyCommitteeResponse.Committee.Invite();
        invite.setMobilNumber("Committee Created");
        invites.add(0, invite);
        MyCommitteeInviteesAdapter myCommitteeInviteesAdapter = new MyCommitteeInviteesAdapter(invites, committeeResponse.committeeList.get(0).getStartDate(), this);
        this.inviteesList = new ArrayList<>(invites);
        inviteesRecyclerView.setAdapter(myCommitteeInviteesAdapter);
    }

    private void setMembersAdapter(List<MyCommitteeResponse.Committee.Member> members) {
        MyCommitteeMembersAdapter myCommitteeMembersAdapter = new MyCommitteeMembersAdapter(members, this);
        this.memberList = members;
        rvMembers.setAdapter(myCommitteeMembersAdapter);
    }

    @Override
    public void onClickMembers(@NonNull MyCommitteeResponse.Committee.Member member) {

    }

    @Override
    public void onClickInvitee(@NonNull MyCommitteeResponse.Committee.Invite invite) {

    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        try {

            switch (view.getId()) {
                case R.id.tvViewAllMember:
                    MyCommitteeMembersActivityRouter.createInstance().setDependenciesAndRoute(this, memberList);
//                    Toast.makeText(this, "Will be implemented", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.tvViewAllUpdates:
                    CommitteeUpdateActivityRouter.createInstance().setDependenciesAndRoute(this, this.inviteesList);
                    break;
                case R.id.btnGroupChat:
//                    Toast.makeText(this, "Coming Soon", Toast.LENGTH_LONG).show();
                    CommitteeGroupChatActivityRouter.createInstance().setDependenciesAndRoute(this,committeeResponse);
                    break;
                case R.id.btnContributeNow:
                    CommitteeContributeActivityRouter.createInstance().setDependenciesAndRoute(this, committeeResponse);
                    break;
                case R.id.ivInfo:
                    CommitteeSummaryActivityRouter.createInstance().setDependenciesAndRoute(this, committeeResponse);
                    break;
                case R.id.ivHamburger:
                    finishActivity();
                    break;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        mMyCommitteeActivityPresenter.getMyCommittee();
    }
}