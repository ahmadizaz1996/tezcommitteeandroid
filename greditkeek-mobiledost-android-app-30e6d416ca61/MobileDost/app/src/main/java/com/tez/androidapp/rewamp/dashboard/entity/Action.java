package com.tez.androidapp.rewamp.dashboard.entity;

import java.io.Serializable;

public class Action implements Serializable {

    private String actionType;
    private String actionSubType;
    private String value;

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionSubType() {
        return actionSubType;
    }

    public void setActionSubType(String actionSubType) {
        this.actionSubType = actionSubType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isSafe() {
        return getActionType() != null || getActionSubType() != null || getValue() != null;
    }
}
