package com.tez.androidapp.rewamp.committee.view;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezImageButton;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.response.CommitteeMetaDataResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeCreationActivityRouter;
import com.tez.androidapp.rewamp.committee.router.CommitteePackagesActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class CommitteeCreationActivity extends BaseActivity implements DoubleTapSafeOnClickListener {

//    @BindView(R.id.tvWalletName)
//    private TezTextView tvWalletName;
//
//    @BindView(R.id.tvWalletNo)
//    private TezTextView tvWalletNo;
//
//    @BindView(R.id.tvAmountValue)
//    private TezTextView tvAmountValue;
//
//    @BindView(R.id.tvTransactionIdValue)
//    private TezTextView tvTransactionIdValue;
//
//    @BindView(R.id.tvDateValue)
//    private TezTextView tvDateValue;
//
//    @BindView(R.id.tvAmountRemainingValue)
//    private TezTextView tvAmountRemainingValue;
//
//    @BindView(R.id.tvDueDateValue)
//    private TezTextView tvDueDateValue;
//
//    @BindView(R.id.tvRepayNote)
//    private TezTextView tvRepayNote;
//
//    @BindView(R.id.groupRemainingAmount)
//    private Group groupRemainingAmount;
//
//    @BindView(R.id.btDone)
//    private TezButton btDone;


    @BindView(R.id.btnDecreament)
    TezImageButton btnAmountDecrement;

    @BindView(R.id.btnIncreament)
    TezImageButton btnAmountIncrement;

    @BindView(R.id.tvAmount)
    TezEditTextView tvAmount;

    @BindView(R.id.tvMembers)
    TezTextView tvMembers;

    @BindView(R.id.maxamount)
    TezTextView maxAmountInfo;

    @BindView(R.id.maxMembersInfo)
    TezTextView maxMembersInfo;

    @BindView(R.id.btnDecreament1)
    TezImageButton btnMemberDecrement;

    @BindView(R.id.btnIncreament1)
    TezImageButton btnMemberIncrement;

    @BindView(R.id.btContinue)
    TezButton btnContinue;

    Integer minAmount;
    Integer minMembers;
    Integer maxAmount;
    Integer maxMembers;
    Integer amount;
    Integer members;
    String amountToShow;
    CommitteeMetaDataResponse committeeMetadata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee_create_f);
        ViewBinder.bind(this);
        initClickListeners();
        fetchExtras();
        setmaxmembers();
        tvAmount.addTextChangedListener(textWatcher);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.toString().equals(""))
                Utility.setErrorOnTezViews(tvAmount, "Required field");
            else {
                amount = Integer.parseInt(s.toString());
                int amount = Integer.parseInt(s.toString());
                setmaxmembers();
                if (amount < minAmount) {
                    Utility.setErrorOnTezViews(tvAmount, "At least " + minAmount + " required");
                } else if (amount > maxAmount) {
                    Utility.setErrorOnTezViews(tvAmount, "Amount cannot exceed " + maxAmount);
                } else if (amount % 500 != 0) {
                    Utility.setErrorOnTezViews(tvAmount, "Amount must be multiple of 500");
                }
            }

        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };


    void setmaxmembers() {
        members = minMembers;
        tvMembers.setText(members + "");
        int value = amount / 500;
        if (value >= 6)
            maxMembers = 6;
        else if (value >= 5)
            maxMembers = 5;
        else if (value >= 4)
            maxMembers = 4;
        else maxMembers = 3;
    }

    private void fetchExtras() {
        if (getIntent() != null
                && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(CommitteeCreationActivityRouter.COMMITTEE_METADATA)) {

            committeeMetadata = (CommitteeMetaDataResponse) getIntent().getSerializableExtra(CommitteeCreationActivityRouter.COMMITTEE_METADATA);
            if (committeeMetadata != null) {
                System.out.println("committeemetadata " + committeeMetadata.toString());
                minAmount = committeeMetadata.getMinAmount();
                maxAmount = committeeMetadata.getMaxAmount();
                minMembers = committeeMetadata.getMinMembers();
                maxMembers = committeeMetadata.getMaxMembers();
                amount = minAmount;
                amountToShow = "RS " + minAmount;
                tvAmount.setText(amount.toString());
                members = minMembers;
                tvMembers.setText(members + "");
                maxAmountInfo.setText("Maximum Amount is " + maxAmount);
                maxMembersInfo.setText("Maximum number of members: " + maxMembers + " (Including yourself)");
                btnContinue.setButtonNormal();
            } /*else {
                amount = 1500;
                minAmount = 1500;
                maxAmount = 25000;
                minMembers = 3;
                maxMembers = 6;
                tvAmount.setText(amount.toString());
                members = minMembers;
                tvMembers.setText(members + "");
                maxAmountInfo.setText("Maximum Amount is " + maxAmount);
                maxMembersInfo.setText("Maximum number of members: " + maxMembers + " (Including yourself)");
                btnContinue.setButtonNormal();
            }*/
        }

    }

    private void initClickListeners() {
        btnAmountDecrement.setDoubleTapSafeOnClickListener(this);
        btnAmountIncrement.setDoubleTapSafeOnClickListener(this);
        btnMemberDecrement.setDoubleTapSafeOnClickListener(this);
        btnMemberIncrement.setDoubleTapSafeOnClickListener(this);
        btnContinue.setDoubleTapSafeOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected String getScreenName() {
        return "CommitteeCreationActivity";
    }


    private void onAmountDecrement() {
        setmaxmembers();
        if ((amount > minAmount)) {
            amount -= 500;
            amountToShow = "Rs " + amount;
            tvAmount.setText(amount.toString());
        } else
            Toast.makeText(this, "Minimum amount is" + minAmount + " required", Toast.LENGTH_SHORT).show();
    }

    private void onAmountIncrement() {
        setmaxmembers();
        amount = Integer.parseInt(tvAmount.getText().toString());
        if ((amount < maxAmount)) {
            amount += 500;
            amountToShow = "Rs " + amount;
            tvAmount.setText(amount.toString());
        } else Toast.makeText(this, "Maximum amount is " + maxAmount, Toast.LENGTH_SHORT).show();

    }

    private void onMemberDecrement() {
        if ((members > minMembers)) {
            members -= 1;
            tvMembers.setText(members + "");
        } else Toast.makeText(this, "Minimum member is " + minMembers, Toast.LENGTH_SHORT).show();
    }

    private void onMemberIncrement() {
        System.out.println("maxmembers " + maxMembers);
        if ((members < maxMembers)) {
            members += 1;
            tvMembers.setText(members + "");
        } else {
            Toast.makeText(this, "Maximum member is " + maxMembers, Toast.LENGTH_SHORT).show();
        }
    }

    private void onContinueButtonClick() {
        amount = Integer.parseInt(tvAmount.getText().toString());
        if (amount % 500 != 0) {
            Toast.makeText(this, "Amount must be multiple of 500", Toast.LENGTH_LONG).show();
            return;
        }
        if (amount / members >= 500)
            CommitteePackagesActivityRouter.createInstance().setDependenciesAndRoute(this, amount, members);
        else
            Toast.makeText(this, "Installment amount must be greater than 500", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.btnDecreament:
                    onAmountDecrement();
                    break;
                case R.id.btnIncreament:
                    onAmountIncrement();
                    break;
                case R.id.btnDecreament1:
                    onMemberDecrement();
                    break;
                case R.id.btnIncreament1:
                    onMemberIncrement();
                    break;
                case R.id.btContinue:
                    onContinueButtonClick();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
