package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponseMemberList;
import com.tez.androidapp.rewamp.committee.response.CommitteeResponseTimeline;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeFilterActivity;
import com.tez.androidapp.rewamp.committee.view.MyCommitteeUpdatesActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommitteeFilterActivityRouter extends BaseActivityRouter {


    public static final String INVITEES_LIST = "INVITEES_LIST";
    public static final String MEMBERS_LIST = "MEMBERS_LIST";
    public static final String HASH_MAP = "HASH_MAP";

    public static CommitteeFilterActivityRouter createInstance() {
        return new CommitteeFilterActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, List<CommitteeFilterResponseMemberList> committeeFilterResponseMemberLists) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(MEMBERS_LIST, new ArrayList<>(committeeFilterResponseMemberLists));
        intent.putExtras(bundle);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeFilterActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    /*public void setDependenciesAndRoute(BaseActivity from, ArrayList<MyCommitteeResponse.Committee.Invite> inviteesList) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(INVITEES_LIST, inviteesList);
        intent.putExtras(bundle);
        route(from, intent);
    }*/

    public void setDependenciesAndRoute(BaseActivity from, ArrayList<CommitteeResponseTimeline> finalList, HashMap<String, List<CommitteeResponseTimeline>> hashMap) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putSerializable(MEMBERS_LIST, finalList);
        bundle.putSerializable(HASH_MAP, hashMap);
        intent.putExtras(bundle);
        route(from, intent);
    }
}
