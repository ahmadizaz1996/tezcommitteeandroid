package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.committee.interactor.MyCommitteeActivityInteractor;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.IMyCommitteeActivityView;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class MyCommitteeActivityPresenter implements IMyCommitteeActivityPresenter, IMyCommitteeActivityInteractorOutput {

    private final IMyCommitteeActivityView mIMyCommitteeActivityView;
    private final MyCommitteeActivityInteractor mMyCommitteeActivityInteractor;

    public MyCommitteeActivityPresenter(IMyCommitteeActivityView mIMyCommitteeActivityView) {
        this.mIMyCommitteeActivityView = mIMyCommitteeActivityView;
        mMyCommitteeActivityInteractor = new MyCommitteeActivityInteractor(this);
    }

    /*@Override
    public void getInvite(List<CommitteeInviteRequest> committeeInviteRequest) {
        iCommitteeInviteActivityView.showLoader();
        committeeInviteActivityInteractor.getInvite(committeeInviteRequest);
    }

    @Override
    public void onInviteSuccess() {
        iCommitteeInviteActivityView.hideLoader();
        iCommitteeInviteActivityView.onInvite();
    }

    @Override
    public void onInviteFailure(int errorCode, String message) {
        iCommitteeInviteActivityView.hideLoader();
        iCommitteeInviteActivityView.showError(errorCode,
                (dialog, which) -> iCommitteeInviteActivityView.finishActivity());
    }*/

    @Override
    public void onMyCommitteeSuccess(MyCommitteeResponse myCommitteeResponse) {
        mIMyCommitteeActivityView.hideLoader();
        mIMyCommitteeActivityView.onMyCommittee(myCommitteeResponse);
    }

    @Override
    public void onMyCommitteeFailure(int errorCode, String message) {
        mIMyCommitteeActivityView.showError(errorCode,
                (dialog, which) -> mIMyCommitteeActivityView.finishActivity());
    }

    @Override
    public void getMyCommittee() {
        mIMyCommitteeActivityView.showLoader();
        mMyCommitteeActivityInteractor.getMyCommittee();
    }
}
