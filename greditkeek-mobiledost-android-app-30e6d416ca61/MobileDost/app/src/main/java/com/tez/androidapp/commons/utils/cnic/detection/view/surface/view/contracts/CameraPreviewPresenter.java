package com.tez.androidapp.commons.utils.cnic.detection.view.surface.view.contracts;

import android.hardware.Camera;

import com.tez.androidapp.commons.utils.cnic.detection.util.timer.callback.TimeOutCallback;

public interface CameraPreviewPresenter extends TimeOutCallback {

    void sendImageToFirebase(byte[] bytes, Camera camera, boolean isLive, boolean isFace);

    void processByteArray(Camera camera, byte[] bytes);
}
