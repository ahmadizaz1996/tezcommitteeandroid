package com.tez.androidapp.app.vertical.bima.policies.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by VINOD KUMAR on 12/24/2018.
 */
public class GetManageBeneficiaryPolicyRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/insurance/policy/beneficiary/manage";
}
