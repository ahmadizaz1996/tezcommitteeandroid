package com.tez.androidapp.rewamp.general.termsandcond;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezDialog;
import com.tez.androidapp.commons.widgets.TezToolbar;
import com.tez.androidapp.rewamp.AppOpener;
import com.tez.androidapp.rewamp.signup.router.ContactUsActivityRouter;

import net.tez.fragment.util.optional.Optional;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.io.IOException;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public class DetailedTermConditionDialog extends TezDialog {

    private String fileName;
    private String terms;

    @BindView(R.id.webView)
    private WebView webView;

    @BindView(R.id.toolbar)
    private TezToolbar toolbar;

    public DetailedTermConditionDialog(@NonNull Context context) {
        super(context, R.style.NavigationTermsDialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_detailed_term_condition);
        ViewBinder.bind(this);
        configureWebViewClient();
        initTermDetailFromFile();
        initTermDetailFromString();
        initToolbarListener();
    }

    private void configureWebViewClient() {
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return proceedUrl(url);
            }


            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return proceedUrl(request.getUrl().toString());
            }

            private boolean proceedUrl(@NonNull String url) {
                if (url.contains("mailto:")) {
                    AppOpener.openEmail(getBaseActivity(), url.replace("mailto:", ""));
                    return true;
                } else if (url.contains("http:")) {
                    AppOpener.openUrl(getBaseActivity(), url);
                    return true;
                }
                return false;
            }
        });
    }

    private BaseActivity getBaseActivity() {
        ContextThemeWrapper contextThemeWrapper = (ContextThemeWrapper) getContext();
        return (BaseActivity) contextThemeWrapper.getBaseContext();
    }

    private void initTermDetailFromFile() {
        if (fileName == null)
            return;
        try {
            String htmlTerms = Utility.readFromAssets(getContext(), fileName);
            loadHtml(htmlTerms);
        } catch (IOException e) {
            Crashlytics.logException(e);
        }
    }

    private void initTermDetailFromString() {
        Optional.ifPresent(terms, this::loadHtml);
    }

    private void loadHtml(String html) {
        webView.loadDataWithBaseURL(null, html, "text/html", null, null);
    }

    private void initToolbarListener() {
        toolbar.setOnBackPressListener(view -> onBackPressed());
        toolbar.setOnCustomerCareListener(view -> ContactUsActivityRouter.createInstance().setDependenciesAndRoute(getContext()));
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    @Override
    public void onBackPressed() {
        cancel();
    }
}
