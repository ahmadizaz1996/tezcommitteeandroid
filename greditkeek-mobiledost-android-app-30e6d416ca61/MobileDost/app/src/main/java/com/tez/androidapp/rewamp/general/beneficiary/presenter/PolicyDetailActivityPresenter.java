package com.tez.androidapp.rewamp.general.beneficiary.presenter;

import android.view.View;

import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.BeneficiaryDetails;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.MobileUserPolicyDetailsDto;
import com.tez.androidapp.rewamp.general.beneficiary.interactor.IPolicyDetailActivityInteractor;
import com.tez.androidapp.rewamp.general.beneficiary.interactor.PolicyDetailActivityInteractor;
import com.tez.androidapp.rewamp.general.beneficiary.view.IPolicyDetailActivityView;

import java.util.List;

public class PolicyDetailActivityPresenter implements IPolicyDetailActivityPresenter, IPolicyDetailActivityInteractorOutput {

    private final IPolicyDetailActivityView iPolicyDetailActivityView;
    private final IPolicyDetailActivityInteractor iPolicyDetailActivityInteractor;

    public PolicyDetailActivityPresenter(IPolicyDetailActivityView iPolicyDetailActivityView) {
        this.iPolicyDetailActivityView = iPolicyDetailActivityView;
        iPolicyDetailActivityInteractor = new PolicyDetailActivityInteractor(this);
    }

    @Override
    public void getPolicyDetails(int policyId) {
        iPolicyDetailActivityView.setClContentVisibility(View.GONE);
        iPolicyDetailActivityView.showTezLoader();
        iPolicyDetailActivityInteractor.getPolicyDetails(policyId);
    }

    @Override
    public void onGetActiveInsurancePolicyDetailsSuccess(MobileUserPolicyDetailsDto mobileUserPolicyDetailsDto) {
        iPolicyDetailActivityView.dismissTezLoader();
        iPolicyDetailActivityView.setBtEditBeneficiaryVisibility(mobileUserPolicyDetailsDto.getBeneficiaryEditable() ? View.VISIBLE : View.GONE);
        iPolicyDetailActivityView.setBtTermsAndConditionsClickListener(mobileUserPolicyDetailsDto.getTermsConditions());
        List<BeneficiaryDetails> beneficiaryDetailsList = mobileUserPolicyDetailsDto.getBeneficiaryDtoList();
        if (beneficiaryDetailsList != null && !beneficiaryDetailsList.isEmpty()) {
            iPolicyDetailActivityView.initBeneficiaryDetails(beneficiaryDetailsList.get(0));
            iPolicyDetailActivityView.setClContentVisibility(View.VISIBLE);
        } else
            iPolicyDetailActivityView.finishActivity();
    }

    @Override
    public void onGetActiveInsurancePolicyDetailsFailure(int errorCode, String message) {
        iPolicyDetailActivityView.dismissTezLoader();
        iPolicyDetailActivityView.showError(errorCode,
                (dialog, which) -> iPolicyDetailActivityView.finishActivity());
    }
}
