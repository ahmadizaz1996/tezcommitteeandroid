package com.tez.androidapp.rewamp.advance.repay.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Group;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.repay.router.RepaymentSuccessfulActivityRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class RepaymentSuccessfulActivity extends BaseActivity {

    @BindView(R.id.tvWalletName)
    private TezTextView tvWalletName;

    @BindView(R.id.tvWalletNo)
    private TezTextView tvWalletNo;

    @BindView(R.id.tvAmountValue)
    private TezTextView tvAmountValue;

    @BindView(R.id.tvTransactionIdValue)
    private TezTextView tvTransactionIdValue;

    @BindView(R.id.tvDateValue)
    private TezTextView tvDateValue;

    @BindView(R.id.tvAmountRemainingValue)
    private TezTextView tvAmountRemainingValue;

    @BindView(R.id.tvDueDateValue)
    private TezTextView tvDueDateValue;

    @BindView(R.id.tvRepayNote)
    private TezTextView tvRepayNote;

    @BindView(R.id.groupRemainingAmount)
    private Group groupRemainingAmount;

    @BindView(R.id.btDone)
    private TezButton btDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repayment_successful);
        ViewBinder.bind(this);
        LoanDetails loanDetails = getIntent().getParcelableExtra(RepaymentSuccessfulActivityRouter.LOAN_DETAILS);
        Optional.ifPresent(loanDetails, this::init, this::finish);
    }

    private void init(@NonNull LoanDetails loanDetails) {

        tvWalletName.setText(Utility.getWalletName(loanDetails.getWallet().getServiceProviderId()));
        tvWalletNo.setText(loanDetails.getWallet().getMobileAccountNumberFormatted());
        tvAmountValue.setText(getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabel(loanDetails.getRepayed())));
        tvTransactionIdValue.setText(loanDetails.getTransactionId());
        tvDateValue.setText(loanDetails.getReturnDateDDMMMM());

        if (loanDetails.getAmountToReturn() > 0) {
            tvAmountRemainingValue.setText(getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabel(loanDetails.getAmountToReturn())));
            tvDueDateValue.setText(loanDetails.getDueDateDDMMMM());
        } else {
            groupRemainingAmount.setVisibility(View.GONE);
            tvRepayNote.setVisibility(View.GONE);
            MDPreferenceManager.setDisbursementReceiptViewed(false);
        }

        btDone.setDoubleTapSafeOnClickListener(view -> {
            DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
            finish();
        });
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected String getScreenName() {
        return "RepaymentSuccessfulActivity";
    }
}
