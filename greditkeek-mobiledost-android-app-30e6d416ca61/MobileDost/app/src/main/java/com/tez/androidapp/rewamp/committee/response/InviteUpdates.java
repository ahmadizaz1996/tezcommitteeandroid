package com.tez.androidapp.rewamp.committee.response;

public class InviteUpdates extends MyCommitteeResponse.Committee.Invite {

    public boolean isDate = false;


    public boolean isDate() {
        return isDate;
    }

    public void setDate(boolean date) {
        isDate = date;
    }
}
