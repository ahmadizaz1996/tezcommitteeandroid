package com.tez.androidapp.rewamp.advance.limit.presenter;

public interface ICheckYourLimitActivityPresenter {

    void applyLoanLimit();
}
