package com.tez.androidapp.app.general.feature.account.feature.pin.code.handlers;


import androidx.annotation.Nullable;

import com.google.firebase.iid.FirebaseInstanceId;
import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserLoginCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PermissionCheckUtil;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

import java.util.Objects;

/**
 * Created  on 12/15/2016.
 */

public class UserLoginRH extends BaseRH<UserLoginResponse> {

    private UserLoginCallback userLoginCallback;

    public UserLoginRH(BaseCloudDataStore baseCloudDataStore, @Nullable UserLoginCallback userLoginCallback) {
        super(baseCloudDataStore);
        this.userLoginCallback = userLoginCallback;
    }

    @Override
    protected void onSuccess(Result<UserLoginResponse> value) {
        final UserLoginResponse loginResponse = value.response().body();
        Optional.doWhen(loginResponse != null
                        && loginResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR,
                () -> {
                    MDPreferenceManager.setAccessToken(Objects.requireNonNull(loginResponse).getToken());
                    MDPreferenceManager.setPrincipalName(loginResponse.getPrincipalName());
                    MDPreferenceManager.setUser(loginResponse.getUserDetails());
                    MDPreferenceManager.setUserSignedUp(true);
                    MDPreferenceManager.setReferralCode(loginResponse.getReferralCode());
                    FirebaseInstanceId.getInstance().getInstanceId();
                    Optional.ifPresent(userLoginCallback, callback -> {
                        callback.onUserLoginSuccess(loginResponse);
                    });
                },
                () -> Optional.ifPresent(loginResponse, response -> {
                            onFailure(response.getStatusCode(), response.getErrorDescription());
                        },
                        () -> onFailure(0, null)));
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        final String messageText = message == null ? Constants.ERROR_MESSAGE : message;
        Optional.ifPresent(userLoginCallback, callback -> {
            callback.onUserLoginFailure(errorCode, messageText);
        });
    }
}
