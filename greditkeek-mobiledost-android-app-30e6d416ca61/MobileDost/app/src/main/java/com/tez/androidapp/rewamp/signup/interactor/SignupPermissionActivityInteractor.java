package com.tez.androidapp.rewamp.signup.interactor;

import com.tez.androidapp.app.general.feature.authentication.signup.callbacks.UserSignUpCallback;
import com.tez.androidapp.commons.models.network.dto.request.UserSignUpRequest;
import com.tez.androidapp.commons.models.network.dto.response.UserSignUpResponse;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.signup.presenter.ISignupPermissionActivityInteractorOutput;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

public class SignupPermissionActivityInteractor implements ISignupPermissionActivityInteractor {

    private final ISignupPermissionActivityInteractorOutput iSignupPermissionActivityInteractorOutput;

    public SignupPermissionActivityInteractor(ISignupPermissionActivityInteractorOutput iSignupPermissionActivityInteractorOutput) {
        this.iSignupPermissionActivityInteractorOutput = iSignupPermissionActivityInteractorOutput;
    }

    @Override
    public void submitUserSignUpRequest(UserSignUpRequest userSignUpRequest){
        UserCloudDataStore.getInstance().signUp(userSignUpRequest, new UserSignUpCallback() {
            @Override
            public void onUserSignUpSuccess(UserSignUpResponse responseBody) {
                iSignupPermissionActivityInteractorOutput.onSubmitSignUpRequestSuccess();
            }

            @Override
            public void onUserSignUpError(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    submitUserSignUpRequest(userSignUpRequest);

                else if(errorCode == ResponseStatusCode.USER_ALREADY_EXIST_AGAINST_MOBILE_NUMBER.getCode())
                    iSignupPermissionActivityInteractorOutput.mobileNumberAlreadyExistForSignUp();

                else
                    iSignupPermissionActivityInteractorOutput.onSubmitSignUpRequestFailure(errorCode, message);
            }
        });
    }

}
