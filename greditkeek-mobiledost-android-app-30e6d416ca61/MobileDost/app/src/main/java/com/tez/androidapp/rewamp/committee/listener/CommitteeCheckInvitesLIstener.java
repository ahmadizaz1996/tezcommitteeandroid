package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeMetaDataResponse;

import java.util.List;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface CommitteeCheckInvitesLIstener {

    void onCommitteeCheckInvitesSuccess(List<CommitteeCheckInvitesResponse> committeeCheckInvitesResponse);

    void onCommitteeCheckInvitesFailure(int errorCode, String message);

}
