package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.BasicInformation;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Question;

import java.util.List;

public interface ProfileBasicCallback {
    void onProfileBasicSuccess(BasicInformation data);
    void onProfileBasicFailure(int errorCode, String message);
}
