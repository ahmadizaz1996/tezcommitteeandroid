package com.tez.androidapp.app.general.feature.account.feature.pin.code.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 8/25/2017.
 */

public class ChangePinRequest extends BaseRequest{
    public static final String METHOD_NAME = "v2/user/account/changePin";
    private String newPin;
    private String oldPin;

    public ChangePinRequest(String newPin, String oldPin) {
        this.newPin = newPin;
        this.oldPin = oldPin;
    }

    @Override
    public String toString() {
        return "ChangePinRequest{" +
                "newPin='" + newPin + '\'' +
                ", oldPin='" + oldPin + '\'' +
                '}';
    }
}
