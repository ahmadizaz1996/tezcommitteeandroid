package com.tez.androidapp.rewamp.bima.products.policy.response;

import com.tez.androidapp.app.base.response.BaseResponse;

public class PurchasePolicyResponse extends BaseResponse {

    private PurchasedInsurancePolicy finalizeInsurancePolicyResponseDto;

    public PurchasedInsurancePolicy getFinalizeInsurancePolicyResponseDto() {
        return finalizeInsurancePolicyResponseDto;
    }
}
