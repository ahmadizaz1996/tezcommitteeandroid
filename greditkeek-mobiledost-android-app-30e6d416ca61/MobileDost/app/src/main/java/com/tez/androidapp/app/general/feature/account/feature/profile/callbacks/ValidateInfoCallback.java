package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.ValidateInfo;

/**
 * Created by FARHAN DHANANI on 12/3/2018.
 */
public interface ValidateInfoCallback {

    void onValidateInfoSuccess(ValidateInfo validateInfo);

    void onValidateInfoFailure(int errorCode, String message);
}
