package com.tez.androidapp.app.vertical.bima.policies.models.network.dto.request;

/**
 * Created by FARHAN DHANANI on 7/18/2018.
 */
public class GetActivieInsurancePolicyDetailsRequest {
    public static final String METHOD_NAME = "v1/insurance/policy/{" + GetActivieInsurancePolicyDetailsRequest.Params.ACTIVE_INSURANCE_POLICY_ID + "}";

    public abstract static class Params {
        public static final String ACTIVE_INSURANCE_POLICY_ID = "insurancePolicyId";
    }
}
