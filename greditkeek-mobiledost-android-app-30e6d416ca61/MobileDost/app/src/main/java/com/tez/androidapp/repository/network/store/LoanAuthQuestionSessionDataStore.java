package com.tez.androidapp.repository.network.store;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.PersonalQuestionCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.PersonalQuestionRH;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.network.AuthHeaderInterceptor;

import net.tez.fragment.util.optional.Optional;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;

public class LoanAuthQuestionSessionDataStore extends LoanQuestionSessionDataStore {

    public static LoanAuthQuestionSessionDataStore getInstance() {
        return LazyHolder.INSTANCE;
    }

    @Override
    protected OkHttpClient.Builder addMoreFeaturesToClient() {
        okHttpClientBuilder = super.addMoreFeaturesToClient();
        okHttpClientBuilder.addInterceptor(new AuthHeaderInterceptor());
        return okHttpClientBuilder;
    }

    public void getPersonalQuestions(String lang, PersonalQuestionCallback personalQuestionCallback) {
        Optional.ifPresent(MDPreferenceManager.getAccessToken(), accessToken -> {
            mobileDostLoanAPI.getPersonalQuestions(accessToken.getAccessToken(), lang)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new PersonalQuestionRH(this, personalQuestionCallback));
        });
    }

    private static class LazyHolder {
        private static final LoanAuthQuestionSessionDataStore INSTANCE = new LoanAuthQuestionSessionDataStore();
    }
}
