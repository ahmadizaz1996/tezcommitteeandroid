package com.tez.androidapp.rewamp.bima.products.policy.response;

import com.tez.androidapp.app.base.response.BaseResponse;

public class InsuranceProductDetailResponse extends BaseResponse {

    private InsuranceProductDetails insuranceProductDetails;

    public InsuranceProductDetails getInsuranceProductDetails() {
        return insuranceProductDetails;
    }
}
