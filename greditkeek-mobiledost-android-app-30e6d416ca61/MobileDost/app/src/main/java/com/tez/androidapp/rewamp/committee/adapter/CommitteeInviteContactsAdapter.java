package com.tez.androidapp.rewamp.committee.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.models.extract.ExtractedContact;
import com.tez.androidapp.commons.utils.app.Log;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezCheckBox;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.MissingFormatArgumentException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeInviteContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    public static final int HEADER_ITEM = 0;
    public static final int CONTACT_ITEM = 1;
    private final List<ExtractedContact> listItems = new ArrayList<>();
    private final ContactsListener listener;
    private final int memberSize;
    private ContactFilter contactFilter;
    private ArrayList<ExtractedContact> originalList = new ArrayList<>();
    HashMap<String, ExtractedContact> extractedContactHashMap = new HashMap<>();

/*    GenericRecyclerViewAdapter<ExtractedContact,
            CommitteeInviteContactsAdapter.ContactsListener,
            CommitteeInviteContactsAdapter.ContactsViewHolder>*/


    public CommitteeInviteContactsAdapter(@NonNull List<ExtractedContact> items, @Nullable ContactsListener listener, int memberSize) {
//        super(items, listener);
        this.listItems.addAll(items);
        this.originalList.addAll(this.listItems);
        this.listener = listener;
        this.memberSize = memberSize;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == HEADER_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_item, parent, false);
            return new ContactsHeaderViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item, parent, false);
            return new ContactsViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (listItems.get(position).isHeader())
            return HEADER_ITEM;
        else
            return CONTACT_ITEM;
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ExtractedContact item = listItems.get(position);

        if (holder instanceof ContactsViewHolder) {
            try {
                ContactsViewHolder contactItemVH = (ContactsViewHolder) holder;

                contactItemVH.tvNmae.setText(item.getDisplayName());
                contactItemVH.tvNumber.setText(item.getPhone());

                contactItemVH.contactCheckBox.setOnCheckedChangeListener(null);

                if (item.isSelected()) {
                    Log.d("CONTACT_ADAPTER : ", item.getDisplayName() + " selected");
                    contactItemVH.contactCheckBox.setChecked(true);
                } else {
                    Log.d("CONTACT_ADAPTER : ", item.getDisplayName() + " unselected");
                    contactItemVH.contactCheckBox.setChecked(false);
                }

            /*if (!isEligible) {
                contactItemVH.contactCheckBox.setChecked(false);
            }*/

                contactItemVH.contactCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        Log.d("CONTACT_ADAPTER : ", b + "");
                        boolean isEligible = extractedContactHashMap.size() < memberSize;
                        if (b) {
                            if (isEligible) {
                                Log.d("CONTACT_ADAPTER", "isEligible " + isEligible);
                                item.setSelected(b);
                                extractedContactHashMap.put(item.getPhone(), item);
                            } else {
                                Log.d("CONTACT_ADAPTER", "setCheck " + false);
                                item.setSelected(false);
                                compoundButton.setChecked(false);
                            }
                        } else {
                            if (extractedContactHashMap.containsKey(item.getPhone())) {
                                extractedContactHashMap.remove(item.getPhone());
                                item.setSelected(false);
                            }
                        }
                        listener.onClickContact(item, contactItemVH.contactCheckBox, true);
                    }
                });

                contactItemVH.ivImage.setVisibility(View.GONE);
                contactItemVH.tvLetters.setVisibility(View.VISIBLE);
                contactItemVH.viewStepCnicBg.setVisibility(View.VISIBLE);
                String charOne = String.valueOf(item.getDisplayName().charAt(0));

                String charTwo = "";
                /*if (item.getDisplayName().split(" ").length > 1)
                    charTwo = String.valueOf(item.getDisplayName().charAt(1));*/

                contactItemVH.tvLetters.setText(charOne + charTwo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ContactsHeaderViewHolder contactsHeaderViewHolder = (ContactsHeaderViewHolder) holder;
            contactsHeaderViewHolder.headerAlphabet.setText(listItems.get(position).getDisplayName());
        }
    }

    public void deSelect(@Nullable Object item) throws ClassCastException {
        int position = this.listItems. indexOf(item);
        if (position > -1) {
            this.listItems.get(position).setSelected(false);
            if (extractedContactHashMap.containsKey(item))
                extractedContactHashMap.remove(item);
            notifyItemChanged(position);

        }
    }

    @Override
    public Filter getFilter() {
        if (contactFilter == null) {
            contactFilter = new ContactFilter();
        }
        return contactFilter;
    }

    private class ContactFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();

            if (constraint == null || constraint.length() == 0) {
                filterResults.count = originalList.size();
                filterResults.values = originalList;
            } else {
                List<ExtractedContact> filterContacts = new ArrayList<>();
                for (ExtractedContact userContact : originalList) {
                    if (userContact.getDisplayName().toLowerCase().trim().startsWith(constraint.toString().toLowerCase().trim())) {
                        filterContacts.add(userContact);
                    }
                }

                filterResults.values = filterContacts;
                filterResults.count = filterContacts.size();
            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            List<ExtractedContact> filterContacts = (List<ExtractedContact>) results.values;

            listItems.clear();
            listItems.addAll(filterContacts);


            notifyDataSetChanged();
        }
    }


    public interface ContactsListener extends BaseRecyclerViewListener {
        void onClickContact(@NonNull ExtractedContact contactsModel, TezCheckBox contactCheckBox, boolean isCheckbox);
    }

    static class ContactsViewHolder extends BaseViewHolder<ExtractedContact, ContactsListener> {

        @BindView(R.id.tv_name)
        private TezTextView tvNmae;

        @BindView(R.id.tv_number)
        private TezTextView tvNumber;

        @BindView(R.id.contactCardView)
        private TezCardView contactCardView;

        @BindView(R.id.tcCheckbox)
        private TezCheckBox contactCheckBox;

        @BindView(R.id.tvLetters)
        private TezTextView tvLetters;

        @BindView(R.id.viewStepCnicBg)
        private CircleImageView viewStepCnicBg;

        @BindView(R.id.ivImage)
        private TezImageView ivImage;


        public ContactsViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(ExtractedContact item, @Nullable ContactsListener listener) {
            super.onBind(item, listener);


        }
    }

    static class ContactsHeaderViewHolder extends BaseViewHolder<ExtractedContact, ContactsListener> {

        @BindView(R.id.headerAlphabet)
        private TezTextView headerAlphabet;

        public ContactsHeaderViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(ExtractedContact item, @Nullable ContactsListener listener) {
            super.onBind(item, listener);
        }
    }
}
