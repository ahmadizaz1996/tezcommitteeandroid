package com.tez.androidapp.rewamp.bima.products.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.rewamp.bima.products.callback.ProductDetailTermConditionCallback;
import com.tez.androidapp.rewamp.bima.products.callback.ProductTermConditionsCallback;
import com.tez.androidapp.rewamp.bima.products.response.ProductTermConditionsResponse;

public class ProductTermConditionsActivityInteractor implements IProductTermConditionsActivityInteractor {

    private final IProductTermConditionsActivityInteractorOutput output;

    public ProductTermConditionsActivityInteractor(IProductTermConditionsActivityInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void getProductTermConditions(int productId) {
        BimaCloudDataStore.getInstance().getProductTermConditions(productId, new ProductTermConditionsCallback() {
            @Override
            public void onGetTermsConditionsSuccess(ProductTermConditionsResponse response) {
                output.onGetTermsConditionsSuccess(response);
            }

            @Override
            public void onGetTermsConditionsFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getProductTermConditions(productId);
                else
                    output.onGetTermsConditionsFailure(errorCode, message);
            }
        });
    }

    @Override
    public void getProductDetailedTermCondition(int productId) {
        BimaCloudDataStore.getInstance().getProductTermCondition(productId, new ProductDetailTermConditionCallback() {
            @Override
            public void onGetProductDetailTermConditionSuccess(String htmlTermCondition) {
                output.onGetProductDetailTermConditionSuccess(htmlTermCondition);
            }

            @Override
            public void onGetProductDetailTermConditionFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getProductDetailedTermCondition(productId);
                else
                    output.onGetProductDetailTermConditionFailure(errorCode, message);
            }
        });
    }
}
