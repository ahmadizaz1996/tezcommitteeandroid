package com.tez.androidapp.rewamp.profile.complete.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.profile.complete.view.CnicInformationActivity;
import com.tez.androidapp.rewamp.profile.router.CompleteProfileRouter;

public class CnicInformationActivityRouter extends CompleteProfileRouter {

    public static final String OCR_CNIC = "OCR_CNIC";
    public static final String OCR_ISSUE_DATE = "OCR_ISSUE_DATE";
    public static final String OCR_EXPIRY_DATE = "OCR_EXPIRY_DATE";
    public static final String OCR_DOB = "OCR_DOB";

    public static CnicInformationActivityRouter createInstance() {
        return new CnicInformationActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        String cnic, String dob,
                                        String issueDate, String expiryDate,
                                        int routeTo) {
        Intent intent = createIntent(from);
        intent.putExtra(OCR_CNIC, cnic);
        intent.putExtra(OCR_ISSUE_DATE, issueDate);
        intent.putExtra(OCR_EXPIRY_DATE, expiryDate);
        intent.putExtra(OCR_DOB, dob);
        addAfterCompletionRoute(intent, routeTo);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, int routeTo) {
        Intent intent = createIntent(from);
        addAfterCompletionRoute(intent, routeTo);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CnicInformationActivity.class);
    }
}
