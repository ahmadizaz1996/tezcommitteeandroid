package com.tez.androidapp.rewamp.signup.presenter;

public interface INewTermsAndConditionActivityPresenter {

    void callSetPin(String pin);

    void callLogin(String pin);
}
