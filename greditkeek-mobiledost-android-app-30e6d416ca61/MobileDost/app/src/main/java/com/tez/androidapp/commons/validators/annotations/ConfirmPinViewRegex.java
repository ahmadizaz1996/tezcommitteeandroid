package com.tez.androidapp.commons.validators.annotations;

import com.tez.androidapp.commons.validators.filters.ConfirmPinViewFilter;
import com.tez.androidapp.commons.validators.filters.PinViewFilter;

import net.tez.validator.library.annotations.Filterable;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by FARHAN DHANANI on 6/4/2019.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Filterable(ConfirmPinViewFilter.class)
public @interface ConfirmPinViewRegex {
    int id();
    int[] value();
}
