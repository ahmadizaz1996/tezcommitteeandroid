package com.tez.androidapp.rewamp.general.termsandcond;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public class NavigationTermsAndConditionsActivityRouter extends BaseActivityRouter {


    public static NavigationTermsAndConditionsActivityRouter createInstance() {
        return new NavigationTermsAndConditionsActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, NavigationTermsAndConditionsActivity.class);
    }
}
