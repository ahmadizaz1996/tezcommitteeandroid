package com.tez.androidapp.rewamp.advance.request.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.rewamp.general.beneficiary.router.BeneficiaryListActivityRouter;

import net.tez.viewbinder.library.core.OnClick;

public class FreeBimaOnBoardingActivity extends PlaybackActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free_bima_onboarding);
        this.updateToolbar(findViewById(R.id.toolbar));
    }

    @OnClick(R.id.btContinue)
    private void routeToBeneficiaryListActivity() {
        BeneficiaryListActivityRouter.createInstance().setDependenciesAndRouteForResult(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BeneficiaryListActivityRouter.REQUEST_CODE_BENEFICIARY_LIST && resultCode == RESULT_OK)
            finish();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    protected String getScreenName() {
        return "FreeBimaOnBoardingActivity";
    }
}
