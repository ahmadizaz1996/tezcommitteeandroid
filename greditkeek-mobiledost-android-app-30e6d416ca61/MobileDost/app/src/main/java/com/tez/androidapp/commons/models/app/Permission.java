package com.tez.androidapp.commons.models.app;

/**
 * Created  on 12/29/2016.
 */

public class Permission {

    private String permissionFullName;
    private String permissionDisplayName;
    private boolean isGranted;

    public Permission(String permissionFullName, String permissionDisplayName) {
        this.permissionFullName = permissionFullName;
        this.permissionDisplayName = permissionDisplayName;
    }

    public Permission(String permissionFullName, String permissionDisplayName, boolean isGranted) {
        this.permissionFullName = permissionFullName;
        this.permissionDisplayName = permissionDisplayName;
        this.isGranted = isGranted;
    }

    public boolean isGranted() {
        return isGranted;
    }

    public void setGranted(boolean granted) {
        isGranted = granted;
    }

    public String getPermissionFullName() {
        return permissionFullName;
    }

    public String getPermissionDisplayName() {
        return permissionDisplayName;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "permissionFullName='" + permissionFullName + '\'' +
                ", permissionDisplayName='" + permissionDisplayName + '\'' +
                ", isGranted=" + isGranted +
                '}';
    }
}
