package com.tez.androidapp.rewamp.advance.limit.view;

import android.os.Bundle;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;

import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

public class LimitRejectedActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_limit_rejected);
        ViewBinder.bind(this);
    }

    @Override
    public void onBackPressed() {
        routeToDashboard();
    }

    @OnClick(R.id.btOkay)
    private void routeToDashboard() {
        DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
    }

    @Override
    protected String getScreenName() {
        return "LimitRejectedActivity";
    }
}
