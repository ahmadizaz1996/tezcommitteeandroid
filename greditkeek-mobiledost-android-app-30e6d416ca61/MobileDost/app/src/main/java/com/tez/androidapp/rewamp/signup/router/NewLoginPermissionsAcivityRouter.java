package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.NewLoginPermissionsActivity;

public class NewLoginPermissionsAcivityRouter extends BaseActivityRouter {

    public static final String MOBILE_NUMBER = "MOBILE_NUMBER";
    public static final String SOCIAL_ID = "SOCIAL_ID";
    public static final String SOCIAL_TYPE = "SOCIAL_TYPE";
    public static final String PIN = "PIN";

    public static NewLoginPermissionsAcivityRouter createInstance() {
        return new NewLoginPermissionsAcivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @Nullable String mobileNumber,
                                        @NonNull String pin,
                                        @Nullable String socialId,
                                        @Nullable Integer socialType) {
        Intent intent = createIntent(from);
        intent.putExtra(MOBILE_NUMBER, mobileNumber);
        intent.putExtra(SOCIAL_ID, socialId);
        intent.putExtra(SOCIAL_TYPE, socialType);
        intent.putExtra(PIN, pin);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, NewLoginPermissionsActivity.class);
    }
}
