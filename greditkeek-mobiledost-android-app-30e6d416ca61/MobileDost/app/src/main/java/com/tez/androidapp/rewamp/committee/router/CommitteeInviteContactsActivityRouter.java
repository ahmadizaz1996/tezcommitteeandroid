package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.view.CommitteeCompletionActivity;
import com.tez.androidapp.rewamp.committee.view.CommitteeInviteContactsActivity;

import static com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter.COMMITTEE_DATA;

public class CommitteeInviteContactsActivityRouter extends BaseActivityRouter {


    public static final int CONTACT_FETCH = 6262;

    public static CommitteeInviteContactsActivityRouter createInstance() {
        return new CommitteeInviteContactsActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteePackageModel committeePackageModel) {
        Intent intent = createIntent(from);
        intent.putExtra(COMMITTEE_DATA, committeePackageModel);
        routeForResult(from, intent,CONTACT_FETCH);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        routeForResult(from, intent, CONTACT_FETCH);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeInviteContactsActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

}
