package com.tez.androidapp.rewamp.signup;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutRegex;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;

import net.tez.validator.library.annotations.Order;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ForgotPinActivity extends PlaybackActivity implements ValidationListener {

    @TextInputLayoutRegex(regex = Constants.PHONE_NUMBER_VALIDATOR_REGEX, value = {1, R.string.string_please_enter_valid_phone_number})
    @Order(1)
    @BindView(R.id.tilMobileNumber)
    private TezTextInputLayout tilMobileNumber;

    @BindView(R.id.etMobileNumber)
    private TezEditTextView etMobileNumber;

    @BindView(R.id.btCreatePin)
    private TezButton btRecoverPin;

    private CompositeDisposable allDisposables;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pin2);
        ViewBinder.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.allDisposables = new CompositeDisposable();
        listenChangesOnViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        allDisposables.dispose();
    }

    private void listenChangesOnViews() {
        allDisposables.add(RxTextView.textChanges(this.etMobileNumber).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));

    }


    private void init() {
        initOnListners();
    }

    private void initOnListners() {
        this.btRecoverPin.setDoubleTapSafeOnClickListener(view -> onClickBtRecoverPin());
    }

    private void onClickBtRecoverPin() {
        FieldValidator.validate(this, new ValidationListener() {
            @Override
            public void validateSuccess() {
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                filterChain.doFilter();
            }
        });
    }

    public int getAudioId() {
        return 0;
    }

    @Override
    public void validateSuccess() {
        this.btRecoverPin.setButtonNormal();
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.btRecoverPin.setButtonInactive();
    }

    @Override
    protected String getScreenName() {
        return "ForgotPinActivity";
    }
}
