package com.tez.androidapp.rewamp.profile.complete.presenter;

import android.content.DialogInterface;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.android.gms.common.util.CollectionUtils;
import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Answer;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.UserProfile;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateProfileWithCnicRequest;
import com.tez.androidapp.commons.models.app.City;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.rewamp.profile.complete.interactor.CnicInformationFragmentInteractor;
import com.tez.androidapp.rewamp.profile.complete.interactor.ICnicInformationFragmentInteractor;
import com.tez.androidapp.rewamp.profile.complete.view.ICnicInformationFragmentView;

import net.tez.logger.library.utils.TextUtil;

import java.util.List;

public class CnicInformationFragmentPresenter extends CompleteProfileCnicUploadActivityPresenter
        implements ICnicInformationFragmentPresenter, ICnicInformationFragmentInteractorOutput {

    private final ICnicInformationFragmentView iCnicInformationFragmentView;
    private final ICnicInformationFragmentInteractor iCnicInformationFragmentInteractor;
    private AnswerSelected userAnswerSelected;

    public CnicInformationFragmentPresenter(ICnicInformationFragmentView iCnicInformationFragmentView) {
        super(iCnicInformationFragmentView);
        this.iCnicInformationFragmentView = iCnicInformationFragmentView;
        this.iCnicInformationFragmentInteractor = new CnicInformationFragmentInteractor(this);
    }

    @Override
    public void getProfile() {
        this.iCnicInformationFragmentInteractor.getCnicUploads();
    }

    @Override
    public void onCompleteUserProfileSuccess(UserProfile userProfile,
                                             User user,
                                             @NonNull List<City> cities,
                                             @NonNull List<Option> martialStatusOptions) {
        this.iCnicInformationFragmentView.setTextToAutoCompleteTextViewPlaceOfBirth(user.getPlaceOfBirthName(),
                user.getPlaceOfBirthId());
        this.iCnicInformationFragmentView.setTextToEtMothersMaiden(user.getMotherMaidenName());
        this.iCnicInformationFragmentView.setTextToEtName(user.getFullName());
        this.iCnicInformationFragmentView.populateDropDownForPlaceOfBirth(cities);
        this.iCnicInformationFragmentView.populateDropDownForCurrentCity(cities);
        this.iCnicInformationFragmentView.setDoubleTapOnClickListenerToBtContinue(userProfile, user);
        this.iCnicInformationFragmentView.setVisibillityToShimmer(View.GONE);
        this.iCnicInformationFragmentView.setVisibillityToDetailCardView(View.VISIBLE);
        this.updateMaritalStatus(user.getPersonalInformationList(), martialStatusOptions, user.getGender());
        Optional.doWhen(TextUtil.equals(user.getGender(), User.GENDER_MALE),
                () -> {
                    this.iCnicInformationFragmentView.setTextToSGender(0, martialStatusOptions.get(0));
                    this.iCnicInformationFragmentView.updateHintToTilFatherName(0);
                });
        Optional.doWhen(TextUtil.equals(user.getGender(), User.GENDER_FEMALE),
                () -> {
                    this.iCnicInformationFragmentView.setTextToSGender(1, martialStatusOptions.get(1));
                    this.iCnicInformationFragmentView.updateHintToTilFatherName(0);
                });
        Optional.doWhen(TextUtil.equals(user.getGender(), User.GENDER_TRANSGENDER),
                () -> {
                    this.iCnicInformationFragmentView.setTextToSGender(2, martialStatusOptions.get(2));
                    this.iCnicInformationFragmentView.updateHintToTilFatherName(2);
                });
        Optional.ifPresent(user.getFatherName(), fatherName -> {
            this.iCnicInformationFragmentView.setTextToEtFatherName(user.getFatherName());
            this.iCnicInformationFragmentView.setHintToTilFatherName(Utility.getStringFromResource(R.string.father_name));
        });
        Optional.ifPresent(user.getHusbandName(), husbandName -> {
            this.iCnicInformationFragmentView.setTextToEtFatherName(user.getHusbandName());
            this.iCnicInformationFragmentView.setHintToTilFatherName(Utility.getStringFromResource(R.string.husband_name));
        });
        this.iCnicInformationFragmentView.onClickListenerForSGender(martialStatusOptions);
        this.iCnicInformationFragmentView.setTextToEtCnicNumber(TextUtil.isEmpty(this.userCNIC) ? user.getCnic() : getOCRCnic());
        Optional.doWhen(TextUtil.isEmpty(this.userCNICDateOfIssue),
                () -> this.iCnicInformationFragmentView.setTextToEtDateOfIssue(user.getCnicDateOfIssue()),
                () -> this.iCnicInformationFragmentView.setTextToEtDateOfIssueFromOcr(this.userCNICDateOfIssue));
        Optional.doWhen(TextUtil.isEmpty(this.userCNICDateOfBirth),
                () -> this.iCnicInformationFragmentView.setTextToEtDOB(user.getDateOfBirth()),
                () -> this.iCnicInformationFragmentView.setTextToEtDobFromOcr(this.userCNICDateOfBirth));
        this.updateUserCnicExpiryDate(user);
        this.updateViewsForCnicImages(userProfile);
        this.updateUserAddresses(user);
        this.handleEditableFeilds(user);
    }

    private void updateUserCnicExpiryDate(User user) {
        Optional.doWhen(TextUtil.isEmpty(this.userCNICDateOfExpiry),
                () -> Optional.doWhen(user.isCnicLifeTimeValid(),
                        iCnicInformationFragmentView::setLifeTimeToEtDateOfExpiry,
                        () -> iCnicInformationFragmentView.setTextToEtDateOfExpiry(user.getCnicDateOfExpiry())),
                () -> iCnicInformationFragmentView.setTextToEtDateOfExpiryFromOcr(userCNICDateOfExpiry));
    }

    private String getOCRCnic() {
        return this.userCNIC.replace("-", "");
    }

    private void updateMaritalStatus(List<AnswerSelected> answerSelecteds,
                                     List<Option> martialStatusOptions,
                                     String gender) {
        Optional.doWhen(!CollectionUtils.isEmpty(answerSelecteds) && answerSelecteds.get(0) != null,
                () -> Optional.doWhen(TextUtil.equals(gender, User.GENDER_MALE),
                        () -> parseMaritalStatus(martialStatusOptions.get(0), answerSelecteds.get(0).getAnswerReference()),
                        () -> Optional.doWhen(TextUtil.equals(gender, User.GENDER_FEMALE),
                                () -> parseMaritalStatus(martialStatusOptions.get(1), answerSelecteds.get(0).getAnswerReference()),
                                () -> Optional.doWhen(TextUtil.equals(gender, User.GENDER_TRANSGENDER),
                                        () -> parseMaritalStatus(martialStatusOptions.get(2), answerSelecteds.get(0).getAnswerReference())))));
    }

    private void parseMaritalStatus(Option selectedOption, String selectedAnswerRefrence) {
        for (Answer answer : selectedOption.getAnswers()) {
            Optional.doWhen(TextUtil.equals(selectedAnswerRefrence, answer.getRefName()),
                    () -> {
                        userAnswerSelected = new AnswerSelected(selectedOption.getRefName(), answer);
                        iCnicInformationFragmentView.setTextToSMaritalStatus(userAnswerSelected);
                    });
        }
    }

    @Override
    public boolean isMaritalStatusEquals() {
        AnswerSelected userMaritalStatus = this.iCnicInformationFragmentView.getSelectedMaritalStatus();
        if (this.userAnswerSelected == null && userMaritalStatus == null) {
            return true;
        } else if (userAnswerSelected == null || userMaritalStatus == null) {
            return false;
        } else {
            return this.userAnswerSelected.equals(userMaritalStatus);
        }
    }

    private void callForUpdateUserProfile(final User user,
                                          final UpdateProfileWithCnicRequest profileWithCnicRequest,
                                          final boolean isPartiallRequest, City selectedPlaceOfBirthCity,
                                          City selectedCurrentCity, AnswerSelected selectedMaritalStatus) {
        Optional.doWhen(userHasEditedAnyFeilds(user, profileWithCnicRequest), () -> {
            if (noFeildsAreChanged(user, profileWithCnicRequest)) {
                profileWithCnicRequest.setCnicFieldsDetectionStatus(user.getCnicFieldsDetectionStatus());
            } else if (this.ocrHasBeenRun && allFeildsAreDetected(profileWithCnicRequest)) {
                profileWithCnicRequest.setCnicFieldsDetectionStatus(UpdateProfileWithCnicRequest.CnicFieldsDetectionStatus.AUTO_FILLED.getValue());
            } else if (this.ocrHasBeenRun && (someFeildsAreDetectedOrEdited(profileWithCnicRequest))) {
                profileWithCnicRequest.setCnicFieldsDetectionStatus(UpdateProfileWithCnicRequest.CnicFieldsDetectionStatus.AUTO_FILLED_EDITED.getValue());
            } else {
                profileWithCnicRequest.setCnicFieldsDetectionStatus(UpdateProfileWithCnicRequest.CnicFieldsDetectionStatus.MANUAL_FILLED.getValue());
            }
            this.iCnicInformationFragmentView.showTezLoader();
            this.iCnicInformationFragmentInteractor.updateUserProfile(user, profileWithCnicRequest,
                    isPartiallRequest, selectedPlaceOfBirthCity, selectedCurrentCity, selectedMaritalStatus);
        }, this.iCnicInformationFragmentView::routeToPersonalInformationScreen);
    }

    private boolean checkPartialRequestForCnicImages(boolean[] flagsForPicturesNeedToUploaded) {
        return (flagsForPicturesNeedToUploaded[0] && !userFrontNicIsUploaded)
                || (flagsForPicturesNeedToUploaded[1] && !userBackNicIsUploaded)
                || (flagsForPicturesNeedToUploaded[2] && !userSelfieIsUploaded);
    }

    @Override
    public void updateUserProfile(UserProfile userProfile,
                                  User user,
                                  UpdateProfileWithCnicRequest updateProfileWithCnicRequest, City selectedPlaceOfBirthCity,
                                  City selectedCurrentCity, AnswerSelected selectedMaritalStatus) {
        Optional.ifPresent(userProfile.getRequestId(), id -> {
            Optional.ifPresent(userProfile.getCnicUploads(), (cnicUploads) -> {
                Optional.doWhen(checkPartialRequestForCnicImages(getFlagsForPicturesNeedsToUploaded(cnicUploads)),
                        () -> {
                            iCnicInformationFragmentView.showInformativeDialog(R.string.notice, R.string.please_enter_all_fields_correctly);
                            handleEditableFeilds(user);
                        },
                        () -> {
                            callForUpdateUserProfile(user, updateProfileWithCnicRequest, false, selectedPlaceOfBirthCity, selectedCurrentCity, selectedMaritalStatus);
                            this.iCnicInformationFragmentView.setCvFrontPartRejected(false);
                            this.iCnicInformationFragmentView.setCvSelfiePartRejected(false);
                            this.iCnicInformationFragmentView.setCvBackPartRejected(false);
                        });
            }, () -> callForUpdateUserProfile(user, updateProfileWithCnicRequest, false, selectedPlaceOfBirthCity, selectedCurrentCity, selectedMaritalStatus));
        }, () -> Optional.ifPresent(userProfile.getCnicUploads(), (cnicUploads) -> {
            boolean isPartialRequest = checkPartialRequestForCnicImages(getFlagsForPicturesNeedsToUploaded(cnicUploads));
            Optional.doWhen(isPartialRequest, () ->
                            iCnicInformationFragmentView.showInformativeDialog(R.string.notice,
                                    R.string.some_feilds_are_not_entered,
                                    (dialog, which) -> Optional.doWhen(which == DialogInterface.BUTTON_POSITIVE,
                                            () -> callForUpdateUserProfile(user, updateProfileWithCnicRequest,
                                                    isPartialRequest, selectedPlaceOfBirthCity, selectedCurrentCity,
                                                    selectedMaritalStatus))),
                    () -> callForUpdateUserProfile(user, updateProfileWithCnicRequest,
                            isPartialRequest, selectedPlaceOfBirthCity, selectedCurrentCity,
                            selectedMaritalStatus));
        }, () -> callForUpdateUserProfile(user, updateProfileWithCnicRequest, false, selectedPlaceOfBirthCity, selectedCurrentCity, selectedMaritalStatus)));
    }

    @Override
    public void updateUserProfile(UserProfile userProfile,
                                  final User user,
                                  final UpdateProfileWithCnicRequest profileWithCnicRequest,
                                  final boolean validationsPartiallyPassed, City selectedPlaceOfBirthCity,
                                  City selectedCurrentCity, AnswerSelected selectedMaritalStatus) {
        Optional.doWhen(userProfile.getRequestId() == null && validationsPartiallyPassed,
                () -> iCnicInformationFragmentView.showInformativeDialog(R.string.notice,
                        R.string.some_feilds_are_not_entered,
                        (dialog, which) -> Optional.doWhen(which == DialogInterface.BUTTON_POSITIVE,
                                () -> callForUpdateUserProfile(user, profileWithCnicRequest, true,
                                        selectedPlaceOfBirthCity, selectedCurrentCity, selectedMaritalStatus))), () -> iCnicInformationFragmentView.showInformativeDialog(R.string.notice, R.string.please_enter_all_fields_correctly));
    }

    @Override
    public boolean userHasEditedAnyFeilds(User user, UpdateProfileWithCnicRequest updateProfileWithCnicRequest) {
        return !TextUtil.equals(user.getFullName(), updateProfileWithCnicRequest.getFullName()) ||
                !TextUtil.equals(user.getGender(), updateProfileWithCnicRequest.getGender()) ||
                !TextUtil.equals(user.getHusbandName(), updateProfileWithCnicRequest.getHusbandName()) ||
                !TextUtil.equals(user.getFatherName(), updateProfileWithCnicRequest.getFatherName()) ||
                !TextUtil.equals(user.getCnic(), updateProfileWithCnicRequest.getCnic()) ||
                !TextUtil.equals(user.getCnicDateOfIssue(), updateProfileWithCnicRequest.getCnicDateOfIssue()) ||
                !TextUtil.equals(user.getCnicDateOfExpiry(), updateProfileWithCnicRequest.getCnicDateOfExpiry()) ||
                !TextUtil.equals(user.getCnicDateOfExpiry(), updateProfileWithCnicRequest.getCnicDateOfExpiry()) ||
                !TextUtil.equals(user.getDateOfBirth(), updateProfileWithCnicRequest.getDateOfBirth()) ||
                !TextUtil.equals(user.getMotherMaidenName(), updateProfileWithCnicRequest.getMotherMaidenName()) ||
                !(user.isPlaceOfBirthEquals(updateProfileWithCnicRequest.getPlaceOfBirthId())) ||
                !user.isAddressEquals(updateProfileWithCnicRequest.getAddress()) ||
                !user.isCityEquals(updateProfileWithCnicRequest.getCityId()) ||
                !isMaritalStatusEquals() ||
                userSelfieIsUploaded ||
                userBackNicIsUploaded ||
                userFrontNicIsUploaded;
    }

    private boolean noFeildsAreChanged(User user, UpdateProfileWithCnicRequest profileWithCnicRequest) {
        return TextUtil.equals(user.getCnic(), profileWithCnicRequest.getCnic())
                && user.isCnicLifeTimeValid() == profileWithCnicRequest.isCnicLifeTimeValid()
                && TextUtil.equals(user.getCnicDateOfIssue(), profileWithCnicRequest.getCnicDateOfIssue())
                && TextUtil.equals(user.getCnicDateOfExpiry(), profileWithCnicRequest.getCnicDateOfExpiry())
                && TextUtil.equals(user.getDateOfBirth(), profileWithCnicRequest.getDateOfBirth());
    }

    private boolean userHasFilledNothing(User user, UpdateProfileWithCnicRequest profileWithCnicRequest) {
        return !areAllFeildsAbsent(profileWithCnicRequest)
                && TextUtil.isEmpty(user.getCnic())
                && TextUtil.isEmpty(user.getCnicDateOfExpiry())
                && TextUtil.isEmpty(user.getDateOfBirth())
                && TextUtil.isEmpty(user.getCnicDateOfIssue());
    }

    private boolean allFeildsAreDetected(UpdateProfileWithCnicRequest updateProfileWithCnicRequest) {
        return areAllOCRFeildsPresent(updateProfileWithCnicRequest) && allOCRFeildsAreUnEdited(updateProfileWithCnicRequest);
    }

    private boolean isAnyFeildEditedWithOutOCR(User user, UpdateProfileWithCnicRequest updateProfileWithCnicRequest) {
        return !TextUtil.equals(updateProfileWithCnicRequest.getCnicDateOfExpiry(), user.getCnicDateOfExpiry())
                || !user.isCnicLifeTimeValid() == updateProfileWithCnicRequest.isCnicLifeTimeValid()
                || !TextUtil.equals(updateProfileWithCnicRequest.getCnicDateOfIssue(), user.getCnicDateOfIssue())
                || !TextUtil.equals(updateProfileWithCnicRequest.getDateOfBirth(), user.getDateOfBirth())
                || !TextUtil.equals(updateProfileWithCnicRequest.getCnic(), user.getCnic());
    }

    private boolean someFeildsAreDetectedOrEdited(UpdateProfileWithCnicRequest updateProfileWithCnicRequest) {
        return !(areAllOCRFeildsPresent(updateProfileWithCnicRequest) &&
                allOCRFeildsAreUnEdited(updateProfileWithCnicRequest));
    }

    private boolean allOCRFeildsAreUnEdited(UpdateProfileWithCnicRequest updateProfileWithCnicRequest) {
        return TextUtil.equals(updateProfileWithCnicRequest.getCnicDateOfExpiry(), Utility.getFormattedDate(userCNICDateOfExpiry,
                Constants.UI_DATE_FORMAT, Constants.BACKEND_DATE_FORMAT))
                && TextUtil.equals(updateProfileWithCnicRequest.getCnicDateOfIssue(), Utility.getFormattedDate(userCNICDateOfIssue,
                Constants.UI_DATE_FORMAT, Constants.BACKEND_DATE_FORMAT))
                && TextUtil.equals(updateProfileWithCnicRequest.getDateOfBirth(), Utility.getFormattedDate(userCNICDateOfBirth,
                Constants.UI_DATE_FORMAT, Constants.BACKEND_DATE_FORMAT))
                && TextUtil.equals(updateProfileWithCnicRequest.getCnic(), userCNIC.replaceAll("-", ""));
    }

    private boolean allFeildsAreEdited(UpdateProfileWithCnicRequest updateProfileWithCnicRequest) {
        return !(TextUtil.equals(updateProfileWithCnicRequest.getCnicDateOfExpiry(), userCNICDateOfExpiry)
                || TextUtil.equals(updateProfileWithCnicRequest.getCnicDateOfIssue(), userCNICDateOfIssue)
                || TextUtil.equals(updateProfileWithCnicRequest.getDateOfBirth(), userCNICDateOfBirth)
                || TextUtil.equals(updateProfileWithCnicRequest.getCnic(), userCNIC));
    }

    private boolean areAllOCRFeildsPresent(UpdateProfileWithCnicRequest updateProfileWithCnicRequest) {
        return !(TextUtil.isEmpty(updateProfileWithCnicRequest.getCnicDateOfExpiry())
                || TextUtil.isEmpty(updateProfileWithCnicRequest.getCnicDateOfIssue())
                || TextUtil.isEmpty(updateProfileWithCnicRequest.getDateOfBirth())
                || TextUtil.isEmpty(updateProfileWithCnicRequest.getCnic()));
    }

    private boolean areAllFeildsAbsent(UpdateProfileWithCnicRequest updateProfileWithCnicRequest) {
        return (TextUtil.isEmpty(updateProfileWithCnicRequest.getCnicDateOfExpiry())
                && TextUtil.isEmpty(updateProfileWithCnicRequest.getCnicDateOfIssue())
                && TextUtil.isEmpty(updateProfileWithCnicRequest.getDateOfBirth())
                && TextUtil.isEmpty(updateProfileWithCnicRequest.getCnic()));
    }

    @Override
    public void handleEditableFeilds(User user) {
        Optional.doWhen(user.getEditable(), () -> Optional.ifPresent(user.getMobileUserRejectedFieldsDto(), rejectedDto -> {
            this.iCnicInformationFragmentView.setsGenderRejected(rejectedDto.getGenderRejected());
            this.iCnicInformationFragmentView.setAutoCompleteTextViewPlaceOfBirthRejected(rejectedDto.getPlaceOfBirthRejected());
            this.iCnicInformationFragmentView.setEtMothersMaidenRejected(rejectedDto.getMotherMaidenNameRejected());
            this.iCnicInformationFragmentView.setEtDobRejected(rejectedDto.getDateOfBirthRejected());
            this.iCnicInformationFragmentView.setEtNameRejected(rejectedDto.getFullNameRejected());
            this.iCnicInformationFragmentView.setEtCnicNumberRejected(rejectedDto.getCnicRejected());
            this.iCnicInformationFragmentView.setEtDateOfIssueRejected(rejectedDto.getCnicDateOfIssueRejected());
            this.iCnicInformationFragmentView.setEtDateOfExpiryRejeced(rejectedDto.getCnicExpiryDateRejected());
            this.iCnicInformationFragmentView.setsMaritalStatusRejected(rejectedDto.getMaritalStatusRejected());
            this.iCnicInformationFragmentView.setEtFatherNameRejected(rejectedDto.getFatherNameRejected()
                    || rejectedDto.getHusbandNameRejected());
        }, () -> makeUserFieldsEditable(true)), () -> makeAllFieldsEditable(false));
    }

    private void makeAllFieldsEditable(boolean editable) {
        makeUserFieldsEditable(editable);
        this.iCnicInformationFragmentView.setEditableEtCurrentAddress(true);
        this.iCnicInformationFragmentView.setEditableAutoCompleteTextViewCurrentCity(true);
    }

    private void enableAllFormOptions(boolean editable) {
        makeAllFieldsEditable(editable);
        makeAllCnicPicturesEditable(editable);
    }

    private void makeUserFieldsEditable(boolean editable) {
        this.iCnicInformationFragmentView.setEditableSGender(editable);
        this.iCnicInformationFragmentView.setEditableAutoCompleteTextViewPlaceOfBirth(editable);
        this.iCnicInformationFragmentView.setEditableEtMothersMaiden(editable);
        this.iCnicInformationFragmentView.setEditableEtDob(editable);
        this.iCnicInformationFragmentView.setEditableEtName(editable);
        this.iCnicInformationFragmentView.setEditableEtFatherName(editable);
        this.iCnicInformationFragmentView.setEditableCnicNumber(editable);
        this.iCnicInformationFragmentView.setEditableToEtDateOfIssue(editable);
        this.iCnicInformationFragmentView.setEditableEtDateOfExpiry(editable);
        this.iCnicInformationFragmentView.setEditableSMaritalStatus(editable);
    }

    private void updateUserAddresses(User user) {
        Optional.ifPresent(user.getAddresses(), userAddresses -> {
            Optional.doWhen(userAddresses.size() >= 1
                            && userAddresses.get(0) != null,
                    () -> {
                        this.iCnicInformationFragmentView.setTextToEtCurrentAddress(userAddresses.get(0).getAddress());
                        Optional.ifPresent(userAddresses.get(0).getCity(),
                                this.iCnicInformationFragmentView::setTextToAutoCompleteTextViewCurrentCity);
                    });
        });
    }

    private void updateViewsForCnicImages(UserProfile userProfile) {
        Optional.doWhen(userProfile.getEditable(),
                () -> Optional.ifPresent(userProfile.getRequestId(), id -> {
                    getAllCnicImages();
                    updateCnicImagesForExistingRequest(userProfile.getCnicUploads());
                }, () -> {
                    makeAllCnicPicturesEditable(true);
                    updateCnicImagesForFirstRequest(userProfile.getCnicUploads());
                })
                , () -> {
                    getAllCnicImages();
                    makeAllCnicPicturesEditable(false);
                });
    }

    private void getAllCnicImages() {
        this.iCnicInformationFragmentView.callForCnicImageInCvFrontPart();
        this.iCnicInformationFragmentView.callForCnicImageInCvBackPart();
        this.iCnicInformationFragmentView.callForCnicImageInCvSelfiePart();
    }

    private void makeAllCnicPicturesEditable(boolean editable) {
        this.iCnicInformationFragmentView.makeCnicImageInCvFrontPartEditable(editable);
        this.iCnicInformationFragmentView.makeCnicImageInCvBackPartEditable(editable);
        this.iCnicInformationFragmentView.makeCnicImageInCvSelfiePartEditable(editable);
    }

    private void updateCnicImagesForExistingRequest(String[] cnicUploads) {
        Optional.ifPresent(cnicUploads, cnic -> {
            boolean[] flagsForPicturesNeedToUploaded = getFlagsForPicturesNeedsToUploaded(cnicUploads);
            this.iCnicInformationFragmentView.setCvFrontPartRejected(flagsForPicturesNeedToUploaded[0]);
            this.iCnicInformationFragmentView.setCvBackPartRejected(flagsForPicturesNeedToUploaded[1]);
            this.iCnicInformationFragmentView.setCvSelfiePartRejected(flagsForPicturesNeedToUploaded[2]);
        }, () -> makeAllCnicPicturesEditable(false));
    }

    private void updateCnicImagesForFirstRequest(String[] cnicUploads) {
        Optional.ifPresent(cnicUploads, (cnic) -> {
            boolean[] flagsForPicturesNeedToUploaded = getFlagsForPicturesNeedsToUploaded(cnicUploads);
            Optional.doWhen(!flagsForPicturesNeedToUploaded[0], this.iCnicInformationFragmentView::callForCnicImageInCvFrontPart);
            Optional.doWhen(!flagsForPicturesNeedToUploaded[1], this.iCnicInformationFragmentView::callForCnicImageInCvBackPart);
            Optional.doWhen(!flagsForPicturesNeedToUploaded[2], this.iCnicInformationFragmentView::callForCnicImageInCvSelfiePart);
        }, this::getAllCnicImages);
    }

    @Override
    public void onCompleteUserProfileFailure(int errorCode, String message) {
        this.iCnicInformationFragmentView.showError(errorCode,
                (d, v) -> this.iCnicInformationFragmentView.finishActivity());
    }

    @Override
    public void onGetCnicUploadsCallbackSuccess(UserProfile userProfile) {
        Optional.ifPresent(this.iCnicInformationFragmentView.getSelectedLanguage(), lang -> {
            getUserProfile(lang, userProfile);
        });
    }


    private void getUserProfile(String lang, UserProfile userProfile) {
        this.iCnicInformationFragmentInteractor.getUserProfile(lang, userProfile);
    }

    @Override
    public void onGetCnicUploadsCallbackFailure(int errorCode, String message) {
        this.iCnicInformationFragmentView.dismissTezLoader();
        this.iCnicInformationFragmentView.showError(errorCode, (d, v) -> iCnicInformationFragmentView.finishActivity());
    }

    @Override
    protected void postExtractingData() {
        Optional.doWhen(TextUtil.isNotEmpty(this.userCNIC),
                () -> this.iCnicInformationFragmentView.setTextToEtCnicNumber(getOCRCnic()));
        Optional.doWhen(TextUtil.isNotEmpty(this.userCNICDateOfIssue),
                () -> this.iCnicInformationFragmentView.setTextToEtDateOfIssueFromOcr(this.userCNICDateOfIssue));
        Optional.doWhen(TextUtil.isNotEmpty(this.userCNICDateOfExpiry),
                () -> this.iCnicInformationFragmentView.setTextToEtDateOfExpiryFromOcr(this.userCNICDateOfExpiry));
        Optional.doWhen(TextUtil.isNotEmpty(this.userCNICDateOfBirth),
                () -> this.iCnicInformationFragmentView.setTextToEtDobFromOcr(this.userCNICDateOfBirth));
    }

    @Override
    public void onSuccessUpdateProfileWithCnicCallback(final boolean isPartiallRequest, AnswerSelected answerSelected) {
        enableAllFormOptions(isPartiallRequest);
        this.userAnswerSelected = answerSelected;
        this.iCnicInformationFragmentView.setTezLoaderToBeDissmissedOnTransition();
        this.iCnicInformationFragmentView.routeToPersonalInformationScreen();
    }

    @Override
    public void onFailureUpdateProfileWithCnicCallback(int errorCode, String message) {
        this.iCnicInformationFragmentView.dismissTezLoader();
        this.iCnicInformationFragmentView.showError(errorCode);
    }
}
