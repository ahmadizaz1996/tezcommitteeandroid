package com.tez.androidapp.rewamp.advance.request.presenter;

public interface ICancelLimitActivityPresenter {

    void cancelLoanLimit(int loanId, int cancellationReasonId);
}
