package com.tez.androidapp.app.general.feature.wallet.my.wallet.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 12/26/2017.
 */

public class SetDefaultWalletRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/defaultWallet";
    Integer walletServiceProviderId;
    String mobileAccountNumber;

    public SetDefaultWalletRequest(Integer walletServiceProviderId, String mobileAccountNumber) {
        this.walletServiceProviderId = walletServiceProviderId;
        this.mobileAccountNumber = mobileAccountNumber;
    }

    public Integer getWalletServiceProviderId() {
        return walletServiceProviderId;
    }

    public void setWalletServiceProviderId(Integer walletServiceProviderId) {
        this.walletServiceProviderId = walletServiceProviderId;
    }

    public String getMobileAccountNumber() {
        return mobileAccountNumber;
    }

    public void setMobileAccountNumber(String mobileAccountNumber) {
        this.mobileAccountNumber = mobileAccountNumber;
    }
}
