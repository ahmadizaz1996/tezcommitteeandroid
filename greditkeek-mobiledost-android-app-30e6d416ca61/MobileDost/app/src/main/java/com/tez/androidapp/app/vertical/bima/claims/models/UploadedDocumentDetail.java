package com.tez.androidapp.app.vertical.bima.claims.models;

import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.DocumentFile;

import java.util.List;

/**
 * Created by FARHAN DHANANI on 7/2/2018.
 */
public class UploadedDocumentDetail {
    private final int id;
    private List<DocumentFile> filesList;
    private Integer claimDocumentId;

    public UploadedDocumentDetail(int id, List<DocumentFile> filesList) {
        this.id = id;
        this.filesList = filesList;
        this.claimDocumentId = null;
    }

    public UploadedDocumentDetail(int id, Integer claimDocumentId, List<DocumentFile> filesList) {
        this.id = id;
        this.filesList = filesList;
        this.claimDocumentId = claimDocumentId;
    }


    public Integer getClaimDocumentId() {
        return claimDocumentId;
    }


    public void setClaimDocumentId(Integer claimDocumentId) {
        this.claimDocumentId = claimDocumentId;
    }

    public int getId() {
        return id;
    }

    public List<DocumentFile> getFilesList() {
        return filesList;
    }
}
