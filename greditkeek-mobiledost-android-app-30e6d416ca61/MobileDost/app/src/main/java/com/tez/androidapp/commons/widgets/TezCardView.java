package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.tez.androidapp.R;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

/**
 * Created by VINOD KUMAR on 2/15/2019.
 */
public class TezCardView extends CardView implements IBaseWidget {

    public TezCardView(@NonNull Context context) {
        super(context);
    }

    public TezCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);

    }

    public TezCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        super.setOnClickListener(doubleTapSafeOnClickListener);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TezCardView);

        try {

            int clickEffect = typedArray.getInt(R.styleable.TezCardView_click_effect, SCALE_EFFECT);
            attachClickEffect(clickEffect, this);

        } finally {
            typedArray.recycle();
        }
    }
}
