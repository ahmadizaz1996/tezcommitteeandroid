package com.tez.androidapp.app.general.feature.find.agent.callbacks;

import com.tez.androidapp.app.general.feature.find.agent.models.network.dto.request.Agent;

import java.util.List;

/**
 * Created  on 3/7/2017.
 */

public interface FindAgentsCallback {

    void onFindAgentsSuccess(List<Agent> agents);

    void onFindAgentsError(int errorCode, String message);

}
