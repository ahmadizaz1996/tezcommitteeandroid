package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.RequestFailedActivity;

/**
 * Created by VINOD KUMAR on 8/5/2019.
 */
public class RequestFailedActivityRouter extends BaseActivityRouter {

    public static final String TITLE = "TITLE";
    public static final String MESSAGE = "MESSAGE";


    public static RequestFailedActivityRouter createInstance() {
        return new RequestFailedActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @StringRes int title,
                                        @StringRes int message) {
        Intent intent = createIntent(from);
        intent.putExtra(TITLE, title);
        intent.putExtra(MESSAGE, message);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, RequestFailedActivity.class);
    }
}
