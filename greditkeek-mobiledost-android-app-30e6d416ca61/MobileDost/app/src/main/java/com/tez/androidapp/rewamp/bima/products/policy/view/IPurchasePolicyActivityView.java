package com.tez.androidapp.rewamp.bima.products.policy.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.rewamp.bima.products.policy.adapters.InsuranceDuration;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;

import java.util.List;

public interface IPurchasePolicyActivityView extends IBaseView {
    void setClContentVisibility(int visibility);

    void setInsuranceDurations(@NonNull List<InsuranceDuration> insuranceDurations);

    void setUserWallet(@Nullable Wallet wallet);

    void setListeners();

    void routeToTermsAndCondition(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest, int walletServiceProviderId);

    void setDetails(@Nullable String data, double minCoverage, double maxCoverage);

    void setIssuanceDate(String date);

    void setActivationDate(String date);

    void setActivationDateVisibility(int visibility);

    void setTvExpiryDateValue(String date);

    void setTvInsuranceCompValue(String date);
}
