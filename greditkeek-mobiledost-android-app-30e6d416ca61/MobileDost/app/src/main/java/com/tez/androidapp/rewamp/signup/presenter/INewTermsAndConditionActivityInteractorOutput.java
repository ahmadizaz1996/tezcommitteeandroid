package com.tez.androidapp.rewamp.signup.presenter;

import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserLoginCallback;
import com.tez.androidapp.commons.models.network.dto.response.UserSignUpSetPinResponse;

public interface INewTermsAndConditionActivityInteractorOutput extends UserLoginCallback {

    void onUserSignUpSetPinSuccess(UserSignUpSetPinResponse userSignUpSetPinResponse, String pin);

    void onUserSignUpSetPinError(int errorCode, String message);
}
