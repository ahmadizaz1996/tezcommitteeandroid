package com.tez.androidapp.commons.fcm;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tez.androidapp.app.base.push.reciever.PushNotificationReciever;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.advance.loan.shared.push.receiver.AdvanceLoanFireBasePushReceiver;
import com.tez.androidapp.app.vertical.bima.shared.vertical.push.reciever.InsuranceFireBasePushReceiver;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.handlers.callbacks.UserDeviceKeyCallback;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.entity.DashboardCards;

import net.tez.fragment.util.optional.Optional;

import java.util.Map;

public class MobileDostFirebaseMessagingService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {

        Utility.logCrashlytics("push_received");

        Map<String, String> notificationMap = remoteMessage.getData();
        if (!notificationMap.isEmpty()) {
            PushNotificationReciever pushNotificationReciever;
            String verticalType = notificationMap.get(PushNotificationConstants.KEY_VERTICAL);

            if (verticalType != null && verticalType.equalsIgnoreCase(PushNotificationConstants.INSURANCE))
                pushNotificationReciever = new InsuranceFireBasePushReceiver(notificationMap);

            else
                pushNotificationReciever = new AdvanceLoanFireBasePushReceiver(notificationMap);

            Optional.ifPresent(notificationMap.get(PushNotificationConstants.KEY_CARDS), this::setDashboardCards);

            pushNotificationReciever.process();
        } else if (remoteMessage.getNotification() != null && remoteMessage.getNotification().getBody() != null) {
            Intent intent = new Intent();
            intent.putExtra(PushNotificationConstants.KEY_TITLE, remoteMessage.getNotification().getTitle());
            Utility.sendNotification(intent, remoteMessage.getNotification().getBody());
        }
    }

    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void setDashboardCards(@NonNull String cardsJson) {
        try {
            Utility.persistDashboardCards(new Gson().fromJson(cardsJson, DashboardCards.class));
        } catch (JsonSyntaxException e) {
            Crashlytics.logException(e);
        }
    }


    private void sendRegistrationToServer(final String token) {
        MDPreferenceManager.setFirebaseKey(token);
        MDPreferenceManager.setDeviceKeySentToServer(false);
        String principalName = MDPreferenceManager.getPrincipalName();
        if (principalName != null && token != null && Utility.doesUserExist()) {
            UserCloudDataStore userCloudDataStore = UserCloudDataStore.getInstance();
            userCloudDataStore.setUserDeviceKey(principalName, token, Utility.getImei(getApplicationContext()), new UserDeviceKeyCallback() {
                @Override
                public void onUserDeviceKeySuccess(BaseResponse baseResponse) {
                    if (baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
                        MDPreferenceManager.setDeviceKeySentToServer(true);
                    } else
                        onUserDeviceKeyFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
                }

                @Override
                public void onUserDeviceKeyFailure(int errorCode, String message) {
                    MDPreferenceManager.setDeviceKeySentToServer(false);
                }
            });
        }
    }
}
