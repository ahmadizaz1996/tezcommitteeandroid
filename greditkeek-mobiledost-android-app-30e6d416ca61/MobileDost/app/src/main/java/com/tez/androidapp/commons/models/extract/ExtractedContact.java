package com.tez.androidapp.commons.models.extract;

import java.io.Serializable;

import me.everything.providers.android.contacts.Contact;

/**
 * Created  on 12/8/2016.
 */

public class ExtractedContact implements Serializable {
    String displayName;
    String phone;
    boolean isSelected;
    boolean isHeader;

    public ExtractedContact(String displayName, String phone) {
        this.displayName = displayName;
        this.phone = phone;
    }

    public ExtractedContact(String displayName, boolean isHeader) {
        this.displayName = displayName;
        this.isHeader = isHeader;
    }

    public ExtractedContact(Contact contact) {
        this.displayName = contact.displayName;
        this.phone = contact.phone;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isHeader() {
        return isHeader;
    }
}
