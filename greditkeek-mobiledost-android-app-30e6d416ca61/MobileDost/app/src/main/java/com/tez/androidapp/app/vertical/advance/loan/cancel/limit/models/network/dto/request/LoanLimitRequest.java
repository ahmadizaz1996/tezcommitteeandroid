package com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.commons.models.network.DeviceInfo;

/**
 * Created  on 2/17/2017.
 */

public class LoanLimitRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/loan/limit";
    private DeviceInfo deviceInfo;
    private String JSESSIONID;

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getJSessionId() {
        return JSESSIONID;
    }

    public void setJSessionId(String jSessionId) {
        this.JSESSIONID = jSessionId;
    }
}
