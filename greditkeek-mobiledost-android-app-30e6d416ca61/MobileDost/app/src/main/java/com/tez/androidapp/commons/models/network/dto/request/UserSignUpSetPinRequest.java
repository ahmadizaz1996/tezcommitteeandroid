package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 6/12/2017.
 */

public class UserSignUpSetPinRequest extends BaseRequest {

    public static final String METHOD_NAME = "v2/user/setPin";

    private String pin;

    public UserSignUpSetPinRequest(String pin) {
        this.pin = pin;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
