package com.tez.androidapp.app.general.feature.authentication.signin.callbacks;

/**
 * Created  on 4/7/2017.
 */

public interface UserLogoutCallback {

    void onUserLogoutSuccess();

    void onUserLogoutFailure(int errorCode, String message);

}
