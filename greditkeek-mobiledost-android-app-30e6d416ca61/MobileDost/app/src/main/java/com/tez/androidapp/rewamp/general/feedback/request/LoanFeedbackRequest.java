package com.tez.androidapp.rewamp.general.feedback.request;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.request.BaseRequest;

public class LoanFeedbackRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/loan/feedback";

    @NonNull
    private Integer loanId;

    @NonNull
    private Integer rating;

    @Nullable
    private Integer cause;

    @Nullable
    private String comment;

    public LoanFeedbackRequest(@NonNull Integer loanId, @NonNull Integer rating, @Nullable Integer cause, @Nullable String comment) {
        this.loanId = loanId;
        this.rating = rating;
        this.cause = cause;
        this.comment = comment;
    }
}
