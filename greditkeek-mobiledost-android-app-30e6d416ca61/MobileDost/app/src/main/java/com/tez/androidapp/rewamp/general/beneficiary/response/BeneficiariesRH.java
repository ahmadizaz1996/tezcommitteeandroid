package com.tez.androidapp.rewamp.general.beneficiary.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.general.beneficiary.listener.GetBeneficiariesListener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

public class BeneficiariesRH extends BaseRH<BeneficiariesResponse> {

    private GetBeneficiariesListener listener;

    public BeneficiariesRH(BaseCloudDataStore baseCloudDataStore, GetBeneficiariesListener listener) {
        super(baseCloudDataStore);
        this.listener = listener;
    }

    @Override
    protected void onSuccess(Result<BeneficiariesResponse> value) {
        BeneficiariesResponse beneficiariesResponse = value.response().body();
        if (beneficiariesResponse != null && beneficiariesResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onGetBeneficiariesSuccess(beneficiariesResponse.getBeneficiaryList());
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onGetBeneficiariesFailure(errorCode, message);
    }
}
