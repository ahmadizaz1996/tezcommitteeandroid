package com.tez.androidapp.rewamp.signup;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.utils.app.PermissionCheckUtil;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezPinView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.signup.router.ContactUsActivityRouter;
import com.tez.androidapp.rewamp.signup.router.LoginNumberVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NewIntroActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NewLoginActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NewLoginPermissionsAcivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;

import java.util.List;

public class NewLoginActivity extends PlaybackActivity implements TezPinView.OnPinEnteredListener {


    @BindView(R.id.tvSignUp)
    private TezTextView tvSignUp;

    @BindView(R.id.tvForgetPin)
    private TezTextView tvForgetPin;

    @BindView(R.id.tvNumber)
    private TezTextView tvNumber;

    @BindView(R.id.pinView)
    protected TezPinView pinView;

    @Override
    public void onAppStart(boolean inactiveTimeExceeded) {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_login);
        init();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    private void handleIntent() {
        setTvNumberText(getMobileNumber());
        this.pinView.clearText();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    private void init() {
        initOnClickListeners();
        handleIntent();
    }

    private void setTvNumberText(String text) {
        this.tvNumber.setText(text);
    }

    private void initOnClickListeners() {
        this.tvSignUp.setDoubleTapSafeOnClickListener(view -> onClickSignup());
        this.tvForgetPin.setDoubleTapSafeOnClickListener(view -> onClickTvForgetPin());
        this.pinView.setOnPinEnteredListener(this);
    }

    @Override
    protected void performWorkAfterActivityTransition() {
        super.performWorkAfterActivityTransition();
        this.pinView.setEnabled(true);
    }

    @OnClick(R.id.tvEditNumber)
    private void startNewIntroActivity() {
        NewIntroActivityRouter.createInstance().setDependenciesAndRoute(this);
        finish();
    }


    private void onClickSignup() {
        SignupActivityRouter.createInstance().setDependenciesAndRoute(this,
                getMobileNumber(),
                getSocialId(),
                getSocialType(),
                getReferralCode());
        finish();
    }

    private String getReferralCode() {
        return getIntent().getStringExtra(NewLoginActivityRouter.REF_CODE);
    }

    private void onClickTvForgetPin() {
        ContactUsActivityRouter.createInstance().setDependenciesAndRoute(this);
    }


    private String getMobileNumber() {
        Intent intent = getIntent();
        return intent.hasExtra(NewLoginActivityRouter.MOBILE_NUMBER)
                ? intent.getStringExtra(NewLoginActivityRouter.MOBILE_NUMBER)
                : "";
    }

    @Override
    public void onPinEntered(@NonNull String pin) {
        this.pinView.clearText();
        List<String> missingPermissions =
                PermissionCheckUtil.getInstance(MobileDostApplication.getInstance())
                        .getMissingPermissionsForSignUp(MobileDostApplication.getSignupPermissions());
        Optional.doWhen(missingPermissions.isEmpty(),
                () -> this.startLogin(pin),
                () -> this.askPermission(pin));
    }

    protected void startLogin(String pin) {
        startLoginNumberVerificationActivity(getMobileNumber(), pin);
    }

    private String getSocialId() {
        return getIntent().getStringExtra(NewLoginActivityRouter.SOCIAL_ID);
    }

    private Integer getSocialType() {
        return getIntent().getIntExtra(NewLoginActivityRouter.SOCIAL_TYPE, Utility.PrincipalType.MOBILE_NUMBER.getValue());
    }


    private void startLoginNumberVerificationActivity(String mobileNumber, String pin){
        LoginNumberVerificationActivityRouter.createInstance().setDependenciesAndRoute(this,
                mobileNumber, pin, getSocialId(), getSocialType());
    }

    protected void askPermission( String pin) {
        NewLoginPermissionsAcivityRouter.createInstance().setDependenciesAndRoute(this,
                getMobileNumber(), pin, getSocialId(), getSocialType());
    }

    @Override
    protected String getScreenName() {
        return "NewLoginActivity";
    }
}
