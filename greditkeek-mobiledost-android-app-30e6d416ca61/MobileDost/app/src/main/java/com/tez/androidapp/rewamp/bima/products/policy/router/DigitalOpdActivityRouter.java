package com.tez.androidapp.rewamp.bima.products.policy.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.bima.products.policy.view.DigitalOpdActivity;

public class DigitalOpdActivityRouter extends BaseProductActivityRouter {

    public static DigitalOpdActivityRouter createInstance() {
        return new DigitalOpdActivityRouter();
    }

    @NonNull
    @Override
    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, DigitalOpdActivity.class);
    }
}
