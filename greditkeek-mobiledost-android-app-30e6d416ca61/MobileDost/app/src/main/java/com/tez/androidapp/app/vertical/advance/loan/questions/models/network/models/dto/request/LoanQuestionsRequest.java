package com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 2/17/2017.
 */

public class LoanQuestionsRequest extends BaseRequest {
    public static final String METHOD_NAME = "/question";

    private String sessionId;
    private String questionRefName;
    private String answer;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getQuestionRefName() {
        return questionRefName;
    }

    public void setQuestionRefName(String questionRefName) {
        this.questionRefName = questionRefName;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
