package com.tez.androidapp.app.vertical.bima.claims.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.bima.claims.callback.MyClaimsListCallBack;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.MyClaimListResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by FARHAN DHANANI on 6/23/2018.
 */
public class MyClaimListRH extends BaseRH<MyClaimListResponse> {

    private MyClaimsListCallBack myClaimsListCallBack;

    public MyClaimListRH(BaseCloudDataStore baseCloudDataStore,
                         @Nullable MyClaimsListCallBack myClaimsListCallBack) {
        super(baseCloudDataStore);
        this.myClaimsListCallBack = myClaimsListCallBack;
    }


    @Override
    protected void onSuccess(Result<MyClaimListResponse> value) {
        MyClaimListResponse myClaimListResponse = value.response().body();
        if (myClaimListResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            if (this.myClaimsListCallBack != null) this.myClaimsListCallBack.onMyClaimsListSuccess(myClaimListResponse.getClaims());
        } else onFailure(myClaimListResponse.getStatusCode(), myClaimListResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (this.myClaimsListCallBack != null) this.myClaimsListCallBack.onMyClaimsListFailure(errorCode, message);
    }
}
