package com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 12/28/2017.
 */

public class DeleteWalletRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/wallet/{" + Params.MOBILE_ACCOUNT_ID + "}";

    public static final class Params {
        public static final String MOBILE_ACCOUNT_ID = "mobileAccountId";

        private Params() {
        }
    }
}
