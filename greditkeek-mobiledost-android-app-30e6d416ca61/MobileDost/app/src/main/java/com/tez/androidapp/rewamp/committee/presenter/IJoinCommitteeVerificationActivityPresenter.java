package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.request.JoinCommitteeVerificationRequest;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface IJoinCommitteeVerificationActivityPresenter {

    void verifyCommitteeOtp(JoinCommitteeVerificationRequest joinCommitteeVerificationRequest);
}
