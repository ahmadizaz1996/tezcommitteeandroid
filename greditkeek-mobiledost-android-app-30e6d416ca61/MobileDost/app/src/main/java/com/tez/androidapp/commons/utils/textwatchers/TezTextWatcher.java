package com.tez.androidapp.commons.utils.textwatchers;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by VINOD KUMAR on 2/12/2019.
 */
public interface TezTextWatcher extends TextWatcher {

    @Override
    default void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    default void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    default void afterTextChanged(Editable s) {
    }
}
