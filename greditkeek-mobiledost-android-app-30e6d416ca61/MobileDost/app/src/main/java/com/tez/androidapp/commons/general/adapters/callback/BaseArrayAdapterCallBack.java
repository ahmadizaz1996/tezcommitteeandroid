package com.tez.androidapp.commons.general.adapters.callback;

/**
 * Created by FARHAN DHANANI on 8/15/2018.
 */
public interface BaseArrayAdapterCallBack {
    void onItemClickActions();
}
