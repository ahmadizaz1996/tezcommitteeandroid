package com.tez.androidapp.app.general.feature.wallet.shared.api.client;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.general.feature.find.agent.models.network.dto.request.GetNearestEasypaisaAgentsRequest;
import com.tez.androidapp.app.general.feature.find.agent.models.network.dto.response.GetNearestEasypaisaAgentsResponse;

import io.reactivex.Observable;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Rehman Murad Ali on 3/15/2018.
 */

public interface MobileDostWalletAPI {

    @POST(GetNearestEasypaisaAgentsRequest.METHOD_NAME)
    Observable<Result<GetNearestEasypaisaAgentsResponse>> getNearestAgents(
            @Header(GetNearestEasypaisaAgentsRequest.Params.APP_ID) int appId,
            @Header(GetNearestEasypaisaAgentsRequest.Params.APP_KEY) String appKey,
            @Header(GetNearestEasypaisaAgentsRequest.Params.MERCHANT_TYPE) String merchantType,
            @Header(GetNearestEasypaisaAgentsRequest.Params.LATITUDE) double latitude,
            @Header(GetNearestEasypaisaAgentsRequest.Params.LONGITUDE) double longitude,
            @Header(GetNearestEasypaisaAgentsRequest.Params.CONTENT_TYPE) String contentType
    );


}
