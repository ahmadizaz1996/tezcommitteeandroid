package com.tez.androidapp.app.general.feature.questions.handers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.questions.callbacks.QuestionsCallback;
import com.tez.androidapp.app.general.feature.questions.models.network.models.dto.response.QuestionsResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 2/17/2017.
 */

public class QuestionsRH extends BaseRH<QuestionsResponse> {

    private QuestionsCallback questionsCallback;

    public QuestionsRH(BaseCloudDataStore baseCloudDataStore, @Nullable QuestionsCallback
            questionsCallback) {
        super(baseCloudDataStore);
        this.questionsCallback = questionsCallback;
    }

    @Override
    protected void onSuccess(Result<QuestionsResponse> value) {
        QuestionsResponse questionsResponse = value.response().body();
        if (questionsResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            if (questionsCallback != null) questionsCallback.onQuestionsSuccess(value.response().body().getLoanQuestionDtos());
        } else onFailure(questionsResponse.getStatusCode(), questionsResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (questionsCallback != null) questionsCallback.onQuestionsFailure(errorCode, message);
    }
}
