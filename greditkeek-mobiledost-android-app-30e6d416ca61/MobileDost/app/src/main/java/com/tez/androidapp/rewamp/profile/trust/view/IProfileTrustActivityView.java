package com.tez.androidapp.rewamp.profile.trust.view;

import com.tez.androidapp.app.base.ui.IBaseView;

public interface IProfileTrustActivityView extends IBaseView {
    void routeToProfileCompletionCnicUploadActivity(boolean isCnicFrontPicToBeUploaded,
                                                    boolean isCnicBackPicToBeUploaded,
                                                    boolean isCnicSelfieToBeUploaded);

    void routeToCnicInformationActivity();
}
