package com.tez.androidapp.rewamp.committee.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeContributeListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeFilterListener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeFilterRH extends BaseRH<CommitteeFilterResponse> {

    private final CommitteeFilterListener listener;

    public CommitteeFilterRH(BaseCloudDataStore baseCloudDataStore, CommitteeFilterListener committeeFilterListener) {
        super(baseCloudDataStore);
        this.listener = committeeFilterListener;
    }

    @Override
    protected void onSuccess(Result<CommitteeFilterResponse> value) {
        CommitteeFilterResponse committeeFilterResponse = value.response().body();
        if (committeeFilterResponse != null && committeeFilterResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onGetFilterSuccess(committeeFilterResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onGetFilterFailure(errorCode, message);
    }
}
