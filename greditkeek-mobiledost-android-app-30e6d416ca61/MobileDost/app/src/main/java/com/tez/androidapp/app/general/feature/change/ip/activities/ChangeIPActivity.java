package com.tez.androidapp.app.general.feature.change.ip.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.managers.NativeKeysManager;
import com.tez.androidapp.commons.utils.app.AESEncryptionUtil;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.PinView;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.repository.network.store.DataLiftCloudDataStore;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;


public class ChangeIPActivity extends AppCompatActivity {

    private static final int FOUR_DIGIT_PIN = 4;

    @BindView(R.id.pinViewChangeIP)
    PinView pinViewChangeIP;

    @BindView(R.id.cardViewChangeIPPin)
    TezCardView cardViewChangeIPPin;

    @BindView(R.id.radioGroupChangeIP)
    RadioGroup radioGroupChangeIP;

    @BindView(R.id.editTextIP)
    TextInputEditText editTextIP;

    @BindView(R.id.buttonUpdateIP)
    TezButton buttonUpdateIP;

    @BindView(R.id.radioButtonPROD)
    RadioButton radioButtonPROD;

    private EditText editTextPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_ip);
        ViewBinder.bind(this);
        editTextPin = pinViewChangeIP.getEditTextPin();
        editTextPin.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        editTextPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                /*
                    not required to implement at the moment
                 */
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == FOUR_DIGIT_PIN) {
                    editTextPin.setOnEditorActionListener(null);
                    validatePin(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                /*
                    not required to implement at the moment
                 */
            }
        });
        radioGroupChangeIP.setOnCheckedChangeListener(
                (RadioGroup group, int checkedId) -> editTextIP.setVisibility(checkedId == R.id.radioButtonCustom ? View.VISIBLE : View.GONE)
        );

        buttonUpdateIP.setDoubleTapSafeOnClickListener(view -> {
            MobileDostApplication.getInstance().setProductionBuildChangeable(radioButtonPROD.isChecked());
            setBaseUrlForApp();
            Toast.makeText(this, "Base URL Updated", Toast.LENGTH_SHORT).show();
            finish();
        });
    }

    private void setBaseUrlForApp() {
        setBaseURL();
        initializeDataStores();
    }

    private void initializeDataStores() {
        LoanCloudDataStore.clear();
        BimaCloudDataStore.clear();
        DataLiftCloudDataStore.clear();
        UserAuthCloudDataStore.clear();
        UserCloudDataStore.clear();
        LoanCloudDataStore.getInstance();
        BimaCloudDataStore.getInstance();
        DataLiftCloudDataStore.getInstance();
        UserAuthCloudDataStore.getInstance();
        UserCloudDataStore.getInstance();
    }

    private void setBaseURL() {
        String url;
        switch (radioGroupChangeIP.getCheckedRadioButtonId()) {
            case R.id.radioButtonQA:
                url = Utility.getBaseURL("qa");
                break;
            case R.id.radioButtonDEV:
                url = Utility.getBaseURL("dev");
                break;
            case R.id.radioButtonUAT:
                url = Utility.getBaseURL("uat");
                break;
            case R.id.radioButtonPROD:
                url = Utility.getBaseURL("prod");
                break;
            case R.id.radioButtonCustom:
                url = "http://192.168." + editTextIP.getText().toString().trim() + ":8080/api/";
                //url = editTextIP.getText().toString().trim();
                break;
            default:
                url = Utility.getBaseURL("qa");
        }
        MDPreferenceManager.setBaseURL(url);
    }

    private void validatePin(String s) {
        if (s.equalsIgnoreCase(AESEncryptionUtil.getInstance().decrypt(NativeKeysManager.getInstance().getChangeIPCode()))) {
            Utility.hideKeyboard(this, getCurrentFocus());
            cardViewChangeIPPin.setVisibility(View.GONE);
            buttonUpdateIP.setVisibility(View.VISIBLE);
            radioGroupChangeIP.setVisibility(View.VISIBLE);
        } else
            finish();
    }
}
