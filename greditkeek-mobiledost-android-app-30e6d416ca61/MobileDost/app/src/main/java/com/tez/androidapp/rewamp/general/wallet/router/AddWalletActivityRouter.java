package com.tez.androidapp.rewamp.general.wallet.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.view.AddWalletActivity;

public class AddWalletActivityRouter extends BaseActivityRouter {

    public static final int REQUEST_CODE_ADD_WALLET = 19928;

    public static AddWalletActivityRouter createInstance() {
        return new AddWalletActivityRouter();
    }

    public void setDependenciesAndRouteForResult(@NonNull BaseActivity from) {
        routeForResult(from, createIntent(from), REQUEST_CODE_ADD_WALLET);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, AddWalletActivity.class);
    }
}
