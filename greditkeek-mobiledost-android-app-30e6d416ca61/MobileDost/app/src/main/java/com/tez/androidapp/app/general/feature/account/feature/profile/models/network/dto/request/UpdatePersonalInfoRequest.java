package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class UpdatePersonalInfoRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/profilePersonalInfo";

    public static String PERSONAL_INFO = "personalInfo";
}
