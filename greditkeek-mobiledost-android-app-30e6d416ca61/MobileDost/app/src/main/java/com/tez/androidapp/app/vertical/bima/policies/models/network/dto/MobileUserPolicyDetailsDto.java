package com.tez.androidapp.app.vertical.bima.policies.models.network.dto;

import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.BeneficiaryDetails;

import java.io.Serializable;
import java.util.List;

/**
 * Created by FARHAN DHANANI on 7/18/2018.
 */
public class MobileUserPolicyDetailsDto implements Serializable {
    private List<BeneficiaryDetails> beneficiaryDtoList;
    private String policyNumber;
    private String termsConditions;
    private boolean beneficiaryEditable;
    private Boolean amountSelectionRequired;

    public Boolean getAmountSelectionRequired() {
        return amountSelectionRequired;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public List<BeneficiaryDetails> getBeneficiaryDtoList() {
        return beneficiaryDtoList;
    }

    public void setBeneficiaryDtoList(List<BeneficiaryDetails> beneficiaryDtoList) {
        this.beneficiaryDtoList = beneficiaryDtoList;
    }

    public String getTermsConditions() {
        return termsConditions;
    }

    public boolean getBeneficiaryEditable() {
        return beneficiaryEditable;
    }

    public void setTermsConditions(String termsConditions) {
        this.termsConditions = termsConditions;
    }

    public void setAmountSelectionRequired(Boolean amountSelectionRequired) {
        this.amountSelectionRequired = amountSelectionRequired;
    }
}
