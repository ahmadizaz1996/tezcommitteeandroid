package com.tez.androidapp.app.vertical.advance.loan.confirmation.models.network.dto.response;

import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created  on 9/15/2017.
 */

public class GenerateDisbursementReceiptResponse extends BaseResponse {
    LoanDetails loanDetails;

    public LoanDetails getLoanDetails() {
        return loanDetails;
    }
}
