package com.tez.androidapp.rewamp.general.beneficiary.router;

import android.content.Intent;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.beneficiary.view.PolicyDetailActivity;

public class PolicyDetailActivityRouter extends BaseActivityRouter {

    public static final String POLICY = "POLICY";

    public static PolicyDetailActivityRouter createInstance() {
        return new PolicyDetailActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, @NonNull Policy policy) {
        Intent intent = createIntent(from);
        intent.putExtra(POLICY, (Parcelable) policy);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, PolicyDetailActivity.class);
    }
}