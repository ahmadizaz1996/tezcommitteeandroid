package com.tez.androidapp.rewamp.committee.view;

import android.content.Intent;
import android.os.Bundle;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;

import static com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter.COMMITTEE_DATA;


public class TezCommitteeActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee_create_f);
//        ViewBinder.bind(this);
    }



    @Override
    protected String getScreenName() {
        return "TezCommitteeActivity";
    }
}
