package com.tez.androidapp.rewamp.bima.products.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.rewamp.bima.products.callback.ProductListCallback;
import com.tez.androidapp.rewamp.bima.products.model.InsuranceProduct;
import com.tez.androidapp.rewamp.bima.products.response.ProductListResponse;
import com.tez.androidapp.rewamp.general.network.model.NetworkCall;
import com.tez.androidapp.rewamp.general.network.viewmodel.NetworkBoundViewModel;

import java.util.List;

public class ProductsViewModel extends NetworkBoundViewModel {

    private MutableLiveData<List<InsuranceProduct>> insuranceProductListLiveData;

    public MutableLiveData<List<InsuranceProduct>> getInsuranceProductListLiveData() {
        if (insuranceProductListLiveData == null) {
            insuranceProductListLiveData = new MutableLiveData<>();
            getProductsList();
        }
        return insuranceProductListLiveData;
    }

    private void getProductsList() {
        networkCallMutableLiveData.setValue(new NetworkCall(NetworkCall.State.LOADING));
        BimaCloudDataStore.getInstance().getInsuranceProducts(new ProductListCallback() {
            @Override
            public void onGetProductListSuccess(ProductListResponse productListResponse) {

                productListResponse.getProducts().add(new InsuranceProduct(Constants.TEZ_INSURANCE_PLAN_ID,
                        Utility.getStringFromResource(R.string.tez_insurance),
                        Utility.getStringFromResource(R.string.free_coverage_with_tez_loan),
                        true));

                insuranceProductListLiveData.setValue(productListResponse.getProducts());
                networkCallMutableLiveData.setValue(new NetworkCall(NetworkCall.State.SUCCESS));
            }

            @Override
            public void onGetProductListFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getProductsList();
                else {
                    networkCallMutableLiveData.setValue(new NetworkCall(NetworkCall.State.FAILURE, errorCode));
                }
            }
        });
    }
}
