package com.tez.androidapp.app.general.feature.wallet.add.wallet.handlers;


import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks.AddWalletCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 2/14/2017.
 */

public class AddWalletRH extends BaseRH<BaseResponse> {

    private AddWalletCallback addWalletCallback;

    public AddWalletRH(BaseCloudDataStore baseCloudDataStore, @Nullable AddWalletCallback addWalletCallback) {
        super(baseCloudDataStore);
        this.addWalletCallback = addWalletCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (isErrorFree(baseResponse)) {
            addWalletCallback.onAddWalletSuccess(baseResponse);
        } else onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());

    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (addWalletCallback != null) addWalletCallback.onAddWalletFailure(errorCode, message);
    }
}
