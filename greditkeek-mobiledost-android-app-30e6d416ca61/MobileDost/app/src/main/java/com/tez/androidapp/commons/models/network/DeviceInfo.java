package com.tez.androidapp.commons.models.network;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.tez.androidapp.commons.utils.app.Constants;

import java.io.Serializable;


/**
 * Created by Rehman Murad Ali on 4/12/2018.
 */
public class DeviceInfo implements Serializable, Parcelable {
    private String imei;
    private String deviceBrand;
    private String deviceModel;
    private String networkOperator = Constants.NETWORK_NO_SIM;
    private String deviceOs;
    private String defaultBrowser;

    public DeviceInfo() {
    }

    protected DeviceInfo(Parcel in) {
        imei = in.readString();
        deviceBrand = in.readString();
        deviceModel = in.readString();
        networkOperator = in.readString();
        deviceOs = in.readString();
        defaultBrowser = in.readString();
    }

    public static final Creator<DeviceInfo> CREATOR = new Creator<DeviceInfo>() {
        @Override
        public DeviceInfo createFromParcel(Parcel in) {
            return new DeviceInfo(in);
        }

        @Override
        public DeviceInfo[] newArray(int size) {
            return new DeviceInfo[size];
        }
    };

    public String getDefaultBrowser() {
        return defaultBrowser;
    }

    public void setDefaultBrowser(String defaultBrowser) {
        this.defaultBrowser = defaultBrowser;
    }


    public String getDeviceBrand() {
        return deviceBrand;
    }

    public void setDeviceBrand(String deviceBrand) {
        this.deviceBrand = deviceBrand;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getNetworkOperator() {
        return networkOperator;
    }

    public void setNetworkOperator(String networkOperator) {
        if (!TextUtils.isEmpty(networkOperator))
            this.networkOperator = networkOperator;
    }

    public String getDeviceOs() {
        return deviceOs;
    }

    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imei);
        dest.writeString(deviceBrand);
        dest.writeString(deviceModel);
        dest.writeString(networkOperator);
        dest.writeString(deviceOs);
        dest.writeString(defaultBrowser);
    }
}
