package com.tez.androidapp.rewamp.committee.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Log;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezCheckBox;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponseMemberList;
import com.tez.androidapp.rewamp.committee.response.CommitteeResponseFilterUpdates;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/

public class MyCommitteeUpdatesFilterAdapter extends GenericRecyclerViewAdapter<CommitteeFilterResponseMemberList,
        MyCommitteeUpdatesFilterAdapter.MyCommitteeFilterMembersListener,
        MyCommitteeUpdatesFilterAdapter.MyCommitteeMemberViewHolder> {

    public MyCommitteeUpdatesFilterAdapter(@NonNull ArrayList<CommitteeFilterResponseMemberList> items, @Nullable MyCommitteeFilterMembersListener listener) {
        super(items, listener);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public MyCommitteeMemberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.simple_single_string_list_item, parent);
        return new MyCommitteeMemberViewHolder(view);
    }

    public interface MyCommitteeFilterMembersListener extends BaseRecyclerViewListener {
        void onCommitteeFilterMemberClicked(CommitteeFilterResponseMemberList committeeFilterResponseMemberList, boolean isChecked, CompoundButton button);
    }

    static class MyCommitteeMemberViewHolder extends BaseViewHolder<CommitteeFilterResponseMemberList, MyCommitteeFilterMembersListener> {

        @BindView(R.id.itemTextView)
        private TezTextView itemTextView;

        @BindView(R.id.cbWalletCheck)
        private TezCheckBox checkBox;


        public MyCommitteeMemberViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(CommitteeFilterResponseMemberList item, @Nullable MyCommitteeFilterMembersListener listener) {
            super.onBind(item, listener);

            Log.d("LLLLL ", item.getUsername());
            itemTextView.setText(item.getUsername());

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    checkBox.setOnCheckedChangeListener(null);
                    listener.onCommitteeFilterMemberClicked(item, isChecked,buttonView);
                    checkBox.setOnCheckedChangeListener(this::onCheckedChanged);
                }
            });

        }
    }
}
