package com.tez.androidapp.rewamp.advance.request.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.advance.request.entity.ProcessingFee;

import java.util.List;

public class ProcessingFeesResponse extends BaseResponse {

    private List<ProcessingFee> processingFees;

    public List<ProcessingFee> getProcessingFees() {
        return this.processingFees;
    }
}
