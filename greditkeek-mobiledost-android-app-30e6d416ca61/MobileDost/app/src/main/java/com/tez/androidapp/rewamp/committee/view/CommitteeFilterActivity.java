package com.tez.androidapp.rewamp.committee.view;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.transactions.models.network.Transaction;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.committee.adapter.MyCommitteeUpdatesDatesFilterAdapter;
import com.tez.androidapp.rewamp.committee.adapter.MyCommitteeUpdatesFilterAdapter;
import com.tez.androidapp.rewamp.committee.adapter.MyCommitteeUpdatesTransactionFilterAdapter;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponseInviteList;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterResponseMemberList;
import com.tez.androidapp.rewamp.committee.response.CommitteeResponseFilterTransactionList;
import com.tez.androidapp.rewamp.committee.response.CommitteeResponseTimeline;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;
import com.tez.androidapp.rewamp.committee.router.CommitteeUpdateActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.tez.androidapp.rewamp.committee.router.CommitteeFilterActivityRouter.HASH_MAP;
import static com.tez.androidapp.rewamp.committee.router.CommitteeFilterActivityRouter.MEMBERS_LIST;

public class CommitteeFilterActivity extends BaseActivity implements IBaseView, MyCommitteeUpdatesTransactionFilterAdapter.CommitteeFilterTransactionListener, MyCommitteeUpdatesDatesFilterAdapter.CommitteeFilterDatesListener, MyCommitteeUpdatesFilterAdapter.MyCommitteeFilterMembersListener, ICommitteeFilterActivityView, DoubleTapSafeOnClickListener {


    @BindView(R.id.transactionRecycleView)
    RecyclerView transactionRecycleView;

    @BindView(R.id.MembersRecyclerView)
    RecyclerView MembersRecyclerView;

    @BindView(R.id.dateRecyclerView)
    RecyclerView dateRecyclerView;

    @BindView(R.id.filter)
    ImageButton filter;

    @BindView(R.id.btnFilter)
    TezButton btnFilter;


    ArrayList<String> transactionsList = new ArrayList<>();
    ArrayList<CommitteeResponseFilterTransactionList> finalTransactionsList = new ArrayList<>();
    ArrayList<CommitteeFilterResponseMemberList> finalMemberList = new ArrayList<>();
    ArrayList<CommitteeResponseTimeline> finalDatesList = new ArrayList<>();
    ArrayList<CommitteeResponseTimeline> finalCommitteeResponseTimelineList = new ArrayList<>();
    ArrayList<String> datesList = new ArrayList<>();
    ArrayList<CommitteeFilterResponseMemberList> memberLists = new ArrayList<>();
    ArrayList<String> memberListNames = new ArrayList<>();
    private ArrayList<CommitteeResponseTimeline> memberList = new ArrayList<>();
    private HashMap<String, List<CommitteeResponseTimeline>> hashMap = new HashMap<>();

//    private final CommitteeFilterActivityPresenter committeeFilterPresenter;

 /*   public CommitteeFilterActivity() {
        committeeFilterPresenter = new CommitteeFilterActivityPresenter(this);
    }*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee_filter);
        ViewBinder.bind(this);

        ArrayList<String> transactionList = new ArrayList<>();
        transactionList.add(Transactions.ALL.getText());
        transactionList.add(Transactions.CONTRIBUTIONS_ONLY.getText());
        transactionList.add(Transactions.RECEIVING_ONLY.getText());

        ArrayList<String> datesList = new ArrayList<>();
        datesList.add(Date.ALL.getText());
        datesList.add(Date.TODAY.getText());
        datesList.add(Date.LAST_7_DAYS.getText());
        datesList.add(Date.ONE_MONTH.getText());
        datesList.add(Date.THREE_MONTHS.getText());

        if (getIntent() != null) {
            memberList = (ArrayList<CommitteeResponseTimeline>) getIntent().getExtras().getSerializable(MEMBERS_LIST);
            hashMap = (HashMap<String, List<CommitteeResponseTimeline>>) getIntent().getExtras().getSerializable(HASH_MAP);
        }

        /*transactionList.clear();
        datesList.clear();
        memberLists.clear();*/

        btnFilter.setButtonInactive();
//        committeeFilterPresenter.getCommitteeFilter("150");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        MyCommitteeUpdatesTransactionFilterAdapter myCommitteeUpdatesTransactionFilterAdapter = new MyCommitteeUpdatesTransactionFilterAdapter(transactionList, this);
        this.transactionRecycleView.setLayoutManager(linearLayoutManager);
        transactionRecycleView.setAdapter(myCommitteeUpdatesTransactionFilterAdapter);
        MyCommitteeUpdatesDatesFilterAdapter myCommitteeUpdatesDatesFilterAdapter = new MyCommitteeUpdatesDatesFilterAdapter(datesList, this);
        LinearLayoutManager _linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        this.dateRecyclerView.setLayoutManager(_linearLayoutManager);
        dateRecyclerView.setAdapter(myCommitteeUpdatesDatesFilterAdapter);

        List<CommitteeFilterResponseMemberList> committeeResponseTimelines = new ArrayList<>();
        for (CommitteeResponseTimeline list : memberList) {
            committeeResponseTimelines.addAll(list.getmMemberList());
        }
        ArrayList<CommitteeFilterResponseMemberList> committeeFilterResponseMemberLists = new ArrayList<>();
        committeeFilterResponseMemberLists.addAll(committeeResponseTimelines);
        MyCommitteeUpdatesFilterAdapter myCommitteeUpdatesFilterAdapter = new MyCommitteeUpdatesFilterAdapter(committeeFilterResponseMemberLists, this);
        LinearLayoutManager linearLayoutManagers = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        this.MembersRecyclerView.setLayoutManager(linearLayoutManagers);
        MembersRecyclerView.setAdapter(myCommitteeUpdatesFilterAdapter);
        btnFilter.setButtonNormal();

        btnFilter.setDoubleTapSafeOnClickListener(this);


    }

    @Override
    protected String getScreenName() {
        return null;
    }

    @Override
    public void onSelectedTransactionFilter(String item, boolean isChecked, CompoundButton button) {
        if (isChecked) {
            transactionsList.add(item);
        } else {
            transactionsList.remove(item);
        }
    }

    @Override
    public void onSelectedDatesFilter(String item, boolean isChecked, CompoundButton button) {
        if (isChecked) {
            datesList.add(item);
        } else {
            datesList.remove(item);
        }
    }

    @Override
    public void onCommitteeFilterMemberClicked(CommitteeFilterResponseMemberList committeeFilterResponseMemberList, boolean isChecked, CompoundButton button) {
        if (isChecked) {
            memberLists.add(committeeFilterResponseMemberList);
            memberListNames.add(committeeFilterResponseMemberList.getUsername());
        } else {
            memberLists.remove(committeeFilterResponseMemberList);
            memberListNames.remove(committeeFilterResponseMemberList.getUsername());
        }
    }

    @Override
    public void showLoader() {
        showTezLoader();
    }

    @Override
    public void hideLoader() {
        dismissTezLoader();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onFilter(CommitteeFilterResponse committeeFilterResponse) {
       /* MyCommitteeUpdatesFilterAdapter myCommitteeUpdatesFilterAdapter = new MyCommitteeUpdatesFilterAdapter(committeeFilterResponse.getUpdates().getTimeline(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        this.MembersRecyclerView.setLayoutManager(linearLayoutManager);
        MembersRecyclerView.setAdapter(myCommitteeUpdatesFilterAdapter);
        btnFilter.setButtonNormal();*/
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.btnFilter:
                    applyFilter();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void applyFilter() {
        try {
            if (transactionsList != null && transactionsList.size() > 0) {
                for (String type : transactionsList) {
                    if (type.equalsIgnoreCase(Transactions.ALL.text)) {
                        for (CommitteeResponseTimeline committeeResponseTimeline : memberList) {
                            finalTransactionsList.addAll(committeeResponseTimeline.getmTransactionList());
                        }
                        break;
                    } else if (type.equalsIgnoreCase(Transactions.CONTRIBUTIONS_ONLY.text)) {
                        for (CommitteeResponseTimeline committeeResponseTimeline : memberList) {
                            for (CommitteeResponseFilterTransactionList transactionList : committeeResponseTimeline.getmTransactionList()) {
                                if (transactionList.getMtype().equalsIgnoreCase("CONTRIBUTION")) {
                                    finalTransactionsList.add(transactionList);
                                }
                            }
                        }
                    } else {
                        for (CommitteeResponseTimeline committeeResponseTimeline : memberList) {
                            for (CommitteeResponseFilterTransactionList transactionList : committeeResponseTimeline.getmTransactionList()) {
                                if (transactionList.getMtype().equalsIgnoreCase("RECEIVING")) {
                                    finalTransactionsList.add(transactionList);
                                }
                            }
                        }
                    }
                }
            }


            if (memberListNames != null && memberListNames.size() > 0) {
                for (String name : memberListNames) {
                    for (CommitteeResponseTimeline committeeResponseTimeline : memberList) {
                        for (CommitteeResponseFilterTransactionList transactionList : committeeResponseTimeline.getmTransactionList()) {
                            if (transactionList.getUsername().equalsIgnoreCase(name)) {
                                finalTransactionsList.add(transactionList);
                            }
                        }

                        for (CommitteeFilterResponseMemberList memberList : committeeResponseTimeline.getmMemberList()) {
                            if (memberList.getUsername().equalsIgnoreCase(name)) {
                                finalMemberList.add(memberList);
                            }
                        }
                    }
                }
            }

            if (datesList != null && datesList.size() > 0) {
                for (String dates : datesList) {
                    for (CommitteeResponseTimeline committeeResponseTimeline : memberList) {
                        if (dates.contains(Transactions.ALL.text)) {
                            finalDatesList.addAll(memberList);
                            break;
                        } else if (dates.equalsIgnoreCase(Date.ONE_MONTH.text)) {
                            finalDatesList.addAll(hashMap.get("1 month ago".trim()));
                        } else if (dates.equalsIgnoreCase(Date.THREE_MONTHS.text)) {
                            finalDatesList.addAll(hashMap.get("3 months ago".trim()));
                        } else if (dates.equalsIgnoreCase(Date.LAST_7_DAYS.text)) {
                            finalDatesList.addAll(hashMap.get("7 days ago"));
                        }
                    }
                }
            }


            if (finalDatesList != null && finalDatesList.size() > 0) {
                finalCommitteeResponseTimelineList.addAll(finalDatesList);
            }

            if (finalTransactionsList != null && finalTransactionsList.size() > 0) {
                CommitteeResponseTimeline committeeResponseTimeline = new CommitteeResponseTimeline();
                committeeResponseTimeline.setmTransactionList(finalTransactionsList);

                finalCommitteeResponseTimelineList.add(committeeResponseTimeline);
            }


            if (finalMemberList != null && finalMemberList.size() > 0) {
                CommitteeResponseTimeline _committeeResponseTimeline = new CommitteeResponseTimeline();
                _committeeResponseTimeline.setmMemberList(finalMemberList);

                finalCommitteeResponseTimelineList.add(_committeeResponseTimeline);
            }

//            for (CommitteeResponseFilterTransactionList committeeResponseFilterTransactionList : finalTransactionsList) {
//                CommitteeResponseTimeline committeeResponseTimeline = new CommitteeResponseTimeline();
//                committeeResponseTimeline.setmTransactionList(committeeResponseFilterTransactionList);
//
//                finalCommitteeResponseTimelineList.add(committeeResponseTimeline);
//            }

            /*for (CommitteeFilterResponseMemberList committeeFilterResponseMemberList : finalMemberList) {
                CommitteeResponseTimeline committeeResponseTimeline = new CommitteeResponseTimeline();
                committeeResponseTimeline.setmTransactionList(committeeFilterResponseMemberList);

                finalCommitteeResponseTimelineList.add(committeeResponseTimeline);
            }*/


            CommitteeUpdateActivityRouter.createInstance().setDependenciesAndRoute(this, finalCommitteeResponseTimelineList, null);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public enum Transactions {
        ALL("All"),
        CONTRIBUTIONS_ONLY("Contributions Only"),
        RECEIVING_ONLY("Receiving Only");

        public String text;

        Transactions(String s) {
            text = s;
        }

        public String getText() {
            return text;
        }
    }

    public enum Date {
        ALL("All"),
        TODAY("Today"),
        LAST_7_DAYS("Last 7 days"),
        ONE_MONTH("1 Month"),
        THREE_MONTHS("3 Months");

        public String text;

        Date(String s) {
            text = s;
        }

        public String getText() {
            return text;
        }
    }

}