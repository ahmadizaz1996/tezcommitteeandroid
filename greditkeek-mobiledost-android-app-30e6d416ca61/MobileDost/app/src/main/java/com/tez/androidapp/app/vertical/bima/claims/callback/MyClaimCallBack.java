package com.tez.androidapp.app.vertical.bima.claims.callback;

import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.ClaimDetailDto;

/**
 * Created by FARHAN DHANANI on 6/29/2018.
 */
public interface MyClaimCallBack {

    void onMyClaimSuccess(ClaimDetailDto claimDetailDto);

    void onMyClaimFailure(int errorCode, String message);
}
