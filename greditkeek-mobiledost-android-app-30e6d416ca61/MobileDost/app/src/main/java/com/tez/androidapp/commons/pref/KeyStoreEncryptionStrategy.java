package com.tez.androidapp.commons.pref;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.ParcelFormatException;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import androidx.annotation.NonNull;
import android.util.Base64;

import com.crashlytics.android.Crashlytics;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.utils.app.Utility;

import net.tez.storage.library.strategy.TextTransformStrategy;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import static java.security.KeyStore.getDefaultType;
import static java.security.KeyStore.getInstance;

/**
 * Created by FARHAN DHANANI on 1/18/2019.
 */
public class KeyStoreEncryptionStrategy implements TextTransformStrategy {

    private static final String KEYSTORE = "keystore";
    private static final String AES_CBC_PKCS7PADDING = "AES/CBC/PKCS7Padding";
    private static final String UTF_8 = "UTF-8";
    private static final String IV_SEPARATOR = "]";
    private static final String PROVIDER_ANDROID_KEY_STORE = "AndroidKeyStore";
    private static final String ALGORITHM = "AES";
    private static final int KEY_SIZE = 256;
    private final File mKeystoreFile;
    private final SecretKey secretKey;
    private KeyStore keyStoreDefault;
    private KeyStore keyStoreAndroid;

    public KeyStoreEncryptionStrategy() {
        mKeystoreFile = new File(MobileDostApplication.getInstance().getFilesDir(), KEYSTORE);
        String alias = "com.tez.androidapp";
        char[] keyStorePassword = "com.yakivmospan.scytale".toCharArray();
        if (!hasKey(alias, keyStorePassword)) {
            secretKey = generateSymmetricKey(alias, keyStorePassword);
        } else {
            secretKey = getSymmetricKey(alias, keyStorePassword);
        }
    }

    private SecretKey getSymmetricKey(@NonNull String alias, char[] password) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return getSymmetricKeyFromDefaultKeyStore(alias, password);
        } else {
            return getSymmetricKeyFromAndroidtKeyStore(alias);
        }
    }

    private SecretKey getSymmetricKeyFromDefaultKeyStore(@NonNull String alias, char[] password) {
        SecretKey result = null;
        try {
            KeyStore keyStore = createDefaultKeyStore(password);
            result = (SecretKey) keyStore.getKey(alias, null);
        } catch (KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException | UnrecoverableEntryException e) {
            Crashlytics.logException(e);
        }
        return result;
    }

    private SecretKey getSymmetricKeyFromAndroidtKeyStore(@NonNull String alias) {
        SecretKey result = null;
        try {
            KeyStore keyStore = createAndroidKeystore();
            result = (SecretKey) keyStore.getKey(alias, null);
        } catch (KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException | UnrecoverableEntryException | ParcelFormatException e) {
            Crashlytics.logException(e);
        }
        return result;
    }

    private boolean hasKey(@NonNull String alias, char[] password) {
        boolean result = false;
        try {
            KeyStore keyStore;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
                keyStore = createDefaultKeyStore(password);
                result = isKeyEntry(alias, keyStore);
            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                keyStore = createAndroidKeystore();
                result = isKeyEntry(alias, keyStore);
                if (!result) {
                    keyStore = createDefaultKeyStore(password);
                    result = isKeyEntry(alias, keyStore);
                }
            } else {
                keyStore = createAndroidKeystore();
                result = isKeyEntry(alias, keyStore);
            }

        } catch (KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException | RuntimeException e) {
            Crashlytics.logException(e);
        }
        return result;
    }

    private KeyStore createAndroidKeystore()
            throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        if (this.keyStoreAndroid == null) {
            this.keyStoreAndroid = KeyStore.getInstance(PROVIDER_ANDROID_KEY_STORE);
        }
        this.keyStoreAndroid.load(null);
        return this.keyStoreAndroid;
    }

    private boolean isKeyEntry(@NonNull String alias, KeyStore keyStore) throws KeyStoreException {
        return keyStore != null && keyStore.isKeyEntry(alias);
    }

    @NonNull
    @Override
    public String transformOnPut(@NonNull String value) {
        return encrypt(value);
    }

    @Override
    public String transformOnGet(@NonNull String value) {
        return Utility.isEmpty(value) ? value : decrypt(value);
    }

    private String encrypt(@NonNull String data) {
        String result = "";
        try {
            Cipher cipher = Cipher.getInstance(AES_CBC_PKCS7PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            byte[] iv = cipher.getIV();
            String ivString = Base64.encodeToString(iv, Base64.DEFAULT);
            result = ivString + IV_SEPARATOR;

            byte[] plainData = data.getBytes(UTF_8);
            byte[] decodedData;
            decodedData = decode(cipher, plainData);
            String encodedString = Base64.encodeToString(decodedData, Base64.DEFAULT);
            result += encodedString;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IOException | RuntimeException e) {
            Crashlytics.logException(e);
        }
        return result;
    }

    private String decrypt(@NonNull String data) {
        String result = null;
        try {
            Cipher cipher = Cipher.getInstance(AES_CBC_PKCS7PADDING);

            String encodedString;
            String[] split = data.split(IV_SEPARATOR);
            String ivString = split[0];
            encodedString = split[1];
            IvParameterSpec ivSpec = new IvParameterSpec(Base64.decode(ivString, Base64.DEFAULT));
            cipher.init(Cipher.DECRYPT_MODE, secretKey, ivSpec);

            byte[] decodedData;
            byte[] encryptedData = Base64.decode(encodedString, Base64.DEFAULT);
            decodedData = decode(cipher, encryptedData);
            result = new String(decodedData, UTF_8);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IOException | InvalidAlgorithmParameterException| RuntimeException e) {
            Crashlytics.logException(e);
        }
        return result;
    }

    private byte[] decode(@NonNull Cipher cipher, @NonNull byte[] plainData)
            throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        CipherOutputStream cipherOutputStream = new CipherOutputStream(baos, cipher);
        cipherOutputStream.write(plainData);
        cipherOutputStream.close();
        return baos.toByteArray();
    }


    private SecretKey generateSymmetricKey(final String alias, final char[] keyStorePassword) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return generateDefaultSymmetricKey(alias, keyStorePassword);
        } else {
            return generateAndroidSymmetricKey(alias);
        }
    }

    private SecretKey generateDefaultSymmetricKey(String alias, char[] keyStorePassword) {
        try {
            final char[] password = null;
            SecretKey key = createSymmetricKey();
            KeyStore.SecretKeyEntry keyEntry = new KeyStore.SecretKeyEntry(key);
            KeyStore keyStore = createDefaultKeyStore(keyStorePassword);

            keyStore.setEntry(alias, keyEntry, new KeyStore.PasswordProtection(password));
            keyStore.store(new FileOutputStream(mKeystoreFile), keyStorePassword);
            return key;
        } catch (NoSuchAlgorithmException | CertificateException | KeyStoreException | IOException e) {
            Crashlytics.logException(e);
        }
        return null;
    }

    private SecretKey createSymmetricKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM);
        keyGenerator.init(KEY_SIZE);
        return keyGenerator.generateKey();
    }

    private KeyStore createDefaultKeyStore(char[] password)
            throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException {
        if (this.keyStoreDefault == null) {
            this.keyStoreDefault = getInstance(getDefaultType());
            if (mKeystoreFile.exists()) {
                this.keyStoreDefault.load(new FileInputStream(mKeystoreFile), password);
            } else {
                this.keyStoreDefault.load(null);
            }
        }
        return this.keyStoreDefault;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private SecretKey generateAndroidSymmetricKey(String alias) {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM, PROVIDER_ANDROID_KEY_STORE);
            KeyGenParameterSpec keySpec = keyPropsToKeyGenParameterSSpec(alias);
            keyGenerator.init(keySpec);
            return keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException | NullPointerException e) {
            Crashlytics.logException(e);
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private KeyGenParameterSpec keyPropsToKeyGenParameterSSpec(String alias) {
        int purposes = KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT;
        return new KeyGenParameterSpec.Builder(alias, purposes)
                .setKeySize(KEY_SIZE)
                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                .build();
    }
}
