package com.tez.androidapp.rewamp.bima.products.dialog;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezDialog;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.AppOpener;
import com.tez.androidapp.rewamp.bima.products.adapter.CallNumbersAdapter;

import java.util.Arrays;

public class SelectNumberDialog extends TezDialog implements CallNumbersAdapter.CallNumberListener {

    private String[] numbersArray;

    private BaseActivity baseActivity;

    public SelectNumberDialog(@NonNull Context context, @NonNull BaseActivity baseActivity) {
        super(context);
        this.baseActivity = baseActivity;
    }

    public void setNumbersArray(String[] numbersArray) {
        this.numbersArray = numbersArray;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_number_dialog);
        TezTextView tvCancel = findViewById(R.id.tvCancel);
        tvCancel.setDoubleTapSafeOnClickListener(v -> dismiss());

        RecyclerView rvNumbers = findViewById(R.id.rvNumbers);
        CallNumbersAdapter adapter = new CallNumbersAdapter(Arrays.asList(numbersArray), this);
        rvNumbers.setAdapter(adapter);
    }

    @Override
    public void onClickNumber(@NonNull String number) {
        AppOpener.openDialer(baseActivity, number.trim());
        dismiss();
    }
}
