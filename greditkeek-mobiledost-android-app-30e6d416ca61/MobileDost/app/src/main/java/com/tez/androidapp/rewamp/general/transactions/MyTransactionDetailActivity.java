package com.tez.androidapp.rewamp.general.transactions;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.transactions.models.network.Transaction;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.repay.router.RepayLoanActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class MyTransactionDetailActivity extends BaseActivity implements IMyTransactionDetailActivityView {

    private final IMyTransactionDetailActivityPresenter iMyTransactionDetailActivityPresenter;

    @BindView(R.id.tvAmount)
    private TezTextView tvAmount;

    @BindView(R.id.tvPaymentStatus)
    private TezTextView tvPaymentStatus;

    @BindView(R.id.tvRemainingAmountText)
    private TezTextView tvRemainingAmountText;

    @BindView(R.id.tvRemainingAmount)
    private TezTextView tvRemainingAmount;

    @BindView(R.id.btRepayNow)
    private TezButton btRepayNow;

    @BindView(R.id.rvMyTransactionDetails)
    private RecyclerView rvMyTransactionDetails;

    @BindView(R.id.layoutEmptyTransactionDetail)
    private TezConstraintLayout layoutEmptyTransactionDetail;

    @BindView(R.id.tvEmptyTransactionDetail)
    private TezTextView tvEmptyTransactionDetail;

    public MyTransactionDetailActivity() {
        iMyTransactionDetailActivityPresenter = new MyTransactionDetailActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_transaction_detail);
        ViewBinder.bind(this);
        Transaction transaction = getTransactionFromIntent();
        if (transaction != null) {
            setTransactionStatus(transaction);
            iMyTransactionDetailActivityPresenter.getLoanTransactionDetail(transaction.getId());
        } else
            finish();
    }

    @Nullable
    private Transaction getTransactionFromIntent() {
        return (Transaction) getIntent().getSerializableExtra(MyTransactionDetailActivityRouter.TRANSACTION);
    }

    private void setTransactionStatus(@NonNull Transaction transaction) {

        tvAmount.setText(getString(R.string.rs_value_string, transaction.getStringAmount()));
        tvPaymentStatus.setText(transaction.getPaymentStatus());
        tvPaymentStatus.setTextColor(Utility.getColorFromResource(transaction.isCleared() ? R.color.textViewTextColorGreen : R.color.stateRejectedColorRed));
        int visibility = transaction.isCleared() ? View.GONE : View.VISIBLE;
        btRepayNow.setVisibility(visibility);
        btRepayNow.setDoubleTapSafeOnClickListener(transaction.isCleared() ? null : view -> RepayLoanActivityRouter.createInstance().setDependenciesAndRoute(this, transaction.getId()));
        tvRemainingAmount.setVisibility(View.GONE);
        tvRemainingAmountText.setVisibility(View.GONE);
    }

    @Override
    public void setTransactionSummaryError() {
        String error = getString(R.string.error) + ": " + getString(R.string.transaction_details_are_not_available);
        tvEmptyTransactionDetail.setText(error);
        setLayoutEmptyTransactionDetailVisibility(View.VISIBLE);
    }

    @Override
    public void setLayoutEmptyTransactionDetailVisibility(int visibility) {
        layoutEmptyTransactionDetail.setVisibility(visibility);
    }

    @Override
    public void setTvRemainingAmountText(String text) {
        tvRemainingAmount.setText(getString(R.string.rs_value_string, text));
    }

    @Override
    public void setTvRemainingAmountVisibility() {
        Transaction transaction = getTransactionFromIntent();
        if (transaction != null) {
            int visibility = transaction.isCleared() ? View.GONE : View.VISIBLE;
            tvRemainingAmountText.setVisibility(visibility);
            tvRemainingAmount.setVisibility(visibility);
        }
    }

    @Override
    public void setRvMyTransactionDetailsVisibility(int visibility) {
        rvMyTransactionDetails.setVisibility(visibility);
    }

    @Override
    public void setMyTransactionDetailAdapter(@NonNull List<MyTransactionDetail> myTransactionDetailList) {
        MyTransactionDetailAdapter adapter = new MyTransactionDetailAdapter(myTransactionDetailList);
        rvMyTransactionDetails.setLayoutManager(new LinearLayoutManager(this));
        rvMyTransactionDetails.setAdapter(adapter);
    }

    @Override
    public void showTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.GONE);
        ViewGroup viewGroup = findViewById(R.id.rootViewGroup);
        viewGroup.removeView(shimmer);
    }

    @Override
    protected String getScreenName() {
        return "MyTransactionDetailActivity";
    }
}
