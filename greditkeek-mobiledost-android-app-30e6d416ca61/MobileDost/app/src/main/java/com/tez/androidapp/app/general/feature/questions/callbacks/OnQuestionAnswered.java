package com.tez.androidapp.app.general.feature.questions.callbacks;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;

/**
 * Created  on 6/8/2017.
 */

public interface OnQuestionAnswered extends BaseRecyclerViewListener {

    void questionAnswered(int questionPosition);
}