package com.tez.androidapp.commons.utils.app;

import com.facebook.appevents.AppEventsLogger;
import com.tez.androidapp.app.MobileDostApplication;

public class AppEventsHelperUtil {

    public static AppEventsLogger getInstance() {
        return LazyHolder.INSTANCE;
    }

    private static class LazyHolder {
        private static final AppEventsLogger INSTANCE = AppEventsLogger.newLogger(MobileDostApplication.getInstance().getApplicationContext());
    }
}
