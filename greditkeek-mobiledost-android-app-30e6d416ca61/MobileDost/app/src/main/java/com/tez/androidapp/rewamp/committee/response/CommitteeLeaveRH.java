package com.tez.androidapp.rewamp.committee.response;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeInviteListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeLeaveListener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import androidx.annotation.Nullable;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeLeaveRH extends BaseRH<CommitteeLeaveResponse> {

    private final CommitteeLeaveListener listener;

    public CommitteeLeaveRH(BaseCloudDataStore baseCloudDataStore, CommitteeLeaveListener committeeInviteListener) {
        super(baseCloudDataStore);
        this.listener = committeeInviteListener;
    }

    @Override
    protected void onSuccess(Result<CommitteeLeaveResponse> value) {
        CommitteeLeaveResponse committeeLeaveResponse = value.response().body();
        if (committeeLeaveResponse != null && committeeLeaveResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onCommitteeLeaveSuccess(committeeLeaveResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onCommitteeLeaveFailure(errorCode, message);
    }
}
