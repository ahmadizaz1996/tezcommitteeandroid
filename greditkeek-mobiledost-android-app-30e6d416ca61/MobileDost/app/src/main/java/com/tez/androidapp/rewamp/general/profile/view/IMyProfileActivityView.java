package com.tez.androidapp.rewamp.general.profile.view;

import com.tez.androidapp.app.base.ui.IBaseView;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

public interface IMyProfileActivityView extends IBaseView {
    void setVisibillityForTezConstraintLayoutPager(int visibillity);

    void setVisibillityForTezLinearLayoutShimmer(int visibillity);

    void setTextToTvFullName(String text);

    void setListenerToBtGoogle(DoubleTapSafeOnClickListener doubleTapSafeOnClickListener);

    void setListenerToBtFacebook(DoubleTapSafeOnClickListener doubleTapSafeOnClickListener);

    void setTextToTvMobileNumber(String text);

    void setTextToTvDateOfBirth(String text);

    void setTextToTvCnic(String text);

    void setVisibillityToBtCompleteProfile(int visibillity);

    void setVisibillityToIvFacebookTick(int visibillity);

    void setVisibillityToIvGoogleTick(int visibillity);

    void setTextToTvEmailAddress(String text);

    void setTextToTvCurrentAddress(String text);

    void onClickTezImageButtonFacebook();
}
