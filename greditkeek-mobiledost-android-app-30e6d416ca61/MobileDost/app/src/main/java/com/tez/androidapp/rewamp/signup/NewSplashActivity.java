package com.tez.androidapp.rewamp.signup;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.utils.extract.DeviceDataExtractionJobService;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SelectLanguageActivityRouter;
import com.tez.androidapp.rewamp.signup.router.WelcomeBackActivityRouter;
import com.tez.androidapp.rewamp.util.DialogUtil;

import net.tez.fragment.util.optional.Optional;
import net.tez.fragment.util.optional.TextUtil;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class NewSplashActivity extends BaseActivity {

    private static final int ONE_HOUR = 60 * 60;
    private static final int TWO_HOURS = 2 * ONE_HOUR;
    private static final int FOUR_HOURS = 4 * ONE_HOUR;

    @BindView(R.id.tllTopView)
    private ConstraintLayout tllTopView;

    @BindView(R.id.bottomView)
    private TezLinearLayout bottomView;

    @BindView(R.id.tvName)
    private TezTextView tvName;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        LocaleHelper.onAttach(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_splash_activity);
        ViewBinder.bind(this);
        this.startDataLiftJobService();
        this.init();
        this.logFirebaseEvents();
        this.buildingUserCrashlyticsProfile();
    }

    private Animation.AnimationListener fadeInAnimationListener() {
        return new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                splashIsLoadedSuccessfully();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
    }

    private Animation.AnimationListener topToBottomAnimationListener() {
        return new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation anim = AnimationUtils.loadAnimation(NewSplashActivity.this, R.anim.fade_in);
                anim.setAnimationListener(fadeInAnimationListener());
                bottomView.setVisibility(View.VISIBLE);
                tvName.setVisibility(View.VISIBLE);
                bottomView.setAnimation(anim);
                tvName.setAnimation(anim);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
    }

    private void init() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.top_to_bottom);
        anim.setAnimationListener(this.topToBottomAnimationListener());
        tllTopView.setAnimation(anim);
    }

    private void startDataLiftJobService() {
        if (SystemClock.elapsedRealtime() < MDPreferenceManager.getLastUploadElapsedTime())
            MDPreferenceManager.setLastUploadElapsedTime(-Constants.TIME_TO_LIFT);

        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job job = dispatcher.newJobBuilder().setService(DeviceDataExtractionJobService.class)
                .setTag(Constants.DATA_LIFT_JOB_TAG)
                .setRecurring(true)
                .setLifetime(Lifetime.FOREVER)
                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                .setTrigger(Trigger.executionWindow(TWO_HOURS, FOUR_HOURS))
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build();
        dispatcher.schedule(job);
    }

    private void splashIsLoadedSuccessfully() {
        Optional.doWhen(Utility.isDeviceRooted(getApplicationContext())
                        || Utility.checkIsEmulator(),
                () -> DialogUtil.showInformativeMessage(this, getString(R.string.string_device_rooted),
                        (dialog, which) -> System.exit(1)),
                this::LoadNextActivity);
    }

    private void LoadNextActivity() {
        Optional.doWhen(Utility.doesUserExist(),
                this::loadWelcomeBackActivity,
                this::loadStartUpActivity);
/*        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this);
        finish();*/
    }

    private void loadWelcomeBackActivity() {
        User user = MDPreferenceManager.getUser();
        String principalName = user.getPrincipalName();
        if(TextUtil.isNotEmpty(principalName)) {
            if(principalName.length() == 13){
                MDPreferenceManager.clearAllPreferences();
                loadStartUpActivity();
            } else {
                WelcomeBackActivityRouter.createInstance().setDependenciesAndRoute(this);
            }
        } else {
            loadStartUpActivity();
        }
    }

    private void loadStartUpActivity() {
        loadSelectLanguageActivity();
    }

    private void loadSelectLanguageActivity() {
        SelectLanguageActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    private void logFirebaseEvents() {
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "screen");
        params.putString(FirebaseAnalytics.Param.ITEM_NAME, NewSplashActivity.class.getSimpleName());
        FirebaseAnalytics.getInstance(this).logEvent(FirebaseAnalytics.Event.VIEW_ITEM, params);
    }

    private void buildingUserCrashlyticsProfile() {

        Optional.ifPresent(MDPreferenceManager.getUser(),
                (user) -> {
                    Utility.setUserForCrashlytics();
                    Utility.setUserForFacebook();
                    Utility.setUserForFirebase();
                });
    }

    @Override
    protected String getScreenName() {
        return "NewSplashActivity";
    }
}
