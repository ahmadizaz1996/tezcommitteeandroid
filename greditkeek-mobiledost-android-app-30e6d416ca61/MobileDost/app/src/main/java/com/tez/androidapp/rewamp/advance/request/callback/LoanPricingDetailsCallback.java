package com.tez.androidapp.rewamp.advance.request.callback;

import com.tez.androidapp.rewamp.advance.request.entity.PricingDetail;

import java.util.List;

public interface LoanPricingDetailsCallback {

    void onSuccessLoanPricingDetails(List<PricingDetail> loanPricingDetails, double latePaymentCharges);

    void onFailureLoanPricingDetails(int statusCode, String description);
}
