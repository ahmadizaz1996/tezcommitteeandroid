package com.tez.androidapp.rewamp.signup;

import android.os.Bundle;
import android.view.View;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.contact.us.ui.activities.ContactUsActivity;

public class LoginFailedActivity extends ContactUsActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        tvTitle.setText(R.string.login_to_tez);
        tvMessage.setText(R.string.your_data_private_msg);
    }

    @Override
    protected String getScreenName() {
        return "LoginFailedActivity";
    }
}
