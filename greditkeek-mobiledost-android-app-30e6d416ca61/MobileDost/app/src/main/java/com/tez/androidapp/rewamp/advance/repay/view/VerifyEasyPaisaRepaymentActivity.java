package com.tez.androidapp.rewamp.advance.repay.view;

import android.location.Location;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezPinView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.repay.presenter.IVerifyEasyPaisaRepaymentActivityPresenter;
import com.tez.androidapp.rewamp.advance.repay.presenter.VerifyEasyPaisaRepaymentActivityPresenter;
import com.tez.androidapp.rewamp.advance.repay.router.RepaymentSuccessfulActivityRouter;
import com.tez.androidapp.rewamp.advance.repay.router.VerifyEasyPaisaRepaymentRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import net.tez.viewbinder.library.core.BindView;

public class VerifyEasyPaisaRepaymentActivity extends PlaybackActivity implements IVerifyEasyPaisaRepaymentActivityView, TezPinView.OnPinEnteredListener {

    @BindView(R.id.pinViewOTP)
    protected TezPinView pinViewOTP;

    @BindView(R.id.tvResendCode)
    protected TezTextView tvResendCode;

    @BindView(R.id.tvDidNotReceiveCode)
    protected TezTextView tvDidNotReceiveCode;

    @BindView(R.id.tvEnterCode)
    protected TezTextView tvEnterCode;

    private IVerifyEasyPaisaRepaymentActivityPresenter iVerifyEasyPaisaRepaymentActivityPresenter;
    private static final int EASY_PAISA_PIN_LENGTH = 5;

    public VerifyEasyPaisaRepaymentActivity() {
        iVerifyEasyPaisaRepaymentActivityPresenter = new VerifyEasyPaisaRepaymentActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_repayment);
        init();
    }

    protected void init(){
        pinViewOTP.setOnPinEnteredListener(this);
        initiateRepayment();
        initViews();
    }

    public void setIVerifyEasyPaisaRepaymentActivityPresenter(IVerifyEasyPaisaRepaymentActivityPresenter iVerifyEasyPaisaRepaymentActivityPresenter) {
        this.iVerifyEasyPaisaRepaymentActivityPresenter = iVerifyEasyPaisaRepaymentActivityPresenter;
    }

    @Override
    public void setPinViewOtpClearText() {
        pinViewOTP.clearText();
    }

    @Override
    public void setPinViewOtpEnabled(boolean enabled) {
        pinViewOTP.setEnabled(enabled);
    }

    @Override
    public void navigateToDashboardWithNewStack(){
        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public String getFormattedErrorMessage(@StringRes int message) {
        String messageString = getString(message);
        messageString = messageString.replaceAll(ResponseStatusCode.PlaceHolders.WALLET_NAME.getValue(), Utility.getWalletName(getServiceProviderIdFromIntent()));
        return messageString.replaceAll(ResponseStatusCode.PlaceHolders.WALLET_SUPPORT_NUMBER.getValue(), Utility.getWalletProviderNumber(getServiceProviderIdFromIntent()));
    }

    protected void initViews() {
        tvDidNotReceiveCode.setVisibility(View.GONE);
        tvResendCode.setVisibility(View.GONE);
        pinViewOTP.setNumDigits(getPinLength());
        tvEnterCode.setText(getString(R.string.please_enter_your_wallet_pin_to_confirm_transaction,
                Utility.getWalletName(getServiceProviderIdFromIntent())));
    }

    protected int getPinLength(){
        return EASY_PAISA_PIN_LENGTH;
    }

    protected void initiateRepayment() {
        iVerifyEasyPaisaRepaymentActivityPresenter.initiateRepayment(getLoanIdFromIntent(), getMobileAccountIdFromIntent());
    }

    protected int getLoanIdFromIntent() {
        return getIntent().getIntExtra(VerifyEasyPaisaRepaymentRouter.LOAN_ID, -100);
    }

    protected int getMobileAccountIdFromIntent() {
        return getIntent().getIntExtra(VerifyEasyPaisaRepaymentRouter.WALLET_MOBILE_ACCOUNT_ID, -100);
    }

    protected int getServiceProviderIdFromIntent() {
        return getIntent().getIntExtra(VerifyEasyPaisaRepaymentRouter.WALLET_SERVICE_PROVIDER_ID, -100);
    }

    protected double getRepayAmountFromIntent() {
        return getIntent().getDoubleExtra(VerifyEasyPaisaRepaymentRouter.AMOUNT, -100);
    }

    @Override
    public void routeToRepaymentSuccessFullActivity(@NonNull LoanDetails loanDetails) {
        RepaymentSuccessfulActivityRouter.createInstance().setDependenciesAndRoute(this, loanDetails);
        finish();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    public void onPinEntered(@NonNull String pin) {
        showTezLoader();
        getCurrentLocation(new LocationAvailableCallback() {
            @Override
            public void onLocationAvailable(@NonNull Location location) {
                makeRepayment(pin, location);
            }

            @Override
            public void onLocationFailed(String message) {
                makeRepayment(pin, null);
            }

            @Override
            public void onLocationPermissionDenied() {
                makeRepayment(pin, null);
            }
        });
    }

    protected void makeRepayment(@NonNull String pin, @Nullable Location location) {
        iVerifyEasyPaisaRepaymentActivityPresenter.repayLoan(pin, getMobileAccountIdFromIntent(),
                getRepayAmountFromIntent(), null, location);
    }

    @Override
    protected String getScreenName() {
        return "VerifyEasyPaisaRepaymentActivity";
    }
}
