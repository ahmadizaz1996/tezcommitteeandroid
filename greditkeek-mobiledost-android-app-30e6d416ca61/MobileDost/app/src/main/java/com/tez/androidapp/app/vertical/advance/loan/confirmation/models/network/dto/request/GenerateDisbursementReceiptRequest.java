package com.tez.androidapp.app.vertical.advance.loan.confirmation.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 9/14/2017.
 */

public class GenerateDisbursementReceiptRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/loan/disbursement/receipt/";

}
