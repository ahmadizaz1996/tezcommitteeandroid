package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.listener.CommitteeInstallmentPayLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeWalletVerificationLIstener;
import com.tez.androidapp.rewamp.committee.listener.JoinCommitteeVerificationLIstener;
import com.tez.androidapp.rewamp.committee.response.CommitteeInstallmentPayResponse;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface ICommitteeWalletVerificationActivityInteractorOutput extends CommitteeWalletVerificationLIstener, CommitteeInstallmentPayLIstener {
}
