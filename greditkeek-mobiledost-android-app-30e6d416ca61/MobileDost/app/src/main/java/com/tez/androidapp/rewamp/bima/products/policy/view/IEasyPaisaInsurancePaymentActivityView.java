package com.tez.androidapp.rewamp.bima.products.policy.view;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.bima.products.policy.response.PurchasedInsurancePolicy;

public interface IEasyPaisaInsurancePaymentActivityView extends IBaseView {

    void setPinViewOtpClearText();

    void setPinViewOtpEnabled(boolean enabled);

    void navigateToDashboard();

    String getFormattedErrorMessage(@StringRes int message);

    void routeToRepaymentSuccessFullActivity(@NonNull PurchasedInsurancePolicy loanDetails);
}
