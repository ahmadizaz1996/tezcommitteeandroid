package com.tez.androidapp.rewamp.signup.view;

import com.tez.androidapp.app.base.ui.IBaseView;

import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener;

public interface INewLoginActivityView extends IBaseView {

    void onTemporaryPinSet();

    void userPinIsVerified();

    void startRequestFailedActivity();

    void onUserReactivateAccount();

    DoubleTapSafeDialogClickListener getDialogOnClickListener();

    void startUserReactiviateAccount();
}
