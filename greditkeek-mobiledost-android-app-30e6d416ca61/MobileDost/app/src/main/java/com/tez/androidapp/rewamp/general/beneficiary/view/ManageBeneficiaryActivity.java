package com.tez.androidapp.rewamp.general.beneficiary.view;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.ToolbarActivity;
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.rewamp.general.beneficiary.adapter.ManageBeneficiaryAdapter;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.IManageBeneficiaryActivityPresenter;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.ManageBeneficiaryActivityPresenter;
import com.tez.androidapp.rewamp.general.beneficiary.router.PolicyDetailActivityRouter;
import com.tez.androidapp.rewamp.profile.trust.router.ProfileTrustActivityRouter;

import net.tez.viewbinder.library.core.BindView;

import java.util.List;

public class ManageBeneficiaryActivity extends ToolbarActivity implements IManageBeneficiaryActivityView, ManageBeneficiaryAdapter.ManageBeneficiaryListener {

    private final IManageBeneficiaryActivityPresenter iManageBeneficiaryActivityPresenter;

    @BindView(R.id.rvPolicies)
    private RecyclerView rvPolicies;

    @BindView(R.id.layoutEmptyBeneficiary)
    private TezConstraintLayout layoutEmptyBeneficiary;

    public ManageBeneficiaryActivity() {
        iManageBeneficiaryActivityPresenter = new ManageBeneficiaryActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_beneficiary);
        this.tezToolbar.setToolbarTitle(R.string.manage_beneficiary);
        iManageBeneficiaryActivityPresenter.getManageBeneficiaryPolicies();
    }

    @Override
    public void setManageBeneficiaryAdapter(@NonNull List<Policy> items) {
        ManageBeneficiaryAdapter adapter = new ManageBeneficiaryAdapter(items, this);
        rvPolicies.setLayoutManager(new LinearLayoutManager(this));
        rvPolicies.setAdapter(adapter);
    }

    @Override
    public void setLayoutEmptyBeneficiaryVisibility(int visibility) {
        layoutEmptyBeneficiary.setVisibility(visibility);
    }

    @Override
    public void setRvPoliciesVisibility(int visibility) {
        rvPolicies.setVisibility(visibility);
    }

    @Override
    public void onProfileIncompleteError() {
        ProfileTrustActivityRouter.createInstance().setDependenciesAndRoute(this);
        finish();
    }

    @Override
    public void showTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.GONE);
        ViewGroup viewGroup = findViewById(R.id.rootViewGroup);
        viewGroup.removeView(shimmer);
    }

    @Override
    public void onPolicyClick(@NonNull Policy policy) {
        PolicyDetailActivityRouter.createInstance().setDependenciesAndRoute(this, policy);
    }

    @Override
    protected String getScreenName() {
        return "ManageBeneficiaryActivity";
    }
}
