package com.tez.androidapp.rewamp.general.wallet.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.WalletAddedActivity;

public class WalletAddedActivityRouter extends BaseActivityRouter {

    public static final String SERVICE_PROVIDER_ID = "SERVICE_PROVIDER_ID";

    public static WalletAddedActivityRouter createInstance() {
        return new WalletAddedActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        int serviceProviderId) {
        Intent intent = createIntent(from);
        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        intent.putExtra(SERVICE_PROVIDER_ID, serviceProviderId);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, WalletAddedActivity.class);
    }
}
