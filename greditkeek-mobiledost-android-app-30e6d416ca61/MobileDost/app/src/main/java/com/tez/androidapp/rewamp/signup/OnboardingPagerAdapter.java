package com.tez.androidapp.rewamp.signup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by VINOD KUMAR on 5/28/2019.
 */
public class OnboardingPagerAdapter extends FragmentPagerAdapter {

    private List<OnboardingContent> onboardingFragmentList;

    public OnboardingPagerAdapter(List<OnboardingContent> items, FragmentManager fm) {
        super(fm);
        this.onboardingFragmentList = items;
    }

    @Override
    public Fragment getItem(int i) {
        return OnboardingContentFragment.newInstance(onboardingFragmentList.get(i));
    }

    @Override
    public int getCount() {
        return onboardingFragmentList.size();
    }
}


