package com.tez.androidapp.rewamp.bima.products.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class CallNumbersAdapter extends GenericRecyclerViewAdapter<String, CallNumbersAdapter.CallNumberListener, CallNumbersAdapter.CallNumberViewHolder> {

    public CallNumbersAdapter(@NonNull List<String> items, @Nullable CallNumberListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public CallNumberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.list_item_call_number, parent);
        return new CallNumberViewHolder(view);
    }

    public interface CallNumberListener extends BaseRecyclerViewListener {

        void onClickNumber(@NonNull String number);
    }

    static class CallNumberViewHolder extends BaseViewHolder<String, CallNumberListener> {

        @BindView(R.id.tvNumber)
        private TezTextView tvNumber;

        private CallNumberViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(String item, @Nullable CallNumberListener listener) {
            super.onBind(item, listener);
            tvNumber.setText(item);
            if (listener != null)
                this.setItemViewOnClickListener(v -> listener.onClickNumber(item));
        }
    }
}
