package com.tez.androidapp.rewamp.bima.products.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.bima.products.model.InsuranceProduct;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class ProductsAdapter extends GenericRecyclerViewAdapter<InsuranceProduct, ProductsAdapter.ProductListener, ProductsAdapter.ProductViewHolder> {

    public ProductsAdapter(@NonNull List<InsuranceProduct> items, @Nullable ProductListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.list_item_product, parent);
        return new ProductViewHolder(view);
    }

    public interface ProductListener extends BaseRecyclerViewListener {

        void onPurchaseProduct(@NonNull InsuranceProduct insuranceProduct);

        void onGetProductDetails(@NonNull InsuranceProduct insuranceProduct);

        void onClickTezInsurance(@NonNull InsuranceProduct insuranceProduct);
    }

    static class ProductViewHolder extends BaseViewHolder<InsuranceProduct, ProductListener> {

        @BindView(R.id.rootCv)
        private TezCardView rootCv;

        @BindView(R.id.tvTitle)
        private TezTextView tvTitle;

        @BindView(R.id.ivVerticalLine)
        private TezImageView ivVerticalLine;

        @BindView(R.id.tvDescription)
        private TezTextView tvDescription;

        @BindView(R.id.ivIcon)
        private TezImageView ivIcon;

        @BindView(R.id.ivArrow)
        private TezImageView ivArrow;

        private ProductViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(InsuranceProduct item, @Nullable ProductListener listener) {
            super.onBind(item, listener);
            ivIcon.setImageResource(Utility.getInsuranceProductIcon(item.getId()));
            tvTitle.setText(item.getTitle());
            tvDescription.setText(item.getPolicyTagLine());

            int etGrey = Utility.getColorFromResource(R.color.editTextBottomLineColorGrey);
            int tvBlue = Utility.getColorFromResource(R.color.textViewTitleColorBlue);
            int tvBlack = Utility.getColorFromResource(R.color.textViewTextColorBlack);
            int tvGreen = Utility.getColorFromResource(R.color.textViewTextColorGreen);

            ivArrow.setVisibility(item.isDisabled() ? View.GONE : View.VISIBLE);
            ivVerticalLine.setColorFilter(item.isDisabled() ? etGrey : tvGreen);
            ivIcon.setColorFilter(item.isDisabled() ? etGrey : tvBlue);
            tvTitle.setTextColor(item.isDisabled() ? etGrey : tvBlue);
            tvDescription.setTextColor(item.isDisabled() ? etGrey : tvBlack);
            rootCv.attachClickEffect(item.isDisabled() ? IBaseWidget.NO_EFFECT : IBaseWidget.SCALE_EFFECT, rootCv);

            this.setItemViewOnClickListener(listener != null && !item.isDisabled() ? v -> {
                if (item.getId() == Constants.TEZ_INSURANCE_PLAN_ID)
                    listener.onClickTezInsurance(item);
                else if (item.isProductPurchased())
                    listener.onGetProductDetails(item);
                else
                    listener.onPurchaseProduct(item);
            } : null);
        }
    }
}
