package com.tez.androidapp.app.base.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.observer.SessionTimeOutObserver;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.rewamp.advance.limit.router.LimitAssignedActivityRouter;
import com.tez.androidapp.rewamp.advance.request.router.LoanReceiptActivityRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.profile.trust.router.ProfileTrustActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NewIntroActivityRouter;
import com.tez.androidapp.rewamp.signup.router.WelcomeBackActivityRouter;

import net.tez.fragment.util.optional.Optional;

public class PushNotificationHandlerActivity extends BaseActivity {

    @Override
    public void onAppStart(boolean inactiveTimeExceeded) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.navigateNotification();
    }

    private void navigateNotification() {
        Optional.ifPresent(MDPreferenceManager.getUser(), user -> {
            Optional.doWhen(SessionTimeOutObserver.getInstance().isAppActive(),
                    () -> MobileDostApplication.getInstance().sendOrderedBroadcast(new Intent(Constants.BROAD_CAST_RECIEVER_ON_VALIDATE_PIN_ACTIVITY), null,
                            new BroadcastReceiver() {
                                @Override
                                public void onReceive(Context context, Intent intent) {
                                    Optional.doWhen(getResultCode() == 1,
                                            PushNotificationHandlerActivity.this::startWelcomeBackActivity,
                                            () -> onRoute(getIntent().getStringExtra(PushNotificationConstants.KEY_ROUTE_TO)==null?""
                                                    :getIntent().getStringExtra(PushNotificationConstants.KEY_ROUTE_TO)));
                                }
                            }, null, RESULT_OK, null, null),

                    this::startWelcomeBackActivity);
        }, this::startNewIntroActivity);
    }

    private void onRoute(@NonNull String routeTo) {
        switch (routeTo) {

            case PushNotificationConstants.LIMIT_ASSIGNED_ACTIVITY:
                LimitAssignedActivityRouter.createInstance().setDependenciesAndRoute(this);
                break;

            case PushNotificationConstants.LOAN_RECEIPT_ACTIVITY:
                LoanReceiptActivityRouter.createInstance().setDependenciesAndRoute(this);
                break;

            case PushNotificationConstants.PROFILE_TRUST_ACTIVITY:
                ProfileTrustActivityRouter.createInstance().setDependenciesAndRoute(this);
                break;

            default:
                DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
                break;
        }
        finish();
    }

    private void startNewIntroActivity() {
        NewIntroActivityRouter.createInstance().setDependenciesAndRoute(this);
        finish();
    }

    protected void startWelcomeBackActivity() {
        WelcomeBackActivityRouter.createInstance().setDependenciesAndRouteForPush(this,
                getIntent().getStringExtra(PushNotificationConstants.KEY_ROUTE_TO));
        finish();
    }

    @Override
    protected String getScreenName() {
        return "PushNotificationHandlerActivity";
    }
}
