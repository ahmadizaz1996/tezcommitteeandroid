package com.tez.androidapp.rewamp.bima.products.presenter;

import android.view.View;

import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.bima.products.interactor.IProductPolicyActivityInteractor;
import com.tez.androidapp.rewamp.bima.products.interactor.IProductPolicyActivityInteractorOutput;
import com.tez.androidapp.rewamp.bima.products.interactor.ProductPolicyActivityInteractor;
import com.tez.androidapp.rewamp.bima.products.model.ProductPolicy;
import com.tez.androidapp.rewamp.bima.products.response.ProductPolicyResponse;
import com.tez.androidapp.rewamp.bima.products.view.IProductPolicyActivityView;

import net.tez.fragment.util.optional.Optional;
import net.tez.fragment.util.optional.TextUtil;

public class ProductPolicyActivityPresenter implements IProductPolicyActivityPresenter,
        IProductPolicyActivityInteractorOutput {

    private final IProductPolicyActivityView iProductPolicyActivityView;
    private final IProductPolicyActivityInteractor iProductPolicyActivityInteractor;

    public ProductPolicyActivityPresenter(IProductPolicyActivityView iProductPolicyActivityView) {
        this.iProductPolicyActivityView = iProductPolicyActivityView;
        this.iProductPolicyActivityInteractor = new ProductPolicyActivityInteractor(this);
    }

    @Override
    public void getProductPolicy(int policyId) {
        iProductPolicyActivityView.setClContentVisibility(View.GONE);
        iProductPolicyActivityView.showTezLoader();
        iProductPolicyActivityInteractor.getProductPolicy(policyId);
    }

    @Override
    public void onGetProductPolicySuccess(ProductPolicyResponse productPolicyResponse) {
        ProductPolicy productPolicy = productPolicyResponse.getPolicyDetailsDto();
        iProductPolicyActivityView.dismissTezLoader();
        iProductPolicyActivityView.initPolicyDetails(productPolicy);
        iProductPolicyActivityView.setInsuranceProviderLogo(Utility.getInsuranceProviderLogo(productPolicy.getInsuranceServiceProviderCode()));

        Optional.ifPresent(productPolicy.getContact().getContactEmail(),
                iProductPolicyActivityView::setGroupEmailListener,
                iProductPolicyActivityView::hideTllEmail);

        Optional.ifPresent(productPolicy.getContact().getContactWhatsApp(),
                iProductPolicyActivityView::setGroupWhatsappListener,
                iProductPolicyActivityView::hideWhatsapp);

        String[] numbers = productPolicy.getContact().getContactPhone().split(",");

        if (numbers.length == 1)
            iProductPolicyActivityView.setGroupCallListener(numbers[0]);
        else
            iProductPolicyActivityView.setGroupCallListener(numbers);

        iProductPolicyActivityView.setClContentVisibility(View.VISIBLE);
    }

    @Override
    public void onGetProductPolicyFailure(int errorCode, String message) {
        iProductPolicyActivityView.dismissTezLoader();
        iProductPolicyActivityView.showError(errorCode, (dialog, which) -> iProductPolicyActivityView.finishActivity());
    }
}
