package com.tez.androidapp.rewamp.signup.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserVerifyRequest;
import com.tez.androidapp.repository.network.handlers.callbacks.UserDeviceKeyCallback;
import com.tez.androidapp.repository.network.models.request.VerifyActiveDeviceRequest;

public interface IWelcomeBackActivityInteractor extends IBaseInteractor, UserDeviceKeyCallback {
    void verifyUserActiveDevice(@NonNull VerifyActiveDeviceRequest verifyActiveDeviceRequest, String pin);

    void login(@NonNull UserLoginRequest userLoginRequest, boolean callDeviceKey, String imei);

    void verifyUser(@NonNull UserVerifyRequest userVerifyRequest, boolean callDeviceKey, String imei);
}
