package com.tez.androidapp.rewamp.advance.onboarding;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezToolbar;
import com.tez.androidapp.rewamp.general.profile.router.BasicProfileActivityRouter;
import com.tez.androidapp.rewamp.signup.BaseOnboardingActivity;
import com.tez.androidapp.rewamp.signup.OnboardingContent;

import java.util.ArrayList;
import java.util.List;

public class AdvanceOnBoardingActivity extends BaseOnboardingActivity {

    @Override
    protected void initViews(TezToolbar onboardingToolbar) {
        btMainButton.setText(getString(R.string.string_unlock));
        btMainButton.setDoubleTapSafeOnClickListener(view -> {
            BasicProfileActivityRouter.createInstance().setDependenciesAndRoute(this);
            finish();
        });
    }

    @NonNull
    @Override
    protected List<OnboardingContent> getOnboardingContentList() {
        List<OnboardingContent> list = new ArrayList<>();
        list.add(new OnboardingContent(R.drawable.img_tez_onboarding_1, R.string.advance_onboarding_title_1, R.string.advance_onboarding_description_1));
        list.add(new OnboardingContent(R.drawable.img_tez_onboarding_2, R.string.advance_onboarding_title_2, R.string.advance_onboarding_description_2));
        list.add(new OnboardingContent(R.drawable.img_tez_onboarding_3, R.string.advance_onboarding_title_3, R.string.advance_onboarding_description_3));
        return list;
    }

    @Override
    protected String getScreenName() {
        return "AdvanceOnBoardingActivity";
    }
}
