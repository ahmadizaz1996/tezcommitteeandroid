package com.tez.androidapp.rewamp.bima.products.policy.interactor;

import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InitiatePurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.request.PurchasePolicyRequest;

public interface IEasyPaisaInsurancePaymentActivityInteractor {
    void initiatePurchasePolicy(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest);

    void purchasePolicy(PurchasePolicyRequest purchasePolicyRequest);

    void resendCode(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest, InitiatePurchasePolicyCallback callback);
}
