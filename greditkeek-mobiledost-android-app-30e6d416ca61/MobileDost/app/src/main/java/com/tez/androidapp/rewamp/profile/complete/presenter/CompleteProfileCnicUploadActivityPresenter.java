package com.tez.androidapp.rewamp.profile.complete.presenter;

import androidx.annotation.Nullable;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateCnicPictureRequest;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;
import com.tez.androidapp.rewamp.profile.complete.interactor.CompleteProfileCnicUploadActivityInteractor;
import com.tez.androidapp.rewamp.profile.complete.interactor.ICompleteProfileCnicUploadActivityInteractor;
import com.tez.androidapp.rewamp.profile.complete.view.ICompleteProfileCnicUploadActivityView;

public class CompleteProfileCnicUploadActivityPresenter
        implements ICompleteProfileCnicUploadActivityPresenter,
        ICompleteProfileCnicUploadActivityInteractorOutput{
    private final static int MAX_OCR_FAILED_COUNT = 3;

    private final ICompleteProfileCnicUploadActivityView iCompleteProfileCnicUploadActivityView;
    private final ICompleteProfileCnicUploadActivityInteractor iCompleteProfileCnicUploadActivityInteractor;

    protected String userCNIC = null;
    protected String userCNICDateOfExpiry = null;
    protected String userCNICDateOfIssue = null;
    protected String userCNICDateOfBirth = null;
    protected boolean ocrHasBeenRun;
    protected boolean userSelfieIsUploaded;
    protected boolean userFrontNicIsUploaded;
    protected boolean userBackNicIsUploaded;

    public CompleteProfileCnicUploadActivityPresenter(ICompleteProfileCnicUploadActivityView iCompleteProfileCnicUploadActivityView) {
        this.iCompleteProfileCnicUploadActivityView = iCompleteProfileCnicUploadActivityView;
        this.iCompleteProfileCnicUploadActivityInteractor = new CompleteProfileCnicUploadActivityInteractor(this);
    }

    @Override
    public void setUserSelfieIsUploaded(){
        this.userSelfieIsUploaded = true;
    }

    @Override
    public void setUserFrontNicIsUploaded(){
        this.userFrontNicIsUploaded = true;
    }

    @Override
    public void setUserBackNicIsUploaded(){
        this.userBackNicIsUploaded = true;
    }

    @Override
    public void startCamera(int requestCode, String detectObject, boolean isSelfie){
        Optional.doWhen(requestCode!=-1
                && detectObject!=null
                && this.isOCRAllowed(),
                ()-> this.iCompleteProfileCnicUploadActivityView.
                        startRetumModuleScreen(requestCode, detectObject),
                ()->this.iCompleteProfileCnicUploadActivityView.startBackupCamera(requestCode, isSelfie));
    }

    @Override
    public void setUserCnic(String cnic){
        this.userCNIC = cnic;
    }

    @Override
    public void setUserCNICDateOfExpiry(String dateOfExpiry){
        this.userCNICDateOfExpiry = dateOfExpiry;
    }

    @Override
    public void setUserCNICDateOfBirth(String dateOfBirth){
        this.userCNICDateOfBirth = dateOfBirth;
    }

    @Override
    public void setUserCNICDateOfIssue(String userCNICDateOfIssue){
        this.userCNICDateOfIssue = userCNICDateOfIssue;
    }

    @Override
    public void updateCnicImageCardViews(int requestCode,@Nullable String filePath,
                                         boolean isAutoDetected, @Nullable RetumDataModel retumDataModel){
        setOcrHasBeenRun(ocrHasBeenRun || isAutoDetected);
        Optional.ifPresent(filePath, (fp)->{
            switch (requestCode){
                case Constants.CNIC_REQUEST_CODE_FRONT:
                    this.iCompleteProfileCnicUploadActivityView.setLoaderInUpCVFrontPartVisible(true);
                    this.iCompleteProfileCnicUploadActivityInteractor.updateUserCnicPicture(isAutoDetected,
                            requestCode, UpdateCnicPictureRequest.FRONT, filePath, retumDataModel);
                    break;
                case Constants.CNIC_REQUEST_CODE_BACK:
                    this.iCompleteProfileCnicUploadActivityView.setLoaderInUpCVBackPart(true);
                    this.iCompleteProfileCnicUploadActivityInteractor.updateUserCnicPicture(isAutoDetected,
                            requestCode, UpdateCnicPictureRequest.BACK, filePath, retumDataModel);
                    break;
                case Constants.CNIC_REQUEST_CODE_FACE:
                    this.iCompleteProfileCnicUploadActivityView.setLoaderInUpCVSelfiePartVisbile(true);
                    this.iCompleteProfileCnicUploadActivityInteractor.updateUserCnicPicture(isAutoDetected,
                            requestCode, UpdateCnicPictureRequest.SELFIE, filePath, retumDataModel);
                    break;
            }
        });
    }

    @Override
    public void setOcrHasBeenRun(boolean ocrHasBeenRun){
        this.ocrHasBeenRun = ocrHasBeenRun;
    }



    @Override
    public void onUpdateCnicPictureSuccess(int requestCode,
                                           @Nullable String filePath,
                                           @Nullable RetumDataModel retumDataModel) {
        Optional.ifPresent(filePath, (fp)->{
            switch (requestCode){
                case Constants.CNIC_REQUEST_CODE_FRONT:
                    this.iCompleteProfileCnicUploadActivityView.setLoaderInUpCVFrontPartVisible(false);
                    this.iCompleteProfileCnicUploadActivityView.loadImageInUpCVFrontPart(filePath);
                    saveExtractedData(retumDataModel);
                    this.userFrontNicIsUploaded =true;
                    break;
                case Constants.CNIC_REQUEST_CODE_BACK:
                    this.iCompleteProfileCnicUploadActivityView.setLoaderInUpCVBackPart(false);
                    this.iCompleteProfileCnicUploadActivityView.loadImageInUpCVBackPart(filePath);
                    saveExtractedData(retumDataModel);
                    this.userBackNicIsUploaded =true;
                    break;
                case Constants.CNIC_REQUEST_CODE_FACE:
                    this.iCompleteProfileCnicUploadActivityView.setLoaderInUpCVSelfiePartVisbile(false);
                    this.iCompleteProfileCnicUploadActivityView.loadImageInUpCVSelfiePart(filePath);
                    this.userSelfieIsUploaded =true;
                    break;
            }
            postExtractingData();
        });
    }

    protected void postExtractingData(){
        Optional.doWhen(this.userSelfieIsUploaded
                && this.userBackNicIsUploaded && this.userFrontNicIsUploaded,
                ()->this.iCompleteProfileCnicUploadActivityView.setBtContinueActive(userCNIC,
                        userCNICDateOfBirth,userCNICDateOfIssue, userCNICDateOfExpiry),
                ()-> Optional.doWhen(ocrHasBeenRun,
                        ()-> this.iCompleteProfileCnicUploadActivityView.updateClickListeners(userCNIC,
                                userCNICDateOfBirth, userCNICDateOfIssue, userCNICDateOfExpiry)));
    }

    private void saveExtractedData(@Nullable RetumDataModel retumDataModel){
        this.userCNIC = retumDataModel!=null && retumDataModel.getCnicNumber()!=null?
                retumDataModel.getCnicNumber(): userCNIC;
        this.userCNICDateOfBirth = retumDataModel!=null && retumDataModel.getDateOfBirth()!=null?
                retumDataModel.getDateOfBirth():userCNICDateOfBirth;
        this.userCNICDateOfExpiry = retumDataModel!=null && retumDataModel.getCnicExpiryDate()!=null?
                retumDataModel.getCnicExpiryDate():this.userCNICDateOfExpiry;
        this.userCNICDateOfIssue = retumDataModel!=null && retumDataModel.getCnicIssueDate()!=null?
                retumDataModel.getCnicIssueDate():userCNICDateOfIssue;
    }

    @Override
    public void onUpdateCnicPictureFailure(int requestCode, int errorCode, String message) {
        this.iCompleteProfileCnicUploadActivityView.showError(errorCode);
        switch (requestCode){
            case Constants.CNIC_REQUEST_CODE_FRONT:
                this.iCompleteProfileCnicUploadActivityView.setLoaderInUpCVFrontPartVisible(false);
                break;
            case Constants.CNIC_REQUEST_CODE_BACK:
                this.iCompleteProfileCnicUploadActivityView.setLoaderInUpCVBackPart(false);
                break;
            case Constants.CNIC_REQUEST_CODE_FACE:
                this.iCompleteProfileCnicUploadActivityView.setLoaderInUpCVSelfiePartVisbile(false);
                break;
        }
    }

    private boolean isOCRAllowed(){
        return MDPreferenceManager.getOCRFailedCount() < MAX_OCR_FAILED_COUNT;
    }
}
