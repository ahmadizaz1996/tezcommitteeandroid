package com.tez.androidapp.rewamp.advance.request.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.advance.request.callback.LoanRemainingStepsCallback;
import com.tez.androidapp.rewamp.advance.request.presenter.ILoanRequiredStepsActivityInteractorOutput;

import java.util.List;

public class LoanRequiredStepsActivityInteractor implements ILoanRequiredStepsActivityInteractor {

    protected final ILoanRequiredStepsActivityInteractorOutput iLoanRequiredStepsActivityInteractorOutput;

    public LoanRequiredStepsActivityInteractor(ILoanRequiredStepsActivityInteractorOutput iLoanRequiredStepsActivityInteractorOutput) {
        this.iLoanRequiredStepsActivityInteractorOutput = iLoanRequiredStepsActivityInteractorOutput;
    }

    @Override
    public void getLoanRemainingSteps() {
        LoanCloudDataStore.getInstance().getLoanRemainingSteps(new LoanRemainingStepsCallback() {
            @Override
            public void onGetLoanRemainingStepsSuccess(List<String> stepsLeft) {
                iLoanRequiredStepsActivityInteractorOutput.onGetLoanRemainingStepsSuccess(stepsLeft);
            }

            @Override
            public void onGetLoanRemainingStepsFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getLoanRemainingSteps();
                else
                    iLoanRequiredStepsActivityInteractorOutput.onGetLoanRemainingStepsFailure(errorCode, message);

            }
        });
    }
}
