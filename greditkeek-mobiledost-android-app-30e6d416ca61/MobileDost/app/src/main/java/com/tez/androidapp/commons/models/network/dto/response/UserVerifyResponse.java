package com.tez.androidapp.commons.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created  on 9/15/2017.
 */

public class UserVerifyResponse extends BaseResponse {

    private Boolean isTemporaryPinSet;

    public Boolean getTemporaryPinSet() {
        return isTemporaryPinSet == null ? false : isTemporaryPinSet;
    }
}
