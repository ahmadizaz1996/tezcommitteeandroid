package com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserVerifyResponse;

/**
 * Created  on 12/15/2016.
 */

public interface UserVerifyCallback {

    void onUserVerifySuccess(UserVerifyResponse userVerifyResponse);

    void onUserVerifyFailure(BaseResponse baseResponse);
}
