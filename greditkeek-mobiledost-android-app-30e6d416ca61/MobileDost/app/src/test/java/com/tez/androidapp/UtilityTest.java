package com.tez.androidapp;

import com.tez.androidapp.app.vertical.bima.shared.models.BimaRelation;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;

import junit.framework.AssertionFailedError;

import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.lang.String.valueOf;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;


/**
 * Created by Rehman Murad Ali on 10/24/2017.
 */

public class UtilityTest {


    private List<BimaRelation> bimaRelations;


    @Before
    public void setUpForTest() {

    }

    @Test
    public void isEmptyTest_PassingNullAndEmptyString_ReturnTrue() {
        assertEquals(true, Utility.isEmpty(""));
        assertEquals(true, Utility.isEmpty(null));

    }


    @Test
    public void getFormattedDateTest_PassingDateLongWithValidDateFormat_ReturnStringWithCorrectFormat() {
        Calendar calendar = Calendar.getInstance();
        //Month in Calender class is 0th-base
        calendar.set(2017, 0, 1);
        Date date = calendar.getTime();
        assertEquals("2017-01-01", Utility.getFormattedDate(date.getTime(), Constants.BACKEND_DATE_FORMAT));
    }

    @Test
    public void getFormattedDateTest_PassingDateLongWithInvalidDateFormat_ReturnStringWithInCorrectFormat() {
        Calendar calendar = Calendar.getInstance();
        //Month in Calender class is 0th-base
        calendar.set(2017, 0, 1);
        Date date = calendar.getTime();
        assertNotSame("2017-01-01", Utility.getFormattedDate(date.getTime(), Constants.UI_DATE_FORMAT));
    }

    @Test(expected = NullPointerException.class)
    public void getFormattedDateTest_PassingDateLongWithNullDateFormat_ReturnNullPointerException() {
        Calendar calendar = Calendar.getInstance();
        //Month in Calender class is 0th-base
        calendar.set(2017, 0, 1);
        Date date = calendar.getTime();
        fail(Utility.getFormattedDate(date.getTime(), null));
    }

    @Test(expected = NullPointerException.class)
    public void getFormattedDateTest_PassingDateNullWithValidDateFormat_ReturnNullPointerException() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        //Month in Calender class is 0th-base
        calendar.set(2017, 0, 1);
        Date date = calendar.getTime();
        fail(valueOf(Utility.getFormattedDate(null, Constants.UI_DATE_FORMAT)));
    }


    @Test
    public void getFormattedDateTest_PassingDateStringWithValidDateFormat_ReturnDateObject() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        //Month in Calender class is 0th-base
        calendar.set(2017, 0, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date date = calendar.getTime();
        assertEquals(date, Utility.getFormattedDate("01-01-2017", Constants.UI_DATE_FORMAT));
    }


    @Test(expected = ParseException.class)
    public void getFormattedDateTest_PassingDateStringWithInvalidDateFormat_ReturnParseException() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        //Month in Calender class is 0th-base
        calendar.set(2017, 0, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date date = calendar.getTime();
        fail(String.valueOf(Utility.getFormattedDate("01-01-2017", "dd/MM-yyyy")));
    }


    @Test
    public void getCorrectOffsetMonthTest_PassingValidMonthWithZeroBase_ReturnMonthPlusOne() {
        assertEquals(1, Utility.getCorrectOffsetMonth(0));
        assertEquals(2, Utility.getCorrectOffsetMonth(1));
    }


    @Test
    public void getCorrectOffsetMonthTest_PassingInValidMonthWithZeroBase_ReturnMonthOneOrTwelve() {
        assertEquals(1, Utility.getCorrectOffsetMonth(-2));
        assertEquals(12, Utility.getCorrectOffsetMonth(12));
        assertEquals(1, Utility.getCorrectOffsetMonth(0));
        assertEquals(1, Utility.getCorrectOffsetMonth(-1));
        assertEquals(12, Utility.getCorrectOffsetMonth(11));
        assertEquals(12, Utility.getCorrectOffsetMonth(12));
    }


    @Test
    public void getTwoDigitIntegerTest_PassingValidInput_ReturnIntegerWithTwoDigit() {
        assertEquals("00", Utility.getTwoDigitInteger(0));
        assertEquals("07", Utility.getTwoDigitInteger(7));
        assertEquals("10", Utility.getTwoDigitInteger(10));
    }

    @Test
    public void getTwoDigitIntegerTest_PassingInvalidInput_ReturnSameInteger() {
        assertEquals("111", Utility.getTwoDigitInteger(111));
    }


    @Test
    public void getFormattedAmountWithoutDecimalsTest_PassingValidInput_ReturnFormattedAmount() {
        assertEquals("PKR 100", Utility.getFormattedAmountWithoutDecimals(100));
        assertEquals("PKR 0", Utility.getFormattedAmountWithoutDecimals(0));
    }

    @Test
    public void getFormattedAmountWithoutCurrencyLabelAndDecimalsTest_PassingValidInput_ReturnFormattedAmount() {
        assertEquals("100", Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(100));
        assertEquals("0", Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(0));
    }

    @Test
    public void getFormattedAmountWithoutDecimals_PassingValidInput_ReturnFormattedAmount() {
        assertEquals("PKR 100", Utility.getFormattedAmountWithoutDecimals(100));
        assertEquals("PKR 0", Utility.getFormattedAmountWithoutDecimals(0));
    }


    @Test
    public void getFormattedDateTest_PassingValidInput_ReturnDateWithOutputFormat() {
        assertEquals("21/03/2017", Utility.getFormattedDate("21-03-2017", Constants.UI_DATE_FORMAT, "dd/MM/2017"));
        assertEquals("21-03/2017", Utility.getFormattedDate("21-03-2017", Constants.UI_DATE_FORMAT, "dd-MM/2017"));
        assertEquals("03/2017", Utility.getFormattedDate("21-03-2017", Constants.UI_DATE_FORMAT, "MM/2017"));

    }


    @Test
    public void isUnauthorizedTest_PassingValidInput() {
        assertEquals(true, Utility.isUnauthorized(Constants.UNAUTHORIZED_ERROR_CODE));
    }


    @Test
    public void getBimaRelationsTest_ComparingNotNull() {

    }

    @Test
    public void getBimaRelationForRelationShipTest_PassingValidRelationShip_ReturnRelationshipDetails() {
        //assertEquals("Father", Utility.getBimaRelationForRelationShip("Father").getRelationship());
    }



    @Test
    public void getFormattedMobileNumberTest_PassingNull_ReturnNull() {
        assertNull(Utility.getFormattedMobileNumberWithDashes(null));
    }

    @Test
    public void getFormattedMobileNumberTest_PassingValidInput_ReturnFormattedNumber() {
        assertEquals("0345-2076766", Utility.getFormattedMobileNumberWithDashes("0345 2076766"));
        assertEquals("0345-2076766", Utility.getFormattedMobileNumberWithDashes("+9234520 7 6766"));
        assertEquals("0345-2076766", Utility.getFormattedMobileNumberWithDashes("+92 3 4 5 2076766"));
    }

    @Test
    public void getFormattedMobileNumberTest_PassingInvalidInput_ReturnFormattedNumber() {
        assertEquals("0207-6766", Utility.getFormattedMobileNumberWithDashes("02076766"));
        assertEquals("+973-45276766", Utility.getFormattedMobileNumberWithDashes("+973452 7 6766"));
        assertEquals("0000-223452076766", Utility.getFormattedMobileNumberWithDashes("000022 3 4 5 2076766"));
    }


    @Test
    public void isDateExpiredTest_PassingNullValues_ReturnFalse() {
        assertFalse(Utility.isDateExpired(null, null, null));
        assertFalse(Utility.isDateExpired("21-02-2017", null, null));
        assertFalse(Utility.isDateExpired(null, "mm-dd-yyyy", null));
    }

    @Test(expected = AssertionFailedError.class)
    public void isDateExpiredTest_PassingInvalidInput_ThrowsParseException() {
        fail(String.valueOf(Utility.isDateExpired("21-02-2017", "mm-dd-yyyy", null)));
    }

    @Test(expected = AssertionFailedError.class)
    public void isDateOfBirthValidTest_PassingNullValues_ReturnFalse() {
        fail(String.valueOf(Utility.isDateOfBirthInvalidOrLessThanEightYears(null, null)));
    }




    @Test
    public void getWalletIconResourceIdTest_PassingInvalidValue_Return0() {
        assertEquals(0, Utility.getWalletIconResourceId(-1));
    }

    @Test
    public void getWalletIconResourceIdTest_PassingValidValue_ReturnValidResourceId() {
        assertEquals(0, Utility.getWalletIconResourceId(-1));
        assertEquals(R.drawable.logo_easypaisa, Utility.getWalletIconResourceId(Constants.EASYPAISA_ID));
        assertEquals(R.drawable.logo_simsim, Utility.getWalletIconResourceId(Constants.SIMSIM_ID));
    }



    @Test
    public void isValidName_PassingValidInput_ReturnsTrue() {
        assertEquals(true, Utility.isValidName("Rehman"));
        assertEquals(true, Utility.isValidName("Farhan Dhanani"));
    }

    @Test
    public void isValidName_PassingInvalidInput_ReturnsFalse() {
        assertEquals(false, Utility.isValidName("Jhonny<>"));
        assertEquals(false, Utility.isValidName("Sunny Kamran;;;"));
    }


    @Test
    public void formatMobileNumberInitials_PassingValidInput_ReturnsNumberReplacingZeroWithPakistanCode() {
        assertEquals("+923345424244", Utility.formatMobileNumberInitials("03345424244"));
        assertEquals("+923663453689", Utility.formatMobileNumberInitials("03663453689"));
    }



    @Test
    public void formatDate_PassingValidInput_ReturnDateWithFormat() {
        assertEquals("2016-04-22", Utility.formatDate("22/04/2016", "dd/MM/yyyy"));
        assertEquals("2011-04-22", Utility.formatDate("22-04/2011","dd-MM/yyyy"));
    }









}
