/*
package com.tez.androidapp.features;

import androidx.test.espresso.intent.rule.IntentsTestRule;

import com.tez.androidapp.rewamp.signup.SelectLanguageActivity;
import com.tez.androidapp.steps.Actions;
import com.tez.androidapp.steps.ScreenShotSteps;
import com.tez.androidapp.steps.TezOnboardingActivitySteps;
import com.tez.latte.Scenario;
import com.tez.latte.ScenarioConfig;
import com.tez.latte.TezLatteConfig;
import com.tez.latte.TezLatteTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.IOException;
import java.util.Locale;

*/
/**
 * Created by FARHAN DHANANI on 6/12/2019.
 *//*


@RunWith(Parameterized.class)
public class SelectLanguageTest extends TezLatteTest {

    @Rule
    public IntentsTestRule<SelectLanguageActivity> activity = new IntentsTestRule<>(SelectLanguageActivity.class);

    public SelectLanguageTest(ScenarioConfig scenario) {
        super(scenario);
    }

    @Parameters(name = "{0}")
    public static Iterable<ScenarioConfig> scenarios() throws IOException
    {
        return new TezLatteConfig()
                .withFeatureFromAssets("assets/features/SL.feature")
                .takeScreenshotOnFail()
                .scenarios();
    }

    @Test
    public void test()
    {
        start(new Actions(),
                new TezOnboardingActivitySteps(),
                new ScreenShotSteps());
    }

    @Override
    protected void beforeScenarioStarts(Scenario scenario, Locale locale)
    {
        // do something
    }
}
*/
