package com.tez.androidapp.activities;

import androidx.test.espresso.ViewInteraction;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.signup.SelectLanguageActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

/**
 *
 * Created by FARHAN DHANANI on 6/10/2019.
 */
@RunWith(AndroidJUnit4.class)
public class SelectLanguageActivityTest  {

    @Rule
    public final ActivityTestRule<SelectLanguageActivity> mActivityTestRule =
            new ActivityTestRule<>(SelectLanguageActivity.class);

    @Test
    public void verifyText() {
        ViewInteraction viewInteraction = onView(withId(R.id.buttonEnglish))
                .perform(click());
        viewInteraction.perform(click());
        onView(withId(R.id.btMainButton))
                .perform(click());
    }
}
