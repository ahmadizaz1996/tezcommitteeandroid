package com.tez.androidapp.activities;

import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.signup.TezOnboardingActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
/**
 * Created by FARHAN DHANANI on 6/10/2019.
 */
@RunWith(AndroidJUnit4.class)
public class TezOnboardingActivityTest {

    @Rule
    public final ActivityTestRule<TezOnboardingActivity> mActivityTestRule =
            new ActivityTestRule<>(TezOnboardingActivity.class);

    @Test
    public void verifyText() {
        onView(withId(R.id.btMainButton))
                .perform(click());

    }
}
