/*
package com.tez.androidapp.steps;

import com.tez.androidapp.Commands;
import com.tez.androidapp.R;
import com.tez.latte.TezLatteSteps;
import com.tez.latte.annotations.Given;
import com.tez.latte.annotations.Then;
import com.tez.latte.annotations.When;

*/
/**
 * Created by FARHAN DHANANI on 6/12/2019.
 *//*


public class Actions extends TezLatteSteps {


    @When(Commands.REGEX_FOR_TAP_ON_VIEW_WITH_TEXT)
    public void whenIPressViewWithText(String text) {
        pressViewWithText(text);
    }

    @Then(Commands.REGEX_FOR_TAP_ON_VIEW_WITH_TEXT)
    public void thenIPressButtonWithText(String text) {
        pressViewWithText(text);
    }

    @Then(Commands.REGEX_FOR_FACEBOOK_TAP)
    public void pressFacebook() {
        closeKeyboard();
        onViewWithId(R.id.btFacebook).click();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Then(Commands.REGEX_FOR_VIEW_MUST_APPEAR_WITH_TEXT)
    public void thenIMustSeeButtonWithText(String text) {
        onViewWithText(text).isDisplayed();
    }

    @Given(Commands.REGEX_FOR_API_CALL_WITH_CORRECT_RESPONSE)
    public void callApiWithGivenCorrectResponse(String url, String requiredResponseFileName) {
        mockApiResponseWithJsonBody(url, requiredResponseFileName, 200);
    }

    @Given(Commands.REGEX_FOR_API_CALL_WITH_ERROR_AND_GIVEN_BODY_IN_RESPONSE)
    public void callApiWithGivenErrorAndBodyInResponse(String url, String requiredResponseFileName, int statusCode) {
        mockApiResponseWithJsonBody(url, requiredResponseFileName, statusCode);
    }

    @Given(Commands.REGEX_FOR_CALL_API_WITH_REQUEST_AND_CORRECT_RESPONSE)
    public void callApiGivenWithRequestAndCorrectResponse(String url,
                                                          String requiredRequestFileName, String requiredResponseFileName) {
        mockApiWithRequestAndJsonResponse(url, requiredResponseFileName,
                requiredRequestFileName, 200);
    }

    @Given(Commands.REGEX_FOR_API_CALL_WITH_REQUEST_AND_ERROR_WITH_BODY_IN_RESPONSE)
    public void callApiWithGivenRequestAndBodyWithGivenErrorInResponse(String url, String requiredRequestFileName,
                                                                       int statusCode, String requiredResponseFileName) {
        mockApiWithRequestAndJsonResponse(url, requiredResponseFileName,
                requiredRequestFileName, statusCode);
    }

    @Given(Commands.REGEX_FOR_API_CALL_WITH_ERROR_CODE)
    public void callApiWithGivenErrorInRespnse(String url, int statusCode) {
        mockApiWithErrorAndEmptyResponse(url, statusCode);
    }

    @Given(Commands.REGEX_FOR_API_CALL_WITH_REQUEST_AND_ERROR_IN_RESPONSE)
    public void callApiWithGivenRequestAndErrorInRepsonse(String url, String requiredRequestFileName, int statusCode) {
        mockApiWithRequestAndEmptyResponseWithGivenError(url, requiredRequestFileName, statusCode);
    }

    private void pressViewWithText(String text) {
        onViewWithText(text).click();
    }
}
*/
