# DexGuard's default settings are fine for this sample application.

# We'll just display some more statistics about the processed code.
-verbose

# If you encounter problems in your project, you can let DexGuard instrument
# your code, so the app prints out configuration suggestions at run-time, in
# the logcat:
#
#-addconfigurationdebugging

# Otherwise, you can try to narrow down the issue by disabling some processing
# steps:
#
#-dontshrink
#-dontoptimize
#-dontobfuscate

# You can also check if the problem can be solved by keeping additional
# code and/or resources:
#
#-keep class * { *; }
#-keepattributes *
#
#-keepresources */*
#-keepresourcefiles res/**
#-keepresourcefiles assets/**
#-keepresourcefiles lib/**
#-keepresourcexmlelements **
#-keepresourcexmlattributenames **

# These are really crude settings that you shouldn't use for building
# actual releases, but it's easier to start from a working configuration,
# which you can then refine.
-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions


-dontwarn me.everything.providers.android.browser.Bookmark
-dontwarn me.everything.providers.android.browser.Search
-dontwarn okio.Okio
-dontwarn okio.DeflaterSink
-dontwarn com.squareup.okhttp.**

-keep class com.google.gson.** { *; }
-keep class com.google.inject.** { *; }
-keep class org.apache.http.** { *; }
-keep class org.apache.james.mime4j.** { *; }
-keep class javax.inject.** { *; }
-dontwarn rx.**
-dontwarn okio.**

-keep class com.tez.androidapp.repository.network.** { *; }
-keep class com.tez.androidapp.utils.extract.models.** { *; }
-keep class com.tez.androidapp.models.** { *; }
-keep class me.everything.providers.** { *; }

-dontwarn com.squareup.okhttp.**
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }

-dontwarn retrofit.**
-dontwarn retrofit.appengine.UrlFetchClient
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}

-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception

-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**



-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepclassmembers class com.sinch.** extends android.os.AsyncTask {
    *;
}

-keep,includedescriptorclasses class com.sinch.** {
    *;
}




