package net.tez.fragment.util.effect;

import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

/**
 * Created by VINOD KUMAR on 7/19/2019.
 */
public abstract class TouchEffectListener implements View.OnTouchListener {

    private int[] location;
    private long lastTapMs;
    private boolean isActionMove;
    private Rect rect;

    public TouchEffectListener() {
        this.rect = new Rect();
        this.location = new int[2];
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (event.getActionMasked()) {

            case MotionEvent.ACTION_DOWN:
                isActionMove = false;
                if (isNoDoubleTap())
                    onTouchEffect(v);
                break;

            case MotionEvent.ACTION_MOVE:
                if (isTouchMovedOutsideBound(v, event)) {
                    isActionMove = true;
                    onNormalEffect(v);
                }
                break;

            case MotionEvent.ACTION_UP:
                if (isNoSwipe()) {
                    onNormalEffect(v);
                    if (isNoDoubleTap())
                        v.performClick();
                }
                lastTapMs = System.currentTimeMillis();
                break;

            case MotionEvent.ACTION_CANCEL:
                if (isNoSwipe())
                    onNormalEffect(v);
                lastTapMs = System.currentTimeMillis();
                break;

            default:
                onNormalEffect(v);
                lastTapMs = System.currentTimeMillis();
                break;
        }

        return true;
    }

    private boolean isTouchMovedOutsideBound(View view, MotionEvent event) {
        view.getDrawingRect(rect);
        view.getLocationOnScreen(location);
        rect.offset(location[0], location[1]);
        return !rect.contains((int) event.getRawX(), (int) event.getRawY());
    }

    private boolean isNoSwipe() {
        return !isActionMove;
    }

    private boolean isNoDoubleTap() {
        return System.currentTimeMillis() - lastTapMs >= ViewConfiguration.getDoubleTapTimeout();
    }

    public abstract void onTouchEffect(View view);

    public abstract void onNormalEffect(View view);
}
