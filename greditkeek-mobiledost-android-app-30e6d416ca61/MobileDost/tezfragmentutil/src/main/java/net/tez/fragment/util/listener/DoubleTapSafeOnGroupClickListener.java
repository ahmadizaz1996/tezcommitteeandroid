package net.tez.fragment.util.listener;

import android.view.View;
import android.widget.ExpandableListView;

import net.tez.fragment.util.Constants;

/**
 * Created by VINOD KUMAR on 4/22/2019.
 */
public interface DoubleTapSafeOnGroupClickListener extends ExpandableListView.OnGroupClickListener {

    boolean doubleTapSafeOnGroupClick(ExpandableListView parent, View v, int groupPosition, long id);

    @Override
    default boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        synchronized (Holder.LOCK) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - Holder.clickTime > Constants.CLICK_THRESHOLD_MS) {
                Holder.clickTime = currentTime;
                return doubleTapSafeOnGroupClick(parent, v, groupPosition, id);
            }
            return false;
        }
    }


    class Holder {
        private static final Object LOCK = new Object();
        private static long clickTime = 0;
    }
}
