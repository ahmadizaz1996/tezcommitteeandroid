package net.tez.fragment.util;

/**
 * Created by VINOD KUMAR on 4/19/2019.
 */
public final class Constants {

    public static final long CLICK_THRESHOLD_MS = 1000;
    public static final String BROADCAST_LOG_CLICK_EVENT = "BROADCAST_LOG_CLICK_EVENT";
    public static final String BUTTON_NAME = "BUTTON_NAME";

    private Constants() {

    }
}
