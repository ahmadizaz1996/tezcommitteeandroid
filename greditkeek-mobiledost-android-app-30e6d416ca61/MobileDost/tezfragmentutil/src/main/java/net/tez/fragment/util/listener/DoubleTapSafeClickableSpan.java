package net.tez.fragment.util.listener;

import androidx.annotation.NonNull;
import android.text.style.ClickableSpan;
import android.view.View;

import net.tez.fragment.util.Constants;

/**
 * Created by VINOD KUMAR on 4/18/2019.
 */
public abstract class DoubleTapSafeClickableSpan extends ClickableSpan {

    private static long clickTime = 0;

    protected abstract void doubleTapSafeSpanOnClick(@NonNull View widget);

    @Override
    public final void onClick(@NonNull View widget) {
        long currentTime = System.currentTimeMillis();
        if (currentTime - clickTime > Constants.CLICK_THRESHOLD_MS) {
            clickTime = currentTime;
            doubleTapSafeSpanOnClick(widget);
        }
    }
}
