package net.tez.fragment.util.listener;

import android.content.Intent;
import android.view.View;

import net.tez.fragment.util.Constants;
import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.optional.TextUtil;

/**
 * Created by VINOD KUMAR on 4/19/2019.
 */
public interface DoubleTapSafeOnClickListener extends View.OnClickListener {

    void doubleTapSafeOnClick(View view);


    @Override
    default void onClick(View v) {
        synchronized (Holder.LOCK) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - Holder.clickTime > Constants.CLICK_THRESHOLD_MS) {
                Holder.clickTime = currentTime;
                sendBroadcast(v);
                doubleTapSafeOnClick(v);
            }
        }
    }

    default void sendBroadcast(View v) {
        try {
            Intent intent = new Intent(Constants.BROADCAST_LOG_CLICK_EVENT);
            if (v instanceof IBaseWidget) {
                String label = ((IBaseWidget) v).getLabel();
                if (TextUtil.isNotEmpty(label)) {
                    intent.putExtra(Constants.BUTTON_NAME, label);
                }

                v.getContext().sendBroadcast(intent);
            }
        } catch (Exception ignore) {

        }
    }

    class Holder {
        private static final Object LOCK = new Object();
        private static long clickTime = 0;
    }
}
