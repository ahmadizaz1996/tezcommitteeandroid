package net.tez.fragment.util.actions;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by VINOD KUMAR on 10/10/2018.
 */

@FunctionalInterface
public interface Functionable1<I, O> {

    @Nullable O apply(@NonNull I t);
}
