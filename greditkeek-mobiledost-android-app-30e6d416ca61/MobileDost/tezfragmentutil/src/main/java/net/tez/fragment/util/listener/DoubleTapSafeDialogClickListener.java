package net.tez.fragment.util.listener;

import android.app.Dialog;
import android.content.DialogInterface;

import net.tez.fragment.util.Constants;

/**
 * Created by VINOD KUMAR on 4/11/2019.
 */
public interface DoubleTapSafeDialogClickListener extends Dialog.OnClickListener {


    void doubleTapSafeDialogClick(DialogInterface dialog, int which);


    @Override
    default void onClick(DialogInterface dialog, int which) {
        long currentTime = System.currentTimeMillis();
        if (currentTime - Holder.clickTime > Constants.CLICK_THRESHOLD_MS) {
            Holder.clickTime = currentTime;
            doubleTapSafeDialogClick(dialog, which);
        }
    }

    class Holder {
        private static long clickTime = 0;
    }
}
