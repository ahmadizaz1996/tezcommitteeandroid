package net.tez.fragment.util.actions;

import androidx.annotation.NonNull;

/**
 * Created by VINOD KUMAR on 10/3/2018.
 */
@FunctionalInterface
public interface Actionable1<T>{

    void action(@NonNull T t);
}
