package net.tez.fragment.util.actions;

import androidx.annotation.Nullable;

/**
 * Created by VINOD KUMAR on 10/16/2018.
 */
@FunctionalInterface
public interface Functionable0<O> {

    @Nullable
    O apply();
}
