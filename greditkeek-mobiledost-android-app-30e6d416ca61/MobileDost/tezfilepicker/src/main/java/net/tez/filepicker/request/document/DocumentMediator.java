package net.tez.filepicker.request.document;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import net.tez.filepicker.FilePickerActivity;
import net.tez.filepicker.base.BaseMediator;
import net.tez.filepicker.request.ClientRequest;
import net.tez.filepicker.utils.Constants;

/**
 * Created by VINOD KUMAR on 3/28/2019.
 */
public class DocumentMediator extends BaseMediator implements DocumentFilePicker {

    DocumentMediator() {
        super(new ClientRequest());
    }

    @Override
    public void pickDocument(@NonNull Activity activity) {
        clientRequest.setRequestImage(false);
        Intent intent = new Intent(activity, FilePickerActivity.class);
        intent.putExtra(Constants.CLIENT_REQUEST, clientRequest);
        activity.startActivityForResult(intent, DocumentPickerRequestBuilder.REQUEST_CODE_DOCUMENT);
    }

    @Override
    public void pickDocument(@NonNull Fragment fragment) {
        clientRequest.setRequestImage(false);
        Context context = fragment.getContext();
        if (context != null) {
            Intent intent = new Intent(context, FilePickerActivity.class);
            intent.putExtra(Constants.CLIENT_REQUEST, clientRequest);
            fragment.startActivityForResult(intent, DocumentPickerRequestBuilder.REQUEST_CODE_DOCUMENT);
        }
    }
}
