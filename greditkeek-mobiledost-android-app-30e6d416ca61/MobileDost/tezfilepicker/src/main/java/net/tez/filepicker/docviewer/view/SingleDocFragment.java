package net.tez.filepicker.docviewer.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vendvinodkumar.tezfilepicker.R;

import net.tez.filepicker.base.BaseFragment;
import net.tez.filepicker.docviewer.adapter.DocListAdapter;
import net.tez.filepicker.docviewer.listener.DocSelectListener;
import net.tez.filepicker.docviewer.model.MediaDocument;
import net.tez.filepicker.utils.MediaUtil;
import net.tez.fragment.util.optional.Optional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VINOD KUMAR on 3/28/2019.
 */
public class SingleDocFragment extends BaseFragment implements DocSelectListener {

    private String title;
    private String[] extensionArray;
    private RecyclerView rvDocItem;

    public static SingleDocFragment newInstance(String title, String[] extension) {
        SingleDocFragment singleDocFragment = new SingleDocFragment();
        singleDocFragment.setTitle(title);
        singleDocFragment.setExtensionArray(extension);
        return singleDocFragment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getExtensionArray() {
        return extensionArray;
    }

    public void setExtensionArray(String[] extensionArray) {
        this.extensionArray = extensionArray;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_single_document, container, false);
        rvDocItem = view.findViewById(R.id.rvDocItem);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getBaseFragmentContext(context -> {

                    List<MediaDocument> documentList = MediaUtil.getDocuments(context, getExtensionArray());

                    Optional.doWhen(isEmptyList(documentList),
                            () -> {
                                rvDocItem.setVisibility(View.GONE);
                                view.findViewById(R.id.cvNoDocFoundError).setVisibility(View.VISIBLE);
                            },
                            () -> {
                                rvDocItem.setLayoutManager(new LinearLayoutManager(context));
                                rvDocItem.setAdapter(new DocListAdapter(documentList, this, getExtensionArray()));
                            });

                });
    }

    @Override
    public void OnDocSelected(MediaDocument document) {
        List<String> documentPaths = new ArrayList<>();
        documentPaths.add(document.getPath());
        Optional.ifPresent(getHostActivity(), hostActivity -> {
            hostActivity.onDocumentSelected(documentPaths);
        });
    }
}
