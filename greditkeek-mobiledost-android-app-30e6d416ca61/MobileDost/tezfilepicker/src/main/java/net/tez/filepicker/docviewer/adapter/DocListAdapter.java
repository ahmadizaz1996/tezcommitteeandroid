package net.tez.filepicker.docviewer.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.example.vendvinodkumar.tezfilepicker.R;

import net.tez.filepicker.docviewer.listener.DocSelectListener;
import net.tez.filepicker.docviewer.model.MediaDocument;
import net.tez.filepicker.utils.MediaUtil;
import net.tez.filepicker.utils.Util;
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;

import java.util.List;

/**
 * Created by VINOD KUMAR on 3/28/2019.
 */
public class DocListAdapter extends GenericRecyclerViewAdapter<MediaDocument, DocSelectListener, DocListAdapter.DocViewHolder> {

    private String[] docExtensionArray;

    public DocListAdapter(List<MediaDocument> items, DocSelectListener listener, String[] docExtensionArray) {
        super(items, listener);
        this.docExtensionArray = docExtensionArray;
    }

    @NonNull
    @Override
    public DocViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DocViewHolder(inflate(parent.getContext(), R.layout.list_item_doc, parent));
    }

    class DocViewHolder extends BaseViewHolder<MediaDocument, DocSelectListener> {

        private final AppCompatImageView ivDocImage;
        private final AppCompatTextView tvDocName;
        private final AppCompatTextView tvDocSize;

        DocViewHolder(View itemView) {
            super(itemView);
            this.ivDocImage = itemView.findViewById(R.id.ivDocImage);
            this.tvDocName = itemView.findViewById(R.id.tvDocName);
            this.tvDocSize = itemView.findViewById(R.id.tvDocSize);
        }

        @Override
        public void onBind(MediaDocument item, @Nullable DocSelectListener listener) {
            super.onBind(item, listener);
            this.ivDocImage.setImageResource(getExtensionDrawable(docExtensionArray));
            this.tvDocName.setText(item.getName());
            this.tvDocSize.setText(getSize(item.getSize()));
            if (listener != null)
                this.setItemViewOnClickListener(view -> {
                    view.startAnimation(AnimationUtils.loadAnimation(view.getContext(), R.anim.anim_item_clik));
                    listener.OnDocSelected(item);
                });
        }

        @DrawableRes
        private int getExtensionDrawable(String[] extensionArray) {

            for (String extension : extensionArray) {
                if (extension.matches(MediaUtil.PDF_EXT))
                    return R.drawable.ic_pdf;

                if (extension.matches(MediaUtil.DOC_EXT))
                    return R.drawable.ic_word;

                if (extension.matches(MediaUtil.DOCX_EXT))
                    return R.drawable.ic_word;

                if (extension.matches(MediaUtil.TXT_EXT))
                    return R.drawable.ic_txt;
            }

            return R.drawable.ic_txt;
        }

        private String getSize(String size) {
            int byteSize = Integer.valueOf(size);
            if (byteSize > 1000) {
                int kbSize = Util.scaleUpByte(byteSize);
                if (kbSize > 1000) {
                    int mbSize = Util.scaleUpByte(kbSize);
                    return mbSize + "MB";
                } else
                    return kbSize + "KB";
            }
            return byteSize + "B";
        }
    }
}
