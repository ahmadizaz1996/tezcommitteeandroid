package net.tez.filepicker.docviewer.adapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import net.tez.filepicker.docviewer.view.SingleDocFragment;

import java.util.List;

/**
 * Created by VINOD KUMAR on 3/28/2019.
 */
public class DocsPagerAdapter extends FragmentPagerAdapter {

    private List<SingleDocFragment> data;


    public DocsPagerAdapter(FragmentManager fm, List<SingleDocFragment> data) {
        super(fm);
        this.data = data;
    }

    public void add(SingleDocFragment singleDocFragment) {
        this.data.add(singleDocFragment);
    }

    @Override
    public Fragment getItem(int i) {
        return data != null ? data.get(i) : null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return data != null ? data.get(position).getTitle() : "";
    }

    @Override
    public int getCount() {
        return data != null ? data.size() : 0;
    }
}
