package net.tez.filepicker.imageviewer.listener;

import net.tez.filepicker.imageviewer.model.MediaImage;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;

import java.util.List;

/**
 * Created by VINOD KUMAR on 3/27/2019.
 */
public interface ImageSelectListener extends BaseRecyclerViewListener {

    void onImageCheckedChange(boolean isChecked);

    void onImageSelected(MediaImage mediaImage);

    void onImagesSelected(List<String> imageList);
}
