package net.tez.filepicker;

import androidx.annotation.IdRes;
import android.view.View;

import net.tez.filepicker.base.BaseFragment;
import net.tez.filepicker.request.ClientRequest;

import java.util.List;

/**
 * Created by VINOD KUMAR on 3/28/2019.
 */
public interface HostActivity {

    void registerFragment(BaseFragment baseFragment);

    void onImageSelected(List<String> mediaImageList);

    void onDocumentSelected(List<String> mediaDocumentList);

    ClientRequest getClientRequest();

    View getHostActivityViewById(@IdRes int id);
}
