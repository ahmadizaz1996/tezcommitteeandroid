package net.tez.filepicker.imageviewer.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vendvinodkumar.tezfilepicker.R;

import net.tez.filepicker.base.BaseFragment;
import net.tez.filepicker.imageviewer.adapter.ImagesGridAdapter;
import net.tez.filepicker.imageviewer.listener.ImageSelectListener;
import net.tez.filepicker.imageviewer.model.MediaImage;
import net.tez.filepicker.request.ClientRequest;
import net.tez.filepicker.utils.MediaUtil;
import net.tez.filepicker.utils.Util;
import net.tez.fragment.util.optional.Optional;
import net.tez.fragment.util.optional.TextUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VINOD KUMAR on 3/27/2019.
 */
public class ImagePickerFragment extends BaseFragment implements ImageSelectListener {

    private RecyclerView rvImages;
    private AppCompatTextView tvSelectCount;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_picker, container, false);

        rvImages = view.findViewById(R.id.rvImages);

        Optional.isInstanceOf(getHostActivityViewById(R.id.tvSelectCount),
                AppCompatTextView.class,
                appCompatTextView -> tvSelectCount = appCompatTextView);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final ClientRequest clientRequest = getClientRequest();

        getBaseFragmentContext(context -> {
            Optional.doWhen(isSafeObject(clientRequest) && getClientRequest().getMaxImageCount() > 0,
                    () -> {

                        int col = Util.calculateNoOfColumns(context, 125);

                        boolean isSingleImage = getClientRequest().getMaxImageCount() == 1;

                        ImagesGridAdapter adapter = new ImagesGridAdapter(MediaUtil.getImages(context),
                                this,
                                isSingleImage,
                                getClientRequest().getMaxImageCount());

                        rvImages.setLayoutManager(new GridLayoutManager(context, col));
                        rvImages.setAdapter(adapter);

                        configureDoneButton(isSingleImage, adapter);
                    },
                    this::finishActivity);
        });
    }

    @Override
    public void onImageCheckedChange(boolean isChecked) {
        if (tvSelectCount != null && tvSelectCount.getText() != null) {

            String maxImageCount = String.valueOf(getClientRequest().getMaxImageCount());

            if (TextUtil.isEmpty(tvSelectCount.getText())) {
                String string = "1/" + maxImageCount;
                tvSelectCount.setText(string);
                return;
            }

            String text = tvSelectCount.getText().toString();
            String[] splitText = text.split("/");

            int count = Integer.valueOf(splitText[0]);

            if (isChecked)
                count++;
            else
                count--;

            updateCount(count, maxImageCount);
        }
    }

    @Override
    public void onImageSelected(MediaImage mediaImage) {
        List<String> imagePaths = new ArrayList<>();
        imagePaths.add(mediaImage.getPath());
        sendResultsToHostActivity(imagePaths);
    }

    @Override
    public void onImagesSelected(List<String> imageList) {
        this.sendResultsToHostActivity(imageList);
    }

    private void configureDoneButton(boolean isSingleImage, ImagesGridAdapter adapter) {
        Optional.ifPresent(getHostActivityViewById(R.id.tvDone),
                tvDone -> {

                    Optional.doWhen(isSingleImage,
                            () -> tvDone.setVisibility(View.GONE),

                            () -> tvDone.setOnClickListener(view1 -> {
                                tvDone.setEnabled(false);
                                sendResultsToHostActivity(adapter.getCheckedImagesPaths());
                            }));
                });
    }

    private void updateCount(int count, String maxImageCount) {
        Optional.doWhen(count > 0,
                () -> {
                    String updatedCount = count + "/" + maxImageCount;
                    tvSelectCount.setText(updatedCount);
                },
                () -> tvSelectCount.setText(""));
    }

    private void sendResultsToHostActivity(List<String> imagePaths) {
        Optional.ifPresent(getHostActivity(), hostActivity -> {
            hostActivity.onImageSelected(imagePaths);
        });
    }
}
