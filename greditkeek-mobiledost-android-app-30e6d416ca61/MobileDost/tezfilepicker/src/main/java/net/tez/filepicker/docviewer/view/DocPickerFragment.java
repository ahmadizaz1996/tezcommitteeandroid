package net.tez.filepicker.docviewer.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.example.vendvinodkumar.tezfilepicker.R;
import com.google.android.material.tabs.TabLayout;

import net.tez.filepicker.base.BaseFragment;
import net.tez.filepicker.docviewer.adapter.DocsPagerAdapter;
import net.tez.fragment.util.optional.Optional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by VINOD KUMAR on 3/28/2019.
 */
public class DocPickerFragment extends BaseFragment {

    private ViewPager docsViewPager;
    private TabLayout docsTabLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doc_picker, container, false);
        docsViewPager = view.findViewById(R.id.docsViewPager);
        docsTabLayout = view.findViewById(R.id.docsTabLayout);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Optional.ifPresent(getHostActivityViewById(R.id.tvSelectCount),
                tvSelectCount -> {
                    tvSelectCount.setVisibility(View.GONE);
                });

        Optional.ifPresent(getHostActivityViewById(R.id.tvDone),
                tvDone -> {
                    tvDone.setVisibility(View.GONE);
                });

        List<SingleDocFragment> singleDocFragments = new ArrayList<>();
        Map<String, String[]> fileDetails = getClientRequest().getFileDetails();

        if (!isSafeObject(fileDetails)) {
            finishActivity();
            return;
        }

        for (Map.Entry entry : fileDetails.entrySet()) {
            String title = (String) entry.getKey();
            String[] extensionArray = (String[]) entry.getValue();
            singleDocFragments.add(SingleDocFragment.newInstance(title, extensionArray));
        }

        DocsPagerAdapter docsPagerAdapter = new DocsPagerAdapter(getChildFragmentManager(), singleDocFragments);
        docsViewPager.setAdapter(docsPagerAdapter);
        docsTabLayout.setupWithViewPager(docsViewPager);
    }
}
