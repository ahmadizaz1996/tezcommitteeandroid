package net.tez.storage.library.strategy;

import androidx.annotation.NonNull;

/**
 * Created by FARHAN DHANANI on 1/17/2019.
 */
public interface TextTransformStrategy {

    @NonNull
    String transformOnPut(@NonNull String value);
    String transformOnGet(@NonNull String value);
}
