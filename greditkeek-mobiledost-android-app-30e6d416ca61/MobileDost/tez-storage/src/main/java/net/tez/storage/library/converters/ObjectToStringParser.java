package net.tez.storage.library.converters;

import androidx.annotation.NonNull;

/**
 * Created by FARHAN DHANANI on 5/7/2018.
 */

public interface ObjectToStringParser {
    String convertToString(@NonNull Object obj);
}
