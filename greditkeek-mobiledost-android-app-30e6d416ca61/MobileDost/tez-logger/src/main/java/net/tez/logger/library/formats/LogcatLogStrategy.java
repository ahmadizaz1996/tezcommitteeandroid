package net.tez.logger.library.formats;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;

import net.tez.logger.library.core.LogStrategy;

/**
 * Created by FARHAN DHANANI on 5/10/2018.
 */

public class LogcatLogStrategy implements LogStrategy {

    @Override
    public void log(int priority, @Nullable String tag, @NonNull String message) {
        Log.println(priority, tag == null ? "TEZ_LOG" : tag, message);
    }
}
