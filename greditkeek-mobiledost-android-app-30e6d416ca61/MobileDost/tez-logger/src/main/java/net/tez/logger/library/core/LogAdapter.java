package net.tez.logger.library.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by FARHAN DHANANI on 5/10/2018.
 */

public interface LogAdapter {

    boolean isLoggable(int priority, @Nullable String tag);
    void log(int priority, @Nullable String tag, @NonNull String message);
}
