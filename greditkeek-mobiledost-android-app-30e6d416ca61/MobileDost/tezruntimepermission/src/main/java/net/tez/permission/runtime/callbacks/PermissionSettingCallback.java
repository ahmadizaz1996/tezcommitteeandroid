package net.tez.permission.runtime.callbacks;

import androidx.annotation.NonNull;

import net.tez.permission.runtime.models.Perm;

/**
 * Created by Vinod Kumar on 10/13/2018.
 */
public interface PermissionSettingCallback<T> {
    void onGranted(@NonNull Perm<T> perm);
    void onDenied(@NonNull Perm<T> perm);
}
