package net.tez.permission.runtime.models;

/**
 * Created by Vinod Kumar on 5/8/2018.
 */

public interface Perm<T> {

    String getType();
    int getRequestCode();
    T getCaller();
}
