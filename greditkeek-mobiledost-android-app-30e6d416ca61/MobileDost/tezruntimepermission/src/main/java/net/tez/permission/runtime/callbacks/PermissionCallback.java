package net.tez.permission.runtime.callbacks;

import net.tez.permission.runtime.models.Perm;

/**
 * Created by Vinod Kumar on 5/8/2018.
 */

public interface PermissionCallback<T> {
    void onGranted(Perm<T> perm);
    void onDenied(Perm<T> perm);
    void onPermanentDenied(Perm<T> perm);
}
