package net.tez.viewbinder.library.core;

import android.app.Dialog;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


/**
 * Created by FARHAN DHANANI on 8/6/2018.
 */
public interface ClassFilter {

    default Class<?> getActivityClass() {
        return AppCompatActivity.class;
    }
    default Class<?> getDialogClass() {
        return Dialog.class;
    }
    default Class<?> getViewClass() {
        return View.class;
    }
    default Class<?> getSupportFragmentClass() { return Fragment.class; }
    @SuppressWarnings("deprecation")
    default Class<?> getFragmentClass() { return android.app.Fragment.class; }
    Class<?> getViewHolderClass();
}