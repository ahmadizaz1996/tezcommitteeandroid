package com.example;

import com.google.gson.annotations.*;

public class Addressee {

    @SerializedName("address")
    private Address address;

    public Addressee(Address address) {
        this.address = address;
    }


    public Address getAddress() {
        return address;
    }
}
