# DexGuard's default settings are fine for this sample application,
# but we'll add some more layers of obfuscation to the license checking parts.

# Display some more statistics about the processed code.
-verbose

# Encrypt license-related strings from the sample application, including the
# Base64-encoded public key from the Google Play store (specified using
# wildcards here) and the identifier of the unique device ID.
-encryptstrings
  "M???????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????",
  "http://market.android.com/details?id=",
  "android_id"

# Encrypt strings from the License Verification Library.
-encryptstrings
  "com.android.vending.licensing.ILicensingService",
  "com.android.vending.licensing.ILicenseResultListener",
  "com.android.vending.licensing.ServerManagedPolicy",
  "com.android.vending.licensing.AESObfuscator-1|",
  "background thread",

  # Encryption parameters.
  "RSA",
  "SHA1withRSA",
  "PBEWITHSHAAND256BITAES-CBC-BC",
  "AES",
  "AES/CBC/PKCS5Padding",

  # Data fields from the default policy.
  "lastResponse",
  "validityTimestamp",
  "retryUntil",
  "maxRetries",
  "retryCount",
  "maxRetries",
  "retryUntil",
  "VT",
  "GT",
  "GR",

  # Exception messages (actually removed from our copy of the library).
  "Wrong number of fields.",
  "Invalid environment",
  "Header not found (invalid data or key):",
  "invalid padding byte '=' at byte offset ",
  "padding byte '=' falsely signals end of encoded value at offset ",
  "encoded value has invalid trailing byte",
  "Bad Base64 input character at ",
  "(decimal)",
  "single trailing character at offset "

# Access some run-time APIs through reflection, since these quite explicitly
# point to our license check and they are potential points of attack.
# We're writing them out here, but we could also have used wildcards.
# Additionally, encrypt the strings resulting from this reflection.
-accessthroughreflection,encryptstrings class android.content.Context {
    android.content.pm.PackageManager getPackageManager();
}

-accessthroughreflection,encryptstrings class android.content.pm.PackageManager {
    int checkSignatures(int, int);
    int checkSignatures(java.lang.String, java.lang.String);
    android.content.pm.InstrumentationInfo getInstrumentationInfo(android.content.ComponentName, int);
    java.util.List queryInstrumentation(java.lang.String, int);
    android.content.pm.PackageInfo getPackageInfo(java.lang.String, int);
    android.content.pm.PackageInfo getPackageArchiveInfo(java.lang.String, int);
}

-accessthroughreflection,encryptstrings class android.content.pm.PackageInfo {
    java.lang.String                         packageName;
    java.lang.String                         versionName;
    int                                      versionCode;
    long                                     firstInstallTime;
    long                                     lastUpdateTime;
    android.content.pm.Signature[]           signatures;
    android.content.pm.InstrumentationInfo[] instrumentation;
}

-accessthroughreflection,encryptstrings class java.security.KeyFactory {
    java.security.PublicKey generatePublic(java.security.spec.KeySpec);
    java.security.KeyFactory getInstance(java.lang.String);
}

-accessthroughreflection,encryptstrings class java.security.spec.KeySpec

-accessthroughreflection,encryptstrings class java.security.spec.AlgorithmParameterSpec

-accessthroughreflection,encryptstrings class java.security.PublicKey

-accessthroughreflection,encryptstrings class java.security.Key {
    byte[] getEncoded();
}

-accessthroughreflection,encryptstrings class java.security.SecureRandom {
    <init>();
    int nextInt();
}

-accessthroughreflection,encryptstrings class java.security.Signature {
    java.security.Signature getInstance(java.lang.String);
    void                    initVerify(java.security.PublicKey);
    void                    update(byte[]);
    boolean                 verify(byte[]);
}

-accessthroughreflection,encryptstrings class java.security.spec.X509EncodedKeySpec {
    <init>(byte[]);
}

-accessthroughreflection,encryptstrings class javax.crypto.Cipher {
    javax.crypto.Cipher getInstance(java.lang.String);
    void                init(int, java.security.Key, java.security.spec.AlgorithmParameterSpec);
    byte[]              doFinal(byte[]);
}

-accessthroughreflection,encryptstrings class javax.crypto.SecretKeyFactory {
    javax.crypto.SecretKeyFactory getInstance(java.lang.String);
    javax.crypto.SecretKey        generateSecret(java.security.spec.KeySpec);
    byte[]                        getEncoded();
}

-accessthroughreflection,encryptstrings class javax.crypto.SecretKey {
    byte[] getEncoded();
}

-accessthroughreflection,encryptstrings class javax.crypto.spec.SecretKeySpec {
    <init>(byte[], java.lang.String);
}

-accessthroughreflection,encryptstrings class javax.crypto.spec.IvParameterSpec {
    <init>(byte[]);
}

-accessthroughreflection,encryptstrings class javax.crypto.spec.PBEKeySpec {
    <init>(char[], byte[], int, int);
}

# Remove Android logging code (at least in the release version, for which
# optimization is switched on). This is important, to remove many obvious
# debug traces in the License Verification Library.
-assumenosideeffects class android.util.Log {
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}
