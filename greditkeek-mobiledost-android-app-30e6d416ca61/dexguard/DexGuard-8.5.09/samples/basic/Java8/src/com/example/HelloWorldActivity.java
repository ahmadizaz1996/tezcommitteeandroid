/*
 * Sample application to illustrate processing with DexGuard.
 *
 * Copyright (c) 2017 Guardsquare NV
 */
package com.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.*;

/**
 * Sample activity that displays a button with an OnClickListener.
 */
public class HelloWorldActivity extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        // Display the message.
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener((view) -> {
            Toast.makeText(this, new GreetingImpl().getGreeting(), Toast.LENGTH_LONG).show();
        });

        Toast.makeText(this, "DexGuard has backported this Java 8 sample to Android", Toast.LENGTH_LONG).show();
    }
}
