This sample illustrates how DexGuard can be used to protect
an aar file in standalone mode.

To execute it run the following command (assuming the environment
variable DEXGUARD_HOME is set to point to the DexGuard installation
directory):

Windows:

    %DEXGUARD_HOME%\bin\dexguard.bat @dexguard-library-standalone.txt

Linux/Mac:

    $DEXGUARD_HOME/bin/dexguard.sh @dexguard-library-standalone.txt

After the execution has finished successfully, the protected
library will be available as `Library-protected.aar`.
