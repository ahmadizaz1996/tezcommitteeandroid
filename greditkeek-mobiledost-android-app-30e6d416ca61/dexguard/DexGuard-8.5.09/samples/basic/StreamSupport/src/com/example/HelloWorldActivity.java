/*
 * Sample application to illustrate Java 8 stream API conversion with DexGuard.
 *
 * Copyright (c) 2017 Guardsquare NV
 */
package com.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.*;

import java.util.*;
import java.util.stream.*;
import java.util.function.*;

/**
 * Sample activity that displays a button with an OnClickListener.
 */
public class HelloWorldActivity extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        StreamTests.streamTests();

        // Display the message.
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener((view) -> {
            Toast.makeText(this, "Hello, world!", Toast.LENGTH_LONG).show();
        });

        Toast.makeText(this, "DexGuard has backported this Java 8 stream sample to Android", Toast.LENGTH_LONG).show();
    }
}
