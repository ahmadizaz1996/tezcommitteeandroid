buildscript {
    repositories {
        flatDir { dirs("../../../lib") } // For the DexGuard plugin.
        google()                         // For the Android plugin.
        jcenter()                        // For anything else.
    }
    dependencies {
        classpath("com.android.tools.build:gradle:3.2.1")
        classpath(":dexguard:")
    }
}
