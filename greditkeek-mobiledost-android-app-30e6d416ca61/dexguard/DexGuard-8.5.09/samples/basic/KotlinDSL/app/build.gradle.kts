plugins {
    id("com.android.application")
    id("dexguard")
}

repositories {
    google()  // For the Android support libraries.
    jcenter() // For any other libraries.
}

android {
    compileSdkVersion(28)

    signingConfigs {
        getByName("debug") {
            storeFile     = file("../../../debug.keystore")
            storePassword = "android"
            keyAlias      = "androiddebugkey"
            keyPassword   = "android"
        }
    }

    defaultConfig {
        minSdkVersion(4)
        targetSdkVersion(28)
        signingConfig = signingConfigs["debug"]
    }

    sourceSets {
        getByName("main") {
            manifest.srcFile("AndroidManifest.xml")
            java.srcDir("src")
            resources.srcDir("src")
            aidl.srcDir("src")
            renderscript.srcDir("src")
            res.srcDir("res")
            assets.srcDir("assets")
        }
    }

    buildTypes {
        getByName("debug") {
            proguardFile(extraDexGuardMethods.getDefaultDexGuardFile("dexguard-debug.pro"))
            proguardFile("dexguard-project.txt")
            //proguardFile("dexguard-project-debug.txt")
            proguardFile("proguard-project.txt")
        }
        getByName("release") {
            proguardFile(extraDexGuardMethods.getDefaultDexGuardFile("dexguard-release-aggressive.pro"))
            proguardFile("dexguard-project.txt")
            //proguardFile("dexguard-project-release.txt")
            proguardFile("proguard-project.txt")
        }
    }
}

// Show DexGuard's console output, so we can see some statistics.
//afterEvaluate {
//    dexguardRelease.logging.level = "INFO"
//}
