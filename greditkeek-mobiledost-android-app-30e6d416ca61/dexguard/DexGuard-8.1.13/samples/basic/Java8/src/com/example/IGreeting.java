/*
 * Sample application to illustrate processing with DexGuard.
 *
 * Copyright (c) 2017 GuardSquare NV
 */
package com.example;

public interface IGreeting
{
    static String getOS() {
        return "Android";
    }

    default String getGreeting() {
        return "Hello";
    }
}
