/*
 * This sample application illustrates how DexGuard can automatically transform
 * a traditional installed app into an Instant App.
 *
 * Copyright (c) 2012-2017 GuardSquare NV
 */
package com.example;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

/**
 * Sample activity that displays "Hello!".
 */
public class HelloActivity extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Display the view.
        setContentView(R.layout.hello);

        // Show the received message, if any.
        Intent intent = getIntent();
        if (intent.hasExtra(Message.ID))
        {
            Message  message  = (Message)intent.getParcelableExtra(Message.ID);
            TextView textView = (TextView)findViewById(R.id.hello_text);

            textView.setText("Hello " + message.getValue() + "!");
        }

        // Attach a listener to the button.
        findViewById(R.id.hello_button).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Send an intent with a message to the Goodbye activity.
                Intent intent =
                    new Intent(HelloActivity.this, GoodbyeActivity.class)
                    .putExtra(Message.ID, new Message("Hello feature"));

                startActivity(intent);
            }
        });

        // Briefly display a comment.
        Toast.makeText(this, "DexGuard has automatically converted this sample into an Instant App", Toast.LENGTH_LONG).show();
    }
}
