/*
 * Sample application to illustrate processing with DexGuard.
 *
 * Copyright (c) 2012-2017 GuardSquare NV
 */
package com.example;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * A parcelable message to pass between activities.
 */
public class Message implements Parcelable
{
    public static String ID = "com.example.Message";


    private String value;


    public Message(String value)
    {
        this.value = value;
    }


    public String getValue()
    {
        return value;
    }


    // Implementations for Parcelable.

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags)
    {
        parcel.writeString(value);
    }

    public static final Creator<Message> CREATOR = new Creator<Message>()
    {
        // Implementations for Creator.

        @Override
        public Message createFromParcel(Parcel parcel)
        {
            return new Message(parcel.readString());
        }

        @Override
        public Message[] newArray(int size)
        {
            return new Message[size];
        }
    };
}
