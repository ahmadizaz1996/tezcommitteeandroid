/*
 * Sample application to illustrate processing with DexGuard.
 *
 * Copyright (c) 2017 GuardSquare NV
 */
package com.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.*;

/**
 * Sample activity that displays a button with an OnClickListener.
 */
public class HelloWorldActivity extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Fetch and display the message.
        TextView view = new TextView(this);
        view.setText(HelloWorld.getMessage());
        view.setGravity(Gravity.CENTER);
        setContentView(view);

        // Briefly display a comment.
        Toast.makeText(this, "DexGuard has backported the Java 9 code included in this sample.", Toast.LENGTH_LONG).show();
    }
}
