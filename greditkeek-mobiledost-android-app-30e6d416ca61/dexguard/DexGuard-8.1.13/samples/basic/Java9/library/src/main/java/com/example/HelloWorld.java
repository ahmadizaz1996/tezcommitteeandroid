/*
 * Sample application to illustrate processing with DexGuard.
 *
 * Copyright (c) 2015-2017 GuardSquare NV
 */
package com.example;

public class HelloWorld {

    public static String getMessage()
    {
        String m1 = "Hello ";
        String m2 = "world";
        char   m3 = '!';
        return m1 + m2 + m3;
    }
}
