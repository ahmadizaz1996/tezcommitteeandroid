/*
 * Sample application to illustrate processing with DexGuard.
 *
 * Copyright (c) 2017 Guardsquare NV
 */
package com.example;

public class GreetingImpl implements IGreeting
{
    public String getGreeting() {
        return IGreeting.super.getGreeting() + " World on " + IGreeting.getOS();
    }
}
