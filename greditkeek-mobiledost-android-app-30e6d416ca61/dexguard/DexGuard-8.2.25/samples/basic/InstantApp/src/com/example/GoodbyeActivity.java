/*
 * This sample application illustrates how DexGuard can automatically transform
 * a traditional installed app into an Instant App.
 *
 * Copyright (c) 2012-2018 Guardsquare NV
 */
package com.example;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

/**
 * Sample activity that displays "Goodbye!".
 */
public class GoodbyeActivity extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Display the view.
        setContentView(R.layout.bye);

        // Show the received message, if any.
        Intent intent = getIntent();
        if (intent.hasExtra(Message.ID))
        {
            Message  message  = (Message)intent.getParcelableExtra(Message.ID);
            TextView textView = (TextView)findViewById(R.id.bye_text);

            textView.setText("Goodbye " + message.getValue() + "!");
        }

        // Attach a listener to the button.
        findViewById(R.id.bye_button).setOnClickListener(new View.OnClickListener() 
        {
            @Override
            public void onClick(View view) 
            {   
                // Send an intent with a message to the Hello activity.
                Intent intent =
                    new Intent(GoodbyeActivity.this, HelloActivity.class)
                    .putExtra(Message.ID, new Message("Bye feature"));

                startActivity(intent);
            }
        });

        // Briefly display a comment.
        Toast.makeText(this, "DexGuard has automatically converted this sample into an Instant App", Toast.LENGTH_LONG).show();
    }
}
