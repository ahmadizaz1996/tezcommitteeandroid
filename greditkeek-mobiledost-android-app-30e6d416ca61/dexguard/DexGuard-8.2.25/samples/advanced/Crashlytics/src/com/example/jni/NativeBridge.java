/*
 * Sample application to illustrate processing with DexGuard.
 *
 * Copyright (c) 2012-2018 Guardsquare NV
 */
package com.example.jni;

/**
 * Sample class that loads a native library and provides a native method.
 */
public class NativeBridge
{
    static {
        System.loadLibrary("nativecrash");
    }


    /**
     * Returns the secret string "Hello world!".
     */
    public native void crashOnNull();
}
