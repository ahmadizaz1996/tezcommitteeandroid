/*
 * Sample application to illustrate processing with DexGuard.
 *
 * Copyright (c) 2012-2018 Guardsquare NV
 */
package com.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.example.jni.NativeBridge;

import io.fabric.sdk.android.Fabric;

/**
 * Sample activity that allows to trigger a Java & native crash collected by crashlytics.
 */
public class HelloWorldActivity extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Fabric fabric =
            new Fabric.Builder(this)
                .kits(new Crashlytics(), new CrashlyticsNdk())
                // Optionally enable debugging
                //.debuggable(true)
                .build();

        Fabric.with(fabric);

        setContentView(R.layout.main);
    }

    public void forceJavaCrash(View view)
    {
        throw new RuntimeException("This is a crash");
    }

    public void forceNativeCrash(View view)
    {
        (new NativeBridge()).crashOnNull();
    }

}
