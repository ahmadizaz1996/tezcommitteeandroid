#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL Java_com_example_jni_NativeBridge_crashOnNull
  (JNIEnv *env, jobject obj)
{
    int *x = NULL; *x = 42;
    //volatile unsigned int* ptr = 0;
    //*ptr = 0xDEAD;
}

#ifdef __cplusplus
}
#endif
