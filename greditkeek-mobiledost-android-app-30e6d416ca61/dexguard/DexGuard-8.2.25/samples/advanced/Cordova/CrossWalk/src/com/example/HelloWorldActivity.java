/*
 * Sample application to illustrate Cordova asset encryption with DexGuard.
 *
 * Copyright (c) 2012-2018 Guardsquare NV
 */
package com.example;

import android.os.Bundle;

import org.apache.cordova.*;
import org.crosswalk.engine.XWalkCordovaView;
import org.crosswalk.engine.XWalkWebViewEngine;

import com.guardsquare.dexguard.runtime.encryption.EncryptedXWalkCordovaResourceClient;

/**
 * Sample activity that shows a Cordova CrossWalk web view.
 */
public class HelloWorldActivity extends CordovaActivity
{
    // An arbitrary http URL prefix to refer to local assets
    // (encrypted or not).
    private static final String ENCRYPTED_ASSET_PREFIX = "file:///";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Start Cordova with the main page.
        super.onCreate(savedInstanceState);
        super.init();
        super.loadUrl(ENCRYPTED_ASSET_PREFIX + "www/index.html");
    }

    @Override
    protected CordovaWebViewEngine makeWebViewEngine() {
        XWalkWebViewEngine engine = (XWalkWebViewEngine) super.makeWebViewEngine();

        XWalkCordovaView webView = (XWalkCordovaView) engine.getView();

        // Configure an encrypted XWalkCordovaResourceClient on the webView.
        EncryptedXWalkCordovaResourceClient webViewClient =
            new EncryptedXWalkCordovaResourceClient(engine,
                                                    getAssets(),
                                                    ENCRYPTED_ASSET_PREFIX);
        webView.setResourceClient(webViewClient);

        return engine;
    }
}

